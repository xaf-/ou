<?php

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 02/07/2012
	 */

	OU_Config::IncClass(
		array(
			"OU_Base",
			"OU_Array",
			"OU_App",
			"OU_Comments"
		)
	);
	
	OU_Config::IncClassHelper("OU_Utils_Smarty");
	 
	/**
	 * Clase base de los controladores. Esta definida con methodos "abstractos" que permiten generar el contenido de las web y controlar las solicitudes.
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_controller.php 340 2014-03-16 23:37:36Z xaguilarf $
	 * 
	 * @property-read string $fileName
	 * @property-read OU_App $app
	 * 
	 * @event		beforePost
	 * @event		afterPost
	 * @event		beforeContent
	 * @event		afterContent
	 */
	class OU_Controller extends OU_Base
	{
		
		/**
		 * @var OU_App
		 */
		protected $_app;

        protected function getApp() { return $this->_app; }
		/**
		 * @var OU_Array
		 */
		protected $_vars;
		/**
		 * @var OU_Array
		 */
		protected $_routes;
		/**
		 * @var OU_Array
		 */
		protected $_request;
		protected $_fileName;
		protected function getFileName() { return $this->_fileName; }
		public static $_fileNames = array();
		protected $_className;
		
		protected static function _initializeStatic(){

        }

        public function valid(){
            return true;
        }

		protected function _initialize()
		{
		}

		/* Generates */
		
		protected function _getPath()
		{
			return OU_App::getAppPath($this->_app->appName);
		}
		
		/**
		 * @param string $fileName
		 * @param array|OU_Array $vars
		 * @return string
		 */
		protected function php($fileName, $vars = array())
		{
			$vars = OU_Array::FromArray($vars);
			$vars->Add("controller", $this);
			return $this->_app->php($fileName, null, $vars);	
		}
		
		/**
		 * @see OU_App::tpl()
		 * @param string $fileName
		 * @param array|OU_Array $vars
		 * @return string
		 */
		protected function tpl($fileName, $vars = array())
		{
			$vars = OU_Array::FromArray($vars);
			$vars->Add("controller", $this);
			OU_Utils_Smarty::assign($vars->toArray());
			return $this->_app->tpl($fileName);
		}
		
		protected function prePost()
		{
			return $this->post();
		}
		
		/**
		 * Se ejecuta justo antes del content()
		 * @return null
		 */
		protected function pre_content() { }
		
			/**
		 * @param OU_App $app
		 * @param string $fileName
		 */
		public function __construct($app, $fileName)
		{
			$this->_app = $app;
			$this->_vars = new OU_Array();
			$this->_routes = new OU_Array();
			$this->_request = new OU_Array();
			$this->_fileName = $fileName;
		}
		
		/**
		 * Genera el contenido de una pagina. (header + content + footer)
		 * @return string
		 */
		public function page() 
		{
			$this->_initialize();
			$post = $this->_post();
			if ($post !== false)
			{
				$this->pre_content();
                $content = $this->_content();

                return
					$this->_header() . 
					$content .
					$this->_footer();
			}else{
				return "";
			}
		}
		
		/**
		 * Controla las solicitudes antes de generar la pagina.
		 * @return boolean Si se devuelve FALSE <b>la página</b> no se generará
		 */
		public function post() { return true; }
		/**
		 * Genera el contenido de la cabecera de la web.
		 * @return string
		 */
		public function header() { return ""; }
		/**
		 * Genera el contenido del pie de página de la web.
		 * @return string
		 */
		public function footer() { return ""; }
		/**
		 * Genera el contenido de la web. El contenido aparece entre header() y footer()
		 * @return string
		 */
		public function content() { return ""; }
		
		/* --------- */

        /**
         * Incluye el archivo y lee todas las opciones de dicho archivo.
         * @param unknown_type $app
         * @param unknown_type $fileName
         * @return Ambiguous
         */
		public static function FromFile($app, $fileName)
		{
			return self::_loadDetails($app, $fileName);
		}

        /***
         * @param OU_Comments $comments
         * @param OU_App $app
         * @param string $fileName
         * @return bool True si el controlador tiene que incluirse en la aplicación.
         */
        protected static function IncludeFile($app, $fileName)
        {
            return true;
        }

        /**
		 * @return OU_Comments
		 */
		public static function getComments()
		{
			$class = get_called_class();
			if (!isset(self::$_tmpComments[$class]))
			{
				self::$_tmpComments[$class] = OU_Comments::fromFile(static::$_fileNames[$class], array("numAsterisk" => 3));
			}
			return self::$_tmpComments[$class];
		}

        /**
         * Recibe la dirección uri indicada en los comentarios del controlador. Si existe indicado más de uno se devolvera el que coincida con el número de parametros pasados.
         * @param string $param1
         * @param null $param2
         * @param null $param3
         * @return string
         * @internal param string $_
         */
		public static function uri($param1 = null, $param2 = null, $param3 = null)
		{
			$comments = self::getComments();
			$c = $comments->comment("uri");
				
			if ($c)
			{
				$values = array();
				foreach ($c->values() as $value)
				{
					$value = str_replace('{base}', OU_App::app()->baseRelativeUri(), $value);
					$values[substr_count($value, "#")] = $value;
				}
		
				ksort($values);
		
				$cnt = func_num_args();
		
				if (isset($values[$cnt]))
				{
					$c = $values[$cnt];
				}else if (count($values) > 0){
					$c = OU_Array::FromArray($values)->First();
				}else
					$c = "";
			}else
				$c = "";
				
			$cnt = 0;
			foreach (func_get_args() as $arg)
			{
				$cnt++;
				$c = str_replace("#" . $cnt, $arg === null ? "" : $arg, $c);
			}
				
			return OU_Config::$base_uri . "/" . $c;
		}
		
		/**
		 * Indica si esta clase es la clase que se está ejecutando actualmente.
		 * @param string $name Nombre de la clase del controlador.
		 * @return boolean
		 */
		public static function isControllerActive($name, $params = null)
		{
			$c = get_class(OU_App::app()->current);
			$b = $name === $c;
			if (!$b)
			{
				$b = is_subclass_of($c, $name);
			}
			if ($b && $params !== null)
			{
				if (OU_App::app()->current->_request->count() > 0 && (OU_App::app()->current->_request->First() === $params))
				{
					$b = true;
				}else{
					$b = false;
				}
			}
			return $b;
		}
		
		/**
		 * Indica si esta clase es la clase que se está ejecutando actualmente.
		 * @return boolean
		 */
		public static function isActive($params = null)
		{
			return self::isControllerActive(get_called_class(), $params);
		}
		
		/**
		 * Comprueba si este controlador coincide con un codigo HTTP.
		 * @param string $code
		 * @return boolean
		 */
		public function checkRouteCode($code)
		{
			if (isset($this->_vars["code"]) && $this->_vars["code"] == $code)
				return true;
			else
				return false;
		}
		
		/**
		 * Comprueba si este controladores coincide con la uri indicada y devuelve los parametros de la expresión regular de la ruta seleccionada.
		 * @param string $uri Defecto : $_SERVER["REQUEST_URI"].
		 * @return multitype:|boolean
		 */
		public function checkRoute($uri = false)
		{
			
			if ($uri === false)
			{
				$uri = $_SERVER["REQUEST_URI"];
			}
				
			if (($p = strpos($uri, "?")) !== false)
			{
				$uri = substr($uri, 0, $p);
			}
				
				
			foreach ($this->_routes->toArray() as $route)
			{
				$matches = array();
		
				$isFunc = strpos($route, 'fnc:');
				if ($isFunc)
				{
					$fnc = substr($route, strlen('fnc:'));
					if (call_user_func(array($this, $fnc), $uri))
					{
						return true;
					}
				}else{
					$r_req = '(?P<request>(|\?.*?))';
						
					$route = str_replace(
						'{base}',
						str_replace(
								array("\\", "/"),
								array("\\\\", "\\/"),
								$this->_app->baseRelativeUri()
						),
						$route
					);
					
					$r_match = '(?P<match>' . $route . ')';
					$r = '/^' . $r_match . $r_req . '$/';
						
					if (preg_match($r, $uri, $matches))
					{
						foreach ($matches as $k=>$v)
							if (!is_numeric($k) && $k != "match" && $k != "request")
							{
								$this->_request->Add($k, $v);
							}
								
							return $matches;
					}elseif (preg_match($r, $uri . "/", $matches))
					{
					    // Si la coincideix amb "/" al final:
					    $p = OU_Path::Create($_SERVER['REQUEST_URI'], true);
					    $q = $p->query;
					    OU_Header::Location($p->path . "/" . ($q ? "?" . $q : ""));
					    die();
					}
				}
			}
				
			return false;
				
		}
		
		private static $_tmpComments = array();
		private static function _parseComments($content)
		{
			return new OU_Comments($content, array("numAsterisk" => 3));
		}
		
		/**
		 * Lee si existen opciones "use". Estas opciones son leidas independientemente de las otras ya que, es necesario incluir los uses antes que el mismo archivo.
		 * @param string $content
		 * @param OU_App $app
		 */
		private static function _loadUses($comments, $app)
		{
			
			$matches = array();

			if ($comments->comment("use"))
			{
				foreach ($comments->comment("use")->values() as $use)
				{
					$line = $use;
					foreach (explode(",", $line) as $s_line)
					{
						$s_line2 = trim($s_line);
						if (strpos($s_line2, "OU_") !== false)
						{
							if (!$app->IncClass($s_line2))
							{
								OU_Config::IncClassHelper($s_line2);
							}
						}
						else
							if (!$app->IncController($s_line2))
								$app->IncClass($s_line2);
					}
				}
			}
		}

		/**
		 * Parecido a "use" pero solo para controladores. Incluye la aplicación el controlador indicado.
		 * @param string $content
		 * @param OU_App $app
		 */
		private static function _loadRequires($comments, $app)
		{

			$matches = array();

			if ($comments->comment("require"))
			{
				foreach ($comments->comment("require")->values() as $require)
				{
                    $f = $app->GetController($require);
                    if ($f)
                        $app->loadController($f);
				}
			}
		}
		
		/**
		 * Lee las opciones del archivo de un controlador.
		 * @param OU_App $app
		 * @param string $fileName
		 * @throws Exception
		 * @return static|bool
		 */
		private static function _loadDetails($app, $fileName)
		{
			
			$file_content = file_get_contents($fileName);
            $comments = static::_parseComments($file_content);

			self::_loadUses($comments, $app);
			self::_loadRequires($comments, $app);

			$matches = array();
			$content = &$file_content;

			if ($content && preg_match('/class (\w+) extends/mis', $content, $matches))
			{
				$className = $matches[1];
				if (preg_match('/namespace\s+([\w\\\\]+)\s*\;/mis', $content, $matches))
				{
					$className = "\\" . $matches[1] . "\\" . $className;
				}
			}else{
				throw new Exception("No se ha encontrado ninguna clase en '".basename($fileName)."'");
			}


            require_once($fileName);

			self::$_fileNames[trim($className, "\\")] = $fileName;

            if (!$className::IncludeFile($app, $fileName))
                return false;

            $className::_initializeStatic();

            $obj = new $className($app, $fileName);
			
			foreach ($comments->comments() as $comment)
			{
				/* @var $comment OU_Comment */
				foreach ($comment->values() as $val)
				{
					switch ($comment->name())
					{
						case "extend":
							$obj->_app->Extend(
								OU_Path::Absolute(
									$val,
									dirname($fileName)
								)
							);
							break;
						case "use":
							// Load before contructor class
							break;
						case "route":
							$obj->_routes->Add($val);
							break;
						default:
							$obj->_vars->Add($comment->name(), $val);
					}
				}
			}
			
			return $obj;
			
		}
		
		/**
		 * Lee la información de cada linea de opciones y ejecuta las operaciones. Existen las siguientes opciones:
		 * - use : Esta opción no sera procesada en este paso. Se ejecutará antes de incluir el archivo que se está procesando.
		 * - route : Permite especificar rutas URI para este archivo.
		 * - extend : Permite especificar direcciones relativas al mismo para ampliar las localizaciones donde se tienen que buscar los controladores.
		 * @param OU_Comment $comment
		 * @param string $fileName
		 */
		private function _loadDetail($comment, $fileName)
		{
			//$line = trim($line);			
			// Variable
			//if (substr($line, 0, 1) == "@")
			{
				//$matches = array();
				//if (preg_match('/^\@(\?P<varname>.*?)(| (\?P<params>.*?))$/', $line, $matches))
				{
					$varname = $comment->name(); //strtolower(trim($matches["varname"]));
					$params = $comment->value(); //$matches["params"];
					switch ($varname)
					{
						case "extend":
							$this->_app->Extend(
								OU_Path::Absolute(
									$params,
									dirname($fileName)
								)
							);
							break;
						case "use":
							// Load before contructor class
							break;
						case "route":
							$this->_routes->Add($params);
							break;
						default:
							$this->_vars->Add($varname, $params);
					}
				}
			}
		}
		
		private function _ob_fnc($fnc)
		{
			//ob_start();
			$r = call_user_func(array($this, $fnc), false);

			//$c = ob_get_contents();
			//@ob_end_clean();
			return /*$c . */$r;
		}
		
		private function _post()
		{
			OU_Debug::point("Controller - Post");
			$this->raise("beforePost");
			$res = $this->prePost();
			$this->raise("afterPost");
			return $res;
		}
		
		private function _header()
		{
			OU_Debug::point("Controller - Header");
			return $this->_ob_fnc("header");
		}
		
		private function _footer()
		{
			OU_Debug::point("Controller - Footer");
			return $this->_ob_fnc("footer");
		}
		
		private function _content()
		{
			OU_Debug::point("Controller - Content");
			$this->raise("beforeContent");
			$res = $this->_ob_fnc("content");
			$this->raise("afterContent");
			return $res;
		}

	}

