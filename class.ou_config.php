<?php

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 03/08/2012
	 */

	require_once(dirname(__FILE__)."/class.ou_base.php");
	 
	/**
	 * Clase principal de la API. Contiene las funciones y métodos que permiten realizar las operaciones más 
	 * habituales de manera mas sencilla y rápida.
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_config.php 338 2013-11-26 12:01:16Z xaguilarf $
	 * 
	 */
	class OU_Config extends OU_Base 
	{
		
		/**
		 * Nombre de la carpeta de las imágenes de la applicación.
		 * @var string
		 */
		public static $path_image = "imgs";
		/**
		 * Nombre de la carpeta donde se buscarán los archivos de javascript de la aplicación.
		 * @var string
		 */
		public static $path_js = "js";
		/**
		 * Nombre de la carpeta donde se buscarán los archivos css de la aplicación.
		 * @var string
		 */
		public static $path_css = "css";
		/**
		 * Nombre de la carpeta donde se encuentran las librerias externas.
		 * @var string
		 */
		public static $path_lib_ext = "ext";
		/**
		 * Nombre de la carpeta donde se alojarán las clases "helper".
		 * @var string
		 */
		public static $path_lib_helper = "helper";
		/**
		 * Nombre de la carpeta donde se buscarán las aplicaciones.
		 * @var string
		 */
		public static $path_apps = "apps";
		public static $path_apps_d = "apps";
		/**
		 * Base de la dirección URI de la aplicación. False si aún no se ha inicializado ningúna aplicación.
		 * @var string
		 */
		public static $base_uri = false;
		/**
		 * Nombre de la carpeta donde se guardara todas las caches de la aplicación.
		 * @var string|bool
		 */
		public static $path_cache = "cache";
		/**
		 * Nombre de la carpeta donde se alojarán los módulos.
		 * @var string|bool
		 */
		public static $path_modules = "modules";
		/**
		 * Nombre de la carpeta donde se guardaran los "datos" de la aplicación.
		 * @var string|bool
		 */
		public static $path_data = "data";
		/**
		 * Indica si se debe ejecutar las aplicaciones en modo desarrollo.
		 * @var bool
		 */
		public static $development = true;
		
		/**
		 * Static Constructor. Se ejecuta la función en cuanto haya la oportunidad.
		 */
		public static function __sconstructor()
		{
		}
		
		/**
		 * Recible la instancia de la clase OU_App del $appName indicado. Consulta la documentación de 
		 * OU_App::app() para más información.
		 * @see OU_App::app()
		 * @param string $appName Nombre de la aplicación
		 * @return OU_App Instancia de la clase OU_App
		 */
		public static function app($appName, $options = array())
		{
			OU_Config::IncClass("OU_App");
			return OU_App::app($appName, $options);
		}
		
		/**
		 * Devuelve el directorio de la libreria
		 * @return string
		 */
		public static function OUPath()
		{
			return dirname(__FILE__) . DIRECTORY_SEPARATOR;
		}
		
		/**
		 * Incluye un archivo PHP. Globalizando todas las variables antes.
		 * <code>
		 * OU_Config::Page("index.php");
		 * </code>
		 * @param string $var_relpath Dirección local relativa a la carpeta principal de la web.
		 * @return bool TRUE si se a realizado con exito o FALSE en caso de error.
		 */
		public static function Page($var_relpath)
		{
			
			$var_abspath = realpath(dirname(__FILE__) . "/../" . $var_relpath);
			if (file_exists($var_abspath) && is_file($var_abspath))
			{
				foreach ($GLOBALS as $k=>$v)
					if ($k != "var_relpath" && $k != "var_abspath")
						global ${$k};
				
				include($var_abspath);
				
				foreach (get_defined_vars() as $k=>$v)
					if ($k != "var_relpath" && $k != "var_abspath")
						$GLOBALS[$k]= $v;
						
				return true;
			}else
				return false;
					
		}
		
		/**
		 * Añade una dirección de inclusión. Utiliza la función set_include_path preservando las anteriores direcciones establecidas anteriormente.
		 * <code>
		 * OU_Config::AddIncludePath(dirname(__FILE__));
		 * </code>
		 * @param string $path,.. Dirección local
		 */
		public static function AddIncludePath($path)
		{
			$path = realpath($path);
			foreach (func_get_args() AS $path){
				if (!file_exists($path) OR (file_exists($path) && filetype($path) !== 'dir')){
					trigger_error("Include path '{$path}' not exists", E_USER_WARNING);continue;
				}
				$paths = explode(PATH_SEPARATOR, get_include_path());
				if (array_search($path, $paths) === false)
					array_push($paths, $path);
				set_include_path(implode(PATH_SEPARATOR, $paths));
			}
		}

		/**
		 * Borra una dirección de inclusión. Utiliza la función set_include_path preservando las anteriores direcciones establecidas anteriormente.
		 * <code>
		 * OU_Config::RemoveIncludePath(dirname(__FILE__));
		 * </code>
		 * @param string $path,.. Dirección local
		 */
		public static function RemoveIncludePath($path)
		{
			foreach (func_get_args() AS $path){
				$paths = explode(PATH_SEPARATOR, get_include_path());
				if (($k = array_search($path, $paths)) !== false)
					unset($paths[$k]);
				else 
					continue;
				if (!count($paths)){
					trigger_error("Include path '{$path}' can not be removed because it is the only", E_USER_NOTICE);
					continue;
				}
				set_include_path(implode(PATH_SEPARATOR, $paths));
			}
		}
		
		/**
		 * Incluye un archivo de la carpeta "/lib/".
		 * @param string $path Dirección relativa a la carpeta "/lib/"
		 */
		public static function IncludeLib($path)
		{
			include(dirname(__FILE__)."/" . $path);
		}
		
		/**
		 * Requiere un archivo de la carpeta "/lib/".
		 * @param string $path Dirección relativa a la carpeta "/lib/"
		 */
		public static function RequireLib($path)
		{
			require(dirname(__FILE__)."/" . $path);
		}
		
		/**
		 * Incluye unva vez un archivo de la carpeta "/lib/".
		 * @param string $path Dirección relativa a la carpeta "/lib/"
		 */
		public static function IncludeOnceLib($path)
		{
			include_once(dirname(__FILE__)."/" . $path);
		}
		
		/**
		 * Requiere unva vez un archivo de la carpeta "/lib/".
		 * @param string $path Dirección relativa a la carpeta "/lib/"
		 */
		public static function RequireOnceLib($path, $dir = false)
		{
			if (!$dir) $dir = dirname(__FILE__);
			require_once($dir . "/" . $path);
		}
		
		/**
		 * Alias de la función OU_Config::RequireOnceLib. Acepta una array.
		 * @param string|Array $path Dirección relativa a la carpeta "/lib/"
		 */
		public static function Inc($path, $dir = false)
		{
			if (!is_array($path))
				OU_Config::RequireOnceLib($path, $dir);
			else
				foreach ($path as $p)
					OU_Config::RequireOnceLib($p, $dir);
		}
		
		/**
		 * Requiere una vez un archivo PHP globalizando todas las variables antes.
		 * <code>
		 * OU_Config::IncGlobal("ou_config.php");
		 * </code>
		 * @param string $var_relpath Dirección local absolute.
		 */
		public static function IncGlobal($var_relabs)
		{
			foreach ($GLOBALS as $k=>$v)
				if ($k != "var_relabs")
					global ${$k};
					
			require_once($var_relabs);
			
			foreach (get_defined_vars() as $k=>$v)
				if ($k != "var_relabs")
					$GLOBALS[$k]= $v;
		}
		
		/**
		 * Requiere una vez un archivo PHP globalizando todas las variables antes.
		 * <code>
		 * OU_Config::IncGlobal("index.php");
		 * </code>
		 * @param string|array $var_relpath Dirección local relativa a la carpeta $var_basepath
		 * @param string $var_basepath Carpeta a buscar el archivo $var_relpath
		 */
		public static function IncRel($var_relpath, $var_basepath)
		{
			
			if (is_array($var_relpath))
			{
				foreach ($var_relpath as $r)
					self::IncRel($r, $var_basepath);
				return;
			}
			
			foreach ($GLOBALS as $k=>$v)
				if ($k != "var_relpath" && $k != "var_basepath")
					global ${$k};
					
			require_once(dirname($var_basepath) . "/" . $var_relpath);
			
			foreach (get_defined_vars() as $k=>$v)
				if ($k != "var_relpath" && $k != "var_basepath")
					$GLOBALS[$k]= $v;
		}
		
		
		/**
		 * Requiere un archivo de la carpeta OU_Config::$path_lib_ext
		 * @param array|OU_Array|string $path Dirección (o conjunto en una array) relativa a la carpeta OU_Config::$path_lib_ext. También se puede indicar el nombre de la libreria, y si los index.php estan bien introducidos se cargara la versión por defecto o la indicada.
		 * @param string $version Indica que versión de la libreria quieres incluir. Solo se utiliza en el caso que $path sera un string.
		 * @param string $rel_path Indica la dirección relativa en el caso que que $path se haya indicado el nombre de la libreria. 
		 */
		public static function IncExt($path, $version = null, $rel_path = null)
		{
			if (is_array($path))
			{
				$path = OU_Array::FromArray($path);
			}
			
			if ($path instanceof  OU_Array)
			{
				foreach ($path->toArray() as $v)
					self::IncExt($v);
				return;
			}
			
			$path = OU_Config::$path_lib_ext . "/" . $path;
			
			if (is_file(dirname(__FILE__) . "/" . $path))
				OU_Config::RequireOnceLib($path);
			else if (is_dir(dirname(__FILE__) . "/" . $path))
			{
				if ($version !== null)
				{
					$path = $path . "/" . $version;
				}else{
					//$path = $path;
				}
				if ($rel_path !== null)
					$path = $path . "/" . $rel_path;
				else
					$path = $path . "/index.php";
				OU_Config::RequireOnceLib($path);
			}				
		}
		
		/**
		 * Requiere un archivo de la carpeta OU_Config::$path_lib_helper
		 * @param string $path Dirección relativa a la carpeta OU_Config::$path_lib_ext
		 */
		public static function IncHelper($path)
		{
			OU_Config::RequireOnceLib(OU_Config::$path_lib_helper . "/" . $path);
		}
		
		/**
		 * Permite crear una clase automáticamente. Consulta la documentación de OU_Config::GetFileClass para más información.
		 * Opciones :
		 * 		- args 		- Argumentos para el contructor de la clase.
		 *		- file		- Especificar el archivo que contiene la clase manualmente. O FALSE para buscar el archivo.
		 * @param string $className nombre de la clase a crear
		 * @param Array|Options $options Permite especificar las opciones a utilizar.
		 * @return Una nueva instancia de la clase. FALSE si no se ha encontrado ningún archivo Si la función encuentra el archivo pero no la clase se devolvera la excepción nativa de PHP.
		 */
		public static function CreateClass($className, $options=array())
		{
			
			OU_Config::IncClass("Options");
			
			$options = OU_Options::FromArray(
					$options,
					array(
						"args" => array(),
						"file" => false
					)
				);
			
			$file = realpath($options->file);
			
			if ($file === false) $file = OU_Config::GetFileClass($className);
			
			if ($file === false) return false;
			
			OU_Config::RequireOnceLib($file);
			
			$reflect = new ReflectionClass($className);
			return call_user_func_array(array($reflect, "newInstanceArgs"), $options->args);
			
		}
		
		
		/**
		 * Busca el archivo que contiene una clase. Escribiendo el nombre de la clase (Nombre_Clase), la función buscará en los siguientes archivos y con el siguiente orden:
		 * - class.Nombre_Clase.php
		 * - class.nombre_clase.php
		 * - Nombre_Clase.php
		 * - nombre_clase.php
		 * @param string $className nombre de la clase a buscar
		 * @return string|bool Devolvera la dirección local o FALSE si no se ha encontrado el archivo.
		 */
		public static function GetFileClass($className, $path = false)
		{
			$file = false;
			
			$path = $path ? $path . "/" : dirname(__FILE__)."/";
			
			$a = explode("_", $className);
			if (count($a) > 2 && ($file = "class.".strtolower($className).".php") && realpath($path . "/".strtolower($a[1])."/" . $file)) return strtolower($a[1])."/" . $file;
			if (($file = "class.".strtolower($className).".php") && realpath($path . $file)) return $file;
			if (($file = "class.".$className.".php") && realpath($path . $file)) return $file;
			if (($file = $className.".php") && realpath($path . $file)) return $file;
			if (($file = strtolower($className).".php") && realpath($path . $file)) return $file;
			
			return false;
			
		}
		
		/**
		 * Busca y incluye el archivo que contiene la clase indicada. Consulta la documentación de
		 * OU_Config::GetFileClass para más información.
		 * @param string $className Nombre de la clase
		 * @return bool TRUE si se ha realizado con éxito.
		 */
		public static function IncClass($className, $path = false)
		{
			if (is_array($className))
			{
				foreach ($className as $c)
					OU_Config::IncClass($c, $path);
				return;
			}
			
			$file = OU_Config::GetFileClass($className, $path);
						
			if ($file === false)
				return false;
			else
			{
				OU_Config::Inc($file, $path);
				return true;
			}
		}
		
		/**
		 * Busca y incluye el archivo que contiene la clase indicada dentro de la carpeta OU_Config::$path_lib_helper. Consulta la documentación de OU_Config::GetFileClass para más información.
		 * @param string|Array $className Nombre de la clase o una array con los nombres a incluir.
		 * @return bool TRUE si se ha realizado con éxito.
		 */
		public static function IncClassHelper($className)
		{
			
			if (is_array($className))
			{
				foreach ($className as $c)
					OU_Config::IncClassHelper($c);
				return;
			}
			
			$file = OU_Config::GetFileClass($className, dirname(__FILE__)."/" . self::$path_lib_helper . "/");
			if ($file === false)
				return false;
			else
			{
				OU_Config::IncHelper($file);
				return true;
			}
		}
		
	}
	
	class_alias('OU_Config', 'OU');
	// Eclipse PDT OU Alias
	
	// Init Static Constructor
	OU_Config::__sconstructor();
	
	if (false) // IDE Support
	{
		/**
		 * Alias de OU_Config. <i>Esta clase esta definida en el código pero no existe. OU es un alias de la clase OU_Config definido por class_alias().
		 * 
		 * @see			OU_Config
		 * @author		fontcolor
		 * @package		OU Framework
		 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
		 * 
		 */
		class OU extends OU_Config {}
	}

?>