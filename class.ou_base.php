<?php

/*
 * The source code is given as is. The author is not responsible for any
 * possible damage done due to the use of this code. The component can be freely
 * used in any application. The complete source code remains property of the
 * author and may not be distributed, published, given or sold in any form as
 * such. No parts of the source code can be included in any other component or
 * application without written authorization of fontcolor. 25/06/2012
 */
	
	/**
	 * Añade la funcionalidad de getters y setters pudiende así por ejemplo definir el metodo getFieldTest para poder controlar la lectura del campo $this->fieldTest.
	 * Añade la funcionalidad de eventos (listener).
	 *
	 * @author fontcolor
	 * @package OU Framework
	 * @copyright Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_base.php 331 2013-11-06 00:14:51Z xaguilarf $
	 *           
	 */
	abstract class OU_Base {
		
		/**
		 * Listado de eventos registrados.
		 * @var unknown
		 */
		protected $events = array ();
		
		/*
		 * Sense construct! Aixi es permet crear clases amb constructor private/protected
		 public function __construct() {
		 	
		 }
		*/
		
		public function __isset($property)
		{
		    if (method_exists ( $this, "get" . ucfirst ( $property ) )) {
		        return true;
		    } else {
		        return isset($this->{$property});
		    }
		}
	
		/**
		 * Permite la creación de propiedades de acceso a través de "getters" y "setters".
		 * Si las funciones no están definidas, se devolvera el valor de la variable.
		 * Ejemplo: private $visible; function getVisible(){ return $this->visible;
		 * }; function setVisible($v){ $this->visible = $v; }
		 *
		 * @param string $property Nombre de la propiedad
		 * @return mixed
		 */
		public function __get($property) {
			if (method_exists ( $this, "get" . ucfirst ( $property ) )) {
				return call_user_func ( array (
						$this,
						"get" . ucfirst ( $property )
				) );
			} else {
				return $this->{$property};
			}
		}
	
		/**
		 * Véase la documentación de __get.
		 * 
		 * @param string $property Nombre de la propiedad
		 * @param string $value Valor a asignar
		 * @return mixed
		 */
		public function __set($property, $value) {
			if (method_exists ( $this, "set" . ucfirst ( $property ) )) {
				return call_user_func ( array (
						$this,
						"set" . ucfirst ( $property ) 
				), $value );
			} else {
				return $this->{$property} = $value;
			}
		}
		
		/**
		 * @param string $name
		 * @param array $args
		 * @return mixed
		 */
		public function __call($name, $args)
		{
			$n = $name."__instance";
			if (method_exists($this, $n))
				return call_user_func_array(array($this, $n), $args);
		}
		
		/**
		 * @param string $name
		 * @param array $args
		 * @return mixed
		 */
		public static function __callStatic($name, $args)
		{
			$n = $name."__static";
			$c = get_called_class();
			
			if (method_exists($c, $n))
				return call_user_func_array(array($c, $n), $args);
		}
		
		/**
		 * Añade una función para un evento en particular.
		 *
		 * @param string $event Nombre del evento
		 * @param string|function Referencia a una función, nombre de una función o código PHP para ejecutado con eval().
		 */
		public function on($event, $handler, $data=null) {
			if (! isset ( $this->events [$event] ))
				$this->events [$event] = array ();
			$this->events [$event] [] = array(
						"handler" => $handler,
						"data" => $data
					);
		}
		
		/**
		 * Borrar un evento previamente registrado.
		 *
		 * @param string $event 
		 * 			Nombre del evento
		 * @param string $handler 
		 * 			Referencia a una función, nombre de una función o código PHP. Si el valor es null, se borrarán 
		 * 			todos los eventos del tipo $event.
		 * @return bool TRUE 
		 * 			si se ha registrado correctamente. FALSE en caso de no encontrar ningún evento que coincida con 
		 * 			los parametros.
		 */
		public function off($event, $handler = null) {
			if (isset ( $this->events [$event] )) {
				if ($handler === null) {
					unset ( $this->events [$event] );
					return true;
				} else {
					if (is_array ( $this->events [$event] )) {
						$a = array ();
						foreach ( $this->events [$event] as $k => $v ) {
							if ($v["handler"] != $handler)
								$a [] = $v;
						}
						$r = count ( $a ) != count ( $this->events [$event] );
						$this->events [$event] = $a;
						return $r;
					}
				}
			}
			return false;
		}
		
		/**
		 * Permite ejecutar los eventos del tipo $event.
		 *
		 * @param string $event
		 *        	Nombre del evento
		 * @param array $params
		 *        	Listado de parametros que serán pasado a las funciones de los
		 *        	eventos. El primer parámetro en $params pasara a la segunda
		 *        	posición, y en la primera se obtendra una referencia a $this.
		 * @return bool El valor devuelto dependerá de todos los valores devueltos
		 *         en los eventos que se ejecuten. Si un evento devuelve FALSE el
		 *         resultado total de la función sera FALSE. También devolvera FALSE
		 *         si no existen eventos con el nombre $event.
		 */
		public function raise($event, $params = array()) {
			if (isset ( $this->events [$event] ) && is_array ( $this->events [$event] )) {
				$b = true;
				$a = array (
					$this 
				);
				foreach ( $params as &$v2 )
				{
					$a[] = &$v2;
				}
				foreach ( $this->events [$event] as $k => $v ) {
					$a2 = $a;
					$a2[] = $v["data"]; // Add $data as last param  
					$b = $b && ($this->_internalRaiseEvent ( $event, $v["handler"], $a2 ) === false ? false : true);
				}
				return $b;
			}
			return false;
		}
		
		/**
		 * Crea una copia de este objeto.
		 * 
		 * @return OU_Base
		 */
		public function CloneObject() {
			return clone $this;
		}
		
		/**
		 * @see OU_Base::on
		 * @deprecated
		 */
		public function AddEventHandler($event, $handler, $data=null)
		{
			return $this->on($event, $handler, $data);
		}
		
		/**
		 * @see OU_Base::off
		 * @deprecated
		 */
		public function RemoveEventHandler($event, $handler = null) {
			return $this->off($event, $handler);
		}
		
		/**
		 * @see OU_Base::raise
		 * @deprecated
		 */
		public function RaiseEventHandler($event, $params = array()) {
			return $this->do($event, $params);
		}
		
		private function _internalRaiseEvent($event, $handler, $params = array()) {
			if (is_string ( $handler )) {
				if (function_exists ( $handler )) {
					call_user_func_array ( $handler, $params );
				}
			}else{
				if (is_callable($handler))
				{
					call_user_func_array( $handler, $params );
				}
			}
		}
		
	}

?>