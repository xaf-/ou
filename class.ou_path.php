<?php

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 14/07/2012
	 */

	OU_Config::IncClass(
		array(
			"OU_Base",
			"OU_Options"
		)
	);
	 
	/**
	 * Controla direcciones locales.
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_path.php 322 2013-10-24 10:58:11Z xaguilarf $
	 * @example		ou_path.php
	 * @example		ou_path/breadcrumb.php
	 * @example		ou_path/instance.php
	 * @see 		OU_Path_Instance
	 */
	class OU_Path extends OU_Base
	{
		
		// Tradueix en el Correct
		/**
		 * @var array
		 */
		private static $ALIAS = array(
//			"/back/" => "Y:/"
		);
		
		/**
		 * Crea una instancia de OU_Path_Instance
		 * @param string $path
		 * @return OU_Path_Instance
		 * @see OU_Path_Instance
		 */
		public static function Create($path, $isURL = false)
		{
			return new OU_Path_Instance($path, $isURL);
		}
		
		/**
		 * Crea una instancia de OU_Path_Instance de la url actual.
		 * @return OU_Path_Instance
		 */
		public static function CurrentURL()
		{
			return self::Create(
				(isset($_SERVER["REQUEST_SCHEME"]) ? $_SERVER["REQUEST_SCHEME"] : "http") . "://" . 
				$_SERVER["HTTP_HOST"] . 
				$_SERVER["REQUEST_URI"],
				true
			);
		}
		
		private static function _Path2Url($relPath, $options=false)
		{
			
			OU_Config::IncClass("OU_Options");
			
			$options = OU_Options::FromArray(
					$options,
					array(
						"useHost" 	=> true,
						"useSSH" 	=> "auto"
					)
				);
			
			$filePathName = realpath($relPath);
			$filePath = realpath(dirname($relPath));
			
			if ($filePathName === false) return false;
			
			$basePath = realpath($_SERVER['DOCUMENT_ROOT']);
			
			// Comprueba que $basePath tenga una cantidad de caracteres menores a $filePath
			if (strlen($basePath) > strlen($filePath)) { return false; }
			
			// Extrae la parte relative de DOCUMENT_ROOT y el archivo especificado
			$filePathName = substr($filePathName, strlen($basePath));
			$filePathName = str_replace("\\", "/", $filePathName); 
			
			if (strlen($filePathName) > 0 && $filePathName[0] != "/") $filePathName = "/" . $filePathName;
			
			$http = "http";
			if ($options->useSSH !== false)
			{
				$http = "http";
				if ($options->useSSH === true || isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on")
					$http = "https";
			}
			
			$r = $options->useHost ? ($http.'://' . $_SERVER['HTTP_HOST'] . $filePathName) : $filePathName;
			$r = str_replace("\\", "/", $r); 
			
			return $r;
		}
		
		/**
		 * Añade DIRECTORY_SEPARATOR al final de una ruta.
		 * @param string $path
		 * @return string
		 */
		public static function IncludePathDelimiter($path)
		{
			$last = substr($path, -1);
			if ($last != "/" && $last != "\\") 
				return $path . DIRECTORY_SEPARATOR;
			else
				return $path;
		}
		

		/**
		 * Convierte una dirección local a dirección URL.
		 * 
		 * @param string $localPath Dirección local de un archivo o directorio
		 * @param OU_Array|OU_Options $options
		 * @example ou_path/path2url.php
		 * @return string|bool Dirección URL
		 */
		public static function Path2Url($localPath, $options=array())
		{
			$r = OU_Path::_Path2Url($localPath, $options);
			return $r;
		}
		
		/**
		 * Devuelve en formato HTML un Breadcrumb a partir de una array.
		 * 
		 * @param array $a Formato : $a[url] = text;. Si url no esta definido el elemento será generado como <span> y no como <a>.
		 * @return string Código HTML del BreadCrumb.
		 */
		public static function BreadcrumbA($a)
		{
			$a2 = array();
			foreach ($a as $k=>$v)
			{
				if (!is_int($k))
					$a2[] = '<a href="'.$v.'">'.$k.'</a>';
				else
					$a2[] = '<a href="javascript:void(0)">'.$v.'</a>';
			}
			
			return '<span class="breadcrumb breadcrumb-inline">'.implode("/", $a2).'</span>';
			
		}
		
		/**
		 * Separa las carpetas de una dirección en partes.
		 * @param string $path
		 * @param bool $validate
		 * @return array
		 */
		public static function Split($path, $validate=true)
		{
			if (!$path) return array();
			$p = $validate ? OU_Path::Path2Url($path, array("useHost" => false)) : OU_Path::Correct($path, array("check" => false, "separator" => "/"));
			$a = explode("/", $p);
			$a2 = array();
			foreach ($a as $b)
				if ($b) $a2[] = $b;
			return $a2;
		}
		
		/**
		 * Convierte una ruta de archivo o directorio a breadcrumb en código HTML. 
		 * 
		 * @param 	string $path Dirección del archivo
		 * @param 	boolean $validate Si es TRUE la dirección será comprobada y en caso de no existir la función no devolvera ningún valor.
		 * @example ou_path/breadcrumb.php
		 * @return 	string Código HTML del BreadCrumb
		 * @see		OU_Path::BreadcrumbA() test askdjasdklj asdkjlh
		 */
		public static function Breadcrumb($path, $validate=true)
		{
			$p = $validate ? OU_Path::Path2Url($path, array("useHost" => false)) : $path;
			$a = explode("/", $p);
			$a2 = array();
			
			$p = "";
			
			foreach ($a as $v)
			{
				$p .= $v . "/";
				$url = OU_Path::Path2Url($p);
				if ($p && $p != "")
					$a2[$v] = $url;
				else
					$a2[] = $v;
			}
			
			return OU_Path::BreadcrumbA($a2);
			
		}
		
		/**
		 * Convierte una dirección relativa a la carpeta principal (dirname(__FILE__)."/..").
		 * 
		 * @param string $path Dirección local relativa a la carpeta principal
		 * @param boolean $withOutHost Si el valor es TRUE devuelve la dirección sin el host ni "http" delante.
		 * @return string|boolean Dirección URL
		 */
		public static function PathRoot($path, $options=array())
		{
			return OU_Path::Path2Url(dirname(__FILE__) . "/../" . $path, $options);
		}
		
		/**
		 * Convierte una dirección relativa a la carpeta de imágenes en URL.
		 * 
		 * @param string $path Dirección local relativa a la carpeta de imágenes.
		 * @param boolean $withOutHost Si el valor es TRUE devuelve la dirección sin el host ni "http" delante.
		 * @return string|boolean Dirección URL
		 */
		public static function PathImg($path, $options=array())
		{
			return OU_Path::Path2Url(dirname(__FILE__) . "/../".OU_Config::$path_image."/" . $path, $options);
		}
				
		/**
		 * Convierte una dirección relativa a la carpeta de Javascript en URL.
		 * 
		 * @param string $path Dirección local relativa a la carpeta de javascript.
		 * @param boolean $withOutHost Si el valor es TRUE devuelve la dirección sin el host ni "http" delante.
		 * @return string|boolean Dirección URL.
		 */
		public static function PathJs($path, $options=array())
		{
			return OU_Path::Path2Url(dirname(__FILE__) . "/../js/" . $path, $options);
		}
		
		/**
		 * Convierte una dirección relativa a la carpeta de Javascript Closeure en URL.
		 * 
		 * @param string $path Dirección local relativa a la carpeta de javascript.
		 * @return string|boolean Dirección URL.
		 */
		public static function PathXJs($path)
		{
			$r1 = OU_Path::Path2Url(dirname(__FILE__) . "/../");
			$r2 = OU_Path::Path2Url(dirname(__FILE__) . "/../js/" . $path);
			return $r2 !== FALSE ? str_replace($r1 . "/js/", $r1 . "/xjs/", $r2) : false;
		}
		
		/**
		 * Convierte una dirección relativa a la carpeta de css en URL.
		 * 
		 * @param string $path Dirección local relativa a la carpeta de css.
		 * @return string|boolean Dirección URL
		 */
		public static function PathXCss($path)
		{
			$r1 = OU_Path::Path2Url(dirname(__FILE__) . "/../");
			$r2 = OU_Path::Path2Url(dirname(__FILE__) . "/../".OU_Config::$path_css."/" . $path);
			return $r2 !== FALSE ? str_replace($r1 . "/".OU_Config::$path_css."/", $r1 . "/xcss/", $r2) : false;
		}
		
		/**
		 * Convierte una dirección relativa a la carpeta de css en URL.
		 * 
		 * @param string $path Dirección local relativa a la carpeta de css.
		 * @param array|OU_Array|OU_Options Opciones de Path2Url.
		 * @see OU_Path::Path2Url()
		 * @return string|boolean Dirección URL
		 */
		public static function PathCss($path, $options=array())
		{
			return OU_Path::Path2Url(dirname(__FILE__) . "/../".OU_Config::$path_css."/" . $path, $options);
		}
		
		/**
		 * Recibe la URL base de los thumbs.
		 * @param array|OU_Array|OU_Options $options
		 * @see OU_Path::Path2Url()
		 * @return string
		 */
		public static function PathThumb($options=array())
		{
			return OU_Path::Path2Url(dirname(__FILE__) . "/../".OU_Config::$path_image."/thumb/", $options) . "/";
		}	
		
		/**
		 * Recibe la URL de un Thumb.
		 * @param string $image_src Dirección local relativa de una imagen.
		 * @param number $w Ancho de la imagen a generar.
		 * @param unknown $h Alto de la imagen a generar.
		 * @param array|OU_Array|OU_Options $options Opciones de Path2Url. 
		 * @param string $zc
		 * @see OU_Path::Path2Url()
		 * @return string
		 */
		public static function PathThumbS($image_src, $w, $h, $options=array(), $zc=NULL)
		{
			return OU_Path::Path2Url(dirname(__FILE__) . "/../".OU_Config::$path_image."/thumb/", $options) . "?src=" . urlencode($image_src) . "&w=$w&h=$h&f=png" . ($zc ? "&zc=" . $zc : ""); 
		}
	
		/**
		 * Devuelve una dirección URL sin los parametros $_GET
		 * 
		 * @param string $url Dirección URL
		 * @return string Dirección URL sin parametros
		 */
		public static function DelQuery($url)
		{
			if (strpos($url, "?") > -1)
			{
				return substr($url, 0, strpos($url, "?"));
			}
			return $url;
		}
		
		/**
		 * Corrige una dirección local, comprueba que este bien escrita y verifica que la dirección existe.
		 * 
		 * @param string $path Dirección local o relativa a la raíz.
		 * @return boolean|string Dirección local
		 */
		public static function Correct($path, $options = array())
		{
			
			if (is_bool($options))
				$options = array("check" => $options);
			
			$options = OU_Options::FromArray(
				$options,
				array(
					"endSeparator" => true,
					"check" => true,
					"separator" => DIRECTORY_SEPARATOR
				)
			);
			
			$p = str_replace("\\", $options->separator, $path);
			$p = str_replace("/", $options->separator, $p);
			while (strpos($p, $options->separator.$options->separator) !== false)
				$p = str_replace($options->separator.$options->separator, $options->separator, $p);
			
			$a = explode($options->separator, $p);
			$p = "";
			
			$a = array_reverse($a);
			
			$ignore_next = 0;
			
			$a2 = array();
			foreach ($a as $b)
			{
				//if (trim($b) == "") continue;
				if ($ignore_next <= 0 && $b != "..")
				{
					$a2[] = $b;
				}
				if ($b == "..")
					$ignore_next++;
				else if ($ignore_next > 0)
					$ignore_next--;
			}
			$a = array_reverse($a2);
			
			$p = implode($options->separator, $a);
			
			$p = OU_Path::DelQuery($p);
			
			foreach (OU_Path::$ALIAS as $k=>$v)
			{
				if (strpos($p, $k) === 0)
				{
					$p = $v . substr($p, strlen($k) );
				}
			}

			if ($options->check)
				$p = realpath($p);
			
			if ($p === false) return false;

			if (is_file($p))
				return $p;
			else
                if (@$p{strlen($p)-1} == $options->separator)
                {
                    if ($options->endSeparator)
                        return $p;
                    else
                        return substr($p, 0, strlen($p) - 1);
                }else{
                    if ($options->endSeparator)
                        return $p . $options->separator;
                    else
                        return $p;
                }
		}
		
		/**
		 * Devuelve la extensión de un fichero. 
		 * 
		 * @param string $path Dirección de un archivo
		 * @return string|boolean
		 */
		public static function Ext($path, $full = true)
		{
			// if ($manual) {
				$pos = strrpos(str_replace("\\", "/", $path), "/");
				if ($pos === false)
				{
					$pos = 0;
				}
				if ($full)
					$pos = strpos($path, ".", $pos);
				else
					$pos = strrpos($path, ".", $pos);
				
				if ($pos !== false)
					return substr($path, $pos);
				
			/*}else{
				$p = pathinfo(OU_Path::Correct($path));
				return isset($p["extension"]) && $p["extension"]?strtolower(".".$p["extension"]):false;
			}*/
		}
		
		/**
		 * Comprueba si una dirección es relativa
		 * @param string $path
		 */
		public static function IsRelative($path)
		{
			$p = str_replace("\\", "/", $path);
			if (strlen($p) > 2 && ($p{0} == "/" || $p{1} == ":"))
			{
				return false;
			}
			return true;
		}
		
		public static function Path2Array($path)
		{
			$path = OU_Path::Correct($path, array("endSeparator" => false, "check" => false));
			return explode(DIRECTORY_SEPARATOR, $path);
		}
		
		/**
		 * Devuelve la parte de la ruta relativa de $parent.
		 * @param string $path
		 * @param string $parent
		 * @return string
		 */
		public static function Relative($path, $parent = "")
		{
			
			$path = OU_Path::Correct($path, array("endSeparator" => false, "check" => false));
			$parent = OU_Path::Correct($parent, array("endSeparator" => true, "check" => false));
			
			$url1 = $path;
			$url2 = $parent;
			
			$from = $parent;
			$to = $path;
			
			
			// some compatibility fixes for Windows paths
			$from = is_dir($from) ? rtrim($from, '\/') . '/' : $from;
			$to   = is_dir($to)   ? rtrim($to, '\/') . '/'   : $to;
			$from = str_replace('\\', '/', $from);
			$to   = str_replace('\\', '/', $to);
			
			$from     = explode('/', $from);
			$to       = explode('/', $to);
			$relPath  = $to;
			
			foreach($from as $depth => $dir) {
				// find first non-matching dir
				if($depth < count($to) && $dir === $to[$depth]) {
					// ignore this directory
					array_shift($relPath);
				} else {
					// get number of remaining dirs to $from
					$remaining = count($from) - $depth;
					if($remaining > 1) {
						// add traversals up to first matching dir
						$padLength = (count($relPath) + $remaining - 1) * -1;
						$relPath = array_pad($relPath, $padLength, '..');
						break;
					} else {
//						$relPath[0] = './' . $relPath[0];
					}
				}
			}
			
			return implode('/', $relPath);
		}
		
		/**
		 * Crea una ruta absolute partiendo de una ruta relativa.
		 * @param string $rel
		 * @param string $abs
		 * @param OU_Options|array $options 
		 * 				- correctOpts : OU_Options | array : Son las opciones que se pasaran a la función OU_Config::Correct()
		 * @return string|boolean string
		 */
		public static function Absolute($rel, $abs = "", $options = array())
		{
			
			$options = OU_Options::FromArray(
				$options,
				array(
					"correctOpts" => array(
						"check" => false
					)
				)
			);
			
			if (self::IsRelative($rel))
				return OU_Path::Correct($abs . DIRECTORY_SEPARATOR . $rel, $options->correctOpts);
			else
				return OU_Path::Correct($rel, $options->correctOpts);
		}
		
		/**
		 * Canvia la extensión de una dirección
		 * @param string $path Dirección
		 * @param string $ext Nueva extensión
		 */
		public static function ChangeExt($path, $ext = "", $full = false)
		{
			$str = str_replace("\\", "/", $path);
			/* ?
			if ($full)
				$pos = strpos($str, "/");
			else
				$pos = strrpos($str, "/");
			*/
			$pos = strrpos($str, "/");
			if ($pos === false)
			{
				$pos = 0;
			}
			
			
			if ($full)
				$pos = strpos($path, ".", $pos);
			else 
				$pos = strrpos($path, ".", $pos);
			
			if ($pos !== false)
				return substr($path, 0, $pos) . $ext;
			else
				return $path . $ext;
		}
		
	}
	
	/**
	 * 
	 * Gestiona un dirección de cualquier tipo, local o URI. Accede o establece cada una de sus partes y detecta el estado en que se encuentra. 
	 * 
	 * Se debería acceder a esta clase mediante OU_Path::Create.
	 * 
	 * @property string $value				Devuelve la ruta original.
	 * @property-read boolean $valueWithDelimiter Devuelve la ruta original con delimitador final.
	 * @property-read boolean $isFile		TRUE si es un archivo
	 * @property-read boolean $isDir		TRUE si es un directorio
	 * @property boolean $isUrl		TRUE si es un directorio
	 * @property-read boolean $exists		TRUE si existe
	 * @property string $ext				Devuelve o establece la extensión del archivo
	 * @property string $fullExt			Devuelve o establece todas las extensiones del archivo.
	 * @property string $parentDirectory	Devuelve o establece la dirección del directorio padre.
	 * @property string $name				Devuelve o establece el nombre del archivo o directorio.
	 * @property-read string $url			Devuelve la URL de este archivo o directorio
	 * @property-read string $breadcrumb	Devuelve un "breadcrumb" en formato HTML.
	 * @property string $query				Devuelve o establece el "query" de la dirección.
	 * @property strnig $hash				Devuelve o establece el "hash" de la dirección.
	 * @property string $path				Devuelve o establece el "path" de la dirección.
	 * @property-read OU_Array $queryVars	Accede a las variables de query.
	 * @property string $protocol			Devuelve el protocolo de una dirección. Si es un archivo local devuelve "file"
	 * @property string $host				Devuelve el host de una url.
	 * @property strnig $port
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_path.php 322 2013-10-24 10:58:11Z xaguilarf $
	 * @example		ou_path/instance.php
	 * @see			OU_Path
	 */
	class OU_Path_Instance extends OU_Base
	{
		
		protected $_value;
		protected $_isUrl;
		
		public function __construct($value, $isURL = false)
		{
			$this->_value = $value;
			$this->_isUrl = $isURL || $this->query || $this->hash; // Te protocol?
		}
		
		private $_tmpValues = array();
		private function _tmp($name, $valFunc)
		{
			if (!isset($this->_tmpValues[$name]))
			{
				$this->_tmpValues[$name] = $valFunc($this);
			}
			return $this->_tmpValues[$name];
		}
		
		private function _tmpClear() { $this->_tmpValues = array(); }
		
		protected function getValue() { return $this->_value; }
		protected function setValue($value) { $this->_value = $value; $this->_tmpClear(); }
		
		protected function getValueWithDelimiter(){ return OU_Path::IncludePathDelimiter($this->_value); }
		
		protected function getIsFile(){ return $this->_tmp("isFile", function($t){ return is_file($t->_value); }); }
		protected function getIsDir(){ return $this->_tmp("isDir", function($t){ return is_dir($t->_value); }); }
		protected function getExists(){ return $this->_tmp("exists", function($t){ return file_exists($t->_value); }); }
		
		protected function getIsUrl(){ return $this->_isUrl; }
		protected function setIsUrl($value){ $this->_isUrl = $value; $this->_tmpClear(); }
		
		protected function getUrl(){
			if ($this->isUrl)
			{
				OU_Config::IncExt("compatible");
				return http_build_url($this->_value);
			}
			else 
				return $this->_tmp("url", function($t){ return OU_Path::Path2Url($t->_value); }); 
		}
		protected function getBreadcrumb(){ 
			return $this->_tmp("breadcrumb", function($t){
				
				$a = explode($t->isUrl ? "/" : DIRECTORY_SEPARATOR, $t->path);
				$a2 = array();
					
				$p = array();
				
				$path = $t->CloneObject();
				
				$count = count($a);
				$cnt = 0;
					
				foreach ($a as $v)
				{
					$cnt++;
					$p[] = $v;
					$path->value = implode($t->isUrl ? "/" : DIRECTORY_SEPARATOR, $p);
					$url = $path->url;
					
					if ($cnt == $count)
					{
						$x = 
							(($q = $t->query) ? "?" . $q : "") . 
							(($h = $t->hash) ? "#" . $h : "");
						$v .= $x;
						if ($url !== false) $url .= $x; 
					}
					
					if ($p && $p != "" && $url !== false)
						$a2[$v] = $url;
					else
						$a2[] = $v;
					
				}
					
				return OU_Path::BreadcrumbA($a2);
				
			}); 
		}
		
		protected function getExt() { return OU_Path::Ext($this->path, false); }
		protected function setExt($value) {
			$this->_value = 
				OU_Path::ChangeExt($this->path, $value, false) . 
				(($q = $this->query) ? "?" . $q : "") . 
				(($h = $this->hash) ? "#" . $h : ""); 
			$this->_tmpClear();
		}
		
		protected function getFullExt() { return OU_Path::Ext($this->path, true); }
		protected function setFullExt($value) { 
			$this->_value = 
				OU_Path::ChangeExt($this->_value, $value, true) .
				(($q = $this->query) ? "?" . $q : "") . 
				(($h = $this->hash) ? "#" . $h : ""); 
			$this->_tmpClear();
		}
		
		protected function getName() { return basename($this->path); }
		protected function setName($value) { 
			$this->_value = 
				OU_Path::IncludePathDelimiter($this->parentDirectory) . $value . 
				(($q = $this->query) ? "?" . $q : "") . 
				(($h = $this->hash) ? "#" . $h : ""); 
			$this->_tmpClear(); 
		}
		
		protected function getParentDirectory() { 
			$p = $this->CloneObject(); 
			$p->value = OU_Path::IncludePathDelimiter(dirname($this->_value)); 
			return $p->Correct(); 
		}
		protected function setParentDirectory($value) { 
			$this->_value = OU_Path::IncludePathDelimiter($value) . $this->name . 
				(($q = $this->query) ? "?" . $q : "") . 
				(($h = $this->hash) ? "#" . $h : ""); 
			$this->_tmpClear(); 
		}
		
		protected function getProtocol(){
			
			if ($this->isUrl)
			{
				$info = parse_url($this->_value);
				if ($info && isset($info["scheme"]))
				{
					return $info["scheme"];
				}else{
					return "http";
				}
			}else{
				return "file";
			}
		}
		protected function setProtocol($v){
			if ($this->isUrl)
			{
				$info = parse_url($this->_value);
				OU_Config::IncExt("compatible");
				$this->_value = http_build_url(
					$this->_value,
					array(
						"scheme" => $v
					)	
				);
				$this->_tmpClear();
			}
		}
		
		protected function getHost()
		{
			if ($this->isUrl)
			{
				$info = parse_url($this->_value);
				if ($info && isset($info["host"]))
				{
					return $info["host"];
				}else{
					return null;
				}
			}else{
				return null;
			}
		}
		protected function setHost($v){
			if ($this->isUrl)
			{
				$info = parse_url($this->_value);
				OU_Config::IncExt("compatible");
				$this->_value = http_build_url(
					$this->_value,
					array(
						"host" => $v
					)
				);
				$this->_tmpClear();
			}
		}
		
		protected function getPort()
		{
			if ($this->isUrl)
			{
				$info = parse_url($this->_value);
				if ($info && isset($info["port"]))
				{
					return $info["port"];
				}else{
					return "80";
				}
			}else{
				return null;
			}
		}
		protected function setPort($v){
			if ($this->isUrl)
			{
				$info = parse_url($this->_value);
				OU_Config::IncExt("compatible");
				$this->_value = http_build_url(
					$this->_value,
					array(
						"port" => $v
					)
				);
				$this->_tmpClear();
			}
		}
		
		/**
		 * @return string
		 */
		protected function getQuery() { 
			
			$num2 = strpos($this->_value, "#");
			if ($num2 === FALSE) $num2 = strlen($this->_value);
			
			if (($num = strpos($this->_value, "?")) !== false)
			{
				if ($num < $num2)
					return substr($this->_value, $num + 1, $num2 - ($num + 1));
				else
					return "";
			}
			else
				return ""; 
		}
		
		/**
		 * @param string $value
		 */
		protected function setQuery($value) { 
			
			if ($this->isUrl)
			{
				OU_Config::IncExt("compatible");
				$this->_value = http_build_url($this->_value, array("query" => $value));
				$this->_tmpClear();
			}else{
				$p = $this->path;
				if ($value) $p .= "?" . $value;
				if ($h = $this->hash) $p .= "#" . $h;
				if ($p !== $this->_value)
				{
					$this->_value = $p;
					$this->_tmpClear();
				}
			}
		}
		
		/**
		 * @return string
		 */
		public function getHash()
		{
			if (($num = strpos($this->_value, "#")) !== false)
				return substr($this->_value, $num + 1);
			else
				return "";
		}
		
		/**
		 * @param string $value
		 */
		public function setHash($value)
		{
			if ($this->isUrl)
			{
				OU_Config::IncExt("compatible");
				$this->_value = http_build_url($this->_value, array("fragment" => $value));
				$this->_tmpClear();
			}else{
				$p = $this->path;
				
				// Query
				if ($q = $this->query) $p .= "?" . $q;
				// Hash
				if ($value) $p .= "#" . $value;
				
				if ($p !== $this->_value)
				{
					$this->_value = $p;
					$this->_tmpClear();
				}
			}
		}
		
		/**
		 * @return string
		 */
		protected function getPath() {

			$v = $this->_value;
			
			if ($this->isUrl)
			{
				$url = parse_url($this->_value);
				if ($url && isset($url["path"]))
					$v = $url["path"]; 
			}
			
			$num1 = strpos($v, "#");
			$num2 = strpos($v, "?");
			
			$num = false;
			if ($num1 && $num2)
			{
				if ($num2 < $num1)
					$num = $num2;
				else
					$num = $num1;
			}else if ($num1){
				$num = $num1;				
			}else if ($num2){
				$num = $num2;
			}
			
			if ($num !== false)
				$p = substr($v, 0, $num);
			else
				$p = $v;
			return $p;
		}
		
		/**
		 * @param string $value
		 */
		protected function setPath($value){
			
			$num1 = strpos($this->_value, "#");
			$num2 = strpos($this->_value, "?");
				
			$num = false;
			if ($num1 && $num2)
			{
				if ($num2 < $num1)
					$num = $num2;
				else
					$num = $num1;
			}else if ($num1){
				$num = $num1;
			}else if ($num2){
				$num = $num2;
			}
			
			if ($num !== false)
				$p = $value . substr($this->_value, $num);
			else
				$p = $value;
			$this->_value = $p;
			$this->_tmpClear();
		}
		
		protected function getQueryVars() {
			parse_str($this->query, $out);
			$a = OU_Array::FromArray($out);
			$a->on("change", function($self, $t){
				$t->query = http_build_query($self->toArray());;
			}, $this);
			return $a;
		}
		
		/**
		 * Comprueba si esta direccion esta dentro de otra
		 * @param string|OU_Path_Instance $path
		 */
		public function in($path)
		{
			$p1 = $this->Correct();
			if (($path instanceof OU_Path_Instance))
			{
				$p2 = $this->CloneObject();
				$p2->value = $path->value;
			}
			else
			{
				$p2 = $this->CloneObject();
				$p2->value = $path;
			}
			
//			$p2 = OU_Path::IncludePathDelimiter($p2->Correct());
			$p2 = $p2->Correct();
			
			return (strpos($p1, $p2) === 0 && strlen($p1) > strlen($p2));
			
		}
		
		/**
		 * @see OU_Path::Relative()
		 * @param string|OU_Path_Instance $path
		 * @return OU_Path_Instance
		 */
		public function relative($path)
		{
			if ($path instanceof OU_Path_Instance) $path = $path->Correct();
			return OU_Path::Create(OU_Path::Relative($this->_value, $path), $this->isUrl);
		}
		
		/**
		 * @see OU_Path::Absolute()
		 * @param string|OU_Path_Instance $path
		 * @return OU_Path_Instance
		 */
		public function absolute($path, $options = array())
		{
			if ($path instanceof OU_Path_Instance) $path = $path->Correct();
			return OU_Path::Create(OU_Path::Absolute($this->_value, $path, $options), $this->isUrl);
		}
		
		/**
		 * Comprueba si contiene un suffijo.
		 * @param string|array $suffix
		 */
		public function hasSuffix($suffix)
		{
			if (is_array($suffix))
			{
				$or = false;
				foreach ($suffix as $s)
					$or = $or || $this->hasSuffix($s);
				return $or;
			}
			$str = $this->Correct();
			return (strrpos($this->Correct(), $suffix) - strlen($str) + strlen($suffix))  == 0;
		}
		
		/**
		 * Devuelve las extensiones de una archivo.
		 * <code>
		 * 	echo OU_Path::Create('my_file.hello.world.txt')->getExtension(2);
		 *  // .world.txt
		 * </code>
		 * @param number $num Numero de extensiones a devolver. 
		 * @return string
		 */
		public function getExtension($num = 1)
		{
			
			$path = $this->Correct();
			
			$pos = strrpos(str_replace("\\", "/", $path), "/");
			if ($pos === false)
			{
				$pos = 0;
			}
			
			$sub_path = substr($path, $pos + 1);
			// Exclude hash
			$pos = strpos($sub_path, "#");
			if ($pos !== false) $sub_path = substr($sub_path, 0, $pos);
			$a = explode(".", $sub_path);
			
			if (count($a) > 0)
			{
 				unset($a[0]); // Borra el nom
				if ($num < count($a))
				{
					$count = count($a) - $num;
					for ($i = 0; $i < $count; $i++)
					{
						unset($a[$i + 1]);
					}
				}				
				return "." . implode(".", $a);
			}
			return "";
		}
		
		/**
		 * @see OU_Path::Correct()
		 * @return string|FALSE
		 */
		public function Correct(){
			$r = OU_Path::Correct(
				$this->path,
				array(
                    "check" => false,
                    "endSeparator" => false,
                    "separator" => $this->isUrl ? "/" : DIRECTORY_SEPARATOR
				)
			);

			if ($r)
			{
				if ($this->isUrl)
				{
					$r = $r .
						(($q = $this->query) ? "?" . $q : "") .
						(($h = $this->hash) ? "#" . $h : "");
						
				}else if ($this->isDir)
				{
					$r = OU_Path::IncludePathDelimiter($r);
				}
			}
			return $r;
		}
		
		public function __toString()
		{
			return $this->Correct();
		}		
		
	}

?>