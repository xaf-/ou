<?php

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 29/06/2012
	 */

	OU_Config::IncClass("OU_Base");
	
	OU_Config::IncClassHelper(
		array(
			"OU_Debug_PrintR",
			"OU_Debug_Object",
			"OU_Debug_Class",
			"OU_Debug_Colorize"
		)
	);
	 
	/**
	 * Agrupa un listado de funciones de pruebas en desarrollo.
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_debug.php 265 2013-07-12 01:08:34Z xaguilarf $
	 */
	class OU_Debug extends OU_Base
	{
		
		/**
		 * Convierte un código a formato HTML coloreado.
		 * @param string $code Código de convertir.
		 * @param string $type Lenguage de Geshi compatible
		 * @param OU_Options|array $options
		 * @return string
		 */
		public static function syntax($code, $type="php", $options = array())
		{
			return OU_Debug_Colorize::syntax($code, $type, $options);
		}
		
		/**
		 * Converte el contenido de un código a formato HTML coloreado.
		 * @param string $fileName Dirección del archivo
		 * @return string|false
		 */
		public static function syntaxFromFile($fileName, $options = array())
		{
			return OU_Debug_Colorize::fromFile($fileName, $options);
		}
		
		private static $_tmpClasses = array();
		/**
		 * Obtiene la información de una referencia.
		 * @see OU_Debug_Object
		 * @param object $object
		 * @return OU_Debug_Object|OU_Debug_Class|OU_Debug_Object
		 */
		public static function reference($object)
		{
			
			$res = null;
			if (is_string($object)) {
				if (!isset(self::$_tmpClasses[$object]))
					self::$_tmpClasses[$object] = new OU_Debug_Class(new ReflectionClass($object));
				$res = self::$_tmpClasses[$object];
			}else
			if (is_object($object)) $res = new OU_Debug_Object(new ReflectionObject($object), $object);
			
			return $res;
		}
		
		/**
		 * Parecido a la función nativa print_r() pero con la información amplificada y en formato HTML coloreado.
		 * @see OU_Debug_PrintR
		 * @return string
		 */
		public static function print_r($obj, $options = array(), $count = 0)
		{
			return OU_Debug_PrintR::print_r($obj, $options, $count);
		}
		
		
		private static $_hookDebug = false;
		private static $_hookStartTime = 0;
		private static $_hookStartMemory = 0;
		/**
		 * Indica que se debe empezar a recabar información.
		 */
		public static function start()
		{
			if (OU_Config::$development)
			{
				self::$_hookStartTime = microtime(true);
				self::$_hookStartMemory = memory_get_usage();
				self::$_hookDebug = true;
			}
		}
		/**
		 * Indica que se debe finalizar el recabar información.
		 */
		public static function end()
		{
			self::$_hookDebug = false;
		}
		
		public static $points = array();
		/**
		 * Establece puntos de control. 
		 */
		public static function point($msg = null)
		{
			if (self::$_hookDebug)
			{
				$debug = debug_backtrace();
				self::$points[] = array(
					"msg" => $msg,
					"debug" => $debug,
					"time" => microtime(true),
					"memory" => memory_get_usage(),
					"args" => func_get_args()				
				);
			}
		}
		
		public static function pointInfo($options = array())
		{
			
			if (!OU_Config::$development) return;
			
			if (count(OU_Debug::$points) == 0) return;
			
			OU_Config::IncClassHelper(
				"OU_Utils_Format"
			);
			
			$options = OU_Options::FromArray(
				$options,
				array(
					"max_memory_display" => 1024
				)
			);
			
			$f_time = self::$_hookStartTime;
			$f_memory = self::$_hookStartMemory;
			
			// Borra la memoria dels debug
			$cnt = 0;
			foreach (OU_Debug::$points as &$point)
			{
				$memory_start = memory_get_usage();
				$copy_point = $point;
				$memory_size = memory_get_usage() - $memory_start;
				unset($copy_point);
				
				$cnt += $memory_size;
				
				$point["memory"] -= $cnt;				
			}
			
			$max = array(
				"memory" => 0,
				"time" => 0
			);
			$l_point = null;
			foreach (OU_Debug::$points as $k => $point)
			{
				$max["memory"] = max($max["memory"], $l_point == null ? 0 : $point["memory"] - $l_point["memory"]);
				$max["time"] = max($max["time"], $l_point == null ? 0 : $point["time"] - $l_point["time"]);
				$l_point = $point;
			}
			
			
			$fncIcon = function($value)
			{
				if ($value > 0)
				{
					return '<img width=8 height=8 src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAYAAABWdVznAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAANtJREFUeNpi/P//PwMpgAVE7NyxDkXw1BthsClmIm8ZkcXdPYIgGtAVG6pKQdi3Gf6ja2JBV6yrJMFgqa0A5v/5+4/h1D1UTSzIirUUxBn0VGQZXn/+CRYDsf/++89w6gFCEwtMsbaiOIOushzD+69/GH7++QfWwM7CBNQkx8AIVHrqPsN/dwYGRrgNV++/BGM+Pn4Gdh4hsNjPL+8YPn36iBlKQOsaEU5jqFcVFwOzbz/7iCKH7IcGJLF6UV52iAZMOcxgBQFJfnb8EYcO1u4/S7wGdDejA4AAAwDNjlDiCqMxuwAAAABJRU5ErkJggg==" /> + ';
				}else if ($value < 0)
				{
					return '<img width=8 height=8 src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAYAAABWdVznAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAANVJREFUeNpi/P//PwMpgAVE7NyxDi5w6o0wiglmIm8ZYWx3jyAGJmymVEbagTFOG9DB559/8DsJHbz9/Au/BmR3y0uKMLz69BvOPvWcAS7nzsDACLdBVUaMQUxMjOHZhx8MFx59BIvxcQowWBtJMLx69Yrh9pNXYDEmaEg0ggQePn3OwMXGzMAJxSA2SAwkB1KD7IcGoADDqZcM9f/+MTCIi4uBBV++fMXw7PVbmOIGdE9DNL1mqGdjhQQ9umJsoQTR9IyhHuZUZMW4ghWsCcZGlwQIMADf6VTY/KQw+gAAAABJRU5ErkJggg==" /> - ';
				}else return '';
			};
			
			echo '
			<style>
				.OU_TableDebugPointInfo
				{
					border: 1px solid silver;
					margin: 0 auto;
				}
				.OU_TableDebugPointInfo
				{
					font-family: Segoe UI, Arial;
					font-size: 12px !important;
				}
				.OU_TableDebugPointInfo td, .OU_TableDebugPointInfo th
				{
					font-weight: normal;
					color: black;
					text-align: left;
					padding: 5px;
					border: 1px solid silver;
				}
				.OU_TableDebugPointInfo th
				{
					font-weight: bold;
				}
			</style>
			';
						
			echo '<table border=1 class=OU_TableDebugPointInfo>';
			echo '<tr>';
				echo '<td colspan="4">';
					$p = OU_Array::FromArray(OU_Debug::$points);
					echo "Total time : " . OU_Utils_Format::seconds($p->Last()->time - $p->First()->time) . "s<br/>\n";
					echo "Total memory: " . OU_Utils_Format::bytes($p->Last()->memory - $p->First()->memory)  . "<br/>\n";
				echo '</td>';
			echo '</tr>';
			echo '<tr>';
				echo '<th>Stack</th>';
				echo '<th>Memory</th>';
				echo '<th>Time</th>';
				echo '<th>Params</th>';
			echo '</tr>';
			$id = 0;
			
			$l_point = null;
			
			foreach (OU_Debug::$points as &$point)
			{
				$id++;
				
				echo '<tr>';
				
					// Stack
				
					echo '<td>';
						echo $point["debug"][0]["file"].(isset($point["debug"][0]["line"]) ? ":".$point["debug"][0]["line"] : "");
						echo '<div id="OU_TableDebugPointInfo_Stack_More_'.$id.'" style="display:none">';
						$frst = true;
						foreach ($point["debug"] as $d)
						{
							if ($frst) { $frst = false; continue; }
							echo "<div>".(isset($d["file"]) ? $d["file"] : "").(isset($d["line"]) ? ":$d[line]" : "")."</div>";
						}
						echo '</div>';
						if (count($point["debug"]) > 0)
							echo '<a style="font-size:80%;display:block" id="OU_TableDebugPointInfo_Stack_ViewMore_'.$id.'" href="javascript:document.getElementById(\'OU_TableDebugPointInfo_Stack_More_'.$id.'\').style.display=\'block\';document.getElementById(\'OU_TableDebugPointInfo_Stack_ViewMore_'.$id.'\').style.display=\'none\';">View more...</a>';
					echo '</td>';
						
					// Memory
						
					$hasDif = $l_point && round($point["memory"], 2) != round($l_point["memory"],2);
					$dif = $point["memory"] - $l_point["memory"];
					if ($hasDif) {
						$c = $max["memory"] / 2;
						if ($dif > $c)
							$color = "rgba(255, 128, 128, ".number_format(0.5 * ($dif - $c) / $c, 2, ".", "").")";
						else
							$color = "rgba(128, 255, 128, ".number_format(0.25 - (0.25 * $dif / $c), 2, ".", "").")";
					} else $color = false;
						
					echo '<td style="text-align:right; '.($color ? 'background-color: '.$color : '').'">';
						echo OU_Utils_Format::bytes($point["memory"] - $f_memory);
						if ($hasDif) echo '<br />' . $fncIcon($dif) . OU_Utils_Format::bytes(abs($dif));
					echo '</td>';
						
					// Time
						
					$hasDif = $l_point && round($point["time"], 2) != round($l_point["time"],2);
					$dif = $point["time"] - $l_point["time"];
					if ($hasDif) {
						$c = $max["time"] / 2;
						if ($dif > $c)
							$color = "rgba(255, 128, 128, ".number_format(0.5 * ($dif - $c) / $c, 2, ".", "").")";
						else
							$color = "rgba(128, 255, 128, ".number_format(0.25 - (0.25 * $dif / $c), 2, ".", "").")";
					} else $color = false;
						
					echo '<td style="text-align:center; '.($color ? 'background-color: '.$color : '').'">';
						echo OU_Utils_Format::seconds($point["time"] - $f_time);
						if ($hasDif) 
							echo '<br />' . $fncIcon($dif) . OU_Utils_Format::seconds(abs($dif));
					echo '</td>';
						
					// Args
						
					echo '<td>';
						echo '<div>';
							echo OU_Debug::print_r($point["args"]);
						echo '</div>';
					echo '</td>';
						
				echo '</tr>';
				$l_point = $point;
			}
			echo '</table>';
				
		}
		
	}
	
?>