
	<ou:register name="minifier"><?php
	   OU_Config::IncClassHelper("OU_Utils_Format");
	   if (isset($this->force))
	   {
	       $this->content = str_replace("\t", " ", $this->content);
	       while (strpos($this->content, "  ") !== false)
	           $this->content = str_replace("  ", " ", $this->content);
	   }
	   echo OU_Config::$development && (!isset($this->enabled) || !$this->enabled) ? $this->content : OU_Utils_Format::minify($this->content);
	?></ou:register>