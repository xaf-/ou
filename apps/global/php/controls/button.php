
    <ou:include control="minifier" />
    
    <ou:register name="button"><ou:minifier enabled=true force=true>
    
        <?php
            if (isset($this->autofocus) && !$this->autofocus) unset($this->autofocus); 
            if (isset($this->disabled)) 
                if ($this->disabled)
                    $this->disabled = "disabled";
                else
                    unset($this->disabled);
        ?>
    
		<button 
		      ou:param="button |class|"
		      ou:param="style"
		      ou:param="name"
		      ou:param="type"
		      ou:param="id"
		      ou:param="onclick"
		      ou:param="autofocus"
		      ou:param="disabled"
		      ou:param="form"
		      ou:param="formaction"
		      ou:param="formenctype"
		      ou:param="formmethod"
		      ou:param="formnovalidate"
		      ou:param="formtarget"
		      >
		      
			<ou:content />
			
		</button>
		
	</ou:minifier></ou:register>