
    <ou:register name="var"><?php

        $name = $this->name;
        $scope = $this->scope;
        $t = $this;

        $def = function($n, $v) use ($t){
            if (isset($t->{$n}))
                return $t->{$n};
            else
                return $v;
        };

        $default = $def("default", null);
        $value = $def("value", $default);
        $require = $def("require", false);
        if ($require && isset($this->default)) $require = false;

        $isset = isset($scope->{$name});

        if ($require){
            throw new Exception("Se requiere el campo '$name'");
        }

        if (isset($this->value)){
            $scope->{$name} = $value;
        }else
            if (!$isset){
                $scope->{$name} = $value;
            }

    ?></ou:register>