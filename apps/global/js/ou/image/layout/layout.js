
	if (typeof(OU) === "undefined") OU = {};
	
	if (typeof(OU.image) === "undefined") OU.image = {};

	if (typeof(OU.image.layout) === "undefined") OU.image.layout = {};
	
	OU.image.layout.inherits = function (childCtor, parentCtor) {
		/** @constructor */
		function tempCtor() {};
		tempCtor.prototype = parentCtor.prototype;
		childCtor.superClass_ = parentCtor.prototype;
		childCtor.prototype = new tempCtor();
		/** @override */
		childCtor.prototype.constructor = childCtor;
	};
	
	/**
	 * @import "classes/classes.js"
	 */
