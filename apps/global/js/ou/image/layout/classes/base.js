
	/**
	 * @constructor
	 */
	OU.image.layout.classes.base = (function(){
		
		var base = function(images, options){
			
			this.images = images;
			
			this.options = {
				areaWidth: window.innerWidth,
				margin: 5
			};
			
			if (options)
				$.extend(this.options, options);
			
			this.run = function(){ };
			
		};
		
		return base;
		
	}());