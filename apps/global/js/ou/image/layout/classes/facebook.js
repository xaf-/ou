
	/**
	 * @constructor
	 * @base {OU.image.layout.classes.base}
	 */
	OU.image.layout.classes.facebook = (function(){
		
		var facebook = function(images, options){
			
			var opt = {
				columnWidth: 200
			};
			
			if (options)
				$.extend(opt, options);
			
			OU.image.layout.classes.base.call(this, images, opt);
			
			this.run = function(){ 
				
				var size = this.options.areaWidth;
				var n_columns = Math.floor(size
						/ (2 * (this.options.columnWidth + this.options.margin)));
	
				var HEIGHTS = [];
				for ( var i = 0; i < n_columns; ++i) {
					HEIGHTS.push(0);
				}
				
				function get_min_column() {
					var min_height = Infinity;
					var min_i = -1;
					for ( var i = 0; i < HEIGHTS.length; ++i) {
						if (HEIGHTS[i] < min_height) {
							min_height = HEIGHTS[i];
							min_i = i;
						}
					}
					return min_i;
				};
				
				function add_column_elem(i, elem, is_big) {
					$(elem).css(
						{
							'margin-left' : MARGIN + (COLUMN_WIDTH + MARGIN) * i,
							'margin-top' : HEIGHTS[Math.floor(i / 2)] * (COLUMN_WIDTH + MARGIN),
							'width' : is_big ? COLUMN_WIDTH * 2 + MARGIN : COLUMN_WIDTH,
							'height' : is_big ? COLUMN_WIDTH * 2 + MARGIN : COLUMN_WIDTH
						}
					);
					$(elem).css('background-image', $(elem).css('background-image').replace(/w[0-9]+-h[0-9]+/, 'w' + $(elem).width() + '-h' + $(elem).height()));
				};
	
				var images = this.images;
				var small_images = [];
				for ( var i = 0; i < images.length; ++i) {
					var image = images[i];
					var column = get_min_column();
					if (Math.random() > 0.8) { // is big?
						add_column_elem(column * 2, image, true);
						HEIGHTS[column] += 2;
					} else {
						small_images.push(image);
						if (small_images.length === 2) {
							add_column_elem(column * 2, small_images[0], false);
							add_column_elem(column * 2 + 1, small_images[1], false);
							HEIGHTS[column] += 1;
							small_images = [];
						}
					}
				}
				if (small_images.length) {
					column = get_min_column();
					add_column_elem(column * 2, small_images[0], false);
				}
					
			};
			
		};
		
		return facebook;
		
	}());
	
	OU.image.layout.inherit(OU.image.layout.classes.facebook, OU.image.layout.classes.base);