
	/**
	 * @import "metro/accordion.js"; 
	 * @import "metro/buttonset.js"; 
	 * @import "metro/carousel.js"; 
	 * @import "metro/dropdown.js"; 
	 * @import "metro/pagecontrol.js"; 
	 * @import "metro/rating.js"; 
	 * @import "metro/slider.js"; 
	 * @import "metro/tile-slider.js"; 
	 */


	$(document).ready(function(){
		
		var _showElement = function(t, show)
		{
			
			if (!$(t).hasClass("method") && !$(t).hasClass("property"))
			{
				return;
			}
			
			if (show == null) {
				$(t).find(".toggle").slideToggle();
			}else if (show) {
				$(t).find(".toggle").slideDown();
			}else{
				$(t).find(".toggle").slideUp();
			}
			
		};
		
		var _updateHash = function()
		{
			var hash = window.document.location.hash;
			if (hash && hash.length > 0)
			{
				if (hash[0] === "#")
				{
					hash = hash.substr(1);
				}
				var a = $("a[name='"+hash+"']").next();
				a.slideDown();
				_showElement(a, true);
			}
		};
		
		window.onhashchange = function()
		{
			_updateHash();
		};
		
		$(".method, .property").click(function(e){
			
			if (e.srcElement && e.srcElement.tagName.toLowerCase() == "a") return;
			_showElement(this);
			
		});
		
		$(".access_select .access input").change(function(){
			
			var t = $(this).attr("data-type");
			var o = $(".property, .method, .item_property, .item_method");
			
			if (this.checked)
				o.filter("." + t).slideDown();
			else
				o.filter("." + t).slideUp();
			
		});
		
		_updateHash();
		
	});