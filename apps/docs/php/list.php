
	<ou:template_default>

		<ou:part name="title">
			asd
		</ou:part>

		<div>
		
			
		  	<table style="width: 100%; table-layout: fixed">
		  		<colgroup>
		  			<col width="30%" />
		  		</colgroup>
		  		<thead>
		  			<tr>
		  				<th>Nombre</th>
		  				<th></th>
		  			</tr>
		  		</thead>
		  		<tbody>
		  		<?php foreach (get_declared_classes() as $className) { ?>
		  			<?php
		  				$class = OU_Debug::reference($className); 
		  			?>
			  		<tr>
			  			<td class="trim"><ou:type><?=$className?></ou:type></td>
			  			<td class="trim"><ou:desc><?=$class->description()?></ou:desc></td>
			  		</tr>
			  	<?php } ?>
			  	</tbody>
		  		</table>
			
		</div>
	
	</ou:template_default>