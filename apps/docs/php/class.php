<ou:template_sidebar>

	<?php
		$class = $this->class; 
	?>

	<ou:part name="messages">
		<ou:test tag="start" />
		<noscript>
			Debe habilitar JavaScript en su navegador para ver correctamente este sitio.
	    </noscript>
	</ou:part>
	
	<ou:part name="menu">
		<ul class="place-right">
	    	<ou:menuitem text=$class->name() isRight="true">
	    		<ou:menuitem href="#syntax" text="Sintaxis" />
	    		<ou:menuitem href="#constructor" text="Constructor" />
	    		<?php if (count($class->props()) > 0) { ?>
	    		<ou:menuitem href="#properties" text="Propiedades" />
	    		<?php } ?>
	    		<?php

	    			$methods1 = $class->methods();
	    			$methods2 = $class->methods(ReflectionMethod::IS_STATIC);
	    		
	    		?>
	    		<?php if (count($methods1) - count($methods2) > 0) { ?>
	    		<ou:menuitem href="#methods" text="Métodos" />
	    		<?php } ?>
	    		<?php if (count($methods2) > 0) { ?>
	    		<ou:menuitem href="#methods_s" text="Métodos estáticos" />
	    		<?php } ?>
	    	</ou:menuitem>
	    </ul>
	</ou:part>
	
	<ou:part name="title">
		<h1><?=$class->name()?><small></small></h1>
	</ou:part>
	
	<ou:part name="sidebar">
 		<ul>
            <li><a class="trim" href="#"><?=$class->name()?></a></li>
            <ou:menu_properties class=$class />
            <ou:menu_methods class=$class />
            <ou:menu_methods class=$class static=true />
        </ul>
	</ou:part>
		
	<div>
	
		<p><ou:desc><?=$class->description()?></ou:desc></p>		
		
		<a name="inheritance"></a>
		<ou:class_inheritance class=$class tmp=$this->tmp />
		
		<a name="syntax"></a>
		<ou:class_syntax class=$class />
		
		<a name="example"></a>
		<ou:class_example class=$class />
		
		<a name="constructor"></a>
		<div style="position: relative">
		
			<ul class="access_select" style="position: absolute; right: 0; top: 4px">
				<li class=""><span class="access access_private"><input type="checkbox" data-type="access_private" /></span> private</li>
				<li class=""><span class="access access_protected"><input type="checkbox" data-type="access_protected" /></span> protected</li>
				<li class=""><span class="access access_public"><input checked="checked" type="checkbox" data-type="access_public" /></span> public</li>
				<li class=""><span class="access access_magic"><input type="checkbox" data-type="access_magic" /></span> magic</li>
			</ul>
			
			<ou:class_constructor class=$class />
			
		</div>
		
		<a name="properties"></a>
		<ou:class_properties class=$class />
		
		<a name="methods"></a>
		<ou:class_methods class=$class static=false />
			
		<a name="methods_s"></a>
		<ou:class_methods class=$class static=true />
	
	</div>
	
	<ou:test tag="end" />
		
</ou:template_sidebar>