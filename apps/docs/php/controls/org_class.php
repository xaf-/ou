
	<ou:register name="org_class">
	
		<?php
			$class = $this->class;
			$tmp = $this->tmp;
		?>

		<script type='text/javascript' src='https://www.google.com/jsapi'></script>
	    <script type='text/javascript'>
	      google.load('visualization', '1', { packages:['orgchart']});
	      google.setOnLoadCallback(drawChart);
	      function drawChart() { 
	        var data = new google.visualization.DataTable();
	        data.addColumn('string', 'Name');
	        data.addColumn('string', 'Extends');
	        data.addColumn('string', 'ToolTip');
	        var a = [
	          ['<?=$class->name()?>', '', '']
	          <?php
	          	$cnt = 0;
	          	foreach ($tmp->extend[$class->name()] as $name) {
					$cnt++;
	          		if ($cnt <= 8) { 
	          		if ($cnt == 8) { 
				?>
	          
	         	 ,['Más... (<?=count($tmp->extend[$class->name()]) - $cnt + 1?>)', '<?=$class->name()?>', '']
	          
	          	<?php }else{ ?>

		          ,['<?=$name?>', '<?=$class->name()?>', '']

		          	<?php if (isset($tmp->extend[$name])){ ?>
		          		<?php
		          		$cnt2 = 0;
		          		foreach ($tmp->extend[$name] as $name2) {
							$cnt2++;
							if ($cnt2 <= 2) {
								if ($cnt2 == 2) { 
						?>
			            
		          		,['Más... (<?=count($tmp->extend[$name]) - $cnt2 + 1?>)', '<?=$name?>', '']
		          	
			          	<?php }else{ ?>

		          		,['<?=$name2?>', '<?=$name?>', '']
			          	
			          	<?php } ?>
			          	<?php } ?>
			          	
		          	<?php } ?>
		          	<?php } ?>
		          	
	          	<?php } ?>
	          	<?php } ?>
	          	
          	<?php } ?>
	          
	        ];
	        data.addRows(a);
	        var chart = new google.visualization.OrgChart(document.getElementById('org_chart'));
	        google.visualization.events.addListener(chart, 'select', function(e){
		        var s = chart.getSelection();
		        if (s && s.length > 0)
		        {
			        var n = a[s[0].row][0];
			        if (n.indexOf("Más") > -1) return;
			        var u = "{ClassController::uri('%class_name%')}";
			        document.location.href = u.replace("%class_name%", n);
		        }
            });
	        chart.draw(data, { allowHtml:true, nodeClass : 'org_chart_item', selectedNodeClass : 'org_chart_item_selected' });
	      }
	    </script>
	    <div id="org_chart"></div>
	    
	
	</ou:register>