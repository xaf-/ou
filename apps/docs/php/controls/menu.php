
	<ou:register name="menu">
	
		<?php
			global $_CONFIG; 
		?>
	
		<div class="navigation-bar">
	        <div class="navigation-bar-inner">
	        
	        	<span class="menu-pull"></span>
	        
	            <div class="brand">
		            <a href="/"><span class="metro-ui-logo place-left"></span></a>
		            <a href="/"><span class="name"><sup class="fg-color-yellow tertiary-info-secondary-text"> v <?=$_CONFIG["VERSION"]?></sup></span></a>
		        </div>
		        
		        <ou:content />
	 
	            <ul>
	            
	            	<ou:menuitem text="Clases">
	            		<?php $tmp = $this->controller->tmp(); ?>
	            		<?php foreach ($tmp->base->toArray() as $kname=>$name){ ?>
	            			<ou:menuitem href=ClassController::uri($kname) text=$kname />
	            		<?php } ?>
	            	</ou:menuitem>
	            	
	            	<ou:menuitem text="Item 2">
	            	
	            		<ou:menuitem text="SubItem" />
	            		<ou:menuitem text="SubItem" />
	            		
	            	</ou:menuitem>
	            </ul>
	        </div>
	    </div>
	
	</ou:register>
	