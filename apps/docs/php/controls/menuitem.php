
	<ou:register name="menuitem">
	
		<?php
			$hasSub = isset($this->content) && $this->content; 
		?>
		
		<li <?php if ($hasSub){ ?>data-role="dropdown"<?php } ?> class="sub-menu"{/if}>
			<?php if (isset($this->href)) { ?>
				<a href="<?=$this->href?>"><?=$this->text?></a> 
			<?php }else{ ?>
				<a><?=$this->text?></a>
			<?php } ?>
			<?php if ($hasSub){ ?>
			<ul class="dropdown-menu <?php if (isset($this->isRight) && $this->isRight){?>place-right<?php } ?>">
				<ou:content />
			</ul>
			<?php } ?>
		</li>
	
	</ou:register>
