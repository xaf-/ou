
	<ou:register name="method">
		
		<?php
			$method = $this->method; 
		?>
	
		<a name="m:<?=$method->name()?>"></a>
	
		<div class="access_<?=$method->access()?> <?php if ($method->isMagic()) { ?>access_magic<?php } ?> method <?php if ($method->isDeprecated()){ ?>deprecated <?php } ?> <?php if (!$method->isOwner()){ ?>inherited<?php } ?>">
			<h4><?=$method->syntax()?></h4>
			<div>
				<ou:desc><?=$method->description()?></ou:desc>
			</div>
			<ou:see data=$method />
			<div class="toggle params">
				<?php foreach ($method->params() as $param) { ?>
					<h5>$<?=$param->name()?> : <ou:type><?=$param->type("unknown")?></ou:type> <?php if ($param->hasDefaultValue()){?>(Defecto : <?php var_export($param->defaultValue()) ?>)<?php } ?></h5>
					<div><ou:desc><?=$param->description()?></ou:desc></div>
				<?php } ?>
				<?php if ($method->result()) { ?>
					<h5>return : <ou:type><?=$method->result()?></ou:type></h5>
					<div><ou:desc><?=$method->resultDesc()?></ou:desc></div>
				<?php } ?>
			</div>
			<ou:test tag="method" />
		</div>
		
	</ou:register>