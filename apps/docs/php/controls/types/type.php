
	<ou:register name="type">
	
		<?php
		
			if (isset($this->class)) $class = $this->class; else $class = "";
			if (isset($this->showType)) $showType = $this->showType; else $showType = true;
			
			$types = explode("|", $this->content);
			$cnt = 0;
			foreach ($types as $type2) {
				$cnt++;
				$type = trim($type2);
				$method = false;
				if (strpos($type, "::"))
				{
					$type = explode("::", $type);
					$method = trim($type[1], '()');
					$type = $type[0];
				} 
				if (class_exists($type))
				{
					if (strpos($method, '$') === false)
					{
		?>
					<a class="<?=$class?>" href="<?=ClassController::uri($type)?><?php if ($method){ ?>#m:<?=$method?><?php } ?>">
						<?php if ($showType){ ?><?=$type?><?php } ?><?php if ($method && $showType){ ?>::<?php } ?><?php if ($method){ ?><?=$method?><?php } ?>
					</a>		
		<?php 
					}else{
		?>
					<a class="<?=$class?>" href="<?=ClassController::uri($type)?><?php if ($method){ ?>#p:<?=$method?><?php } ?>">
						<?php if ($showType) { ?><?=$type?><?php } ?><?php if ($method && $showType){ ?>::<?php } ?><?php if ($method) { ?><?=$method?><?php } ?>
					</a>
		<?php 
					}
				}else{
		?>
				<?=$type?><?php if ($method) { ?>::<?=$method?><?php } ?>
		<?php 
				}
		?>
				<?php if ($cnt < count($types)) { ?> | <?php } ?>
		<?php 
			}
		?>

	</ou:register>