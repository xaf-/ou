
	<ou:register name="see">
	
		<?php
			$data = $this->data;
			$sees =  $data->comments()->comment("see");
		?>
	
	<?php if ($sees) { ?>
		<div class="toggle see">
			<?php foreach ($sees->values() as $see){ ?>
			
				<?php 
					$num = strpos($see, " ");
					$see_desc = "";
					if ($num !== false)
					{
						$see_desc = substr($see, $num);
						$see = substr($see, 0, $num);
					} 
				?>
				
				<div>· Vea <ou:type><?=$see?></ou:type> <?=$see_desc?></div>
				
			<?php } ?>
		</div>
	<?php } ?>
	
	</ou:register>