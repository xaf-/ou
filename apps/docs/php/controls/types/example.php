
	<ou:register name="example">
	
		<?php
			global $_CONFIG;
			$examples = $this->examples; 
		?>
	
		<?php if ($examples) { ?>
		
			<?php
				$id = rand(100000000, 999999999);
				$examples = $examples->values();
			?>
		
			<?php if (count($examples) > 0) { ?>
			
				<h2>Ejemplo</h2>
				
				<div class="page-control" id="page-control-<?=$id?>" data-role="page-control">
			        <ul>
			        	<?php $cnt = 0; foreach ($examples as $example) { $cnt++; ?>
			        		<?php
			        			$path =  $_CONFIG["PATH_TESTS"].$example;
			        			if (file_exists($path))
			        			{
			        		?>
				            <li class="<?php if ($cnt==0) { ?>active<?php } ?> frame_code"><a href="#frame<?=$cnt-1?>">
				            	<?php if (count($examples) > 0) { ?>Ejemplo <?=$cnt?><?php }else{ ?>Código<?php } ?>
				            </a></li>
			        		<?php 
			        			}
							} 
						?>
					    <li class="frame_result" style="display: none"><a href="#frameN">Resultado</a></li>
			        </ul>
			        <div class="frames">
			        	<?php $cnt=0; foreach ($examples as $example) { $cnt++; ?>
			        		<?php
			        			$path = $_CONFIG["PATH_TESTS"] . $example;
			        			if (file_exists($path))
			        			{
			        				$o = array();
			        				$o["div"] = false;
			        				$o["style"] = "";
			        				$o["cache"] = true;
			        			}
			        		?>
			        			<div class="frame <?php if ($cnt == 0) { ?>active<?php } ?> frame_code" id="frame<?=$cnt-1?>">
					            	<?=OU_Debug::syntax(file_get_contents($path), "php", $o)?>
					            	<a href="javascript:runSample<?=$id?>(<?=$cnt?>)" style="position: absolute; right: 15px; bottom: 15px">Ejecutar</a> 
			        			</div>
			        	<?php } ?>
			            <div class="frame frame_result" style="display: none" id="frameN">
			            	<pre id="sample_result"></pre>
			            </div>
			        </div>
			    </div>
					
				
				
				
				<script>
					function runSample<?=$id?>(idx)
					{
	
						var surl = "<?=$this->ajaxUri?>";
	
						$.ajax(
							{
								url : surl,
								data : { "index" : idx },
								method : "post",
								success: function(msg)
								{
									$("#page-control-<?=$id?> .frame_code").removeClass("active");
									$("#page-control-<?=$id?> .frame.frame_code").hide();
									$("#page-control-<?=$id?> .frame_result").addClass("active").show();
									$("#page-control-<?=$id?> #sample_result").html(msg);
								}
							}
						);
						
					};
				</script>
		
			<?php } ?>
		<?php } ?>
	
	</ou:register>