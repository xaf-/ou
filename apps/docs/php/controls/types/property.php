
	<ou:register name="property">
	
		<?php
			$prop = $this->prop; 
		?>

		
		<a name="p:$<?=$prop->name()?>"></a>
	
		<div class="access_<?=$prop->access()?> property <?php if (!$prop->isOwner()) { ?>inherited<?php } ?>">
			<h4><?=$prop->syntax()?></h4>
			<div><ou:desc><?=$prop->description()?></ou:desc></div>
			<ou:see data=$prop />
			<?php if ($prop->isVirtual()) { ?>
			<div class="toggle params">
				<?php if ($prop->getGetter()) { ?>
				<h5>Getter : <a href="#m:<?=$prop->getGetter()?>"><?=$prop->getGetter()?></a></h5>
				<?php } if ($prop->getSetter()) { ?>
				<h5>Setter : <a href="#m:<?=$prop->getSetter()?>"><?=$prop->getSetter()?></a></h5>
				<?php } ?>
			</div>
			<?php } ?>
			<ou:test tag="property" />
		</div>
		
	</ou:register>