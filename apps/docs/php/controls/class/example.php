
	<ou:register name="class_example">
	
		<?php
			$class = $this->class;
			$examples = $class->comments()->comment("example");
			$ajaxUri = ClassController::uri($class->name(), '!example'); 
		?>
	
		<ou:example examples=$examples ajaxUri=$ajaxUri />
		
	</ou:register>