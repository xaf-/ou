
	<ou:register name="class_inheritance">
	
		<?php
			$class = $this->class;
			$tmp = $this->tmp;
			$parents = array_reverse($class->parents());
			if (count($tmp->extend[$class->name()]) > 0 || count($parents) > 0 ) {
				
		?>
			<h2>Jerarquía de herencia</h2>
		<?php 
			} 
		?>
		
		<?php if (count($parents) > 0) { ?>
			<h3>Heredado</h3>
			<?php $cnt = 0; foreach ($parents as $parent) { $cnt++; ?>
				<ou:type><?=$parent->name()?></ou:type>
				
				<?php if ($cnt < count($parents)) { ?>
					<?=htmlentities(">")?>
				<?php } ?>
				
			<?php } ?>
		<?php } ?>
		
		<?php if (count($tmp->extend[$class->name()]) > 0) { ?>
			<h3>Jerarquía de herencia</h3>
			<ou:org_class class=$class tmp=$tmp />
		<?php } ?>
		
	</ou:register>