
	<ou:register name="class_constructor">
		<?php
			$class = $this->class;
			$c = $class->constructor();
			if ($c)
			{
				
				$str = "";
				foreach ($c->params() as $param)
				{
					$str .= $param->name() . ($param->hasDefaultValue() ? " = " . var_export($param->defaultValue(), true) : "");
				}
				
				$str = $c->name() . "(" . $str . ")";
				
				$o = array();
				$o["div"] = true;
				$o["style"] = "";
				$o["cache"] = true;
		?>
			<p><?=$c->description()?></p>
			<?=OU_Debug::syntax(str_replace("\n", "", $str), "php", $o)?>
		<?php 
			} 
		?>

	</ou:register>