
	<ou:register name="class_methods">
	
		<?php
			$class = $this->class;
			$static = isset($this->static) && $this->static;
			$methods = $static ?  $class->methods(ReflectionMethod::IS_STATIC) : $class->methods();
			if (count($methods) > 0) {
				$cntMethods = 0;
				
				$str = "";
				foreach ($methods as $method)
					if (!$static && $method->isStatic()) {} else $cntMethods++;
				
				if ($cntMethods > 0)
				{
					 
			?>
					<h2>Métodos <?=$static?"estáticos":""?></h2>
					<?php foreach ($methods as $method) { ?>
						<ou:method method=$method />
					<?php } ?>
			<?php
				} 
			?>
		<?php
			} 
		?>
		
	</ou:register>