
	<ou:register name="template_sidebar" inherit="template_layout">
		
	    <div class="page with-sidebar">
	    	<ou:part_content name="messages" />
	    	<?php if (isset($this->parts->title) && $this->parts->title ) { ?>
	    		<div class="page-header">
	    			<div class="page-header-content">
						<ou:part_content name="title" />
						<a href="<?=$this->base_uri?>" class="back-button big page-back"></a>
					</div>
	    		</div>
	    	<?php } ?>
			<div class="page-sidebar">
				<ou:part_content name="sidebar" />
			</div>
	        <div class="page-region">
	        	<div class="page-region-content">
			        <ou:part_content name="messages" />
			        <ou:content />
			    </div>
	        </div>
	    </div>
		
	</ou:register>