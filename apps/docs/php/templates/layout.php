<ou:register name="template_layout">

<?php 
	global $pgtime, $_CONFIG;
?>
	
	<!DOCTYPE html>
	<html class="blue ">
	
	<head>
	
		<ou:js file="ext/jquery/1.8.3.js" />
		<ou:css file="main.css" /> 
		<ou:css file="docs.css" /> 
		<ou:js file="main.js" />
	
		<meta charset="utf8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Documentación de OU <?=$_CONFIG["VERSION"]?>" />
		
		<ou:part_content name="header" />
	
	</head>
	
	<body>
	
	 	<div class="page <?=!isset($secondary) || $secondary?"secondary":""?>">
	 	
	 		<ou:menu>
	 			<ou:part_content name="menu" />
			</ou:menu>
			
			<ou:part_content name="messages" />
			
			<ou:content />
			
			<div style="clear:both"></div>
			<div class="navigation-bar">
			    <div class="navigation-bar-inner bg-color-darken">
			        <ul style="display: block">
			            <li class="no-hover">
			            	OU Framework 2012 © by 
			            	<a class="fg-color-blueLight" href="//x-s.es">fontcolor</a>
			            </li>
			            <li class="no-hover" style="float:right">
			            	<div>Página generada en <?=number_format(microtime(true) - $pgtime, 6)?> segundos</span>
			            </li>
			        </ul>
			        
			    </div>
			</div>
			
	    </div>
	    
	</body>
	
	</html>
	
</ou:register>