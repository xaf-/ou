
	<ou:register name="template_default" inherit="template_layout">
	
		<?php if (isset($this->parts->title)) { ?>
			<div class="page-header">
			<?php if (strpos($this->parts->title->content, "<h1") !== false){ ?>
	            <div class="page-header-content">
	            	<ou:part_content name="title" />
	            </div>
		    <?php }else{ ?>
		    	<ou:part_content name="title" />
		    <?php } ?>
		    </div>
        <?php } ?>
 
        <div class="page-region">
        
        	<ou:part_content name="messages" />
        	
            <div class="page-region-content">
            	<ou:content />
            </div>
        </div>
		
	</ou:register>