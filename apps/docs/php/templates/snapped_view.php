
	<ou:register name="template_snapped_view" inherit="template_layout">
	
	
		<div class="page snapped">
        	<div style="padding: 20px;"><ou:part_content name="sidebar" /></div>
	    </div>
	    <div class="page fill">
	       <div style="padding: 20px">
	        <?php if (isset($this->parts->title)) { ?>
	    		<div class="page-header"><div class="page-header-content"><ou:part_content name="title" /></div></div>
	    	<?php } ?>
	       	<ou:content />
	       </div>
	    </div>
	
	</ou:register>