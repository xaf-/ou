
	<ou:register name="menu_properties">
	
		<?php
			$class = $this->class; 
		?>

		<?php if (count($class->props()) > 0) { ?>
			<li class="sticker sticker-color-orange">
				<a class="trim">Propiedades</a>
				<ul class="sub-menu light">
					<?php foreach ($class->props() as $prop) {  ?>
					<li class="item_property access_<?=$prop->access()?>"><ou:type class="trim" showType=false><?=$class->name()?>::$<?=$prop->name()?></ou:type></li>
					<?php } ?>
				</ul>
			</li>
		<?php } ?>
	
	</ou:register>