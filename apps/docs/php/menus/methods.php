
	<ou:register name="menu_methods">
	
		<?php
			$static = isset($this->static) ? $this->static : false;
			$class = $this->class;
			if ($static)
				$methods1 = $class->methods(ReflectionMethod::IS_STATIC);
			else
				$methods1 = $class->methods();
			if (count($methods1) > 0) {
		?>
			<li class="sticker sticker-color-blueLight">
				<a>Métodos <?=$static?"estáticos":""?></a>
				<ul class="sub-menu light">
					<?php foreach ($methods1 as $method) { ?>
						<?php if ($static || !$method->isStatic()) { ?>
							<li class="item_method access_<?=$method->access()?> <?php if ($method->isMagic()) { ?>access_magic <?php } ?>"><ou:type class="trim" showType=false><?=$class->name()?>::<?=$method->name()?>()</ou:type></li>
						<?php } ?>
					<?php } ?>
				</ul>
			</li>
		<?php 
			}
		?>
	
	</ou:register>