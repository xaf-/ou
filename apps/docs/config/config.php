<?php

	OU_Config::IncClassHelper(
		array(
			"OU_Utils_Smarty"
		)
	);
	
	$_CONFIG_PATH = defined("OU_DOCS_PATH") ? OU_DOCS_PATH : dirname(__FILE__) . "/../../../";

	$_CONFIG = array(
		// Global
		"VERSION" => "0.1",
		"ICON_UNK" => "unk.png",
		"PATH" => $_CONFIG_PATH,
		// Paths
		"PATH_TESTS" =>  $_CONFIG_PATH . "/res/tests/"
	);
	
	$codeset = "UTF-8";
	$lang = "esp_ESP";
	
//	ini_set("default_charset", $codeset);
//	putenv('LANG='.$lang.'.'.$codeset);
//	putenv('LANGUAGE='.$lang.'.'.$codeset);
	
	date_default_timezone_set('Europe/Madrid');
//	setlocale(LC_ALL, $lang, $lang.'.'.$codeset);
	
	mb_internal_encoding($codeset);
	mb_http_output($codeset);
	mb_http_input($codeset);
	mb_regex_encoding($codeset);
	
	
	ini_set("iconv.input_encoding", $codeset);
	ini_set("iconv.internal_encoding", $codeset);
	ini_set("iconv.output_encoding", $codeset);	
	
	OU_Utils_Smarty::assign("_CONFIG", $_CONFIG);
	
	
?>