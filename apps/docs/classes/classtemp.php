<?php 

	OU::IncClass(
		array(
			"OU_Array"
		)
	);
	
	class ClassTemp
	{
		
		/**
		 * @var OU_Array
		 */
		public $base;
		public $extend;
		
		private function _getDataPath($name)
		{
			return OU_App::app()->cache("tmp") . "/$name.json";
		}
		
		public function __construct()
		{
			$this->base = OU_Array::FromFile($this->_getDataPath("base"));
			$this->extend = OU_Array::FromFile($this->_getDataPath("extend"));
		}
		
		public function __destruct()
		{
			if ($this->base->isModified())
				$this->base->SaveToFile($this->_getDataPath("base"));
			if ($this->extend->isModified())
				$this->extend->SaveToFile($this->_getDataPath("extend"));
		}
		
		/**
		 * 
		 * @param OU_Debug_Class $class
		 */
		public function register($class)
		{
			
			$p = "";
			if ($class->parent()) $p = $class->parent()->name();
			
			if (!isset($this->base[$class->name()])) $this->base[$class->name()] = array();
			if (array_search($p, $this->base[$class->name()]) === false)
			{
				$this->base[$class->name()][] = $p;
				$this->base->DoChange();
			}
			
			if (!isset($this->extend[$p])) $this->extend[$p] = array();
			if (array_search($class->name(), $this->extend[$p]) === false)
			{
				$this->extend[$p][] = $class->name();
				$this->extend->DoChange();
			}
			
		}
		
	}

?>