{assign var="secondary" value=true scope="global"}

{capture name="header"}
	
{/capture}

{capture name="title"}
	asd
{/capture}

{capture name="body"}

	<div>
	
		
	  	<table style="width: 100%; table-layout: fixed">
	  		<colgroup>
	  			<col width="30%" />
	  		</colgroup>
	  		<thead>
	  			<tr>
	  				<th>Nombre</th>
	  				<th></th>
	  			</tr>
	  		</thead>
	  		<tbody>
	  		{foreach from=get_declared_classes() item=className}
				{assign var="class" value=OU_Debug::reference($className)}
		  		<tr>
		  			<td class="trim"><ou:type>{$className}</ou:type></td>
		  			<td class="trim"><ou:desc>{$class->description()}</ou:desc></td>
		  		</tr>
		  	{/foreach}
		  	</tbody>
	  		</table>
		
		
		
	</div>
	
{/capture}

{include file="templates/default.tpl"}
