
	{assign var="body" value=$smarty.capture.body}
	
	{assign var="title" value=$smarty.capture.profile_title}
	{assign var="desc" value=$smarty.capture.profile_desc}
	
	{capture name="body"}
		
		<div class="profile">
	
			<div class="block" style="padding: 0">
				<div class="cover">
					<div class="pic">
						<div class="pic_img"></div>
					</div>
				</div>
				
				<div class="profile_title">
					<h1>
						{$title}
					</h1>
				</div>
				<div class="p">
					{$desc}
				</div>
			</div>
			
			{$body}
			
		</div>
			
	{/capture}
	
	{include file="templates/layout.tpl"}