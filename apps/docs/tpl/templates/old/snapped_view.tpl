
	{if isset($smarty.capture.messages)}
		{assign var="messages" value=$smarty.capture.messages}
	{else}
		{assign var="messages" value=false}
	{/if}	
	{capture name="messages"}{/capture}

	{assign var="body_right" value=$smarty.capture.body}
	{if isset($smarty.capture.body_left)}
		{assign var="body_left" value=$smarty.capture.body_left}
	{else}
		{assign var="body_left" value=""}
	{/if}
	
	{capture name="body"}
	
		<div class="page snapped">
        	<div style="padding: 20px;">{$body_left}</div>
	    </div>
	    <div class="page fill">
	       <div style="padding: 20px">
	        {$messages}
	        {if isset($title)}
	    		<div class="page-header"><div class="page-header-content">{$title}</div></div>
	    	{/if}
	       	{$body_right}
	       </div>
	    </div>

	
	{/capture}
	
	{include file="templates/layout.tpl"}