
	{if isset($smarty.capture.messages)}
		{assign var="messages" value=$smarty.capture.messages}
	{else}
		{assign var="messages" value=false}
	{/if}	
	{capture name="messages"}{/capture}

	{assign var="body_right" value=$smarty.capture.body}
	{if isset($smarty.capture.body_left)}
		{assign var="body_left" value=$smarty.capture.body_left}
	{else}
		{assign var="body_left" value=""}
	{/if}
	{if isset($smarty.capture.title)}
		{assign var="title" value=$smarty.capture.title}
	{else}
		{assign var="title" value=false}
	{/if}
	
	{capture name="body"}
	
	    <div class="page with-sidebar">
	    	{$messages}
	    	{if $title}
	    		<div class="page-header">
	    			<div class="page-header-content">
						{$title}
						<a href="{$base_uri}" class="back-button big page-back"></a>
					</div>
	    		</div>
	    	{/if}
			<div class="page-sidebar">
				{$body_left}
			</div>
	        <div class="page-region">
	        	<div class="page-region-content">
			        {$messages}
			        {$body_right}
			    </div>
	        </div>
	    </div>
	{/capture}
	
	{include file="templates/layout.tpl"}