
	{assign var="body" value=$smarty.capture.body}
	
	{if isset($smarty.capture.messages)}
		{assign var="messages" value=$smarty.capture.messages}
	{else}
		{assign var="messages" value=false}
	{/if}	
	{capture name="messages"}{/capture}
	
	{if isset($smarty.capture.title)}
		{assign var="title" value=$smarty.capture.title}
	{else}
		{assign var="title" value=false}
	{/if}
	{capture name="body"}
	
		{if $title}
			<div class="page-header">
			{if strpos($title, "<h1") !== false}
	            <div class="page-header-content">
	            	{$title}
	            </div>
		    {else}
		    	{$title}
		    {/if}
		    </div>
        {/if}
 
        <div class="page-region">
        
        	{$messages}
        	
            <div class="page-region-content">
            	{$body}
            </div>
        </div>
        
	{/capture}
	
	{include file="templates/layout.tpl"}