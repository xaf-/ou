<!DOCTYPE html>
<html class="blue ">

<head>

{ou_js file="ext/jquery/1.8.3.js"}
{ou_css file="main.css"} 
{ou_css file="docs.css"} 
{ou_js file="main.js"}

	<meta charset="utf8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Documentación de OU {$_CONFIG.VERSION}" />
 
{if isset($smarty.capture.header)} 
	{$smarty.capture.header} 
{/if}

</head>

<body>

 	<div class="page {if !isset($secondary) || $secondary} secondary{/if}">
 	
 		<ou:menu>
 		{if isset($smarty.capture.menu)} 
			{$smarty.capture.menu} 
		{/if}	
		</ou:menu>
		
		{if isset($smarty.capture.messages)}
			{$smarty.capture.messages} 
		{/if}
		
		{if isset($smarty.capture.body)}
			{$smarty.capture.body} 
		{/if}
		
		<div style="clear:both"></div>
		<div class="navigation-bar">
		    <div class="navigation-bar-inner bg-color-darken">
		        <ul style="display: block">
		            <li class="no-hover">
		            	OU Framework 2012 © by 
		            	<a class="fg-color-blueLight" href="//x-s.es">fontcolor</a>
		            </li>
		            <li class="no-hover" style="float:right">
		            	<div>Página generada en {number_format(microtime(true) - $pgtime, 6)} segundos</span>
		            </li>
		        </ul>
		        
		    </div>
		</div>
		
    </div>
    
</body>

</html>
