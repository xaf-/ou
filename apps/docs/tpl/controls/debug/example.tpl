
	<ou:register name="example">
	
		{if $examples}
		
			{assign var="id" value=rand(100000000, 999999999)}
		
			{assign var="examples" value=$examples->values()}
			{if count($examples) > 0}
			
				<h2>Ejemplo</h2>
				
				<div class="page-control" id="page-control-{$id}" data-role="page-control">
			        <ul>
			        	{foreach from=$examples item=example name=example}
			        		{assign var="path" value="{$_CONFIG.PATH_TESTS}{$example}"}
							{if file_exists($path)}
					            <li class="{if $smarty.foreach.example.first}active{/if} frame_code"><a href="#frame{$smarty.foreach.example.index}">
					            	{if count($examples) > 0}Ejemplo {$smarty.foreach.example.index+1}{else}Código{/if}
					            </a></li>
					        {/if}
				        {/foreach}
					    <li class="frame_result" style="display: none"><a href="#frameN">Resultado</a></li>
			        </ul>
			        <div class="frames">
			        	{foreach from=$examples item=example name=example}
							{assign var="path" value="{$_CONFIG.PATH_TESTS}{$example}"}
							{if file_exists($path)}
								{assign var="o" value=array()}
								{$o.{"div"} = false}
								{$o.{"style"} = ""}
								{$o.{"cache"} = true}
					            <div class="frame {if $smarty.foreach.example.first}active{/if} frame_code" id="frame{$smarty.foreach.example.index}">
					            	{OU_Debug::syntax(file_get_contents($path), "php", $o)}
					            	<a href="javascript:runSample{$id}({$smarty.foreach.example.index})" style="position: absolute; right: 15px; bottom: 15px">Ejecutar</a> 
					            </div>
			            	{/if}
			            {/foreach}
			            <div class="frame frame_result" style="display: none" id="frameN">
			            	<pre id="sample_result"></pre>
			            </div>
			        </div>
			    </div>
					
				
				
				
				<script>
					function runSample{$id}(idx)
					{
	
						var surl = "{$ajaxUri}";
	
						$.ajax(
							{
								url : surl,
								data : { "index" : idx },
								method : "post",
								success: function(msg)
								{
									$("#page-control-{$id} .frame_code").removeClass("active");
									$("#page-control-{$id} .frame.frame_code").hide();
									$("#page-control-{$id} .frame_result").addClass("active").show();
									$("#page-control-{$id} #sample_result").html(msg);
								}
							}
						);
						
					};
				</script>
		
			{/if}
		{/if}
	
	</ou:register>