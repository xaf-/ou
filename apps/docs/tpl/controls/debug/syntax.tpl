
	<ou:register name="syntax">

		{if isset($div) && $div}
		
			{assign var="code" value=str_replace("\n", "", "{$content}")}
		
			<span class="syntax">
			
				{assign var="keys" value=explode(' ', 'public private protected final abstract class extends interface static')}
			
				{assign var="code" value=preg_replace("/(\".*?\")/", "<span class=\"string\">\$1</span>", $code)}
				{assign var="code" value=preg_replace("/('.*?')/", "<span class=\"string\">\$1</span>", $code)}
			
				{assign var="keys" value=explode(' ', 'public private protected final abstract class extends interface static')}
				{assign var="code" value=preg_replace("/({implode('|', $keys)})(\s|\$)/", "<span class=\"key\">\$1</span> ", $code)}
				
				{assign var="code" value=preg_replace("/\b(\d+)\b/", "<span class=\"number\">\$1</span>", $code)}
				
				{assign var="code" value=preg_replace("/(\\$[a-zA-Z0-9\_]+)/", "<span class=\"var\">\$1</span>", $code)}
				
				{$code}
				
			</span>
		
		{else}
		
			{assign var="o" value=array()}
			{$o.{"style"} = ""}
			{if isset($cache)}
				{$o.{"cache"} = $cache}
			{/if}
			{OU_Debug::syntax(str_replace("\n", "", $content), "php", $o)}
		
		{/if}
		
	</ou:register>