
	<ou:register name="property">

		
		<a name="p:{'$'}{$prop->name()}"></a>
	
		<div class="access_{$prop->access()} property {if !$prop->isOwner()}inherited{/if}">
			<h4>{$prop->syntax()}</h4>
			<div><ou:desc>{$prop->description()}</ou:desc></div>
			<ou:see data=$prop />
			{if $prop->isVirtual()}
			<div class="toggle params">
				{if $prop->getGetter()}
				<h5>Getter : <a href="#m:{$prop->getGetter()}">{$prop->getGetter()}</a></h5>
				{/if}{if $prop->getSetter()}
				<h5>Setter : <a href="#m:{$prop->getSetter()}">{$prop->getSetter()}</a></h5>
				{/if}
			</div>
			{/if}
			<ou:test tag="property" />
		</div>
		
	</ou:register>