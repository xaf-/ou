
	<ou:register name="type">
	
		{if !isset($class)}
			{assign var="class" value=""}
		{/if}
		{if !isset($showType)}
			{assign var="showType" value=true}
		{/if}
	
		{assign var="types" value=explode("|", $content)}
		{foreach from=$types item=type2 name="type"}
		
			{assign var="type" value=trim($type2)}
			{assign var="method" value=false}
			
			{if strpos($type, "::")}
				{assign var="type" value=explode("::", $type)}
				{assign var="method" value=trim("{$type[1]}", '()')}
				{assign var="type" value="{$type[0]}"}
			{/if}
			
			{if class_exists($type)}
				{if strpos($method, '$') === false}
					<a class="{$class}" href="{ClassController::uri($type)}{if $method}#m:{$method}{/if}">
						{if $showType}{$type}{/if}{if $method && $showType}::{/if}{if $method}{$method}{/if}
					</a>
				{else}
					<a class="{$class}" href="{ClassController::uri($type)}{if $method}#p:{$method}{/if}">
						{if $showType}{$type}{/if}{if $method && $showType}::{/if}{if $method}{$method}{/if}
					</a>
				{/if}
			{else}
				{$type}{if $method}::{$method}{/if}
			{/if}
			
			{if !$smarty.foreach.type.last} | {/if}
			
		{/foreach}
	
	</ou:register>