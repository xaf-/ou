
	<ou:register name="method">
	
		<a name="m:{$method->name()}"></a>
	
		<div class="access_{$method->access()} {if $method->isMagic()}access_magic {/if}method {if $method->isDeprecated()}deprecated {/if} {if !$method->isOwner()}inherited{/if}">
			<h4>{$method->syntax()}</h4>
			<div>
				<ou:desc>{$method->description()}</ou:desc>
			</div>
			<ou:see data=$method />
			<div class="toggle params">
				{foreach from=$method->params() item=param name=method}
					<h5>{"$"|htmlentities}{$param->name()} : <ou:type>{$param->type("unknown")}</ou:type> {if $param->hasDefaultValue()}(Defecto : {var_export($param->defaultValue())}){/if}</h5>
					<div><ou:desc>{$param->description()}</ou:desc></div>
				{/foreach}
				{if $method->result()}
					<h5>return : <ou:type>{$method->result()}</ou:type></h5>
					<div><ou:desc>{$method->resultDesc()}</ou:desc></div>
				{/if}
			</div>
			<ou:test tag="method" />
		</div>
		
	</ou:register>