
	<ou:register name="see">
	
	{assign var="sees" value=$data->comments()->comment("see")}
	{if $sees}
		<div class="toggle see">
			{foreach from=$sees->values() item=see}
			
				{assign var="num" value=strpos($see, " ")}
				{assign var="see_desc" value=""}
				{if $num !== false}
					{assign var="see_desc" value=substr($see, $num)}
					{assign var="see" value=substr($see, 0, $num)}
				{/if}
				
				<div>· Vea <ou:type>{$see}</ou:type> {$see_desc}</div>
				
			{/foreach}
		</div>
	{/if}
	
	</ou:register>