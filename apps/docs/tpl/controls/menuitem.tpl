
	<ou:register name="menuitem">
	
		{assign var="hasSub" value=isset($content)}
		
		<li {if $hasSub}data-role="dropdown" class="sub-menu"{/if}>
			{if isset($href)}
				<a href="{$href}">{$text}</a> 
			{else}
				<a>{$text}</a>
			{/if}
			{if $hasSub}
			<ul class="dropdown-menu {if isset($isRight) && $isRight}place-right{/if}">
				{$content}
			</ul>
			{/if}
		</li>
	
	</ou:register>
