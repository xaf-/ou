
	<ou:register name="menu">
	
		<div class="navigation-bar">
	        <div class="navigation-bar-inner">
	        
	        	<span class="menu-pull"></span>
	        
	            <div class="brand">
		            <a href="/"><span class="metro-ui-logo place-left"></span></a>
		            <a href="/"><span class="name"><sup class="fg-color-yellow tertiary-info-secondary-text"> v {$_CONFIG.VERSION}</sup></span></a>
		        </div>
		        
		        {if isset($content)}{$content}{/if}
	 
	            <ul>
	            
	            	<ou:menuitem text="Clases">
	            		{assign var="tmp" value=$controller->tmp()}
	            		{foreach from=$tmp->base->toArray() key=kname item=name}
	            			<ou:menuitem href="{ClassController::uri($kname)}" text="{$kname}" />
	            		{/foreach}
	            	</ou:menuitem>
	            	
	            	<ou:menuitem text="Item 2">
	            	
	            		<ou:menuitem text="SubItem" />
	            		<ou:menuitem text="SubItem" />
	            		
	            	</ou:menuitem>
	            </ul>
	        </div>
	    </div>
	
	</ou:register>
	