{assign var="secondary" value=true scope="global"}

{capture name="header"}
	
{/capture}

{capture name="title"}
	<a href="../" class="back-button big page-back" style="position: absolute; z-index: 2"></a>
{/capture}

{capture name="body"}

	{$fileContent}
	
{/capture}

{include file="templates/default.tpl"}
