{capture name="header"}
	
{/capture}

{capture name="messages"}
	<ou:test tag="start" />
	<noscript>
	
    	Debe habilitar JavaScript en su navegador para ver correctamente este sitio.
    
	</noscript>
{/capture}

{capture name="menu"}
	<ul class="place-right">
    	{ou_control_block name="menuitem" text="{$class->name()}" isRight="true"}
    		<ou:menuitem href="#syntax" text="Sintaxis" />
    		<ou:menuitem href="#constructor" text="Constructor" />
    		{if count($class->props()) > 0}
    		<ou:menuitem href="#properties" text="Propiedades" />
    		{/if}
    		{assign var="methods1" value=$class->methods()}
    		{assign var="methods2" value=$class->methods(ReflectionMethod::IS_STATIC)}
    		{if (count($methods1) - count($methods2)) > 0}
    		<ou:menuitem href="#methods" text="Métodos" />
    		{/if}
    		{if count($methods2)}
    		<ou:menuitem href="#methods_s" text="Métodos estáticos" />
    		{/if}
    	{/ou_control_block}
    </ul>
{/capture}

{capture name="title"}
	<h1>{$class->name()}<small></small></h1>
{/capture}

{capture name="body_left"}

 		<ul>
            <li><a class="trim" href="#">{$class->name()}</a></li>
            {include file="./inc/class/menu/properties.tpl"}
            {include file="./inc/class/menu/methods.tpl"}
            {include file="./inc/class/menu/methods_s.tpl"}
        </ul>

{/capture}

{capture name="body"}

	<div>
	
		{* Version 
			{assign var="version" value=$class->comments()->comment("version")}
			{if $version}
				<span style="float: right">{$version->value()}</span>
			{/if}
		*}
	
		<p><ou:desc>{$class->description()}</ou:desc></p>		
		
		<a name="inheritance"></a>
		{include file="./inc/class/inheritance.tpl"}
		
		<a name="syntax"></a>
		{include file="./inc/class/syntax.tpl"}
		
		<a name="example"></a>
		{include file="./inc/class/example.tpl"}
		
		<a name="constructor"></a>
		<div style="position: relative">
		
			<ul class="access_select" style="position: absolute; right: 0; top: 4px">
				<li class=""><span class="access access_private"><input type="checkbox" data-type="access_private" /></span> private</li>
				<li class=""><span class="access access_protected"><input type="checkbox" data-type="access_protected" /></span> protected</li>
				<li class=""><span class="access access_public"><input checked="checked" type="checkbox" data-type="access_public" /></span> public</li>
				<li class=""><span class="access access_magic"><input type="checkbox" data-type="access_magic" /></span> magic</li>
			</ul>
			
			{include file="./inc/class/constructor.tpl"}
		
		</div>
		
		<a name="properties"></a>
		{include file="./inc/class/properties.tpl"}
		
		<a name="methods"></a>
		{include file="./inc/class/methods.tpl" static=false}
			
		<a name="methods_s"></a>
		{include file="./inc/class/methods.tpl" static=true}
			
			
		</p>
	
	</div>
	
	<ou:test tag="end" />
	
{/capture}

{include file="templates/sidebar.tpl"}
