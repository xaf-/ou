
	{assign var="parents" value=array_reverse($class->parents())}

	{if count($tmp->extend.{$class->name()}) > 0 || count($parents) > 0}
		<h2>Jerarquía de herencia</h2>
	{/if}

	{if count($parents) > 0}	
		<h3>Heredado</h3>
		{foreach from=$parents item=parent name=parents}
			<ou:type>{$parent->name()}</ou:type>
			{if !$smarty.foreach.parents.last}
				{">"|htmlentities}
			{/if}
		{/foreach}
	{/if}
	
	{if count($tmp->extend.{$class->name()}) > 0}
		<h3>Jerarquía de herencia</h3>
		<ou:org_class class=$class tmp=$tmp />
	   	
	{/if}
	