
	{assign var="c" value=$class->constructor()}
	
	{if $c}
	
		<h2>Constructor</h2>
		
		{capture name="constructor"}{strip}
		
		{$c->name()}(
			{foreach from=$c->params() item=param}
				{$param->name()} {if $param->hasDefaultValue()} = {var_export($param->defaultValue())}{/if}
			{/foreach}
		)
		
		{/strip}{/capture}
		
		<p>{$c->description()}</p>
		
		{assign var="o" value=array()}
		{$o.{"div"} = true}
		{$o.{"style"} = ""}
		{$o.{"cache"} = true}
		{OU_Debug::syntax(str_replace("\n", "", $smarty.capture.constructor), "php", $o)}
	
	{/if}