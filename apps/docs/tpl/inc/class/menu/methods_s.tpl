
    {assign var="methods" value=$class->methods(ReflectionMethod::IS_STATIC)}

	{if count($methods) > 0}
		<li class="sticker sticker-color-greenLight">
			<a>Métodos</a>
			<ul class="sub-menu light">
				{foreach from=$methods item=method}
					<li class="item_method access_{$method->access()} {if $method->isMagic()}access_magic {/if}"><ou:type class="trim" showType=false>{$class->name()}::{$method->name()}()</ou:type></li>
				{/foreach}
			</ul>
		</li>
	{/if}