
	{if count($class->props()) > 0}
	<li class="sticker sticker-color-orange">
		<a class="trim">Propiedades</a>
		<ul class="sub-menu light">
			{foreach from=$class->props() item=prop}
			<li class="item_property access_{$prop->access()}"><ou:type class="trim" showType=false>{$class->name()}::{'$'}{$prop->name()}</ou:type></li>
			{/foreach}
		</ul>
	</li>
	{/if}