
	{assign var="methods1" value=$class->methods()}
    {assign var="methods2" value=$class->methods(ReflectionMethod::IS_STATIC)}

	{if (count($methods1) - count($methods2)) > 0}
		<li class="sticker sticker-color-blueLight">
			<a>Métodos</a>
			<ul class="sub-menu light">
				{foreach from=$methods1 item=method}
					{if !$method->isStatic()}
						<li class="item_method access_{$method->access()} {if $method->isMagic()}access_magic {/if}"><ou:type class="trim" showType=false>{$class->name()}::{$method->name()}()</ou:type></li>
					{/if}
				{/foreach}
			</ul>
		</li>
	{/if}