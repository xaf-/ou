
	{if $static}
		{assign var="methods" value=$class->methods(ReflectionMethod::IS_STATIC)}
	{else}
		{assign var="methods" value=$class->methods()}
	{/if}
	
	{if count($methods) > 0}
			
		{assign var="cntMethods" value=0}
		{capture name="methodFilter"}
		
			{foreach from=$methods item=method}
				{if !$static && $method->isStatic()}
				{else}
					<ou:method method=$method />
					{assign var="cntMethods" value=$cntMethods + 1}
				{/if}
			{/foreach}
			
		{/capture}
		
		{if $cntMethods > 0}
			<h2>Métodos {if $static}estáticos{/if}</h2>
			{$smarty.capture.methodFilter}
		{/if}
		
	{/if}