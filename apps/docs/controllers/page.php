<?php
/***
  * @route 	docs\/pages\/(?<path>.*?)
  * @use 	OU_Header 
  * 
  * @uri	docs/pages/
  * @uri	docs/pages/#1
  */

	OU_Config::IncExt("markdown");
	
	require_once(dirname(__FILE__) . "/base.php");
	
	class PageController extends BaseController
	{
		
		private $_tmpPageFile = null;
		public function getPageFile()
		{
			if ($this->_tmpPageFile === null)
			{
				$path = $this->sourcePath() . "/res/pages/" . (isset($this->_request["path"]) ? $this->_request["path"] : "");
				if ($path{strlen($path) - 1} == "/" || $path{strlen($path) - 1} == "\\")
					$path = substr($path, 0, strlen($path) - 1);
				$ext = mb_strtolower(OU_Path::Ext($path));
				
				$path = OU_Path::Correct($path);
				
				if ($ext !== "" && is_file($path))
				{
					$this->_tmpPageFile = $path;
				}
				else
				{
					$path = $path . "/Index.md";
					if (is_file($path))
						$this->_tmpPageFile = $path;
					else
						$this->_tmpPageFile = false;
				}
				
			}
			return $this->_tmpPageFile;
		}
		
		public function content()
		{
			
			$file = $this->getPageFile();
			
			if ($file)
			{
				$c = file_get_contents($file);
				if (OU_Path::Ext($file) === "")
				{
					$c = "<pre>$c</pre>";
				}else if (OU_Path::Ext($file) === ".md")
				{
					$c = Markdown($c);
				}
				
				return $this->php("page.php", 
					array(
						"file" => $file,
						"fileContent" => $c
				));
			}else{
				OU_Header::Location("../");
				die();
			}
		}
	}

?>