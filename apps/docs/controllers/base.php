<?php

	OU::IncClass(
		array(
			"OU_Controller",
			"OU_Comments"
		)
	);
	
	OU::IncRel("../config/config.php", __FILE__);

	class BaseController extends OU_Controller
	{
		
		private $_sourcePath = null;
		
		protected $_tmp;
		public function tmp(){ return $this->_tmp; }
		
		public function sourcePath()
		{
			global $_CONFIG;
			if ($this->_sourcePath === null)
			{
				$this->_sourcePath = realpath($_CONFIG["PATH"]) . "/";
			}
			return $this->_sourcePath;
		}
		
		public static function uri($param1 = null, $param2 = null, $param3 = null)
		{
			
			$comments = OU_Comments::fromFile(static::$_fileNames[get_called_class()], array("numAsterisk" => 3));
			$c = $comments->comment("uri");
			
			if ($c)
			{
				$values = array();
				foreach ($c->values() as $value)
				{
					$values[substr_count($value, "#")] = $value;
				}
				
				ksort($values);
				
				$cnt = func_num_args();
				
				if (isset($values[$cnt]))
				{
					$c = $values[$cnt];
				}else if (count($values) > 0){
					$c = OU_Array::FromArray($values)->First();
				}else
					$c = "";
			}else
				$c = "";
			
			$cnt = 0;
			foreach (func_get_args() as $arg)
			{
				$cnt++;
				$c = str_replace("#" . $cnt, $arg === null ? "" : $arg, $c);
			}
			
			return OU_Config::$base_uri . "/" . $c;
		}
		
		protected function pre_content()
		{
			
			global $pgtime;
			$pgtime = microtime(true);
			
			$this->_tmp = new ClassTemp();
			
			session_write_close();
			
			foreach (glob(dirname(__FILE__) . "/../php/controls/*.php") as $file) $this->php($file);
			foreach (glob(dirname(__FILE__) . "/../php/controls/class/*.php") as $file) $this->php($file);
			foreach (glob(dirname(__FILE__) . "/../php/controls/types/*.php") as $file) $this->php($file);
			foreach (glob(dirname(__FILE__) . "/../php/templates/*.php") as $file) $this->php($file);
			foreach (glob(dirname(__FILE__) . "/../php/menus/*.php") as $file) $this->php($file);
			
		}
		
	}

?>