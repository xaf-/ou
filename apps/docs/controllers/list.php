<?php
/***
  * @route docs\/lista\/ 
  */

	require_once(dirname(__FILE__) . "/base.php");
	
	class ListController extends BaseController
	{
		public function content()
		{
			return $this->php("list.php");
		}
	}

?>