<?php
/***
  * @route docs\/ 
  */

	require_once(dirname(__FILE__) . "/base.php");
	
	class IndexController extends BaseController
	{
		public function post()
		{
			OU_Header::Location(PageController::uri());
			die();
		}
	}

?>