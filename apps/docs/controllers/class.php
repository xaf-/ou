<?php
/***
  * @route 	docs\/classes\/(?<class>[\w\_]+)\/
  * @route 	docs\/classes\/(?<class>[\w\_]+)(?<example>\/\!example)\/
  * @use 	OU_Config, OU_Header
  * @uri	docs/classes/#1/
  * @uri	docs/classes/#1/#2/
  */

	OU::IncClassHelper("OU_DB_MySQL");

	require_once(dirname(__FILE__) . "/base.php");
	require_once(dirname(__FILE__) . "/../classes/classtemp.php");
	
	class ClassController extends BaseController
	{
		
		public function _my_error_handler($errno, $errstr, $errfile, $errline)
		{
			
			restore_error_handler();
			
			if (!(error_reporting() & $errno)) {
				// Este código de error no está incluido en error_reporting
				return;
			}
			
			echo "<div class=\"result_error\">";
			
			echo "<div class=\"result_error_msg\">";
			
			$die = false;

			switch ($errno) {
				case E_USER_ERROR:
					echo "E_USER_ERROR: [$errno] $errstr<br />\n";
					echo "Abortando...<br />\n";
					$die = true;
					break;

				case E_USER_WARNING:
					echo "E_USER_WARNING: [$errno] $errstr<br />\n";
					break;

				case E_USER_NOTICE:
					echo "E_USER_NOTICE: [$errno] $errstr<br />\n";
					break;

				default:
					echo "E_USER_WARNING: [$errno] $errstr<br />\n";
					break;
			}
			
			echo "</div>";
			
			$trace = debug_backtrace();
			echo "<table width=\"100%\">";
			$cnt = 0;
			foreach ($trace as $t)
			{
				$cnt++;
				if ($cnt === 1) continue;
				$str1 = isset($t["class"]) ? $t["class"] : "";
				$str1 .= isset($t["function"]) ? ($str1 ? "::" : "") . $t["function"] : "";
				
				$a1 = array();
				$a2 = array();
				
				foreach ($t["args"] as $arg)
				{
					$a1[] = gettype($arg);
					$a2[] = print_r($arg, true);
					
				}
				
				$str21 = implode(", ", $a1);
				$str22 = implode(", ", $a2);
				
				$str3 = isset($t["file"]) ? "$t[file] ($t[line])" : "";
				echo "<tr><td>$str1</td><td title=\"".stripslashes($str22)."\">$str21</td><td>$str3</td></tr>";
			}
			echo "</table>";
			
			echo "<div class=\"result_error_file\">";
			echo "Error en $errfile ($errline)";
			echo "</div>";
				
			echo "</div>";
			
			if ($die) exit(1);

			/* No ejecutar el gestor de errores interno de PHP */
			return true;
		}
		
		public function content()
		{
			
			$class = $this->_request["class"];
			
			if (!OU::IncClass($class))
				OU::IncClassHelper($class);
			
			if (!class_exists($class))
			{
                \App::ErrorCode(404);
				die();
			}
			
			$t1 = microtime(true);
			$class = OU_Debug::reference($class);
			
			$this->_tmp->register($class);
				
			if (isset($this->_request["example"]))
			{
				
				$idx = isset($_REQUEST["index"]) ? $_REQUEST["index"] : 0;
				
				global $_CONFIG;
				
				set_error_handler(array($this, "_my_error_handler"));
				
				$values = $class->comments()->comment("example")->values();
				$example = $values[$idx];
				$path = $_CONFIG["PATH_TESTS"] . $example;
				if (file_exists($path))
				{
					include(realpath($path));
				}
			
				
				die();
				
			}else{
				
				return $this->php("class.php", array("class" => $class, "tmp" => $this->_tmp));
				
			}
		}
	}

?>