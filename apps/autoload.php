<?php

	require_once (dirname ( __FILE__ ) . "/../class.ou_config.php");
	
	OU_Config::IncClassHelper(
		array(
			"OU_Debug_Colorize"
		)
	);
	
	OU_Config::IncClass (
		array (
			"OU_App"
		)
	);
	
	OU_Debug_Colorize::loadTheme();
		
	$options = dirname(__FILE__);
	$sitename = OU_App::sitename(null, $options);
	OU_Config::app($sitename, $options)->autoload();

?>