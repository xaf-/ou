<?php 

	/**
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 07/12/2012
	 */

	OU_Config::IncClass(
		array(
			"OU_Color",
			"OU_Path",
			"OU_Options"
		)
	);
	 
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_image.php 338 2013-11-26 12:01:16Z xaguilarf $
	 */
	class OU_Image extends OU_Base
	{
		
		private $_img;
		public function __construct($img)
		{
			$this->_img = $img;
		}
		
		private $_destroyed = false;
		public function isDestroyed()
		{
			return $this->_destroyed;
		}
		
		private function _destroy()
		{
			if (!$this->_destroyed)
			{
				$this->_destroyed = true;
				imagedestroy($this->_img);
			}
		}
		
		/**
		 * @return number
		 */
		public function width()
		{
			return imagesx($this->_img);
		}
		
		/**
		 * @return number
		 */
		public function height()
		{
			return imagesy($this->_img);
		}
		
		/**
		 * @param string $fileName
		 * @param number $quality 0-9
		 * @return boolean
		 */
		public function png($fileName=null, $quality = 0, $filters = PNG_NO_FILTER)
		{
			return imagepng($this->_img, $fileName, $quality, $filters);
		}
		
		/**
		 * @param string $fileName
		 * @param number $quality
		 * @return boolean
		 */
		public function jpeg($fileName=null, $quality=80)
		{
			return imagejpeg($this->_img, $fileName, $quality);
		}
		
		/**
		 * @param string $fileName
		 * @param string $type
		 * @return OU_Image
		 */
		public static function fromFile($fileName, $type = "auto")
		{
			if ($type === "auto") $type = strtolower(substr(OU_Path::Ext($fileName), 1));
			switch ($type)
			{
				case "jpeg":
				case "jpg": 	
					return self::fromJpeg($fileName);
				case "png":
				case "xpng":	
					return self::fromPng($fileName);
				case "gif":
					return self::fromGif($fileName);
				default:
					return self::fromString(file_get_contents($fileName));
			}
		}
		
		/**
		 * @param string $fileName
		 * @return OU_Image
		 */
		public static function fromJpeg($fileName)
		{
			return new OU_Image(imagecreatefromjpeg($fileName));
		}
		
		/**
		 * @param string $fileName
		 * @return OU_Image
		 */
		public static function fromPng($fileName)
		{
			$img = new OU_Image(imagecreatefrompng($fileName));
			$img->AlphaBlending(true);
			$img->SaveAlpha(true);
			return $img;
		}
		
		/**
		 * @param string $fileName
		 * @return OU_Image
		 */
		public static function fromGd2($fileName)
		{
			return new OU_Image(imagecreatefromgd2($fileName));
		}
		
		/**
		 * @param string $fileName
		 * @return OU_Image
		 */
		public static function fromGif($fileName)
		{
			return new OU_Image(imagecreatefromgif($fileName));
		}
		
		/**
		 * @param string $data
		 * @return OU_Image
		 */
		public static function fromString($data)
		{
			$img = @imagecreatefromstring($data);
			if ($img === false)
				return false;
			else
				return new OU_Image($img);
		}
		
		private $_alphaBlending = false;
		private $_saveAlpha = false;
		
		/**
		 * @param NULL|boolean $value
		 * @return boolean
		 */
		public function AlphaBlending($value = null)
		{
			if ($value !== null)
			{
				$this->_alphaBlending = $value;
				imagealphablending($this->_img, $value);
			}
			return $this->_alphaBlending;
		}
		
		/**
		 * @param NULL|boolean $value
		 * @return boolean
		 */
		public function SaveAlpha($value = null)
		{
			if ($value !== null)
			{
				$this->_saveAlpha = $value;
				imagesavealpha($this->_img, $value);
			}
			return $this->_saveAlpha;
		}
		
		/**
		 * @param number $x
		 * @param number $y
		 * @param number $width
		 * @param number $height
		 * @param number|OU_Color $color
		 */
		public function Rectangle($x, $y, $width, $height, $color)
		{
			if (is_object($color) && ($color instanceof OU_Color))
				$color = $this->colorAllocate($color);
			imagerectangle($this->_img, $x, $y, $x + $width, $y + $height, $color);
		}
		
		/**
		 * @param number $x
		 * @param number $y
		 * @param number $width
		 * @param number $height
		 * @param number|OU_Color $color
		 */
		public function FillRectangle($x, $y, $width, $height, $color)
		{
			if (is_object($color) && ($color instanceof OU_Color))
				$color = $this->colorAllocate($color);
			imagefilledrectangle($this->_img, $x, $y, $x + $width, $y + $height, $color);
		}
		
		/**
		 * @param number $x1
		 * @param number $y1
		 * @param number $x2
		 * @param number $y2
		 * @param number|OU_Color $color
		 */
		public function Line($x1, $y1, $x2, $y2, $color)
		{
			if (is_object($color) && ($color instanceof OU_Color))
				$color = $this->colorAllocate($color);
			imageline($this->_img, $x1, $y1, $x2, $y2, $color);
		}
		
		/**
		 * @param number $x
		 * @param number $y
		 * @param number|OU_Color $color
		 */
		public function Fill($x, $y, $color)
		{
			if (is_object($color) && ($color instanceof OU_Color))
				$color = $this->colorAllocate($color);
			imagefill($this->_img, $x, $y, $color);
		}
		
		/**
		 * @param OU_Color $color
		 * @return number
		 */
		public function colorAllocate($color)
		{
			if ($this->_saveAlpha)
				return $this->colorAllocateAlpha($color);
			else{
				return imagecolorallocate($this->_img, $color->red, $color->green, $color->blue);
			}
		}
		
		/**
		 * @param OU_Color $color
		 * @return number
		 */
		public function colorAllocateAlpha($color)
		{
			if ($color->alpha === false)
				return imagecolorallocatealpha($this->_img, $color->red, $color->green, $color->blue, 0);
			else
				return imagecolorallocatealpha($this->_img, $color->red, $color->green, $color->blue, 127 - ($color->alpha * 127));
		}
		
		/**
		 * @param number $width
		 * @param number $height
		 * @return OU_Image
		 */
		public static function create($width, $height)
		{
			return new OU_Image(imagecreate($width, $height));
		}
		
		/**
		 * @param number $width
		 * @param number $height
		 * @return OU_Image
		 */
		public static function createTrueColor($width, $height)
		{
			return new OU_Image(imagecreatetruecolor($width, $height));
		}
		
		/**
		 * @param number $width
		 * @param number $height
		 * @return OU_Image
		 */
		public static function createAlpha($width, $height)
		{
			$img = self::createTrueColor($width, $height);
			$img->SaveAlpha(true);
			$img->AlphaBlending(true);
			$trans = $img->colorAllocateAlpha(new OU_Color(array(0,0,0,0)));
			$img->Fill(0, 0, $trans);
			return $img;
		}
		
		/**
		 * @param number $width
		 * @param number $height
		 * @param array|OU_Options $options
		 * @return OU_Image
		 */
		public function Resize($width, $height, $options = array())
		{
			
			$options = OU_Options::FromArray(
				$options,
				array(
					"resample" => false
				)
			);
			
			if ($this->SaveAlpha())
				$img = self::createAlpha($width, $height);
			else
				$img = self::createTrueColor($width, $height);
			if ($options->resample)
				imagecopyresampled($img->_img, $this->_img, 0, 0, 0, 0, $width, $height, $this->width(), $this->height());
			else
				imagecopyresized($img->_img, $this->_img, 0, 0, 0, 0, $width, $height, $this->width(), $this->height());
			return $img;
		}
		
		/**
		 * @param number $width
		 * @param number $height
		 * @param Array|OU_Options $options
		 * @return OU_Image
		 */
		public function ResizeAspectRatio($width, $height, $options = array())
		{

			$options = OU_Options::FromArray(
				$options,
				array(
					"resample" => false,
					"max_original" => false
				)
			);
				
			$x1 = $width / $this->width();
			$x2 = $height / $this->height();
			
			$d = min($x1, $x2);
			if ($options->max_original) $d = min(1, $d);
			
			$w = $this->width() * $d;
			$h = $this->height() * $d;
			
			$w = round($w);
			$h = round($h);
			
			if ($this->SaveAlpha())
				$img = self::createAlpha($w, $h);
			else
				$img = self::createTrueColor($w, $h);
				
			if ($options->resample)
				imagecopyresampled($img->_img, $this->_img, 0, 0, 0, 0, $w, $h, $this->width(), $this->height());
			else
				imagecopyresized($img->_img, $this->_img, 0, 0, 0, 0, $w, $h, $this->width(), $this->height());
			
			return $img;
		}
		
		/**
		 * @param number $left
		 * @param number $top
		 * @param number $width
		 * @param number $height
		 * @param Array|OU_Options $options
		 * @return OU_Image
		 */
		public function Copy($left, $top, $width, $height, $options = array())
		{
			$options = OU_Options::FromArray(
				$options,
				array(
					"resample" => false
				)
			);
			
			if ($this->SaveAlpha())
				$img = self::createAlpha($width, $height);
			else
				$img = self::createTrueColor($width, $height);
			
			if ($options->resample)
				imagecopyresampled($img->_img, $this->_img, 0, 0, $left, $top, $width, $height, $width, $height);
			else
				imagecopyresized($img->_img, $this->_img, 0, 0, $left, $top, $width, $height, $width, $height);
			
			return $img;
			
		}
		
		/**
		 * @param number $width
		 * @param number $height
		 * @param number $options
		 * @return OU_Image
		 */
		public function ResizeCropAspectRatio($width, $height, $options = array())
		{

			$options = OU_Options::FromArray(
				$options,
				array(
					"resample" => false,
					"callback" => false,
					"valign" => "center"
				)
			);
				
			$x1 = $width / $this->width();
			$x2 = $height / $this->height();
			
			$d = max($x1, $x2);
			
			$w = $this->width() * $d;
			$h = $this->height() * $d;
			
			$l = ($width - $w) / 2;
			
			switch ($options->valign)
			{
				case "top":
					$t = 0;
					break;
				case "bottom":
					$t = ($height - $h);
					break;
				case "middle":
				case "center":
				default:
					$t = ($height - $h) / 2;
					break;
			}
			
			
			$w = round($w);
			$h = round($h);
			$l = round($l);
			$t = round($t);
			
			if ($options->callback)
				call_user_func_array($options->callback, array(&$l, &$t, &$w, &$h));
			
			if ($this->SaveAlpha())
				$img = self::createAlpha($width, $height);
			else
				$img = self::createTrueColor($width, $height);
				
			if ($options->resample)
				imagecopyresampled($img->_img, $this->_img, $l, $t, 0, 0, $w, $h, $this->width(), $this->height());
			else
				imagecopyresized($img->_img, $this->_img, $l, $t, 0, 0, $w, $h, $this->width(), $this->height());
			
			return $img;
		}
		
		/**
		 * 
		 * @param number $filtertype
		 * @param number $arg1
		 * @param number $arg2
		 * @param number $arg3
		 * @param number $arg4
		 */
		public function Filter ( $filtertype , $arg1 = null , $arg2 = null , $arg3 = null , $arg4 = null )
		{
			imagefilter($this->_img, $filtertype, $arg1, $arg2, $arg3, $arg4);
		}
		
		/**
		 * @param OU_Image $img
		 * @param number $x
		 * @param number $y
		 * @param array $options
		 */
		public function Draw($img, $x = 0, $y = 0, $options = array())
		{
			$img->DrawTo($this, $x, $y, $options);
		}
		
		/**
		 * @param string $text
		 * @param number|OU_Color $color
		 * @param string $fontfile
		 * @param number $x
		 * @param number $y
		 * @param number $size
		 * @param number $angle
		 */
		public function TtfText($text, $color, $fontfile = "Arial.ttf", $x = 0, $y = 0, $size = 12, $angle = 0)
		{
			if (is_object($color) && ($color instanceof OU_Color))
				$color = $this->colorAllocate($color);
			imagettftext($this->_img, $size, $angle, $x, $y, $color, $fontfile, $text);
		}
		
		public function TtfWordWrap($text, $color, $width, $height, $fontfile = "Arial.ttf", $x = 0, $y = 0, $size = 12, $angle = 0)
		{
			$img = $this->createAlpha($width, $height);
			$a = explode(" ", $text);
			
			$line_height = $size * 1.5;
			
			$sx = 0;
			$sy = 0;
			
			foreach ($a as $v)
			{
				$t = $v . " ";
				$b = $img->TtfBBox($t, $fontfile, $size, $angle);
				
				// Line bread
				if ($sx - $b["left"] + $b["width"] > $width)
				{
					$sx = 0;
					$sy += $line_height;
				}
				
				$tx = $sx + $b["left"];
				$ty = $sy + $line_height;
				
				$img->TtfText(
					$t, 
					$color, 
					$fontfile, 
					$tx, 
					$ty, 
					$size, 
					$angle
				);
				
				$sx += $b["width"];
				
			}
			
			$this->Draw($img, $x, $y);
			unset($img);
			
		}
		
		public function TtfBBox($text, $fontfile = "Arial.ttf", $size = 12, $angle = 0)
		{
			$r = imagettfbbox($size, $angle, $fontfile, $text);
			$a = array();
			$a["width"] = min($r[2], $r[4]) - min($r[0], $r[6]);
			$a["height"] = min($r[1], $r[3]) - min($r[5], $r[7]);
			$a["left"] = -min($r[0], $r[6]);
			$a["top"] = -min($r[5], $r[7]);
			return $a;
		}
		
		/**
		 * @param OU_Image $img
		 * @param number $x
		 * @param number $y
		 */
		public function DrawTo($img, $x = 0, $y = 0, $options = array())
		{
			$options = OU_Options::FromArray(
				$options,
				array(
					"resample" => false
				)
			);
			
			if ($options->resample)
				imagecopyresampled($img->_img, $this->_img, $x, $y, 0, 0, $this->width(), $this->height(), $this->width(), $this->height());
			else
				imagecopyresized($img->_img, $this->_img, $x, $y, 0, 0, $this->width(), $this->height(), $this->width(), $this->height());
		}
		
		
		
	}

?>