<?php

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 29/06/2012
	 */
	 
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_data_values.php 320 2013-10-17 10:56:20Z xaguilarf $
	 */
	class OU_Data_Values extends OU_Base
	{
		
		public $isModified = false;
		protected $values = array();
		
		/**
		 * Devuelve los valores en una array
		 * @return array 
		 */
		public function toArray()
		{
			return $this->values;
		}
		
		/**
		 * Añade una array a la actual.
		 * @param unknown_type $array
		 */
		public function AddArray($array)
		{
			foreach ($array as $k=>$v)
				$this->Add($k, $v);
		}
		
		/**
		 * Añade un nuevo valor a la array
		 * @param mxied $name
		 * @param mxied $value
		 */
		public function Add($name, $value)
		{
			$this->{$name} = $value;
		}
		
		/**
		 * Borra un valor en la array
		 * @param unknown_type $name
		 */
		public function Remove($name)
		{
			unset($this->{$name});
		}
		
		public function __get ( $name )
		{
			if (isset($this->values[$name]))
				return $this->values[$name];
			else {
				if (method_exists ( $this, "get" . ucfirst ( $name ) ))
					return parent::__get($name);
			}
		}
		
		public function __set ( $name, $value )
		{
			if (!isset($this->values[$name]) || $this->values[$name] !== $value)
			{
				$this->values[$name] = $value;
				$this->DoChange($name);
			}
		}
		
		public function __isset( $name )
		{
			return isset($this->values[$name]);
		}
		
		public function __unset($name)
		{
			if (isset($this->values[$name]))
			{
				unset($this->values[$name]);
				$this->DoChange($name);
			}
		}
		
		/**
		 * Se ejecuta quando se hayan realizado canvios en las propiedades (elementos del array)
		 * @param unknown_type $propName
		 */
		protected function DoChange($propName = false)
		{
			$this->isModified = true;
			$this->raise("modified", array(true));
		}
		
	}
	
?>