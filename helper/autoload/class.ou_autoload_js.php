<?php 

	/**
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 08/04/2013
	 */
	 
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_autoload_js.php 340 2014-03-16 23:37:36Z xaguilarf $
	 * @subpackage	OU Autoload Helper
	 */
	class OU_Autoload_Js extends OU_Autoload_Base
	{
		
		/**
		 * @param OU_Autoload_Controller $controller
		 * @return OU_Path_Instance|NULL
		 */
		protected static function getFilename($controller)
		{
			
			$pathJs = OU_Path::Create($controller->app->path() . OU_Config::$path_js);
			$globalPathJs = OU_Path::Create($controller->app->globalBasePath() . OU_Config::$path_js);
			$path = $controller->path();
			
			if ($path->in($pathJs))
			{
				if (strtolower($path->ext) == ".js")
				{
					if ($path->exists) return $path;
					else
					{
						$path->ext = ".compile.js";
						if ($path->exists)
							return $path;
					}
				}
					
			}
			
			return NULL;
			
		}
		
		/**
		 * @param OU_Autoload_Controller $controller
		 */
		public static function execute($controller)
		{
			
			$path = self::getFilename($controller);
			
			if ($path->hasSuffix(".compile.js"))
			{

				$opath = $path;
					
				OU_Header::AutoCache();
				OU_Header::ContentType ( "text/javascript; charset=UTF-8" );
		
				
				$compiler = new OU_Compiler_Js($path->Correct());
				
				global $global_sitename;
				$global_sitename = $controller->app->appName;
					
				$compiler->on("importFile", 
					function($sender, $fileName, &$data, $children){
				
						global $global_sitename;
						$sitename = $global_sitename;
						
						if (!$sender->useSmarty) return;
						
						// Smarty Config
						OU_Utils_Smarty::setDelimiter ( "{:", "}" );
						OU_Utils_Smarty::setConfigPath ( OU_App::app ( $sitename )->path () . "/" . OU_App::$_dir_config );
						
						// Default vars
						OU_Utils_Smarty::assign(
							array(
								"base_uri" => OU_App::app($sitename)->baseURI()
							)
						);
						
						$compiler = new OU_Compiler_Tpl($fileName);
						$data = $compiler->compile();
						
						// Smarty Restore Config
						OU_Utils_Smarty::restoreDelimiter ();
				
					}
				);
					
				$data = $compiler->compile();
				
				echo $data;
					
			}else{
				
				OU_Header::File($path, false); //, !OU_Config::$development);
			}
			
		}
		
		/**
		 * @param OU_Autoload_Controller $controller
		 * @return boolean
		 */
		public static function check($controller)
		{
			return self::getFilename($controller) ? true : false;
		}
		
	}

?>