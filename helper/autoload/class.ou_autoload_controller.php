<?php 

	/**
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 08/04/2013
	 */

	OU_Config::IncClassHelper("OU_Autoload_Base");
	 
	/**
	 * @property-read OU_App $app
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_autoload_controller.php 340 2014-03-16 23:37:36Z xaguilarf $
	 * @subpackage	OU Autoload Helper
	 */
	class OU_Autoload_Controller extends OU_Base
	{

		private $_app;
		public function __construct($app)
		{
			$this->_app = $app;
		}
		
		// public $app
		protected function getApp(){ return $this->_app; }
		
		public function path()
		{
			if (preg_match("/^(\/app|\/)(?<sitename>.*?)(?<uri>\/.*)$/", $_SERVER["REQUEST_URI"], $m))
			{
				
				// Permet direccions a traves de la carpeta app i directes.
				
				$path = OU_Path::Create($_SERVER["REQUEST_URI"]);
				$rel = OU_Path::Create($rel = OU_Path::Absolute(OU_Config::$path_apps_d, $this->_app->rootURI(false)) . "/" . $this->_app->appName);

				if ($path->in($rel))
                {
					$path = $path->relative($rel);
                    return OU_Path::Create($this->_app->basePath() . "/" . $this->_app->appName . "/" . $path->path);
                }
				else {
					$globalPath = $this->_app->globalBaseURI(false);
					if ($path->in($globalPath))
					{
						$path = $path->relative($globalPath);
						return OU_Path::Create($this->_app->globalBasePath() . "/" . $path->path);
					}
					else
						$path = $path->relative($rel = OU_Path::Create($this->_app->rootURI(false)));
				}

				return OU_Path::Create($this->_app->basePath() . "/" . $path->path);
			}
			else
				return OU_Path::Create($this->_app->basePath() . "/" . $_SERVER["REQUEST_URI"]);

		}

		/**
		 * @param OU_App $app
		 */
		public function execute()
		{

			$autoloads = array(
				"OU_Autoload_Css",
				"OU_Autoload_Js",
				"OU_Autoload_File"
			);

			foreach ($autoloads as $autoload)
			{
				OU_Config::IncClassHelper($autoload);
				if ($autoload::check($this))
				{
					call_user_func(array($autoload, "execute"), $this);
					return true;
				}
			}	
			
			return false;
			
		}		
		
	}

?>