<?php 

	/**
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 08/04/2013
	 */
	 
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_autoload_base.php 240 2013-05-23 11:21:29Z xaguilarf $
	 * @subpackage	OU Autoload Helper
	 */
	abstract class OU_Autoload_Base extends OU_Base
	{
		
		/**
		 * @param OU_Autoload_Controller $controller
		 * @return OU_Path_Instance
		 */
		public static function path($controller)
		{
			if (preg_match("/^(\/app|\/)(?<sitename>.*?)(?<uri>\/.*)$/", $_SERVER["REQUEST_URI"], $m))
			{
				$rel = OU_Path::Relative( $_SERVER["REQUEST_URI"], $controller->app->rootURI(false) . $controller->app->appName);
				return OU_Path::Create($controller->app->basePath() . "/" . $controller->app->appName . "/" . $rel);
			}
			else
				return OU_Path::Create($controller->app->basePath() . "/" . $_SERVER["REQUEST_URI"]);
		}
		
		public static function execute($controller)
		{
			return false;
		}
		
		/**
		 * Detecta si este "autoload" se debe ejecutar.
		 * @param OU_App $app
		 * @return boolean
		 */
		public static function check($controller)
		{
			return false;
		}
		
	}

?>