<?php 

	/**
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 08/04/2013
	 */
	 
	/**
	 * 
	 * Controla el acceso a todos los archivos no especificos en otro "autoload"
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_autoload_file.php 240 2013-05-23 11:21:29Z xaguilarf $
	 * @subpackage	OU Autoload Helper
	 */
	class OU_Autoload_File extends OU_Autoload_Base
	{
		
		/**
		 * @param OU_Autoload_Controller $controller
		 * @return boolean
		 */
		public static function check($controller)
		{
			
			$p = $controller->path();

			if ($p->isFile)
			{
				switch (strtolower($p->ext))
				{
					case ".php":
					case ".inc":
					case ".tpl":
					case ".conf":
					case ".cnf":
					case ".hsaccess":
					case ".htaccess":
					case ".project":
					case ".buildpath":
					case ".lang":
						return false;
				}
				return true;
			}
			return false;			
			
		}
		
		/**
		 * @param OU_Autoload_Controller $controller
		 * @return boolean
		 */
		public static function execute($controller)
		{
			OU_Header::File($controller->path()->Correct());

		}
		
	}

