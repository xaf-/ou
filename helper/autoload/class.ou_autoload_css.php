<?php 

	/**
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 08/04/2013
	 */
	 
	/**
	 * 
	 * Controla el acceso a los archivos .css, .less.
	 * Si la extensión del archivo es:
	 * <ul>
	 * 	<li>.less, .less.css : Se compila con OU_Compiler_Less</li>
	 * 	<li>.scss, .scss.css : Se compila con OU_Compiler_Scss</li>
	 * 	<li>.css : Se muestra el archivo original</li>
	 * </ul>
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_autoload_css.php 338 2013-11-26 12:01:16Z xaguilarf $
	 * @subpackage	OU Autoload Helper
	 * 
	 * @see OU_Compiler_Less
	 * @see OU_Compiler_Scss
	 * 
	 */
	class OU_Autoload_Css extends OU_Autoload_Base
	{
		
		/**
		 * @param OU_Autoload_Controller $controller
		 * @return OU_Path_Instance|NULL
		 */
		protected static function getFilename($controller)
		{

			OU_Config::IncClassHelper("OU_Compiler_Scss");

			$pathCss = OU_Path::Create($controller->app->path() . OU_Config::$path_css);
			$path = $controller->path();
			if ($path->in($pathCss))
			{

				switch (strtolower($path->ext))
				{
					case ".css":
						if ($path->exists) return $path;
						else
						{

							$path->ext = ".less";
							if ($path->exists) return $path;
							else{
								$path->ext = ".scss";

								if ($path->exists) return $path;
							}
						}
						break;
					// case ".less.css":
					// case ".scss.css":
						// if ($path->exists) return $path;
						
				}
					
			}
			
			return NULL;
			
		}

		/**
		 * @param OU_Autoload_Controller $controller
		 * @return bool|void
		 */
		public static function execute($controller)
		{
			
			$path = self::getFilename($controller);
			
			if ($path->hasSuffix(".scss") || $path->hasSuffix(".scss.css"))
			{
				$opath = $path;
				
				OU_Header::AutoCache();
				OU_Header::ContentType ( "text/css" );
					
				// Smarty Config
				OU_Utils_Smarty::setConfigPath ( $controller->app->path () . "/" . OU_App::$_dir_config );
					OU_Utils_Smarty::assign(
					array(
						"_REQUEST" => $_REQUEST,
						"_GET" => $_GET,
						"_POST" => $_POST,
						"base_uri" => $controller->app->baseURI()
					)
				);

				//$compiler = new OU_Compiler_Tpl($path->Correct());
				//$compiler->compile();
				$compiler = new OU_Compiler_Scss($path->Correct());
				echo $compiler->compile();
				
			}else if ($path->hasSuffix(".less") || $path->hasSuffix(".less.css"))
			{
				$opath = $path;
					
				OU_Header::AutoCache();
				OU_Header::ContentType ( "text/css" );
				
				// Smarty Config
				OU_Utils_Smarty::setConfigPath ( $controller->app->path () . "/" . OU_App::$_dir_config );
				OU_Utils_Smarty::assign(
					array(
						"_REQUEST" => $_REQUEST,
						"_GET" => $_GET,
						"_POST" => $_POST,
						"base_uri" => $controller->app->baseURI()
					)
				);
				
				
				$compiler = new OU_Compiler_Tpl($path->Correct());
				$compiler->compile();
				
				$compiler = new OU_Compiler_Less($compiler->cachePath(), $compiler);
				echo $compiler->compile();
				
			}else{
				OU_Header::File($path->Correct(), false);//, !OU_Config::$development);
			}
			
			
		}
		
		/**
		 * @param OU_Autoload_Controller $controller
		 * @return boolean
		 */
		public static function check($controller)
		{
			return self::getFilename($controller) ? true : false;
		}
		
	}

?>