<?php 

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 24/10/2012
	 */

	OU_Config::IncClass(
		array(
			"OU_Base",
			"OU_Array"
		)
	);
	
	/**
	 * 
	 * Clase que controla la base de los módulos.
	 * 
	 * @property OU_Module_Manager $manager
	 * @property string $name
	 * @property string $path
	 *
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @subpackage	OU Module  Helper
	 * @version		$Id$
	 */
	class OU_Module_Item extends OU_Base
	{

        private $_initialized = false;
		
		private $_manager;
		private $_path;
		private $_name;

        /**
         * @return OU_Module_Manager
         */
		public function getManager() { return $this->_manager; }
		public function setManager($v) { $this->_manager = $v; }

		public function getName() { return $this->_name; }
		public function setName($v) { $this->_name = $v; }

		public function getPath() { return $this->_path; }
		public function setPath($v) { $this->_path = $v; }

		public function __construct($name, $path)
		{
			$this->_name = $name;
			$this->_path = $path;
		}

		public function init(){
            if ($this->_initialized) return;
            $this->_initialized = true;
            $this->getManager()->doItemInit($this);
        }

		
	}

