<?php

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 03/03/2016
	 */

	OU_Config::IncClass(
		array(
			"OU_Base"
		)
	);
	
	/**
	 * Gestiona los módulos de la aplicación
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @subpackage	OU Module Helper
	 * @version		$Id$
	 */
	class OU_Module_Manager extends OU_Base implements Iterator, ArrayAccess, Countable
	{

        /**
         * @var Closure
         */
        public $onregistermodule = null;
		/**
		 * @var OU_Module_Item[]
		 */
		private $_modules = array();
        /**
         * @var OU_App
         */
        private $_app;

        /**
         * OU_Module_Manager constructor.
         * @param OU_App $app
         */
		public function __construct($app)
		{
            $this->_app = $app;
		}
		
		/* Iterator */
		private $_icurrent = 0;
		public function current () { return $this->_modules[$this->_icurrent]; }
		public function next () { ++$this->_icurrent; }
		public function key () { return $this->_modules[$this->_icurrent]->name; }
		public function valid () { return isset($this->_modules[$this->_icurrent]); }
		public function rewind () { $this->_icurrent = 0; }

		/* ArraAccess */
		public function offsetExists ($offset) { return $this->indexOf($offset) > -1; }
		public function offsetGet ($offset) { return $this->getByName($offset); }

		/**
		 * @param OU_Module_Base $value
		 * @see ArrayAccess::offsetSet()
		 */
		public function offsetSet ($offset, $value) {
			$value->setManager($this);
			if (($num = $this->indexOf($offset)) > -1)
				$this->_modules[$num] = $value;
			else
				$this->_modules[] = $value;
		}
		public function offsetUnset ($offset) { unset($this->_modules[$this->indexOf($offset)]); }
		
		/* Countable */
		public function count () { return count($this->_modules); }
		
		/* ---------- */
		
		/**
		 * @param string $name
		 * @return number
		 */
		private function indexOf($name)
		{
			foreach ($this->_modules as $k => $v)
			{
				if ($v->name == $name)
				{
					return $k;
				}
			}
			return -1;
		}

		/**
		 * @param string $name
		 * @return OU_Module_Base|NULL
		 */
		private function getByName($name)
		{
			if (($num = $this->indexOf($name)) > -1)
				return $this->_modules[$num];
			else
				return null;
		}
		
		/**
		 * @return OU_Module_Base
		 */
		public function Last()
		{
			return $this->_modules[count($this->_modules) - 1];
		}

        /**
         * @param $name
         * @return null|OU_Module_Item
         */
		function fromName($name)
		{
			foreach ($this->_modules as $m){
				/* @var $m OU_Module_Item */
				if ($m->name == $name)
					return $m;
			}
			return null;
		}

		public function Register($name, $path = false){

            OU_Config::IncClassHelper("OU_Module_Item");

			if ($path === false){
				$path = OU_App::app()->modulePath() . "/" . $name;
			}

			$module = new OU_Module_Item($name, $path);
			$module->setManager($this);
			$this->_modules[] = $module;

			$config = $module->path . "/config/config.php";
			if (file_exists($config)) require_once($config);

            if ($this->getApp()->isInitialized())
                $module->init();
		}

		public function initAll(){
            foreach ($this->getModules() as $module){
                $module->init();
            }
        }

        /**
         * @param OU_Module_Item $item
         */
		public function doItemInit($item){
            if ($this->onregistermodule) {
                call_user_func($this->onregistermodule, $item);
            }
        }

        /**
         * @return OU_App
         */
        public function getApp()
        {
            return $this->_app;
        }

        /**
         * @return OU_Module_Item[]
         */
        public function getModules()
        {
            return $this->_modules;
        }

    }

