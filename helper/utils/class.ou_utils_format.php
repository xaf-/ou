<?php 

	/**
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 09/07/2013
	 */
	 
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: org.eclipse.php.ui.prefs 240 2013-05-23 11:21:29Z xaguilarf $
	 *
	 */
	class OU_Utils_Format extends OU_Base
	{
		
		private static $_tmpLocale = null;
		private static $_tmpLocaleInfo = null;
		private static function localeInfo()
		{
			if (self::$_tmpLocale !== ($l = setlocale(LC_ALL, 0)))
			{
				self::$_tmpLocale = $l;
				self::$_tmpLocaleInfo = OU_Array::FromArray(localeconv()); 
			}
			return self::$_tmpLocaleInfo;			
		}
		
		public static function bytes($value, $options = array())
		{
			
			$options = OU_Options::FromArray(
				$options,
				array(
					"precision" => 2
				)
			);
			
			$units = array('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
			$bytes = max($value, 0);
			$pow = floor(($bytes ? log($bytes) : 0) / log(1024));
			$pow = min($pow, count($units) - 1);
			$bytes /= pow(1024, $pow);
			return round($bytes, $options->precision) . ' ' . $units[$pow];
			
		}
		
		public static function time($value)
		{
			return strftime("%H:%M:%S", $value);
		} 
		
		public static function seconds($value, $options = array())
		{
			$options = OU_Options::FromArray(
				$options,
				array(
					"precision" => 2
				)
			);
			
			$start = mktime(0,0,0);
			
			$secs = intval(floor($value));
			if ($options->precision != null) {
				$value = round($value, $options->precision);
			}

			$secs = floor($value);
			$dec = $value - $secs;

			if ($start + $value > $start + (60 * 60))
				$str = strftime("%H:%M:%S", $start + $secs);
			else
				$str = strftime("%M:%S", $start + $secs);
			
			if ($dec != 0) $str .= trim($dec, "0");
						
			return $str;
		} 
		
		public static function minify($html)
		{
		    
		    OU_Config::IncExt("minifier");
		    	
		    if (!trim($html, " \n\r"))
		        return "";
		
		    return Minifier::minify($html, array("xhtml" => true));
		
		    $reg = '!<script.*>.*?</script>!';
		    preg_match_all($reg,$html,$script);
		    $html = preg_replace($reg, '#xscript#', $html);
		
		    $html = preg_replace('#<!�[^\[].+�>#', '', $html);
		    $html = preg_replace('/[\r\n\t]+/', ' ', $html);
		    $html = preg_replace('/>[\s]+</', '> <', $html);
		    $html = preg_replace('/[\s]+/', ' ', $html);
		
		
		    if (!empty($script[0])) {
		        foreach ($script[0] as $tag) {
		            $html = preg_replace('!#xscript#!', $tag, $html,1);
		        }
		    }
		
		    // echo $html; ????
		    return $html;
		}
		
	}

?>