<?php 

	/**
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 28/10/2013
	 */
	 
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: org.eclipse.php.ui.prefs 240 2013-05-23 11:21:29Z xaguilarf $
	 *
	 */
	class OU_Utils_Unit extends OU_Base
	{
		
		/*
		const UNIT_PX = "px";
		const UNIT_KM = "km";
		const UNIT_CM = "km";
		const UNIT_MM = "km";
		const UNIT_DM = "km";
		const UNIT_M = "m";
		*/
		
		public static function MilleToKm($value)
		{
			return $value * 1609.344;
		}
		
		public static function KmToMille($value)
		{
			return $value / 1609.344;
		}
		
		public static function PX2CM($value, $dpi = 96)
		{
			return $value * 2.54 / $dpi;
		}
		
		public static function CM2PX($value, $dpi = 96)
		{
			return $value * $dpi / 2.54;
		}
		
	}
	
	

?>