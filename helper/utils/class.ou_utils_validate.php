<?php

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 29/06/2012
	 */

	OU_Config::IncClass("OU_Base");
	 
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @subpackage	OU Utils Helper
	 * @version		$Id: class.ou_utils_validate.php 312 2013-09-19 10:58:02Z xaguilarf $
	 */
	class OU_Utils_Validate extends OU_Base
	{
		
		/**
		 * Completa la información de validación.
		 * @param OU_Array|string $validateInfo
		 * @param string $fieldName
		 * @return OU_Array
		 */
		public static function GetValidateInfo($validateInfo, $fieldName)
		{
			
			if (is_string($validateInfo))
			{
				$from = new OU_Array();
				$from->Add("validateType", $validateInfo);
			}else{
				$from = OU_Array::FromArray($validateInfo);
			}
			
			if (!isset($from->validateValue)) $from->Add("validateValue", false);
			if (!isset($from->canEmpty)) $from->Add("canEmpty", false);
			if (!isset($from->fieldName)) $from->Add("fieldName", $fieldName);
			if (!isset($from->displayName)) $from->Add("displayName", ucfirst($fieldName));
			
			return $from;
			
		}
		
		private static function _AddValidator($msg, $value, $fieldName, $validator, $msgType = "error")
		{
			
			if ($validator instanceof OU_Options)
				$validator->Add(
					array(
						"msg" => $msg,
						"fieldName" => $fieldName,
						"value" => $value,
						"type" => $msgType
					)
				);
			else
				if ($validator instanceof OU_Array)
					$validator->Add($msg);
				
		}
		
		private static function _Validate($value, $validateType, $displayName, $validator = null)
		{

			$alias = array(
					"notEmpty" 		=> "ValidNotEmpty",
					"isNotEmpty" 	=> "ValidNotEmpty",
					"isInteger" 	=> "ValidInt",
					"isInt" 		=> "ValidInt",
					"isFloat"		=> "ValidFloat",
					"isNumber" 		=> "ValidNumber",
					"isNum" 		=> "ValidNumber",
					"isUsername" 	=> "ValidUsername",
					"isUser" 		=> "ValidUsername",
					"isPassword" 	=> "ValidPassword",
					"isPass" 		=> "ValidPassword",
					"isPersonName" 	=> "ValidPersonName",
					"isDate" 		=> "ValidDate",
					"isStrDate" 	=> "ValidStrDate",
					"isEmail" 		=> "ValidEmail",
					"isPhone" 		=> "ValidPhone"
			);
			
			if (isset($alias[$validateType]))
			{
				$fnc = $alias[$validateType];
				return call_user_func_array("OU_Utils_Validate::" . $fnc, array($value, $displayName, $validator));
			}else{
				throw new Exception("El tipo de validación '".addslashes($validateType)."' no es válido.");
			}
		}
		

		/**
		 * @param OU_Array $validator
		 */
		public static function Validate($value, $fieldName, $validateInfo, $validator = null)
		{

			if (is_string($validateInfo) && strpos($validateInfo, ",") !== false)
				$validateInfo = explode(",", $validateInfo);
			
			if (is_array($validateInfo) && !isset($validateInfo["validateType"]))
			{
				$b = true;
				foreach ($validateInfo as $v)
                {
					$b = $b && static::Validate($value, $fieldName, $v, $validator);
                }
				return $b;
			}
			
			if ($validator instanceof  OU_Options)
				$newValidator = new OU_Options();
			else
				$newValidator = new OU_Array();

			$info = self::GetValidateInfo($validateInfo, $fieldName);

			if ($info->validateValue) $value = $info->validateValue;
			
			if ($info->canEmpty && !self::ValidNotEmpty($value))
			{
				return true;
			}

			$validateType = $info->validateType;
			
			if ((is_object($validateType) && $validateType instanceof OU_Array))
				$validateType = $validateType->toArray();

			if (is_array($validateType))
			{
				$b = true;
				foreach ($validateType as $v)
				{
					$b2 = self::_Validate($value, $v, $info->displayName, $newValidator);
					$b = $b2 && $b;
					if (!$b2) break; // Si se produce un error no comprueba las siguientes reglas de validación.
				}
				
			}else{
				$b = self::_Validate($value, $validateType, $info->displayName, $newValidator);
			}
			
			
			$cnt = 0;
			foreach ($newValidator->toArray() as $v)
			{
				
				$cnt++;
				$validator->Add($info->fieldName . "_" . $cnt, $v);
			}
			
			return $b;
			
		}
		
		public static function IsComposedChars($chars, $value)
		{
			$cnt = 0;
			$keys = array();
			for ($i = 0; $i < count($chars); $i++)
			{
				$char = $chars[$i];
				if (!isset($keys[$char]))
				{
					$keys[$char] = true;
					$cnt += substr_count($value, $char);
				}
			}
			return $cnt === strlen($value);
		}
		
		public static function ValidUsername($value, $fieldName = "", $validator = null)
		{
			$valid = preg_match('/^[A-Za-z0-9]+$/', $value);
			if ($valid)
				$valid = strlen($value) >= 3;
			if (!$valid && $validator)
				self::_AddValidator("El campo $fieldName debe estar compuesto por letras y números, y tener como mínimo 3 caracteres.", $value, $fieldName, $validator);
			return $valid;
		}
		
		public static function ValidPassword($value, $fieldName = "", $validator = null)
		{
			$valid = strlen($value) >= 5;
			if (!$valid && $validator)
				self::_AddValidator("El campo $fieldName debe tener como mínimo 5 caracteres.", $value, $fieldName, $validator, "error");
			return $valid;
		}
		
		public static function ValidPersonName($value, $fieldName = "", $validator = null)
		{
			$valid = preg_match('/^[\.\'\s\w\-\º\ªàèìòùáéíóúäëïöüâêîôûñç]+$/', mb_strtolower($value));
			if (!$valid && $validator)
				self::_AddValidator("El campo $fieldName no es válido. Debe estar compuesto por letras y espacios.", $value, $fieldName, $validator, "error");
			
			return $valid;
		}
		
		public static function ValidDate($value, $fieldName = "", $validator = null)
		{
			$b = $value != 0;
			if (!$b && $validator)
				self::_AddValidator("El campo $fieldName no tiene una fecha válida. Ej.: 31/12/2012.", $value, $fieldName, $validator, "error");
			return $b;
		}
		
		public static function ValidNotEmpty($value, $fieldName = "", $validator = null)
		{
			$b = empty($value) || trim($value) == "";
			if ($b && $value !== "0") // empty fa que "0" sigui true
			{
				if ($validator) self::_AddValidator("El campo $fieldName es obligatorio.", $value, $fieldName, $validator, "error");
				return false;
			}
			return true;
		}
		
		public static function ValidStrDate($value, $fieldName = "", $validator = null)
		{
			$separator_type= array(
					"/"
			);
			$separator_used = false;
			foreach ($separator_type as $separator) {
				$find= stripos($value,$separator);
				if($find<>false){
					$separator_used= $separator;
				}
			}
			$b = $separator_used;
			if ($b)
			{
				$input_array= explode($separator_used,$value);
				$b = checkdate($input_array[1],$input_array[0],$input_array[2]);
			}
			if (!$b && $validator)
				self::_AddValidator("El campo $fieldName tiene un formato incorrecto. Ej.: 31/12/2012.", $value, $fieldName, $validator, "error");
			return $b;
		}
		
		public static function ValidEmail($email, $fieldName = "", $validator = null)
		{
			
			$valid = true;
			$findats = strrpos($email, "@");
			if (is_bool($findats) && !$findats) {
				$valid = false;
			}
			else {
				$domain = substr($email, $findats+1);
				$local = substr($email, 0, $findats);
				$locallength = strlen($local);
				$domainlength = strlen($domain);
				if ($locallength < 1 || $locallength > 64) {
					$valid = false;
				}
				elseif ($domainlength < 1 || $domainlength > 256) {
					$valid = false;
				}
				elseif ($local[0] == '.' || $local[$locallength-1] == '.') {
					$valid = false;
				}
				elseif ((preg_match('/\\.\\./', $local)) || (preg_match('/\\.\\./', $domain))) {
					$valid = false;
				}
				elseif (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain)) {
					$valid = false;
				}
				elseif (!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\","",$local))) {
					if (!preg_match('/^"(\\\\"|[^"])+"$/', str_replace("\\\\","",$local))) {
						$valid = false;
					}
				}
				//elseif (!(checkdnsrr($domain,"MX") || checkdnsrr($domain,"A"))) {
				//	$valid = false; 
				//}
			}
			
			if (!$valid && $validator)
				self::_AddValidator("El correo electrónico es incorrecto. Ej.: ejemplo@dominio.es.", $email, $fieldName, $validator, "error");
			
			return $valid;
			
		}
		
		public static function ValidInt($value, $fieldName = "", $validator = null)
		{
			$valid = is_int($value);
			if (!$valid && $validator)
				if ($validator) self::_AddValidator("El campo $fieldName debe tener un valor numérico.", $value, $fieldName, $validator, "error");
			return $valid;
		}
		
		public static function ValidNumber($value, $fieldName = "", $validator = null)
		{
			$valid = is_numeric($value);
			if (!$valid && $validator)
				if ($validator) self::_AddValidator("El campo $fieldName debe tener un valor numérico.", $value, $fieldName, $validator, "error");
			return $valid;
		}
		
		public static function ValidFloat($value, $fieldName = "", $validator = null)
		{
			$valid = is_float($value) || is_numeric($value);
			if (!$valid && $validator)
				if ($validator) self::_AddValidator("El campo $fieldName debe tener un valor numérico.", $value, $fieldName, $validator, "error");
			return $valid;
		}
		
		public static function ValidPhone($value, $fieldName = "", $validator = null)
		{
			$valid = preg_match('/^[(\[]?\d{3}[)-\.\] ]*\d{3}[-\. ]?\d{4}$/', $value);
			if (!$valid) $valid = preg_match('/^\d+$/', trim($value)) && (($n = strlen(trim($value))) !== 0 && $n >= 9 && $n <= 11);
			if (!$valid && $validator)
				self::_AddValidator("El campo $fieldName no tiene un formato de teléfono válido. Ej: 912345678, 912.345.678.", $value, $fieldName, $validator, "error");
			return $valid;
		}
		
		
	}
	
?>