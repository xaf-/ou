<?php

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 03/07/2012
	 */

	OU_Config::IncClass(
		array(
			"OU_Base",
			"OU_Path"
		)
	);
	 
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @subpackage	OU Utils Helper
	 * @version		$Id: class.ou_utils_smarty.php 171 2013-01-30 12:04:44Z xaguilarf $
	 */
	class OU_Utils_Smarty extends OU_Base
	{
		
		/**
		 * @var Smarty
		 */
		private static $_smarty = false;
		private static $_instance = false;
		
		private static function _getInstance()
		{
			if (!self::$_instance)
			{
				self::$_instance = new OU_Utils_Smarty();
			}
			return self::$_instance;
		}
		
		public static function isCached($template = null, $cache_id = null, $compile_id = null, $parent = null)
		{
			self::_getSmarty()->isCached($template, $cache_id, $compile_id, $parent);
		}
		
		/**
		 * @return OU_Utils_Smarty
		 */
		public static function instance()
		{
			return self::_getInstance();
		}
		
		/**
		 * @return Smarty
		 */
		public static function _getSmarty()
		{
			if (!self::$_smarty)
			{
				OU_Config::IncExt("smarty");
				self::$_smarty = new Smarty();
				self::_init();
			}
			return self::$_smarty;
		}
		
		/*
		 * Plugins Smarty
		 */
		
		
		private $_ou_controls = array();
		public function ou_control_register($params, $data)
		{
			if ($data === NULL) return;
			
			$name = $params["name"];
			$this->_ou_controls[$name] = ($data);	
			
		}
		
		/**
		 * @see Smarty::configLoad()
		 * @param string $fileName
		 * @return Ambigous <Smarty_Internal_Data, Smarty>
		 */
		public static function configLoad($fileName)
		{
			return self::_getSmarty()->configLoad($fileName);
		}
		
		/**
		 * Recibe o establece las variables de configuración de Smarty.
		 * @param string|array $arg1 String para indicar el nombre de la variable. Array para establecer un listado de variables.
		 * @param mixed|null $arg2 Mixed para establecer el valor de la variable siempre que se haya especificado en el parametro $arg1 el nombre de la variable. null si se quiere leer el valor o si se pasa a $arg1 una array para establecer un listado de valores. 
		 * @return string|null 
		 */
		public static function config($arg1, $arg2 = null)
		{
			$key = $arg1; 
			if ($arg2 != null || is_array($arg1))
			{
				if (is_array($arg1))
				{
					foreach ($arg1 as $k=>$v)
						self::_getSmarty()->config_vars[$k] = $v;
				}else{
					self::_getSmarty()->config_vars[$arg1] = $arg2;
				}
				
				return;
			}
			return self::_getSmarty()->getConfigVars($key);
		}
		
		public function ou_php($params, $data)
		{
			return eval($data);
		}
		/*
		 * ************ *
		 */
		
		public static function registerPlugin($type, $tag, $callback, $cacheable = true, $cache_attr = null)
		{
			return self::_getSmarty()->registerPlugin($type, $tag, $callback);
			// return call_user_func_array(array(self::_getSmarty(), "registerPlugin"), func_get_args());
		}
		
		public static function registerFilter($type, $callback)
		{
			return self::_getSmarty()->registerFilter($type, $callback);
		}
		
		/**
		 * @deprecated
		 */
		public function ou_fullname($params)
		{
			$firstname = $params["firstname"];
			$surname = isset($params["surname"]) ? $params["surname"] : "";
			$surname_first = (isset($params["sf"]) && $params["sf"]) || (isset($params["surname_first"]) && $params["surname_first"]);
			
			$firstname = trim(ucwords(mb_strtolower($firstname)));
			$surname = trim(ucwords(mb_strtolower($surname)));
			
			if ($surname_first)
				return $surname . ($firstname && $surname ? ", " : "") . $firstname;
			else
				return $firstname . " " . $surname;
		}
		
		/**
		 * @deprecated
		 */
		public function ou_base_convert($params)
		{
			$from = isset($params["from"]) ? $params["from"] : 10;
			$to = isset($params["to"]) ? $params["to"] : 36;
			$value = $params["value"];
			$minchars = isset($params["minchars"])?$params["minchars"]:6;
			
			$r = base_convert($value, $from, $to);
			
			if ($minchars)
			{
				if  (strlen($r) < $minchars)
					$r = str_repeat("0", $minchars - strlen($r)) . $r;
			}
			$r = strtoupper($r);
			
			return $r;
		}
		
		private static function _init()
		{
		}
		
		/**
		 * @see OU_Compiler_Tpl::compile()
		 * @param string $fileName
		 * @return string
		 */
		public static function tpl($fileName, $data = null, $path = null)
		{
			$compiler = new OU_Compiler_Tpl($fileName, null, $data);
			$compiler->path_tpl = $path;
			return $compiler->compile();
		}
		
		/**
		 * @see Smarty::fetch()
		 * @param string $fileName
		 * @return string
		 */
		public static function internalTpl($fileName)
		{
			return call_user_func_array(array(self::_getSmarty(), 'fetch'), func_get_args());
		}
		
		/**
		 * @see Smarty::display()
		 * @param string $fileName
		 */
		public static function display($fileName)
		{
			return call_user_func_array(array(self::_getSmarty(), 'display'), func_get_args());
		}
		
		private static $_lastPath = false;
		public static function setPath($path)
		{
			$path = realpath($path);
			if ($path)
			{
				self::$_lastPath = self::_getSmarty()->addTemplateDir($path);
			}
		}
		
		public static function assign($args)
		{
			return call_user_func_array(array(self::_getSmarty(), "assign"), func_get_args());
		}
		
		public static function setConfigPath($path)
		{
			if (!is_dir($path))
			{
				@mkdir($path);
			}
			$path = realpath($path);
			if ($path)
			{
				self::_getSmarty()->addConfigDir($path);
			}
		}

		
		private static $_lastCachePath = false;
		public static function setCachePath($path)
		{
			if (!is_dir($path))
			{
				@mkdir($path);
				if (!is_dir($path)) return false;
			}
			
			self::$_lastCachePath = self::_getSmarty()->getCacheDir();
			self::_getSmarty()->setCacheDir($path);
			self::_getSmarty()->setCompileDir($path);
		}
		
		public static function restorePath()
		{
			if (self::$_lastCachePath !== FALSE)
			{
				self::_getSmarty()->setCacheDir(self::$_lastCachePath);
			}
			self::$_lastCachePath = false;
			self::$_lastPath = false;
		}
		
		private static $_lastStartDelimiter = false;
		private static $_lastEndDelimiter = false;
		public static function setDelimiter($start, $end)
		{
			self::$_lastStartDelimiter = self::_getSmarty()->left_delimiter;
			self::$_lastEndDelimiter = self::_getSmarty()->right_delimiter;
			self::_getSmarty()->left_delimiter = $start;
			self::_getSmarty()->right_delimiter = $end;			
		}
		
		public static function restoreDelimiter()
		{
			if (self::$_lastStartDelimiter !== FALSE)
				self::_getSmarty()->left_delimiter = self::$_lastStartDelimiter;
			if (self::$_lastEndDelimiter !== FALSE)
				self::_getSmarty()->right_delimiter = self::$_lastEndDelimiter;
			self::$_lastStartDelimiter = false;
			self::$_lastEndDelimiter = false;
		}
		
		public static function setDebugging($value)
		{
			self::_getSmarty()->debugging = $value;
		}
		
		public static function setDevelopment($value)
		{
			self::setDebugging($value);
			self::_getSmarty()->caching = !$value;
		}
		
	}

?>