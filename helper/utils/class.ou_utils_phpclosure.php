<?php

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 03/07/2012
	 */

	OU_Config::IncClass(
		array(
			"OU_Base"
		)
	);
	 
	/**
	 * @link https://developers.google.com/closure/
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @subpackage	OU Utils Helper
	 * @version		$Id: class.ou_utils_phpclosure.php 171 2013-01-30 12:04:44Z xaguilarf $
	 */
	class OU_Utils_PhpClosure extends OU_Base
	{
		
		public function __call($name, $arguments)
		{
			return call_user_func_array(array('OU_Utils_PhpClosure', $name), $arguments);
		}
		/**
		 * @var Smarty
		 */
		private static $_closure = false;
		private static $_instance = false;
		
		/**
		 * @return OU_Utils_PhpClosure
		 */
		private static function _getInstance()
		{
			if (!self::$_instance)
			{
				self::$_instance = new OU_Utils_PhpClosure();
			}
			return self::$_instance;
		}
		
		/**
		 * @return PhpClosure
		 */
		private static function _getCompiler()
		{
			if (!self::$_closure)
			{
				OU_Config::IncExt("phpClosure");
				self::$_closure = new PhpClosure();
				self::_init();
			}
			return self::$_closure;
		}
		
		private static function _init()
		{
			
		}
		
		public static function compile($fileName = false, &$recompile = false)
		{
			if ($fileName !== false)
			{
				self::_getCompiler()->clear();
				self::_getCompiler()->add($fileName);
			}
			return self::_getCompiler()->write(true, $recompile);
		}
		
		public static function compileStr($str = false)
		{
			/*
			if ($str !== false)
			{
				self::_getCompiler()->clear();
				self::_getCompiler()->addStr($str);
			}
			return self::_getCompiler()->write();
			*/
		}
		
		public static function add($fileName, $data = false)
		{
			self::_getCompiler()->add($fileName, false, $data);
			return self::_getInstance();
		}
		
		public static function addFirst($fileName, $data = false)
		{
			self::_getCompiler()->addFirst($fileName, false, $data);
			return self::_getInstance();
		}
		
		public static function addStr($str)
		{
			self::_getCompiler()->addStr($str);
			return self::_getInstance();
		}
		
		public static function display($fileName = false)
		{
			if ($fileName !== false)
			{
				self::_getCompiler()->clear();
				self::_getCompiler()->add($fileName);
			}
			self::_getCompiler()->write();
			return self::_getInstance();
		}
		
		public static function setDebug($val)
		{
			self::_getCompiler()->_debug = $val;
			return self::_getInstance();
		}
		
		public static function simpleMode()
		{
			self::_getCompiler()->simpleMode();
			return self::_getInstance();
		}
		
		public static function prettyPrint()
		{
			self::_getCompiler()->prettyPrint();
			return self::_getInstance();
		}
		
		public static function useClosureLibrary()
		{
			self::_getCompiler()->useClosureLibrary();
			return self::_getInstance();
		}
		
		public static function cacheDir($cacheDir)
		{
			self::_getCompiler()->cacheDir($cacheDir);
			return self::_getInstance();
		}
		
	}

?>