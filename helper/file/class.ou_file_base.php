<?php

	/*
	 * The source code is given as is. The author is not responsible
	 * for any possible damage done due to the use of this code.
	 * The component can be freely used in any application. The complete
	 * source code remains property of the author and may not be distributed,
	 * published, given or sold in any form as such. No parts of the source
	 * code can be included in any other component or application without
	 * written authorization of fontcolor. 23/10/2012
	 */	
	
	OU_Config::IncClass(
		array(
			"OU_Base"
		)
	);

	/**
	 * 
	 * @property string $fileName
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @subpackage	OU File Helper
	 * @version		$$Id: class.ou_file_base.php 240 2013-05-23 11:21:29Z xaguilarf $$
	 *
	 */
	abstract class OU_File_Base extends OU_Base
	{
		/**
		 * @var string
		 */
		private $_fileName;
		public function __construct($fileName)
		{
			$this->_fileName = $fileName;			
		}
		
		/**
		 * @return string
		 */
		protected function getFileName()
		{
			return $this->_fileName;
		}
		
		/**
		 * Abre el archivo indicado.
		 */
		public abstract function open();
		/**
		 * Crea un archivo nuevo.
		 */
		public abstract function create();
		
	}

?>