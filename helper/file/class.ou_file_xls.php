<?php

	/**
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 31/01/2013
	 */

	OU_Config::IncClassHelper(
		array(
			"OU_File_Workbook"
		)
	);
	
	OU_Config::IncExt("phpExcel", "v1.7.8", "PHPExcel/IOFactory.php");
	 
	/**
	 * Añade soporte para archivos XLS (Office Excel 97-2003).
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$$Id: class.ou_file_xls.php 240 2013-05-23 11:21:29Z xaguilarf $$
	 * @example		ou_file/xls.php
	 */
	class OU_File_XLS extends OU_File_Workbook
	{
		
		/**
		 * (non-PHPdoc)
		 * @see OU_File_Workbook::createIO()
		 */
		protected function createIO()
		{
			$obj = PHPExcel_IOFactory::createReader('Excel5');
			/* @var $obj PHPExcel_Reader_Excel5 */
			return $obj;
		}
		
	}

?>