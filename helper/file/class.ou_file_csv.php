<?php

	/**
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 31/01/2013
	 */

	OU_Config::IncClassHelper(
		array(
			"OU_File_Workbook"
		)
	);
	
	OU_Config::IncExt("phpExcel", "v1.7.8", "PHPExcel/IOFactory.php");
	 
	/**
	 * Añade soporte para archivos CSV.
	 * 
	 * @property string delimiter
	 * @property string enclosure
	 * @property string lineEnding
	 * @property string sheetIndex
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$$Id: class.ou_file_csv.php 240 2013-05-23 11:21:29Z xaguilarf $$
	 * @example		ou_file/csv.php
	 */
	class OU_File_CSV extends OU_File_Workbook
	{
		
		private $_delimiter = ";";
		protected function setDelimiter($value) { $this->_delimiter = $value; }
		protected function getDelimiter() { return $this->_delimiter; }
		 
		private $_enclosure = "\"";
		protected function setEnclosure($value) { $this->_enclosure = $value; }
		protected function getEnclosure() { return $this->_enclosure; }
		
		private $_lineEnding = "\r\n";
		protected function setLineEnding($value) { $this->_lineEnding = $value; }
		protected function getLineEnding() { return $this->_sheetIndex; }
		
		private $_sheetIndex = 0;
		protected function setSheetIndex($value) { $this->_sheetIndex = $value; }
		protected function getSheetIndex() { return $this->_sheetIndex; }
		
		
		/**
		 * (non-PHPdoc)
		 * @see OU_File_Workbook::createIO()
		 */
		protected function createIO()
		{
			$obj = PHPExcel_IOFactory::createReader('CSV');
			$obj->setDelimiter($this->_delimiter)
				->setEnclosure($this->_enclosure)
				->setLineEnding($this->_lineEnding)
				->setSheetIndex($this->_sheetIndex);
			return $obj;
		}
		
	}

?>