<?php

	/**
	 * The source code is given as is. The author is not responsible
	 * for any possible damage done due to the use of this code.
	 * The component can be freely used in any application. The complete
	 * source code remains property of the author and may not be distributed,
	 * published, given or sold in any form as such. No parts of the source
	 * code can be included in any other component or application without
	 * written authorization of fontcolor. 31/01/2013
	 */

	OU_Config::IncClassHelper(
		array(
			"OU_File_Base"
		)
	);

	/**
	 * @property-read \Shortcut\File $data
	 *
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 * @example		ou_file/xls.php
	 */
	class OU_File_Lnk extends OU_File_Base
	{

		/**
		 * @var \Shortcut\File
		 */
		private $_data;

		protected function getData(){ return $this->_data; }

		public function __construct($fileName)
		{
			parent::__construct($fileName);
			OU_Config::IncExt("shortcut");
			$this->_data = new \Shortcut\File();
		}

		public function save($filename){
			$this->_data->save($filename);
		}

		/**
		 *
		 */
		public function open()
		{
			$this->_data->open($this->fileName);

		}

		/**
		 *
		 */
		public function create(){

		}

	}

?>