<?php

	/**
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 31/01/2013
	 */

	OU_Config::IncClassHelper(
		array(
			"OU_File_Base"
		)
	);
	
	OU_Config::IncRel("inc/class.ou_file_workbook_cell.php", __FILE__);
	
	OU_Config::IncExt("phpWord", "v0.6.3p", "PHPWord/IOFactory.php");
	 
	/**
	 * Añade soporte para archivos DOCX.
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$$Id: class.ou_file_doc.php 240 2013-05-23 11:21:29Z xaguilarf $$
	 * @example		ou_file/docx.php
	 */
	class OU_File_Doc extends OU_File_Base
	{
		
		/**
		 * @var PHPWord
		 */
		private $_object;
		
		public function open()
		{
			$this->_object = new PHPWord();
			$this->_object->loadTemplate($this->getFileName());
		}
		
	}

?>