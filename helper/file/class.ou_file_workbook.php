<?php

	/**
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 31/01/2013
	 */

	OU_Config::IncClassHelper(
		array(
			"OU_File_Base"
		)
	);
	
	OU_Config::IncRel("inc/class.ou_file_workbook_cell.php", __FILE__);
	
	OU_Config::IncExt("phpExcel", "v1.7.8", "PHPExcel/IOFactory.php");
	 
	/**
	 * Añade soporte para archivos CSV.
	 * 
	 * @property number $columnCount
	 * @property number $rowCount
	 * @property string $html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$$Id: class.ou_file_workbook.php 240 2013-05-23 11:21:29Z xaguilarf $$
	 * @example		ou_file/csv.php
	 */
	abstract class OU_File_Workbook extends OU_File_Base
	{
		
		/**
		 * @var PHPExcel
		 */
		private $_object;
		
		/**
		 * @return PHPExcel
		 */
		public function getObject()
		{
			return $this->_object;
		}
		
		/**
		 * @return PHPExcel_IOFactory
		 */
		protected abstract function createIO();		
		
		/**
		 * (non-PHPdoc)
		 * @see OU_File_Base::open()
		 */
		public function open()
		{
			$this->_object = $this->createIO()->load($this->fileName);
		}
		
		public function create()
		{
			// $this->_object = $this->createIO()->
		}
		
		protected function getColumnCount()
		{
			return PHPExcel_Cell::columnIndexFromString($this->_object->getActiveSheet()->getHighestColumn());
		}
		
		protected function getRowCount()
		{
			return $this->_object->getActiveSheet()->getHighestRow();
		}
		
		protected function getHtml($options = array())
		{
			
			$options = OU_Options::FromArray(
				$options, 
				array(
					"style_inline" => true
				)
			);
			
			
			$html = "";
			
			if (!$options->style_inline)
			{
				$html .= "<style type=\"text/css\">" . $this->getCss() . "</style>";
			}
			
			$html .= "<table>";
			for ($y = 1; $y <= $this->rowCount; $y++)
			{
				$html .= "<tr>";
				for ($x = 0; $x < $this->columnCount; $x++)
				{
					$cell = $this->cell($x, $y);
					if (!$cell->valid) continue;
					
					$style = ""; if ($options->style_inline) $style = "style=\"".$cell->style->cssStr."\"";
					
					$html .= "<td class=\"cell_$cell->cordinate\" colspan=$cell->colspan rowspan=$cell->rowspan $style>$cell->html</td>";
				}
				$html .= "</tr>";
			}
			$html .= "</table>";
			return $html;
		}
		
		protected function getCss()
		{			
			
			$css = "";
			for ($y = 1; $y <= $this->rowCount; $y++)
			{
				for ($x = 0; $x < $this->columnCount; $x++)
				{
					$cell = $this->cell($x, $y);
					if (!$cell->valid) continue;
					$css .= ".cell_$cell->cordinate {";
					$css .= $cell->style->cssStr;
					$css .= "}";
				}
				
			}
			return $css;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see OU_File_Base::open()
		 * @return OU_File_Workbook_Cell
		 */
		public function cell($x, $y = null)
		{
			if ($y === null)
				return OU_File_Workbook_Cell::fromCord($this, $x);
			else
				return OU_File_Workbook_Cell::fromXY($this, $x, $y);
		}
		
		/**
		 * Convierte un número a string. (A,B,C,...)
		 * @param number $index
		 * @return string
		 */
		public static function indexToString($index)
		{
			return PHPExcel_Cell::stringFromColumnIndex($index);
		}
		
		/**
		 * Convierte una columna en string a número. Base zero.
		 * @param string $str
		 * @return number
		 */
		public static function stringToIndex($str)
		{
			return PHPExcel_Cell::columnIndexFromString($str) - 1;
		}
		
	}

?>