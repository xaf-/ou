<?php

	/**
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 31/01/2013
	 */

	OU_Config::IncRel(
		array(
			"style/class.ou_file_workbook_style_alignment.php",
			"style/class.ou_file_workbook_style_background.php",
			"style/class.ou_file_workbook_style_font.php",
			"style/class.ou_file_workbook_style_borders.php"
		),
		 __FILE__
	);
	 
	/**
	 * 
	 * @property OU_File_Workbook_Cell $cell
	 * 
	 * @property OU_File_Workbook_Style_Alignment $alignment
	 * @property OU_File_Workbook_Style_Borders $borders
	 * @property OU_File_Workbook_Style_Background $background
	 * @property OU_File_Workbook_Style_Font $font
	 * 
	 * @property array $css
	 * @property string $cssStr
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_file_workbook_style.php 240 2013-05-23 11:21:29Z xaguilarf $
	 */
	class OU_File_Workbook_Style extends OU_Base
	{
		
		/* Virtual properties */
		
		/**
		 * @var OU_File_Workbook_Cell
		 */
		private $_cell;
		protected function getCell() { return $this->_cell; }
		/**
		 * @var PHPExcel_Style
		 */
		private $_object;
		public function getObject() { return $this->_object; }
		
		/**
		 * @var OU_File_Workbook_Style_Alignment
		 */
		private $_alignment = null;
		public function getAlignment() { if ($this->_alignment === null) $this->_alignment = new OU_File_Workbook_Style_Alignment($this); return $this->_alignment; }
		/**
		 * @var OU_File_Workbook_Style_Borders
		 */
		private $_borders = null;
		public function getBorders() { if ($this->_borders === null) $this->_borders = new OU_File_Workbook_Style_Borders($this); return $this->_borders; }
		/**
		 * @var OU_File_Workbook_Style_Background
		 */
		private $_background = null;
		public function getBackground() { if ($this->_background === null) $this->_background = new OU_File_Workbook_Style_Background($this); return $this->_background; }
		/**
		 * @var OU_File_Workbook_Style_Font
		 */
		private $_font = null;
		public function getFont() { if ($this->_font === null) $this->_font = new OU_File_Workbook_Style_Font($this); return $this->_font; }
		
		protected function getCss(){
			return
				$this->alignment->css + 
				$this->borders->css +
				$this->background->css +
				$this->font->css;
		}
		
		
		protected function setCss($value)
		{
			$this->alignment->css = $value;
			$this->borders->css = $value;
			$this->background->css = $value;
			$this->font->css = $value;
		}
		
		public static function cssToStr($css)
		{
			$str = array();
			foreach ($css as $k => $v)
				if ($v !== null)
				$str[] = "$k : $v";
			return implode("; ", $str);
		}
		
		protected function getCssStr()
		{
			return $this->cssToStr($this->css);
		}
		
		/* ---------- */		
		
		public function __construct($cell)
		{
			$this->_cell = $cell;
			$this->_object = $this->cell->file->getObject()->getActiveSheet()->getStyle($this->cell->cordinate);
		}
		
	}

?>