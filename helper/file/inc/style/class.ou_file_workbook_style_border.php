<?php 

	/**
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 01/02/2013
	 */

	OU_Config::IncRel("class.ou_file_workbook_style_base.php", __FILE__);
	
	OU_Config::IncClass("OU_Color");
	 
	/**
	 * 
	 * @property string $color RGB (hex)
	 * @property string $style 
	 * 
	 * @property string $cssColor
	 * @property string $cssStyle
	 * @property string $cssWidth
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_file_workbook_style_border.php 240 2013-05-23 11:21:29Z xaguilarf $
	 *
	 */
	class OU_File_Workbook_Style_Border extends OU_File_Workbook_Style_Base
	{

		/**
		 * @var string
		 */
		private $_type;
		/**
		 * @var PHPExcel_Style_Border
		 */
		private $_object;
		/**
		 * @param OU_File_Workbook_Style_Borders $borders
		 * @param PHPExcel_Style_Border
		 */
		public function __construct($borders, $object, $type)
		{
			$this->_object = $object;
			$this->_type = $type;
			parent::__construct($borders->_style);
		}
		
		/* Virtual properties */
		
		protected function getColor(){ return $this->_object->getColor()->getRGB(); }
		protected function setColor($value) { return $this->_object->getColor()->setRGB($value); }
		
		protected function getStyle(){ return $this->_object->getBorderStyle(); }
		protected function setStyle($value) { return $this->_object->setBorderStyle($value); }
		
		protected function getCssColor() { return "#" . $this->color; }
		protected function setCssColor($value) { $this->color = str_replace("#", "", $value); }
		
		protected function getCssWidth() { 
			$css = $this->style;
			switch ($css)
			{
				case PHPExcel_Style_Border::BORDER_NONE: return "0px";
				case PHPExcel_Style_Border::BORDER_HAIR: return "0.5px";
				case PHPExcel_Style_Border::BORDER_MEDIUM: 
				case PHPExcel_Style_Border::BORDER_MEDIUMDASHDOT: 
				case PHPExcel_Style_Border::BORDER_MEDIUMDASHDOTDOT: 
				case PHPExcel_Style_Border::BORDER_MEDIUMDASHED: 
					return "2px";
				case PHPExcel_Style_Border::BORDER_THICK:
				default:
					return "1px";
			}
		}
		
		
		protected function getCssStyle() { 
			
			$css = $this->style;
			switch ($css)
			{
				case PHPExcel_Style_Border::BORDER_THIN: 
				case PHPExcel_Style_Border::BORDER_THICK: 
				case PHPExcel_Style_Border::BORDER_MEDIUM: 
				case PHPExcel_Style_Border::BORDER_HAIR: 
					return "solid";
					
				case PHPExcel_Style_Border::BORDER_DASHDOT:
				case PHPExcel_Style_Border::BORDER_DASHED:
				case PHPExcel_Style_Border::BORDER_MEDIUMDASHDOT:
				case PHPExcel_Style_Border::BORDER_MEDIUMDASHED:
				case PHPExcel_Style_Border::BORDER_SLANTDASHDOT:
					return "dashed";
					
				case PHPExcel_Style_Border::BORDER_DASHDOTDOT:
				case PHPExcel_Style_Border::BORDER_DOTTED:
				case PHPExcel_Style_Border::BORDER_MEDIUMDASHDOTDOT:
					return "dotted";
					
				case PHPExcel_Style_Border::BORDER_NONE: return "none";
				
				default:
					return "solid";
			}
		}
		
		protected function setCssStyle($value)
		{
			$this->style = $value;
		}
		
		protected function getCss()
		{
			return array(
				"border-".$this->_type."-color" => $this->cssColor,
				"border-".$this->_type."-style" => $this->cssStyle,
				"border-".$this->_type."-width" => $this->cssWidth
			);
		}
						
		/* ---------- */
		
	}

?>