<?php 

	/**
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 01/02/2013
	 */

	OU_Config::IncRel("class.ou_file_workbook_style_base.php", __FILE__);
	 
	/**
	 * 
	 * @property strnig $type
	 * @property string $startColor
	 * @property string $endColor
	 * @property string $color
	 * 
	 * @property string $cssStartColor
	 * @property string $cssEndColor
	 * @property string $cssColor
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_file_workbook_style_background.php 240 2013-05-23 11:21:29Z xaguilarf $
	 *
	 */
	class OU_File_Workbook_Style_Background extends OU_File_Workbook_Style_Base
	{

		/**
		 * @var PHPExcel_Style_Fill
		 */
		private $_object;
		public function __construct($style)
		{
			parent::__construct($style);
			$this->_object = $this->get_Style()->getObject()->getFill();
			//$this->_object->get
		}
		
		/* Virtual properties */

		protected function getType() { return $this->_object->getFillType(); }
		protected function setType($value) { $this->_object->setFillType($value); }
		
		protected function getStartColor() { return $this->_object->getStartColor()->getRGB(); }
		protected function setStartColor($value) { $this->_object->getStartColor()->setRGB($value); }
		
		protected function getEndColor() { return $this->_object->getEndColor()->getRGB(); }
		protected function setEndColor($value) { $this->_object->getEndColor()->setRGB($value); }
		
		protected function getColor() { return $this->getStartColor(); }
		protected function setColor($value) { $this->setStartColor($value); }
		
		protected function getCssStartColor() { return "#" . $this->startColor; }
		protected function setCssStartColor($value) { $this->startColor = str_replace("#", "", $value); }
				
		protected function getCssEndColor() { return "#" . $this->endColor; }
		protected function setCssEndColor($value) { $this->endColor = str_replace("#", "", $value); }
				
		protected function getCssColor() { return $this->cssStartColor; }
		protected function setCssColor($value) { $this->cssStartColor = $value; }
				
		/**
		 * (non-PHPdoc)
		 * @see OU_File_Workbook_Style_Base::getCss()
		 */
		protected function getCss()
		{
			
			$css = array();
			if ($this->type === PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR)
			{
				$css["background"] = "linear-gradient(to bottom, ".$this->cssStartColor." 0%, ".$this->cssEndColor." 100%);";
			}else if ($this->type === PHPExcel_Style_Fill::FILL_GRADIENT_PATH)
			{
				
			}else if ($this->type === PHPExcel_Style_Fill::FILL_SOLID)
			{
				$css["background-color"] = $this->cssStartColor;
			}
			
			return $css;
		}
		
		
		/* ---------- */
		
		public function isGradient()
		{
			return $this->type === PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR || $this->type === PHPExcel_Style_Fill::FILL_GRADIENT_PATH;
		}
		
	}

?>