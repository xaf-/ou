<?php 

	/**
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 01/02/2013
	 */

	OU_Config::IncRel("class.ou_file_workbook_style_base.php", __FILE__);
	 
	/**
	 * 
	 * @property string $color
	 * @property boolean $italic
	 * @property boolean $bold
	 * @property string $underline
	 * @property float $size
	 * @property boolean $subScript
	 * @property boolean $superScript
	 * @property boolean $through
	 * @property string $name
	 * 
	 * @property string $cssColor
	 * @property string $cssSize
	 * @property string $cssStyle
	 * @property string $cssWeight
	 * @property string $cssDecoration
	 * @property string $cssName
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_file_workbook_style_font.php 240 2013-05-23 11:21:29Z xaguilarf $
	 *
	 */
	class OU_File_Workbook_Style_Font extends OU_File_Workbook_Style_Base
	{

		/**
		 * @var PHPExcel_Style_Font
		 */
		private $_object;
		public function __construct($style, $object = null)
		{
			parent::__construct($style);
			$this->_object = $object ? $object : $this->get_Style()->getObject()->getFont();
		}
		
		/* Virtual properties */
		
		protected function getColor() { return $this->_object->getColor()->getRGB(); }
		protected function setColor($value) { return $this->_object->getColor()->setRGB($value); }
		
		protected function getItalic() { return $this->_object->getItalic(); }
		protected function setItalic($value) { $this->_object->setItalic($value); }
		
		protected function getBold() { return $this->_object->getBold(); }
		protected function setBold($value) { $this->_object->setBold($value); }
		
		protected function getUnderline() { return $this->_object->getUnderline(); }
		protected function setUnderline($value) { $this->_object->setUnderline($value); }
		
		protected function getThrough() { return $this->_object->getStrikethrough(); }
		protected function setThrough($value) { $this->_object->setStrikethrough($value); }
		
		protected function getSize() { return $this->_object->getSize(); }
		protected function setSize($value) { $this->_object->setSize($value); }
		
		protected function getSubScript() { return $this->_object->getSubScript(); }
		protected function setSubScript($value) { $this->_object->setSubScript($value); }
		
		protected function getSuperScript() { return $this->_object->getSuperScript(); }
		protected function setSuperScript($value) { $this->_object->setSuperScript($value); }
		
		protected function getName() { return $this->_object->getName(); }
		protected function setName($value) { $this->_object->setName($value); }
		
		protected function getCssSize() { return number_format($this->size, null) . "pt"; }
		protected function setCssSize($value) { str_replace("pt", "", $this->_object->setSubScript($value)); }
		
		protected function getCssColor() { return "#" . $this->color; }
		protected function setCssColor($value) { $this->color = str_replace("#", "", $value); }
		
		protected function getCssStyle() { return $this->italic ? "italic" : "normal"; }
		protected function setCssStyle($value) { $this->italic = strtolower($value) == "italic"; }
		
		protected function getCssWeight() { return $this->bold ? "bold" : "normal"; }
		protected function setCssWeight($value) { $this->italic = strtolower($value) == "bold"; }
		
		protected function getCssName() { return "'$this->name'"; }
		protected function setCssName($value) { $this->name = str_replace(array("'", '"'), "", $value); }
		
		protected function getCssDecoration() { 
			$css = "";
			if ($this->underline && $this->underline !== PHPExcel_Style_Font::UNDERLINE_NONE) $css .= "underline ";
			if ($this->through) $css .= "line-through ";
			return $css;
		}
		protected function setCssDecoration($value) {
			$value = strtolower($value);
			$a = explode(" ", $value);
			$this->underline = array_search("underline", $value) !== false; 
			$this->through = array_search("line-through", $value) !== false; 
		}
		
		/**
		 * (non-PHPdoc)
		 * @see OU_File_Workbook_Style_Base::getCss()
		 */
		protected function getCss()
		{
			return
				array(
					"font-family" => $this->cssName,
					"font-size" => $this->cssSize ? $this->cssSize : null, 
					"color" => $this->cssColor,
					"text-decoration" => $this->cssDecoration ? $this->cssDecoration : null,
					"font-style" => $this->cssStyle,
					"font-weight" => $this->cssWeight
				);
		}

		/* ---------- */
		
	}

?>