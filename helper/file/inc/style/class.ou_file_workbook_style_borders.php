<?php 

	/**
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 01/02/2013
	 */

	OU_Config::IncRel("class.ou_file_workbook_style_base.php", __FILE__);
	OU_Config::IncRel("class.ou_file_workbook_style_border.php", __FILE__);
	 
	/**
	 * 
	 * @property OU_File_Workbook_Style_Border $left
	 * @property OU_File_Workbook_Style_Border $top
	 * @property OU_File_Workbook_Style_Border $right
	 * @property OU_File_Workbook_Style_Border $bottom
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_file_workbook_style_borders.php 240 2013-05-23 11:21:29Z xaguilarf $
	 *
	 */
	class OU_File_Workbook_Style_Borders extends OU_File_Workbook_Style_Base
	{

		/**
		 * @var PHPExcel_Style_Borders
		 */
		private $_object;
		public function __construct($style)
		{
			parent::__construct($style);
			$this->_object = $this->get_Style()->getObject()->getBorders();
		}
		
		/* Virtual properties */
		
		private $_tmpBorderA = array();
		private function _tmpBorder($key, $obj)
		{
			if (!isset($this->_tmpBorderA[$key]))
			{
				$this->_tmpBorderA[$key] = new OU_File_Workbook_Style_Border($this, $obj, $key);
			}
			return $this->_tmpBorderA[$key];
		}
		
		protected function getLeft() { return $this->_tmpBorder("left", $this->_object->getLeft()); }
		protected function getTop() { return $this->_tmpBorder("top", $this->_object->getTop()); }
		protected function getRight() { return $this->_tmpBorder("right", $this->_object->getRight()); }
		protected function getBottom() { return $this->_tmpBorder("bottom", $this->_object->getBottom()); }
		
		protected function getCss()
		{
			return 
				$this->left->css +
				$this->right->css +
				$this->top->css +
				$this->bottom->css;
		}		
				
		/* ---------- */
		
	}

?>