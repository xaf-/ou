<?php 

	/**
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 01/02/2013
	 */

	OU_Config::IncRel("class.ou_file_workbook_style_base.php", __FILE__);
	 
	/**
	 * 
	 * @property string vertical
	 * @property string horizontal
	 * @property boolean wrapText
	 * @property number indent
	 * 
	 * @property string cssVertical
	 * @property string cssHorizontal
	 * @property string cssWrapText
	 * @property string cssIndent 
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_file_workbook_style_alignment.php 240 2013-05-23 11:21:29Z xaguilarf $
	 *
	 */
	class OU_File_Workbook_Style_Alignment extends OU_File_Workbook_Style_Base
	{

		/**
		 * @var PHPExcel_Style_Alignment
		 */
		private $_object;
		public function __construct($style)
		{
			parent::__construct($style);
			$this->_object = $this->get_Style()->getObject()->getAlignment();
		}
		
		/* Virtual properties */
		
		protected function getVertical() { return $this->_object->getVertical(); }
		protected function getHorizontal() { return $this->_object->getHorizontal(); }
		protected function setVertical($value) { $this->_object->setVertical($value); }
		protected function setHorizontal($value) { $this->_object->setHorizontal($value); }
		protected function getWrapText() { return $this->_object->getWrapText(); }
		protected function setWrapText($value) { $this->_object->setWrapText($value); }
		protected function getIndent() { return $this->_object->getIndent(); }
		protected function setIndent($value) { return $this->_object->setIndent($value); }

		protected function getCssIndent() { return $this->indent . "px"; }
		protected function getCssWrapText() { return $this->wrapText ? "normal" : "nowrap"; }
		protected function getCssVertical() {
			$css = $this->vertical;
			switch ($css)
			{
				case PHPExcel_Style_Alignment::VERTICAL_TOP: return "top";
				case PHPExcel_Style_Alignment::VERTICAL_BOTTOM: return "bottom";
				case PHPExcel_Style_Alignment::VERTICAL_CENTER: return "middle";
				default: return null;
			}
		}
		private function setCssVertical($value)
		{
			$value = strtolower($value);
			switch ($value)
			{
				case "middle":
					$value = "center";
					break;
			}
			$this->vertical = $value;
		}
		protected function getCssHorizontal() {
			$css = $this->horizontal;
			switch ($css)
			{
				case PHPExcel_Style_Alignment::HORIZONTAL_LEFT: return "left"; 
				case PHPExcel_Style_Alignment::HORIZONTAL_RIGHT: return "right";  
				case PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY: return "justify"; 
				case PHPExcel_Style_Alignment::HORIZONTAL_CENTER:  
				case PHPExcel_Style_Alignment::HORIZONTAL_CENTER_CONTINUOUS:
					return "center";
				default:
					return null;
			}
		}
		private function setCssHorizontal($value)
		{
			$value = strtolower($value);
			$this->horizontal = $value;
		}

		/**
		 * (non-PHPdoc)
		 * @see OU_File_Workbook_Style_Base::getCss()
		 */
		public function getCss()
		{
			return array(
				"text-align" => $this->cssHorizontal,
				"vertical-align" => $this->cssVertical,
				"white-space" => $this->cssWrapText,
				"text-indent" => $this->cssIndent
			);
		}
		
		/* ---------- */
		
	}

?>