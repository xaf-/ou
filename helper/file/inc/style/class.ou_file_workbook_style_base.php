<?php

	/**
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 01/02/2013
	 */

	/**
	 * 
	 * @property array $css
	 * @property OU_File_Workbook_Style $_style
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_file_workbook_style_base.php 240 2013-05-23 11:21:29Z xaguilarf $
	 *
	 */
	class OU_File_Workbook_Style_Base extends OU_Base
	{
		
		/**
		 * @var OU_File_Workbook_Style
		 */
		private $__style;
		/**
		 * @return OU_File_Workbook_Style
		 */
		protected function get_Style() { return $this->__style; }

		/* Virtual properties */
		protected function getCss() { return array(); }
		protected function setCss($value) {  }
		/* ---------- */
		
		/**
		 * @param OU_File_Workbook_Style $style
		 */
		public function __construct($style)
		{
			$this->__style = $style;
		}
		
	}

?>