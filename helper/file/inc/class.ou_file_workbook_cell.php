<?php

	/**
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 31/01/2013
	 */

	OU_Config::IncRel("class.ou_file_workbook_style.php", __FILE__);
	 
	/**
	 * 
	 * @property mixed $value
	 * @property string $html
	 * @property OU_File_Workbook $file
	 * @property string $cordinate
	 * @property number $columnIndex
	 * @property number $rowIndex
	 * @property OU_File_Workbook_Style $style
	 * @property string formattedValue
	 * @property string dataType
	 * @property number $colspan
	 * @property number $rowspan
	 * @property boolean $valid
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_file_workbook_cell.php 240 2013-05-23 11:21:29Z xaguilarf $
	 */
	class OU_File_Workbook_Cell extends OU_Base
	{
		
		/**
		 * @var PHPExcel_Cell
		 */
		private $_object;
		
		/* Virtual properties */
		
		/**
		 * @var OU_File_Workbook
		 */
		private $_file;
		protected function getFile(){ return $this->_file; }
		/**
		 * @var string
		 */
		protected function getCordinate(){ return $this->_object->getCoordinate(); }
		protected function getColumnIndex() { $r = PHPExcel_Cell::coordinateFromString($this->cordinate); return $this->file->stringToIndex($r[0]); }
		protected function getRowIndex() { $r = PHPExcel_Cell::coordinateFromString($this->cordinate); return $r[1]; }
		
		/**
		 * @var OU_File_Workbook_Style
		 */
		private $_style = null;
		protected function getStyle() { if ($this->_style === null) $this->_style = new OU_File_Workbook_Style($this); return $this->_style; }
		
		protected function getValue() { return $this->_object->getValue(); }
		protected function setValue($value) { $this->_object->setValue($value); }
		
		protected function getDataType() { return $this->_object->getDataType(); }
		protected function setDataType($value) { $this->_object->setDataType($value); }
		
		protected function getFormattedValue() { return $this->_object->getFormattedValue(); }
		
		private function getSpan()
		{
			foreach ($this->file->getObject()->getActiveSheet()->getMergeCells() as $range)
			{
				list($cells, ) = PHPExcel_Cell::splitRange($range);
				list($first, $last) = $cells;
				if ($first == $this->cordinate)
				{
					
					list($first_x, $first_y) = PHPExcel_Cell::coordinateFromString($first);
					list($last_x, $last_y) = PHPExcel_Cell::coordinateFromString($last);
					
					$first_x = $this->file->stringToIndex($first_x);
					$last_x = $this->file->stringToIndex($last_x);
					
					return array(
						$last_x - $first_x + 1,
						$last_y - $first_y + 1
					);
						
					break;
				}
			}
			return array(1, 1);
		}
		
		private function isSpanned()
		{
			foreach ($this->file->getObject()->getActiveSheet()->getMergeCells() as $range)
			{
				list($cells, ) = PHPExcel_Cell::splitRange($range);
				list($first, $last) = $cells;
				if ($first == $this->cordinate)
					continue;
				if ($this->_object->isInRange($range))
					return true;
			}
			return false;
		}
		
		protected function getColspan() { $a = $this->getSpan(); return $a[0]; }
		protected function getRowspan() { $a = $this->getSpan(); return $a[1]; }
		
		protected function getValid()
		{
			return !$this->isSpanned();
		}
		
		protected function getHtml()
		{
			
			switch ($this->dataType)
			{
				case PHPExcel_Cell_DataType::TYPE_FORMULA:
					$value = $this->formattedValue; 
					break;
				default:
					$value = $this->value;
			}
			
			if ($value instanceof PHPExcel_RichText)
			{
				$rich = $value;
				/* @var PHPExcel_RichText $rich */
				$str = array();
				foreach ($rich->getRichTextElements() as $element)
				{
					/* @var $element PHPExcel_RichText_TextElement */
					$font = new OU_File_Workbook_Style_Font($this->getStyle(), $element->getFont());
					
					$v = htmlentities($element->getText());
					if ($this->style->alignment->wrapText) $v = nl2br($v);
					
					$str[] = "<span style=\"".$this->style->cssToStr($font->css)."\">".$v."</span>";
				}
				return implode("", $str);
			}else{
				$v = htmlentities($value);
				if ($this->style->alignment->wrapText) $v = nl2br($v);
				return $v;
			}
		}
		protected function setHtml($value) {}
		
		/* ---------- */
		
		/**
		 * @param OU_File_Workbook $file
		 */
		private function __construct($file,  $object)
		{
			$this->_file = $file;
			$this->_object = $object;
		}
		
		/**
		 * @param number $x
		 * @param number $y
		 * @param OU_File_Workbook $file
		 * @return OU_File_Workbook_Cell
		 */
		public static function fromXY($file, $x, $y)
		{
			$cord = $file->indexToString($x) . $y;
			return self::fromCord($file, $cord);
		}
		
		private static $_tmp_cells = array();
		/**
		 * @param OU_File_Workbook $file
		 * @param string $cord
		 * @return OU_File_Workbook_Cell
		 */
		public static function fromCord($file, $cord)
		{
			if (!isset(self::$_tmp_cells[$cord]))
				self::$_tmp_cells[$cord] = new OU_File_Workbook_Cell($file, $file->getObject()->getActiveSheet()->getCell($cord));
			return self::$_tmp_cells[$cord];
		}
		
	}

?>