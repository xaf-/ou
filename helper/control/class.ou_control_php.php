<?php 

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 24/10/2012
	 */

	OU_Config::IncClassHelper(
		array(
			"OU_Control_Base",
			"OU_Utils_Smarty"
		)
	);
	
	/**
	 * Permite crear controles en PHP.
	 * @property OU_Array $resources
	 * 
	 * @property function $code
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @subpackage	OU Control Helper
	 * @version		$$Id: class.ou_control_php.php 324 2013-10-28 01:03:28Z xaguilarf $$
	 * @example		ou_control/ou_control_php/test1.php
	 */
	class OU_Control_Php extends OU_Control_Base
	{
		
		public $parts = array();
		
		private $_resources;
		protected function getResources(){ return $this->_resources; }

		public function __construct($name, $code, $params)
		{

			$this->parts = new OU_Array();
			$this->_resources = new OU_Array();
			parent::__construct($name, $code, $params);
		}

		protected function _compile(&$params, $options)
		{

			$options = OU_Options::FromArray(
				$options,
				array(
				)
			);
				
			// Clear: Nomes fa falta l'últim?
			$this->manager->compiled_controls->Clear()->Add(
				array(
					"params" => $params
				)
			);
			
			if (!isset($params["content"]))
				$params["content"] = "";


			ob_start();

			OU_Complier_Php_Data::Create($this->code, $params)->exec();
			$c = ob_get_contents();
			ob_end_clean();

			if (isset($this->params["strip"]) && ($this->params["strip"] === true || $this->params["strip"] === "true"))
			{
				$c = preg_replace('!\s+!u', ' ', $c);
			}
			
			if (isset($this->params["inherit"]))
			{
				$p2 = OU_Array::FromArray($params, $this->params); 
				$p2["content"] = $c;
				
				$o2 = OU_Array::FromArray($options);
				if (isset($o2["parts"]))
					$o2->parts = OU_Array::FromArray($o2->parts, $this->parts);
				else
					$o2->parts = OU_Array::FromArray($this->parts);
				
				$p2["inherited"] = $this->name;
				 
				return $this->manager[$this->params["inherit"]]->compile($p2, $o2);
			}
			
			return $c;
			
		}
		
	}
	
?>