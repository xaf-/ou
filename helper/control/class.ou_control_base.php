<?php 

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 24/10/2012
	 */

	OU_Config::IncClass(
		array(
			"OU_Base",
			"OU_Array"
		)
	);
	
	/**
	 * 
	 * Clase que controla la base de los controles. Se ocupa de gestionar los paraemtros base y heredados.
	 * No se ocupa de generar los contenidos de los controles heredades ni las partes.
	 * 
	 * @property array $params
	 * @property string $code
	 * @property string $name
	 * @property OU_Control_Manager $manager
	 * @property OU_Array $elements
	 * 
	 * @see			OU_Control_Php
	 * @see			OU_Control_Tpl
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @subpackage	OU Control Helper
	 * @version		$$Id: class.ou_control_base.php 328 2013-10-31 11:03:58Z xaguilarf $$
	 */
	abstract class OU_Control_Base extends OU_Base
	{
		
		private $_name;
		private $_params;
		private $_code;
		private $_manager;

		public function getParams() { return $this->_params; }
		public function getCode() { return $this->_code; }
		public function getName() { return $this->_name; }

		public function getManager() { return $this->_manager; }
		public function setManager($v) { $this->_manager = $v; }

		public function __construct($name, $code, $params)
		{
			$this->_name = $name;
			$this->_params = $params;
			$this->_code = $code;
		}
		
		/**
		 * @param array $params
		 * @param OU_Options|array $options
		 */
		public function compile($params = array(), $options = array())
		{
			
			$options = OU_Options::FromArray(
				$options,
				array(
					"parts" => array()
				)
			);
			
			$params["controller"] = OU_App::app()->current;
			$params["base_uri"] = OU_App::app()->baseURI();
			
			global $_CONFIG;
			if (isset($_CONFIG))
				$params["_CONFIG"] = $_CONFIG;
				
			global $mainColor;
			if (isset($mainColor))
				$params["mainColor"] = $mainColor;
			
			
			$parts = $options->parts->toArray();
			
			/**
			 * Base Params
			 * -----------
			 * S'ocupa de pasar els parametres del control anterior al control actual en cas de ser un control heredat.
			 * No s'encarrega de generar el contingut del control heredat. Aixo s'ocupa cada tipus de control:
			 * 	- PHP : OU_Control_Php::compile() - Compila el contingut del "parent" i afegeix com a content el "child"
			 *  - TPL : OU_Compiler_Tpx_Plugin_Control::_getInherited() - Modifica el contingut string.
			 */
			// Base Params
			if (isset($params["inherited"]) && $params["inherited"])
			{
				// Static params on register
				$inh = $this->manager[$params["inherited"]];
				if (isset($inh->params["params"]))
				{
					$p = json_decode($inh->params["params"]);
					foreach ($p as $k=>$v)
						$params[$k] = $v;
				}
			
				// Parent params without content
				$last = $this->manager->compiled_controls->Last();
				if ($last)
				{
					$p = $last["params"];
					foreach ($p as $k=>$v)
						if ($k != "content")
							$params[$k] = $v;
				}
				
			}
				
			if (isset($params["parts"])){
				foreach ($parts as $k=>$v)
				{
					if (!isset($params["parts"][$k]))
						$params["parts"][$k] = $v;
				}
			}else
				$params["parts"] = $parts;
				
			
			$r = $this->_compile($params, $options);
			
			return $r;
		}
		
		abstract protected function _compile(&$params, $options);
		
	}

?>