<?php 

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 24/10/2012
	 */

	OU_Config::IncClassHelper(
		array(
			"OU_Control_Base",
			"OU_Utils_Smarty"
		)
	);
	
	/**
	 * Permite crear controles en TPL.
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @subpackage	OU Control Helper
	 * @version		$Id: class.ou_control_tpl.php 264 2013-07-11 10:59:21Z xaguilarf $
	 * @example		ou_control/ou_control_tpl/test1.php
	 * @example		ou_control/ou_control_tpl/test2.php
	 */
	class OU_Control_Tpl extends OU_Control_Base
	{
		
		protected function _compile(&$params, $options)
		{
			
			$options = OU_Options::FromArray(
				$options,
				array(
					"smarty" => null
				) 
			);
			
			if ($options->smarty == null)
			{
				$smarty = OU_Utils_Smarty::_getSmarty();
				$s = $smarty;
			}else{
				$smarty = $options->smarty;
				$s = $smarty->smarty;
			}
			
			$idx = count($s->_tag_stack) - 1;
			
			
			$params["smarty_config"] = $smarty->getConfigVars();
			
			// Clear: Nomes fa falta l'últim?
			$this->manager->compiled_controls->Clear()->Add(
				array(
					"params" => $params
				)
			);
			
			$data = $smarty->createData($params);
			$tpl = $smarty->createTemplate("string:" . $this->code, $data);
			return $tpl->fetch();
		}
		
	}

?>