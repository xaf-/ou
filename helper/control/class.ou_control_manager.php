<?php 

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 24/10/2012
	 */

	OU_Config::IncClass(
		array(
			"OU_Base"
		)
	);
	
	/**
	 * Gestiona los controles de la aplicación
	 * 
	 * @property OU_Array $compiled_controls
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @subpackage	OU Control Helper
	 * @version		$Id: class.ou_control_manager.php 255 2013-06-18 10:58:33Z xaguilarf $
	 */
	class OU_Control_Manager extends OU_Base implements Iterator, ArrayAccess, Countable
	{
		
		/**
		 * @var OU_Control_Base[]
		 */
		private $_controls = array();
		private $_compiled_controls;
		public function getCompiled_controls() { return $this->_compiled_controls; }
		
		public function __construct()
		{
			$this->_compiled_controls = new OU_Array();
		}
		
		/* Iterator */
		private $_icurrent = 0;
		public function current () { return $this->_controls[$this->_icurrent]; }
		public function next () { ++$this->_icurrent; }
		public function key () { return $this->_controls[$this->_icurrent]->name; }
		public function valid () { return isset($this->_controls[$this->_icurrent]); }
		public function rewind () { $this->_icurrent = 0; }

		/* ArraAccess */
		/**
		 * @return OU_Control_Base
		 */
		public function offsetExists ($offset) { return $this->indexOf($offset) > -1; }
		public function offsetGet ($offset) { return $this->getByName($offset); }
		/**
		 * @param OU_Control_Base $value
		 * @see ArrayAccess::offsetSet()
		 */
		public function offsetSet ($offset, $value) {
			$value->setManager($this);
			if (($num = $this->indexOf($offset)) > -1)
				$this->_controls[$num] = $value;
			else
				$this->_controls[] = $value;
		}
		public function offsetUnset ($offset) { unset($this->_controls[$this->indexOf($offset)]); }
		
		/* Countable */
		public function count () { return count($this->_controls); }
		
		/* ---------- */
		
		/**
		 * @param string $name
		 * @return number
		 */
		private function indexOf($name)
		{
			foreach ($this->_controls as $k => $v)
			{
				if ($v->name == $name)
				{
					return $k;
				}
			}
			return -1;
		}
		
		/**
		 * @param OU_Control_Base $control
		 * @return number
		 */
		private function indexOfControl($control)
		{
			foreach ($this->_controls as $k => $v)
			{
				if ($v === $control)
				{
					return $k;
				}
			}
			return -1;
		}
		
		/**
		 * @param string $name
		 * @return OU_Control_Base|NULL
		 */
		private function getByName($name)
		{
			if (($num = $this->indexOf($name)) > -1)
				return $this->_controls[$num];
			else
				return null;
		}
		
		/**
		 * @return OU_Control_Base
		 */
		public function Last()
		{
			return $this->_controls[count($this->_controls) - 1];
		}
		
	}

?>