<?php 

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 23/10/2012
	 */

	OU_Config::IncClassHelper(
		array(
			"OU_Debug_Reference",
			"OU_Debug_Parameter"
		)
	);
	 
	/**
	 * @todo		Falta poder accedir als parametres de les funcions
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @property 	ReflectionMethod $_reflactor
	 * @subpackage	OU Debug Helper
	 * @version		$Id: class.ou_debug_method.php 208 2013-04-09 12:00:32Z xaguilarf $
	 */
	class OU_Debug_Method extends OU_Debug_Reference
	{
		
		public static function isMagicMethod($name)
		{
			$names = array("__construct", "__destruct", "__call", "__callStatic", "__get", "__set", "__isset", "__unset", "__sleep", "__wakeup", "__toString", "__invoke", "__set_state", "__clone");
			return array_search($name, $names) !== false;
		}		
		
		
		public function isMagic() { return self::isMagicMethod($this->name()); } 
		public function isStatic() { return $this->_reflactor->isStatic(); }
		public function isPublic() { return $this->_reflactor->isPublic(); }
		public function isPrivate() { return $this->_reflactor->isPrivate(); }
		public function isProtected() { return $this->_reflactor->isProtected(); }
		public function isFinal() { return $this->_reflactor->isFinal(); }
		public function isAbstract() { return $this->_reflactor->isAbstract(); }
		public function isContructor() { return $this->_reflactor->isConstructor(); }
		public function isDestructor() { return $this->_reflactor->isDestructor(); }
		public function isClosure() { return $this->_reflactor->isClosure(); }
		public function isInternal() { return $this->_reflactor->isInternal(); }
		public function isUserDefined() { return $this->_reflactor->isUserDefined(); }
		public function isDeprecated() { return $this->_reflactor->isDeprecated() || $this->comments()->comment("deprecated"); }
		public function isOwner() { return $this->class_name() == $this->_object->name(); }
		public function isNormal() { 
			return 
				$this->isPublic() && 
				!$this->isAbstract() && 
				!$this->isContructor() && 
				!$this->isDestructor() && 
				!$this->isDeprecated() && 
				!$this->isInternal() && 
				!$this->isMagic() && 
				!$this->isClosure(); 
		}
		
		public function access()
		{
			return ($this->isPrivate() ? "private" : ($this->isProtected() ? "protected" : "public"));
		}
		
		private $_tmpResult = array();
		/**
		 * 
		 * @param string $default
		 * @return string
		 */
		public function result($default = false)
		{
			if (!isset($this->_tmpResult[$default]))
			{
				$a = $this->comments()->comment("return");
				
				if (!$a) return $default;
				$a = $a->paramized();
				$this->_tmpResult[$default] = $a[0];
			}
			return $this->_tmpResult[$default];
		}
		
		private $_tmpResultDesc = array();
		/**
		 * 
		 * @param string $default
		 * @return string
		 */
		public function resultDesc($default = false)
		{
			if (!isset($this->_tmpResultDesc[$default]))
			{
				$a = $this->comments()->comment("return");
				
				if (!$a) return $default;
				$a = $a->paramized();
				if (count($a) == 1) return $default;
				array_shift($a);
				$this->_tmpResultDesc[$default] = implode(" ", $a);
			}
			return $this->_tmpResultDesc[$default];
		}
		
		private $_tmpParams = false;

        /**
         * @param bool|string $name
         * @return OU_Debug_Parameter[]
         */
		public function params($name = false)
		{
			
			if ($name !== false)
			{
				$a = $this->params();
				foreach ($a as $b)
					if ($b->name() == $name)
						return $a;
				return null;
			}
			
			if ($this->_tmpParams === false)
			{
				$a = array();
				foreach ($this->_reflactor->getParameters() as $p)
					$a[] = new OU_Debug_Parameter($p, $this);
				$this->_tmpParams = $a;
			}
			return $this->_tmpParams;
		}
		
		/**
		 * @var OU_Debug_Reference
		 */
		private $_object;
		public function __construct($reflactor, $object = null)
		{
			$this->_object = $object;
			parent::__construct($reflactor);
			
		}
		
		public function class_name()
		{
			return $this->_reflactor->class;
		}
		
		private $_tmpClass = false;
		/**
		 * 
		 * @return OU_Debug_Class
		 */
		public function clss()
		{
			if ($this->_tmpClass === false)
			{
				$c = new ReflectionClass($this->class_name());
				if ($c)
					$this->_tmpClass = new OU_Debug_Class($c);
			}
			return $this->_tmpClass;
		}
		
		private $_tmpDesc = false;
		/**
		 * (non-PHPdoc)
		 * @see OU_Debug_Reference::description()
		 */
		public function description()
		{
			if ($this->_tmpDesc === false)
			{
				$d = parent::description();
				if ($d && $this->clss() && $this->clss()->hasParent() && ($m = $this->clss()->parent()->method($this->name())))
				{
					$this->_tmpDesc = $m->description();
				}else{
					$this->_tmpDesc = $d;
				}
			}
			return $this->_tmpDesc;
		}
		
		public function call($params = array())
		{
			if ($this->isStatic())
				return call_user_func_array($this->class_name() . "::" . $this->name(), $params);
			else if ($this->_object instanceof OU_Debug_Object)
				return call_user_func_array(array($this->_object->object(), $this->name()), $params);
		}
		
		public function syntax() {
			return new OU_Debug_Method_Syntax($this);			
		}
		
	}

?>