<?php

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 29/06/2012
	 */

	OU_Config::IncClass("OU_Base");
	
	OU_Config::IncClassHelper(
		array(
			"OU_Debug_Colorize"
		)
	);
	 
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @subpackage	OU Debug Helper
	 * @version		$Id: class.ou_debug_printr.php 211 2013-04-11 12:01:01Z xaguilarf $
	 */
	class OU_Debug_PrintR extends OU_Base
	{
		
		const IS_STATIC = 1 ;
		const IS_PUBLIC = 256 ;
		const IS_PROTECTED = 512 ;
		const IS_PRIVATE = 1024 ;

		// Internal PrintR Functions
		
		private static function _print_format($k, $v, $format, $options, $count)
		{
			$tabs = str_repeat($options->tabStr, $count);
			$str = $options->formats->{$format};
			$str = str_replace("<newline>", $options->newlineStr, $str);
			$str = str_replace("<tabs>", $tabs, $str);
			$str = str_replace("<name>", $k, $str);
			$str = str_replace("<value>", $v, $str);
			return $str;
		}
		
		private static function _print_colorize_var($var, $options, $type=false)
		{
			$style = false;
			if (!$type) $type = gettype($var);
			switch ($type)
			{
				case "boolean":
					$style = "keyword";
					$var = $var === true ? "true" : ($var === false ? "false" : $var);
					break;
				case "string":
					$style = "string";
					$var = '"' . htmlentities($var) . '"';
					break;
				case "integer": 
				case "double": 
				case "float":
					$style = "number"; 
					break; 
			}
			if ($style)
				$style = $options->styles->{$style};
			else if ($type && $options->hasOptionKey($type))
				$style = $options->styles->{$type};
			else
				$style = "";
			return '<span style="'.$style.'" class="ou_print_r_var">' . $var . '</span>';
		}
		
		private static function _print_colorize_keyword($var, $options)
		{
			
			$style = $options->styles->keyword;
			return '<span style="'.$style.'" class="ou_print_r_var">' . $var . '</span>';
		}
		
		private static function _print_colorize_class($var, $options)
		{
			if ($var instanceof OU_Base)
				$style = $options->styles->class;
			else
				$style = $options->styles->class;
			return '<span style="'.$style.'" class="ou_print_r_var">' . $var . '</span>';
		}
		
		private static function _property($propertyName, $object, &$prop=null)
		{
			$ref = new ReflectionObject($object);
			$prop = $ref->getProperty($propertyName);
			$prop->setAccessible(true);
			$value = $prop->getValue($object);
			/*
			 if ($prop->isPrivate())
				$value = $prop->getValue($object);
			else if ($prop->isProtected())
				$value =  "*protected";
			else
				$value = $prop->getValue($object);
			*/
			return $value;
		}
		
		/**
		 * Parecido a la función nativa print_r() pero con la información amplificada y en formato HTML coloreado.
		 * @param mixed $obj
		 * @param array|OU_Options $options
		 * @param number $count
		 * @return string
		 */
		public static function print_r($obj, $options = array(), $count = 0)
		{
			
			$r = "";
			$options = OU_Options::FromArray(
				$options, 
				array(
					"tabStr" => "&nbsp;&nbsp;\t",
					"newlineStr" => "<br />\n",
					"formats" => array(
						"array" => "<newline><tabs>(<newline><value><tabs>)<newline>",
						"object" => "<newline><tabs>(<newline><value><tabs>)<newline>",
						"array_line" => "<tabs>[<name>] => <value><newline>",
						"object_line" => "<tabs><name> <value><newline>"
					),
					"styles" => array(
						"string" => OU_Debug_Colorize::$COLORIZE_STRING,
						"number" => OU_Debug_Colorize::$COLORIZE_NUMBER,
						"keyword" => OU_Debug_Colorize::$COLORIZE_KEYWORD,
						"class" => OU_Debug_Colorize::$COLORIZE_CLASS,
						"array_key" => OU_Debug_Colorize::$COLORIZE_ARRAY_KEY
					),		
					"propertyFilter" => 
						self::IS_STATIC |
						self::IS_PUBLIC |
						self::IS_PROTECTED |
						self::IS_PRIVATE										
				)
			);
			
			$t = $options->tabStr;
			$n = $options->newlineStr;
			
			$tabs = str_repeat($t, $count + 1);
			$tabs2 = str_repeat($t, $count);
			
			if (is_object($obj))
				$r .= self::_print_colorize_class(get_class($obj), $options) . " ";
			else
				$r .= self::_print_colorize_keyword(gettype($obj), $options) . " ";
			
			if (is_array($obj))
			{
				
				// Properties
				
				$content = "";
				
				foreach ($obj as $k=>$v)
				{
					
					$content .= self::_print_format(
						self::_print_colorize_var($k, $options, "array_key"), 
						self::print_r($v, $options, $count + 1), 
						"array_line", 
						$options, 
						$count + 1
					);
					
				}
				
				$r .= self::_print_format(
					false, 
					$content, 
					"array", 
					$options, 
					$count
				);
				
				
			}else
			if (is_object($obj))
			{
				
				$ref = new ReflectionObject($obj);
				
				// Properties
				
				$content = "";
				
				foreach ($ref->getProperties($options->propertyFilter) as $property)
				{
					
					$name = $property->name;
					
					$prop = false;
					$content .= self::_print_format(
						$name, 
						self::print_r(self::_property($name, $obj, $prop), $options, $count + 1), 
						"object_line", 
						$options, 
						$count + 1,
						$prop
					);
					
					$value = self::_property($name, $obj);
					
				}
				
				$r .= self::_print_format(
					false, 
					$content, 
					"object", 
					$options, 
					$count
				);
				
				
			}else
				$r .= self::_print_colorize_var($obj, $options);
				
			if ($count === 0)
				return
					'<div style="font-family: monospace;">' . 
						$r .
					'</div>';
			else
				return $r;
			
		}
		
	}
	
?>