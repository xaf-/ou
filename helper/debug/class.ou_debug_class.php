<?php 

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 23/10/2012
	 */

	OU_Config::IncClassHelper(
		array(
			"OU_Debug_Reference",
			"OU_Debug_Property",
			"OU_Debug_VirtualProperty",
			"OU_Debug_Method"
		)
	);
	 
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @property 	ReflectionClass $_reflactor
	 * @subpackage	OU Debug Helper
	 * @version		$Id: class.ou_debug_class.php 212 2013-04-12 12:01:36Z xaguilarf $
	 */
	class OU_Debug_Class extends OU_Debug_Reference
	{
		
		public function isFinal() { return $this->_reflactor->isFinal(); }
		
		public function isAbstract() { return $this->_reflactor->isAbstract(); }
		
		protected function _createMethod($r)
		{
			return new OU_Debug_Method($r, $this);
		}
		
		protected function _createProperty($r)
		{
			return new OU_Debug_Property($r, $this);
		}
		
		protected function _createVirtualProperty($prop, $readOnly=false)
		{
			return new OU_Debug_VirtualProperty($prop, $this, $readOnly);
		}
		
		private $_tmpInterfaces = false;
		/**
		 * 
		 * @return array
		 */
		public function interfaces()
		{
			if ($this->_tmpInterfaces === false)
				$this->_tmpInterfaces = $this->_reflactor->getInterfaceNames();
			return $this->_tmpInterfaces;
		}
		
		private $_tmpProps = false;
		/**
		 * @return multitype:OU_Debug_Property
		 */
		public function props()
		{
			if ($this->_tmpProps === false)
			{
				$m = array();
				foreach ($this->_reflactor->getProperties() as $r)
				{
					$m[] = $this->_createProperty($r);
				}
				foreach ($this->virtualProps() as $p)
				{
					$m[] = $p;
				}
				$this->_tmpProps = $m;
			}
			return $this->_tmpProps;			
		}
		
		private $_tmpVirtualProps = false;
		/**
		 * @return multitype:OU_Debug_VirtualProperty
		 */
		public function virtualProps()
		{
			if ($this->_tmpVirtualProps === false)
			{
				$m = array();
				
				if ($p = $this->comments()->comment("property"))
					foreach ($p->paramizedValues() as $paramizedValue)
					{
						$m[] = $this->_createVirtualProperty($paramizedValue);
					}
					
				if ($p = $this->comments()->comment("property-read"))
					foreach ($p->paramizedValues() as $paramizedValue)
					{
						$m[] = $this->_createVirtualProperty($paramizedValue, true);
					}
				
				$this->_tmpVirtualProps = $m;
			}
			return $this->_tmpVirtualProps;			
		}
		
		private $_tmpMethods = array();

        /**
         * @param null $filter
         * @return OU_Debug_Method[]
         */
		public function methods($filter = null)
		{
			if (!isset($this->_tmpMethods[$filter]))
			{
				$m = array();
				foreach ($filter === null ? $this->_reflactor->getMethods() : $this->_reflactor->getMethods($filter) as $r)
				{
					$m[] = $this->_createMethod($r);
				}
				$this->_tmpMethods[$filter] = $m;
			}
			return $this->_tmpMethods[$filter];
		}
		
		private $_tmpMethodA = array();
		/**
		 * @param string $name
		 * @return OU_Debug_Method
		 */
		public function method($name)
		{
			if (!isset($this->_tmpMethodA[$name]))
			{
				if ($this->_reflactor->hasMethod($name))
				{
					$m = $this->_reflactor->getMethod($name);
					if (!$m)
						$this->_tmpMethodA[$name] = null;
					else
						$this->_tmpMethodA[$name] = new OU_Debug_Method($m);
				}else{
					$this->_tmpMethodA[$name] = null;
				}
			}
			return $this->_tmpMethodA[$name];
		}
		
		private $_tmpPropsA = array();
		/**
		 * @param string $name
		 * @return OU_Debug_Property
		 */
		public function prop($name)
		{
			if (!isset($this->_tmpPropsA[$name]))
			{
				if ($this->_reflactor->hasProperty($name))
				{
					$m = $this->_reflactor->getProperty($name);
					if (!$m)
						$this->_tmpPropsA[$name] = null;
					else
						$this->_tmpPropsA[$name] = $this->_createProperty($m);
				}else{
					$this->_tmpPropsA[$name] = null;
				}
			}
			return $this->_tmpPropsA[$name];
		}
		
		/**
		 * Devuelve true si la clase es heredada por alguna otra.
		 * @return boolean
		 */
		public function hasParent()
		{
			return $this->parent() != null; 
		}
		
		private $_tmpParent = false;
		/**
		 * Recibe la clase heredada
		 * @return OU_Debug_Class|null
		 */
		public function parent()
		{
			if ($this->_tmpParent === false)
			{
				$p = $this->_reflactor->getParentClass();
				if ($p)
					$this->_tmpParent = new OU_Debug_Class($p);
				else
					$this->_tmpParent = null;
			}
			return $this->_tmpParent;
		}
		
		private $_tmpDesc = false;
		/**
		 * (non-PHPdoc)
		 * @see OU_Debug_Reference::description()
		 */
		public function description()
		{
			if ($this->_tmpDesc === false)
			{
				$d = parent::description();
				if (!$d && $this->hasParent())
					$this->_tmpDesc = $this->parent()->description();
				else
					$this->_tmpDesc = $d;
			}
			return $this->_tmpDesc;
		}
		
		/**
		 * Recibe una array con todos los padres.
		 * @return array
		 */
		public function parents()
		{
			$a = array();
			$p = $this->parent();
			while ($p)
			{
				$a[] = $p;
				$p = $p->parent();
			}
			return $a;
		}
		
		private $_tmpConstructor = false;
		/**
		 * Recibe el constructor de la clase.
		 * @return OU_Debug_Method|NULL
		 */
		public function constructor()
		{
			if ($this->_tmpConstructor === false)
			{
				$p = $this->_reflactor->getConstructor();
				if ($p)
					$this->_tmpConstructor = new OU_Debug_Method($p);
				else
					$this->_tmpConstructor = null;
			}
			return $this->_tmpConstructor;
		}
		
		public function syntax()
		{
			return new OU_Debug_Class_Syntax($this);
		}
		
	}

?>