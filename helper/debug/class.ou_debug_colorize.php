<?php 

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 23/10/2012
	 */

	OU_Config::IncClass(
		array(
			"OU_Options",
			"OU_Base",
			"OU_Path",
			"OU_Debug"
		)
	);
	
	OU_Config::IncExt("geshi");
	 
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @subpackage	OU Debug Helper
	 * @version		$Id: class.ou_debug_colorize.php 212 2013-04-12 12:01:36Z xaguilarf $
	 * @example		ou_debug_colorize.php
	 */
	class OU_Debug_Colorize extends OU_Base
	{
		
		public static $DEFAULT_THEME = "roboticket.xml";
		
		public static $COLORIZE_COLOR_STRING = "#317ECC";
		public static $COLORIZE_COLOR_NUMBER = "#AF0F91";
		public static $COLORIZE_COLOR_KEYWORD = "#295F94";
		public static $COLORIZE_COLOR_COMMENT = "gray";
		public static $COLORIZE_COLOR_VAR = "#8EA7C0";
		public static $COLORIZE_COLOR_CLASS = "#AB2525";
		public static $COLORIZE_COLOR_ARRAY_KEY = "#55aa55";
		public static $COLORIZE_COLOR_TAGS = "#EE0000";
		public static $COLORIZE_COLOR_BACKGROUND = "transparent";
		public static $COLORIZE_COLOR_TEXT = "#000000";
		public static $COLORIZE_COLOR_METHOD = "#000000";
		public static $COLORIZE_COLOR_SYMBOL = "#000000";
		
		public static $COLORIZE_STRING = "color: #317ECC";
		public static $COLORIZE_NUMBER = "color: #AF0F91";
		public static $COLORIZE_KEYWORD = "color: #295F94";
		public static $COLORIZE_COMMENT = "color: gray";
		public static $COLORIZE_VAR = "color: #8EA7C0";
		public static $COLORIZE_CLASS = "color: #AB2525; font-size: italic;";
		public static $COLORIZE_ARRAY_KEY = "color: #55aa55;";
		public static $COLORIZE_TAGS = "color: #EE0000;";
		public static $COLORIZE_BACKGROUND = "backgound: transparent;";
		public static $COLORIZE_TEXT = "color: inherit;";
		public static $COLORIZE_METHOD = "color: inherit;";
		public static $COLORIZE_SYMBOL = "color: inherit;";
		
		private static $_tmpGeshi = array();
		
		public static function fromFile($fileName, $options = array())
		{
			$ext = strtolower(OU_Path::Ext($fileName));
			$type = false;
			switch ($ext)
			{
				case ".css": 
					$type = "css";
					break;
				case ".tpl": 
				case ".php": 
					$type = "php";
					break;
				case ".cs": 
					$type = "csharp";
					break;
				case ".pas": 
					$type = "pas";
					break;
				case ".txt": 
				case ".dat": 
				case ".raw": 
				case ".inf": 
				case ".info": 
				case ".md": 
					$type = "txt";
					break;
				case ".xml": 
				case ".xaml": 
				case ".wsdl": 
				case ".xsd": 
					$type = "xml";
					break;
			}
			$content = file_get_contents($fileName);
			if ($type !== false)
				return self::syntax($content, $type, $options);
			else
				return false;
		}
		
		public static function loadTheme($fileName = null)
		{
			
			if ($fileName === null) $fileName = self::$DEFAULT_THEME;			
			$fileName = OU_Path::Absolute($fileName, dirname(__FILE__) . "/themes/");
			
			if ($fileName)
			{
				
				$ref = OU_Debug::reference("OU_Debug_Colorize");
				
				$fnc = function($ref, $varName, $xml, $xmlName)
				{
					if (!isset($xml->$xmlName)) return;
					$xml = $xml->$xmlName;
					
					/* @var $xml SimpleXMLElement */
					/* @var $ref OU_Debug_Class */
					
					$n = "COLORIZE_" . strtoupper($varName);
					
					if ($p = $ref->prop($n))
					{
						
						$css = array();
						
						if (isset($xml["color"])) {
							if ($varName == "background")
								$css[] = "background: ".$xml["color"];
							else
								$css[] = "color: ".$xml["color"];
							$n2 = "COLORIZE_COLOR_" . strtoupper($varName);
							if ($p2 = $ref->prop($n2))
							{
								$p2->value($xml["color"] . "");
							}
						}
						if (isset($xml["bold"]) && $xml["bold"] == "true") $css[] = "text-weight: bold";						
						if (isset($xml["italic"]) && $xml["italic"] == "true") $css[] = "font-style: italic";						
						if ((isset($xml["underline"]) && $xml["underline"] == "true") && (isset($xml["strikethrough"]) && $xml["strikethrough"] == "true")) 
							$css[] = "text-decoration: underline line-through";
						else if (isset($xml["strikethrough"]) && $xml["strikethrough"] == "true")
							$css[] = "text-decoration: line-through";
						else if (isset($xml["underline"]) && $xml["underline"] == "true")
							$css[] = "text-decoration: underline";
						
						$p->value(implode(";", $css) . ";");
						
					}
					
				};
				
				$xml = simplexml_load_file($fileName);
				$fnc($ref, "string", $xml, "string");
				$fnc($ref, "number", $xml, "number");
				$fnc($ref, "keyword", $xml, "keyword");
				$fnc($ref, "comment", $xml, "singleLineComment");
				$fnc($ref, "var", $xml, "localVariable");
				$fnc($ref, "class", $xml, "class");
				$fnc($ref, "tags", $xml, "localVariableDeclaration");
				$fnc($ref, "background", $xml, "background");
				$fnc($ref, "text", $xml, "foreground");
				$fnc($ref, "method", $xml, "method");
				$fnc($ref, "symbol", $xml, "operator");
				
			}
		}
		
		public static function syntax($content, $type="php", $options = array())
		{
			
			$div = false;
			$numbers = false;
			if ($options == "div")
			{
				$div = true;
				$options = array();
			}
			if ($options == "numbers")
			{
				$numbers = true;
				$options = array();
			}
			$options = OU_Options::FromArray($options, 
				array(
					"style" => false,
					"div" => $div,
					"cache" => false,
					"numbers" => $numbers
				)
			);
			
			if ($options->cache)
			{
				$md5 = md5($content . json_encode($options->toArray()));
				$cache = OU_App::app()->cache("syntax") . "/" . $md5;
				if (file_exists($cache))
					return file_get_contents($cache);
			}
			
			$geshi = new GeSHi($content, $type);
			
			$geshi->set_numbers_highlighting(self::$COLORIZE_NUMBER);
			
			$geshi->set_regexps_style(0, self::$COLORIZE_VAR);
			
			$geshi->set_strings_style(self::$COLORIZE_STRING);
			
			$geshi->set_keyword_group_style(1, self::$COLORIZE_KEYWORD); // return, if, while, do, for, switch
			$geshi->set_keyword_group_style(2, self::$COLORIZE_KEYWORD); // null, false, true, <_?
			
			$geshi->set_symbols_style(self::$COLORIZE_SYMBOL, false, 0);
			$geshi->set_symbols_style(self::$COLORIZE_KEYWORD, false, 1); // ?_>
			
			$geshi->set_keyword_group_style(3, self::$COLORIZE_KEYWORD); // echo, htmlspecialchars
			$geshi->set_keyword_group_style(4, self::$COLORIZE_KEYWORD); // int, double, real, static
			
			$geshi->set_comments_style(1, self::$COLORIZE_COMMENT);
			$geshi->set_comments_style(2, self::$COLORIZE_COMMENT);
			$geshi->set_comments_style(3, self::$COLORIZE_COMMENT);
			
			$geshi->set_methods_style(0, self::$COLORIZE_METHOD);
			$geshi->set_methods_style(1, self::$COLORIZE_METHOD);
			
			$geshi->set_comments_style(4, self::$COLORIZE_COMMENT); // Multi-linia?
			$geshi->set_comments_style('MULTI', self::$COLORIZE_COMMENT); // Multi-linia
			
			$geshi->set_script_style("color: red", false, 0);
			$geshi->set_script_style("color: fuchsia", false, 1);
			$geshi->set_script_style("color: orange", false, 2);
			$geshi->set_script_style("color: ble", false, 3);
			$geshi->set_script_style("color: green", false, 4);
			$geshi->set_script_style("color: yellow", false, 5);
			
			$geshi->keyword_links = false;
			if ($options->numbers)
				$geshi->enable_line_numbers(GESHI_NORMAL_LINE_NUMBERS);
			if ($options->style !== false)
				$geshi->set_overall_style($options->style);
			
			$geshi->set_overall_style(self::$COLORIZE_BACKGROUND . self::$COLORIZE_TEXT);
			
			if ($options->div)
				$geshi->set_header_type(GESHI_HEADER_DIV);
			
			set_time_limit(0);
			$code = $geshi->parse_code();
			
			if ($options->cache)
			{
				@file_put_contents($cache, $code);
			}
			
			return $code;
		}
	}

?>