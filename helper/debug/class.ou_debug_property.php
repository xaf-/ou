<?php 

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 23/10/2012
	 */

	OU_Config::IncClassHelper(
		array(
			"OU_Debug_Class"
		)
	);
	 
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @property	ReflectionProperty $_reflactor
	 * @subpackage	OU Debug Helper
	 * @version		$Id: class.ou_debug_property.php 212 2013-04-12 12:01:36Z xaguilarf $
	 */
	class OU_Debug_Property extends OU_Debug_Reference
	{
		
		/**
		 * @var OU_Debug_Class
		 */
		protected $_class;
		
		public function __construct($reflactor, $class)
		{
			$this->_class = $class;
			parent::__construct($reflactor);
		}
		
		public function isPublic() { return $this->_reflactor->isPublic(); }
		public function isPrivate() { return $this->_reflactor->isPrivate(); }
		public function isProtected() { return $this->_reflactor->isProtected(); }
		public function isStatic() { return $this->_reflactor->isStatic(); }
		
		public function access()
		{
			return $this->isPrivate() ? "private" : ($this->isProtected() ? "protected" : "public");
		}
		
		public function isOwner()
		{
			if ($this->isVirtual())
				return true;
			else
				return $this->_class->name() == $this->_reflactor->class;
		}
		
		private $_tmpType = array();
		public function type($default=null)
		{
			if (!isset($this->_tmpType[$default]))
			{
				$param = $this->comments()->comment("var");
				$this->_tmpType[$default] = $default;
				if ($param)
				{
					$value = $param->paramized();
					if ($value)
					{
						$this->_tmpType[$default] = $value[0];
					}
				}				
			}
			return $this->_tmpType[$default];
		}
		

		public function isVirtual()
		{
			return false;
		}
		
		private $_tmpHasDefaultValue = null;
		public function hasDefaultValue()
		{
			if ($this->_tmpHasDefaultValue === null)
				$this->_tmpHasDefaultValue = $this->_reflactor->isDefault();
			return $this->_tmpHasDefaultValue;
		}
		
		public function defaultValue()
		{
			$defaults = $this->_class->_reflactor->getDefaultProperties();
			if (isset($defaults[$this->name()]))
				return $defaults[$this->name()];
			else
				return NULL;
		}
		
		public function value($value = "€~#@!")
		{
			$this->_reflactor->setAccessible(true);
			if ($value !== "€~#@!")
			{
				if ($this->isStatic())
					$this->_reflactor->setValue($value);
				else
					$this->_reflactor->setValue($this->object(), $value);					
			}else{
				if ($this->isStatic())
					return $this->_reflactor->getValue();
				else
				{
					return $this->_reflactor->getValue($this->object());
				}
			}
		}
		
		public function syntax()
		{
			return new OU_Debug_Property_Syntax($this);
		}
		
	}

?>