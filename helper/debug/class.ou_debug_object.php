<?php 

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 23/10/2012
	 */

	OU_Config::IncClassHelper(
		array(
			"OU_Debug_Class"
		)
	);
	 
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @subpackage	OU Debug Helper
	 * @version		$Id: class.ou_debug_object.php 211 2013-04-11 12:01:01Z xaguilarf $
	 */
	class OU_Debug_Object extends OU_Debug_Class
	{
		
		protected function _createMethod($r)
		{
			return new OU_Debug_Method($r, $this);
		}
		
		protected function _createProperty($r)
		{
			return new OU_Debug_Property($r, $this);
		}
		
		private $_object;
		public function __construct($reflactor, $object)
		{
			$this->_object = $object;
			parent::__construct($reflactor);
		}
		
		public function object()
		{
			return $this->_object;
		}
		
	}

?>