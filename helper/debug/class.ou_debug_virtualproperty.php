<?php 

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 23/10/2012
	 */

	OU_Config::IncClassHelper(
		array(
			"OU_Debug_Class"
		)
	);
	 
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @property	ReflectionProperty $_reflactor
	 * @subpackage	OU Debug Helper
	 * @version		$Id: class.ou_debug_virtualproperty.php 240 2013-05-23 11:21:29Z xaguilarf $
	 */
	class OU_Debug_VirtualProperty extends OU_Debug_Property
	{
		
		private $_prop;
		private $_isReadOnly;
		public function __construct($prop, $class, $isReadOnly = false)
		{
			$this->_prop = $prop;
			$this->_isReadOnly = $isReadOnly;
			parent::__construct(null, $class);
		}
		
		public function name()
		{
			return trim($this->_prop[1], '$');
		}
		
		public function isPublic() { return true; }
		public function isPrivate() { return false; }
		public function isProtected() { return false; }
		public function isStatic() { return false; }
		
		public function type($default=null)
		{
			return $this->_prop[0];
		}
		
		public function description()
		{
			$a = $this->_prop;
			$a = array_slice($a, 2);
			return implode(" ", $a);
		}
		
		public function isReadOnly()
		{
			return $this->_isReadOnly;
		}
		
		public function getGetter()
		{
			if (is_subclass_of($this->_class->name(), "OU_Base"))
				return "get" . ucfirst($this->name());
			else
				return false;
		}
		public function getSetter()
		{
			if ($this->isReadOnly()) return false;
			if (is_subclass_of($this->_class->name(), "OU_Base"))
				return "set" . ucfirst($this->name());
			else
				return false;
			}
		
		public function isVirtual()
		{
			return true;
		}
		
		public function hasDefaultValue()
		{
			return false;
		}
		
		public function defaultValue()
		{
			return NULL;
		}
		
		public function value($value = "€~#@!")
		{
			$this->_reflactor->setAccessible(true);
			if ($value !== "€~#@!")
			{
				if ($this->isStatic())
					$this->_reflactor->setValue($value);
				else
					$this->_reflactor->setValue($this->object(), $value);					
			}else{
				if ($this->isStatic())
					return $this->_reflactor->getValue();
				else
				{
					return $this->_reflactor->getValue($this->object());
				}
			}
		}
		
		public function syntax()
		{
			return new OU_Debug_Property_Syntax($this);
		}
		
	}

?>