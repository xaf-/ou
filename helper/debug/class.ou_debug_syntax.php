<?php

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 23/10/2012
	 */

	OU_Config::IncClass(
		array(
			"OU_Base",
			"OU_Comments"
		)
	);
	
	/**
	 * @property-read OU_Debug_Reference $reference
	 * @property-read string $html
	 * @property-read string $code
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @subpackage	OU Debug Helper
	 * @version		$Id: class.ou_debug_syntax.php 240 2013-05-23 11:21:29Z xaguilarf $
	 */
	abstract class OU_Debug_Syntax extends OU_Base
	{
		
		private $_reference;
		protected $_options;
		
		protected function getReference() { return $this->_reference; }
		
		/**
		 * @param OU_Debug_Reference $reference
		 * @param OU_Array|array $options
		 */
		public function __construct($reference, $options = array())
		{
			$this->_reference = $reference;
			$this->_options = OU_Options::FromArray(
				array(
					"style" => false
				),
				$options
			);
		}
		
		/**
		 * Devuelve el código de la referencia en formato html.
		 */
		protected function getHtml() {
			return '<span class="syntax">' . $this->parts()->syntax() . '</span>';
		}
		
		/**
		 * Devuelve el código de la referencia. 
		 * @return string
		 */
		protected function getCode()
		{
			return $this->parts()->code();
		}
		
		/**
		 * @return OU_Debug_Syntax_Parts
		 */
		abstract public function parts();
		
		public function __toString()
		{
			return $this->html;
		}
		
	}
	
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_debug_syntax.php 240 2013-05-23 11:21:29Z xaguilarf $
	 * @subpackage	OU Debug Helper
	 */
	class OU_Debug_Syntax_Parts
	{
		private $_parts = array();
		public function add($type, $code)
		{
			$this->_parts[] = array(
				"type" => $type,
				"code" => $code
			);
		}
	
		public function explode($type, $glue, $code)
		{
			// todo $this->add($type, explode($glue, $code));
		}
	
		public function count()
		{
			return count($this->_parts);
		}
	
		public function get($index)
		{
			return $this->_parts[$index];
		}
		
		private function _syntaxValue($value)
		{
			$type = false;
			
			if (preg_match("/^\".*?\"$/", $value) || preg_match("/^\'.*?\'$/", $value) )
				$type = "string";
			else  if (preg_match("/^(|\d+\.)\d+$/", $value))
				$type = "number";
			else if (array_search(strtoupper($value), array("NULL", "TRUE", "FALSE")))
				$type = "key";
				
			if ($type)
			{
				return '<span class="'.$type.'">'.$value.'</span>';
			}else
				return $value;
		}
		
		private function _syntaxType($type)
		{

			$a = explode("|", $type);
			
			$r = array();
			
			foreach ($a as $b)
			{
				$c = trim($b);
				if ($c) {
					if (class_exists($c))
						$r[] = '<a title="'.strip_tags(OU_Debug::reference($c)->description()).'" href="'.ClassController::uri($c).'">'.$c.'</a>';
					else
						$r[] = $c;
				}
			}
			
			return implode(" | ", $r);
			
		}
	
		public function syntax()
		{
			$str = array();
			foreach ($this->_parts as $part)
			{
				if ($part["code"] instanceof OU_Debug_Syntax_Parts)
				{
					if ($part["code"]->count() == 0) continue;
					$s = $part["code"]->syntax();
				}
				else
				{
					$t = $part["type"];
					switch ($t)
					{
						case "type":
							$s = $this->_syntaxType($part["code"]);
							break;
						case "value":
							$s = $this->_syntaxValue($part["code"]);
							break;
						default:
							$s = $part["code"];
					}
						
				}
				$str[] = '<span class="'.$part["type"].'">'.$s.'</span>';
			}
			return implode(" ", $str);
		}
	
		public function code()
		{
			$str = array();
			foreach ($this->_parts as $part)
			{
				if ($part["code"] instanceof OU_Debug_Syntax_Parts)
					$str[] = $part["code"]->code();
				else
					$str[] = $part["code"];
					
			}
			return implode(" ", $str);
		}
	
	}
	
	/**
	 * @property OU_Debug_Method $reference
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @subpackage	OU Debug Helper
	 * @version		$Id: class.ou_debug_syntax.php 240 2013-05-23 11:21:29Z xaguilarf $
	 */
	class OU_Debug_Method_Syntax extends OU_Debug_Syntax
	{
		public function parts()
		{
			
			$parts = new OU_Debug_Syntax_Parts();
			
			// Definition
			
			if ($this->reference->isAbstract()) 	$parts->add("key", "abstract");
			if ($this->reference->isFinal()) 		$parts->add("key", "final");
			
			$parts->add("key", $this->reference->access());
			
			if ($this->reference->isStatic()) 		$parts->add("key", "static");
			
			if ($this->_options->style == "php") 			$parts->add("key", "function");
						
			$parts->add("identifier", $this->reference->name());
			
			$parts->add("symbol", "(");
			
			$cnt = 0;
			foreach ($this->reference->params() as $p)
			{
				/* @var $p OU_Debug_Parameter */
				$parts->add("param", $p->syntax()->parts());
				$cnt++;
				if ($cnt < count($this->reference->params()))
					$parts->add("symbol", ",");
			}

			$parts->add("symbol", ")");
			
			$parts->add("symbol", ":");
			
			$parts->add("type", $this->reference->result('void'));
			
			return $parts;			 
			
		}
	}
	

	/**
	 * @property OU_Debug_Parameter $reference
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @subpackage	OU Debug Helper
	 * @version		$Id: class.ou_debug_syntax.php 240 2013-05-23 11:21:29Z xaguilarf $
	 */
	class OU_Debug_Parameter_Syntax extends OU_Debug_Syntax
	{
		public function parts()
		{
			$parts = new OU_Debug_Syntax_Parts();
			
			$parts->add("type", $this->reference->type('void'));
			$parts->add("var", '$' . $this->reference->name());
			
			if ($this->reference->hasDefaultValue())
			{
				$parts->add("symbol", "=");
				$parts->add("value", var_export($this->reference->defaultValue(), true));
			}
			
			return $parts;
		}
	}
	
	/**
	 * @property OU_Debug_Property $reference
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @subpackage	OU Debug Helper
	 * @version		$Id: class.ou_debug_syntax.php 240 2013-05-23 11:21:29Z xaguilarf $
	 */
	class OU_Debug_Property_Syntax extends OU_Debug_Syntax
	{
		public function parts()
		{
			
			$parts = new OU_Debug_Syntax_Parts();
			
			$isVirtual = $this->reference instanceof OU_Debug_VirtualProperty;
			
			$parts->add("key", $this->reference->access());
			
			if ($isVirtual)
				$parts->add("key", "virtual");
				
			if ($this->reference->isStatic()) $parts->add("key", "static");
			if ($isVirtual && $this->reference->isReadOnly()) $parts->add("key", "readonly");
			
			$parts->add("var", '$' . $this->reference->name());
			
			if (($t = $this->reference->type(NULL)) !== NULL)
			{
				$parts->add("symbol", ':');
				$parts->add("type", $t);
			}
			
			if ($this->reference->hasDefaultValue())
			{
				$parts->add("symbol", "=");
				$parts->add("value", var_export($this->reference->defaultValue(), true));
			}
				
			return $parts;
			
		}
	}
	
	/**
	 * @property OU_Debug_Class $reference
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @subpackage	OU Debug Helper
	 * @version		$Id: class.ou_debug_syntax.php 240 2013-05-23 11:21:29Z xaguilarf $
	 */
	class OU_Debug_Class_Syntax extends OU_Debug_Syntax
	{
		public function parts()
		{
			
			$parts = new OU_Debug_Syntax_Parts();
			
			$parts->add("identifier", $this->reference->name());
			
			if ($this->reference->parent())
			{
				$parts->add("key", "extends");
				$parts->add("type", $this->reference->parent()->name());
			}
			
			if ($this->reference->interfaces())
			{
				$parts->add("key", "implements");
				$parts->add("type", implode(", ", $this->reference->interfaces()));
			}
			
			return $parts;
			
		}
	}
	