<?php

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 23/10/2012
	 */

	OU_Config::IncClass(
		array(
			"OU_Base",
			"OU_Comments"
		)
	);
	
	OU_Config::IncClassHelper("OU_Debug_Syntax");
	 
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @protperty	Reflactor $_reflactor
	 * @subpackage	OU Debug Helper
	 * @version		$Id: class.ou_debug_reference.php 208 2013-04-09 12:00:32Z xaguilarf $
	 */
	abstract class OU_Debug_Reference extends OU_Base
	{
		
		/**
		 * @var OU_Comments
		 */
		private $_comments = false;
		/**
		 * @var ReflectionClass
		 */
		protected $_reflactor;
		/**
		 * @param ReflectionClass $reflactor
		 */
		public function __construct($reflactor)
		{
			$this->_reflactor = $reflactor;
			// OU_Debug_Parameter no te commentari!
		}
		
		private $_tmpDescB = false;
		/**
		 * Devuelve la descripción de la documentación.
		 * @see OU_Comments::description()
		 */
		public function description()
		{
			if ($this->_tmpDescB === false)
				$this->_tmpDescB = $this->comments()->description();
			return $this->_tmpDescB;
		}
		
		/**
		 * Devuelve el nombre de la referencia.
		 * @return string 
		 */
		public function name()
		{
			return $this->_reflactor->name;
		}
		
		/**
		 * Devuelve los comentarios de esta referencia
		 * @return OU_Comments
		 */
		public function comments()
		{
			if ($this->_comments === false)
				$this->_comments = new OU_Comments(method_exists($this->_reflactor, "getDocComment") ? $this->_reflactor->getDocComment() : "");
			return $this->_comments;
		}
		
		abstract public function syntax();

	}

?>