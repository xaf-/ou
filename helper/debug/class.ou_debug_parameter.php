<?php 

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 23/10/2012
	 */

	OU_Config::IncClassHelper(
		array(
			"OU_Debug_Class"
		)
	);
	 
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @property	ReflectionParameter $_reflactor
	 * @subpackage	OU Debug Helper
	 * @version		$Id: class.ou_debug_parameter.php 208 2013-04-09 12:00:32Z xaguilarf $
	 */
	class OU_Debug_Parameter extends OU_Debug_Reference
	{
		
		/**
		 * @var OU_Debug_Method
		 */
		protected $_method;
		
		public function __construct($reflactor, $method)
		{
			$this->_method = $method;
			parent::__construct($reflactor);
		}
		
		private $_tmpOpcional = null;
		/**
		 * 
		 */
		public function isOptional()
		{
			if ($this->_tmpOpcional === null)
				$this->_tmpOpcional = $this->_reflactor->isOptional();
			return $this->_tmpOpcional;
		}
		
		private $_tmpHasDefaultValue = null;
		/**
		 * 
		 */
		public function hasDefaultValue()
		{
			if ($this->_tmpHasDefaultValue === null)
				$this->_tmpHasDefaultValue = $this->_reflactor->isDefaultValueAvailable();
			return $this->_tmpHasDefaultValue;
		}
		
		private $_tmpType = array();
		/**
		 * 
		 * @param string $default
		 * @return Ambigous <multitype:>|string
		 */
		public function type($default=null)
		{
			if (!isset($this->_tmpType[$default]))
			{
				$param = $this->_method->comments()->comment("param");
				$this->_tmpType[$default] = $default;
				if ($param)
				{
					foreach ($param->paramizedValues() as $values)
					{
						if (count($values) > 1 && $values[1] == '$' . $this->name())
						{
							$this->_tmpType[$default] = $values[0];
						}
					}
				}
			}
			return $this->_tmpType[$default];
		}
		
		private $_tmpDefaultValue = "ñcx-,-";
		/**
		 * 
		 */
		public function defaultValue()
		{
			if ($this->_tmpDefaultValue === "ñcx-,-")
				$this->_tmpDefaultValue = $this->_reflactor->getDefaultValue();
			return $this->_tmpDefaultValue;
		}
		
		private $_tmpIsArray = null;
		/**
		 * 
		 */
		public function isArray()
		{
			if ($this->_tmpIsArray === null)
				$this->_tmpIsArray = $this->_reflactor->isArray();
			return $this->_tmpIsArray;
		}
		
		private $_tmpDesc = false;
		/**
		 * (non-PHPdoc)
		 * @see OU_Debug_Reference::description()
		 */
		public function description()
		{
			if ($this->_tmpDesc === false)
			{
				if ($this->_method)
				{
					$desc = $this->_method->comments()->comment("param");
					if ($desc)
					{
						$this->_tmpDesc = "";
						$desc = $desc->paramizedValues();
						foreach ($desc as $d)
						{
							if (count($d) > 2)
							{
								if (trim($d[1]) == '$'.$this->name())
								{
									
									$this->_tmpDesc = implode(" ", array_slice($d, 2));
									break;
								}
							}
						}
					}else{
						$this->_tmpDesc = "";
					}
				}else{
					$this->_tmpDesc = "";
				}
			}
			return $this->_tmpDesc;
		}
		
		public function code($options = array())
		{
			$options = OU_Options::FromArray(
				array(
					"style" => false
				),
				$options
			);
			
			$code = '$' . $this->name();
			$t = $this->type(false);
				
			if ($t !== false)
			{
				switch ($options->style)
				{
					case "delphi":
						$code = $code . " : " . $this->type();
						break;
					case "java":
					default:
						$code = $this->type() . " " . $code;
						break;
				}
			}
			
			return trim($code);
			
		}
		
		public function syntax()
		{
			return new OU_Debug_Parameter_Syntax($this);
		}
		
	}

?>