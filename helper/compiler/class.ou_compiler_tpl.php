<?php 

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 24/10/2012
	 */

	OU_Config::IncClass(
		array(
			"OU_App",
			"OU_Base",
			"OU_Comments"
		)
	);
	
	OU_Config::IncClassHelper(
		array(
			"OU_Compiler_Base",
			"OU_Utils_Smarty"
		)
	);
	
	/**
	 * Compilador de archivos .tpl de Smarty.
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @subpackage	OU Compiler Helper
	 * @cache 		tpl 
	 * @version		$Id: class.ou_compiler_tpl.php 215 2013-04-23 12:01:13Z xaguilarf $
	 */
	class OU_Compiler_Tpl extends OU_Compiler_Base
	{
		
		public function __construct($fileName, $parentCompiler = null, $data = null, $customHash = false)
		{
			OU_Utils_Smarty::registerFilter(
				"pre", 
				array(&$this, "_preSmarty")
			);
			
			parent::__construct($fileName, $parentCompiler, $data, $customHash);
		}
		
		public function _preSmarty($source, Smarty_Internal_Template $template){
			$this->RaiseEventBeforeCompile($source);
			return $source;
		}
		
		
		public function hasCache()
		{
			return false; // Sense cache ! Contingut dinamic
		}
		
		public $path_tpl = null;
		
		protected function _compile() 
		{
			
			if (OU_App::hasApp())
			{
				
				$app = OU_App::app();
				
				$path = $app->path();
				if ($this->path_tpl) 
					$path_tpl = $this->path_tpl;
				else
					$path_tpl = $path . OU_App::$_dir_tpl;
					
				OU_Utils_Smarty::setPath($path_tpl);
				OU_Utils_Smarty::setCachePath($app->cache("tpl"));
				OU_Utils_Smarty::setConfigPath($path . OU_App::$_dir_config);
				
			}
			
			$r = OU_Utils_Smarty::internalTpl($this->_fileName, $this->_customData);

			if (OU_App::hasApp())
				OU_Utils_Smarty::restorePath();
			
			return $r;
			
		}
		
	}

?>