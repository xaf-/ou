<?php 

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 24/10/2012
	 */

	OU_Config::IncClass(
		array(
		)
	);
	
	OU_Config::IncClassHelper(
		array(
			"OU_Compiler_Base"
		)
	);
	
	 
	/**
	 * Compilador de archivos MD.
	 * @link		http://daringfireball.net/projects/markdown/syntax
	 * @see			Markdown_Parser
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @subpackage	OU Compiler Helper
	 * @cache 		md
	 * @version		$Id: class.ou_compiler_md.php 240 2013-05-23 11:21:29Z xaguilarf $ 
	 */
	class OU_Compiler_MD extends OU_Compiler_Base
	{
	
		protected function _compile()
		{
			return Markdown($this->content());
		}
	
	}

?>
 