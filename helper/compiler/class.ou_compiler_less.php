<?php 

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 24/10/2012
	 */

	OU_Config::IncClass(
		array(
			"OU_Base"
		)
	);
	
	OU_Config::IncClassHelper(
		array(
			"OU_Compiler_Css"
		)
	);
	
	OU_Config::IncExt("less");
	 
	/**
	 * Compilador de archivos Less.
	 * @link		http://lesscss.org/
	 * @link		http://leafo.net/lessphp/
	 * @see			lessc
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @subpackage	OU Compiler Helper
	 * @cache 		less 
	 * @version		$Id: class.ou_compiler_less.php 322 2013-10-24 10:58:11Z xaguilarf $
	 */
	class OU_Compiler_Less extends OU_Compiler_Css
	{
		
		private $_childrens;
		
		public function __construct($fileName, $parentCompiler = null, $data = null, $customHash = false)
		{
			parent::__construct($fileName, $parentCompiler, $data, $customHash);
			$this->_childrens = new OU_Array();
			if (!$this->isForceCache())
				$this->_generated_content = $this->_findChildrens();
		}
		
		private $_generated_content = "";
		
		private function _findChildrens()
		{
			$file = $this->fileName();
			$data = $this->content();
			$a = array();
			if (preg_match_all('/@import (|\w+ )\"(.*?)\"/', $data, $a, PREG_OFFSET_CAPTURE))
			{
				//print_r($a);
				//die();
				
				$g_offset = 0;
				
				foreach ($a[2] as $k=>$a2)
				{
					$fn = $a[2][$k][0];
					$atext = $a[0][$k][0];
					$offset = $a[0][$k][1];
					
					$mode = trim(strtolower($a[1][$k][0]));
					if ($mode == "") $mode = "after";
					
					$fn_full = dirname($file) . "/" . $fn;
					
					$files = glob($fn_full);
					$str = "";
					foreach ($files as $fn)
					{
							
						$fn_full = realpath($fn);
						$str .= "@import \"".addslashes(OU_Path::Relative($fn_full, dirname($file)))."\";";
						
						if ($fn_full && file_exists($fn_full) && is_file($fn_full))
						{
							$this->_childrens->Add(new OU_Compiler_Less($fn_full));
						}else{
							// Error load file
						}
							
					}
					
					// Reparar de "@import *.less" a n * @import n[name].less
					$o = $offset - $g_offset;
					$data1 = substr($data, 0, $o);
					$data2 = substr($data, $o + strlen($atext));
					
					$data = $data1 . $str . $data2;
					
					$g_offset += strlen($atext) - strlen($str);
					
				}
				
			}
			return $data;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see OU_Compiler_Base::hash()
		 */
		public function hash()
		{
			// Agafa la data del arxiu original i no del tpl compilat! 
			$hash = md5($this->fileName() . @filemtime($this->fileName()));
			
			foreach ($this->_childrens->toArray() as $children)
			{
				/* @var $children OU_Compiler_Less */
				$hash .= $children->hash();
			}
			
			if ($this->_customHash) $hash .= $this->_customHash;
			
			return md5($hash);
		}
		
		private $_testa = false;
		private function _test()
		{
			/*$a = microtime(true);
			if ($this->_testa === false) $this->_testa = $a;
			echo "/* ".($a - $this->_testa)." */
		}
		
		protected function _compile()
		{
			$this->_test();
			$less = new lessc($this->fileName(), null, $this->_generated_content);
			$this->_test();
			$compiled = $less->parse();
			$this->_test();
			$compiled = self::_finalCompress($compiled);
			$this->_test();			
			return $compiled;
						
		}
		
		public static function resourceURL($file, $options = array())
		{
			$options = OU_Options::FromArray(
				$options,
				array(
					"withTimestamp" => false,
					"params" => array()
				)
			);

			if (is_string($options->params)) {
				$a = array();
				parse_str($options->params, $a);
				$options->params = $a;
			}

			// Relative Last APP
			$fn = OU_Path::Absolute($file, OU_App::app()->path() . "css/", array("correctOpts" => array("endSeparator" => false)));
			if (!file_exists($fn)) $fn = OU_Path::ChangeExt($fn, ".less");

			if (!file_exists($fn)) $fn = OU_Path::Absolute($file, dirname(__FILE__) . "/../../../../apps/global/css/", array("correctOpts" => array("endSeparator" => false)));
			if (!file_exists($fn) && strtolower(OU_Path::Ext($file)) == ".css") $fn = OU_Path::ChangeExt($fn, ".less");
			
			if (file_exists($fn))
			{
				$url = OU_Path::Path2Url(dirname($fn), array("useHost" => false)).'/'.basename($file);
				if ($options->withTimestamp)
				{
					$compiler = new OU_Compiler_Less($fn);
					if ($compiler->hasCache())
						$url .= "?t=" . filemtime($compiler->cachePath());
				}

				$url = OU_Path::Create($url, true);
				$url->queryVars->AddArray($options->params);

				return $url->url;
			}else{
				return false;
			}
		}
		public static function resourceHTML($file, $options = array())
		{
			$options = OU_Options::FromArray(
				$options,
				array(

				)
			);
			$fn = static::resourceURL($file, $options);
			return '<link rel="stylesheet" href="'.addslashes($fn).'" type="text/css" />';
		}
		
	}

?>