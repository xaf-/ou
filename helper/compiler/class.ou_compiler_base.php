<?php 

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 24/10/2012
	 */

	OU_Config::IncClass(
		array(
			"OU_Base",
			"OU_Debug"
		)
	);
	
	require_once(dirname(__FILE__) . "/plugins/base.php");
	 
	/**
	 * Contiene una serie de metodos y funciones base para la compilación de contenidos.
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @subpackage	OU Compiler Helper
	 * @version		$Id: class.ou_compiler_base.php 267 2013-07-15 00:41:09Z xaguilarf $
	 * @
	 */
	abstract class OU_Compiler_Base extends OU_Base
	{
		
		/**
		 * @var OU_Array
		 */
		protected $_plugins;
		/**
		 * @var string
		 */
		protected $_fileName;
		/**
		 * @var OU_Compiler_Base
		 */
		protected $_customHash = false;
		protected $_parentCompiler;
		protected $_customData;
		
		public function __construct($fileName, $parentCompiler = null, $data = null, $customHash = false)
		{
			$this->_customHash = $customHash;
			$this->_customData = $data;
			$this->_plugins = new OU_Array();
			$this->_parentCompiler = $parentCompiler;
			$this->_fileName = $fileName;
			$this->_loadPlugins();
			if (!$this->forceCache() && is_file($this->forceCachePath())) @unlink($this->forceCachePath());
		}
		
		public function __destruct()
		{
			if (self::$_forceCacheList !== null && self::$_forceCacheList->isModified())
			{
				self::$_forceCacheList->Save();
			}
		}
		
		public function setCustomHash($v) { $this->_customHash = $v; }
		public function getCustomHash() { return $this->_customHash; }
		
		public function useCache()
		{
			return true;
		}
		
		/**
		 * Recibe el listado de plugins disponibles de este compilador.
		 * @return OU_Array
		 */
		public function plugins()
		{
			return $this->_plugins->toArray();
		}
		
		protected static function _finalCompress($str)
		{
			return str_replace("\n", "", str_replace("\r", "", str_replace("\t", "", $str)));
		}
		
		
		private static $_tmpPluginsClasses = array();
		private static function _getPluginClass($file)
		{
			if (!isset(self::$_tmpPluginsClasses[$file]))
			{
				$matches = false;
				$content = file_get_contents($file);
				if ($content && preg_match('/class (\w+) extends/mis', $content, $matches))
				{
					$class = $matches[1];
					require_once $file;
				}else{
					$class = false;
				}
				self::$_tmpPluginsClasses[$file] = $class;
			}
			return self::$_tmpPluginsClasses[$file];
		}
		
		private function _loadPlugins()
		{
			$path = realpath(dirname(__FILE__) . "/plugins/" . $this->cacheID() . "/");
			if ($path)
			{
				$files = glob($path . "/*.php");
				foreach ($files as $file)
				{
					$class = $this->_getPluginClass($file);
					if ($class) $this->_plugins->Add(new $class($this, $file));
					
				}
				$this->RaiseEventCompiler("init", array());
			}
		}
		
		/**
		 * Devuelve TRUE si existe cache.
		 * @return boolean
		 */
		public function hasCache()
		{
			if ($file = $this->forceCacheFile())
			{
				return $file ? true : false;
			}
			return $this->useCache() && file_exists($this->cachePath());
		}
		
		/**
		 * @return string
		 */
		public function fileName()
		{
			if ($this->_parentCompiler)
				return $this->_parentCompiler->fileName();
			else
				return $this->_fileName;
		}
		
		/**
		 * Comprueba si la dirección es valida
		 * @return number
		 */
		protected function isFileNameValid()
		{
			return !(mb_strpos($this->fileName(), "string:") === 0);
		}
		
		private $_tmpFileTime = null;
		private function _getFileTime()
		{
			if ($this->_tmpFileTime === null)
			{
				$this->_tmpFileTime = @filemtime($this->fileName());
			}
			return $this->_tmpFileTime;
		}
		
		/**
		 * Dirección local absoluta del archivo de cache.
		 * @return string
		 */
		public function cachePath()
		{
			if ($file = $this->forceCacheFile())
			{
				return $file;
			}
			$name = false;
			if($this->isFileNameValid()) {
				$name = basename($this->fileName());
				$fn = OU_Path::ChangeExt($name, "");
			}			
			$t = $this->_getFileTime();
			return 
				$this->_cacheBasePath() .
				$this->hash() . "_" .
				strftime("%d%m%Y-%H%M%S", $t ? $t : 0) . 
				($name ? OU_Path::Ext($name) : "");
		}
		
		/**
		 * Parte fija de la dirección local de las cache de este archivo.
		 * @return string
		 */
		protected function _cacheBasePath()
		{
			if ($this->isFileNameValid())
			{
				$name = substr(basename($this->fileName()), 0, 50);
				$fn = OU_Path::ChangeExt($name, "");
			}else{
				$fn = "string" . substr(md5(substr($this->fileName(), 0, 100)), 0, 5);
			}
			
			$fn .= "_";
			
			return 
				$this->_cacheRoot() . 
				$fn . 
				md5($this->fileName()) . "_";
		}
		
		private $_tmpCacheID = false;
		/**
		 * Obtiene el identificador de la cache.
		 * @return string
		 */
		public function cacheID()
		{
			if ($this->_tmpCacheID === false)
			{
				$c = OU_Debug::reference($this)->comments()->comment("cache");
				if ($c) $this->_tmpCacheID = $c->value();
				else $this->_tmpCacheID = "unk";
			}
			return $this->_tmpCacheID;
		}
		
		/**
		 * Directorio padre donde se alojarán los archivos de cache.
		 * @return string
		 */
		protected function _cacheRoot()
		{
			$type = $this->cacheID();
			return OU_App::app()->cache($type) . "/";
		}
		
		protected function RaiseEventBeforeCompile(&$content)
		{
			$this->RaiseEventCompiler("beforeCompile", array(&$content));
		}
		
		protected function RaiseEventCompiler($type, $args)
		{
			foreach ($this->_plugins->toArray() as $plugin)
			{
				/* @var $plugin OU_Compiler_BasePlugin */
				$plugin->raise($type, $args);
			}
			return $this->raise($type, $args);
		}
		
		private $_content = false;
		/**
		 * Recibe el contenido original del archivo a compilar.
		 * @return boolean
		 */
		protected function content()
		{
			if ($this->_content === false)
			{
				$content = file_get_contents($this->_fileName);
				$this->RaiseEventBeforeCompile($content);
				$this->_content = $content;
			}
			return $this->_content;
		}
		
		private function &_doCompile()
		{
			$compile = $this->_compile();
			$this->RaiseEventCompiler("afterCompile", array(&$compile));
			return $compile;
		}
		
		/**
		 * Fuerza a crear la cache.
		 */
		public function createCache()
		{
			$this->deleteCache();
			$compile = &$this->_doCompile();
			if ($compile !== null)
			{
				if (file_put_contents($this->cachePath(), $compile))
					return true;
				else
					return false;
			}else{
				return false;
			} 
		}
		
		/**
		 * Borra todas las caches de este fichero. 
		 */
		public function deleteCache()
		{
			$path = $this->_cacheBasePath();
			foreach (glob($path . "*") as $file)
			{
				if (is_file($file)) @unlink($file);
			}
		}
		
		/**
		 * Regenera la cache.
		 */
		public function build()
		{
			$this->createCache();
		}
		
		/**
		 * Genera un código que identifica la cache actual.
		 * @return string
		 */
		public function hash()
		{
			$r = md5($this->fileName() . @filemtime($this->_fileName));
			if ($this->_customHash) $r .= $this->_customHash;
			return $r;
		}
		
		/**
		 * Devuelve el contenido de la cache o FALSE si no existe.
		 * @return string|boolean
		 */
		public function cache()
		{
			if ($this->hasCache())
				return $this->_cacheContent();
			else
				return false;
		}
		
		/**
		 * Devuelve el contenido del archivo cache
		 * @return string
		 */
		private function _cacheContent()
		{
			$this->forceCacheFile($this->cachePath());
			return file_get_contents($this->cachePath());
		}
		
		private static $_forceCacheList = null;
		private function forceCache() {
			return OU_App::$forceCache;
		}
		private static function forceCachePath(){
			return OU_App::cachePath() . "/forceCacheTmp.data";
		}
		private static function forceCacheList()
		{
			if (self::$_forceCacheList === null)
			{
				self::$_forceCacheList = OU_Array::FromFile(self::forceCachePath());
			}
			return self::$_forceCacheList;
		}
		protected function isForceCache()
		{
			return $this->forceCache() && $this->forceCacheFile();
		}
		private function forceCacheFile($v = null)
		{
			if ($this->useCache() && $this->forceCache())
			{
				$a = self::forceCacheList();
				$k = $this->cacheID() . "::" . $this->fileName();
				if ($v === null)
				{
					if (isset($a[$k]))
						return $a[$k];
					else
						return false;
				}else{
					$a[$k] = $v;
				}
			}else{
				return false;
			}
		}
		
		/**
		 * Función base para la compilación del contenido.
		 */
		abstract protected function _compile();
		/**
		 * Devuelve el contenido desde la cache. Si no existe en la cache la crea.
		 * @return Ambigous <string, boolean>
		 */
		public function compile()
		{
			if ($this->hasCache())
				return $this->cache();
			else
			{
				if ($this->useCache())
				{
					if ($this->createCache())
					{
						return $this->_cacheContent();
					}else{
						// Error create Cache
					}
				}else{
					if (($content = $this->_doCompile()) !== null)
					{
						return $content;
					}else{
						// Error compile
					}
				}
			}
		} 
		
	}

?>