<?php 

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 24/10/2012
	 */

	OU_Config::IncClass(
		array(
			"OU_App",
			"OU_Base",
			"OU_Comments"
		)
	);
	
	OU_Config::IncClassHelper(
		array(
			"OU_Control_Php",
			"OU_Compiler_Base",
			"OU_Utils_Smarty"
		)
	);
	
	/**
	 * 
	 * @property OU_Array $params
	 * 
	 * Compilador de archivos .php.
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @subpackage	OU Compiler Helper
	 * @cache 		php 
	 * @version		$Id: class.ou_compiler_php.php 339 2013-12-20 19:19:20Z xaguilarf $
	 */
	class OU_Compiler_Php extends OU_Compiler_Base
	{
		
		private $_params;
		protected function getParams(){ return $this->_params; }
		protected function setParams($value){ $this->_params = OU_Array::FromArray($value); }
		
		public function __construct($fileName, $parentCompiler = null, $data = null, $customHash = false)
		{
			$this->_params = new OU_Array();
			parent::__construct($fileName, $parentCompiler, $data, $customHash);
		}
		
		public function _preSmarty($source, Smarty_Internal_Template $template){
			$this->RaiseEventBeforeCompile($source);
			return $source;
		}
		
		public function hasCache()
		{
			return false; // Sense cache ! Contingut dinamic
		}
		
		public function useCache()
		{
			return false;
		}
		
		
		private static $_tmpFuncs = array();
		protected function _compile() 
		{
			
			$tmp = realpath($this->cachePath());
			
			$experimental = true;
			
			if ($experimental)
			{
    			
    			/**
    			 * Experimental:
    			 * 
    			 * 		Guarda en cache el resultat del codi despres de ser transformat per els plugins. La transformacio dels plugins
    			 * 		no es dinamica i no varia mai, sempre que no s hagi modificat el contingut del arxiu.
    			 * 
    			 * 		La cache es individual i no aprofita el sistema de cache deribat del "base". El que es guarda en cache no es el
    			 * 		resultat sino el codi que despres generarera aquest resultat.
    			 * 
    			 * 		Sempre que es modifiqui un arxiu es borraran tots els caches relacionats amb aquest i despres es creara un de 
    			 * 		nou. 
    			 * 
    			 *       # S'ha modificat de eval a include
    			 */
			    
			    $fncName = "ou_compiler_php_" . $this->hash();
    			
    			if (is_file($tmp))
    			{
    				// $code = file_get_contents($tmp);
    			}else{
    						
    				foreach (glob($this->_cacheBasePath() . "*") as $file) unlink($file);
    				$file = file_get_contents($this->fileName());
    				$this->RaiseEventBeforeCompile($file);
    				$file = '?>' . $file . '<?php ';
    				$code = '<?php $'.$fncName.' = function (){ ' . $file . "\n" .' } ?>';
    				file_put_contents($this->cachePath(), $code);
    			}
    			
	    		$c = $this->cachePath();
    			if (isset(self::$_tmpFuncs[$c]))
    			{
    				$fnc = self::$_tmpFuncs[$c];
    			}else{
	    			require_once($c);
	    			$fnc = ${$fncName};
	    			self::$_tmpFuncs[$c] = $fnc;
    			}
    			 
    			 
			}else{
			    
			    foreach (glob($this->_cacheBasePath() . "*") as $file) unlink($file);
			    $file = file_get_contents($this->fileName());
			    $this->RaiseEventBeforeCompile($file);
			    $file = '?>' . $file . '<?php ';
			    $code = 'return function(){ '.$file.' };';
			    
			}
			
			ob_start();
			
			OU_Complier_Php_Data::Create($fnc, $this->_params)->exec();
			$c = ob_get_contents();
			ob_end_clean();			
				
			return $c;
		}
		
	}
	
	class OU_Complier_Php_Parts extends OU_Array
	{
		public function Add($key_or_value, $value = "{�}")
		{
			if ($value !== "{�}")
			{
				if (preg_match("/^(.*?)\[\]$/", $key_or_value, $m))
				{
					$k = $m[1];
					if (!isset($this[$k]))
					{
						parent::Add($k, new OU_Complier_Php_Parts2());
					}
					if ($value)
					{
						$this[$k]->Add($value);
					}
				}else{
					if (isset($this[$key_or_value]) && is_object($this[$key_or_value]) && $this[$key_or_value] instanceof OU_Complier_Php_Parts2)
					{
						$this[$key_or_value]->Add($value);
					}
					else
						return parent::Add($key_or_value, $value);
				}
			}else
				return parent::Add($key_or_value, $value);
		}
		
	}
	
	class OU_Complier_Php_Parts2 extends OU_Array{}
	
	/**
	 * @property-read string $params
	 */
	class OU_Complier_Php_Data extends OU_Array
	{
		/**
		 * @var Closure
		 */
		protected $_fnc;
	
		public static function Create($fnc, $params)
		{
            $vars = OU_App::app()->getGlobalVars();

			$obj = OU_Complier_Php_Data::FromArray($params, $vars);
			$obj->_fnc = $fnc;
			return $obj;
		}
	
		public function exec()
		{
			$f = $this->_fnc->bindTo($this);
			call_user_func_array($f, array());
		}
	}

?>