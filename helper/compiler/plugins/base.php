<?php

	/*
	 * The source code is given as is. The author is not responsible
	 * for any possible damage done due to the use of this code.
	 * The component can be freely used in any application. The complete
	 * source code remains property of the author and may not be distributed,
	 * published, given or sold in any form as such. No parts of the source
	 * code can be included in any other component or application without
	 * written authorization of oudesign. 30/10/2012
	 */


	OU_Config::IncClass(
		array(
			"OU_Base",
			"OU_Path"
		)
	);

	/**
	 * @author		oudesign
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 oudesign (http://x-s.es)
	 */
	abstract class OU_Compiler_BasePlugin extends OU_Base
	{
		/**
		 * @var OU_Compiler_Base
		 */
		protected $_compiler;
		/**
		 * @var string
		 */
		protected $_fileName;
		
		public function __construct($compiler, $fn)
		{
			
			$this->_fileName = $fn;
			$this->_compiler = $compiler;
			
			$this->on("init", function($sender){
				$sender->init();
			});
			$this->on("beforeCompile", function($sender, &$content){
				$sender->before($content, $sender);
			});
			$this->on("afterCompile", function($sender, &$content){
				$sender->after($content, $sender);
			});
			
		}
		
		/**
		 * Devuelve el nombre del plugin
		 */
		public function name()
		{
			return OU_Path::ChangeExt(basename($this->_fileName), "");
		}
		
		public function init() { }
		/**
		 * @param string $content
		 * @param OU_Compiler_BasePlugin $compiler
		 */
		public function before(&$content, $plugin) {}
		/**
		 * @param string $content
		 * @param OU_Compiler_BasePlugin $compiler
		 */
		public function after(&$content, $plugin) {}
		
	}

?>