<?php


	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of oudesign. 30/10/2012
	 */

	OU_Config::IncClassHelper("OU_Compiler_Php");
	
	/**
	 * Extiende la funcionalidad de php permitiendo crear etiquetas personalizadas que hacen referencia a controles definidos.
	 * @author		oudesign
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 oudesign (http://x-s.es)
	 * @subpackage	OU Compiler Helper
	 */
	class OU_Compiler_Php_Plugin_Ext extends OU_Compiler_BasePlugin
	{

		/**
		 * (non-PHPdoc)
		 * @see OU_Compiler_BasePlugin::before()
		 * @param string $content
		 * @param OU_Compiler_Php_Plugin_Ext $plugin
		 * @return string
		 */
		public function before(&$content, $plugin) 
		{
			
			$cE = '(|(.*?)[^\-])'; // res o contingut no acabat amb "-" + ">"
			$cN = '(\"(.*?)\"|\\\'(.*?)\\\'|(.*?))\s*';

			// ou:js
			$content = preg_replace_callback('/\<ou\:js'.$cE.'\/\>/', 
				function($a){
					require_once(dirname(__FILE__) . "/../tpl/ou_js.php");
					return OU_Compiler_Tpx_Plugin_Js::_smarty_fnc(OU_CPPEM::attrs_to_array($a[1]));
				}, 
				$content);
			
			// ou:css
			$content = preg_replace_callback('/\<ou\:css'.$cE.'\/\>/', 
				function($a){
					require_once(dirname(__FILE__) . "/../tpl/ou_css.php");
					return OU_Compiler_Tpx_Plugin_Css::_smarty_fnc(OU_CPPEM::attrs_to_array($a[1]));
				}, 
				$content);
			
			// -- Controls --
			
				// ou:register
				$content = preg_replace_callback('/\<ou\:register'.$cE.'\>/',
					function($a){
						return  '<?php $parent_registered_control = OU_CPPEM::registerControl("'.addslashes($a[1]).'"); $parent_registered_control->code = function() use ($parent_registered_control){ $registered_control = $parent_registered_control; ?>';
					},
					$content);
				
				// /ou:register
				$content = preg_replace_callback('/\<\/ou\:register\>/',
					function($a){
						return  '<?php } ?>';
					},
					$content);
				
				// ou:content
				$content = preg_replace_callback('/\<ou\:content'.$cE.'\/\>/',
					function($a){
						return  '<?=OU_CPPEM::echoContent($this);?>';
					},
					$content);
				
				// ou:part_inherit
				$content = preg_replace_callback('/\<ou\:part_inherit name='.$cN.'\/>/',
					function($a){
						return  '<ou:part name="'.addslashes($a[1]).'"><ou:part_content name="'.addslashes($a[1]).'" /></ou:part>';
					},
					$content);
				
				// ou:part_content
				$content = preg_replace_callback('/\<ou\:part_content name='.$cN.'\/>/',
					function($a){
						return '<?=OU_CPPEM::echoPartContent($this, "'.addslashes($a[2]).'")?>';
					},
					$content);
				
				/* // ou:part with value 
				$content = preg_replace_callback('/\<ou\:part name='.$cN.' value='.$cN.$cE.'\/\>/',
					function($a){
						return  '<?php OU_CPPEM::$php_ext_controls->Last()->parts->Add("'.addslashes($a[2]).'", '.$a[6].'); ?>';
					},
					$content);
				*/
				
				// ou:part
				$content = preg_replace_callback('/\<ou\:part'.$cE.'\/\>/',
					function($a){
						return  '<?php OU_CPPEM::addPart("'.addslashes($a[2]).'", null); ?>';
					},
					$content);
				
				// ou:part_block
				$content = preg_replace_callback('/\<ou\:part name='.$cN.$cE.'\>/',
					function($a){
						return  '<?php OU_CPPEM::addPart("'.addslashes($a[2]).'", null, isset($registered_control) ? $registered_control : null); ob_start(); ?>';
					},
					$content);
				
				// /ou:part_block
				$content = preg_replace_callback('/\<\/ou\:part'.$cE.'\>/',
					function($a){
						return  '<?php OU_CPPEM::addPart(null, ob_get_contents(), isset($registered_control) ? $registered_control : null); ob_end_clean(); ?>';
					},
					$content);
				
				// ou:param="" />
				$content = preg_replace_callback('/\s*ou\:param="(\w+)"/',
					function($a){
						return ' <?=OU_CPPEM::echoParam($this, "'.$a[1].'")?>';
					},
					$content);
				
				// ou:param="+pre|name|+pos" />
				$content = preg_replace_callback('/\s*ou\:param="(.*)\|(\w+)\|(.*)"(\s|\>|\/)/',
					function($a){
						return ' <?=OU_CPPEM::echoParam($this, "'.$a[2].'", "'.$a[1].'", "'.$a[3].'")?>'.$a[4];
					},
					$content);
				
				// ou:param="name|+pos" />
				$content = preg_replace_callback('/\s*ou\:param="(\w+)\|(.*)"(\s|\>|\/)/',
					function($a){
						return ' <?=OU_CPPEM::echoParam($this, "'.$a[1].'", null, "'.$a[2].'")?>'.$a[3];
					},
					$content);
				
				// <ou:xxx />
				$content = preg_replace_callback('/\<ou\:(\w+)'.$cE.'\/\>/',
					function($a){
						return 
							'<?php OU_CPPEM::echoControl($this, "'.addslashes($a[1]).'", '.OU_CPPEM::attrs_to_array_str($a[2]).'); ?>';
					},
					$content);
				
				// <ou:xxx>
				$content = preg_replace_callback('/\<ou\:(\w+)'.$cE.'\>/',
					function($a){
						return 
							'<?php OU_CPPEM::startControl("'.addslashes($a[1]).'",'.OU_CPPEM::attrs_to_array_str($a[2]).', isset($registered_control) ? $registered_control : null); ?>';
					},
					$content);
				
				// </ou:xxx>
				$content = preg_replace_callback('/\<\/ou\:(\w+)'.$cE.'\>/',
					function($a){
						return 
							'<?php OU_CPPEM::endControl($this, ob_get_contents(), isset($registered_control) ? $registered_control : null);?>'; 
					},
					$content);
				
				
				// Resources
				
				// Script <script ou:js>
				$content = preg_replace_callback('/\<script ou\:resource'.$cE.'\>/',
						function($a){
							return
							'<?php OU_CPPEM::startResource("js", '.OU_CPPEM::attrs_to_array_str($a[1]).');  ob_start(); ?>';
						},
						$content);
				
				// /Script </script ou:js>
				$content = preg_replace_callback('/\<\/script ou\:resource'.$cE.'\>/',
						function($a){
							return
							'<?php OU_CPPEM::endResource("js", ob_get_contents()); ob_end_clean(); ?>';
						},
						$content);
				
				// Script <style ou:js>
				$content = preg_replace_callback('/\<style ou\:resource'.$cE.'\>/',
						function($a){
							return
							'<?php OU_CPPEM::startResource("css", '.OU_CPPEM::attrs_to_array_str($a[1]).');  ob_start(); ?>';
						},
						$content);
				
				// /Script </style ou:js>
				$content = preg_replace_callback('/\<\/style ou\:resource'.$cE.'\>/',
						function($a){
							return
							'<?php OU_CPPEM::endResource("css", ob_get_contents()); ob_end_clean(); ?>';
						},
						$content);
				
			return $content;
			
		}
	}
	
	
	class OU_Compiler_Php_Plugin_Ext_Manager
	{
		/**
		 * @var OU_Array
		 */
		public static $php_ext_controls;

		public static function attrs_to_array($attrs)
		{
			$r = array();
			if (preg_match_all("/(\w[\w\d\-\_]*)\=(\"(.*?)\"|'(.*?)')/", $attrs, $m))
			{
				for ($i = 0; $i < count($m[0]); $i++)
					$r[$m[1][$i]] = (trim($m[3][$i]) == "") ? $m[4][$i] : $m[3][$i];
			}
			return $r;
		}

		public static function attrs_to_array_str($attrs)
		{
			$r = array();
			if (preg_match_all("/(\w[\w\d\-\_]*)\=(\"(.*?)\"|\'(.*?)\'|(.*?)(\s|$))/", $attrs, $m))
			{

				for ($i = 0; $i < count($m[0]); $i++)
					$r[$m[1][$i]] = $m[2][$i];
			}

			$a = array();
			foreach ($r as $k => $v)
			{
				$a[] = "\"$k\" => $v";
			}
				
			return "array(" . implode(",", $a) . ")";
				
		}
		
		protected static $IN_STATE;
		const IN_STATE_REGISTER = 0;
		const IN_STATE_CONTROL = 1;
		const IN_STATE_RESOURCE = 1;

		public static function registerControl($params)
		{
			self::$IN_STATE = self::IN_STATE_REGISTER;
			$params = str_replace("'", "'", $params);
			$params = OU_CPPEM::attrs_to_array(stripslashes($params));
			$name = $params["name"];
			unset($params["name"]);
			$control = new OU_Control_Php($name, null, $params);
			OU_App::app()->controls[] = $control;
			
			self::$echo_controls_count[$name] = 0;
			
			return $control;
		}
		
		public static function echoControl($scope, $name, $params, $content = null, $parts = array())
		{

			//$params = self::attrs_to_array($params));
			if (isset(OU_App::app()->controls[$name]))
			{

                $last = OU_CPPEM::$php_ext_controls->Last();

				$control = OU_App::app()->controls[$name];
				/* @var $control OU_Control_Php */
				$params["content"] = $content;
				$params["scope"] = $scope;
				$params["self"] = $control;
				$params["parent"] = $last;

				// OU_CPPEM::$php_ext_controls->Last()->resources[$type]->Last()->content = $content;

				// Repara les parts per coincidir amb la estructura de OU_Control_Base
				$new_parts = array();
				foreach ($parts as $k=>$v)
					$new_parts[$k] = array(
							"content" => $v
					);

				echo $control->compile($params, array("parts" => $new_parts));

				return true;

			}else{
				return false;
			}
		}
		
		protected static $echo_controls_count = array();

		/**
		 * @param unknown $scope
		 * @param OU_Array $a
		 * @return boolean
		 */
		public static function echoControlArray($scope)
		{
		}
		
		/**
		 * @param unknown $name
		 * @param unknown $value
		 * @param OU_Control_PHP $registered_control
		 */
		public static function addPart($name, $value, $registered_control)
		{
			$registeredIsOwner = $registered_control && self::$echo_controls_count[$registered_control->name] == 0;
			
			if (!$registeredIsOwner)
			{
				if ($name === null)
					OU_CPPEM::$php_ext_controls->Last()->parts->Last($value);
				else
					OU_CPPEM::$php_ext_controls->Last()->parts->Add($name, $value);
			}else{
				if ($name === null)
					$registered_control->parts->Last(array("content" => $value));
				else
					$registered_control->parts->Add($name, array("content" => $value));
			}
		}
		
		public static function startResource($type, $params)
		{
			self::$IN_STATE = self::IN_STATE_RESOURCE;
			
			$l = OU_App::app()->controls->Last();
			
			// $l = OU_CPPEM::$php_ext_controls->Last();
			if (!isset($l->resources[$type])) $l->resources[$type] = new OU_Array();
			$p = array(
				"params" => $params
			);
		
			$l->resources[$type]->Add(OU_Array::FromArray($p));
		}
		
		public static function endResource($type, $content)
		{
		    $l = OU_App::app()->controls->Last();
			$l->resources[$type]->Last()->content = $content;
		}
		
		/**
		 * @param unknown $name
		 * @param unknown $params
		 * @param OU_Control_PHP $registered_control
		 */
		public static function startControl($name, $params, $registered_control)
		{
			self::$IN_STATE = self::IN_STATE_REGISTER;
			// $params = array("name" => "'.addslashes($a[1]).'", "params" => '.OU_CPPEM::attrs_to_array_str($a[2]).', "parts" => new OU_Complier_Php_Parts());
			$p = array();
			$p["parts"] = new OU_Complier_Php_Parts(); 
			$p["resources"] = array();
			$p["name"] = $name;
			$p["params"] = $params;

			OU_CPPEM::$php_ext_controls->Add(OU_Array::FromArray($p));
			
			if ($registered_control)
				self::$echo_controls_count[$registered_control->name]++;
			
			// OU_CPPEM::$echo_controls_count++;
			ob_start();
		}
		
		public static function endControl($scope, $content, $registered_control)
		{
			OU_CPPEM::$php_ext_controls->Last()->content = $content;
			
			if ($registered_control) self::$echo_controls_count[$registered_control->name]--;
			
			ob_end_clean();
			$a = OU_CPPEM::$php_ext_controls->Last();
			OU_CPPEM::$php_ext_controls->Pop();
			return OU_CPPEM::echoControl(
					$scope,
					$a["name"],
					$a["params"],
					isset($a["content"]) ? $a["content"] : null,
					isset($a["parts"]) ? $a["parts"]->toArray() : array());
				
		}
		
		public static function echoPartContent($obj, $name)
		{
			if (isset($obj->parts[$name]) && isset($obj->parts[$name]["content"])) 
				return $obj->parts[$name]["content"];
			else
				return "";
		}
		
		public static function echoContent($obj)
		{
			if (isset($obj->content)) return $obj->content; else return "";
		}
		
		public static function echoParam($obj, $name, $pre = null, $pos = null)
		{
			$value = "";
			if (isset($obj->{$name})) { 
				$value = $obj->{$name};
			}
			if ($pre) $value = $pre . $value;
			if ($pos) $value = $value . $pos;
			
			if (trim($value) != "")
				return "$name=\"$value\"";
			else
				return "";
			
		}
		
	}

	class OU_CPPEM extends OU_Compiler_Php_Plugin_Ext_Manager { }
	OU_CPPEM::$php_ext_controls = new OU_Array();
	

?>