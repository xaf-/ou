<?php


	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of oudesign. 30/10/2012
	 */

	OU_Config::IncClass(
		array(
		)
	);
	 
	/**
	 * Añade la función {ou_base_convert}. Convierte un numero de una base a otra.
	 * Parametros:
	 * <ul>
	 *  <li>$value :	Valor númerico a procesar.</li> 
	 * 	<li>$from :		Base de origen. (opcional) Defecto : 10</li>
	 *  <li>$to :		Base de destino. (opcional) Defecto : 36</li>
	 *  <li>$minchars :	Número de digitos a mostrar. Rellena con 0 a la izquierda. (opcional) Defecto : 6
	 * </ul>
	 * @author		oudesign
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 oudesign (http://x-s.es)
	 * @subpackage	OU Compiler Helper
	 */
	class OU_Compiler_Tpx_Plugin_Base_Convert extends OU_Compiler_BasePlugin
	{
		
		public static function _smarty_fnc($params)
		{
			
			$from = isset($params["from"]) ? $params["from"] : 10;
			$to = isset($params["to"]) ? $params["to"] : 36;
			$value = $params["value"];
			$minchars = isset($params["minchars"])?$params["minchars"]:6;
				
			$r = base_convert($value, $from, $to);
				
			if ($minchars)
			{
				if  (strlen($r) < $minchars)
					$r = str_repeat("0", $minchars - strlen($r)) . $r;
			}
			$r = strtoupper($r);
				
			return $r;
			
		}
		
		private static $_init = false;
		private static function _sinit()
		{
			if (!self::$_init)
			{
				OU_Utils_Smarty::registerPlugin("function", "ou_base_convert", array(__CLASS__, "_smarty_fnc"));
				self::$_init = true;
			}
		}

		public function init()
		{
			self::_sinit();
		}
		
	}
	

?>