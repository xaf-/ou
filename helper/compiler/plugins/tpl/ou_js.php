<?php


	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of oudesign. 30/10/2012
	 */

	OU_Config::IncClass(
		array(
		)
	);
	 
	/**
	 * Añade la función {ou_color}.
	 * Parametros:
	 * <ul>
	 * 	<li>$file:			Dirección del archivo de Javascript.</li>
	 *  <li>$async :		El formato de html sera <script src= /></script> en caso de false. En case de true, devolvera un código que introduce la etiqueta de manera dinámica. (opcional) Defecto: false
	 *  <li>$gefilename :	En caso de especificar TRUE se devolvera la dirección corregida del archivo. (opcional) Defecto: false</li>
	 * </ul>
	 * @author		oudesign
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 oudesign (http://x-s.es)
	 * @subpackage	OU Compiler Helper
	 */
	class OU_Compiler_Tpx_Plugin_Js extends OU_Compiler_BasePlugin
	{
		
		public static function _smarty_fnc($params)
		{
			$file = $params["file"];
			$async = isset($params["async"]) && $params["async"];
			$getfilename = isset($params["getfilename"]) && $params["getfilename"];
			$useHost = isset($params["useHost"]) && $params["useHost"];
			
			$options = array(
				"async" => $async,
				"useHost" => $useHost,
				"withTimestamp" => true,
				"params" => isset($params["params"]) ? $params["params"] : array()
			);
			
			if ($getfilename)
				return OU_Compiler_Js::resourceURL( $file, $options );
			else
				return OU_Compiler_Js::resourceHTML( $file, $options );
		}
		
		private static $_init = false;
		private static function _sinit()
		{
			if (!self::$_init)
			{
				OU_Utils_Smarty::registerPlugin("function", "ou_js", array(__CLASS__, "_smarty_fnc"));
				self::$_init = true;
			}
		}

		public function init()
		{
			self::_sinit();
		}
		
	}
	

?>