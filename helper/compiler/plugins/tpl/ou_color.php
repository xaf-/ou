<?php


	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of oudesign. 30/10/2012
	 */

	OU_Config::IncClass(
		array(
			"OU_Color"
		)
	);
	 
	/**
	 * Añade la función {ou_color}.
	 * Parametros:
	 * <ul>
	 * 	<li>$value :		Valor del color. Vease OU_Color para más información.</li>
	 *  <li>$format :		Formato de salida. (opcional)
	 *  <li>opcionales :	$r, $g, $b, $h, $s, $v, l</li>
	 * </ul>
	 * @see			OU_Color::__constructor()
	 * @author		oudesign
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 oudesign (http://x-s.es)
	 * @subpackage	OU Compiler Helper
	 */
	class OU_Compiler_Tpx_Plugin_Color extends OU_Compiler_BasePlugin
	{
		
		public static function _smarty_fnc($params)
		{
			
			$color = new OU_Color($params["value"]);
			
			if (isset($params["r"])) $color->red = $params["r"];
			if (isset($params["g"])) $color->green = $params["g"];
			if (isset($params["b"])) $color->blue = $params["b"];
			
			if (isset($params["h"])) $color->hue = $params["h"];
			if (isset($params["s"])) $color->saturation = $params["s"];
			if (isset($params["v"])) $color->value = $params["v"];
			if (isset($params["l"])) $color->lightness = $params["l"];
			
			if (isset($params["format"]))
			{
				$a = $color->rgb() + $color->hsv() + array("hex" => $color->hex());
				$format = $params["format"];
				foreach ($a as $k=>$v)
					$format = str_replace("[$k]", $v, $format);
				return $format;
			}
			
			return $color->hex();
		}
		
		private static $_init = false;
		private static function _sinit()
		{
			if (!self::$_init)
			{
				OU_Utils_Smarty::registerPlugin("function", "ou_color", array(__CLASS__, "_smarty_fnc"));
				self::$_init = true;
			}
		}

		public function init()
		{
			self::_sinit();
		}
		
	}
	

?>