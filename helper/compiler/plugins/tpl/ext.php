<?php


	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of oudesign. 30/10/2012
	 */

	OU_Config::IncClass(
		array(
		)
	);
	
	require_once(dirname(__FILE__) . "/ou_money.php");
	 
	/**
	 * Extiende la funcionalidad de smarty permitiendo crear etiquetas personalizadas que hacen referencia a controles definidos.
	 * <ou:button value="text"/> es equivalente a {ou_control name="button" value="test"}. de igual forma <ou:button>text</ou:button>
	 * es equivalente a {ou_control_block name="button"}text{ou_control_block}. Y por último <ou:register ... ></ou:register> es equivalente 
	 * a {ou_control_register ...}{/ou_control_register}.
	 * @author		oudesign
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 oudesign (http://x-s.es)
	 * @subpackage	OU Compiler Helper
	 */
	class OU_Compiler_Tpx_Plugin_Ext extends OU_Compiler_BasePlugin
	{
		public function before(&$content, $plugin) {
			
			$cE = '(|(.*?)[^\-])'; // res o contingut no acabat amb "-" + ">"
			$cN = '(\"(.*?)\"|\\\'(.*?)\\\'|(.*?))\s*';
			
			$r = array(
					
				// <ou:js .. />
				'/\<ou\:js'.$cE.'\/\>/' => '{ou_js ${1}}',
				// <ou:css .. />
				'/\<ou\:css'.$cE.'\/\>/' => '{ou_css ${1}}',
				// <ou:content .. />
				'/\<ou\:content'.$cE.'\/\>/' => '{if isset($content)}{$content}{/if}',

					
				// <ou:register></ou:register>
				'/\<ou\:register'.$cE.'\>/' => '{ou_control_register $1}{literal}',
				'/\<\/ou\:register\>/' => '{/literal}{/ou_control_register}',
					
				// <ou:part_inherit .. />
				'/\<ou\:part_inherit name='.$cN.'\/>/' => '<ou:part name="${2}"><ou:part_content name="${2}" /></ou:part>',
				// <ou:part_content .. />
				'/\<ou\:part_content name='.$cN.'\/>/' => '{if isset($parts.${2}) && isset($parts.${2}.content)}{$parts.${2}.content}{/if}',
				// <ou:part .. />
				'/\<ou\:part'.$cE.'\/\>/' => '{ou_part ${1}}',
				// <ou:part_block ... ></ou:part_block>
				'/\<ou\:part'.$cE.'\>/' => '{ou_part_block ${1}}',
				'/\<\/ou\:part'.$cE.'\>/' => '{/ou_part_block}',
					
				// ou:param="" />
				'/\sou\:param="(\w+)"(\s|\>|\/)/' => ' {if isset(\$${1})}${1}="{\$${1}}"{/if}${2}',
					// ou:param="" />
				'/\sou\:param="(.*)\|(\w+)\|(.*)"(\s|\>|\/)/' => ' ${2}="${1}{if isset(\$${2})}{\$${2}}{/if}${3}"${4}',
				// ou:param="" />
				'/\sou\:param="(\w+)\|(.*)"(\s|\>|\/)/' => ' ${1}="{if isset(\$${1})}{\$${1}}{/if}${2}"${3}',
					
				// <ou:control .. />
				'/\<ou\:(\w+)'.$cE.'\/\>/' => '{ou_control name="${1}" $2}',
				// <ou:control_block ... ></ou:control_block>
				'/\<ou\:(\w+)'.$cE.'\>/' => '{ou_control_block name="${1}" $2}',
				'/\<\/ou\:(\w+)'.$cE.'\>/' => '{/ou_control_block}'
					
			);
			
			
			foreach ($r as $k => $v)
				$content = preg_replace($k, $v, $content);
			
			return $content;
			
		}
	}
	

?>