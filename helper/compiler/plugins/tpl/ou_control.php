<?php


	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of oudesign. 30/10/2012
	 */

	OU_Config::IncClass(
		array(
		)
	);
	
	OU_Config::IncClassHelper(
		array(
			"OU_Utils_Smarty",
			"OU_Control_Tpl"
		)
	);
	 
	/**
	 * Añade la función {ou_control}. Permite ejecutar un control registrado.
	 * Parametros:
	 * <ul>
	 * 	<li>$name:			Nombre del control.</li>
	 *  <li>*				En cada control se utilizan parametros personalizados. Vease su documentación para más información.</li>
	 * </ul>
	 * @author		oudesign
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 oudesign (http://x-s.es)
	 * @subpackage	OU Compiler Helper
	 */
	class OU_Compiler_Tpx_Plugin_Control extends OU_Compiler_BasePlugin
	{
		
		private static $_ou_controls = array();
		
		protected static function _getInherited($name, $data, $params)
		{
				
			if (isset($params["strip"]) && $params["strip"])
			{
				$data = '{strip}'.$data.'{/strip}';
			}
				
			if (isset($params["inherit"]))
			{
				$data = '{ou_control_block name="'.$params["inherit"].'" inherited="'.$name.'"}'.$data.'{/ou_control_block}';
			}
	
			return $data;
		
		}
		
		public static function _smarty_fnc_register($params, $data)
		{
			if ($data === NULL) return;
			
			$name = $params["name"];
			
			OU_App::app()->controls[] = new OU_Control_Tpl($name, self::_getInherited($name, $data, $params), $params);
			
			self::$_ou_controls[$name] = array(
				"data" => self::_getInherited($name, $data, $params),
				"params" => isset($params["params"]) && $params["params"] ? json_decode($params["params"]) : array()
			);
			
		}
		
		public static function _smarty_fnc_part($params, $smarty)
		{
			
			if (count($smarty->_tag_stack) > 1)
			{
				$pre = &$smarty->smarty->_tag_stack[count($smarty->_tag_stack) - 2];		
				switch ($pre[0])
				{
					case "ou_control_block":
					// case "ou_control":
						
						$a = $params;
						$a["content"] = isset($params["content"]) ? $params["content"] : null;
						
						if (isset($params["name"]))
							$pre["elements"][$params["name"]] = $a;
						else
							$pre["elements"][] = $a;				
						
						
						break;
				}
			}	
				
		}
		
		/**
		 * 
		 * @param unknown $params
		 * @param unknown $data
		 * @param Smarty $smarty
		 */
		public static function _smarty_fnc_part_block($params, $data, $smarty)
		{
			if ($data === NULL) return;
			
			$a = $params;
			$a["content"] = $data;
			
			return self::_smarty_fnc_part($a, $smarty);
			
		}
		
		public static function _smarty_fnc_block($params, $data, $smarty)
		{
			if ($data === NULL)
			{
				
				// Crea la array elements al iniciar tag.
				
				$s = $smarty;
				$idx = count($s->_tag_stack) - 1;
				$s->smarty->_tag_stack[$idx]["elements"] = array();
				
				return;
			}
			
			$a = $params;
			$a["content"] = $data;
				
			return self::_smarty_fnc($a, $smarty);
		}
		
		/**
		 * 
		 * @param unknown $params
		 * @param Smarty_Internal_Template $smarty
		 * @return void|Ambigous <string, mixed>
		 */
		public static function _smarty_fnc($params, $smarty)
		{
			
			$name = $params["name"];
			
			if (isset(self::$_ou_controls[$name]))
			{
				
				$s = $smarty->smarty;
				
				$idx = count($s->_tag_stack) - 1;
				
				$parts = array();
				if (isset($s->smarty->_tag_stack[$idx]["elements"]))
					$parts = $s->smarty->_tag_stack[$idx]["elements"];
								
				return OU_App::app()->controls[$name]->compile($params, array("parts" => $parts));
			}
			else
				return;
			
		}
		
		private static $_init = false;
		private static function _sinit()
		{
			if (!self::$_init)
			{
				
				OU_Utils_Smarty::registerPlugin("function", "ou_part", array(__CLASS__, "_smarty_fnc_part"));
				OU_Utils_Smarty::registerPlugin("block", "ou_part_block", array(__CLASS__, "_smarty_fnc_part_block"));
				
				OU_Utils_Smarty::registerPlugin("function", "ou_control", array(__CLASS__, "_smarty_fnc"));
				OU_Utils_Smarty::registerPlugin("block", "ou_control_block", array(__CLASS__, "_smarty_fnc_block"));
				OU_Utils_Smarty::registerPlugin("block", "ou_control_register", array(__CLASS__, "_smarty_fnc_register"));
				self::$_init = true;
				
			}
		}

		public function init()
		{
			self::_sinit();
		}
		
	}
	

?>