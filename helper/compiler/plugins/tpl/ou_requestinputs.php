<?php


	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of oudesign. 30/10/2012
	 */

	OU_Config::IncClass(
		array(
			"OU_Debug"
		)
	);
	 
	/**
	 * Añade la función {ou_requestinputs}. Devuelve un input oculto por cada variable en $_REQUEST
	 * @author		oudesign
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 oudesign (http://x-s.es)
	 * @subpackage	OU Compiler Helper
	 */
	class OU_Compiler_Tpx_Plugin_RequestInputs extends OU_Compiler_BasePlugin
	{
		
		public static function _smarty_fnc($params)
		{
			$_fnc = function($array, $_fnc, $sub = false)
			{
				$str = "";
				foreach ($array as $k=>$v)
				{
					
					if ($sub)
						$k2 = $sub . "[" . $k . "]";
					else
						$k2 = $k;
						
					if (is_array($v))
						$str .= $_fnc($v, $_fnc, $k2);
					else
						$str .= '<input type="hidden" name="'.$k2.'" value="'.addslashes($v).'"  />';
				}
				return $str;
			};
			
			return $_fnc($_REQUEST, $_fnc);
		}
		
		private static $_init = false;
		private static function _sinit()
		{
			if (!self::$_init)
			{
				OU_Utils_Smarty::registerPlugin("function", "ou_requestinputs", array(__CLASS__, "_smarty_fnc"));
				self::$_init = true;
			}
		}

		public function init()
		{
			self::_sinit();
		}
		
	}
	

?>