<?php


	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of oudesign. 30/10/2012
	 */

	OU_Config::IncClass(
		array(
		)
	);
	 
	/**
	 * Añade la función {ou_fullname}. Repara y muestra un nombre de persona correctamente. 
	 * Parametros:
	 * <ul>
	 * 	<li>$firstname :			Nombre.</li>
	 *  <li>$surname :				Apellidos. (opcional)</li>
	 *  <li>$sf|$surname_first :	Indica si quieres que el apellido sea el primer nombre mostrado. (opcional) Defecto: false</li> 
	 * </ul>
	 * @author		oudesign
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 oudesign (http://x-s.es)
	 * @subpackage	OU Compiler Helper
	 */
	class OU_Compiler_Tpx_Plugin_Fullname extends OU_Compiler_BasePlugin
	{
		
		public static function _smarty_fnc($params)
		{
			$firstname = $params["firstname"];
			$surname = isset($params["surname"]) ? $params["surname"] : "";
			$surname_first = (isset($params["sf"]) && $params["sf"]) || (isset($params["surname_first"]) && $params["surname_first"]);
				
			$firstname = trim(ucwords(mb_strtolower($firstname)));
			$surname = trim(ucwords(mb_strtolower($surname)));
				
			if ($surname_first)
				return $surname . ($firstname && $surname ? ", " : "") . $firstname;
			else
				return $firstname . " " . $surname;
		}
		
		private static $_init = false;
		private static function _sinit()
		{
			if (!self::$_init)
			{
				OU_Utils_Smarty::registerPlugin("function", "ou_fullname", array(__CLASS__, "_smarty_fnc"));
				self::$_init = true;
			}
		}

		public function init()
		{
			self::_sinit();
		}
		
	}
	

?>