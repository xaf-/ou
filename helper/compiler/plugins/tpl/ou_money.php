<?php


	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of oudesign. 30/10/2012
	 */

	OU_Config::IncClass(
		array(
		)
	);
	 
	/**
	 * Añade la función {ou_money}. Convierte un número entero o decimal a formato de importe de dinero.
	 * Parametros:
	 * <ul>
	 * 	<li>num|number : Número a procesar</li>
	 * </ul>
	 * @author		oudesign
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 oudesign (http://x-s.es)
	 * @subpackage	OU Compiler Helper
	 */
	class OU_Compiler_Tpx_Plugin_Money extends OU_Compiler_BasePlugin
	{
		
		public static function _smarty_fnc($params)
		{
			$locale = localeconv();
			if (isset($params["options"]))
				$options = $params["options"];
			else
				$options = array();
				
			$options = OU_Options::FromArray(
				$options, 
				array(
					"currency" =>
						iconv("Windows-1252", "UTF-8//TRANSLIT", $locale["currency_symbol"]) 
				)
			);
			
			if (isset($params["num"]))
				$num = $params["num"];
			else if (isset($params["number"]))
				$num = $params["number"];
				
			$num = floatval($num);
				
			$space = "";
			if ($num >= 0 && $locale["p_sep_by_space"])
				$space = " ";
			else if ($num < 0 && $locale["n_sep_by_space"])
				$space = " ";
			
			
			return number_format($num, $locale["frac_digits"], $locale["decimal_point"], $locale["thousands_sep"]) . $space . $options->currency;
		}
		
		private static $_init = false;
		private static function _sinit()
		{
			if (!self::$_init)
			{
				OU_Utils_Smarty::registerPlugin("function", "ou_money", array(__CLASS__, "_smarty_fnc"));
				self::$_init = true;
			}
		}

		public function init()
		{
			self::_sinit();
		}
		
	}
	

?>