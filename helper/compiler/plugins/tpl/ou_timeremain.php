<?php


	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of oudesign. 30/10/2012
	 */

	OU_Config::IncClass(
		array(
		)
	);
	 
	/**
	 * Añade la función {ou_timeremain}.
	 * Parametros:
	 * <ul>
	 * 	<li>$date :		Data a procesar.</li>
	 * </ul>
	 * @author		oudesign
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 oudesign (http://x-s.es)
	 * @subpackage	OU Compiler Helper
	 */
	class OU_Compiler_Tpx_Plugin_TimeRemain extends OU_Compiler_BasePlugin
	{
		
		public static function _smarty_fnc($params)
		{
			
			$d = $params["date"];
			if ($d === false) return "";
			$ts = $d - time();
			if ($ts > 31536000 && ($v = $ts / 31536000))
				$val = round ( $v, 0 ) . (round ( $v, 0 ) <= 1 ? ' año' : ' años');
			else if ($ts > 2419200 && ($v = $ts / 2419200))
				$val = round ( $v, 0 ) . (round ( $v, 0 ) <= 1 ? ' mes' : ' meses');
			else if ($ts > 604800 && ($v = $ts / 604800))
				$val = round ( $v, 0 ) . (round ( $v, 0 ) <= 1 ? ' semana' : ' semanas');
			else if ($ts > 86400 && ($v = $ts / 86400))
				$val = round ( $v, 0 ) . (round ( $v, 0 ) <= 1 ? ' día' : ' días');
			else if ($ts > 3600 && ($v = $ts / 3600))
				$val = round ( $v, 0 ) . (round ( $v, 0 ) <= 1 ? ' hora' : ' horas');
			else if ($ts > 60 && ($v = $ts / 60))
				$val = round ( $v, 0 ) . (round ( $v, 0 ) <= 1 ? ' minuto' : ' minutos');
			else
				$val = round($ts) . ($ts == 1 ? ' segundo' : ' segundos');
				
			return $val;
			
		}
		
		private static $_init = false;
		private static function _sinit()
		{
			if (!self::$_init)
			{
				OU_Utils_Smarty::registerPlugin("function", "ou_timeremain", array(__CLASS__, "_smarty_fnc"));
				self::$_init = true;
			}
		}

		public function init()
		{
			self::_sinit();
		}
		
	}
	

?>