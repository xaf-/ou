<?php


	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of oudesign. 30/10/2012
	 */

	OU_Config::IncClass(
		array(
		)
	);
	 
	/**
	 * Añade la función {ou_field_td}. Igual que la función {ou_field} pero añadiendo un TD estilizado segun el tipo de dato. 
	 * Parametros:
	 * <ul>
	 * 	<li>name: Nombre del campo</li>
	 * 	<li>field: Objecto de tipo OU_DB_Result</li>
	 * </ul>
	 * @author		oudesign
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 oudesign (http://x-s.es)
	 * @subpackage	OU Compiler Helper
	 */
	class OU_Compiler_Tpx_Plugin_Field_Td extends OU_Compiler_BasePlugin
	{
		
		public static function _smarty_fnc($params)
		{
			$name = $params["name"];
			$field = $params["field"];
			
			$res = $field->getDisplayInfo($name);
			
			$str = "<td ";
			
			if (isset($res["width"]))
				$str .= "width=\"$res[width]\" ";
			
			if (isset($res["align"]))
				$str .= "align=\"$res[align]\" ";
			
			if (isset($res["class"]) || isset($params["class"]))
			{
				$class = "";
				if (isset($res["class"]))
					$class .= $res["class"] . " "; 
				if (isset($params["class"]))
					$class .= $params["class"] . " "; 
				$str .= "class=\"$class\" ";
			}
			
			$str .= ">".($res["val"])."</td>";
			
			return $str;
		}
		
		private static $_init = false;
		private static function _sinit()
		{
			if (!self::$_init)
			{
				OU_Utils_Smarty::registerPlugin("function", "ou_field_td", array(__CLASS__, "_smarty_fnc"));
				self::$_init = true;
			}
		}

		public function init()
		{
			self::_sinit();
		}
		
	}
	

?>