<?php


	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of oudesign. 30/10/2012
	 */

	OU_Config::IncClass(
		array(
		)
	);
	
	require_once(dirname(__FILE__) . "/ou_money.php");
	 
	/**
	 * Añade la función {ou_price}. {ou_price} es un alias de {ou_money}. 
	 * Parametros:
	 * <ul>
	 * 	<li>num|number : Número a procesar</li>
	 * </ul>
	 * @see			OU_Compiler_Tpx_Plugin_Money
	 * @author		oudesign
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 oudesign (http://x-s.es)
	 * @subpackage	OU Compiler Helper
	 */
	class OU_Compiler_Tpx_Plugin_Price extends OU_Compiler_Tpx_Plugin_Money
	{
		
		private static $_init2 = false;
		private static function _sinit2()
		{
			if (!self::$_init2)
			{
				OU_Utils_Smarty::registerPlugin("function", "ou_price", array("OU_Compiler_Tpx_Plugin_Money", "_smarty_fnc"));
				self::$_init2 = true;
			}
		}
		
		public function init()
		{
			self::_sinit2();
			parent::init();
		}
		
	}
	

?>