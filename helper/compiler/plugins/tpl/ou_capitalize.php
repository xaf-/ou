<?php


	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of oudesign. 30/10/2012
	 */

	OU_Config::IncClass(
		array(
			"OU_Path"
		)
	);
	 
	/**
	 * Añade un "modifier" {ou_capitalize}. Convierte todo a minúsculas y el primer caracterer a mayúsculas.
	 * Parametros:
	 * <ul>
	 * 	<li>$file :	Dirección del archivo a incluir. Permite comodines en la dirección.</li>
	 * </ul>
	 * @author		oudesign
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 oudesign (http://x-s.es)
	 * @subpackage	OU Compiler Helper
	 */
	class OU_Compiler_Tpx_Plugin_Capitalize extends OU_Compiler_BasePlugin
	{
		
		public static function _smarty_fnc($str)
		{
			
			return ucfirst(mb_strtolower($str));
			
		}
		
		private static $_init = false;
		private static function _sinit()
		{
			if (!self::$_init)
			{
				OU_Utils_Smarty::registerPlugin("modifier", "ou_capitalize", array(__CLASS__, "_smarty_fnc"));
				self::$_init = true;
			}
		}

		public function init()
		{
			self::_sinit();
		}
		
	}
	

?>