<?php 

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 24/10/2012
	 */

	OU_Config::IncClass(
		array(
			"OU_Base"
		)
	);
	
	OU_Config::IncClassHelper(
		array(
			"OU_Compiler_Css"
		)
	);
	
	OU_Config::IncExt("scss");
	 
	/**
	 * Compilador de archivos Less.
	 * @link		http://sass-lang.com/
	 * @link		http://leafo.net/scssphp/
	 * @see			scss
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @subpackage	OU Compiler Helper
	 * @cache 		scss
	 * @version		$Id: class.ou_compiler_scss.php 269 2013-07-19 08:38:22Z xaguilarf $ 
	 */
	class OU_Compiler_Scss extends OU_Compiler_Css
	{
	
		private $_childrens;
		
		
		public function __construct($fileName, $parentCompiler = null, $data = null, $customHash = false)
		{
			parent::__construct($fileName, $parentCompiler, $data, $customHash);
			$this->_childrens = new OU_Array();
			$this->_findChildrens();
		}
		
		
		private function _findChildrens()
		{
			$file = $this->fileName();
			$content = $this->content();
			$a = array();
			if (preg_match_all('/@import (|\w+ )\"(.*?)\"/', $content, $a))
			{
				foreach ($a[2] as $k=>$fn)
				{
		
					$mode = trim(strtolower($a[1][$k]));
					if ($mode == "") $mode = "after";
					$fn_full = realpath(dirname($file) . "/" . $fn);
					if ($fn_full && file_exists($fn_full) && is_file($fn_full))
					{
						$this->_childrens->Add(new OU_Compiler_Less($fn_full));
					}else{
						// Error load file
					}
						
				}
			}
		}
		
		public function hash()
		{
			$hash = parent::hash();
			foreach ($this->_childrens->toArray() as $children)
			{
				 // @var $children OU_Compiler_Less 
				$hash .= $children->hash();
			}
			return md5($hash);
		} 
		
		protected function _compile()
		{
			
				
			$scss = new scssc();
			$scss->addImportPath(dirname($this->fileName())); // S'hauria de imitar el remplaçament dels @import del less
				
			$compiled = $scss->compile($this->content());
			$compiled = self::_finalCompress($compiled);
				
			return $compiled;
		
		}

		public static function resourceURL($file, $options = array())
		{
			$options = OU_Options::FromArray(
				$options,
				array(
					"withTimestamp" => false,
					"params" => array()
				)
			);

			if (is_string($options->params)) {
				$a = array();
				parse_str($options->params, $a);
				$options->params = $a;
			}

			// Relative Last APP
			$fn = OU_Path::Absolute($file, OU_App::app()->path() . "css/", array("correctOpts" => array("endSeparator" => false)));
			if (!file_exists($fn)) $fn = OU_Path::ChangeExt($fn, ".scss");

			if (!file_exists($fn)) $fn = OU_Path::Absolute($file, dirname(__FILE__) . "/../../../../apps/global/css/", array("correctOpts" => array("endSeparator" => false)));
			if (!file_exists($fn) && strtolower(OU_Path::Ext($file)) == ".css") $fn = OU_Path::ChangeExt($fn, ".scss");

			if (file_exists($fn))
			{
				$url = OU_Path::Path2Url(dirname($fn), array("useHost" => false)).'/'.basename($file);
				if ($options->withTimestamp)
				{
					$compiler = new OU_Compiler_Scss($fn);
					if ($compiler->hasCache())
						$url .= "?t=" . filemtime($compiler->cachePath());
				}

				$url = OU_Path::Create($url, true);
				$url->queryVars->AddArray($options->params);

				return $url->url;
			}else{
				return false;
			}
		}

		public static function resourceHTML($file, $options = array())
		{
			$options = OU_Options::FromArray(
				$options,
				array(

				)
			);
			$fn = static::resourceURL($file, $options);
			return '<link rel="stylesheet" href="'.addslashes($fn).'" type="text/css" />';
		}
		
	}

