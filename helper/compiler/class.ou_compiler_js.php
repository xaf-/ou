<?php 

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 24/10/2012
	 */

	OU_Config::IncClass(
		array(
			"OU_Base",
			"OU_Comments"
		)
	);
	
	OU_Config::IncClassHelper(
		array(
			"OU_Compiler_Base"
		)
	);
	
	OU_Config::IncExt("phpClosure");
	 
	/**
	 * Compilador de archivos de Javascript utilizando el servicio de Google Closure.
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @subpackage	OU Compiler Helper
	 * @cache 		js 
	 * @version		$Id: class.ou_compiler_js.php 338 2013-11-26 12:01:16Z xaguilarf $
	 */
	class OU_Compiler_Js extends OU_Compiler_Base
	{
		
		private $_childrens_before;
		private $_childrens_after;
		private $_language;
		private $_mode = "simple";
		private $_debug = true;
		private $_useSmarty = true;
		protected function getUseSmarty() { return $this->_useSmarty; }
			protected function _addChildrens($closure, $root, $cntBucle = 0)
		{
			
			foreach ($this->_childrens_before->toArray() as $children)
			{
				/* @var $children OU_Compiler_Js */
				$children->_addChildrens($closure, $root, $cntBucle + 1);
			}
				
			$data = null;
			$root->RaiseEventCompiler("importFile", array($this->_fileName, &$data, $this));
			$closure->add($this->_fileName, false, $data);
			
			if (OU_Config::$development)
				echo sprintf("// importFile * ".str_repeat("\t", $cntBucle)." %s\n", OU_Path::Relative($this->fileName(), dirname($root->fileName())));
				
			foreach ($this->_childrens_after->toArray() as $children)
			{
				/* @var $children OU_Compiler_Js */
				$children->_addChildrens($closure, $root, $cntBucle + 1);
				
			}
			
		}
		
		/**
		 * (non-PHPdoc)
		 * @see OU_Compiler_Base::_compile()
		 */
		protected function _compile()
		{

			$closure = new PhpClosure();

			$this->_loadOptions($closure);			
			
			if ($this->_language) $closure->setLanguage($this->_language);
			
			if (!OU_Config::$development || !$this->_debug)
				$closure->hideDebugInfo();
			
			if (OU_Config::$development)
			{
				$closure->prettyPrint();
			}
			
			if (OU_App::hasApp())
			{
				$closure->cacheDir(OU_App::app()->cache("js") . "/");
			}
			
			$this->_addChildrens($closure, $this);
			
			if (OU_App::app()->offlineMode())
			{
				// El sistema offline no permet fer servir les llibreries "useClosureLibrary()". S'hauria de indicar en un error.
				$r = "";
				foreach ($closure->_srcs as $src)
				{
					$r .= 
						(isset($closure->_srcsStr[$src]) ? $closure->_srcsStr[$src] : file_get_contents($src)) . 
						"\n";
				}
				return $r;
			}
				
			return $closure->write(true);
			
		}
		
		/**
		 * @param PhpClosure $closure
		 */
		private function _loadOptions($closure)
		{
			
			$comments = new OU_Comments($this->content(), array("numAsterisk" => 3));
			
			// @useClosureLibrary
			if ($comments->comment("useClosureLibrary"))
			{
				$closure->useClosureLibrary();
				if (OU_Config::$development) echo sprintf("// Options %s\n", "useClosureLibrary");
			}
			
			// @useClosureLibrary
			if ($comments->comment("disableSmarty"))
			{
				$this->_useSmarty = false;
				if (OU_Config::$development) echo sprintf("// Options %s\n", "disableSmarty");
			}
			
			// @useClosureLibrary
			if ($comments->comment("hideDebugInfo"))
			{
				$closure->hideDebugInfo();
				if (OU_Config::$development) echo sprintf("// Options %s\n", "hideDebugInfo");
			}

			// @language
			$language = null;
			if ($comment = $comments->comment("language")){
				$language = $comment->value();
			}
			if (OU_Config::$development) echo sprintf("// Options %s %s\n", "Language", $language ? $language : "default");
			$closure->setLanguage($language);
			
			// @mode
			$mode = null;
			if ($comment = $comments->comment("mode"))
				$mode = trim($comment->value());
			else
				$mode = "simple";
			
			switch (trim($mode))
			{
				case "white":
					$closure->whitespaceOnly();
					break;
				case "advanced":
					$closure->advancedMode();
				case "simple":
				default:
					$closure->simpleMode();
					break;
			}
			if (OU_Config::$development) echo sprintf("// Options %s %s\n", "Mode", $closure->_mode);
			
		}
		
		/**
		 * (non-PHPdoc)
		 * @see OU_Compiler_Base::_compile()
		 */
		private function _findChildrens()
		{
			
			$file = $this->fileName();
			
			$content = $this->content();
			$comments = new OU_Comments($content, array("numAsterisk" => 1));
			
			$comment = $comments->comment("import");
			
			if ($comment)			
			{
				
				foreach ($comment->paramizedValues() as $v)
				{
					$a = false;
					
					if (count($v) > 1)
					{
						$mode = $v[0];
						$value = $v[1];
					}else{
						$mode = "after";
						$value = $v[0];
					}
					
					$fn_full = realpath(dirname($file) . "/" . $value);
					
					// Busca a apps/global
					if (!$fn_full || !is_file($fn_full)) $fn_full = realpath(OU_App::globalBasePath() . "/js/" . $value);
					
					if ($fn_full && is_file($fn_full))
					{
						
						switch ($mode)
						{
							case "before":
								$this->_childrens_before->Add(new OU_Compiler_Js($fn_full));
								break;
							case "after":
							default:
								$this->_childrens_after->Add(new OU_Compiler_Js($fn_full));
								break;
						}
					}
														
				}
			}
			
		}
		
		public function __construct($fileName, $parentCompiler = null, $data = null, $customHash = false)
		{
			parent::__construct($fileName, $parentCompiler, $data, $customHash);
			$this->_childrens_after = new OU_Array();
			$this->_childrens_before = new OU_Array();
			if (!$this->isForceCache())
				$this->_findChildrens();
		}
		
		/**
		 * (non-PHPdoc)
		 * @see OU_Compiler_Base::hash()
		 */
		public function hash()
		{
			$hash = "";
			foreach ($this->_childrens_before->toArray() as $children)
			{
				/* @var $children OU_Compiler_Js */
				$hash .= $children->hash();
			}
			$hash .= parent::hash();
			foreach ($this->_childrens_after->toArray() as $children)
			{
				/* @var $children OU_Compiler_Js */
				$hash .= $children->hash();
			}
			return md5($hash);
		}
		
		public static function resourceURL($file, $options = array())
		{
			
			$options = OU_Options::FromArray(
				$options,
				array(
					"useHost" => false,
					"withTimestamp" => false,
					"params" => array()
				)
			);

			if (is_string($options->params)) {
				$a = array();
				parse_str($options->params, $a);
				$options->params = $a;
			}

			$n = strpos($file, ':');
			if ($n > 0){

				$moduleName = substr($file, 0, $n);
				$m = OU_App::app()->modules->fromName($moduleName);
				if (!$m) throw new Exception('Invalid module');
				$file = substr($file, $n + 1);

				$fn = OU_Path::Absolute($file, $m->path . "/js/", array("correctOpts" => array("endSeparator" => false)));

			}else {
				$fn = OU_Path::Absolute($file, OU_App::app()->path() . "js/", array("correctOpts" => array("endSeparator" => false)));
			}
			if (!file_exists($fn)) $fn = OU_Path::ChangeExt($fn, ".compile.js");
			if (!file_exists($fn)) $fn = OU_Path::Absolute($file, OU_App::globalBasePath() . "js", array("correctOpts" => array("endSeparator" => false)));
			if (!file_exists($fn)) $fn = OU_Path::ChangeExt($fn, ".compile.js");
				
			if (file_exists($fn))
			{
				$url = OU_Path::Path2Url(dirname($fn), array("useHost" => $options->useHost)).'/'.basename($file);
				if ($options->withTimestamp)
				{
					$compiler = new OU_Compiler_Js($fn);
					if ($compiler->hasCache())
						$url .= "?t=" . filemtime($compiler->cachePath());
				}
				$url = OU_Path::Create($url, true);
				$url->queryVars->AddArray($options->params);
				return $url->url;
			}else{
				return false;
			}
			
		}
		
		public static function resourceHTML($file, $options = array())
		{
			$options = OU_Options::FromArray(
				$options,
				array(
					"async" => false
				)
			);
			$url = static::resourceURL($file, $options);
			if ($options->async)
				return "<script> var node = document.createElement('script'); node.type = 'text/javascript'; node.async = true; node.src = '".addslashes($url)."'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(node, s);</script>";
			else
				return '<script type="text/javascript" src="'.addslashes($url).'"></script>';
		}
		
	}

?>