<?php

	OU_Config::IncClassHelper("OU_PS_Base");
	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of oudesign. 03/08/2012
	 */
	 
	 OU_Config::IncClassHelper("OU_PS_Customer");
	 
	/**
	 * @author		oudesign
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 oudesign (http://x-s.es)
	 * @subpackage	OU Prestashop Helper
	 */
	class OU_PS_Product extends OU_PS_Base
	{
		
		public $ps_product = NULL;
		public $tmp = array();
		
		public function __construct($ps_product)
		{
			$this->ps_product = $ps_product;
		}
		
		public function allowOOSP()
		{
			if (isset($this->tmp["allowOOSP"]))
				return $this->tmp["allowOOSP"];
			else
			{
				$r = Product::isAvailableWhenOutOfStock($this->ps_product->out_of_stock);
				$this->tmp["allowOOSP"] = $r;
				return $r;
			}
		}
		
		public function isOnSale()
		{
			return intval($this->ps_product->on_sale) === 1;
		}
		
		public function isOfert()
		{
			$r = false;
			if (isset($this->tmp["isOfert"]))
				return $this->tmp["isOfert"];
			else
			{
				foreach ($this->attr() as $attr)
				{
					$r = $r || ($attr["price"] < 0);
					if ($r)
						break;
				}
				
				$this->tmp["isOfert"] = $r;
				return $r;
			}
			
		}
		
		public function reduction()
		{
			$usetax = Tax::excludeTaxeOption();
			if (isset($this->tmp["reduction"]))
				return $this->tmp["reduction"];
			else
			{
				$r = Product::getPriceStatic((int)$this->ps_product->id, (bool)$usetax, NULL, 6, NULL, true, true, 1, true, NULL, NULL, NULL, $specific_prices);
				$this->tmp["reduction"] = $r;
				return $r;
			}
			
		}
		
		public function availableStock()
		{
			return $this->allowOOSP() || $this->ps_product->quantity > 0;
		}
		
		
		public function isNew()
		{
			return $this->ps_product->isNew();
		}
		
		public function manufacturer()
		{
			if (isset($this->tmp["manufacturer"]))
				return $this->tmp["manufacturer"];
			else
			{
				OU_Config::IncClassHelper("OU_PS_Manufacturer");
				$r = OU_PS_Manufacturer::FromID($this->ps_product->id_manufacturer);
				$this->tmp["manufacturer"] = $r;
				return $r;
			}
		}
		
		public function isShowPrice()
		{
			$o = OU_PS_Customer::FromLogin();
			$b = $o && $o->auth();
			return $b && $this->ps_product->show_price == 1;
		}
		
		public function getPrice($withTax = false)
		{
			
			if (isset($this->tmp[$withTax]))
				return $this->tmp[$withTax];
			
			if ($withTax)
			{
				$r = Product::getPriceStatic((int)$this->getId(), true, NULL, 6);
			}else{
				$r = $this->ps_product->price;
			}
			$this->tmp[$withTax] = $r;
			return $r;
		}
		
		public function isIndexed()
		{
			return $this->ps_product->indexed === 1;
		}
		
		public function isOnlineOnly()
		{
			return $this->ps_product->online_only === 1;
		}
		
		public function isActive()
		{
			return $this->ps_product->active === 1;
		}
		
		public function isAvailableForOrder()
		{
			return $this->ps_product->available_for_order === 1;
		}
		
		public function getName()
		{
			if (is_array($this->ps_product->name))
				return $this->ps_product->name[OU_PS_Base::GetCookie()->id_lang];
			else
				return $this->ps_product->name;
		}
		
		public function getId()
		{
			return $this->ps_product->id;
		}
		
		public function renderBox(
				$options = array()
			)
		{
			$o = 
				$options +
				array(
					"classBox" => "",
					"boxType" => "box_big",
					"imgType" => "home_big",
					"showDetails" => true,
					"showColors" => true,
					"showLinks" => true
				);
			$o["p"] = $this;
			OU_PS_Base::GetSmarty()->assign($o);
			return OU_PS_Base::GetSmarty()->fetch(_PS_THEME_DIR_.'tpl/box.tpl');
		}
		
		public function colors($filerKey = false)
		{
			
			if ($filerKey !== false)
			{
				$r = $this->colors();
				if (is_array($r) && isset($r[$filerKey]))
				{
					return $r[$filerKey];
				}
				return false;
			}
			
			$c = (int)OU_PS_Base::GetCookie()->id_lang;
			
			if (!isset($this->tmp["colors"]))
				$this->tmp["colors"] = array();
			
			if (isset($this->tmp["colors"][$c]))
				return $this->tmp["colors"][$c];
			else
			{
				$colors = array();
				foreach ($this->attr() as $attr)
				{
					if (isset($attr["is_color_group"]) && $attr["is_color_group"] == 1)
					{
						$colors[] = $attr;
					}
				}
				$this->tmp["colors"][$c] = $colors;
				return $colors;
			}
		}
		
		public function colorsGrouped($filerKey = false)
		{
			
			if ($filerKey !== false)
			{
				$r = $this->colors();
				if (is_array($r) && isset($r[$filerKey]))
				{
					return $r[$filerKey];
				}
				return false;
			}
			
			$c = (int)OU_PS_Base::GetCookie()->id_lang;
			
			if (!isset($this->tmp["colorsGrouped"]))
				$this->tmp["colorsGrouped"] = array();
			
			if (isset($this->tmp["colorsGrouped"][$c]))
				return $this->tmp["colorsGrouped"][$c];
			else
			{
				$colors = array();
				$keys = array();
				$imgs = array();
				foreach ($this->attr() as $attr)
				{
					
					$img = _PS_COL_IMG_DIR_ . $attr["id_attribute"] . ".jpg";
					$hasImg = is_file($img);
					
					if (
						(($hasImg && !isset($imgs[$img])) || !isset($keys[$attr["attribute_color"]])) &&
						isset($attr["attribute_color"]) && 
						isset($attr["is_color_group"]) && 
						$attr["is_color_group"] == 1)
					{
						
						if ($hasImg) 
						{
							$attr["img"] = OU_Path::Path2Url($img);
							$imgs[$img] = "";
						}
						$keys[$attr["attribute_color"]] = "";
						$colors[] = $attr;
					}
				}
				$this->tmp["colorsGrouped"][$c] = $colors;
				return $colors;
			}
		}
		
		public function attr($filerKey = false)
		{
			
			if ($filerKey !== false)
			{
				$r = $this->attr();
				if (is_array($r) && isset($r[$filerKey]))
				{
					return $r[$filerKey];
				}
				return false;
			}
			
			$c = (int)OU_PS_Base::GetCookie()->id_lang;
			
			if (!isset($this->tmp["attributes"]))
				$this->tmp["attributes"] = array();
			
			if (isset($this->tmp["attributes"][$c]))
				return $this->tmp["attributes"][$c];
			else
			{
				$res = $this->ps_product->getAttributesGroups($c);
				$this->tmp["attributes"][$c] = $res;
				return $res;
			}
		}
		
		public function combinationImages($filerKey = false)
		{
			if ($filerKey !== false)
			{
				$r = $this->combinationImages();
				if (is_array($r) && isset($r[$filerKey]))
				{
					return $r[$filerKey];
				}
				return false;
			}
			
			$c = (int)OU_PS_Base::GetCookie()->id_lang;
			
			if (!isset($this->tmp["combinationImages"]))
				$this->tmp["combinationImages"] = array();
			
			if (isset($this->tmp["combinationImages"][$c]))
				return $this->tmp["combinationImages"][$c];
			else
			{
				$res = $this->ps_product->getCombinationImages($c);
				$this->tmp["combinationImages"][$c] = $res;
				return $res;
			}
		}
		
		public function GetImageId()
		{
			$res = Product::getCover($this->ps_product->id);
			if ($res && count($res) > 0)
				return $res["id_image"];
			else
				return false;
		}
		
		public function img($type, $subid=false)
		{

			if (isset($this->tmp["img"]) && isset($this->tmp["img"][$subid]))
				return $this->tmp["img"][$subid];
			else
			{
				
				if (!isset($this->tmp["img"])) $this->tmp["img"] = array();
			
				if ($subid)
					$id = $this->ps_product->id . "-" . $subid;
				else
					$id = $this->ps_product->id . "-" . $this->GetImageId();
					
				$fn = dirname(__FILE__) . "/../../../../../" . _THEME_PROD_DIR_ . $id . "-" . $type . ".jpg";
				
				if ($subid && !file_exists($fn))
				{
					$this->tmp["img"][$subid] = false;
					return false;
				}
					
				$r = OU_PS_Base::GetLink()->getImageLink($this->ps_product->link_rewrite, $id, $type);
				$this->tmp["img"][$subid] = $r;
				return $r;
				
			}
		}
		
		public function link()
		{
			if (isset($this->tmp["link"]))
				return $this->tmp["link"];
			else
			{
				$r = OU_PS_Base::GetLink()->getProductLink($this->ps_product->id, $this->ps_product->link_rewrite, OU_PS_Base::GetCookie()->id_lang);
				$this->tmp["link"] = $r;
				return $r;
			}
		}
		
		public static function FromArray($a)
		{
			foreach ($a as $b)
				if ($b instanceof OU_PS_Product)
					return $a;
				else
					break;
					
			if (isset($a["id_product"]))
				return self::FromID($a["id_product"]);
			else if (isset($a["product_id"]))
				return self::FromID($a["product_id"]);
			else
				return false;
		}
		
		public static function FromID($id)
		{
			return OU_PS_Product::FromObject(new Product($id, true, OU_PS_Base::GetCookie()->id_lang));
		}
	
		public static function FromObject($obj)
		{
			return new OU_PS_Product($obj);
		}
	}

?>