<?php

	OU_Config::IncClassHelper("OU_PS_Base");
	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 03/08/2012
	 */
	 
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @subpackage	OU Prestashop Helper
	 * @version		$Id: class.ou_ps_cart.php 135 2012-11-19 11:51:45Z xaguilarf $
	 */
	class OU_PS_Cart extends OU_PS_Base
	{
		
		public $ps_cart = NULL;
		public $tmp = array();
		public static $tmpStatic = array();
		
		public function __construct($ps_cart)
		{
			$this->ps_cart = $ps_cart;
		}
		
		public function getShippingCost($ou_carrier = false, $useTax = true)
		{
			return $this->ps_cart->getOrderShippingCost(
				$ou_carrier ? $ou_carrier->getId() : NULL,
				$useTax
			);
		}
		
		public function getId()
		{
			return $this->ps_cart->id;
		}
		
		public static function FromObject($obj)
		{
			return new OU_PS_Cart($obj);
		}
		
		public static function FromID($id)
		{
			return self::FromObject(new Cart($id));
		}
		
		public static function FromArray($a)
		{
			if (isset($a["id_cart"]))
			{
				return self::FromID($a["id_cart"]); 
			}else
				return false;
		}
		
		public function getName()
		{
			if (is_array($this->ps_cart->name))
				return $this->ps_cart->name[OU_PS_Base::GetCookie()->id_lang];
			else
				return $this->ps_cart->name;
		}
		
	}

?>