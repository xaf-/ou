<?php

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 03/08/2012
	 */

	OU_Config::IncClassHelper("OU_PS_Base");
	 
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @subpackage	OU Prestashop Helper
	 * @version		$Id: class.ou_ps_attribute_group.php 184 2013-03-05 10:09:59Z xaguilarf $
	 */
	class OU_PS_Attribute_Group extends OU_PS_Base
	{
		
		public $ps_attribute_group = NULL;
		public $tmp = array();
		public static $tmpStatic = array();
		
		public function getName()
		{
			if (is_array($this->ps_attribute_group->name))
				return $this->ps_attribute_group->name[OU_PS_Base::GetCookie()->id_lang];
			else
				return $this->ps_attribute_group->name;
		}
		
		public function getPublicName()
		{
			if (is_array($this->ps_attribute_group->public_name))
				return $this->ps_attribute_group->public_name[OU_PS_Base::GetCookie()->id_lang];
			else
				return $this->ps_attribute_group->public_name;
		}
		
		public function __construct($ps_attribute_group)
		{
			$this->ps_attribute_group = $ps_attribute_group;
		}
		
		public static function FromObject($obj)
		{
			return new OU_PS_Attribute_Group($obj);
		}
		
		public static function FromID($id)
		{
			return self::FromObject(new AttributeGroup($id));
		}
		
		public static function FromArray($a)
		{
			if (isset($a["id_attribute_group"]))
			{
				return self::FromID($a["id_attribute_group"]); 
			}else
				return false;
		}
		
		public function getId()
		{
			return $this->ps_attribute_group->id;
		}
		
	}

?>