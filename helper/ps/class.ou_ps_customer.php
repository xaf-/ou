<?php

	OU_Config::IncClassHelper("OU_PS_Base");
	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 03/08/2012
	 */
	 
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @subpackage	OU Prestashop Helper
	 * @version		$Id: class.ou_ps_customer.php 321 2013-10-18 00:20:45Z xaguilarf $
	 */
	class OU_PS_Customer extends OU_PS_Base
	{
		
		public $ps_customer = NULL;
		public $tmp = array();
		public static $tmpStatic = array();
		public static $tmpFromLogin = NULL;
		
		public function __construct($ps_customer)
		{
			$this->ps_customer = $ps_customer;
		}
		
		public function getId()
		{
			return $this->ps_customer->id;
		}
		
		public function isGuest()
		{
			return $this->ps_customer->isGuest();
		}
		
		public function auth()
		{
			return $this->ps_customer->auth;
		}
		
		public static function CheckLogin()
		{
			$login = self::FromLogin();
			if ($login && $login->auth())
			{
			}else{
				Tools::redirect('authentication.php?force&unauth&back=' . $_SERVER['REQUEST_URI']);
				die();
			}
		}
		
		public function valid()
		{
			$b = true;
			// Comprobar RE
			if (!self::FromLogin()) $b = false;
			if (!$this->auth()) $b = false;
			return $b;
		}
		
		public function isRE()
		{
			return $this->ps_customer->isRE();		
		}
		
		public static function FromLogin()
		{
			$id = self::GetCookie()->id_customer;
			if (self::$tmpFromLogin == NULL && $id && $id > 0)
			{
				self::$tmpFromLogin = OU_PS_Customer::FromID($id);
			}
			return self::$tmpFromLogin;
		}
		
		public static function FromObject($obj)
		{
			return new OU_PS_Customer($obj);
		}
		
		public static function FromID($id)
		{
			return self::FromObject(new Customer($id));
		}
		
		public static function FromArray($a)
		{
			if (isset($a["id_customer"]))
			{
				return self::FromID($a["id_customer"]); 
			}else
				return false;
		}
		
		public function getName()
		{
			if (is_array($this->ps_cart->name))
				return $this->ps_cart->name[OU_PS_Base::GetCookie()->id_lang];
			else
				return $this->ps_cart->name;
		}
		
	}

?>