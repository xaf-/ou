<?php

	OU_Config::IncClass("OU_Base");
	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 03/08/2012
	 */
	 
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @subpackage	OU Prestashop Helper
	 * @version		$Id: class.ou_ps_base.php 135 2012-11-19 11:51:45Z xaguilarf $
	 */
	class OU_PS_Base extends OU_Base
	{
		
		public static $link = NULL;
		
		public static function GetCookie()
		{
			global $cookie;
			return isset($cookie) ? $cookie : new Cookie('ps');
		}
		
		public static function GetLink()
		{
			global $link;
			if (isset($link)) self::$link = $link;
			if (self::$link == NULL) self::$link = new Link();
			$link = self::$link;
			return self::$link;
		}
		
		public static function GetSmarty()
		{
			global $smarty;
			return $smarty;
		}
		
	}

?>