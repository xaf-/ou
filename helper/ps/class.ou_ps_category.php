<?php

	OU_Config::IncClassHelper("OU_PS_Base");
	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of oudesign. 03/08/2012
	 */
	 
	/**
	 * @author		oudesign
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 oudesign (http://x-s.es)
	 * @subpackage	OU Prestashop Helper
	 */
	class OU_PS_Category extends OU_PS_Base
	{
		
		public $ps_category = NULL;
		public $tmp = array();
		public static $tmpStatic = array();
		
		public function getName()
		{
			if (is_array($this->ps_category->name))
				return $this->ps_category->name[OU_PS_Base::GetCookie()->id_lang];
			else
				return $this->ps_category->name;
		}
		
		public function getId()
		{
			return $this->ps_category->id;
		}
		
		public function GetImageId()
		{
			return $this->ps_category->id_image;
		}
		
		public function img($type, $subid=false)
		{
			if ($subid)
				$id = $this->ps_category->id . "-" . $subid;
			else
				$id = $this->GetImageId();
			if (!file_exists(dirname(__FILE__) . "/../../../../../" . _THEME_CAT_DIR_ . $id . "-" . $type . ".jpg"))
				return false;
			return "http://" . OU_PS_Base::GetLink()->getCatImageLink($this->ps_category->link_rewrite, $id, $type);
		}
		
		public function link()
		{
			return OU_PS_Base::GetLink()->getCategoryLink($this->ps_category->id, $this->ps_category->link_rewrite, OU_PS_Base::GetCookie()->id_lang);
		}
		
		public function attrs($group_id = false)
		{
			
			
			if ($group_id !== false)
			{
				if (!isset($this->tmp["attrs_filtered"])) $this->tmp["attrs_filtered"] = array();
				if (isset($this->tmp["attrs_filtered"][$group_id]))
				{
					
					$newRes = $this->tmp["attrs_filtered"][$group_id];
					
				}else{
					
					$attrs = $this->attrs();
					
					$newRes = array();
					
					foreach ($attrs as $v)
					{
						if ($v->isGroup($group_id))
						{
							$newRes[] = $v;
						}
					}
				}
				
				$this->tmp["attrs_filtered"][$group_id] = $newRes;
				
				return $newRes;
				
			}
			
			
			
			if (isset($this->tmp["attrs"]))
			{
				$newRes = $this->tmp["attrs"];
			}
			else
			{
				OU_Config::IncClassHelper("OU_PS_Attribute");
				$sql = 'SELECT
							a.*
						FROM
							(
								(
									(
										(ps_product p INNER JOIN ps_category_product cp ON p.id_product = cp.id_product)
											INNER JOIN ps_product_attribute pa ON pa.id_product = p.id_product
									) INNER JOIN ps_product_attribute_combination pac ON pac.id_product_attribute = pa.id_product_attribute
								) INNER JOIN ps_attribute a ON a.id_attribute = pac.id_attribute
							) 
						WHERE
							cp.id_category = ' . $this->ps_category->id .'
						GROUP BY
							a.id_attribute';

				$db = Db::getInstance(_PS_USE_SQL_SLAVE_);
				$res = $db->ExecuteS($sql);			
				
				$newRes = array();
				$valsRes = array();
				
				foreach ($res as $r)
				{
					$at = OU_PS_Attribute::FromArray($r);
					$newRes[] = $at;
					$valsRes[] = $at->getName();
				}
				
				array_multisort($valsRes, $newRes);
				
			}
			
			$this->tmp["attrs"] = $newRes;
			
			return $newRes;
						
		}
		
		public function attrGroups()
		{
			
			if (isset($this->tmp["attrGroups"]))
			{
				$newRes = $this->tmp["attrGroups"];
			}else{
				OU_Config::IncClassHelper("OU_PS_Attribute_Group");
				$sql = 'SELECT
							a.*
						FROM
							(
								(
									(
										(ps_product p INNER JOIN ps_category_product cp ON p.id_product = cp.id_product)
											INNER JOIN ps_product_attribute pa ON pa.id_product = p.id_product
									) INNER JOIN ps_product_attribute_combination pac ON pac.id_product_attribute = pa.id_product_attribute
								) INNER JOIN ps_attribute a ON a.id_attribute = pac.id_attribute
							) 
						WHERE
							cp.id_category = ' . $this->ps_category->id .'
						GROUP BY
							a.id_attribute_group';
							
				$db = Db::getInstance(_PS_USE_SQL_SLAVE_);
				$res = $db->ExecuteS($sql);
				
				$newRes = array();
				
				foreach ($res as $r)
				{
					$newRes[] = OU_PS_Attribute_Group::FromArray($r);
				}
			}
			
			$this->tmp["attrGroups"] = $newRes;
			
			return $newRes;
						
		}
		
		public function manufacturers()
		{
			
			if (isset($this->tmp["manufacturers"]))
			{
				$newRes = $this->tmp["manufacturers"];
			}else{

				OU_Config::IncClassHelper("OU_PS_Manufacturer");
				$sql = 'SELECT
							m.*
						FROM
							(ps_product p INNER JOIN ps_category_product cp ON p.id_product = cp.id_product)
							INNER JOIN ps_manufacturer m ON p.id_manufacturer = m.id_manufacturer
						WHERE
							cp.id_category = ' . $this->ps_category->id .'
						GROUP BY
							m.id_manufacturer';
							
				$db = Db::getInstance(_PS_USE_SQL_SLAVE_);
				$res = $db->ExecuteS($sql);
				
				$newRes = array();
				
				foreach ($res as $r)
				{
					$newRes[] = OU_PS_Manufacturer::FromArray($r);
				}
			}
			
			$this->tmp["manufacturers"] = $newRes;
			
			return $newRes;
						
		}
		
		public function __construct($ps_category)
		{
			$this->ps_category = $ps_category;
		}
		
		public static function FromArray($a)
		{
			if (isset($a["infos"]) && isset($a["infos"]["id_category"]))
				return self::FromID($a["infos"]["id_category"]);
			else
				return false;
		}
		
		public static function FromID($id)
		{
			return self::FromObject(new Category($id, OU_PS_Base::GetCookie()->id_lang));
		}
		
		public static function FromObject($obj)
		{
			return new OU_PS_Category($obj);
		}
		
		public function subcategories()
		{
			return OU_PS_Category::ListCategories($this->getId());
		}
		
		public static function ListCategories($id_parent)
		{
			
			if (!isset(self::$tmpStatic["ListCategories"])) self::$tmpStatic["ListCategories"] = array();
			
			if (!isset(self::$tmpStatic["ListCategories"][$id_parent]))
			{
			
				if ($id_parent == null)
					$sqlf = "";
				else
					$sqlf = "AND c.`id_parent`='$id_parent'";
				$res = Category::getCategories(OU_PS_Base::GetCookie()->id_lang, true, true, $sqlf);
				
				$newRes = array();
				
				if (isset($res[$id_parent]))
					foreach ($res[$id_parent] as $k=>$v)
					{
						$newRes[$k] = OU_PS_Category::FromArray($v);
					}
				
				self::$tmpStatic["ListCategories"][$id_parent] = $newRes;
				
			}
			
			return self::$tmpStatic["ListCategories"][$id_parent];
			
		}
		
	}

?>