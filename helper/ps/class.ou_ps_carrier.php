<?php

	OU_Config::IncClassHelper("OU_PS_Base");
	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 03/08/2012
	 */
	 
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @subpackage	OU Prestashop Helper
	 * @version		$Id: class.ou_ps_carrier.php 135 2012-11-19 11:51:45Z xaguilarf $
	 */
	class OU_PS_Carrier extends OU_PS_Base
	{
		
		public $ps_carrier = NULL;
		public $tmp = array();
		public static $tmpStatic = array();
		
		public function getPrice($ou_cart, $useTax = true)
		{
			return $ou_cart->getShippingCost($this, $useTax);
		}
		
		public function getDelay()
		{
			if (is_array($this->ps_carrier->delay))
				return $this->ps_carrier->delay[OU_PS_Base::GetCookie()->id_lang];
			else
				return $this->ps_carrier->delay;
		}
		
		public function getName()
		{
			if (is_array($this->ps_carrier->name))
				return $this->ps_carrier->name[OU_PS_Base::GetCookie()->id_lang];
			else
				return $this->ps_carrier->name;
		}	
		
		public function getId()
		{
			return $this->ps_carrier->id;
		}
		
		public function __construct($ps_carrier)
		{
			$this->ps_carrier = $ps_carrier;
		}
		
		public static function FromArray($a)
		{
			if (isset($a["id_carrier"]))
				return self::FromID($a["id_carrier"]);
			else
				return false;
		}
		
		public static function FromID($id)
		{
			return self::FromObject(new Carrier($id, OU_PS_Base::GetCookie()->id_lang));
		}
		
		public static function FromObject($obj)
		{
			return new OU_PS_Carrier($obj);
		}
		
		public static function ListCarriers($id_zona = false)
		{
			$carriers = Carrier::getCarriers(OU_PS_Base::GetCookie()->id_lang, true, false, $id_zona);
			$a = array();
			foreach ($carriers as $carrier)
			{
				$a[$carrier["id_carrier"]] = OU_PS_Carrier::FromArray($carrier);
			}
			return $a;
		}
		
	}

?>