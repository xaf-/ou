<?php

	OU_Config::IncClassHelper("OU_PS_Base");
	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 03/08/2012
	 */
	 
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @subpackage	OU Prestashop Helper
	 * @version		$Id: class.ou_ps_manufacturer.php 135 2012-11-19 11:51:45Z xaguilarf $
	 */
	class OU_PS_Manufacturer extends OU_PS_Base
	{
		
		public $ps_manufacturer = NULL;
		public $tmp = array();
		public static $tmpStatic = array();
		
		public function __construct($ps_manufacturer)
		{
			$this->ps_manufacturer = $ps_manufacturer;
		}
		
		public function getId()
		{
			return $this->ps_manufacturer->id;
		}
		
		public function getName()
		{
			if (is_array($this->ps_manufacturer->name))
				return $this->ps_manufacturer->name[OU_PS_Base::GetCookie()->id_lang];
			else
				return $this->ps_manufacturer->name;
		}

		public function img($type)
		{
			if (isset($this->tmp["img"]))
				return $this->tmp["img"];
			else
			{
				if (file_exists(_PS_MANU_IMG_DIR_.'/'.$this->ps_manufacturer->id.'-'.$type.'.jpg'))
				{
					OU_Config::IncClass("OU_Path");
					$r = OU_Path::Path2Url(_PS_MANU_IMG_DIR_).'/'.$this->ps_manufacturer->id.'-'.$type.'.jpg';
				}else{
					$r = false;
					$this->tmp["img"] = $r;
				}
				return $r;
			}
	
		}

		public static function FromArray($a)
		{
			if (isset($a["id_manufacturer"]))
				return self::FromID($a["id_manufacturer"]);
			else
				return false;
		}
		
		public static function FromID($id)
		{
			$obj = new Manufacturer($id, OU_PS_Base::GetCookie()->id_lang);
			return $obj ? self::FromObject($obj) : false;
		}
		
		public static function FromObject($obj)
		{
			return new OU_PS_Manufacturer($obj);
		}
		
	}

?>