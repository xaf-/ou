<?php 

	/**
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 16/11/2012
	 */

	OU_Config::IncClass(
		array(
			"OU_Base"
		)
	);
	 
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @subpackage	OU Helper WSDL
	 * @version		$Id: class.ou_wsdl_descriptor.php 135 2012-11-19 11:51:45Z xaguilarf $
	 */
	class OU_WSDL_Descriptor extends OU_Base
	{
		
		/**
		 * @var OU_WSDL
		 */
		private $_wsdl;

        public $currentPort = null;
        public $currentOperation = null;
		
		/**
		 * @param OU_WSDL $wsdl
		 */
		public function __construct($wsdl){
			$this->_wsdl = $wsdl;
		}
		
		protected function _doHtml($content, $tag, $class = false)
		{
			if ($class)
				$sTag = "<$tag class=\"$class\">";
			else
				$sTag = "<$tag>";
			$eTag = "</$tag>";
			return "$sTag$content$eTag";
		}

        public static function Correct($str, $display=true)
        {
            if ($display) $str = str_replace(".", "_", $str);
            $str = OU_Array::FromArray(explode(":", $str));
            return $str->Last();
        }
		
		private function _code($data, $type)
		{
			switch ($type)
			{
				case "fnc":
					
					echo "<div class=codes>";
						
					$name = $data["name"];
					$in = $data["in"];
					$out = $data["out"];
                    if (!$in) $in = array();
                    if (!$out) $out = array();
					$doc = $data["documentation"];

					if (strlen($doc) > 100) $doc = substr($doc, 0, 100) . "...";

					$return = self::Correct(OU_Array::FromArray($out)->First());

					// Delphi
					$delphiComments = "/// <summary>\n/// \t$doc\n/// </summary>\n";
					$params = array(); foreach ($in as $k=>$v){
						$delphiComments .= "/// <param name=\"".self::Correct($k)."\"></param>\n";
						$params[] = "$k : " . self::Correct($v);
					}
					$delphiComments .= "/// <returns>\n/// \t$return\n/// </returns>\n";
					$delphi = "function " . self::Correct($name) . "(".implode("; ", $params).") : " . $return . ";";
						
					// C#
					$csComments = $delphiComments;
					$params = array(); foreach ($in as $k=>$v) $params[] = self::Correct($v) ." ". $k;
					$cs = "public " . $return ." " . self::Correct($name) .  "(".implode(", ", $params).");";
						
					// Php
					$phpComments = "/**\n * $doc\n";
					$params = array(); foreach ($in as $k=>$v) {
						$phpComments .= " * @var \t\$$k\t".self::Correct($v)."\n";
						$params[] = '$' . $k;
					}
					$phpComments .= " * @return \t" . $return . "\n";
					$phpComments .= " **/\n";
					$php = "public function " . self::Correct($name) .  "(".implode(", ", $params).");";
						
					echo "<b class=\"ltitle\">Delphi :</b>";
					echo OU_Debug::syntax($delphiComments  . $delphi, "delphi");
					echo "<b class=\"ltitle\">C# :</b>";
					echo OU_Debug::syntax($csComments  . $cs, "csharp");
					echo "<b class=\"ltitle\">PHP :</b>";
					echo OU_Debug::syntax($phpComments  . $php, "php");
					
					echo "</div>";
						
					break;
			}
		}		
		
		public function generate($callback = null)
		{
			
			echo "<h1>Soap" . $this->_wsdl->_server->wsdl->serviceName . "</h1>";
			
			if (($n = strpos($this->_wsdl->_options->uri, "//")) !== false)
				$n = strpos($this->_wsdl->_options->uri, "/", $n + 2);
			else
				$n = strpos($this->_wsdl->_options->uri, "/");
			
			$r = substr($this->_wsdl->_options->uri, $n);
			$r = OU_Path::Relative($_SERVER["REQUEST_URI"], $r);
			$r = trim($r, " /\\");
			
			
			$r = (OU_Path::Split($r, false));
			
			$section = "index";
			if (count($r) > 1)
				$section = "operation";
			else if (count($r) > 0)
				$section = "port";

			switch ($section)
			{
				case "operation":

					$port = $r[0];
					$operation = $r[1];

                    $this->currentPort = $port;
                    $this->currentOperation = $operation;

                    echo $this->_doHtml("Operacion $port : $operation", "h2");
						
					$data = $this->_wsdl->_server->wsdl->getOperationData($operation);
					
					$typeInput = $data["input"]["parts"];
					$typeOutput = $data["output"]["parts"];

                    if ($callback){
                        $callback("operation", $data);
                    }
					
					echo $this->_doHtml("Código", "h3");
					
					echo "<div class=wsdl_desc>$data[documentation]</div>";
					
					$this->_code(
						array(
							"action" => $data["soapAction"],
							"in" => $typeInput, 
							"out" => $typeOutput,
							"name" => $operation,
							"documentation" => $data["documentation"]
						), "fnc"
					);
					
					break;
					
				case "port":

                    $port = $r[0];

                    $this->currentPort = $port;

                    echo $this->_doHtml("Operaciones $port", "h2");

					foreach ($this->_wsdl->_server->wsdl->getOperations($port) as $k => $op)
					{
						echo '
							<div class="wsdl_operation">
								<span class="wsdl_name">
									<a href="'.$this->_wsdl->_options->uri . $port . "/" . $k . "/".'">
									'.$k.'
									</a>
								</span>
								<span class="wsdl_binding">'.$op["binding"].'</span>
								<span class="wsdl_endpoint">'.$op["endpoint"].'</span>
								<span class="wsdl_soapaction">'.$op["soapAction"].'</span>
								<span class="wsdl_style">'.$op["style"].'</span>
								<span class="wsdl_namespace">'.$op["namespace"].'</span>
								<span class="wsdl_transport">'.$op["transport"].'</span>
								<span class="wsdl_documentation">'.$op["documentation"].'</span>
							</div>
						';
					}
					
					break;
					
				case "index":
				default:
					
					echo $this->_doHtml("Puertos", "h2");
					
					foreach ($this->_wsdl->_server->wsdl->ports as $k => $port)
					{
						echo '
							<div class="wsdl_port">
								<span class="wsdl_name">
									<a href="'.$this->_wsdl->_options->uri . $k . '/">
									'.$k.'
									</a>
								</span>
								<span class="wsdl_binding">'.$port["binding"].'</span>
								<span class="wsdl_location">'.$port["location"].'</span>
								<span class="wsdl_bindingType">'.$port["bindingType"].'</span>
							</div>
						';
					}
					
					break;
			}
		}
	}

?>