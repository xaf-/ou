<?php

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 28/06/2012
	 */

	OU_Config::IncClass("OU_Database");
	 
	/**
	 * Proporciona la interfaz necesaria para la conexión con una base de datos SQL Server. Para generar consultas SQL vea OU_Query.  
	 * Vea OU_Database para más información. 
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @subpackage  OU DB Helper
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class OU_DB_SqlSrv extends OU_Database
	{
	
		public function Disconnect()
		{
			if (!$this->connected()) return;
			sqlsrv_close($this->_db);
			$this->ResetConnect();
		}
		
		/**
		 * (non-PHPdoc)
		 * @see OU_Database::Connect()
		 */
		public function Connect($host, $user, $pass, $dbname, $port = false)
		{ 
			
			$connectionInfo = array( 
				"Database"=>$dbname, 
				"UID"=>$user, 
				"PWD"=>$pass, 
				"CharacterSet" => $this->_options->charset
			);
			$this->_db = sqlsrv_connect( $host . ($port ? ",$port" : ""), $connectionInfo );
			
			return $this->_db ? true : false;
			
		}
		
		protected function getDefaultOptions()
		{
			return array(
				"host" => "localhost",
				"port" => "1433",
				"username" => "sa",
				"password" => "sa",
				"charset" => "UTF-8"
			);
		}

		public function slashes($str)
		{
			$str = stripslashes($str); 
    		$str = str_replace("'","''",$str); 
    		$str = str_replace("\0","[NULL]",$str);
    		return $str; 
			
		}
		
		public function getInsertId()
		{
			$id = false;
			$this->CheckConnect();
			$rs = sqlsrv_query($this->_db, "SELECT @@identity AS id");
			if ($row = sqlsrv_fetch_array($rs, SQLSRV_FETCH_NUMERIC)) {
				$id = trim($row[0]);
			}
			sqlsrv_free_stmt($rs);
			return $id;
		}
		
		public function LastError()
		{
		
			$e = array();
			
			$er = sqlsrv_errors();
			if ($er)
			{
				foreach ($er as $eri)
					$e[] = $eri["message"];
			}
			
			return count($e) > 0 ? $e : false;
			
		}
		
		/**
		 * @see OU_Database::Query()
		 * @return OU_DB_Results
		 */
		protected function InternalQuery($sql, $options = array())
		{
			$l_results = sqlsrv_query($this->_db, $sql);
			if ($l_results)
			{
				
				$a = $this->createResults($sql, $l_results, $options);
				
				$ar = new OU_Array();
				
				foreach (sqlsrv_field_metadata($l_results) as $field)
//				for ($i = 0; $i < sqlsrv_num_fields($l_results); $i++)
				{
					foreach ($field as $propName => $propValue)
						$field[strtolower($propName)] = $propValue;
//					$field = sqlsrv_get_field($l_results, $i);
					$new = $this->createResult($a, $options);
					$new->AddArray($field);
					$new->isModified = false;
					
					$ar->Add($new);
				}
				
				$a->setFields($ar);
				
				$this->InternalFetchFields($a, $options);
				
				/* $cnt = 0;
				while (($result = sqlsrv_fetch_array($l_results, SQLSRV_FETCH_ASSOC)) !== NULL)
				{
					$cnt++;
					$new = $this->createResult($a, $options);
					$new->AddArray($result);
					$new->isModified = false;
					$a->Add($new);
						
				} */
				return $a;
			}
			return false;
		}
		
		public function Fetch($results)
		{
			$r = $results->resource;
			
			$o = $this->getSeekToRow($results);
			if ($o === NULL)			
				$res = sqlsrv_fetch_array($r, SQLSRV_FETCH_ASSOC);
			else
				$res = sqlsrv_fetch_array($r, SQLSRV_FETCH_ASSOC, SQLSRV_SCROLL_ABSOLUTE, $o);
				
			if ($res === NULL) return NULL;
				
			$new = $this->createResult($results, $results->options);
			$new->AddArray($res);
			$new->isModified = false;
				
			// $results->Add($new);
			
			return $new;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see OU_Database::Count()
		 */
		public function Count($results)
		{
			$r = $results->resource;
			return sqlsrv_num_rows($r);
		}
		
		
		private $_seekToRow = array();
		private function removeSeekToRow($results)
		{
			foreach ($this->_seekToRow as $k => $v) {
				if ($v["r"] === $results){
					unset($this->_seekToRow[$k]);
					break;
				}
			}
		}
		
		private function addSeekToRow($results, $offset)
		{
			$this->removeSeekToRow($results);
			$this->_seekToRow[] = array("r" => $results, "o" => $offset);
		}
		
		private function getSeekToRow($results)
		{
			$r = NULL;
			foreach ($this->_seekToRow as $k => $v) {
				if ($v["r"] === $results){
					$r = $v["o"];
					break;
				}
			}
			$this->removeSeekToRow($results);
			return $r;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see OU_Database::Seek()
		 */
		public function Seek($results, $offset)
		{
			$this->addSeekToRow($results, $offset);
		}
		
		/**
		 * (non-PHPdoc)
		 * @see OU_Database::Free()
		 */
		public function Free($results)
		{
			$r = $results->resource;
			@sqlsrv_free_stmt($r);
		}
		
		public function fieldType($type)
		{
			switch ($type)
			{
				
				case 12: // SQL_VARCHAR
				case -1: // SQL_LONGVARCHAR
					return self::FIELDTYPE_STRING;
				default:
					return self::FIELDTYPE_OTHER;
			}
		}
		
	}

?>