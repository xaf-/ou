<?php

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 28/06/2012
	 */

	OU_Config::IncClass("OU_Database");
	 
	/**
	 * Proporciona la interfaz necesaria para la conexión con una base de datos MySQL. Si no se especifica, por defecto se 
	 * intentará conectar con localhost, usuario "root" y sin contraseña. Para generar consultas SQL vea OU_Query. Vea 
	 * OU_Database para más información. 
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @subpackage  OU DB Helper
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_db_mysql.php 340 2014-03-16 23:37:36Z xaguilarf $
	 */
	class OU_DB_MySQL extends OU_Database
	{
		
		const ER_DUP_ENTRY = 1062;
		
		
		public function Disconnect()
		{
			if (!$this->connected()) return;
			$this->_db->close();
			$this->ResetConnect();
		}
		
		/**
		 * (non-PHPdoc)
		 * @see OU_Database::Connect()
		 */
		public function Connect($host, $user, $pass, $dbname, $port = false)
		{ 
			$d = new mysqli();
			$this->_db = new mysqli($host, $user, $pass, $dbname, $port === false ? ini_get("mysqli.default_port") : $port);
			
			if ($this->_db->connect_errno != 0)
				$this->_db = null;

			if ($this->_db && $this->_options->charset)
				$this->_db->set_charset($this->_options->charset);
			return $this->_db ? true : false;
		}
		
		protected function getDefaultOptions()
		{
			return array(
				"host" => "localhost",
				"port" => ini_get("mysqli.default_port"),
				"username" => "root",
				"password" => ""
			);
		}

		public function slashes($str)
		{
			$this->CheckConnect();
			return $this->_db->real_escape_string($str);
		}
		
		public function getInsertId()
		{
			$this->CheckConnect();
			return $this->_db->insert_id;
		}
		
		private function _getErrorMsg($a)
		{
			if ($a->errno)
			{
				$msg = $a->error;
				switch ($a->errno)
				{
					case self::ER_DUP_ENTRY:
						$res = array();
						if (preg_match("/\'(.*?)\'/", $msg, $res))
							return sprintf("El valor '%s' ya existe. ", $res[1]);
						else
							return $msg;
					default:
						return $msg;
				}
			}
		}
		
		public function LastError()
		{
			$this->CheckConnect();
			$a = $this->_db;
			$e = array();
			
			if ($a->connect_errno) $e[] = $a->connect_error;
			if ($a->errno) 
			{
				$e[] = $this->_getErrorMsg($a);
			}
			
			return count($e) > 0 ? $e : false;
			
		}
		
		/**
		 * @see OU_Database::Query()
		 * @return OU_DB_Results
		 */
		protected function InternalQuery($sql, $options = array())
		{
			$results = $this->_db->query($sql);
			if ($results && $results instanceof mysqli_result)
			{
				
				$a = $this->createResults($sql, $results, $options);
				
				$ar = new OU_Array();
				foreach ($results->fetch_fields() as $field)
				{
					$new = $this->createResult($a, $options);
					$new->AddArray($field);
					$new->isModified = false;
					$ar->Add($new);
				}

				$a->setFields($ar);
				
				$this->InternalFetchFields($a, $options);
				/*
				$cnt = 0;
				while (($result = $results->fetch_assoc()) !== NULL)
				{
					$cnt++;
					$new = $this->createResult($a, $options);
					$new->AddArray($result);
					$new->isModified = false;
					$a->Add($new);
				}*/
				
				return /* $cnt === 0 ? false :  */$a;
			}
			return $results ? true : false;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see OU_Database::Count()
		 */
		public function Count($results)
		{
			$r = $results->resource;
			/* @var $r mysqli_result */
			return $r->num_rows;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see OU_Database::Fetch()
		 */
		public function Fetch($results)
		{
			$r = $results->resource;
			/* @var $r mysqli_result */
			$res = $r->fetch_assoc();
			
			if ($res === NULL) return NULL;
			
			$new = $this->createResult($results, $results->options);
			$new->AddArray($res);
			$new->isModified = false;
			
			// $results->Add($new);
			
			return $new;			
		}
		
		/**
		 * (non-PHPdoc)
		 * @see OU_Database::Free()
		 */
		public function Free($results)
		{
			$r = $results->resource;
			/* @var $r mysqli_result */
			@$r->free();
		}
		
		/**
		 * (non-PHPdoc)
		 * @see OU_Database::Seek()
		 */
		public function Seek($results, $offset)
		{
			$r = $results->resource;
			/* @var $r mysqli_result */
			$r->data_seek($offset);
		}
		
	}

?>