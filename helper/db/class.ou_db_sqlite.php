<?php

/*
 * The source code is given as is. The author is not responsible
 * for any possible damage done due to the use of this code.
 * The component can be freely used in any application. The complete
 * source code remains property of the author and may not be distributed,
 * published, given or sold in any form as such. No parts of the source
 * code can be included in any other component or application without
 * written authorization of fontcolor. 01/08/2012
 */

OU_Config::IncClass(
		array(
				"OU_Database",
				"OU_Path"
		)
);

/**
 * @author		fontcolor
 * @package		OU Framework
 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
 * @subpackage	OU Sqlite Helper
 * @version		$Id: class.ou_db_sqlite.php 340 2014-03-16 23:37:36Z xaguilarf $
 */
class OU_DB_Sqlite extends OU_Database
{

	public function Seek($results, $offset)
	{
		throw new Exception('Sqlite3 no have seek!');
	}

	public function Disconnect()
	{
		if (!$this->connected()) return;
		$this->_db->close();
		$this->ResetConnect();
	}

	public function createFunction($name, $callback){
		$db = $this->_db;
		/* @var $db SQLite3 */
		$db->createFunction($name, $callback);
	}

	/*
     * Cuando se abre la base de datos se utilizara el formato md5($user + _ + $pass) para desencriptar la información
     * @see OU_Database::Connect()
     * @param string $host Dirección de la carpeta donde contiene los archivos .sqlite
     * @param string $dbname Nombre del archivo sqllite a abrir
     */
	public function Connect($host, $user, $pass, $dbname, $port = false) {
		$this->_db = new SQLite3(OU_Path::Absolute($dbname, $host), SQLITE3_OPEN_READWRITE | SQLITE3_OPEN_CREATE, $user === false ? "" : md5($user . "_" . $pass));
		return $this->_db ? true : false;
	}

	public function getInsertId()
	{
		return $this->lastInsertRowID();
	}

	public function LastError()
	{
		return $this->_db->lastErrorMsg();
	}

	/**
	 * @see OU_Database::getDefaultOptions()
	 */
	protected function getDefaultOptions() {
		return array(
				"host" => "",
				"dbname" => "test.sqlite",
				"username" => false
		);
	}

	public function slashes($str)
	{
		return sqlite_escape_string($str);
	}

	/**
	 * @see OU_Database::Query()
	 */
	protected function InternalQuery($sql, $options = array()) {

		$options->fetch_all = true; // Force

		$results = $this->_db->query($sql);

		if ($results && $results instanceof SQLite3Result)
		{

			$a = $this->createResults($sql, $results, $options);



			$ar = new OU_Array();

			for ($i = 0; $i < $results->numColumns(); $i++)
			{
				$new = $this->createResult($a, $options);
				$new->AddArray(
						array(
							"name" => $results->columnName($i),
							"type" => $results->columnType($i)
						)
				);
				$new->isModified = false;
				$ar->Add($new);
			}

			$a->setFields($ar);


			$cnt = 0;
				// while ($this->Fetch($a) !== NULL) {}
				while ($result = $results->fetchArray(SQLITE3_ASSOC))
				{
					$cnt++;
					$new = $this->createResult($a, $options);
					$new->AddArray($result);
					$new->isModified = false;
					$a->Add($new);

				}

				return $cnt === 0 ? false : $a;
		}

		return $results ? true : false;

	}

	public function Fetch($results)
	{
		$r = $results->resource;
		/* @var $r SQLite3Result */

		$res = $r->fetchArray(SQLITE3_ASSOC);

		if ($res === NULL) return NULL;

		$new = $this->createResult($results, $results->options);
		$new->AddArray($res);
		$new->isModified = false;

		// $results->Add($new);

		return $new;
	}

	/**
	 * (non-PHPdoc)
	 * @see OU_Database::Count()
	 */
	public function Count($results)
	{
		// unavaliable
	}

	/**
	 * (non-PHPdoc)
	 * @see OU_Database::Free()
	 */
	public function Free($results)
	{
		$r = $results->resource;
		/* @var $r SQLite3Result */
		@$r->finalize();
	}

	public function fieldType($type)
	{
		switch ($type)
		{
			case SQLITE3_BLOB:
				return self::FIELDTYPE_BLOB;
			case SQLITE3_TEXT:
				return self::FIELDTYPE_STRING;
			default:
				return self::FIELDTYPE_OTHER;
		}
	}

}

