<?php

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 28/06/2012
	 */

	OU_Config::IncClass("OU_Database");
	 
	/**
	 * Proporciona la interfaz necesaria para la conexión con una base de datos SQL Server. Para generar consultas SQL vea OU_Query.  
	 * Vea OU_Database para más información. 
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @subpackage  OU DB Helper
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_db_mssql.php 340 2014-03-16 23:37:36Z xaguilarf $
	 */
	class OU_DB_MSSQL extends OU_Database
	{
		
		public function Disconnect()
		{
			if (!$this->connected()) return;
			mssql_close($this->_db);
			$this->ResetConnect();
		}
		
		/**
		 * (non-PHPdoc)
		 * @see OU_Database::Connect()
		 */
		public function Connect($host, $user, $pass, $dbname, $port = false)
		{ 
			$this->_db = mssql_connect(
						$host . ($port ? ":$port" : ""),
						$user,
						$pass
					);
			
			if ($this->_db)
				mssql_select_db($dbname, $this->_db);
				
			mssql_query("SET DATEFORMAT dmy;", $this->_db); // Aplicar format D-M-Y
			
			return $this->_db ? true : false;
		}
		
		protected function getDefaultOptions()
		{
			return array(
				"host" => "localhost",
				"port" => "1433",
				"username" => "sa",
				"password" => "sa"
			);
		}

		public function slashes($str)
		{
			$str = stripslashes($str); 
    		$str = str_replace("'","''",$str); 
    		$str = str_replace("\0","[NULL]",$str);
    		return $str; 
			
		}
		
		public function getInsertId()
		{
			$this->CheckConnect();
			$id = false;
			$rs = mssql_query("SELECT @@identity AS id", $this->_db);
			if ($row = mssql_fetch_row($rs)) {
				$id = trim($row[0]);
			}
			mssql_free_result($rs);
			return $id;
		}
		
		private function _getErrorMsg($a)
		{
			if ($a->errno)
			{
				$msg = $a->error;
				switch ($a->errno)
				{
					case self::ER_DUP_ENTRY:
						$res = array();
						if (preg_match("/\'(.*?)\'/", $msg, $res))
							return sprintf("El valor '%s' ya existe. ", $res[1]);
						else
							return $msg;
					default:
						return $msg;
				}
			}
		}
		
		public function LastError()
		{
		
			$e = array();
			
			$msg = mssql_get_last_message();
			if (trim($msg) != "") $e[] = $msg;
			
			return count($e) > 0 ? $e : false;
			
		}
		
		/**
		 * @see OU_Database::Query()
		 * @return OU_DB_Results
		 */
		protected function InternalQuery($sql, $options = array())
		{
		
			$l_results = mssql_query($sql, $this->_db);
			if ($l_results)
			{
			
				$a = $this->createResults($sql, $l_results, $options);
				
				$ar = new OU_Array();
				for ($i = 0; $i < mssql_num_fields($l_results); ++$i) {
					$field = (array)mssql_fetch_field($l_results, $i);
					$new = $this->createResult($a, $options);
					$new->AddArray($field);
					$new->isModified = false;
					$ar->Add($new);
				}
				
				$a->setFields($ar);
				
				$this->InternalFetchFields($a, $options);
				
				/* $cnt = 0;
				while (($result = mssql_fetch_assoc($l_results)) !== NULL)
				{
					$cnt++;
					$new = $this->createResult($a, $options);
					$new->AddArray($result);
					$new->isModified = false;
					$a->Add($new);
						
				} */
				return $a;
			}
			return false;
		}
		
		public function Fetch($results)
		{
			$r = $results->resource;
			$res = mssql_fetch_assoc($r);
			if ($res === false) return NULL;
			
			if (isset($this->_options->charset) && $this->_options->charset)
			{
				foreach ($results->getFields() as $field)
				{
					if ($this->fieldType($field->type) == self::FIELDTYPE_STRING)
					{
						$res[$field->name] = mb_convert_encoding($res[$field->name], $this->_options->charset, 'CP1252');
					}
				}
			}
		
			$new = $this->createResult($results, $results->options);
			$new->AddArray($res);
			$new->isModified = false;
		
			return $new;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see OU_Database::Free()
		 */
		public function Free($results)
		{
			$r = $results->resource;
			@mssql_free_result($r);
		}
		
		/**
		 * (non-PHPdoc)
		 * @see OU_Database::Count()
		 */
		public function Count($results)
		{
			$r = $results->resource;
			return mssql_num_rows($r);
		}
		
		public function fieldType($type)
		{
			switch ($type)
			{
				case "char":
				case "varchar":
				case "nvarchar":
				case "nchar":
				case "text":
				case 12: // SQL_VARCHAR
				case -1: // SQL_LONGVARCHAR
					return self::FIELDTYPE_STRING;
				case "binary":
				case "varbinary":
					return self::FIELDTYPE_BLOB;					
				default:
					return self::FIELDTYPE_OTHER;
			}
		}
		
		/**
		 * (non-PHPdoc)
		 * @see OU_Database::Seek()
		 */
		public function Seek($results, $offset)
		{
			$r = $results->resource;
			mssql_data_seek($r, $offset);
		}
		
	}

?>