<?php

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 28/06/2012
	 */

	OU_Config::IncClass("OU_Database");
	 
	/**
	 * Proporciona la interfaz necesaria para la conexión con una base de datos (ODBC). Para generar consultas SQL vea OU_Query.  
	 * Vea OU_Database para más información. 
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @subpackage  OU DB Helper
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class OU_DB_ODBC extends OU_Database
	{
	
	
		public function Disconnect()
		{
			if (!$this->connected()) return;
			odbc_close($this->_db);
			$this->ResetConnect();
		}
		
		/**
		 * (non-PHPdoc)
		 * @see OU_Database::Connect()
		 */
		public function Connect($host, $user, $pass, $dbname, $port = false)
		{ 
			
			$full_host = $host;
			if ($port) {
				if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')
					$full_host .= ",".$port;
				else
					$full_host .= ":".$port;
			}
			
			$dns = "Driver={".$this->_options->driver."};Server=$full_host;Database=$dbname;charset=UTF-8";
			if (isset($this->_options->dns)) $dns = $this->_options->dns;
			
			$this->_db = odbc_connect( $dns, $user, $pass );
			
			return $this->_db ? true : false;
			
		}
		
		protected function getDefaultOptions()
		{
			return array(
				"driver" => "MySQL ODBC 3.51 Driver", 
				"host" => "localhost",
				"port" => ini_get("mysqli.default_port"),
				"username" => "root",
				"password" => "",
				"charset" => "utf8"
			);
		}

		public function slashes($str)
		{
			$str = stripslashes($str); 
    		$str = str_replace("'","''",$str); 
    		$str = str_replace("\0","[NULL]",$str);
    		return $str; 
			
		}
		
		public function getInsertId()
		{
			$id = false;
			$this->CheckConnect();
			$rs = odbc_exec($this->_db, "SELECT @@identity AS id");
			if ($row = odbc_fetch_array($rs)) {
				$id = trim($row[0]);
			}
			odbc_free_result($rs);
			return $id;
		}
		
		public function LastError()
		{
		
			$e = array();
			
			$er = odbc_errormsg();
			if ($er)
			{
				$e[] = $er;
			}
			
			return count($e) > 0 ? $e : false;
			
		}
		
		/**
		 * @see OU_Database::Query()
		 * @return OU_DB_Results
		 */
		protected function InternalQuery($sql, $options = array())
		{
			$l_results = odbc_exec($this->_db, $sql);
			if ($l_results)
			{
				
				$a = $this->createResults($sql, $l_results, $options);
				
				$ar = new OU_Array();
				
				for ($i = 0; $i < odbc_num_fields($l_results); $i++)
				{
					$new = $this->createResult($a, $options);
					$field = array(
						"name" => odbc_field_name($l_results, $i + 1),
						"precision" => odbc_field_precision($l_results, $i + 1),
						"type" => odbc_field_type($l_results, $i + 1),
						"scale" => odbc_field_scale($l_results, $i + 1)
					);
					$new->AddArray($field);
					$new->isModified = false;
					$ar->Add($new);
				}
				
				$a->setFields($ar);
				
				$this->InternalFetchFields($a, $options);
				
				return $a;
			}
			return false;
		}

		/**
		 * (non-PHPdoc)
		 * @see OU_Database::Fetch()
		 */
		public function Fetch($results)
		{
			
			$r = $results->resource;
			
			$o = $this->getSeekToRow($results);
			if ($o === NULL)
				$res = odbc_fetch_array($r);
			else
				$res = odbc_fetch_array($r,$o);
			
			if ($res === false) return null;
				
			
			$new = $this->createResult($results, $results->options);
			$new->AddArray($res);
			$new->isModified = false;
			
			return $new;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see OU_Database::Count()
		 */
		public function Count($results)
		{
			$r = $results->resource;
			return odbc_num_rows($r);
		}
		
		
		private $_seekToRow = array();		
		private function removeSeekToRow($results)
		{
			foreach ($this->_seekToRow as $k => $v) {
				if ($v["r"] === $results){
					unset($this->_seekToRow[$k]);
					break;
				}
			}
		}
		
		private function addSeekToRow($results, $offset)
		{
			$this->removeSeekToRow($results);
			$this->_seekToRow[] = array("r" => $results, "o" => $offset);
		}
		
		private function getSeekToRow($results)
		{
			$r = NULL;
			foreach ($this->_seekToRow as $k => $v) {
				if ($v["r"] === $results){
					$r = $v["o"];
					break;
				}
			}
			$this->removeSeekToRow($results);
			return $r;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see OU_Database::Seek()
		 */
		public function Seek($results, $offset)
		{
			$this->addSeekToRow($results, $offset);
		}
		
		/**
		 * (non-PHPdoc)
		 * @see OU_Database::Free()
		 */
		public function Free($results)
		{
			$r = $results->resource;
			@odbc_free_result($r);
		}
		
		public function fieldType($type)
		{
			switch ($type)
			{
				case SQL_VARCHAR: // SQL_VARCHAR
				case SQL_LONGVARCHAR: // SQL_LONGVARCHAR
					return self::FIELDTYPE_STRING;
				default:
					return self::FIELDTYPE_OTHER;
			}
		}
		
	}

?>