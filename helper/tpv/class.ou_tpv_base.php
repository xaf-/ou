<?php

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 26/06/2012
	 */

	OU_Config::IncClass("OU_Base");
	
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_tpv_base.php 268 2013-07-16 10:58:56Z xaguilarf $
	 *
	 * @property OU_TPV_Base $tpv
	 * @property float $amount
	 * @property number $id
	 * @property OU_Options $config
	 */
	class OU_TPV_Transition extends OU_Base
	{
		private $_tpv;
		protected function getTpv(){ return $this->_tpv; }
		
		private $_amount = 0;
		protected function getAmount(){ return $this->_amount; }
		
		private $_id;
		protected function getId(){ return $this->_id; }
		
		private $_config;
		protected function getConfig(){ return $this->_config; }
		
		private $_interop;
		
		public function __construct($tpv, $id, $amount, $config = array())
		{
			$this->_id = $id;
			$this->_tpv = $tpv;
			$this->_amount = $amount;
			$this->_config = OU_Options::FromArray($config, $this->tpv->config);
		}
		
		public function form($options = array())
		{
			$options = OU_Options::FromArray(
				$options,
				array(
					"id" => false,
					"name" => false
				)
			);
				
			$html =	'<form method="post" action="'.$this->config->url_tpv.'" '.($options->id ? 'id="'.$options->id.'"' : '').' '.($options->name ? 'name="'.$options->name.'"' : '').'>';
			$html .= $this->tpv->formContent($this);
			$html .= '</form>';
			
			return $html;
		}
		
	}
	 
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @subpackage	OU TPV Helper
	 * @version		$Id: class.ou_tpv_base.php 268 2013-07-16 10:58:56Z xaguilarf $
	 * 
	 * @property OU_Options $config
	 */
	abstract class OU_TPV_Base extends OU_Base
	{
		
		private $_config;
		protected function getConfig() { return $this->_config; }
		
		public function __construct($config = array())
		{
			$this->_config = OU_Options::FromArray($config, $this->getDefaultConfig());
		}
		
		protected function getDefaultConfig(){
			return array(
				"language" => "ES",
				"url_tpv" => null,
				"url_end" => null,
				"url_response" => null				
			);
		}
		
		public function transition($id, $amount, $config = array())
		{
			return new OU_TPV_Transition(
				$this,
				$id,
				$amount,
				$config
			);
		}
		
		/**
		 * @param OU_TPV_Transition $trans
		 */
		public abstract function formContent($trans);
		
	}
	
?>