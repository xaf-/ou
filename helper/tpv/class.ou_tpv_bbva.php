<?php

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 26/06/2012
	 */

	OU_Config::IncClassHelper(
		"OU_TPV_Base"
	);
	 
	OU_Config::IncClass(
		array(
			"OU_Options"
		)
	);
	
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 */
	class OU_TPV_BBVA extends OU_TPV_Base
	{
		
		public static $ID_TERMINAL = 999999;
		public static $ID_TRADE = "B6080938100002";	
		public static $KEY = "09;77;00;03;02;06;03;02;04;73;09;79;07;7F;72;0B;04;1C;1E;1E";
		public static $URL = "http://w3.grupobbva.com/TLPV/tlpv/TLPV_pub_RecepOpModeloServidor";	
		public static $PASS = "";
		public static $CURRENCY = "978"; // EUR (http://www.nationsonline.org/oneworld/currencies.htm)
		public static $LANG = "es";
		public static $COUNTRY = "ES";
		public static $URL_END = false;
		public static $URL_COMERCIO_END = false;
		
		protected function getDefaultConfig()
		{
			return array(
				"language" => "es",
				"url_tpv" => "http://w3.grupobbva.com/TLPV/tlpv/TLPV_pub_RecepOpModeloServidor",
				"url_redirect" => null,
				"url_response" => null,

				"id_terminal" => 999999,
				"id_trade" => null,
				"key" => null,
				"pass" => null,
				"currency" => "978",
				"country" => "ES"
			);
		}
		
		/**
		 * Devuelve la transición segun los parametros _REQUEST y valida la firma.
		 * @return OU_TPV_Transition|boolean
		 */
		public function transitionFromRequest()
		{
			if (isset($_REQUEST["peticion"]))
			{
				$xml = simplexml_load_string($_REQUEST["peticion"]);
				if (
						isset($xml->respago) &&
						(isset($xml->respago->idtransaccion) && isset($xml->respago->importe))
					)
				{
					$pago = $xml->respago;
					$trans = $this->transition(intval($pago->idtransaccion), floatval($pago->importe));
					$trans->config->id_terminal = $pago->idterminal;
					$trans->config->idcomercio = $pago->id_trade;
					// Validar firma "Response"
					$firm = mb_strtoupper($this->firmResponse($trans, $pago->idtransaccion, $pago->importe, $pago->estado, $pago->coderror, $pago->codautorizacion));
					
					if (intval($pago->estado) === 2 && $firm == $pago->firma)
					{
						return $trans;
					}else{
						return false;
					}
				}else
					return false;
			}
		}
		
		/**
		 * (non-PHPdoc)
		 * @see OU_TPV_Base::formContent()
		 */
		public function formContent($trans)
		{
			$r =  '<input type="hidden" name="peticion" value="'.addslashes($this->xml($trans)).'" />';
			$r .= '<input type="hidden" name="idterminal" value="'.$trans->config->id_terminal.'" />';
			$r .= '<input type="hidden" name="idcomercio" value="'.$trans->config->id_trade.'" />';
			$r .= '<input type="hidden" name="idtransaccion" value="'.$trans->id.'" />';
			$r .= '<input type="hidden" name="moneda" value="'.$trans->config->currency.'" />';
			$r .= '<input type="hidden" name="urlcomercio" value="'.$trans->config->url_response.'" />';
			$r .= '<input type="hidden" name="pais" value="'.$trans->config->country.'" />';
			$r .= '<input type="hidden" name="urlredir" value="'.($trans->config->url_redirect).'" />';
			return $r;
		}
		
		/**
		 * @param OU_TPV_Transition $trans
		 */
		public function xml($trans)
		{
			$import = number_format(round($trans->amount * 100) / 100, 2, ',', '');
			$xml = '<tpv>';
			$xml .= '<oppago>';
				$xml .= '<idterminal>'.$trans->config->id_terminal.'</idterminal>';
				$xml .= '<idcomercio>'.$trans->config->id_trade.'</idcomercio>';
				$xml .= '<idtransaccion>'.$trans->id.'</idtransaccion>';
				$xml .= '<moneda>'.$trans->config->currency.'</moneda>';
				$xml .= '<importe>'.$import.'</importe>';
				$xml .= '<urlcomercio>'.$trans->config->url_response.'</urlcomercio>';
				$xml .= '<idioma>'.$trans->config->language.'</idioma>';
				$xml .= '<pais>'.$trans->config->country.'</pais>';
		        $xml .= '<urlredir>'.$trans->config->url_redirect.'</urlredir>';
				$xml .= '<firma>'.$this->firm($trans, $import).'</firma>';
			$xml .= '</oppago>';
			$xml .= '</tpv>';
			
			return $xml;
		
		}
		
		/**
		 * @param OU_TPV_Transition $trans
		 */
		private function firm($trans, $_amount)
		{
			$str = $_amount . "";
			$import = "";
			for ($i = 0; $i < strlen($str); $i++) if (is_numeric($str[$i])) $import .= $str[$i];
			return sha1(
					$trans->config->id_terminal .
					$trans->config->id_trade .
					$trans->id .
					$import .
					$trans->config->currency .
					$this->deobfuscate($trans)
			);
		}
		
		private function firmResponse($trans, $_id_t, $_import, $_estado, $_codeerror, $_codeauth)
		{
			$str = $_import . "";
			$import = "";
			for ($i = 0; $i < strlen($str); $i++)
			{
				if (is_numeric($str[$i]))
					$import .= $str[$i];
			}
			return sha1(
				$trans->config->id_terminal .
				$trans->config->id_trade .
				$_id_t .
				$import .
				$trans->config->currency .
				$_estado .
				$_codeerror .
				$_codeauth .
				$this->deobfuscate($trans)
			);
		}
		
		private function deobfuscate($trans)
		{
				
			$pal_sec_ofuscada = $trans->config->key;
			$clave_xor = $trans->config->pass . substr($trans->config->id_trade, 0, 9) . "***";
				
			$cad1_0 = "0";
			$cad2_0 = "00";
			$cad3_0 = "000";
			$cad4_0 = "0000";
			$cad5_0 = "00000";
			$cad6_0 = "000000";
			$cad7_0 = "0000000";
			$cad8_0 = "00000000";
			$pal_sec = "";
		
			$trozos = explode (";", $pal_sec_ofuscada);
			$tope = count($trozos);
		
			for ($i=0; $i<$tope ; $i++)
			{
				$res = "";
				$pal_sec_ofus_bytes[$i] = decbin(hexdec($trozos[$i]));
				if (strlen($pal_sec_ofus_bytes[$i]) == 7){ $pal_sec_ofus_bytes[$i] = $cad1_0.$pal_sec_ofus_bytes[$i]; }
				if (strlen($pal_sec_ofus_bytes[$i]) == 6){ $pal_sec_ofus_bytes[$i] = $cad2_0.$pal_sec_ofus_bytes[$i]; }
				if (strlen($pal_sec_ofus_bytes[$i]) == 5){ $pal_sec_ofus_bytes[$i] = $cad3_0.$pal_sec_ofus_bytes[$i]; }
				if (strlen($pal_sec_ofus_bytes[$i]) == 4){ $pal_sec_ofus_bytes[$i] = $cad4_0.$pal_sec_ofus_bytes[$i]; }
				if (strlen($pal_sec_ofus_bytes[$i]) == 3){ $pal_sec_ofus_bytes[$i] = $cad5_0.$pal_sec_ofus_bytes[$i]; }
				if (strlen($pal_sec_ofus_bytes[$i]) == 2){ $pal_sec_ofus_bytes[$i] = $cad6_0.$pal_sec_ofus_bytes[$i]; }
				if (strlen($pal_sec_ofus_bytes[$i]) == 1){ $pal_sec_ofus_bytes[$i] = $cad7_0.$pal_sec_ofus_bytes[$i]; }
				$pal_sec_xor_bytes[$i] = decbin(ord($clave_xor[$i]));
				if (strlen($pal_sec_xor_bytes[$i]) == 7){ $pal_sec_xor_bytes[$i] = $cad1_0.$pal_sec_xor_bytes[$i]; }
				if (strlen($pal_sec_xor_bytes[$i]) == 6){ $pal_sec_xor_bytes[$i] = $cad2_0.$pal_sec_xor_bytes[$i]; }
				if (strlen($pal_sec_xor_bytes[$i]) == 5){ $pal_sec_xor_bytes[$i] = $cad3_0.$pal_sec_xor_bytes[$i]; }
				if (strlen($pal_sec_xor_bytes[$i]) == 4){ $pal_sec_xor_bytes[$i] = $cad4_0.$pal_sec_xor_bytes[$i]; }
				if (strlen($pal_sec_xor_bytes[$i]) == 3){ $pal_sec_xor_bytes[$i] = $cad5_0.$pal_sec_xor_bytes[$i]; }
				if (strlen($pal_sec_xor_bytes[$i]) == 2){ $pal_sec_xor_bytes[$i] = $cad6_0.$pal_sec_xor_bytes[$i]; }
				if (strlen($pal_sec_xor_bytes[$i]) == 1){ $pal_sec_xor_bytes[$i] = $cad7_0.$pal_sec_xor_bytes[$i]; }
				for ($j=0; $j<8; $j++) (string)$res .= (int)$pal_sec_ofus_bytes[$i][$j] ^ (int)$pal_sec_xor_bytes[$i][$j];
				$xor[$i] = $res;
				$pal_sec .= chr(bindec($xor[$i]));
			}
		
			return $pal_sec;
			
		}
		
		/**
		 * @param OU_TPV_BBVA_Result $peticion
		 * @return boolean|object
		 *
		public static function processResult($peticion = FALSE)
		{
			if ($peticion === false && isset($_REQUEST["peticion"])) $peticion = $_REQUEST["peticion"];
			if ($peticion)
			{
				$xml = simplexml_load_string($peticion, "OU_TPV_BBVA_Result");
				
				if ($xml === false)
					return false;
				else
					return $xml;
			}else{
				return false;
			}
		}
		
		
		public static function getFirmResponse($_id_t, $_import, $_estado, $_codeerror, $_codeauth)
		{
			$str = $_import . "";
			$import = "";
			for ($i = 0; $i < strlen($str); $i++)
			{
				if (is_numeric($str[$i]))
					$import .= $str[$i];
			}
			return sha1(
				self::$ID_TERMINAL . 
				self::$ID_TRADE . 
				$_id_t . 
				$import . 
				self::$CURRENCY .
				$_estado .
				$_codeerror .
				$_codeauth .
				self::deobfuscate()
			);
		}
		*/
		
		
		
		
	}

?>