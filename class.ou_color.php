<?php


	/*
	 * The source code is given as is. The author is not responsible
	* for any possible damage done due to the use of this code.
	* The component can be freely used in any application. The complete
	* source code remains property of the author and may not be distributed,
	* published, given or sold in any form as such. No parts of the source
	* code can be included in any other component or application without
	* written authorization of fontcolor. 20/07/2012
	*/
	
	OU_Config::IncClass(
		array(
			"OU_Base",
			"OU_Array"
		)
	);
	
	
	/**
	 * Permite el control y manipulación de colores.
	 * 
	 * @property number $red Recibe el color rojo (0/255)
	 * @property number $green Recibe el color verde (0/255)
	 * @property number $blue Recibe el color azul (0/255)
	 * @property number $alpha Recibe el alpha del color (0.0/1.0)
	 * 
	 * @property number $hue Recibe el color de HSV (0/255) 
	 * @property number $saturation Recibe la saturación de HSV (0/100) 
	 * @property number $value Recibe el valor de HSV (0/100) 
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @example		test/examples/OU_Color.php Manipulación de colores
	 * @version		$Id: class.ou_color.php 314 2013-09-24 10:58:44Z xaguilarf $
	 */
	class OU_Color extends OU_Base
	{
		
		private $_r = 0;
		private $_g = 0;
		private $_b = 0;
		private $_a = false;
		
		/**
		 * Permite los siguientes valores: 
		 * 	string : #FF0000 - Valor hexadecimal. self::hex()
		 *  array : list(r,g,b) - Array RGB. self::rgb()
		 *  array : list(h,s,v) - Array HSV. self::hsv()
		 * @param string|array $value
		 */
		public function __construct($value)
		{
			if (is_string($value) && strlen($value) > 0 && $value{0} == "#")
			{
				$this->hex($value);
			}else if (is_array($value) && isset($value["r"]) && isset($value["g"]) && isset($value["b"]))
			{
				if (isset($value["a"])) 
					$this->rgba($value["r"], $value["g"], $value["b"], $value["a"]);
				else
					$this->rgb($value["r"], $value["g"], $value["b"]);
			}else if (is_array($value) && isset($value["h"]) && isset($value["s"]) && isset($value["v"]))
			{
				$this->hsv($value["h"], $value["s"], $value["v"]);
			}else if (is_array($value) && count($value) == 3)
			{
				$this->rgba($value[0], $value[1], $value[2]);
			}else if (is_array($value) && count($value) == 4)
			{
				$this->rgba($value[0], $value[1], $value[2], $value[3]);
			}
		}
		
		private static function _colorPalette($imageFile, $numColors, $granularity = 5)
		{
			
			$imageFile = realpath($imageFile);
			
			$granularity = max(1, abs((int)$granularity));
			$colors = array();
			$size = @getimagesize($imageFile);
			if($size === false)
			{
				user_error("Unable to get image size data");
				return false;
			}
			
			$ext = mb_strtolower(OU_Path::Ext($imageFile));
			switch ($ext)
			{
				case ".png":
					$img = @imagecreatefrompng($imageFile);
					break;
				case ".jpg":
				case ".jpeg":
					$img = @imagecreatefromjpeg($imageFile);
					break;
				case ".ico":
					OU_Config::IncExt("ico");
					$ico = new Ico($imageFile);
					$info = $ico->GetIconInfo(0);
					$size[0] = $info["Width"];
					$size[1] = $info["Height"];
					$img = $ico->GetIcon(0);
					break;
				default:
					$img = false;
			}
			
			// Andres mentioned in the comments the above line only loads jpegs,
			// and suggests that to load any file type you can use this:
			// $img = @imagecreatefromstring(file_get_contents($imageFile));
		
			if(!$img)
			{
				user_error("Unable to open image file");
				return false;
			}
			for($x = 0; $x < $size[0]; $x += $granularity)
			{
				for($y = 0; $y < $size[1]; $y += $granularity)
				{
					$thisColor = imagecolorat($img, $x, $y);
					$rgb = imagecolorsforindex($img, $thisColor);
					$red = round(round(($rgb['red'] / 0x33)) * 0x33);
					$green = round(round(($rgb['green'] / 0x33)) * 0x33);
					$blue = round(round(($rgb['blue'] / 0x33)) * 0x33);
					
					if (isset($rgb["alpha"]))
					{
						$alpha = round(round(($rgb['alpha'] / 0x33)) * 0x33);
						$thisRGB = sprintf('%02X%02X%02X%02X', $red, $green, $blue, $alpha);
					}
					else
						$thisRGB = sprintf('%02X%02X%02X', $red, $green, $blue);
					
					if(array_key_exists($thisRGB, $colors))
					{
						$colors[$thisRGB]++;
					}
					else
					{
						$colors[$thisRGB] = 1;
					}
				}
			}
			arsort($colors);
			return array_slice(array_keys($colors), 0, $numColors);
		}
		
		public static function getName($autoValue)
		{
			$color = OU_Color::fromAuto($autoValue);
			return $color->name();
		}
		
		/**
		 * Devuelve una array con los colores principales de la imagen.
		 * @param string $fileName Dirección local de la imagen
		 * @return OU_Array
		 */
		public static function fromImage($fileName)
		{
			$r = self::_colorPalette($fileName, 10, 4);
			$a = array();
			foreach ($r as $k => $v)
				$a[] = new OU_Color("#" . $r[$k]);
			return OU_Array::FromArray($a);
		}
		
		/**
		 * @param string $name
		 * @return OU_Color
		 */
		public static function fromName($name)
		{
			$name = strtolower($name);
			foreach (self::$names as $k=>$v)
			{
				if (strtolower($v) == $name)
				{
					return OU_Color::fromHex($k);
				}
			}
			return null;
		}
		
		/**
		 * 
		 */
		public static function fromRandom()
		{
			return self::fromRGB(rand(0, 255), rand(0, 255), rand(0, 255));
		}
		
		/**
		 * @param string $hex
		 * @return OU_Color
		 */
		public static function fromHex($hex)
		{
			return new OU_Color($hex . "");
		}
		
		/**
		 * @param number $r
		 * @param number $g
		 * @param number $b
		 * @param number $a
		 * @return OU_Color
		 */
		public static function fromRGB($r, $g, $b, $a = null)
		{
			$c = array("r" => $r, "g" => $g, "b" => $b);
			if ($a !== null) $c["a"] = $a;
			return new OU_Color($c);
		}
		
		/**
		 * @param mixed $value
		 * @return OU_Array
		 */
		public static function fromAuto($value)
		{
			return new OU_Color($value);
		}
		
		/**
		 * Devuelve el color en formato css
		 * @return string
		 */
		public function css()
		{
			// ! no %f. %f pot fer que "." sigui "," depen de la config.
			if ($this->alpha !== false)
				return sprintf("rgba(%d, %d, %d, %s)", $this->red, $this->green, $this->blue, number_format($this->alpha, 2, ".", ""));
			else
				return $this->hex();				
		}
		
		/**
		 * Establece o recibe el color en formato HEX (#FFFFFF)
		 * @param string $hex
		 * @return string
		 */
		public function hex($hex = false)
		{
			if ($hex !== false)
			{
				$color = STR_REPLACE('#', '', $hex);
				$r = ARRAY(
					'r' => HEXDEC(SUBSTR($color, 0, 2)),
					'g' => HEXDEC(SUBSTR($color, 2, 2)),
					'b' => HEXDEC(SUBSTR($color, 4, 2))
				);
				
				if (strlen($color) >= 8)
				{
					$r["a"] = hexdec(substr($color, 6, 2)) / 256;
					$this->rgba($r["r"], $r["g"], $r["b"], $r["a"]);
				}else{
					$this->rgb($r["r"], $r["g"], $r["b"]);
				}
				
				
			}
			return "#" . sprintf("%02X%02X%02X", $this->_r, $this->_g, $this->_b);
		}
		
		/**
		 * Establece o recibe el color en RGBA rgbA(255,255,255, 1)
		 * @param number $r
		 * @param number $g
		 * @param number $b
		 * @return multitype:number
		 */
		public function rgba($r = false, $g = false, $b = false, $a = false)
		{
			
			if ($r !== false) $this->red = $r;
			if ($g !== false) $this->green = $g;
			if ($b !== false) $this->blue = $b;
			if ($a !== false) $this->alpha = $a;
			
			if ($this->alpha)
				return array(
					"r" => $this->_r,
					"g" => $this->_g,
					"b" => $this->_b,
					"a" => $this->_a
				);
			else
				return array(
					"r" => $this->_r,
					"g" => $this->_g,
					"b" => $this->_b
				);
		}
		
		/**
		 * Establece o recibe el color en RGB rgb(255,255,255)
		 * @param number $r
		 * @param number $g
		 * @param number $b
		 * @return multitype:number
		 */
		public function rgb($r = false, $g = false, $b = false)
		{
			
			if ($r !== false) $this->red = $r;
			if ($g !== false) $this->green = $g;
			if ($b !== false) $this->blue = $b;
			
			return array(
				"r" => $this->_r,
				"g" => $this->_g,
				"b" => $this->_b
			);
		}
		
		private function _RGBtoHSL(array $rgb)
		{
			list($r,$g,$b) = $rgb;
			$r = ((float)$r) / 255.0;
		    $g = ((float)$g) / 255.0;
		    $b = ((float)$b) / 255.0;
		
		    $maxC = max($r, $g, $b);
		    $minC = min($r, $g, $b);
		
		    $l = ($maxC + $minC) / 2.0;
		
		    if($maxC == $minC)
		    {
		      $s = 0;
		      $h = 0;
		    }
		    else
		    {
		      if($l < .5)
		      {
		        $s = ($maxC - $minC) / ($maxC + $minC);
		      }
		      else
		      {
		        $s = ($maxC - $minC) / (2.0 - $maxC - $minC);
		      }
		      if($r == $maxC)
		        $h = ($g - $b) / ($maxC - $minC);
		      if($g == $maxC)
		        $h = 2.0 + ($b - $r) / ($maxC - $minC);
		      if($b == $maxC)
		        $h = 4.0 + ($r - $g) / ($maxC - $minC);
		
		      $h = $h / 6.0; 
		    }
		
		    $h = (int)round(360.0 * $h);
		    $s = (int)round(100.0 * $s);
		    $l = (int)round(100.0 * $l);
		    
		    $HSL = array("h" => $h, "s" => $s, "l" => $l);
		
		   return $HSL;
		}

		private function _HSLtoRGB(array $hsl)
		{
			list($h, $s, $l) = $hsl;
			$h = ((float)$h) / 360.00;
		    $s = ((float)$s) / 100.00;
		    $l = ((float)$l) / 100.00;
		
		    if($s == 0)
		    {
		      $r = $l;
		      $g = $l;
		      $b = $l;
		    }
		    else
		    {
		      if($l < .5)
		      {
		        $t2 = $l * (1.0 + $s);
		      }
		      else
		      {
		        $t2 = ($l + $s) - ($l * $s);
		      }
		      $t1 = 2.0 * $l - $t2;
		
		      $rt3 = $h + 1.0/3.0;
		      $gt3 = $h;
		      $bt3 = $h - 1.0/3.0;
		
		      if($rt3 < 0) $rt3 += 1.0;
		      if($rt3 > 1) $rt3 -= 1.0;
		      if($gt3 < 0) $gt3 += 1.0;
		      if($gt3 > 1) $gt3 -= 1.0;
		      if($bt3 < 0) $bt3 += 1.0;
		      if($bt3 > 1) $bt3 -= 1.0;
		
		      if(6.0 * $rt3 < 1) $r = $t1 + ($t2 - $t1) * 6.0 * $rt3;
		      elseif(2.0 * $rt3 < 1) $r = $t2;
		      elseif(3.0 * $rt3 < 2) $r = $t1 + ($t2 - $t1) * ((2.0/3.0) - $rt3) * 6.0;
		      else $r = $t1;
		
		      if(6.0 * $gt3 < 1) $g = $t1 + ($t2 - $t1) * 6.0 * $gt3;
		      elseif(2.0 * $gt3 < 1) $g = $t2;
		      elseif(3.0 * $gt3 < 2) $g = $t1 + ($t2 - $t1) * ((2.0/3.0) - $gt3) * 6.0;
		      else $g = $t1;
		
		      if(6.0 * $bt3 < 1) $b = $t1 + ($t2 - $t1) * 6.0 * $bt3;
		      elseif(2.0 * $bt3 < 1) $b = $t2;
		      elseif(3.0 * $bt3 < 2) $b = $t1 + ($t2 - $t1) * ((2.0/3.0) - $bt3) * 6.0;
		      else $b = $t1;
		    }
		
		    $r = (int)round(255.0 * $r);
		    $g = (int)round(255.0 * $g);
		    $b = (int)round(255.0 * $b);
					
		    $RGB['r'] = $r;
		    $RGB['g'] = $g;
		    $RGB['b'] = $b;
		
		    return $RGB;
		    
		}
		
		private function _RGBtoHSV(array $rgb) {
		
			$f = 0.00000001; // Factor de corrección para evitar la división por cero.
		
			list($R,$G,$B) = $rgb;
		
			$R = $R==0?$f:$R/255;
			$G = $G==0?$f:$G/255;
			$B = $B==0?$f:$B/255;
		
			$V = max($R,$G,$B);
			$X = min($R,$G,$B);
			$S = ($V-$X)/$V;
		
			$V_X = $V-$X==0?$f:$V-$X;
		
			$r = ($V-$R)/($V_X);
			$g = ($V-$G)/($V_X);
			$b = ($V-$B)/($V_X);
		
			if ($R == $V)
				$H = $G==$X?(5+$b):(1-$g);
			elseif ($G == $V)
			$H = $B==$X?(1+$r):(3-$b);
			else
				$H = $R==$X?(3+$g):(5-$r);
		
			$H /= 6;
		
			$H = round($H*360);
			$S = round($S*100);
			$V = round($V*100);
		
			return array("h" => $H, "s" => $S, "v" => $V);
		}
		
		private function _HSVtoRGB(array $hsv) {
			
			list ( $H, $S, $V ) = $hsv;
			
			$H = $H / 360;
			$S = $S / 100;
			$V = $V / 100;
			
			// 1
			$H *= 6;
			// 2
			$I = floor ( $H );
			$F = $H - $I;
			// 3
			$M = $V * (1 - $S);
			$N = $V * (1 - $S * $F);
			$K = $V * (1 - $S * (1 - $F));
			// 4
			switch ($I) {
				case 0 :
					list ( $R, $G, $B ) = array (
							$V,
							$K,
							$M 
					);
					break;
				case 1 :
					list ( $R, $G, $B ) = array (
							$N,
							$V,
							$M 
					);
					break;
				case 2 :
					list ( $R, $G, $B ) = array (
							$M,
							$V,
							$K 
					);
					break;
				case 3 :
					list ( $R, $G, $B ) = array (
							$M,
							$N,
							$V 
					);
					break;
				case 4 :
					list ( $R, $G, $B ) = array (
							$K,
							$M,
							$V 
					);
					break;
				case 5 :
				case 6 : // for when $H=1 is given
					list ( $R, $G, $B ) = array (
							$V,
							$M,
							$N 
					);
					break;
			}
			
			$R = round ( $R * 255 );
			$G = round ( $G * 255 );
			$B = round ( $B * 255 );
			
			return array (
					"r" => $R,
					"g" => $G,
					"b" => $B 
			);
		}
		
		/**
		 * Establece o recibe el color en HSV
		 * @param number $h
		 * @param number $s
		 * @param number $v
		 * @return multitype:number 
		 */
		public function hsv($h = false, $s = false, $v = false)
		{
			
			if ($h !== false || $s !== false || $v !== false)
			{
				$r = $this->_HSVtoRGB(
					array(
						$h === false ? $this->hue : $h,
						$s === false ? $this->saturation : $s,
						$v === false ? $this->value : $v
					)
				);
				$this->rgb($r["r"], $r["g"], $r["b"]);	
			}
			return $this->_RGBtoHSV(array($this->red, $this->green, $this->blue));
			
		}
		
		/**
		 * Establece o recibe el color en HSL
		 * @param number $h
		 * @param number $s
		 * @param number $l
		 * @return multitype:number
		 */
		public function hsl($h = false, $s = false, $l = false)
		{
				
			if ($h !== false || $s !== false || $l !== false)
			{
				$r = $this->_HSLtoRGB(
						array(
								$h === false ? $this->hue : $h,
								$s === false ? $this->saturation : $s,
								$l === false ? $this->lightness : $l
						)
				);
				$this->rgb($r["r"], $r["g"], $r["b"]);
			}
			return $this->_RGBtoHSL(array($this->red, $this->green, $this->blue));
				
		}
		
		private static $names = array("#7CB9E8"=>"Aero", "#C9FFE5"=>"Aero blue", "#B284BE"=>"African violet", "#5D8AA8"=>"Air Force blue (RAF)", "#00308F"=>"Air Force blue (USAF)", "#72A0C1"=>"Air superiority blue", "#A32638"=>"Alabama Crimson", "#F0F8FF"=>"Alice blue", "#E32636"=>"Alizarin crimson", "#C46210"=>"Alloy orange", "#EFDECD"=>"Almond", "#E52B50"=>"Amaranth", "#3B7A57"=>"Amazon", "#FFBF00"=>"Amber", "#FF7E00"=>"SAE/ECE Amber (color)", "#FF033E"=>"American rose", "#9966CC"=>"Amethyst", "#A4C639"=>"Android green", "#F2F3F4"=>"Anti-flash white", "#CD9575"=>"Antique brass", "#665D1E"=>"Antique bronze", "#915C83"=>"Antique fuchsia", "#841B2D"=>"Antique ruby", "#FAEBD7"=>"Antique white", "#008000"=>"Ao (English)", "#8DB600"=>"Apple green", "#FBCEB1"=>"Apricot", "#00FFFF"=>"Aqua", "#7FFFD4"=>"Aquamarine", "#4B5320"=>"Army green", "#3B444B"=>"Arsenic", "#E9D66B"=>"Arylide yellow", "#B2BEB5"=>"Ash grey", "#87A96B"=>"Asparagus", "#FF9966"=>"Atomic tangerine", "#A52A2A"=>"Auburn", "#FDEE00"=>"Aureolin", "#6E7F80"=>"AuroMetalSaurus", "#568203"=>"Avocado", "#007FFF"=>"Azure", "#F0FFFF"=>"Azure mist/web", "#89CFF0"=>"Baby blue", "#A1CAF1"=>"Baby blue eyes", "#F4C2C2"=>"Baby pink", "#FEFEFA"=>"Baby powder", "#FF91AF"=>"Baker-Miller pink", "#21ABCD"=>"Ball blue", "#FAE7B5"=>"Banana Mania", "#FFE135"=>"Banana yellow", "#E0218A"=>"Barbie pink", "#7C0A02"=>"Barn red", "#848482"=>"Battleship grey", "#98777B"=>"Bazaar", "#BCD4E6"=>"Beau blue", "#9F8170"=>"Beaver", "#F5F5DC"=>"Beige", "#2E5894"=>"B'dazzled Blue", "#9C2542"=>"Big dip o’ruby", "#FFE4C4"=>"Bisque", "#3D2B1F"=>"Bistre", "#967117"=>"Bistre brown", "#CAE00D"=>"Bitter lemon", "#BFFF00"=>"Bitter lime", "#FE6F5E"=>"Bittersweet", "#BF4F51"=>"Bittersweet shimmer", "#000000"=>"Black", "#3D0C02"=>"Black bean", "#253529"=>"Black leather jacket", "#3B3C36"=>"Black olive", "#FFEBCD"=>"Blanched almond", "#A57164"=>"Blast-off bronze", "#318CE7"=>"Bleu de France", "#ACE5EE"=>"Blizzard Blue", "#FAF0BE"=>"Blond", "#0000FF"=>"Blue", "#1F75FE"=>"Blue (Crayola)", "#0093AF"=>"Blue (Munsell)", "#0087BD"=>"Blue (NCS)", "#333399"=>"Blue (pigment)", "#0247FE"=>"Blue (RYB)", "#A2A2D0"=>"Blue Bell", "#6699CC"=>"Blue-gray", "#0D98BA"=>"Blue-green", "#126180"=>"Blue sapphire", "#8A2BE2"=>"Blue-violet", "#5072A7"=>"Blue yonder", "#4F86F7"=>"Blueberry", "#1C1CF0"=>"Bluebonnet", "#DE5D83"=>"Blush", "#79443B"=>"Bole", "#0095B6"=>"Bondi blue", "#E3DAC9"=>"Bone", "#CC0000"=>"Boston University Red", "#006A4E"=>"Bottle green", "#873260"=>"Boysenberry", "#0070FF"=>"Brandeis blue", "#B5A642"=>"Brass", "#CB4154"=>"Brick red", "#1DACD6"=>"Bright cerulean", "#66FF00"=>"Bright green", "#BF94E4"=>"Bright lavender", "#C32148"=>"Bright maroon", "#FF007F"=>"Bright pink", "#08E8DE"=>"Bright turquoise", "#D19FE8"=>"Bright ube", "#F4BBFF"=>"Brilliant lavender", "#FF55A3"=>"Brilliant rose", "#FB607F"=>"Brink pink", "#004225"=>"British racing green", "#CD7F32"=>"Bronze", "#737000"=>"Bronze Yellow", "#964B00"=>"Brown (traditional)", "#A52A2A"=>"Brown (web)", "#6B4423"=>"Brown-nose", "#1B4D3E"=>"Brunswick green", "#FFC1CC"=>"Bubble gum", "#E7FEFF"=>"Bubbles", "#F0DC82"=>"Buff", "#480607"=>"Bulgarian rose", "#800020"=>"Burgundy", "#DEB887"=>"Burlywood", "#CC5500"=>"Burnt orange", "#E97451"=>"Burnt sienna", "#8A3324"=>"Burnt umber", "#BD33A4"=>"Byzantine", "#702963"=>"Byzantium", "#536872"=>"Cadet", "#5F9EA0"=>"Cadet blue", "#91A3B0"=>"Cadet grey", "#006B3C"=>"Cadmium green", "#ED872D"=>"Cadmium orange", "#E30022"=>"Cadmium red", "#FFF600"=>"Cadmium yellow", "#A67B5B"=>"Café au lait", "#4B3621"=>"Café noir", "#1E4D2B"=>"Cal Poly green", "#A3C1AD"=>"Cambridge Blue", "#C19A6B"=>"Camel", "#EFBBCC"=>"Cameo pink", "#78866B"=>"Camouflage green", "#FFEF00"=>"Canary yellow", "#FF0800"=>"Candy apple red", "#E4717A"=>"Candy pink", "#00BFFF"=>"Capri", "#592720"=>"Caput mortuum", "#C41E3A"=>"Cardinal", "#00CC99"=>"Caribbean green", "#960018"=>"Carmine", "#D70040"=>"Carmine (M&P)", "#EB4C42"=>"Carmine pink", "#FF0038"=>"Carmine red", "#FFA6C9"=>"Carnation pink", "#B31B1B"=>"Carnelian", "#99BADD"=>"Carolina blue", "#ED9121"=>"Carrot orange", "#00563F"=>"Castleton green", "#062A78"=>"Catalina blue", "#703642"=>"Catawba", "#C95A49"=>"Cedar Chest", "#92A1CF"=>"Ceil", "#ACE1AF"=>"Celadon", "#007BA7"=>"Celadon blue", "#2F847C"=>"Celadon green", "#B2FFFF"=>"Celeste (colour)", "#4997D0"=>"Celestial blue", "#DE3163"=>"Cerise", "#EC3B83"=>"Cerise pink", "#007BA7"=>"Cerulean", "#2A52BE"=>"Cerulean blue", "#6D9BC3"=>"Cerulean frost", "#007AA5"=>"CG Blue", "#E03C31"=>"CG Red", "#A0785A"=>"Chamoisee", "#F7E7CE"=>"Champagne", "#36454F"=>"Charcoal", "#232B2B"=>"Charleston green", "#E68FAC"=>"Charm pink", "#DFFF00"=>"Chartreuse (traditional)", "#7FFF00"=>"Chartreuse (web)", "#DE3163"=>"Cherry", "#FFB7C5"=>"Cherry blossom pink", "#954535"=>"Chestnut", "#DE6FA1"=>"China pink", "#A8516E"=>"China rose", "#AA381E"=>"Chinese red", "#856088"=>"Chinese violet", "#7B3F00"=>"Chocolate (traditional)", "#D2691E"=>"Chocolate (web)", "#FFA700"=>"Chrome yellow", "#98817B"=>"Cinereous", "#E34234"=>"Cinnabar", "#D2691E"=>"Cinnamon", "#E4D00A"=>"Citrine", "#9FA91F"=>"Citron", "#7F1734"=>"Claret", "#FBCCE7"=>"Classic rose", "#0047AB"=>"Cobalt", "#D2691E"=>"Cocoa brown", "#965A3E"=>"Coconut", "#6F4E37"=>"Coffee", "#9BDDFF"=>"Columbia blue", "#F88379"=>"Congo pink", "#002E63"=>"Cool black", "#8C92AC"=>"Cool grey", "#B87333"=>"Copper", "#DA8A67"=>"Copper (Crayola)", "#AD6F69"=>"Copper penny", "#CB6D51"=>"Copper red", "#996666"=>"Copper rose", "#FF3800"=>"Coquelicot", "#FF7F50"=>"Coral", "#F88379"=>"Coral pink", "#FF4040"=>"Coral red", "#893F45"=>"Cordovan", "#FBEC5D"=>"Corn", "#B31B1B"=>"Cornell Red", "#6495ED"=>"Cornflower blue", "#FFF8DC"=>"Cornsilk", "#FFF8E7"=>"Cosmic latte", "#FFBCD9"=>"Cotton candy", "#FFFDD0"=>"Cream", "#DC143C"=>"Crimson", "#BE0032"=>"Crimson glory", "#00FFFF"=>"Cyan", "#00B7EB"=>"Cyan (process)", "#58427C"=>"Cyber grape", "#FFD300"=>"Cyber yellow", "#FFFF31"=>"Daffodil", "#F0E130"=>"Dandelion", "#00008B"=>"Dark blue", "#666699"=>"Dark blue-gray", "#654321"=>"Dark brown", "#5D3954"=>"Dark byzantium", "#A40000"=>"Dark candy apple red", "#08457E"=>"Dark cerulean", "#986960"=>"Dark chestnut", "#CD5B45"=>"Dark coral", "#008B8B"=>"Dark cyan", "#536878"=>"Dark electric blue", "#B8860B"=>"Dark goldenrod", "#A9A9A9"=>"Dark gray", "#013220"=>"Dark green", "#00416A"=>"Dark imperial blue", "#1A2421"=>"Dark jungle green", "#BDB76B"=>"Dark khaki", "#483C32"=>"Dark lava", "#734F96"=>"Dark lavender", "#534B4F"=>"Dark liver", "#543D37"=>"Dark liver (horses)", "#8B008B"=>"Dark magenta", "#003366"=>"Dark midnight blue", "#4A5D23"=>"Dark moss green", "#556B2F"=>"Dark olive green", "#FF8C00"=>"Dark orange", "#9932CC"=>"Dark orchid", "#779ECB"=>"Dark pastel blue", "#03C03C"=>"Dark pastel green", "#966FD6"=>"Dark pastel purple", "#C23B22"=>"Dark pastel red", "#E75480"=>"Dark pink", "#003399"=>"Dark powder blue", "#872657"=>"Dark raspberry", "#8B0000"=>"Dark red", "#E9967A"=>"Dark salmon", "#560319"=>"Dark scarlet", "#8FBC8F"=>"Dark sea green", "#3C1414"=>"Dark sienna", "#8CBED6"=>"Dark sky blue", "#483D8B"=>"Dark slate blue", "#2F4F4F"=>"Dark slate gray", "#177245"=>"Dark spring green", "#918151"=>"Dark tan", "#FFA812"=>"Dark tangerine", "#483C32"=>"Dark taupe", "#CC4E5C"=>"Dark terra cotta", "#00CED1"=>"Dark turquoise", "#D1BEA8"=>"Dark vanilla", "#9400D3"=>"Dark violet", "#9B870C"=>"Dark yellow", "#00703C"=>"Dartmouth green", "#555555"=>"Davy's grey", "#D70A53"=>"Debian red", "#A9203E"=>"Deep carmine", "#EF3038"=>"Deep carmine pink", "#E9692C"=>"Deep carrot orange", "#DA3287"=>"Deep cerise", "#FAD6A5"=>"Deep champagne", "#B94E48"=>"Deep chestnut", "#704241"=>"Deep coffee", "#C154C1"=>"Deep fuchsia", "#004B49"=>"Deep jungle green", "#F5C71A"=>"Deep lemon", "#9955BB"=>"Deep lilac", "#CC00CC"=>"Deep magenta", "#D473D4"=>"Deep mauve", "#355E3B"=>"Deep moss green", "#FFCBA4"=>"Deep peach", "#FF1493"=>"Deep pink", "#843F5B"=>"Deep ruby", "#FF9933"=>"Deep saffron", "#00BFFF"=>"Deep sky blue", "#4A646C"=>"Deep Space Sparkle", "#7E5E60"=>"Deep Taupe", "#66424D"=>"Deep Tuscan red", "#BA8759"=>"Deer", "#1560BD"=>"Denim", "#C19A6B"=>"Desert", "#EDC9AF"=>"Desert sand", "#B9F2FF"=>"Diamond", "#696969"=>"Dim gray", "#9B7653"=>"Dirt", "#1E90FF"=>"Dodger blue", "#D71868"=>"Dogwood rose", "#85BB65"=>"Dollar bill", "#664C28"=>"Donkey Brown", "#967117"=>"Drab", "#00009C"=>"Duke blue", "#E5CCC9"=>"Dust storm", "#E1A95F"=>"Earth yellow", "#555D50"=>"Ebony", "#C2B280"=>"Ecru", "#614051"=>"Eggplant", "#F0EAD6"=>"Eggshell", "#1034A6"=>"Egyptian blue", "#7DF9FF"=>"Electric blue", "#FF003F"=>"Electric crimson", "#00FFFF"=>"Electric cyan", "#00FF00"=>"Electric green", "#6F00FF"=>"Electric indigo", "#F4BBFF"=>"Electric lavender", "#CCFF00"=>"Electric lime", "#BF00FF"=>"Electric purple", "#3F00FF"=>"Electric ultramarine", "#8F00FF"=>"Electric violet", "#FFFF33"=>"Electric yellow", "#50C878"=>"Emerald", "#1B4D3E"=>"English green", "#B48395"=>"English lavender", "#AB4B52"=>"English red", "#563C5C"=>"English violet", "#96C8A2"=>"Eton blue", "#44D7A8"=>"Eucalyptus", "#C19A6B"=>"Fallow", "#801818"=>"Falu red", "#B53389"=>"Fandango", "#DE5285"=>"Fandango pink", "#F400A1"=>"Fashion fuchsia", "#E5AA70"=>"Fawn", "#4D5D53"=>"Feldgrau", "#FDD5B1"=>"Feldspar", "#4F7942"=>"Fern green", "#FF2800"=>"Ferrari Red", "#6C541E"=>"Field drab", "#B22222"=>"Firebrick", "#CE2029"=>"Fire engine red", "#E25822"=>"Flame", "#FC8EAC"=>"Flamingo pink", "#6B4423"=>"Flattery", "#F7E98E"=>"Flavescent", "#EEDC82"=>"Flax", "#A2006D"=>"Flirt", "#FFFAF0"=>"Floral white", "#FFBF00"=>"Fluorescent orange", "#FF1493"=>"Fluorescent pink", "#CCFF00"=>"Fluorescent yellow", "#FF004F"=>"Folly", "#014421"=>"Forest green (traditional)", "#228B22"=>"Forest green (web)", "#A67B5B"=>"French beige", "#856D4D"=>"French bistre", "#0072BB"=>"French blue", "#86608E"=>"French lilac", "#9EFD38"=>"French lime", "#D473D4"=>"French mauve", "#C72C48"=>"French raspberry", "#F64A8A"=>"French rose", "#77B5FE"=>"French sky blue", "#AC1E44"=>"French wine", "#A6E7FF"=>"Fresh Air", "#FF00FF"=>"Fuchsia", "#C154C1"=>"Fuchsia (Crayola)", "#FF77FF"=>"Fuchsia pink", "#C74375"=>"Fuchsia rose", "#E48400"=>"Fulvous", "#CC6666"=>"Fuzzy Wuzzy", "#DCDCDC"=>"Gainsboro", "#E49B0F"=>"Gamboge", "#F8F8FF"=>"Ghost white", "#FE5A1D"=>"Giants orange", "#B06500"=>"Ginger", "#6082B6"=>"Glaucous", "#E6E8FA"=>"Glitter", "#00AB66"=>"GO green", "#D4AF37"=>"Gold (metallic)", "#FFD700"=>"Gold (web) (Golden)", "#85754E"=>"Gold Fusion", "#996515"=>"Golden brown", "#FCC200"=>"Golden poppy", "#FFDF00"=>"Golden yellow", "#DAA520"=>"Goldenrod", "#A8E4A0"=>"Granny Smith Apple", "#6F2DA8"=>"Grape", "#808080"=>"Gray", "#BEBEBE"=>"Gray (X11 gray)", "#465945"=>"Gray-asparagus", "#8C92AC"=>"Gray-blue", "#00FF00"=>"Green (color wheel) (X11 green)", "#1CAC78"=>"Green (Crayola)", "#008000"=>"Green (HTML/CSS color)", "#00A877"=>"Green (Munsell)", "#009F6B"=>"Green (NCS)", "#00A550"=>"Green (pigment)", "#66B032"=>"Green (RYB)", "#ADFF2F"=>"Green-yellow", "#A99A86"=>"Grullo", "#00FF7F"=>"Guppie green", "#663854"=>"Halayà úbe", "#446CCF"=>"Han blue", "#5218FA"=>"Han purple", "#E9D66B"=>"Hansa yellow", "#3FFF00"=>"Harlequin", "#C90016"=>"Harvard crimson", "#DA9100"=>"Harvest gold", "#808000"=>"Heart Gold", "#DF73FF"=>"Heliotrope", "#F400A1"=>"Hollywood cerise", "#F0FFF0"=>"Honeydew", "#006DB0"=>"Honolulu blue", "#49796B"=>"Hooker's green", "#FF1DCE"=>"Hot magenta", "#FF69B4"=>"Hot pink", "#355E3B"=>"Hunter green", "#71A6D2"=>"Iceberg", "#FCF75E"=>"Icterine", "#319177"=>"Illuminating Emerald", "#602F6B"=>"Imperial", "#002395"=>"Imperial blue", "#66023C"=>"Imperial purple", "#ED2939"=>"Imperial red", "#B2EC5D"=>"Inchworm", "#138808"=>"India green", "#CD5C5C"=>"Indian red", "#E3A857"=>"Indian yellow", "#6F00FF"=>"Indigo", "#00416A"=>"Indigo (dye)", "#4B0082"=>"Indigo (web)", "#002FA7"=>"International Klein Blue", "#FF4F00"=>"International orange (aerospace)", "#BA160C"=>"International orange (engineering)", "#C0362C"=>"International orange (Golden Gate Bridge)", "#5A4FCF"=>"Iris", "#B3446C"=>"Irresistible", "#F4F0EC"=>"Isabelline", "#009000"=>"Islamic green", "#B2FFFF"=>"Italian sky blue", "#FFFFF0"=>"Ivory", "#00A86B"=>"Jade", "#264348"=>"Japanese indigo", "#5B3256"=>"Japanese violet", "#F8DE7E"=>"Jasmine", "#D73B3E"=>"Jasper", "#A50B5E"=>"Jazzberry jam", "#DA614E"=>"Jelly Bean", "#343434"=>"Jet", "#F4CA16"=>"Jonquil", "#BDDA57"=>"June bud", "#29AB87"=>"Jungle green", "#4CBB17"=>"Kelly green", "#7C1C05"=>"Kenyan copper", "#3AB09E"=>"Keppel", "#C3B091"=>"Khaki (HTML/CSS) (Khaki)", "#F0E68C"=>"Khaki (X11) (Light khaki)", "#882D17"=>"Kobe", "#E79FC4"=>"Kobi", "#354230"=>"Kombu green", "#E8000D"=>"KU Crimson", "#087830"=>"La Salle Green", "#D6CADD"=>"Languid lavender", "#26619C"=>"Lapis lazuli", "#FFFF66"=>"Laser Lemon", "#A9BA9D"=>"Laurel green", "#CF1020"=>"Lava", "#B57EDC"=>"Lavender (floral)", "#E6E6FA"=>"Lavender (web)", "#CCCCFF"=>"Lavender blue", "#FFF0F5"=>"Lavender blush", "#C4C3D0"=>"Lavender gray", "#9457EB"=>"Lavender indigo", "#EE82EE"=>"Lavender magenta", "#E6E6FA"=>"Lavender mist", "#FBAED2"=>"Lavender pink", "#967BB6"=>"Lavender purple", "#FBA0E3"=>"Lavender rose", "#7CFC00"=>"Lawn green", "#FFF700"=>"Lemon", "#FFFACD"=>"Lemon chiffon", "#CCA01D"=>"Lemon curry", "#FDFF00"=>"Lemon glacier", "#E3FF00"=>"Lemon lime", "#F6EABE"=>"Lemon meringue", "#FFF44F"=>"Lemon yellow", "#1A1110"=>"Licorice", "#FDD5B1"=>"Light apricot", "#ADD8E6"=>"Light blue", "#B5651D"=>"Light brown", "#E66771"=>"Light carmine pink", "#F08080"=>"Light coral", "#93CCEA"=>"Light cornflower blue", "#F56991"=>"Light crimson", "#E0FFFF"=>"Light cyan", "#F984EF"=>"Light fuchsia pink", "#FAFAD2"=>"Light goldenrod yellow", "#D3D3D3"=>"Light gray", "#90EE90"=>"Light green", "#F0E68C"=>"Light khaki", "#D39BCB"=>"Light medium orchid", "#ADDFAD"=>"Light moss green", "#E6A8D7"=>"Light orchid", "#B19CD9"=>"Light pastel purple", "#FFB6C1"=>"Light pink", "#E97451"=>"Light red ochre", "#FFA07A"=>"Light salmon", "#FF9999"=>"Light salmon pink", "#20B2AA"=>"Light sea green", "#87CEFA"=>"Light sky blue", "#778899"=>"Light slate gray", "#B0C4DE"=>"Light steel blue", "#B38B6D"=>"Light taupe", "#E68FAC"=>"Light Thulian pink", "#FFFFE0"=>"Light yellow", "#C8A2C8"=>"Lilac", "#BFFF00"=>"Lime (color wheel)", "#00FF00"=>"Lime (web) (X11 green)", "#32CD32"=>"Lime green", "#9DC209"=>"Limerick", "#195905"=>"Lincoln green", "#FAF0E6"=>"Linen", "#C19A6B"=>"Lion", "#6CA0DC"=>"Little boy blue", "#674C47"=>"Liver", "#B86D29"=>"Liver (dogs)", "#6C2E1F"=>"Liver (organ)", "#987456"=>"Liver chestnut", "#FFE4CD"=>"Lumber", "#E62020"=>"Lust", "#FF00FF"=>"Magenta", "#FF55A3"=>"Magenta (Crayola)", "#CA1F7B"=>"Magenta (dye)", "#D0417E"=>"Magenta (Pantone)", "#FF0090"=>"Magenta (process)", "#AAF0D1"=>"Magic mint", "#F8F4FF"=>"Magnolia", "#C04000"=>"Mahogany", "#FBEC5D"=>"Maize", "#6050DC"=>"Majorelle Blue", "#0BDA51"=>"Malachite", "#979AAA"=>"Manatee", "#FF8243"=>"Mango Tango", "#74C365"=>"Mantis", "#880085"=>"Mardi Gras", "#C32148"=>"Maroon (Crayola)", "#800000"=>"Maroon (HTML/CSS)", "#B03060"=>"Maroon (X11)", "#E0B0FF"=>"Mauve", "#915F6D"=>"Mauve taupe", "#EF98AA"=>"Mauvelous", "#73C2FB"=>"Maya blue", "#E5B73B"=>"Meat brown", "#66DDAA"=>"Medium aquamarine", "#0000CD"=>"Medium blue", "#E2062C"=>"Medium candy apple red", "#AF4035"=>"Medium carmine", "#F3E5AB"=>"Medium champagne", "#035096"=>"Medium electric blue", "#1C352D"=>"Medium jungle green", "#DDA0DD"=>"Medium lavender magenta", "#BA55D3"=>"Medium orchid", "#0067A5"=>"Medium Persian blue", "#9370DB"=>"Medium purple", "#BB3385"=>"Medium red-violet", "#AA4069"=>"Medium ruby", "#3CB371"=>"Medium sea green", "#80DAEB"=>"Medium sky blue", "#7B68EE"=>"Medium slate blue", "#C9DC87"=>"Medium spring bud", "#00FA9A"=>"Medium spring green", "#674C47"=>"Medium taupe", "#48D1CC"=>"Medium turquoise", "#79443B"=>"Medium Tuscan red", "#D9603B"=>"Medium vermilion", "#C71585"=>"Medium violet-red", "#F8B878"=>"Mellow apricot", "#F8DE7E"=>"Mellow yellow", "#FDBCB4"=>"Melon", "#0A7E8C"=>"Metallic Seaweed", "#9C7C38"=>"Metallic Sunburst", "#E4007C"=>"Mexican pink", "#191970"=>"Midnight blue", "#004953"=>"Midnight green (eagle green)", "#E3F988"=>"Midori", "#FFC40C"=>"Mikado yellow", "#3EB489"=>"Mint", "#F5FFFA"=>"Mint cream", "#98FF98"=>"Mint green", "#FFE4E1"=>"Misty rose", "#FAEBD7"=>"Moccasin", "#967117"=>"Mode beige", "#73A9C2"=>"Moonstone blue", "#AE0C00"=>"Mordant red 19", "#8A9A5B"=>"Moss green", "#30BA8F"=>"Mountain Meadow", "#997A8D"=>"Mountbatten pink", "#18453B"=>"MSU Green", "#306030"=>"Mughal green", "#C54B8C"=>"Mulberry", "#FFDB58"=>"Mustard", "#317873"=>"Myrtle green", "#F6ADC6"=>"Nadeshiko pink", "#2A8000"=>"Napier green", "#FADA5E"=>"Naples yellow", "#FFDEAD"=>"Navajo white", "#000080"=>"Navy blue", "#9457EB"=>"Navy purple", "#FFA343"=>"Neon Carrot", "#FE4164"=>"Neon fuchsia", "#39FF14"=>"Neon green", "#214FC6"=>"New Car", "#D7837F"=>"New York pink", "#A4DDED"=>"Non-photo blue", "#059033"=>"North Texas Green", "#E9FFDB"=>"Nyanza", "#0077BE"=>"Ocean Boat Blue", "#CC7722"=>"Ochre", "#008000"=>"Office green", "#43302E"=>"Old burgundy", "#CFB53B"=>"Old gold", "#FDF5E6"=>"Old lace", "#796878"=>"Old lavender", "#673147"=>"Old mauve", "#867E36"=>"Old moss green", "#C08081"=>"Old rose", "#848482"=>"Old silver", "#808000"=>"Olive", "#6B8E23"=>"Olive Drab (web) (Olive Drab #3)", "#3C341F"=>"Olive Drab #7", "#9AB973"=>"Olivine", "#353839"=>"Onyx", "#B784A7"=>"Opera mauve", "#FF7F00"=>"Orange (color wheel)", "#FF7538"=>"Orange (Crayola)", "#FF5800"=>"Orange (Pantone)", "#FB9902"=>"Orange (RYB)", "#FFA500"=>"Orange (web color)", "#FF9F00"=>"Orange peel", "#FF4500"=>"Orange-red", "#DA70D6"=>"Orchid", "#F28DCD"=>"Orchid pink", "#FB4F14"=>"Orioles orange", "#654321"=>"Otter brown", "#414A4C"=>"Outer Space", "#FF6E4A"=>"Outrageous Orange", "#002147"=>"Oxford Blue", "#990000"=>"OU Crimson Red", "#006600"=>"Pakistan green", "#273BE2"=>"Palatinate blue", "#682860"=>"Palatinate purple", "#BCD4E6"=>"Pale aqua", "#AFEEEE"=>"Pale blue", "#987654"=>"Pale brown", "#AF4035"=>"Pale carmine", "#9BC4E2"=>"Pale cerulean", "#DDADAF"=>"Pale chestnut", "#DA8A67"=>"Pale copper", "#ABCDEF"=>"Pale cornflower blue", "#E6BE8A"=>"Pale gold", "#EEE8AA"=>"Pale goldenrod", "#98FB98"=>"Pale green", "#DCD0FF"=>"Pale lavender", "#F984E5"=>"Pale magenta", "#FADADD"=>"Pale pink", "#DDA0DD"=>"Pale plum", "#DB7093"=>"Pale red-violet", "#96DED1"=>"Pale robin egg blue", "#C9C0BB"=>"Pale silver", "#ECEBBD"=>"Pale spring bud", "#BC987E"=>"Pale taupe", "#AFEEEE"=>"Pale turquoise", "#DB7093"=>"Pale violet-red", "#78184A"=>"Pansy purple", "#FFEFD5"=>"Papaya whip", "#50C878"=>"Paris Green", "#AEC6CF"=>"Pastel blue", "#836953"=>"Pastel brown", "#CFCFC4"=>"Pastel gray", "#77DD77"=>"Pastel green", "#F49AC2"=>"Pastel magenta", "#FFB347"=>"Pastel orange", "#DEA5A4"=>"Pastel pink", "#B39EB5"=>"Pastel purple", "#FF6961"=>"Pastel red", "#CB99C9"=>"Pastel violet", "#FDFD96"=>"Pastel yellow", "#800080"=>"Patriarch", "#536878"=>"Payne's grey", "#FFE5B4"=>"Peach", "#FFCBA4"=>"Peach (Crayola)", "#FFCC99"=>"Peach-orange", "#FFDAB9"=>"Peach puff", "#FADFAD"=>"Peach-yellow", "#D1E231"=>"Pear", "#EAE0C8"=>"Pearl", "#88D8C0"=>"Pearl Aqua", "#B768A2"=>"Pearly purple", "#E6E200"=>"Peridot", "#CCCCFF"=>"Periwinkle", "#1C39BB"=>"Persian blue", "#00A693"=>"Persian green", "#32127A"=>"Persian indigo", "#D99058"=>"Persian orange", "#F77FBE"=>"Persian pink", "#701C1C"=>"Persian plum", "#CC3333"=>"Persian red", "#FE28A2"=>"Persian rose", "#EC5800"=>"Persimmon", "#CD853F"=>"Peru", "#DF00FF"=>"Phlox", "#000F89"=>"Phthalo blue", "#123524"=>"Phthalo green", "#C30B4E"=>"Pictorial carmine", "#FDDDE6"=>"Piggy pink", "#01796F"=>"Pine green", "#FFC0CB"=>"Pink", "#FFDDF4"=>"Pink lace", "#FF9966"=>"Pink-orange", "#E7ACCF"=>"Pink pearl", "#F78FA7"=>"Pink Sherbet", "#93C572"=>"Pistachio", "#E5E4E2"=>"Platinum", "#8E4585"=>"Plum (traditional)", "#DDA0DD"=>"Plum (web)", "#86608E"=>"Pomp and Power", "#FF5A36"=>"Portland Orange", "#B0E0E6"=>"Powder blue (web)", "#FF8F00"=>"Princeton orange", "#701C1C"=>"Prune", "#003153"=>"Prussian blue", "#DF00FF"=>"Psychedelic purple", "#CC8899"=>"Puce", "#FF7518"=>"Pumpkin", "#800080"=>"Purple (HTML/CSS)", "#9F00C5"=>"Purple (Munsell)", "#A020F0"=>"Purple (X11)", "#69359C"=>"Purple Heart", "#9678B6"=>"Purple mountain majesty", "#FE4EDA"=>"Purple pizzazz", "#50404D"=>"Purple taupe", "#51484F"=>"Quartz", "#436B95"=>"Queen blue", "#E8CCD7"=>"Queen pink", "#5D8AA8"=>"Rackley", "#FF355E"=>"Radical Red", "#FBAB60"=>"Rajah", "#E30B5D"=>"Raspberry", "#915F6D"=>"Raspberry glace", "#E25098"=>"Raspberry pink", "#B3446C"=>"Raspberry rose", "#826644"=>"Raw umber", "#FF33CC"=>"Razzle dazzle rose", "#E3256B"=>"Razzmatazz", "#8D4E85"=>"Razzmic Berry", "#FF0000"=>"Red", "#EE204D"=>"Red (Crayola)", "#F2003C"=>"Red (Munsell)", "#C40233"=>"Red (NCS)", "#ED2939"=>"Red (Pantone)", "#ED1C24"=>"Red (pigment)", "#FE2712"=>"Red (RYB)", "#A52A2A"=>"Red-brown", "#860111"=>"Red devil", "#FF5349"=>"Red-orange", "#C71585"=>"Red-violet", "#A45A52"=>"Redwood", "#522D80"=>"Regalia", "#002387"=>"Resolution blue", "#777696"=>"Rhythm", "#004040"=>"Rich black", "#F1A7FE"=>"Rich brilliant lavender", "#D70040"=>"Rich carmine", "#0892D0"=>"Rich electric blue", "#A76BCF"=>"Rich lavender", "#B666D2"=>"Rich lilac", "#B03060"=>"Rich maroon", "#444C38"=>"Rifle green", "#00CCCC"=>"Robin egg blue", "#8A7F80"=>"Rocket metallic", "#838996"=>"Roman silver", "#FF007F"=>"Rose", "#F9429E"=>"Rose bonbon", "#674846"=>"Rose ebony", "#B76E79"=>"Rose gold", "#E32636"=>"Rose madder", "#FF66CC"=>"Rose pink", "#AA98A9"=>"Rose quartz", "#C21E56"=>"Rose red", "#905D5D"=>"Rose taupe", "#AB4E52"=>"Rose vale", "#65000B"=>"Rosewood", "#D40000"=>"Rosso corsa", "#BC8F8F"=>"Rosy brown", "#0038A8"=>"Royal azure", "#002366"=>"Royal blue (traditional)", "#4169E1"=>"Royal blue (web)", "#CA2C92"=>"Royal fuchsia", "#7851A9"=>"Royal purple", "#FADA5E"=>"Royal yellow", "#CE4676"=>"Ruber", "#D10056"=>"Rubine red", "#E0115F"=>"Ruby", "#9B111E"=>"Ruby red", "#FF0028"=>"Ruddy", "#BB6528"=>"Ruddy brown", "#E18E96"=>"Ruddy pink", "#A81C07"=>"Rufous", "#80461B"=>"Russet", "#679267"=>"Russian green", "#32174D"=>"Russian violet", "#B7410E"=>"Rust", "#DA2C43"=>"Rusty red", "#00563F"=>"Sacramento State green", "#8B4513"=>"Saddle brown", "#FF6700"=>"Safety orange (blaze orange)", "#EED202"=>"Safety yellow", "#F4C430"=>"Saffron", "#23297A"=>"St. Patrick's blue", "#FF8C69"=>"Salmon", "#FF91A4"=>"Salmon pink", "#C2B280"=>"Sand", "#967117"=>"Sand dune", "#ECD540"=>"Sandstorm", "#F4A460"=>"Sandy brown", "#967117"=>"Sandy taupe", "#92000A"=>"Sangria", "#507D2A"=>"Sap green", "#0F52BA"=>"Sapphire", "#0067A5"=>"Sapphire blue", "#CBA135"=>"Satin sheen gold", "#FF2400"=>"Scarlet", "#FD0E35"=>"Scarlet (Crayola)", "#FF91AF"=>"Schauss pink", "#FFD800"=>"School bus yellow", "#76FF7A"=>"Screamin' Green", "#006994"=>"Sea blue", "#2E8B57"=>"Sea green", "#321414"=>"Seal brown", "#FFF5EE"=>"Seashell", "#FFBA00"=>"Selective yellow", "#704214"=>"Sepia", "#8A795D"=>"Shadow", "#FFCFF1"=>"Shampoo", "#009E60"=>"Shamrock green", "#8FD400"=>"Sheen Green", "#D98695"=>"Shimmering Blush", "#FC0FC0"=>"Shocking pink", "#FF6FFF"=>"Shocking pink (Crayola)", "#882D17"=>"Sienna", "#C0C0C0"=>"Silver", "#ACACAC"=>"Silver chalice", "#5D89BA"=>"Silver Lake blue", "#C4AEAD"=>"Silver pink", "#BFC1C2"=>"Silver sand", "#CB410B"=>"Sinopia", "#007474"=>"Skobeloff", "#87CEEB"=>"Sky blue", "#CF71AF"=>"Sky magenta", "#6A5ACD"=>"Slate blue", "#708090"=>"Slate gray", "#003399"=>"Smalt (Dark powder blue)", "#C84186"=>"Smitten", "#738276"=>"Smoke", "#933D41"=>"Smokey topaz", "#100C08"=>"Smoky black", "#FFFAFA"=>"Snow", "#CEC8EF"=>"Soap", "#757575"=>"Sonic silver", "#9E1316"=>"Spartan Crimson", "#1D2951"=>"Space cadet", "#80755A"=>"Spanish bistre", "#D10047"=>"Spanish carmine", "#E51A4C"=>"Spanish crimson", "#E86100"=>"Spanish orange", "#00AAE4"=>"Spanish sky blue", "#0FC0FC"=>"Spiro Disco Ball", "#A7FC00"=>"Spring bud", "#00FF7F"=>"Spring green", "#007BB8"=>"Star command blue", "#4682B4"=>"Steel blue", "#CC3366"=>"Steel pink", "#FADA5E"=>"Stil de grain yellow", "#990000"=>"Stizza", "#4F666A"=>"Stormcloud", "#E4D96F"=>"Straw", "#FC5A8D"=>"Strawberry", "#FFCC33"=>"Sunglow", "#E3AB57"=>"Sunray", "#FAD6A5"=>"Sunset", "#FD5E53"=>"Sunset orange", "#CF6BA9"=>"Super pink", "#D2B48C"=>"Tan", "#F94D00"=>"Tangelo", "#F28500"=>"Tangerine", "#FFCC00"=>"Tangerine yellow", "#E4717A"=>"Tango pink", "#483C32"=>"Taupe", "#8B8589"=>"Taupe gray", "#D0F0C0"=>"Tea green", "#F88379"=>"Tea rose (orange)", "#F4C2C2"=>"Tea rose (rose)", "#008080"=>"Teal", "#367588"=>"Teal blue", "#99E6B3"=>"Teal deer", "#00827F"=>"Teal green", "#CF3476"=>"Telemagenta", "#CD5700"=>"Tenné (Tawny)", "#E2725B"=>"Terra cotta", "#D8BFD8"=>"Thistle", "#DE6FA1"=>"Thulian pink", "#FC89AC"=>"Tickle Me Pink", "#0ABAB5"=>"Tiffany Blue", "#E08D3C"=>"Tiger's eye", "#DBD7D2"=>"Timberwolf", "#EEE600"=>"Titanium yellow", "#FF6347"=>"Tomato", "#746CC0"=>"Toolbox", "#FFC87C"=>"Topaz", "#FD0E35"=>"Tractor red", "#00755E"=>"Tropical rain forest", "#0073CF"=>"True Blue", "#417DC1"=>"Tufts Blue", "#FF878D"=>"Tulip", "#DEAA88"=>"Tumbleweed", "#B57281"=>"Turkish rose", "#30D5C8"=>"Turquoise", "#00FFEF"=>"Turquoise blue", "#A0D6B4"=>"Turquoise green", "#FAD6A5"=>"Tuscan", "#6F4E37"=>"Tuscan brown", "#7C4848"=>"Tuscan red", "#A67B5B"=>"Tuscan tan", "#C09999"=>"Tuscany", "#8A496B"=>"Twilight lavender", "#66023C"=>"Tyrian purple", "#0033AA"=>"UA blue", "#D9004C"=>"UA red", "#8878C3"=>"Ube", "#536895"=>"UCLA Blue", "#FFB300"=>"UCLA Gold", "#3CD070"=>"UFO Green", "#120A8F"=>"Ultramarine", "#4166F5"=>"Ultramarine blue", "#FF6FFF"=>"Ultra pink", "#635147"=>"Umber", "#FFDDCA"=>"Unbleached silk", "#5B92E5"=>"United Nations blue", "#B78727"=>"University of California Gold", "#FFFF66"=>"Unmellow yellow", "#014421"=>"UP Forest green", "#7B1113"=>"UP Maroon", "#AE2029"=>"Upsdell red", "#E1AD21"=>"Urobilin", "#004F98"=>"USAFA blue", "#990000"=>"USC Cardinal", "#FFCC00"=>"USC Gold", "#F77F00"=>"University of Tennessee Orange", "#D3003F"=>"Utah Crimson", "#F3E5AB"=>"Vanilla", "#F3D9DF"=>"Vanilla ice", "#C5B358"=>"Vegas gold", "#C80815"=>"Venetian red", "#43B3AE"=>"Verdigris", "#E34234"=>"Vermilion (cinnabar)", "#D9603B"=>"Vermilion (Plochere)", "#A020F0"=>"Veronica", "#8F00FF"=>"Violet", "#7F00FF"=>"Violet (color wheel)", "#8601AF"=>"Violet (RYB)", "#EE82EE"=>"Violet (web)", "#324AB2"=>"Violet-blue", "#F75394"=>"Violet-red", "#40826D"=>"Viridian", "#009698"=>"Viridian green", "#922724"=>"Vivid auburn", "#9F1D35"=>"Vivid burgundy", "#DA1D81"=>"Vivid cerise", "#CC00FF"=>"Vivid orchid", "#00CCFF"=>"Vivid sky blue", "#FFA089"=>"Vivid tangerine", "#9F00FF"=>"Vivid violet", "#004242"=>"Warm black", "#A4F4F9"=>"Waterspout", "#645452"=>"Wenge", "#F5DEB3"=>"Wheat", "#FFFFFF"=>"White", "#F5F5F5"=>"White smoke", "#A2ADD0"=>"Wild blue yonder", "#D77A02"=>"Wild orchid", "#FF43A4"=>"Wild Strawberry", "#FC6C85"=>"Wild Watermelon", "#AE6838"=>"Windsor tan", "#722F37"=>"Wine", "#673147"=>"Wine dregs", "#C9A0DC"=>"Wisteria", "#C19A6B"=>"Wood brown", "#738678"=>"Xanadu", "#0F4D92"=>"Yale Blue", "#1C2841"=>"Yankees blue", "#FFFF00"=>"Yellow", "#FCE883"=>"Yellow (Crayola)", "#EFCC00"=>"Yellow (Munsell)", "#FFD300"=>"Yellow (NCS)", "#FEDF00"=>"Yellow (Pantone)", "#FFEF00"=>"Yellow (process)", "#FEFE33"=>"Yellow (RYB)", "#9ACD32"=>"Yellow-green", "#FFAE42"=>"Yellow Orange", "#FFF000"=>"Yellow rose", "#0014A8"=>"Zaffre", "#2C1608"=>"Zinnwaldite brown", "#39A78E"=>"Zomp");
		
		/**
		 * Devuelve el nombre del color, si no se encuentra devuelve el valor en hex.
		 */
		public function name()
		{
			$hex = strtoupper($this->hex());
			$map =self::$names;
			return isset($map[$hex]) ? $map[$hex] : $hex;
			
		}
		
		//public function red;
		//public function green;
		//public function blue;
		
		protected function getRed() { return $this->_r; }
		protected function getGreen() { return $this->_g; }
		protected function getBlue() { return $this->_b; }
		protected function getAlpha() { return $this->_a; }
		
		protected function setRed($value){ $this->_r = max(0, min(255, $value)); }
		protected function setGreen($value){ $this->_g = max(0, min(255, $value)); }
		protected function setBlue($value){ $this->_b = max(0, min(255, $value)); }
		protected function setAlpha($value){ $this->_a = max(0, min(1, $value)); }
		
		// public function hue
		// public function saturation
		// public function value
		// public function lightness 
		
		protected function getHue() { $r = $this->hsv(); return $r["h"]; }
		protected function getSaturation() { $r = $this->hsv(); return $r["s"]; }
		protected function getValue() { $r = $this->hsv(); return $r["v"]; }
		protected function getLightness() { $r = $this->hsl(); return $r["l"]; }
		
		protected function setHue($value) { $this->hsv($value); }
		protected function setSaturation($value) { $this->hsv(false, $value); }
		protected function setValue($value) { $this->hsv(false, false, $value); }
		protected function setLightness($value) { $this->hsl(false, false, $value); }
		
	}

?>