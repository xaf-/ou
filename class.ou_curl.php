<?php

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 27/06/2012
	 */

	OU_Config::IncClass(
		array(
			"OU_Base",
			"OU_Options"
		)
	);
	 
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_curl.php 337 2013-11-25 12:03:56Z xaguilarf $
	 */
	class OU_CURL extends OU_Base
	{

		/**
		 * @see OU_CURL::Post()
		 */
		public static function Post($url, $data = NULL, $options = array())
		{
			
			$instance = self::Create();
			$res = $instance->Post($url, $data, $options);
			unset($instance);
			return $res;
            
		}
		
		/**
		 * @see OU_CURL::Get()
		 */
		public static function Get($url, $options = array())
		{
			
			$instance = self::Create();
			$res = $instance->Get($url, $options);
			unset($instance);
			return $res;
		
		}
		
		/**
		 * @return OU_CURL_Instance
		 */
		public static function Create()
		{
			return new OU_CURL_Instance();
		} 
		
		/**
		 * @return string
		 */
		public static function Version()
		{
			return curl_version();
		}
		
	}
	
	/**
	 * 
	 * @property string 	$url
	 * @property array 		$info
	 * @property boolean 	$returnTransfer
	 * @property boolean	$followLocation
	 * @property number		$connectTimeout
	 * @property number		$timeout
	 * @property string		$proxy
	 * @property string		$userAgent
	 * @property boolean	$header
	 * @property boolean	$verbose
	 * @property string		$referer
	 * @property OU_Array	$httpHeader
	 * @property bool		$sslVerifyPeer
	 * @property strnig		$cainfo
	 * @property strnig		$encoding
	 * 
	 * @property-read string $error
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_curl.php 337 2013-11-25 12:03:56Z xaguilarf $
	 */
	class OU_CURL_Instance extends OU_Base
	{
		
		public $_handle;
		private $_cookieTmp;
		
		public function __construct()
		{
			$this->_handle = curl_init();
			
			$this->_cookieTmp = tempnam(sys_get_temp_dir(), "cookie");
			chmod($this->_cookieTmp, 777);
			
			$this->_httpHeader = new OU_Array();
			
			curl_setopt($this->_handle, CURLOPT_COOKIEFILE, $this->_cookieTmp);
			curl_setopt($this->_handle, CURLOPT_COOKIEJAR, $this->_cookieTmp);
			
		}
		
		public function __destruct()
		{
			curl_close($this->_handle);
			@unlink($this->_cookieTmp);
		}
		
		private $_error;
		protected function getError(){ return curl_error($this->_handle); }
		
		protected function getInfo() { return curl_getinfo($this->_handle); }
		
		// url
		private $_url = false;
		protected function getUrl(){ return $this->_url;  }
		protected function setUrl($value) { $this->_url = $value; curl_setopt($this->_handle, CURLOPT_URL, $value); }
		
		private $_encoding = false;
		protected function getEncoding(){ return $this->_encoding; }
		protected function setEncoding($value){ $this->_encoding = $value; curl_setopt($this->_handle, CURLOPT_ENCODING, $value); }
		
		// httpHeader
		private $_httpHeader;
		protected function getHttpHeader() { return $this->_httpHeader; }
		
		// returnTransfer
		private $_returnTransfer = false;
		protected function getReturnTransfer(){ return $this->_returnTransfer;  }
		protected function setReturnTransfer($value) { $this->_returnTransfer = $value; curl_setopt($this->_handle, CURLOPT_RETURNTRANSFER, $value); }
		
		private $_cainfo;
		protected function getCainfo(){ return $this->_cainfo; }
		protected function setCainfo($value){ $this->_cainfo = $value; curl_setopt($this->_handle, CURLOPT_CAINFO, $value); }
		
		// Header
		private $_header = false;
		protected function getHeader(){ return $this->_header; }
		protected function setHeader($value){ var_dump($value); $this->_header = $value; curl_setopt($this->_handle, CURLOPT_HEADER, $value ? 1 : 0); }

		// Verbose
		private $_verbose = false;
		protected function getVerbose(){ return $this->_verbose; }
		protected function setVerbose($value){ $this->_verbose = $value; curl_setopt($this->_handle, CURLOPT_VERBOSE, $value ? 1 : 0); }
				
		// sslVerifyPeer
		private $_sslVerifyPeer = false;
		protected function getSslVerifyPeer(){ return $this->_sslVerifyPeer;  }
		protected function setSslVerifyPeer($value) { $this->_sslVerifyPeer = $value; curl_setopt($this->_handle, CURLOPT_SSL_VERIFYPEER, $value); }

		// userAgent
		private $_userAgent = false;
		protected function getUserAgent(){ return $this->_userAgent;  }
		protected function setUserAgent($value) { $this->_userAgent= $value; curl_setopt($this->_handle, CURLOPT_USERAGENT, $value); }
				
		// followLocation
		private $_followLocation = false;
		protected function getFollowLocation(){ return $this->_followLocation;  }
		protected function setFollowLocation($value) { $this->_followLocation = $value; curl_setopt($this->_handle, CURLOPT_FOLLOWLOCATION, $value); }
		
		// timeout
		private $_timeout = false;
		protected function getTimeout(){ return $this->_timeout;  }
		protected function setTimeout($value) { $this->_timeout = $value; curl_setopt($this->_handle, CURLOPT_TIMEOUT, $value); }
		
		// connectTimeout
		private $_connectTimeout = false;
		protected function getConnectTimeout(){ return $this->_connectTimeout;  }
		protected function setConnectTimeout($value) { $this->_connectTimeout = $value; curl_setopt($this->_handle, CURLOPT_CONNECTTIMEOUT, $value); }
		
		// proxy
		private $_proxy = false;
		protected function getProxy(){ return $this->_proxy;  }
		protected function setProxy($value) { $this->_proxy = $value; curl_setopt($this->_handle, CURLOPT_PROXY, $value); }
		
		// referer
		private $_referer = false;
		protected function getReferer(){ return $this->_referer;  }
		protected function setReferer($value) { $this->_referer = $value; curl_setopt($this->_handle, CURLOPT_REFERER, $value); }
		
		/**
		 * @param array|OU_Array $options
		 * @return mixed
		 */
		private function Exec($options = array())
		{
			$options = OU_Options::FromArray(
				$options,
				array(
					"returnTransfer" => true,
					"followLocation" => true,
					"timeout" => null,
					"connectTimeout" => null,
					"proxy" => null,
					"encoding" => null,
					"sslVerifyPeer" => null
				)
			);
			
			$last_timeout = $this->timeout;
			$last_connectTimeout = $this->connectTimeout;
			$last_followLocation = $this->followLocation;
			$last_returnTransfer = $this->returnTransfer;
			$last_sslVerifyPeer = $this->sslVerifyPeer;
			$last_proxy = $this->proxy;
			$last_encoding = $this->encoding;
			
			if ($this->httpHeader->count() > 0) curl_setopt($this->_handle, CURLOPT_HTTPHEADER, $this->httpHeader->toArray());
			
			$this->returnTransfer = $options->returnTransfer;
			$this->followLocation = $options->followLocation;
			if ($options->timeout != NULL) $this->timeout = $options->timeout;
			if ($options->connectTimeout != NULL) $this->connectTimeout = $options->connectTimeout;
			if ($options->proxy != NULL) $this->proxy = $options->proxy;
			if ($options->sslVerifyPeer != NULL) $this->sslVerifyPeer = $options->sslVerifyPeer;
			
			$r = curl_exec($this->_handle);
			
			$this->timeout = $last_timeout;
			$this->connectTimeout = $last_connectTimeout;
			$this->followLocation = $last_followLocation;
			$this->returnTransfer = $last_returnTransfer;
			$this->sslVerifyPeer = $last_sslVerifyPeer;
			$this->encoding = $last_encoding;
			$this->proxy = $last_proxy;
			
			return $r;
			
		}
		
		/**
		 * Permite hacer una solicitud POST a una URL
		 * @param string $url
		 * @param string $data
		 * @param array|OU_Options
		 * @return mixed
		 */
		public function Post($url, $data = NULL, $options = array())
		{
			$options = OU_Options::FromArray($options);
			
			$this->url = $url; 
			
            curl_setopt($this->_handle, CURLOPT_POST, TRUE);
            if ($data !== NULL)
            	curl_setopt($this->_handle, CURLOPT_POSTFIELDS, $data); 
            
            return $this->Exec($options);             
		}
		
		/**
		 * Permite hacer una solicitud GET a unra URL
		 * @param string $url
		 * @param array|OU_Options
		 * @return mixed
		 */
		public function Get($url, $options = array())
		{
			$options = OU_Options::FromArray($options);
			
			$this->url = $url;
			
			curl_setopt($this->_handle, CURLOPT_POSTFIELDS, "");
			curl_setopt($this->_handle, CURLOPT_POST, FALSE);
			
			return $this->Exec($options);			
		}
		
	}

?>