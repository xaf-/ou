<?php

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 02/08/2012
	 */
	 
	OU_Config::IncClass("OU_Base");
	
	/**
	 * Permite el control de las cabeceras de una manera mas práctica que la función nativa header()
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_header.php 303 2013-09-03 11:00:17Z xaguilarf $
	 */
	class OU_Header extends OU_Base
	{
		
		/**
		 * ETag de cache
		 * @param string $etag
		 */
		public static function ETag($etag)
		{
			header("ETag: $etag", true);
		}
		
		/**
		 * ETag de un archivo
		 * @param string $filename Ruta del archivo
		 * @return boolean
		 */
		public static function ETagFile($filename)
		{
			if (!file_exists($filename) || !($info = stat($filename))) {
				return false;
			}
			$quote = true;
			$q = ($quote) ? '"' : '';
			$etag = sprintf("$q%x-%x-%x$q", $info['ino'], $info['size'], $info['mtime']);
			OU_Header::ETag($etag);
		}
		
		/**
		 * Información de cabecera personalizada del compilador 
		 */
		public static function Compiler($data)
		{
			header("X-Compiler: $data");
		}
		
		/**
		 * Alias de header("Location: $path");
		 * @param string $path
		 */
		public static function Location($path)
		{
			header("Location: $path");
		}
		
		/**
		 * Alias de header("Refresh: $time; url=$url");
		 * @param number $time
		 * @param string $url
		 */
		public static function Refresh($time=10, $url)
		{
			header("Refresh: $time; url=$url");
		}
		
		/**
		 * Devuelve por header() el codigo de HTTP/1.1
		 * @param number $code
		 * @return boolean FALSE si no existe un mensaje por defecto, aun así se enviará al navegador el código.
		 */
		public static function Code($code)
		{
			switch ($code."")
			{
				case "200":
					header('HTTP/1.1 200 OK');
					break;
					
				case "202":
					header('HTTP/1.1 202 Accept');
					break;
					
				case "203":
					header("HTTP/1.1 203 Non-Authoritative Information");
					break;
				
				case "301":
					header('HTTP/1.1 301 Moved Permanently');
					break;
				case "304":
					header('HTTP/1.1 304 Not Modified');
					break;
					
				case "401":
					header('HTTP/1.1 401 Unauthorized');
					break;
				case "403":
					header('HTTP/1.1 403 Forbidden');
					break;
				case "404":
                    header('HTTP/1.1 404 Not Found');
                    break;
					
				case "500":
					header('HTTP/1.1 500 Internal Server Error');
					break;
					
				default:
					header('HTTP/1.1 ' . $code);
					return false;
					
			}
			
			return true;
			
		}
		
		/**
		 * Alias de header("X-Powered-By: $name");
		 * @param string $name
		 */
		public static function XPoweredBy($name)
		{
			header('X-Powered-By: ' . $name);
		}
		
		/**
		 * Alias de header("Content-language: $lang");
		 * @param string $lang
		 */
		public static function ContentLanguage($lang)
		{
			header('Content-language: ' . $lang);
		}
		
		/**
		 * Alias de header("Content-length: $length"); Si la directiva de PHP zlib.output_compression esta activada no se aplicará el header Content-Length
		 * @param number $length
		 */
		public static function ContentLength($length)
		{
			if (!ini_get("zlib.output_compression"))
				header("Content-length: " . $length);
		}
		
		/**
		 * Alias de header("Content-Encoding: $encoding");
		 * @param string $encoding
		 */
		public static function ContentEncoding($encoding)
		{
			header("Content-Encoding: " . $encoding);
		}
		
		/**
		 * Alias de header("Content-Type: $type");
		 * @param string $type
		 */
		public static function ContentType($type)
		{
			header("Content-Type: " . $type);
		}
		
		/**
		 * Alias de header("Content-Disposition: $value");
		 * @param string $value
		 */
		public static function ContentDisposition($value)
		{
			header("Content-Disposition: " . $value);
		}
		
		/**
		 * Alias de header("Content-Disposition: attachment; $filename");
		 * @param string $filename
		 */
		public static function ContentAttachment($filename)
		{
			header('Content-Disposition: attachment; filename="'.$filename.'"');
		}
		
		/**
		 * Alias de header("Content-Transfer-Encoding: $value");
		 * @param string $value
		 */
		public static function ContentTransferEncoding($value = "binary")
		{
			header('Content-Transfer-Encoding: ' . $value);
		}
		
		/**
		 * Alias de header("WWW-Authenticate: $type realm='$realm'");
		 * @param string $type
		 * @param string $realm
		 */
		public static function WWWAuthenticate($type="Basic", $realm="Top Secret")
		{
			header('WWW-Authenticate: '.$type.' realm="'.$realm.'"');
		}
		
		/**
		 * Alias de header("Pragma: $value");
		 * @param string $value
		 */
		public static function Pragma($value)
		{
			header('Pragma: ' . $value);
		}
		
		/**
		 * Alias de header("Expires: $time GMT");
		 * @param number $time
		 */
		public static function Expires($time)
		{
			header('Expires: ' . gmdate('D, d M Y H:i:s', $time) . ' GMT');
		}
		
		/**
		 * @see OU_Header::Expires()
		 * @param number $time_offset
		 */
		public static function ExpiresOffset($time_offset)
		{
			return OU_Header::Expires(time() + $time_offset);
		}
		
		/**
		 * Indica al navegador que no guarde en cache
		 * @see OU_Header::LastModified 
		 * @see OU_Header::CacheControl
		 */
		public static function NoCache()
		{
			OU_Header::LastModified(time());
			OU_Header::CacheControl("no-store, no-cache, must-revalidate");
			OU_Header::CacheControl("post-check=0, pre-check=0", false);
		}
		
		/**
		 * Alias de header("Cache-Control: $name");
		 * @param string $name
		 */
		public static function CacheControl($value)
		{
			header("Cache-Control: ".$value);
		}
		
		/**
		 * @see OU_Header::CacheControl()
		 * @param number $maxage
		 */
		public static function CacheControlMaxAge($maxage)
		{
			return OU_Header::CacheControl("max-age=".$maxage);
		}
		
		/**
		 * Combina Pragma(public) + CacheControlMaxAge() + ExpiresOffset()
		 * @param number $time
		 */
		public static function Cache($time)
		{
			OU_Header::Pragma("public");
			OU_Header::CacheControlMaxAge(max(60 * 60 * 24 * self::$DEFAULT_CACHE_TIME, $time - time()));
			OU_Header::Expires($time);
		}
		
		/**
		 * Combina Pragma(public) + CacheControlMaxAge() + ExpiresOffset()
		 * @param number $time_offset
		 */
		public static function CacheOffset($time_offset)
		{
			OU_Header::Pragma("public");
			OU_Header::CacheControlMaxAge($time_offset);
			OU_Header::ExpiresOffset($time_offset);
		}
		
		/**
		 * Alias de header("CLast modified: F d Y H:i:s.");
		 * @param Date $date
		 */
		public static function LastModified($date)
		{
			header("Last modified: ".date("F d Y H:i:s.", $date));
		}
		
		public static function GetContentType($file)
		{
			$ext = OU_Path::Ext($file, false);
			$ext = trim(strtolower($ext));
			
			$r = false;
			
			switch ($ext)
			{
                case ".docx": $r = "application/vnd.openxmlformats-officedocument.wordprocessingml.template"; break;
				case ".xml": $r = "application/xml"; break;
				case ".csv": $r = "text/csv"; break;
				case ".xpm": $r = "image/x-xpixmap"; break;
				case ".xbm": $r = "image/x-xbitmap"; break;
				case ".gif": $r = "image/gif"; break;
				case ".css": $r = "text/css"; break;
				case ".avi": $r = "video/x-msvideo"; break;
				case ".movie": $r = "video/x-sgi-movie"; break;
				case ".pdf": $r = "application/pdf"; break;
				case ".exe": $r = "application/octet-stream"; break;
				case ".txt": $r = "text/plain"; break;
				case ".bmp": $r = "image/bitmap"; break;

                case ".deploy": $r = "application/octet-stream"; break;
                case ".manifest": $r = "application/x-ms-manifest"; break;
                case ".application": $r = "application/x-ms-application"; break;
					
				case ".tiff":
				case ".tif":
					$r = "image/tiff";
					break;
					
				case ".json":
					$r = "application/json";
					break;
					
				case ".mocha":
				case ".ls":
				case ".js":
					$r = "text/javascript";
					break;
					
				case ".mpeg":
				case ".mpg":
				case ".mpe":
					$r = "video/mpeg";
					break;
					
				case ".mpv2":
				case ".mp2v":
					$r = "video/mpeg-2";
					break;
					
				case ".ico":
					$r = "image/x-icon";
					break;
					
				case ".htm":
				case ".html":
					$r = "text/html";
					break;

				case ".svg":
					$r = "image/svg+xml";
					break;
				case ".png":
					$r = "image/png";
					break;
				case ".jpg":
				case ".jpeg":
				case ".jpe":
					$r = "image/jpeg";
					break;
			
			}
			
			return $r;				
			
		}
		
		public static $DEFAULT_CACHE_TIME = 10;
		
		public static function AutoCache($days = false)
		{
			if ($days === false) $days = self::$DEFAULT_CACHE_TIME;
			self::ExpiresOffset(60 * 60 * 24 * $days);
			self::CacheControlMaxAge(60 * 60 * 24 * $days);
			self::Pragma("public");
			self::CacheControl("private");
		}

		/**
		 * @param string|stdClass $json
		 */
		public static function Json($json){

			if (is_object($json)) $json = json_encode($json);
			$length = strlen($json);
			self::ContentLength($length);
			self::ContentType(self::GetContentType(".json"));
			echo $json;

		}
		
		public static function File($path, $download = false, $cache = true, $return = false, $name = false)
		{
			$path = realpath($path);
			if ($path && is_file($path))
			{
				if ($cache)
				{
					self::ETagFile($path);
					self::AutoCache();
					self::LastModified(filemtime($path));					
				}
				self::ContentLength(filesize($path));
				if (($c = OU_Header::GetContentType($path)) != false) OU_Header::ContentType($c);
				
				if ($download) 
					self::ContentAttachment($name ? $name : basename($path));

				if ($return)
					return file_get_contents($path);
				else
					readfile($path);

			}
			
		}
		
	}
	
