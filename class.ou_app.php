<?php

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 02/07/2012
	 */

	OU_Config::IncClass(
		array(
			"OU_Path",
			"OU_Options",
			"OU_Base",
			"OU_Array",
			"OU_Header"
		)
	);
	
	OU_Config::IncClassHelper(
		array(
			"OU_Compiler_Tpl",
			"OU_Compiler_Js",
			"OU_Compiler_Less",
			"OU_Compiler_Php",
			"OU_Utils_Smarty",		
			"OU_Control_Manager",
			"OU_Module_Manager"
		)
	);
	 
	/**
	 * Permite el acceso y manipulación de las aplicaciones definidas. Se puede utilizar el sistema por defecto o registrar una dirección para las aplicaciones fuera de la ruta de la libreria.
	 * 
	 * @property-read OU_Controller[] $controllers
	 * @property OU_Control_Manager $controls
	 * @property OU_Module_Manager $modules
	 *
	 * @property-read OU_Controller $current
	 * @property string $appName
	 * @property-read boolean $offlineMode
	 * @property-read OU_Options $options
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_app.php 338 2013-11-26 12:01:16Z xaguilarf $
	 * 
	 */
	class OU_App extends OU_Base
	{
		
		public static $_dir_tpl = "tpl";
		public static $_dir_php = "php";
		public static $_dir_controls = "controls";
		public static $_dir_config = "config";
		public static $_dir_db = "db";
		public static $_dir_controllers = "controllers";
		public static $_dir_libs = "classes";
		public static $_dir_sql = "sql";
		public static $_dir_lang = "lang";
		
		private static $_sites = array();
		
		/**
		 * @var OU_Controller[]
		 */
		private $_controllers;
		/**
		 * @var OU_Module_Manager
		 */
		private $_modules;
		/**
		 * @var string
		 */
		private $_appName;
		/**
		 * @var OU_Controller
		 */
		private $_current;
		/**
		 * @var OU_Options
		 */
		private $_options;

        private $_initialized = false;

        /**
         * @return boolean
         */
        public function isInitialized()
        {
            return $this->_initialized;
        }

        /**
         * @param $file
         */
        public function loadController($file)
        {
            $file =  realpath($file);

            $exists = false;
            foreach ($this->_controllers as $controller){
                if ($controller->fileName == $file) {
                    $exists = true;
                    break;
                }
            }

            if ($file && !$exists) {
                $new = OU_Controller::FromFile($this, $file);
                if ($new) $this->_controllers->Add($new);
            }
        }

        protected function getOptions(){ return $this->_options; }
		/**
		 * @var OU_Control_Manager
		 */
		private $_controls;
		/**
		 * @var OU_Array[]
		 */
		private $_tmpLangs = array();
		
		// public $current
		protected function getCurrent(){ return $this->_current; }
		
		// public $controllers
		protected function getControllers(){ return $this->_controllers; }
		
		// public $controls
		protected function getControls(){ return $this->_controls; }
		
		// public $modules
		protected function getModules(){ return $this->_modules; }

		// public $appName;
		protected function getAppName()	{ return $this->_appName; }
		protected function setAppName($value) { /* Nothing */ }

		// public $offlineMode
		protected function getOfflineMode(){ return $this->_options->offline; }
		
		public static $forceCache = false;
		
		/**
		 * Crea una aplicación nueva.
		 * @param string $appName
		 * @param OU_Options|array $options
		 * @throws Exception
		 */
		public function __construct($appName, $options = array())
		{
			
			$this->doBeforeConstructor();
			
			$global_options = array();
			global $_CONFIG;
			if (isset($_CONFIG) && isset($_CONFIG["APP"]))
				$global_options = $_CONFIG["APP"];
			
			$this->_options = OU_Options::FromArray(
				$options, 
				OU_Options::FromArray(
					$global_options,
					array(
						"forceBackslash" => true,
						"offline" => false, // Indica si s'ha de treballar sense connexió a internet 
						"appNameInBaseUri" => true,
						"defaultLang" => "ES"
					)
				)
			);

			$this->_controllers = new OU_Array();
			$this->_appName = $appName;
			$this->_modules = new OU_Module_Manager($this);
			$this->_modules->onregistermodule = function($module){
				/* @var $module OU_Module_Item  */
				$this->_loadControllers($module->path . "/" . self::$_dir_controllers);
			};
			$this->_controls = new OU_Control_Manager();

			if (!$this->_checkExists())
			{
				throw new Exception("No se ha encontrado la app '$appName'");
			}
			
			$this->doBeforeLoadControllers();
			$this->_loadControllers();
			$this->doAfterLoadControllers();
			
			$this->_initializeConfig();
			
			$this->doAfterConstructor();

            $this->_initialized = true;
            $this->modules->initAll();
				
		}
		
		public function __destruct()
		{
			foreach ($this->_tmpLangs as $tmpLang)
			{
				/* @var $tmpLang OU_Array */
				if ($tmpLang->isModified())
					$tmpLang->Save();
			}
		}
		
		/**
		 * Comprueba si existe la aplicación deseada en la ruta esperada.
		 * @return boolean False si no existe la carpeta.
		 */
		private function _checkExists()
		{
			return is_dir($this->path());
		}
		
		/**
		 * Carga todos los controladores y guarda la información de cada uno para acceder a ella mas adelante.
		 * @throws Exception
		 */
		private function _loadControllers($_find_path = false)
		{
			
			OU_Config::IncClass("OU_Controller");
			$path = $this->path();
			
			$find_path = $_find_path;
			if ($find_path === false)
			{
				$find_path = $path . self::$_dir_controllers . DIRECTORY_SEPARATOR;
			}
			
			if ($path && $path = realpath($find_path))
			{
				$path .= DIRECTORY_SEPARATOR . "*.php";
				$files = glob($path);
				
				foreach ($files as $file)
				{
                    $this->loadController($file);
				}
				
			}else{
				if ($_find_path)
					throw new Exception(
						(
							"No se ha podido extender la aplicación a " . $_find_path
						)
					);
			}			
		}
		
		private function _getURI()
		{
			OU_Config::IncClass("OU_Path");
			$path = dirname(__FILE__) . "/";
			$url = OU_Path::Path2Url($path, array("useHost" => false));
			if (isset(OU_Config::$base_uri) && OU_Config::$base_uri !== false)
			{
				$uri = OU_Path::Relative($_SERVER["REQUEST_URI"], OU_Config::$base_uri);
				$uri = OU_Path::Correct($uri, array("check" => false, "separator" => "/", "endSeparator" => $this->_options->forceBackslash));
			}else{
				$uri = str_replace($url, "", $_SERVER["REQUEST_URI"]);
			}
			
			if (substr($uri, 0, 1) == "/") {
				$uri = substr($uri, 1);
				$uri = $uri === false ? "" : $uri;
			}
				
			return $uri;
		}
		
		/**
		 * @return OU_Controller
		 */
		private function _getControllerFromURI()
		{
			$uri = $this->_getURI();
			foreach ($this->_controllers->toArray() as $controller)
			{
				/* @var $controller OU_Controller */
				if ($controller->checkRoute($uri))
				{
					return $controller;					
				}
			}
			return false;
		}
		
		protected function _initializeConfig()
		{
			/*
			$path = $this->path() . "/" . self::$_dir_config . "/config.php";
			if (file_exists($path))
			{
				//OU_Config::IncGlobal(realpath($path));
			}
			*/		
		}
		
		/**
		 * Recibe el sitename automaticamente cogiendo la primera parte de la URI.
		 */
		public static function sitename($default = null, $options = array())
		{
			
			$options = self::parseAppOptions($options);
			$path = OU_Path::Create($_SERVER["REQUEST_URI"], true);
			$path = $path->relative($options->base_uri);
			$p = explode("/", str_replace("\\", "/", $path->url));
			if (count($p) > 0)
			{
				if ($p[0] == OU_Config::$path_apps_d && count($p) > 1)
					$sitename = $p[1];
				else{
					if (!$p[0]) $sitename = $default;
					else $sitename = $p[0];
				}
			}
			else
				$sitename = $default;
			
			
			if ($sitename)
			{
				if (!OU_Path::Create($options->path_apps . "/" . $sitename)->isDir) $sitename = $default;
			}
			
			return $sitename;
		}
		
		private static function parseAppOptions($options = array())
		{
			if (is_string($options))
			{
				$options = array(
					"path_apps" => $options
				);
			
			}
				
			$options = OU_Options::FromArray(
				$options,
				array(
					"path_apps" => false,
					"base_uri" => false
				)
			);
			
			if ($options->path_apps !== false)
			{
				$options->path_apps = OU_Path::Correct($options->path_apps);
			}else{
				$options->path_apps = OU_Path::Correct(dirname(__FILE__) . "/" . OU_Config::$path_apps_d . "/");
			}
			
			if (!$options->base_uri)
				$options->base_uri = OU_Path::Path2Url(OU_Path::Absolute(".." , $options->path_apps), array("useHost" => false));
			
			return $options;
				
		}
		
		/**
		 * Devuelve una instancia de OU_App. Si no existe una intancia con el mismo $appName creado anteriormente
		 * crea una nueva.
		 * @param string $appName Nombre de la aplicación
		 * @return OU_App
		 */
		public static function app($appName = false, $options = array())
		{
			
			if ($appName === false)
			{
				foreach (self::$_sites as $site)
				{
					return $site;
				}
				return false;
			}
			
			if (isset(self::$_sites[$appName]))
			{
				return self::$_sites[$appName];				
			}else{
				
				$options = self::parseAppOptions($options);
				
				OU_Config::$base_uri = $options->base_uri;
				OU_Config::$path_apps = $options->path_apps;
				
				$newApp = new OU_App($appName, $options);
				
				self::$_sites[$appName] = $newApp;
				
				return $newApp;
			}
			
		}
		
		/**
		 * Indica si existe actualmente alguna aplicación registrada.
		 * @return boolean
		 */
		public static function hasApp()
		{
			return count(self::$_sites) > 0;
		}
		
		public static function getAppPath($appName)
		{
			return OU_Path::Absolute(OU_Config::$path_apps . "/" . $appName, dirname(__FILE__));
		}
		
		/**
		 * Devuelve la URI de app global.
		 * @see OU_App::globalBasePath()
		 * @see OU_Path::Path2Url()
		 * @return String
		 */
		public static function globalBaseURI($url = false)
		{
			return OU_Path::Path2Url(self::globalBasePath(), array("useHost" => $url)) . "/";
		}
		
		/**
		 * Devuelve la ruta local de la app global.
		 * @return String
		 */
		public static function globalBasePath()
		{
			return realpath(OU_Config::OUPath() . "apps/global/") . DIRECTORY_SEPARATOR;
		}
		
		/**
		 * Recibe la dirección de la carpeta principal de cache.
		 * @param string|bool $appName Si el valor es false se devolvera la dirección principal de cache de la última app. 
		 * @return string
		 */
		public static function cachePath($appName = false)
		{
			if (!$appName) $appName = OU_App::app()->_appName;
			
			return OU_Path::Absolute(
						OU_Config::$path_apps . "/" . $appName,
						dirname(__FILE__) . "/"
					) . "/" . OU_Config::$path_cache;
			
		}

		/**
		 * Recibe la dirección de la carpeta principal de módulos.
		 * @param string|bool $appName Si el valor es false se devolvera la dirección principal de módulos de la última app.
		 * @return string
		 */
		public static function modulePath($appName = false)
		{
			if (!$appName) $appName = OU_App::app()->_appName;

			return OU_Path::Absolute(
						OU_Config::$path_apps . "/" . $appName,
						dirname(__FILE__) . "/"
					) . "/" . OU_Config::$path_modules;

		}

		/**
		 * Recibe la dirección de la carpeta principal de cache.
		 * @param string|bool $appName Si el valor es false se devolvera la dirección principal de cache de la última app. 
		 * @return string
		 */
		public static function dataPath($appName = false)
		{
			if (!$appName) $appName = OU_App::app()->_appName;
			
			return OU_Path::Absolute(
						OU_Config::$path_apps . "/" . $appName,
						dirname(__FILE__) . "/"
					) . "/" . OU_Config::$path_data;
			
		}

		/**
	 	 * Devuelve la URI base.
	 	 * @param Boolean $url TRUE si deseas recibir la URL completa o FALSE solo la URI.
	 	 * @return String
	 	 */
	 	public function rootURI($url = false) {
	 		$h = "";
	 		if ($url)
	 			$h = "http://" . $_SERVER["HTTP_HOST"]
	 			; // ? HTTP_HOST ja te el Port. . ($_SERVER["SERVER_PORT"] != "80" ? ":" . $_SERVER["SERVER_PORT"] : "");
			return 
				$h  . 
				OU_Config::$base_uri . "/";
		}
		
		public function baseRelativeUri()
		{
			return ($this->_options->appNameInBaseUri) ? $this->_appName . "/" : "";
		}

		/**
	 	 * Devuelve la URI base de la aplicación.
	 	 * @param Boolean $url TRUE si deseas recibir la URL completa o FALSE solo la URI.
	 	 * @return String
	 	 */
	 	public function baseURI($url = false) {
	 		return $this->rootURI($url) . $this->baseRelativeUri();
		}
		
		/**
		 * Devuelve la dirección de la carpeta donde estan las apps.
		 * @return string
		 */
		public function basePath()
		{
			$path = OU_Path::Absolute(OU_Config::$path_apps, dirname(__FILE__));
			return $path ? $path . DIRECTORY_SEPARATOR : false;
		}
		
		/**
		 * Devuelve la dirección del sitio
		 * @return Ambigous <string, boolean> FALSE si no se encuentra la dirección
		 */
		public function path()
		{
			$path = OU_Path::Absolute($this->_appName, $this->basePath());
			return $path ? $path . DIRECTORY_SEPARATOR : false;
		}
				
		/**
		 * Extiende los controlladores a otra carpeta. Al extender a otra carpeta, la aplicación buscará dentro de esta carpeta para ver si existen más controladores.
		 * @param string $path
		 */
		public function Extend($path)
		{
			return $this->_loadControllers($path);
		}
		
		/**
		 * Incluye un archivo
		 * @see OU_Config::Inc()
		 * @param string $path Dirección relativa a la carpeta de la aplicación
		 */
		public function Inc($path)
		{
			return OU_Config::IncRel($path, $this->path());
		}


        /**
         * Recibe la dirección de un controlador si existe.
         * @param string $controllerName
         * @return bool
         */
        public function GetController($controllerName)
        {
            $path = $this->path() . self::$_dir_controllers . "/" . $controllerName . ".php";
            if (!is_file($path))
            {
                $path = strtolower($controllerName);
                $path = str_replace("controller", "", $path);
                $path = $this->path() . self::$_dir_controllers . "/" . $path  . ".php";
            }
            if (is_file($path))
            {
                return $path;
            }
            else
                return false;
        }

        /**
         * Incluye un controlador partiendo de su nombre.
         * @param string $controllerName
         * @return bool
         */
		public function IncController($controllerName)
		{
			$path = $this->GetController($controllerName);
			if ($path)
			{
				require_once($path);
				return true;
			}
			else
				return false;
		}
		
		/**
		 * Incluye un archivo de la carpeta de configuración.
		 * @param string $fileName Nombre del archivo relativo a la carpeta de configuración.
		 */
		public function IncConfig($fileName)
		{
			global $_CONFIG;
			if (!isset($_CONFIG)) $_CONFIG = array();
			require_once $this->path() . self::$_dir_config . "/" . $fileName;
		}

		/**
		 * Incluye una clase introduciendo su nombre.
		 * @see OU_Config::IncClass()
		 * @param string $className Nombre de la clase. Ejemplo : OU_Config::IncClass("My_Class"); El archivo resultante será class.my_class.php y <b>debe de existir en la carpeta libs de la aplicación o en la carpeta principal de la libreria OU</b>.
		 * @return Ambigous <boolean, void>
		 */
		public function IncClass($className)
		{
			$path = $this->path() . self::$_dir_libs. "/";
			$r = OU_Config::IncClass($className, $path);
			if (!$r) $r = OU_Config::IncClass($className);
			return $r;
		}

        public function IncControlBasics(){
            $this->IncControl("include");
            $this->IncControl("var");
        }

        /**
         * Incluye un control de php.
         * @param string $controlName
         * @param array|OU_Array $options
         * @return string
         */
		public function IncControl($controlName, $options = array())
		{
            $options = OU_Options::FromArray(
                $options,
                array(
                    "path" => null,
                    "group" => null
                )
            );

            global $app_inc_controls_list;
            if (!isset($app_inc_controls_list)) $app_inc_controls_list = array();
            if (isset($app_inc_controls_list[$controlName])) return;
            $app_inc_controls_list[$controlName] = true;

		    $path = $this->path();
		    $path_php = $options->path;

		    if ($path_php == null)
		    {
		        if ($options->group !== null)
		        {
		            $path_php = $path . OU_App::$_dir_php . "/" . $options->group;
		        }else{
                    $path_php = $path . OU_App::$_dir_php . "/" . OU_App::$_dir_controls;
		        }
		    }

		    $path_php .= "/";

		    // Curent App
		    $path = $path_php . $controlName . ".php";
		    if (is_file($path)) {

		        return $this->php(basename($path), dirname($path));
		    }
		    else {
		        $path = OU_Config::OUPath() . "apps/global/php/controls/" . $controlName . ".php";
		        if (is_file($path)) return $this->php(basename($path), dirname($path));
		    }

		}

		/**
		 * @return OU_Array
		 */
		public function getGlobalVars()
		{
			return OU_Array::FromArray(
				array(
					"controller" => $this->current,
					"_REQUEST" => $_REQUEST,
					"_GET" => $_GET,
					"_POST" => $_POST,
					"app" => $this,
					"base_uri" => $this->baseURI(),
					"base_url" => OU_Path::Path2Url($this->path(), array("useHost" => true)) . "/"
				)
			);
		}
		
		/**
		 * Muestra el contenido de un plantilla relativa a la carpeta tpl de la aplicación.
		 * @see OU_Utils_Smarty::tpl()
		 * @param string $fileName
		 * @return string
		 */
		public function tpl($fileName, $path_tpl = null)
		{
			
			$path = $this->path();
			
			if ($path_tpl == null)
			{
				$path_tpl = $path . OU_App::$_dir_tpl;
			}
			
			/*
			  
			  S'ha mogut a OU_Compiler_Tpl. OU_Utils_Smarty::tpl -> OU_Compiler_Tpl -> OU_Utils_Smarty::internatlTpl
			   
				OU_Utils_Smarty::setPath($path_tpl);
				OU_Utils_Smarty::setCachePath($this->cache("tpl"));
				OU_Utils_Smarty::setConfigPath($path . OU_App::$_dir_config);
			*/
			
			OU_Utils_Smarty::assign($this->getGlobalVars()->toArray());
			$tpl = OU_Utils_Smarty::tpl($fileName, null, $path_tpl);
			
			// OU_Utils_Smarty::restorePath();
			
			return $tpl;
			
		}

		/**
		 * Muestra el contenido de un archivo php relativo a la carpeta php de la aplicación.
		 * @param string $fileName
		 * @param null $path_php
		 * @param array $vars
		 * @param array $options
		 * @return string
		 * @throws Exception
		 */
		public function php($fileName, $path_php = null, $vars = array(), $options = array())
		{

			// Check if module
			$n = strpos($fileName, ':');
			if ($n > 0){

				$moduleName = substr($fileName, 0, $n);
				$m = $this->modules->fromName($moduleName);
				if (!$m) throw new Exception('Invalid module');

				$path_php = $m->path . "/" . OU_App::$_dir_php;
				$fileName = substr($fileName, $n + 1);

			}

			$options = OU_Options::FromArray(
				$options, 
				array(
				)
			);
			
			if (!is_object($vars)) $vars = OU_Array::FromArray($vars);
			
			$path = $this->path();
			
			if ($path_php == null)
			{
				$path_php = $path . OU_App::$_dir_php;
			}
			
			$path = OU_Path::Create($fileName);
			$path = $path->absolute($path_php);
			
			$compiler = new OU_Compiler_Php($path->Correct());
			$compiler->params = $vars->toArray();
			
			return $compiler->compile();		
			
		}
		
		/**
		 * Devuelve la dirección local donde se almazenará la cache. Crea el directorio en caso de no existir.
		 * @param string|bool $type Tipo de cache. Sino se especifica será 'other'.
		 * @return boolean TRUE si se ha ejecutado con éxito.
		 */
		public function cache($type = false)
		{
			
			if (!is_string($type))
				$type = "other";
					 
			$path = OU_App::cachePath($this->_appName);
			
			if (!is_dir($path)) 
				if (!mkdir($path))
					return false;
			
			if (is_dir($path))
			{ 
				if (!is_dir($path . "/" . $type))
					if (!mkdir($path . "/" . $type))
						return false;
				
				return realpath(OU_Path::Correct($path . "/" . $type, false));
			}
				
			return false;
		}

		/**
		 * Devuelve la dirección local donde se almazenarán los datos. Crea el directorio en caso de no existir.
		 * @param string|bool $type Tipo de dato. Sino se especifica será 'other'.
		 * @return boolean TRUE si se ha ejecutado con éxito.
		 */
		public function data($type = false)
		{
			
			if (!is_string($type))
				$type = "other";
					 
			$path = OU_App::dataPath($this->_appName);
			
			if (!is_dir($path)) 
				if (!mkdir($path))
					return false;
			
			if (is_dir($path))
			{ 
				if (!is_dir($path . "/" . $type))
					if (!mkdir($path . "/" . $type))
						return false;
				
				return realpath(OU_Path::Correct($path . "/" . $type, false));
				
			}
				
			return false;
			
		}

		/**
		 * Recibe o establece los valores de traducción de un lenguaje. Según el valor de $value:
		 * <ul>
		 * 	<li>NULL : Recibe un objecto OU_Array con todas las traducciones del lenguaje.</li>
		 * 	<li>String : Recibe la traducción de un valor.</li>
		 * 	<li>Array o OU_Array: Asigna los valores de traducción a partir de una array asociativa.</li>
		 * </ul>
		 * En el parámetro opciones se puede indicar:
		 * <ul>
		 * 	<li>lang : Abreviación del lenguage a utilizar. Por defecto se utiliza defaultLang de las opciones de la App</li>
		 * </ul>
		 * @param Array|OU_Array|string|NULL $value
		 * @param Array|OU_Options $options
		 * @return OU_Array|string
		 */
		public function lang($value = null, $options = array())
		{
			
			$options = OU_Options::FromArray(
				$options,
				array(
					"lang" => $this->_options->defaultLang
				)
			);
			
			$lang = $options->lang;
			
			if (!isset($this->_tmpLangs[$lang]))
			{
				$file = $this->path() . self::$_dir_lang;
				if (!is_dir($file)) mkdir($file);
				
				$file .= "/" . $options->lang . ".lang";
				
				$this->_tmpLangs[$lang] = OU_Array::FromFile($file);
			}
			
			$a = &$this->_tmpLangs[$lang];
			if ($value === null){ // Read all
				return $a;
			}else if (is_array($value)){ // Set
				
				foreach ($value as $k=>$v)
				{
					$a->Add($k, $v);
				}
				
			}else{
				if (!isset($a[$value]))
					$a[$value] = $value;
				return $a[$value];
			}
			
		}
		
		/**
		 * Ejecuta el controlador de la aplicación en su comportamiento por defecto
		 */
		public function run()
		{
			$this->doBeforeRun();
			 $controller = $this->_getControllerFromURI();
			 if ($controller)
			 {
			 	$this->_current = $controller;
			 	echo $controller->page();
			 	$this->doAfterRun();
			 }else{
			 	$this->doAfterRun();
			 	return false;
			 }
		}
		
		/**
		 * Ejecuta el controlador de la aplicación en su comportamiento por defecto
		 */
		public function runCode($code)
		{
			foreach ($this->_controllers as $controller)
			{
				if (false) $controller = new OU_Controller();
				if ($controller->checkRouteCode($code))
				{
					$this->_current = $controller;
					echo $controller->page();
					return;
				}
			}
			return false;
		}
		
		/**
		 * Autoconfigura el acceso de la aplicación.
		 * @see OU_App::autoloadApp()
		 */
		public function autoload()
		{
			
			$this->doBeforeAutoload();
			
			OU_Config::IncClassHelper("OU_Autoload_Controller");
			
			set_time_limit(60 * 2);
			@session_write_close();				
			
			$controller = new OU_Autoload_Controller($this);
			if (!$controller->execute())
			{
				
				$uri = trim($_SERVER["REQUEST_URI"]);
				
				$path = OU_Path::Create($uri);
				$path->isUrl = true;
				
				// Reparar URI ("/" Final i doble "/")
					if ($this->_options->forceBackslash)
						switch (substr($path->path, -1)){
							case "/": case "\\": break;
							default:
								$path->path .= "/";
								$this->doAfterAutoload();
								OU_Header::Location($path->url);
								die();														
						}
					
					if (strpos($path->path, "//") !== false)
					{
						while (strpos($path->path, "//") !== false)
						{
							$path->path = str_replace("//", "/", $path->path);
						}
						$this->doAfterAutoload();
						OU_Header::Location($path->url);
						die();
					}
						
				if ($this->run() === FALSE)
				{
					
					$this->doAfterAutoload();
					OU_Header::Code ( 404 );
					if ($this->runCode(404) === false)
					{
					    OU_Header::Code(404);
                    }
                    die();
                }
			}
			$this->doAfterAutoload();
		}
		
		// Events
		
		protected function doBeforeConstructor() {
			OU_Debug::point("App - BeforeConstructor"); 
			$this->raise("beforeConstructor");  
		}
		protected function doBeforeLoadControllers() {
			OU_Debug::point("App - BeforeLoadControllers");
			$this->raise("beforeLoadControllers"); 
		}
		protected function doAfterLoadControllers() { 
			OU_Debug::point("App - AfterLoadControllers");
			$this->raise("afterLoadControllers"); 
		}
		protected function doAfterConstructor() { 
			OU_Debug::point("App - AfterConstructor");
			$this->raise("afterConstructor"); 
		}
		protected function doBeforeAutoload() {
			OU_Debug::point("App - BeforeAutoload");
			$this->raise("beforeAutoload");
		}
		protected function doAfterAutoload() {
			OU_Debug::point("App - AfterAutoload");
			$this->raise("afterAutoload");
		}
		protected function doBeforeRun() {
			OU_Debug::point("App - BeforeRun");
			$this->raise("beforeRun");
		}
		protected function doAfterRun() {
			OU_Debug::point("App - AfterRun");
			$this->raise("afterRun");
		}
		
		
	}
	
	class OU_App_Sitename
	{
		
	}

