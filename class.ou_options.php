<?php

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 25/06/2012
	 */

	OU_Config::IncClass("OU_Array");
	 
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @example		ou_options.php
	 * @version		$Id: class.ou_options.php 333 2013-11-08 12:16:43Z xaguilarf $
	 * 
	 */
	class OU_Options extends OU_Array
	{
		
		/**
		 * @param array $_array
		 * @param array $base
		 * @return OU_Options
		 */
		public static function FromArray($_array, $base=array())
		{
			return parent::FromArray($_array, $base);
		}
		
		/**
		 * Comprueva si la opción existe
		 * @param mixed $value Valor a buscar
		 * @return boolean Devuelve true si se ha encontrado, falso en caso contrario.
		 */
		public function hasOption($value)
		{
			return $this->IndexOf($value) !== FALSE;
		}
		
		/**
		 * Comprueva si la opción existe buscando su clave
		 * @param mixed $value Valor a buscar
		 * @return boolean Devuelve true si se ha encontrado, falso en caso contrario.
		 */
		public function hasOptionKey($value)
		{
			return $this->IndexOf(
				$value,
				array(
					"searchInKeys" => true,
					"searchInValues" => false
				) 
			) !== FALSE;
		}
		
		/**
		 * Devuelve los valores de las opciones a string al estilo de parámetros de URL.
		 * @return string
		 */
		public function toStr()
		{
			return http_build_query($this->_array);			
		}

		/**
		 * Convierte una cadena con formato de parametros de URL a OU_Options.
		 * @param string $str
		 * @return OU_Options
		 */
		public static function fromStr($str)
		{
			$a = array();
			parse_str($str, $a);
			return OU_Options::FromArray($a);
		}
		
	}
	
	class_alias("OU_Options", "OU_Opts");
	if (false)
	{
		/**
		 * Alias de OU_Options.
		 * <i>Esta clase esta definida en el código per no existe. OU_Opts es un alias de la clase OU_Options definido por class_alias().
		 * Alias de OU_Options
		 * @see			OU_Options
		 * @author		fontcolor
		 * @package		OU Framework
		 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
		 */
		class OU_Opts extends OU_Options {}
	}
	
?>