<?php

	namespace Word\Files;
	
	use Word\IObject;
	use Word\ST_StyleType;
	use Word\ArrayObj;
	use Word\Config;
	use Word\Namespaces;
	use Word\Objects\Style;
	use Word\Objects\Body;
	use Word\Objects\DocumentBackground;
	
	Config::IncClass(
		array(
			"Base",
			"ArrayObj",
			"Namespaces",
			"Objects\Style",
			"Files\Base",
		)
	);
	 
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: styles.php 300 2013-08-23 11:00:53Z xaguilarf $
	 */
	class Styles extends Base
	{
		
		private $_arrayStyles;
		
		public function AddStyle(&$style)
		{
			$this->_arrayStyles->AddRef($style);
		}
		
		public function AddStyles(&$styles)
		{
			foreach ($styles as $style)
				$this->AddStyle($style);
		}
		
		/**
		 * @return \Word\Objects\Style
		 */
		public function CreateStyle($name, $type = ST_StyleType::Paragraph)
		{			
			$style = new Style("w:style");
			$style->name->val = $name;
			$style->type = $type;
			$this->AddStyle($style);
			return $style;
		}
		
		/* Attributes */
		
		
		/* Childrens */
		
		public function __construct($word)
		{
			$this->_arrayStyles = new ArrayObj();
			parent::__construct($word);
			$this->namespaces->AddArray(
				array(
					"mc" => "http://schemas.openxmlformats.org/markup-compatibility/2006",
					"o" => "urn:schemas-microsoft-com:office:office",
					"r" => "http://schemas.openxmlformats.org/officeDocument/2006/relationships",
					"m" => "http://schemas.openxmlformats.org/officeDocument/2006/math",
					"v" => "urn:schemas-microsoft-com:vml",
					"wp" => "http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing",
					"w10" => "urn:schemas-microsoft-com:office:word",
					"w" => "http://schemas.openxmlformats.org/wordprocessingml/2006/main",
					"wne" => "http://schemas.microsoft.com/office/word/2006/wordml",
					"sl" => "http://schemas.openxmlformats.org/schemaLibrary/2006/main",
					"a" => "http://schemas.openxmlformats.org/drawingml/2006/main",
					"pic" => "http://schemas.openxmlformats.org/drawingml/2006/picture",
					"c" => "http://schemas.openxmlformats.org/drawingml/2006/chart",
					"lc" => "http://schemas.openxmlformats.org/drawingml/2006/lockedCanvas",
					"dgm" => "http://schemas.openxmlformats.org/drawingml/2006/diagram"
				)
			);
			
			// Default styles
			$style = $this->CreateStyle("normal", "paragraph");
			$style->set(array(
				"default" => true,
				"styleId" => "Normal",
				"rPr" => array(
					"rFonts" => array(
						"cs"  => "Arial",
						"hAnsi" => "Arial",
						"eastAsia" => "Arial",
						"ascii" => "Arial"
					)
				)
			));
						
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\IObject::_write()
		 */
		public function _write($xml)
		{
			$xml->startElement("w:styles");
				$this->namespaces->_write($xml);
				foreach ($this->_arrayStyles as $style)
				{
					$style->_write($xml);
				}
			$xml->endElement();
		}
		
	}

?>