<?php

	namespace Word\Files;
	
	use Word\Objects\Background;
	use Word\Objects\Body;
	use Word\Objects\DocumentBackground;
	use Word\Config;
	use Word\Namespaces;


	Config::IncClass(
		array(
			"Base",
			"Object",
			"Namespaces",
			"Files\Base",
			"Objects\Body",
			"Objects\Background",
		)
	);
	
	
	
	/**
	 * @property string $conformance transitional | strict
	 * @property DocumentBackground $background
	 * @property \Word\Objects\Body $body
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: document.php 300 2013-08-23 11:00:53Z xaguilarf $
	 */
	class Document extends Base
	{
		
		/* Attributes */
		
		private $_conformance;
		protected function getConformance(){ return $this->_conformance; }
		protected function setConformance($value){ $this->_conformance = $value; }
		
		/* Childrens */
		
		private $_body;
		protected function getBody(){ return $this->_body; }
		
		private $_background;
		protected function getBackground(){ return $this->_background; }
		
		public function __construct($word)
		{
			parent::__construct($word);
			
			$this->namespaces->AddArray(
				array(
					"ve"=>"http://schemas.openxmlformats.org/markup-compatibility/2006",
					"o"=>"urn:schemas-microsoft-com:office:office",
					"m"=>"http://schemas.openxmlformats.org/officeDocument/2006/math",
					"v"=>"urn:schemas-microsoft-com:vml",
					"wp"=>"http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing",
					"text"=>"urn:oasis:names:tc:opendocument:xmlns:text:1.0",
					"svg"=>"urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0",
					"w10"=>"urn:schemas-microsoft-com:office:word",
					"w14"=>"http://schemas.microsoft.com/office/word/2010/wordml",
					"wne"=>"http://schemas.microsoft.com/office/word/2006/wordml"
				)
			);
			
			$this->_background = new Background("w:background");
			$this->_body = new Body("w:body");
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\IXMLAccess::_write()
		 */
		public function _write($xml)
		{
			$xml->startElement("w:document");
				$this->namespaces->_write($xml);
				$this->background->_write($xml);
				$this->body->_write($xml);
			$xml->endElement();
		}
		
	}

?>