<?php

	

	namespace Word\Files;
	
	use Word\Config;

	use Word\Namespaces;
	use Word\IObject;

	Config::IncClass(
		array(
			"Base"
		)
	);
	 
	/**
	 * @property Namespaces $namespaces
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: base.php 300 2013-08-23 11:00:53Z xaguilarf $
	 */
	abstract class Base extends \Word\Base implements IObject
	{
		
		private $_namespaces;
		protected function getNamespaces(){ return $this->_namespaces; }
		
		private $_word;
		public function __construct($word)
		{
			$this->_word = $word;
			$this->_namespaces = new Namespaces();
			$this->namespaces->AddArray(
				array(
					"r" => "http://schemas.openxmlformats.org/officeDocument/2006/relationships",
					"w" => "http://schemas.openxmlformats.org/wordprocessingml/2006/main"
				)
			);
		}
		
		public function _docWrite()
		{
			$xml = new \XMLWriter();
			$xml->openMemory();
			$xml->setIndent(true);
			$xml->startDocument('1.0', 'UTF-8', 'yes');
			$this->_write($xml);
			return $xml->outputMemory();
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\IObject::_empty()
		 */
		public function _empty()
		{
			return false;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\IObject::_validate()
		 */
		public function _validate()
		{
			return true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\IObject::_write()
		 */
		public function _write($xml) { }
		
	}

?>