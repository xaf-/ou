<?php

	namespace Word;

	Config::IncClass(
		"Base",
		"ArrayObj"
	);
	
	
	interface IObject
	{
		/**
		 * @param XMLWriter $xml
		 */
		public function _write($xml);
		/**
		 * @return bool
		 */
		public function _empty();
		/**
		 * @return bool
		 */
		public function _validate();
	}
	
	
	/**
	 * @property-read string $propName
	 * @property-read \Word\ArrayObj $propOptions
	 * @property-read \Word\ArrayObj $propChildrens
	 * 
	 * @property-read Object $parent
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: object.php 300 2013-08-23 11:00:53Z xaguilarf $
	 *
	 */
	abstract class Object extends Base implements IObject {
		
		private $_parent;
		protected function &getParent(){ return $this->_parent; }
		
		protected $_array;
		protected function &getPropChildrens(){ return $this->_array; }
		
		private $_propName;
		protected function &getPropName(){ return $this->_propName; }
		
		private $_propOptions;
		protected function &getPropOptions(){ return $this->_propOptions; }
		
		// 
		private function isPropForced(){ return isset($this->propOptions["forced"]) && $this->propOptions["forced"]; }
			
		/**
		 * @param string $name
		 */
		public function __construct($name)
		{
			$this->_array = new ArrayObj();
			$this->_propOptions = new ArrayObj();
			$this->_propName = $name;
			
			// echo "CreateO(".get_called_class().")";
			
		}
		
		public function set(Array $a)
		{
			foreach ($a as $k=>$v)
			{
				if (is_object($this->{$k}) && is_array($v))
					$this->{$k}->set($v);
				else
					$this->{$k} = $v;
			}
		}
		
		/**
		 * @param Object $obj
		 * @return Object
		 */
		protected function AddItem($obj)
		{
			$this->_array->Add($obj);
				$obj->_parent = $this; 
				$obj->DoParentChange();
			return $obj;
		}
		
		/**
		 * 
		 */
		protected function DoParentChange() { }
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\IObject::_validate()
		 */
		public function _validate()
		{
			// TO DO
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\IObject::_empty()
		 */
		public function _empty()
		{
			if ($this->isPropForced()) return false;
			foreach ($this->_array as $obj)
				if (!$obj->_empty())
				{
					return false;
				}
			return $this->_emptyObject();
		}
		
		/**
		 * 
		 */
		abstract public function _emptyObject();
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\IObject::_write()
		 */
		public function _write($xml)
		{
			if (!$this->isPropForced() && $this->_empty()) return ;
			$xml->startElement($this->propName);
				$this->_writeObject($xml);
				foreach ($this->_array as $obj)
					if (!$obj->_empty())
						$obj->_write($xml);
			$xml->endElement();
		}
		
		/**
		 * @param \XMLWriter $xml
		 */
		abstract function _writeObject($xml);
		
	}
	
	class PropertyArray implements \ArrayAccess, \Countable, \Iterator
	{

		public function __construct($obj)
		{
			$this->_obj = $obj;
		}
		
		private $_assoc = array();
		/**
		 * 
		 * @var \Word\Object
		 */
		private $_obj;
		
		
		/** ArrayAccess */
		public function offsetExists ($offset) {
			$key = $this->_assoc[$offset];
			return isset($this->_obj->propChildrens[$key]);
		}
		public function &offsetGet ($offset) {
			$key = $this->_assoc[$offset];
			return $this->_obj->propChildrens[$key];
		}
		
		public function AddRef(&$value)
		{
			$this->_obj->propChildrens->AddRef($value);
			end($this->_obj->propChildrens);
			$key = key($this->_obj->propChildrens);
			$this->_assoc[] = $key;
		}
		
		public function offsetSet ($offset, $value) {
			if ($offset === null)
			{
				
				// $this->_array[] = $value;
				$this->_obj->propChildrens->AddRef($value);
				end($this->_obj->propChildrens);
				$key = key($this->_obj->propChildrens);
				$this->_assoc[] = $key;
				
			}
			else{
				if (isset($this->_assoc[$offset]))
				{
					$key = $this->_assoc[$offset];
					$this->_obj->propChildrens[$key] = $value;
					$this->_assoc[$offset] = $key;
				} 
				else
				{
					$this->_obj->propChildrens[] = $value;
					end($this->_obj->propChildrens);
					$key = key($this->_obj->propChildrens);
					$this->_assoc[$offset] = $key;
				}
					
			}
		}
		public function offsetUnset ($offset) {
			$key = $this->_assoc[$offset];
			unset($this->_obj->propChildrens[$key]);
			unset($this->_assoc[$offset]);
		}
		
		/* Countable */
		public function count ( )
		{
			return count($this->_assoc);
		}
		
		// Iterator
		public function current () {
			$key = current($this->_assoc);
			return $this->_obj->propChildrens[$key];
		}
		
		public function next () {
			$key = next($this->_assoc);
			return $this->_obj->propChildrens[$key];
		}
		
		public function key () {
			return key($this->_assoc);
		}
		
		public function valid () {
			return key($this->_assoc) !== NULL;
		}
		
		public function rewind () {
			reset($this->_assoc);
		}
		
	
		public function &get()
		{
			return $this;
		}
	
	}
	
	class PropertyObject
	{
		
		private $_className;
		private $_param;
		private $_obj;
		/**
		 * 
		 * @param unknown $className
		 * @param unknown $param
		 * @param \Word\Object $obj
		 */
		public function __construct($className, $param, $obj)
		{
			$this->_className = $className;
			$this->_param = $param;
			$this->_objP = $obj;
		} 
		
		
		private $_objP = null;
		public function &get()
		{
			if ($this->_obj === null)
			{
				
				if ($this->_param === null)
					$this->_obj = new $this->_className();
				else
					$this->_obj = new $this->_className($this->_param);
				
				$this->_objP->propChildrens->AddRef($this->_obj);
				
			}
			return $this->_obj;
		}
		
	}
	
?>