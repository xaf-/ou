<?php

	namespace Word;

	use Word\ArrayObj;
	use Word\IObject;
	use Word\Base;
	use Word\Config;

	Config::IncClass(
		array(
			"Base",
			"ArrayObj"
		)
	);
	
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: namespaces.php 300 2013-08-23 11:00:53Z xaguilarf $
	 *
	 */
	class Namespaces extends Base implements IObject
	{
		
		private $_array;
		
		public function __construct()
		{
			$this->_array = new ArrayObj();
		}
		
		public function Add($prefix, $ns)
		{
			$this->_array->Add($prefix, $ns);
		}
		
		public function AddArray($array)
		{
			foreach ($array as $prefix => $ns)
				$this->Add($prefix, $ns);
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\IObject::_empty()
		 */
		public function _empty()
		{
			return false;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\IObject::_validate()
		 */
		public function _validate()
		{
			return true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see IObject::_write()
		 */
		public function _write($xml)
		{
			foreach ($this->_array as $k => $v)
			{
				$xml->writeAttribute("xmlns:$k", $v);
			}			
		}
	}

?>