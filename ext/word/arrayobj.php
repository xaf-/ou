<?php

	namespace Word;
	
	class ArrayObj implements \ArrayAccess, \Countable, \Iterator
	{

		protected $_modified = false;		
		protected $_array = array();
		
		/* Countable */
		public function count ( ) {
			return count($this->_array);
		}
		
		public function __construct() {
			$this->_array = array();
		}
		
		// Iterator
		//private $_iteratorPos = 0;
		public function current () {
			return current($this->_array);
			// return $this[$this->_iteratorPos];
		}
		
		public function next () {
			// ++$this->_iteratorPos;
			next($this->_array);
		}
		
		public function key () {
			//return $this->_iteratorPos;
			return key($this->_array);
		}
		
		public function valid () {
			//return isset($this[$this->_iteratorPos]);
			return key($this->_array) !== NULL;
		}
		
		public function rewind () {
			reset($this->_array);
			// $this->_iteratorPos = 0;
		}
		
		/**
		 * Indica si la array se ha modificado.
		 * @return boolean
		 */
		public function isModified()
		{
			return $this->_modified;
		}
		
		/*
		 * (non-PHPdoc) @see ArrayAccess::offsetExists()
		 */
		public function offsetExists($offset) {
			return isset($this->_array[$offset]);
		}
		/*
		 * (non-PHPdoc) @see ArrayAccess::offsetGet()
		 * Antenció ! Nomes funciona en PHP >= 5.3.4.
		 */
		public function &offsetGet($offset) {
			return $this->_array[$offset];
		}
		
		/*
		 * (non-PHPdoc) @see ArrayAccess::offsetSet()
		 */
		public function offsetSet($offset, $value) {
			if (!isset($this->_array[$offset]) || $this->_array[$offset] !== $value)
			{
				//$this->_array[$offset] = $value;
				$this->Add($offset, $value);
				
			}
		}
		/*
		 * (non-PHPdoc) @see ArrayAccess::offsetUnset()
		 */
		public function offsetUnset($offset) {
			unset($this->_array[$offset]);
			$this->DoChange();
		}
		
		public function __get ( $name )
		{
	    	//if (method_exists ( $this, "get" . ucfirst ( $name ) )) {
	    		//return parent::__get($name);
	    	//} else{
				if (isset($this->_array[$name]))
					return $this->_array[$name];
				else
					return false;
	    	//}
		}
		
		public function __isset( $name)
		{
			return isset($this->_array[$name]);
		}
		
		public function __unset($name)
		{
			unset($this->_array[$name]);
			$this->DoChange();
		}
		
		public function &toArray()
		{
			return $this->_array;
		}
		
		public function DoChange()
		{
			$this->_modified = true;
		}

		public function Has($key)
		{
			return isset($this->_array[$key]);
		}
		
		public function AddRef(&$val)
		{
			$this->_array[] = &$val;
		}
		
		public function Add($key_or_value, $value = "{�}")
		{
			
			$issetKey = 
				($value !== "{�}" && $key_or_value !== NULL);
			
			$k = $key_or_value;
			$v = $value ==  "{�}" ? $key_or_value : $value;
			
			if ($issetKey)
			{
				if (!isset($this->_array[$k]) || $this->_array[$k] !== $v)
				{
					$this->_array[$k] = $v;
					$this->DoChange();
				}
			}
			else
			{
				$this->_array[] = $v;
				$this->DoChange();
			}
			
			return $this;
			
		}
		
		public function Pop()
		{
			if ($this->count() > 0)
			{
				array_pop($this->_array);
				$this->DoChange();
			}
		}		
		
		public function Remove($key)
		{
			if ($this->Has($key))
			{
				unset($this->_array[$key]);
				$this->DoChange();
			}
		}
				
	}
	
?>