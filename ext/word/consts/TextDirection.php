<?php 

namespace Word\Consts
{

	/**
	 * Text Flow Direction
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_TextDirection.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class TextDirection
	{
		
		/**
		 * Left to Right, Top to Bottom
		 * @var string
		 */
		const LrTb = "lrTb";

		/**
		 * Top to Bottom, Right to Left
		 * @var string
		 */
		const TbRl = "tbRl";

		/**
		 * Bottom to Top, Left to Right
		 * @var string
		 */
		const BtLr = "btLr";

		/**
		 * Left to Right, Top to Bottom Rotated
		 * @var string
		 */
		const LrTbV = "lrTbV";

		/**
		 * Top to Bottom, Right to Left Rotated
		 * @var string
		 */
		const TbRlV = "tbRlV";

		/**
		 * Top to Bottom, Left to Right Rotated
		 * @var string
		 */
		const TbLrV = "tbLrV";


		
	}
	
}

?>