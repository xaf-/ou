<?php 

namespace Word\Consts
{

	/**
	 * Animated Text Effects
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_TextEffect.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class TextEffect
	{
		
		/**
		 * Blinking Background Animation
		 * @var string
		 */
		const BlinkBackground = "blinkBackground";

		/**
		 * Colored Lights Animation
		 * @var string
		 */
		const Lights = "lights";

		/**
		 * Black Dashed Line Animation
		 * @var string
		 */
		const AntsBlack = "antsBlack";

		/**
		 * Marching Red Ants
		 * @var string
		 */
		const AntsRed = "antsRed";

		/**
		 * Shimmer Animation
		 * @var string
		 */
		const Shimmer = "shimmer";

		/**
		 * Sparkling Lights Animation
		 * @var string
		 */
		const Sparkle = "sparkle";

		/**
		 * No Animation
		 * @var string
		 */
		const None = "none";


		
	}
	
}

?>