<?php 

namespace Word\Consts
{

	/**
	 * Table Cell Vertical Merge Revision Type
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_AnnotationVMerge.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class AnnotationVMerge
	{
		
		/**
		 * Vertically Merged Cell
		 * @var string
		 */
		const Cont = "cont";

		/**
		 * Vertically Split Cell
		 * @var string
		 */
		const Rest = "rest";


		
	}
	
}

?>