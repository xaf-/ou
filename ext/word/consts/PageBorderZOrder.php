<?php 

namespace Word\Consts
{

	/**
	 * Page Border Z-Order
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_PageBorderZOrder.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class PageBorderZOrder
	{
		
		/**
		 * Page Border Ahead of Text
		 * @var string
		 */
		const Front = "front";

		/**
		 * Page Border Behind Text
		 * @var string
		 */
		const Back = "back";


		
	}
	
}

?>