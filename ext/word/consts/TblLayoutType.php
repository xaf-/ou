<?php 

namespace Word\Consts
{

	/**
	 * Table Layout Type
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_TblLayoutType.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class TblLayoutType
	{
		
		/**
		 * Fixed Width Table Layout
		 * @var string
		 */
		const Fixed = "fixed";

		/**
		 * AutoFit Table Layout
		 * @var string
		 */
		const Autofit = "autofit";


		
	}
	
}

?>