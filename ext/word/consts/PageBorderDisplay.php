<?php 

namespace Word\Consts
{

	/**
	 * Page Border Display Options
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_PageBorderDisplay.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class PageBorderDisplay
	{
		
		/**
		 * Display Page Border on All Pages
		 * @var string
		 */
		const AllPages = "allPages";

		/**
		 * Display Page Border on First Page
		 * @var string
		 */
		const FirstPage = "firstPage";

		/**
		 * Display Page Border on All Pages Except First
		 * @var string
		 */
		const NotFirstPage = "notFirstPage";


		
	}
	
}

?>