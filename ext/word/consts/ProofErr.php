<?php 

namespace Word\Consts
{

	/**
	 * Proofing Error Type
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_ProofErr.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class ProofErr
	{
		
		/**
		 * Start of Region Marked as Spelling Error
		 * @var string
		 */
		const SpellStart = "spellStart";

		/**
		 * End of Region Marked as Spelling Error
		 * @var string
		 */
		const SpellEnd = "spellEnd";

		/**
		 * Start of Region Marked as Grammatical Error
		 * @var string
		 */
		const GramStart = "gramStart";

		/**
		 * End of Region Marked as Grammatical Error
		 * @var string
		 */
		const GramEnd = "gramEnd";


		
	}
	
}

?>