<?php 

namespace Word\Consts
{

	/**
	 * Vertical Text Alignment Types
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_TextAlignment.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class TextAlignment
	{
		
		/**
		 * Align Text at Top
		 * @var string
		 */
		const Top = "top";

		/**
		 * Align Text at Center
		 * @var string
		 */
		const Center = "center";

		/**
		 * Align Text at Baseline
		 * @var string
		 */
		const Baseline = "baseline";

		/**
		 * Align Text at Bottom
		 * @var string
		 */
		const Bottom = "bottom";

		/**
		 * Automatically Determine Alignment
		 * @var string
		 */
		const Auto = "auto";


		
	}
	
}

?>