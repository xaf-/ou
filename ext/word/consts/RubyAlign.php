<?php 

namespace Word\Consts
{

	/**
	 * Phonetic Guide Text Alignment
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_RubyAlign.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class RubyAlign
	{
		
		/**
		 * Center
		 * @var string
		 */
		const Center = "center";

		/**
		 * Distribute All Characters
		 * @var string
		 */
		const DistributeLetter = "distributeLetter";

		/**
		 * Distribute all Characters w/ Additional Space On Either Side
		 * @var string
		 */
		const DistributeSpace = "distributeSpace";

		/**
		 * Left Aligned
		 * @var string
		 */
		const Left = "left";

		/**
		 * Right Aligned
		 * @var string
		 */
		const Right = "right";

		/**
		 * Vertically Aligned to Right of Base Text
		 * @var string
		 */
		const RightVertical = "rightVertical";


		
	}
	
}

?>