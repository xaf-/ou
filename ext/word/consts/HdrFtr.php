<?php 

namespace Word\Consts
{

	/**
	 * Header or Footer Type
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_HdrFtr.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class HdrFtr
	{
		
		/**
		 * Even Numbered Pages Only
		 * @var string
		 */
		const Even = "even";

		/**
		 * Default Header or Footer
		 * @var string
		 */
		const Default_ = "default";

		/**
		 * First Page Only
		 * @var string
		 */
		const First = "first";


		
	}
	
}

?>