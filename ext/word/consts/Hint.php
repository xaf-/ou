<?php 

namespace Word\Consts
{

	/**
	 * Font Type Hint
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_Hint.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class Hint
	{
		
		/**
		 * High ANSI Font
		 * @var string
		 */
		const Default_ = "default";

		/**
		 * East Asian Font
		 * @var string
		 */
		const EastAsia = "eastAsia";

		/**
		 * Complex Script Font
		 * @var string
		 */
		const Cs = "cs";


		
	}
	
}

?>