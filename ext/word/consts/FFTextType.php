<?php 

namespace Word\Consts
{

	/**
	 * Text Box Form Field Type Values
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_FFTextType.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class FFTextType
	{
		
		/**
		 * Text Box
		 * @var string
		 */
		const Regular = "regular";

		/**
		 * Number
		 * @var string
		 */
		const Number = "number";

		/**
		 * Date
		 * @var string
		 */
		const Date = "date";

		/**
		 * Current Time Display
		 * @var string
		 */
		const CurrentTime = "currentTime";

		/**
		 * Current Date Display
		 * @var string
		 */
		const CurrentDate = "currentDate";

		/**
		 * Field Calculation
		 * @var string
		 */
		const Calculated = "calculated";


		
	}
	
}

?>