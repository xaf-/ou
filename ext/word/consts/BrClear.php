<?php 

namespace Word\Consts
{

	/**
	 * Line Break Text Wrapping Restart Location
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_BrClear.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class BrClear
	{
		
		/**
		 * Restart On Next Line
		 * @var string
		 */
		const None = "none";

		/**
		 * Restart In Next Text Region When In Leftmost Position
		 * @var string
		 */
		const Left = "left";

		/**
		 * Restart In Next Text Region When In Rightmost Position
		 * @var string
		 */
		const Right = "right";

		/**
		 * Restart On Next Full Line
		 * @var string
		 */
		const All = "all";


		
	}
	
}

?>