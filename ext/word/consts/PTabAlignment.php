<?php 

namespace Word\Consts
{

	/**
	 * Absolute Position Tab Alignment
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_PTabAlignment.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class PTabAlignment
	{
		
		/**
		 * Left
		 * @var string
		 */
		const Left = "left";

		/**
		 * Center
		 * @var string
		 */
		const Center = "center";

		/**
		 * Right
		 * @var string
		 */
		const Right = "right";


		
	}
	
}

?>