<?php 

namespace Word\Consts
{

	/**
	 * Height Rule
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_HeightRule.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class HeightRule
	{
		
		/**
		 * Determine Height Based On Contents
		 * @var string
		 */
		const Auto = "auto";

		/**
		 * Exact Height
		 * @var string
		 */
		const Exact = "exact";

		/**
		 * Minimum Height
		 * @var string
		 */
		const AtLeast = "atLeast";


		
	}
	
}

?>