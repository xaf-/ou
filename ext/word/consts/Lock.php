<?php 

namespace Word\Consts
{

	/**
	 * Locking Types
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_Lock.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class Lock
	{
		
		/**
		 * SDT Cannot Be Deleted
		 * @var string
		 */
		const SdtLocked = "sdtLocked";

		/**
		 * Contents Cannot Be Edited At Runtime
		 * @var string
		 */
		const ContentLocked = "contentLocked";

		/**
		 * No Locking
		 * @var string
		 */
		const Unlocked = "unlocked";

		/**
		 * Contents Cannot Be Edited At Runtime And SDT Cannot Be Deleted
		 * @var string
		 */
		const SdtContentLocked = "sdtContentLocked";


		
	}
	
}

?>