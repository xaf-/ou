<?php 

namespace Word\Consts
{

	/**
	 * Vertical Anchor Location
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_VAnchor.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class VAnchor
	{
		
		/**
		 * Relative To Vertical Text Extents
		 * @var string
		 */
		const Text = "text";

		/**
		 * Relative To Margin
		 * @var string
		 */
		const Margin = "margin";

		/**
		 * Relative To Page
		 * @var string
		 */
		const Page = "page";


		
	}
	
}

?>