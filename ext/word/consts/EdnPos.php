<?php 

namespace Word\Consts
{

	/**
	 * Endnote Positioning Location
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_EdnPos.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class EdnPos
	{
		
		/**
		 * Endnotes Positioned at End of Section
		 * @var string
		 */
		const SectEnd = "sectEnd";

		/**
		 * Endnotes Positioned at End of Document
		 * @var string
		 */
		const DocEnd = "docEnd";


		
	}
	
}

?>