<?php 

namespace Word\Consts
{

	/**
	 * Border Styles
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_Border.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class Border
	{
		
		/**
		 * No Border
		 * @var string
		 */
		const Nil = "nil";

		/**
		 * No Border
		 * @var string
		 */
		const None = "none";

		/**
		 * Single Line Border
		 * @var string
		 */
		const Single = "single";

		/**
		 * Single Line Border
		 * @var string
		 */
		const Thick = "thick";

		/**
		 * Double Line Border
		 * @var string
		 */
		const Double = "double";

		/**
		 * Dotted Line Border
		 * @var string
		 */
		const Dotted = "dotted";

		/**
		 * Dashed Line Border
		 * @var string
		 */
		const Dashed = "dashed";

		/**
		 * Dot Dash Line Border
		 * @var string
		 */
		const DotDash = "dotDash";

		/**
		 * Dot Dot Dash Line Border
		 * @var string
		 */
		const DotDotDash = "dotDotDash";

		/**
		 * Triple Line Border
		 * @var string
		 */
		const Triple = "triple";

		/**
		 * Thin, Thick Line Border
		 * @var string
		 */
		const ThinThickSmallGap = "thinThickSmallGap";

		/**
		 * Thick, Thin Line Border
		 * @var string
		 */
		const ThickThinSmallGap = "thickThinSmallGap";

		/**
		 * Thin, Thick, Thin Line Border
		 * @var string
		 */
		const ThinThickThinSmallGap = "thinThickThinSmallGap";

		/**
		 * Thin, Thick Line Border
		 * @var string
		 */
		const ThinThickMediumGap = "thinThickMediumGap";

		/**
		 * Thick, Thin Line Border
		 * @var string
		 */
		const ThickThinMediumGap = "thickThinMediumGap";

		/**
		 * Thin, Thick, Thin Line Border
		 * @var string
		 */
		const ThinThickThinMediumGap = "thinThickThinMediumGap";

		/**
		 * Thin, Thick Line Border
		 * @var string
		 */
		const ThinThickLargeGap = "thinThickLargeGap";

		/**
		 * Thick, Thin Line Border
		 * @var string
		 */
		const ThickThinLargeGap = "thickThinLargeGap";

		/**
		 * Thin, Thick, Thin Line Border
		 * @var string
		 */
		const ThinThickThinLargeGap = "thinThickThinLargeGap";

		/**
		 * Wavy Line Border
		 * @var string
		 */
		const Wave = "wave";

		/**
		 * Double Wave Line Border
		 * @var string
		 */
		const DoubleWave = "doubleWave";

		/**
		 * Dashed Line Border
		 * @var string
		 */
		const DashSmallGap = "dashSmallGap";

		/**
		 * Dash Dot Strokes Line Border
		 * @var string
		 */
		const DashDotStroked = "dashDotStroked";

		/**
		 * 3D Embossed Line Border
		 * @var string
		 */
		const ThreeDEmboss = "threeDEmboss";

		/**
		 * 3D Engraved Line Border
		 * @var string
		 */
		const ThreeDEngrave = "threeDEngrave";

		/**
		 * Outset Line Border
		 * @var string
		 */
		const Outset = "outset";

		/**
		 * Inset Line Border
		 * @var string
		 */
		const Inset = "inset";

		/**
		 * Apples Art Border
		 * @var string
		 */
		const Apples = "apples";

		/**
		 * Arched Scallops Art Border
		 * @var string
		 */
		const ArchedScallops = "archedScallops";

		/**
		 * Baby Pacifier Art Border
		 * @var string
		 */
		const BabyPacifier = "babyPacifier";

		/**
		 * Baby Rattle Art Border
		 * @var string
		 */
		const BabyRattle = "babyRattle";

		/**
		 * Three Color Balloons Art Border
		 * @var string
		 */
		const Balloons3Colors = "balloons3Colors";

		/**
		 * Hot Air Balloons Art Border
		 * @var string
		 */
		const BalloonsHotAir = "balloonsHotAir";

		/**
		 * Black Dash Art Border
		 * @var string
		 */
		const BasicBlackDashes = "basicBlackDashes";

		/**
		 * Black Dot Art Border
		 * @var string
		 */
		const BasicBlackDots = "basicBlackDots";

		/**
		 * Black Square Art Border
		 * @var string
		 */
		const BasicBlackSquares = "basicBlackSquares";

		/**
		 * Thin Line Art Border
		 * @var string
		 */
		const BasicThinLines = "basicThinLines";

		/**
		 * White Dash Art Border
		 * @var string
		 */
		const BasicWhiteDashes = "basicWhiteDashes";

		/**
		 * White Dot Art Border
		 * @var string
		 */
		const BasicWhiteDots = "basicWhiteDots";

		/**
		 * White Square Art Border
		 * @var string
		 */
		const BasicWhiteSquares = "basicWhiteSquares";

		/**
		 * Wide Inline Art Border
		 * @var string
		 */
		const BasicWideInline = "basicWideInline";

		/**
		 * Wide Midline Art Border
		 * @var string
		 */
		const BasicWideMidline = "basicWideMidline";

		/**
		 * Wide Outline Art Border
		 * @var string
		 */
		const BasicWideOutline = "basicWideOutline";

		/**
		 * Bats Art Border
		 * @var string
		 */
		const Bats = "bats";

		/**
		 * Birds Art Border
		 * @var string
		 */
		const Birds = "birds";

		/**
		 * Birds Flying Art Border
		 * @var string
		 */
		const BirdsFlight = "birdsFlight";

		/**
		 * Cabin Art Border
		 * @var string
		 */
		const Cabins = "cabins";

		/**
		 * Cake Art Border
		 * @var string
		 */
		const CakeSlice = "cakeSlice";

		/**
		 * Candy Corn Art Border
		 * @var string
		 */
		const CandyCorn = "candyCorn";

		/**
		 * Knot Work Art Border
		 * @var string
		 */
		const CelticKnotwork = "celticKnotwork";

		/**
		 * Certificate Banner Art Border
		 * @var string
		 */
		const CertificateBanner = "certificateBanner";

		/**
		 * Chain Link Art Border
		 * @var string
		 */
		const ChainLink = "chainLink";

		/**
		 * Champagne Bottle Art Border
		 * @var string
		 */
		const ChampagneBottle = "champagneBottle";

		/**
		 * Black and White Bar Art Border
		 * @var string
		 */
		const CheckedBarBlack = "checkedBarBlack";

		/**
		 * Color Checked Bar Art Border
		 * @var string
		 */
		const CheckedBarColor = "checkedBarColor";

		/**
		 * Checkerboard Art Border
		 * @var string
		 */
		const Checkered = "checkered";

		/**
		 * Christmas Tree Art Border
		 * @var string
		 */
		const ChristmasTree = "christmasTree";

		/**
		 * Circles And Lines Art Border
		 * @var string
		 */
		const CirclesLines = "circlesLines";

		/**
		 * Circles and Rectangles Art Border
		 * @var string
		 */
		const CirclesRectangles = "circlesRectangles";

		/**
		 * Wave Art Border
		 * @var string
		 */
		const ClassicalWave = "classicalWave";

		/**
		 * Clocks Art Border
		 * @var string
		 */
		const Clocks = "clocks";

		/**
		 * Compass Art Border
		 * @var string
		 */
		const Compass = "compass";

		/**
		 * Confetti Art Border
		 * @var string
		 */
		const Confetti = "confetti";

		/**
		 * Confetti Art Border
		 * @var string
		 */
		const ConfettiGrays = "confettiGrays";

		/**
		 * Confetti Art Border
		 * @var string
		 */
		const ConfettiOutline = "confettiOutline";

		/**
		 * Confetti Streamers Art Border
		 * @var string
		 */
		const ConfettiStreamers = "confettiStreamers";

		/**
		 * Confetti Art Border
		 * @var string
		 */
		const ConfettiWhite = "confettiWhite";

		/**
		 * Corner Triangle Art Border
		 * @var string
		 */
		const CornerTriangles = "cornerTriangles";

		/**
		 * Dashed Line Art Border
		 * @var string
		 */
		const CouponCutoutDashes = "couponCutoutDashes";

		/**
		 * Dotted Line Art Border
		 * @var string
		 */
		const CouponCutoutDots = "couponCutoutDots";

		/**
		 * Maze Art Border
		 * @var string
		 */
		const CrazyMaze = "crazyMaze";

		/**
		 * Butterfly Art Border
		 * @var string
		 */
		const CreaturesButterfly = "creaturesButterfly";

		/**
		 * Fish Art Border
		 * @var string
		 */
		const CreaturesFish = "creaturesFish";

		/**
		 * Insects Art Border
		 * @var string
		 */
		const CreaturesInsects = "creaturesInsects";

		/**
		 * Ladybug Art Border
		 * @var string
		 */
		const CreaturesLadyBug = "creaturesLadyBug";

		/**
		 * Cross-stitch Art Border
		 * @var string
		 */
		const CrossStitch = "crossStitch";

		/**
		 * Cupid Art Border
		 * @var string
		 */
		const Cup = "cup";

		/**
		 * Archway Art Border
		 * @var string
		 */
		const DecoArch = "decoArch";

		/**
		 * Color Archway Art Border
		 * @var string
		 */
		const DecoArchColor = "decoArchColor";

		/**
		 * Blocks Art Border
		 * @var string
		 */
		const DecoBlocks = "decoBlocks";

		/**
		 * Gray Diamond Art Border
		 * @var string
		 */
		const DiamondsGray = "diamondsGray";

		/**
		 * Double D Art Border
		 * @var string
		 */
		const DoubleD = "doubleD";

		/**
		 * Diamond Art Border
		 * @var string
		 */
		const DoubleDiamonds = "doubleDiamonds";

		/**
		 * Earth Art Border
		 * @var string
		 */
		const Earth1 = "earth1";

		/**
		 * Earth Art Border
		 * @var string
		 */
		const Earth2 = "earth2";

		/**
		 * Shadowed Square Art Border
		 * @var string
		 */
		const EclipsingSquares1 = "eclipsingSquares1";

		/**
		 * Shadowed Square Art Border
		 * @var string
		 */
		const EclipsingSquares2 = "eclipsingSquares2";

		/**
		 * Painted Egg Art Border
		 * @var string
		 */
		const EggsBlack = "eggsBlack";

		/**
		 * Fans Art Border
		 * @var string
		 */
		const Fans = "fans";

		/**
		 * Film Reel Art Border
		 * @var string
		 */
		const Film = "film";

		/**
		 * Firecracker Art Border
		 * @var string
		 */
		const Firecrackers = "firecrackers";

		/**
		 * Flowers Art Border
		 * @var string
		 */
		const FlowersBlockPrint = "flowersBlockPrint";

		/**
		 * Daisy Art Border
		 * @var string
		 */
		const FlowersDaisies = "flowersDaisies";

		/**
		 * Flowers Art Border
		 * @var string
		 */
		const FlowersModern1 = "flowersModern1";

		/**
		 * Flowers Art Border
		 * @var string
		 */
		const FlowersModern2 = "flowersModern2";

		/**
		 * Pansy Art Border
		 * @var string
		 */
		const FlowersPansy = "flowersPansy";

		/**
		 * Red Rose Art Border
		 * @var string
		 */
		const FlowersRedRose = "flowersRedRose";

		/**
		 * Roses Art Border
		 * @var string
		 */
		const FlowersRoses = "flowersRoses";

		/**
		 * Flowers in a Teacup Art Border
		 * @var string
		 */
		const FlowersTeacup = "flowersTeacup";

		/**
		 * Small Flower Art Border
		 * @var string
		 */
		const FlowersTiny = "flowersTiny";

		/**
		 * Gems Art Border
		 * @var string
		 */
		const Gems = "gems";

		/**
		 * Gingerbread Man Art Border
		 * @var string
		 */
		const GingerbreadMan = "gingerbreadMan";

		/**
		 * Triangle Gradient Art Border
		 * @var string
		 */
		const Gradient = "gradient";

		/**
		 * Handmade Art Border
		 * @var string
		 */
		const Handmade1 = "handmade1";

		/**
		 * Handmade Art Border
		 * @var string
		 */
		const Handmade2 = "handmade2";

		/**
		 * Heart-Shaped Balloon Art Border
		 * @var string
		 */
		const HeartBalloon = "heartBalloon";

		/**
		 * Gray Heart Art Border
		 * @var string
		 */
		const HeartGray = "heartGray";

		/**
		 * Hearts Art Border
		 * @var string
		 */
		const Hearts = "hearts";

		/**
		 * Pattern Art Border
		 * @var string
		 */
		const HeebieJeebies = "heebieJeebies";

		/**
		 * Holly Art Border
		 * @var string
		 */
		const Holly = "holly";

		/**
		 * House Art Border
		 * @var string
		 */
		const HouseFunky = "houseFunky";

		/**
		 * Circular Art Border
		 * @var string
		 */
		const Hypnotic = "hypnotic";

		/**
		 * Ice Cream Cone Art Border
		 * @var string
		 */
		const IceCreamCones = "iceCreamCones";

		/**
		 * Light Bulb Art Border
		 * @var string
		 */
		const LightBulb = "lightBulb";

		/**
		 * Lightning Art Border
		 * @var string
		 */
		const Lightning1 = "lightning1";

		/**
		 * Lightning Art Border
		 * @var string
		 */
		const Lightning2 = "lightning2";

		/**
		 * Map Pins Art Border
		 * @var string
		 */
		const MapPins = "mapPins";

		/**
		 * Maple Leaf Art Border
		 * @var string
		 */
		const MapleLeaf = "mapleLeaf";

		/**
		 * Muffin Art Border
		 * @var string
		 */
		const MapleMuffins = "mapleMuffins";

		/**
		 * Marquee Art Border
		 * @var string
		 */
		const Marquee = "marquee";

		/**
		 * Marquee Art Border
		 * @var string
		 */
		const MarqueeToothed = "marqueeToothed";

		/**
		 * Moon Art Border
		 * @var string
		 */
		const Moons = "moons";

		/**
		 * Mosaic Art Border
		 * @var string
		 */
		const Mosaic = "mosaic";

		/**
		 * Musical Note Art Border
		 * @var string
		 */
		const MusicNotes = "musicNotes";

		/**
		 * Patterned Art Border
		 * @var string
		 */
		const Northwest = "northwest";

		/**
		 * Oval Art Border
		 * @var string
		 */
		const Ovals = "ovals";

		/**
		 * Package Art Border
		 * @var string
		 */
		const Packages = "packages";

		/**
		 * Black Palm Tree Art Border
		 * @var string
		 */
		const PalmsBlack = "palmsBlack";

		/**
		 * Color Palm Tree Art Border
		 * @var string
		 */
		const PalmsColor = "palmsColor";

		/**
		 * Paper Clip Art Border
		 * @var string
		 */
		const PaperClips = "paperClips";

		/**
		 * Papyrus Art Border
		 * @var string
		 */
		const Papyrus = "papyrus";

		/**
		 * Party Favor Art Border
		 * @var string
		 */
		const PartyFavor = "partyFavor";

		/**
		 * Party Glass Art Border
		 * @var string
		 */
		const PartyGlass = "partyGlass";

		/**
		 * Pencils Art Border
		 * @var string
		 */
		const Pencils = "pencils";

		/**
		 * Character Art Border
		 * @var string
		 */
		const People = "people";

		/**
		 * Waving Character Border
		 * @var string
		 */
		const PeopleWaving = "peopleWaving";

		/**
		 * Character With Hat Art Border
		 * @var string
		 */
		const PeopleHats = "peopleHats";

		/**
		 * Poinsettia Art Border
		 * @var string
		 */
		const Poinsettias = "poinsettias";

		/**
		 * Postage Stamp Art Border
		 * @var string
		 */
		const PostageStamp = "postageStamp";

		/**
		 * Pumpkin Art Border
		 * @var string
		 */
		const Pumpkin1 = "pumpkin1";

		/**
		 * Push Pin Art Border
		 * @var string
		 */
		const PushPinNote2 = "pushPinNote2";

		/**
		 * Push Pin Art Border
		 * @var string
		 */
		const PushPinNote1 = "pushPinNote1";

		/**
		 * Pyramid Art Border
		 * @var string
		 */
		const Pyramids = "pyramids";

		/**
		 * Pyramid Art Border
		 * @var string
		 */
		const PyramidsAbove = "pyramidsAbove";

		/**
		 * Quadrants Art Border
		 * @var string
		 */
		const Quadrants = "quadrants";

		/**
		 * Rings Art Border
		 * @var string
		 */
		const Rings = "rings";

		/**
		 * Safari Art Border
		 * @var string
		 */
		const Safari = "safari";

		/**
		 * Saw tooth Art Border
		 * @var string
		 */
		const Sawtooth = "sawtooth";

		/**
		 * Gray Saw tooth Art Border
		 * @var string
		 */
		const SawtoothGray = "sawtoothGray";

		/**
		 * Scared Cat Art Border
		 * @var string
		 */
		const ScaredCat = "scaredCat";

		/**
		 * Umbrella Art Border
		 * @var string
		 */
		const Seattle = "seattle";

		/**
		 * Shadowed Squares Art Border
		 * @var string
		 */
		const ShadowedSquares = "shadowedSquares";

		/**
		 * Shark Tooth Art Border
		 * @var string
		 */
		const SharksTeeth = "sharksTeeth";

		/**
		 * Bird Tracks Art Border
		 * @var string
		 */
		const ShorebirdTracks = "shorebirdTracks";

		/**
		 * Rocket Art Border
		 * @var string
		 */
		const Skyrocket = "skyrocket";

		/**
		 * Snowflake Art Border
		 * @var string
		 */
		const SnowflakeFancy = "snowflakeFancy";

		/**
		 * Snowflake Art Border
		 * @var string
		 */
		const Snowflakes = "snowflakes";

		/**
		 * Sombrero Art Border
		 * @var string
		 */
		const Sombrero = "sombrero";

		/**
		 * Southwest-themed Art Border
		 * @var string
		 */
		const Southwest = "southwest";

		/**
		 * Stars Art Border
		 * @var string
		 */
		const Stars = "stars";

		/**
		 * Stars On Top Art Border
		 * @var string
		 */
		const StarsTop = "starsTop";

		/**
		 * 3-D Stars Art Border
		 * @var string
		 */
		const Stars3d = "stars3d";

		/**
		 * Stars Art Border
		 * @var string
		 */
		const StarsBlack = "starsBlack";

		/**
		 * Stars With Shadows Art Border
		 * @var string
		 */
		const StarsShadowed = "starsShadowed";

		/**
		 * Sun Art Border
		 * @var string
		 */
		const Sun = "sun";

		/**
		 * Whirligig Art Border
		 * @var string
		 */
		const Swirligig = "swirligig";

		/**
		 * Torn Paper Art Border
		 * @var string
		 */
		const TornPaper = "tornPaper";

		/**
		 * Black Torn Paper Art Border
		 * @var string
		 */
		const TornPaperBlack = "tornPaperBlack";

		/**
		 * Tree Art Border
		 * @var string
		 */
		const Trees = "trees";

		/**
		 * Triangle Art Border
		 * @var string
		 */
		const TriangleParty = "triangleParty";

		/**
		 * Triangles Art Border
		 * @var string
		 */
		const Triangles = "triangles";

		/**
		 * Tribal Art Border One
		 * @var string
		 */
		const Tribal1 = "tribal1";

		/**
		 * Tribal Art Border Two
		 * @var string
		 */
		const Tribal2 = "tribal2";

		/**
		 * Tribal Art Border Three
		 * @var string
		 */
		const Tribal3 = "tribal3";

		/**
		 * Tribal Art Border Four
		 * @var string
		 */
		const Tribal4 = "tribal4";

		/**
		 * Tribal Art Border Five
		 * @var string
		 */
		const Tribal5 = "tribal5";

		/**
		 * Tribal Art Border Six
		 * @var string
		 */
		const Tribal6 = "tribal6";

		/**
		 * Twisted Lines Art Border
		 * @var string
		 */
		const TwistedLines1 = "twistedLines1";

		/**
		 * Twisted Lines Art Border
		 * @var string
		 */
		const TwistedLines2 = "twistedLines2";

		/**
		 * Vine Art Border
		 * @var string
		 */
		const Vine = "vine";

		/**
		 * Wavy Line Art Border
		 * @var string
		 */
		const Waveline = "waveline";

		/**
		 * Weaving Angles Art Border
		 * @var string
		 */
		const WeavingAngles = "weavingAngles";

		/**
		 * Weaving Braid Art Border
		 * @var string
		 */
		const WeavingBraid = "weavingBraid";

		/**
		 * Weaving Ribbon Art Border
		 * @var string
		 */
		const WeavingRibbon = "weavingRibbon";

		/**
		 * Weaving Strips Art Border
		 * @var string
		 */
		const WeavingStrips = "weavingStrips";

		/**
		 * White Flowers Art Border
		 * @var string
		 */
		const WhiteFlowers = "whiteFlowers";

		/**
		 * Woodwork Art Border
		 * @var string
		 */
		const Woodwork = "woodwork";

		/**
		 * Crisscross Art Border
		 * @var string
		 */
		const XIllusions = "xIllusions";

		/**
		 * Triangle Art Border
		 * @var string
		 */
		const ZanyTriangles = "zanyTriangles";

		/**
		 * Zigzag Art Border
		 * @var string
		 */
		const ZigZag = "zigZag";

		/**
		 * Zigzag stitch
		 * @var string
		 */
		const ZigZagStitch = "zigZagStitch";


		
	}
	
}

?>