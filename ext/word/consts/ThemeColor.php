<?php 

namespace Word\Consts
{

	/**
	 * Theme Color
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_ThemeColor.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class ThemeColor
	{
		
		/**
		 * Dark 1 Theme Color
		 * @var string
		 */
		const Dark1 = "dark1";

		/**
		 * Light 1 Theme Color
		 * @var string
		 */
		const Light1 = "light1";

		/**
		 * Dark 2 Theme Color
		 * @var string
		 */
		const Dark2 = "dark2";

		/**
		 * Light 2 Theme Color
		 * @var string
		 */
		const Light2 = "light2";

		/**
		 * Accent 1 Theme Color
		 * @var string
		 */
		const Accent1 = "accent1";

		/**
		 * Accent 2 Theme Color
		 * @var string
		 */
		const Accent2 = "accent2";

		/**
		 * Accent 3 Theme Color
		 * @var string
		 */
		const Accent3 = "accent3";

		/**
		 * Accent 4 Theme Color
		 * @var string
		 */
		const Accent4 = "accent4";

		/**
		 * Accent 5 Theme Color
		 * @var string
		 */
		const Accent5 = "accent5";

		/**
		 * Accent 6 Theme Color
		 * @var string
		 */
		const Accent6 = "accent6";

		/**
		 * Hyperlink Theme Color
		 * @var string
		 */
		const Hyperlink = "hyperlink";

		/**
		 * Followed Hyperlink Theme Color
		 * @var string
		 */
		const FollowedHyperlink = "followedHyperlink";

		/**
		 * No Theme Color
		 * @var string
		 */
		const None = "none";

		/**
		 * Background 1 Theme Color
		 * @var string
		 */
		const Background1 = "background1";

		/**
		 * Text 1 Theme Color
		 * @var string
		 */
		const Text1 = "text1";

		/**
		 * Background 2 Theme Color
		 * @var string
		 */
		const Background2 = "background2";

		/**
		 * Text 2 Theme Color
		 * @var string
		 */
		const Text2 = "text2";


		
	}
	
}

?>