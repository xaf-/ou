<?php 

namespace Word\Consts
{

	/**
	 * Horizontal Alignment Type
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_Jc.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class Jc
	{
		
		/**
		 * Align Left
		 * @var string
		 */
		const Left = "left";

		/**
		 * Align Center
		 * @var string
		 */
		const Center = "center";

		/**
		 * Align Right
		 * @var string
		 */
		const Right = "right";

		/**
		 * Justified
		 * @var string
		 */
		const Both = "both";

		/**
		 * Medium Kashida Length
		 * @var string
		 */
		const MediumKashida = "mediumKashida";

		/**
		 * Distribute All Characters Equally
		 * @var string
		 */
		const Distribute = "distribute";

		/**
		 * Align to List Tab
		 * @var string
		 */
		const NumTab = "numTab";

		/**
		 * Widest Kashida Length
		 * @var string
		 */
		const HighKashida = "highKashida";

		/**
		 * Low Kashida Length
		 * @var string
		 */
		const LowKashida = "lowKashida";

		/**
		 * Thai Language Justification
		 * @var string
		 */
		const ThaiDistribute = "thaiDistribute";


		
	}
	
}

?>