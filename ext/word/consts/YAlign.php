<?php 

namespace Word\Consts
{

	/**
	 * Vertical Alignment Location
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_YAlign.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class YAlign
	{
		
		/**
		 * In line With Text
		 * @var string
		 */
		const Inline = "inline";

		/**
		 * Top
		 * @var string
		 */
		const Top = "top";

		/**
		 * Centered Vertically
		 * @var string
		 */
		const Center = "center";

		/**
		 * Bottom
		 * @var string
		 */
		const Bottom = "bottom";

		/**
		 * Inside Anchor Extents
		 * @var string
		 */
		const Inside = "inside";

		/**
		 * Outside Anchor Extents
		 * @var string
		 */
		const Outside = "outside";


		
	}
	
}

?>