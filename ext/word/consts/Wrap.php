<?php 

namespace Word\Consts
{

	/**
	 * Text Wrapping around Text Frame Type
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_Wrap.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class Wrap
	{
		
		/**
		 * Default Text Wrapping Around Frame
		 * @var string
		 */
		const Auto = "auto";

		/**
		 * No Text Wrapping Beside Frame
		 * @var string
		 */
		const NotBeside = "notBeside";

		/**
		 * Allow Text Wrapping Around Frame
		 * @var string
		 */
		const Around = "around";

		/**
		 * Tight Text Wrapping Around Frame
		 * @var string
		 */
		const Tight = "tight";

		/**
		 * Through Text Wrapping Around Frame
		 * @var string
		 */
		const Through = "through";

		/**
		 * No Text Wrapping Around Frame
		 * @var string
		 */
		const None = "none";


		
	}
	
}

?>