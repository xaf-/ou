<?php 

namespace Word\Consts
{

	/**
	 * Range Permision Editing Group
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_EdGrp.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class EdGrp
	{
		
		/**
		 * No Users Have Editing Permissions
		 * @var string
		 */
		const None = "none";

		/**
		 * All Users Have Editing Permissions
		 * @var string
		 */
		const Everyone = "everyone";

		/**
		 * Administrator Group
		 * @var string
		 */
		const Administrators = "administrators";

		/**
		 * Contributors Group
		 * @var string
		 */
		const Contributors = "contributors";

		/**
		 * Editors Group
		 * @var string
		 */
		const Editors = "editors";

		/**
		 * Owners Group
		 * @var string
		 */
		const Owners = "owners";

		/**
		 * Current Group
		 * @var string
		 */
		const Current = "current";


		
	}
	
}

?>