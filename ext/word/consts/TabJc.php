<?php 

namespace Word\Consts
{

	/**
	 * Custom Tab Stop Type
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_TabJc.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class TabJc
	{
		
		/**
		 * No Tab Stop
		 * @var string
		 */
		const Clear = "clear";

		/**
		 * Left Tab
		 * @var string
		 */
		const Left = "left";

		/**
		 * Centered Tab
		 * @var string
		 */
		const Center = "center";

		/**
		 * Right Tab
		 * @var string
		 */
		const Right = "right";

		/**
		 * Decimal Tab
		 * @var string
		 */
		const Decimal = "decimal";

		/**
		 * Bar Tab
		 * @var string
		 */
		const Bar = "bar";

		/**
		 * List Tab
		 * @var string
		 */
		const Num = "num";


		
	}
	
}

?>