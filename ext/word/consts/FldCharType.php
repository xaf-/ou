<?php 

namespace Word\Consts
{

	/**
	 * Complex Field Character Type
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_FldCharType.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class FldCharType
	{
		
		/**
		 * Start Character
		 * @var string
		 */
		const Begin = "begin";

		/**
		 * Separator Character
		 * @var string
		 */
		const Separate = "separate";

		/**
		 * End Character
		 * @var string
		 */
		const End = "end";


		
	}
	
}

?>