<?php 

namespace Word\Consts
{

	/**
	 * Location of Custom XML Markup Displacing an Annotation
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_DisplacedByCustomXml.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class DisplacedByCustomXml
	{
		
		/**
		 * Displaced by Next Custom XML Markup Tag
		 * @var string
		 */
		const Next = "next";

		/**
		 * Displaced by Previous Custom XML Markup Tag
		 * @var string
		 */
		const Prev = "prev";


		
	}
	
}

?>