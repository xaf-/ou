<?php 

namespace Word\Consts
{

	/**
	 * Footnote Positioning Location
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_FtnPos.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class FtnPos
	{
		
		/**
		 * Footnotes Positioned at Page Bottom
		 * @var string
		 */
		const PageBottom = "pageBottom";

		/**
		 * Footnotes Positioned Beneath Text
		 * @var string
		 */
		const BeneathText = "beneathText";

		/**
		 * Footnotes Positioned At End of Section
		 * @var string
		 */
		const SectEnd = "sectEnd";

		/**
		 * Footnotes Positioned At End of Document
		 * @var string
		 */
		const DocEnd = "docEnd";


		
	}
	
}

?>