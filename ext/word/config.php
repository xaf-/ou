<?php

	namespace Word;
	
	class Config
	{
		
		public static $_incs = array();
		public static function IncClass($className)
		{
			if (is_array($className)){
				foreach ($className as $c)
					self::IncClass($c);
				return;
			}
			
			$path = dirname(__FILE__) . "/" . ($className) . ".php";
			
			if (isset(self::$_incs[$path])) return;
			self::$_incs[$path] = $className;
			
			
			$path = realpath($path);
			
			if (is_file($path))
			{
				@require_once($path);
			}
		}
		
	}

?>