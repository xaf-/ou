<?php

	namespace Word;
	
	abstract class Base {
		
		public function &__get($property) {
			$v = $this->{"get" . ucfirst ( $property )}();
			return $v;
		}
	
		public function __set($property, $value) {
			return call_user_func ( array (
					$this,
					"set" . ucfirst ( $property ) 
			), $value );
		}
		
	}

?>