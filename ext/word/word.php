<?php

	namespace Word;
	
	require_once(dirname(__FILE__) . "/config.php");
	
	use Word\Files\Styles;
	use Word\Files\Document;
	
	Config::IncClass(
		array(
			"Files\Document",
			"Files\Styles"
		)
	);
	
	/**
	 * @property-read \Word\Files\Document $document
	 * @property-read \Word\Files\Styles $styles
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: word.php 300 2013-08-23 11:00:53Z xaguilarf $
	 *
	 */
	class Word extends Base
	{
		
		private $_document;
		protected function getDocument(){ return $this->_document; }
		
		private $_styles;
		protected function getStyles(){ return $this->_styles; }
		
		public function __construct()
		{
			$this->_document = new Document($this);
			$this->_styles = new Styles($this);
		}
		
		public function test()
		{
			
			$dir = dirname(__FILE__) . "/test/files/";
			
			file_put_contents($dir . "word/document.xml", $this->document->_docWrite());
			file_put_contents($dir . "word/styles.xml", $this->styles->_docWrite());
			
			$fnc = function ($dir, $zipArchive, $fnc, $p = null){
				
				if ($p == null) {
					$p = $dir;
					$dir = "";
				}
			    if (is_dir($p . $dir)) {
			        if ($dh = opendir($p . $dir)) {
			
			            //Add the directory
			            $zipArchive->addEmptyDir($dir);
			            
			            // Loop through all the files
			            while (($file = readdir($dh)) !== false) {
			            
			                //If it's a folder, run the function again!
			                if(!is_file($p . $dir . $file)){
			                    // Skip parent and root directories
			                    if( ($file !== ".") && ($file !== "..")){
			                        $fnc($dir . $file . "/", $zipArchive, $fnc, $p);
			                    }
			                    
			                }else{
			                    // Add the files
			                    $zipArchive->addFile($p . $dir . $file, $dir . $file);
			                    
			                    
			                }
			            }
			        }
			    }
			};

			$z = new \ZipArchive();
			@unlink($dir . "/../test.docx");
			$z->open($dir . "/../test.docx", \ZIPARCHIVE::CREATE);
			$fnc($dir, $z, $fnc);
			$z->close();
			
			
			// header("Location: ./test/test.docx");
			//header("Content-type: application/msword");
			//header('Content-Disposition: inline; filename="test.docx";');
			//header('Content-Disposition: attachment; filename="test.docx"');
			/* --- */
			
			
			echo '<iframe style="width: 100%; height: 100%" src="http://docs.google.com/viewer?url='.\OU_Path::Path2Url($dir. "../test.docx").'&embedded=true"></iframe>';
			
		}
		
	}

?>