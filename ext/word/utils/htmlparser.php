<?php

	namespace Word\Utils;
	
	use Word\ST_Border;
	use Word\ST_TblLayoutType;
	use Word\ST_TblWidth;
	use Word\ST_HighlightColor;
	use Word\ST_Underline;
	use Word\ST_BrClear;
	use Word\Objects\Styles\RPr;
	use Word\ST_BrType;
	use Word\Config;
	use Word\Base;
	use Word\Objects\Body;
	
	Config::IncClass(
		array(
			"Base",
			"Config",
			"Objects\Body"
		)
	);
	
	/**
	 * @property Body $body
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: htmlparser.php 298 2013-08-22 10:57:02Z xaguilarf $
	 */
	class HtmlParser extends Base
	{
		
		private $_body;
		protected function getBody(){ return $this->_body; }
		
		/**
		 * @param \Word\Objects\Body $body
		 */
		public function __construct($body)
		{
			$this->_body = $body;
		}
		
		public function parse($html)
		{
			$doc = new \DOMDocument();
			$doc->loadHTML($html);
			
			$body = $doc->getElementsByTagName('body')->item(0);
			foreach ($body->childNodes as $child)
			{
				$this->_parseRootChild($child);
			}
			
		}
		
		/**
		 * @param DOMElement $child
		 */
		private function _parseRootChild($child)
		{
			return $this->_parseText($child, $this->body->AddParagraph());
			/*switch ($child->tagName)
			{
				case 'div':
				case 'p':
					
					$p = $this->body->AddParagraph();
					$this->_parseText($child, $p);					
					
					break;
					
				case "table":
					
					$table = $this->body->AddTable();
					$this->_parseTable($child, $table);
					
					break;
					
			}*/
		}
		
		/**
		 * @param DOMELement $dom
		 * @param \Word\Objects\Tbl $table
		 */
		private function _parseTable($dom, $table)
		{
			
			// Styles
			
			if ($dom->hasAttribute("border"))
			{
				$border = intval($dom->getAttribute("border"));
				$opts = array("val" => ST_Border::Single, "color" => "000000", "sz" => 8 * $border, "space" => 0);
				$table->tblPr->tblBorders->top->set($opts);
				$table->tblPr->tblBorders->right->set($opts);
				$table->tblPr->tblBorders->bottom->set($opts);
				$table->tblPr->tblBorders->left->set($opts);
				$table->tblPr->tblBorders->insideH->set($opts);
				$table->tblPr->tblBorders->insideV->set($opts);
			}
			
			if ($s = $dom->getAttribute("style"))
			{
				\OU::IncExt("cssParse");
				$css = new \CSSParse\Parser(".i{ $s }");
				$parseCss = $css->parse();
				$parseCss->expandShorthands();
				$rules = $parseCss->getContents()[0]->getRules();
				foreach ($rules as $rule)
				{
					/* @var $rule \CSSParse\Rule */
					$n = $rule->getRule();
					switch (strtolower(trim($n)))
					{
				
						case "border-top-width":
							$value = $rule->getValue()->getListComponents()[0];
							if (false) $value = new \CSSParse\Size();
							$table->tblPr->tblBorders->top->sz->set($value->getSize(), $value->getUnit());
							break;
					}
				}
			}
						
			//
			
			// $table->tblPr->tblLayout = ST_TblLayoutType::Autofit;
			
			$table->tblPr->tblW->w = 5000;
			$table->tblPr->tblW->type = ST_TblWidth::Pct;
			
			
			foreach ($dom->childNodes as $child)
			{
				switch (strtolower($child->nodeName))
				{
					case "tbody":
						if (!isset($child->childNodes[0])) break;
						$child = $child->childNodes[0];
						if ($child->nodeName != "tr") break;
					case "tr":
						$tr = $table->AddTr();
						foreach ($child->childNodes as $child2)
						{
							switch (strtolower($child2->nodeName))
							{
								case "td":
									$td = $tr->AddTc();
									
									//$td->tcPr->tcW->type = ST_TblWidth::Auto;
									//$td->tcPr->tcW->w = 0;
									
									$td->tcPr->tcMar->set(
										array(
											"top" => array("w" => 100.0, "type" => "dxa"),
											"left" => array("w" => 100.0, "type" => "dxa"),
											"right" => array("w" => 100.0, "type" => "dxa"),
											"bottom" => array("w" => 100.0, "type" => "dxa")
										)
									);
									$p = $td->AddParagraph(); foreach ($child2->childNodes as $child3) $this->_parseText($child3, $p);
									break;
								default: // Ignore?
							}
						}						
						break;
				}
			}
		}
		
		/**
		 * @param DOMELement $dom
		 * @param \Word\Groups\Paragraph $parent
		 * @param \Word\Objects\Styles\RPr $style
		 */
		private function _parseText($dom, $parent, $style = array())
		{
			if (false) $test = new RPr();
			
			switch ($dom->nodeName)
			{
				
				case "p":
				case "div":
					
					$parent = $this->body->AddParagraph();
					break;
					
				case "table":
					$this->_parseTable($dom, $this->body->AddTable());
					return;
				
				case 'b':
					$style["b"] = true;
					break;
				case 'i':
					$style["i"] = true;
					break;
				case 'u':
					$style["u"]["val"] = ST_Underline::Single;
					
					break;
				case 'br':
					$parent->AddTextRun()->AddBr(ST_BrType::TextWrapping, ST_BrClear::Left);
					return;
			}

			
			// Style
			if (method_exists($dom, "getAttribute") && $s = $dom->getAttribute("style"))
			{
				
				\OU::IncExt("cssParse");
				$css = new \CSSParse\Parser(".i{ $s }");
				
				$rules = $css->parse()->getContents()[0]->getRules();
				foreach ($rules as $rule)
				{
					/* @var $rule \CSSParse\Rule */
					switch (strtolower(trim($rule->getRule())))
					{
						
						case "font-weight":
							
							switch (strtolower($rule->getValue()))
							{
								case "bold":
									$style["b"] = true;
									break;
							}
							
							break;
							
						case "font-style":
							
							switch (strtolower($rule->getValue()))
							{
								case "italic":
									$style["i"] = true;
									break;
							}
							
							break;
						
						case "color":
							
							$v = $rule->getValue()->getColor();
							$hex = str_pad(dechex($v["r"]->getSize()), 2, "0", STR_PAD_LEFT);
							$hex .= str_pad(dechex($v["g"]->getSize()), 2, "0", STR_PAD_LEFT);
							$hex .= str_pad(dechex($v["b"]->getSize()), 2, "0", STR_PAD_LEFT);
							$style["color"]["val"] = $hex;
							
							break;
							
							
						case "background-color":
							
							if (ST_HighlightColor::valid($rule->getValue()))
								$style["highlight"] = $rule->getValue();
							
							break;
							
						case "background":
							
							foreach ($rule->getValue()->getListComponents() as $v)
							{
								if (ST_HighlightColor::valid($v))
								{
									$style["highlight"] = $v;
									break;
								}
							}
							
							break;
									
					}
				}
			}			
			
			// Process childs or add
			
			if ($dom->hasChildNodes())
			{
				
				foreach ($dom->childNodes as $child)
				{
					$this->_parseText($child, $parent, $style);
				}
				
			}else{
				
				if (get_class($dom) == "DOMText")
				{
				
					$text = $dom->textContent;
					$text = str_replace("\t", " ", $text);
					$text = str_replace("\n", " ", $text);
					$text = str_replace("\r", " ", $text);
					while (strpos($text, "  ") !== false) $text = str_replace("  ", " ", $text);
					$text = trim($text);
					
					$run = $parent->AddTextRun();
					$run->rPr->set($style);
					$run->AddText($text);

				}
				
			}
			
		}
		
	}


?>