<?php

	namespace Word\Utils;

	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 * 
	 * @property number value
	 * @property number px
	 * @property number pt
	 * @property number cm
	 * @property number mm
	 * @property number dxa
	 * 
	 * @property string auto
	 */
	class Unit extends Base
	{
		
		/**
		 * Original value in Points
		 * @var number
		 */
		private $_val = null;
		
		protected function getPx() { return $this->_val * 96 / 72; }
		protected function setPx($val) { $this->_val = $val / 96 * 72; }
		
		protected function getPt() { return $this->_val; }
		protected function setPt($val) { $this->_val = $val; }
		
		protected function getCm() { return $this->_val * 0.0352777778; }
		protected function setCm($val) { return $val / 0.0352777778; }
		
		protected function getMm() { return $this->cm * 10; }
		protected function setMm($val){ $this->cm = $val / 10; }
		
		protected function getDxa() { return $this->cm * 567; }
		protected function setDxa($val) { $this->cm = $val / 567; }
		
		protected function getValue() { return $this->_val; }
		
		protected function getAuto() { return $this->pt . "pt"; }
		protected function setAuto($val){ 
			if (preg_match("/^([\d\.]+)(\w*)$/", $val, $m))
			{
				$this->set($m[1], $m[2]);
			}
		}
		
		
		public function set($val, $from)
		{
			switch ($from)
			{
				case "px": $this->px = $val; break;
				case "cm": $this->cm = $val; break;
				case "mm": $this->mm = $val; break;
				case "dxa": $this->dxa = $val; break;
				case "pt": $this->pt = $val; break;
				default: $this->pt = $val;
			}
		}
		
	}
	 
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: org.eclipse.php.ui.prefs 240 2013-05-23 11:21:29Z xaguilarf $
	 *
	 * @property number pct 0-5000
	 */
	class RelativeUnit extends Unit
	{
		
		protected function getPct() { return ""; }
		protected function setPct() { }
		
		public function set($val, $from)
		{
			switch ($from)
			{
				case "%": $this->pct = $val * 5000 / 100; break;
				default: parent::set($val, $from);
			}
		}
	}

?>