<?php

	\Word\Config::IncClass("Consts\LineSpacingRule");

	global $s1, $sl;
	$s1 = microtime(true);
	$sl = $s1;
	function x($x=""){ /*
		global $s1, $sl;
		$c = microtime(true);
		echo "\n<br />($x = ".($c - $s1)." ,\t ".($c - $sl).")";
		
		$sl = $c;
	*/}
	
	
	$doc = new \Word\Word();
	
	/* ***** Styles ***** */
	
	$doc->styles->CreateStyle("test", \Word\Consts\StyleType::Character)->set(array(
		"pPr" => array(
			"spacing" => array(
				"before" => 0,
				"after" => 0,
				"line" => 240,
				"lineRule" => \Word\Consts\LineSpacingRule::Auto
			)
		),
		"rPr" => array(
			"b" => array(
				"val" => true
			)
		),
		"styleId" => "test",
		"customStyle" => true
	));
	
	/* ***************** */
	
	$body = $doc->document->body;
	
	// Paragraph
	$p = $body->createP();
	
		// TextRun
		$p->createR()->createT("hola");
		
		$run = $p->createR();
		$run->createT("hello");
		$run->rPr->b->val = true;
		$run->rPr->i->val = true;
		$run->createBr();
		$run->createT("adeu");
		$run->rPr->rStyle->val = "test";
		
		$run = $p->createR();
		
		
		$run->createT("(");
			//$run->dayShort->add();
			$run->createT("/");
			//$run->monthShort->add();
			$run->createT("/");
			//$run->yearShort->add();
		$run->createT(") (");
			//$run->dayLong->add();
			$run->createT("/");
			//$run->monthLong->add();
			$run->createT("/");
			//$run->yearLong->add();
		$run->createT(")"); 
		
		$run->pgNum;
		
	$p = $body->createP();
	
		$p->createR()->createT('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');

		
	$table = $body->createTbl();
	
	$table->tblPr->tblW->w = 0;
	$table->tblPr->tblW->type = \Word\Consts\TblWidth::Auto;
	
	for ($y = 0; $y < 10; $y++)
	{
		
		$tr = $table->createTr();
		for ($x = 0; $x < 10; $x++)
		{
			
			$td = $tr->createTc();
			
			switch ($y)
			{
				case 0:
					$table->tblGrid->createGridCol()->w = 900;
					break;
				case 1: // Test colors
					$colors = array("58B442","FF007A","FF913D","00A3BB","A13CB4","37C78F","FD573D","5DAFDD","63782F","FF001A","A63232","A5CA54","1B887A","F176A7");
					$td->tcPr->shd->fill = $colors[$x];
					break;
				case 2: // Colspans
					
					if ($x == 3) $td->tcPr->gridSpan->val = 2;
					if ($x == 4) $x++;
						
					if ($x == 0) $td->tcPr->gridSpan->val = 2;
					if ($x == 1) $x++;
								
					break;
			}
			
			if ($y % 2 == $x % 2){ }
			
			$td->tcPr->tcMar->set(array(
				"top" => array("w" => 50, "type" => \Word\Consts\TblWidth::Dxa),
				"bottom" => array("w" => 50, "type" => \Word\Consts\TblWidth::Dxa),
				"left" => array("w" => 50, "type" => \Word\Consts\TblWidth::Dxa),
				"right" => array("w" => 50, "type" => \Word\Consts\TblWidth::Dxa)
			));
			
			
			$td->createP()->createR()->createT("Cell ".($x+1)."x".($y+1));
		}
	}
	
	// $end_time = microtime(true);
	
	//$body->createP()->createR()->createT()->content = 'Create time : ' . ($end_time - $create_time);
	//$body->createP()->createR()->createT()->content = 'Total time : ' . ($end_time - $start_time);
	
	/*
	 $html =
	'
	<div>
	Hola<b>asdasd</b>xxx <br /> Adeu <i>asda<b>xxx</b>d</i> uuu
	<p>
	x1x2<u style="color:#FF0000; background:blue left; content:\';\'">x3</u>x4
	</p>
	<table border="1" style="border:1px">
	<tr>
	<td>123123123</td><td>456</td>
	</tr>
	</table>
	</div>
	';
	
	
	$doc->document->body->AddHtml($html);
	
	echo '<div style="float:right;width:49%">' . $html . '</div>';
	$doc->test();
	
	die();
	*/
	
?>