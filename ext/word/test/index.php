<?php

	require_once(dirname(__FILE__) . "/../../../class.ou_config.php");
	OU::IncClass("OU_Debug");
	OU::IncClassHelper("OU_Utils_Format");
	
	require_once(dirname(__FILE__) . "/../word.php");
	error_reporting(E_ALL);
	ini_set('error_reporting', E_ALL);
	ini_set("display_errors", "on");

	class Test
	{
		public static function Exec($fn)
		{
			$file = dirname(__FILE__) . "/tests/" . $fn;
?>
			<div class="tabs">
			
				<div class="tab">Result</div>
				<div class="tab">PHP</div>
				<div class="tab">Document.xml</div>
				<div class="tab">Styles.xml</div>
				
				<div class="content"><?php
				
					$memory = memory_get_usage();
					$time = microtime(true);
					require_once($file);
					$memory_i = memory_get_usage() - $memory;
					if (isset($doc)) $doc->test();
					$time = microtime(true) - $time;
					$memory = memory_get_usage() - $memory;
					
				?></div>
				<div class="content" style="padding: 20px"><?php 
					 // echo OU_Debug::syntax(file_get_contents($file));
				?></div>
				<div class="content" style="padding: 20px"><?php
  					echo OU_Debug::syntax(file_get_contents(dirname(__FILE__) . "/files/word/document.xml"), "xml"); 
				?></div>
				<div class="content" style="padding: 20px"><?php
					echo OU_Debug::syntax(file_get_contents(dirname(__FILE__) . "/files/word/styles.xml"), "xml"); 
				?></div>
			</div>	
			<div class="panel">
				<h2>Info</h2>
				<div style="padding-left: 10px">
					<?php
					echo '<div>Total time : ' . number_format($time, 4) . 's</div>';
					echo '<div>Create memory usage: ' . OU_Utils_Format::bytes($memory_i) . '</div>';
					echo '<div>Total memory usage: ' . OU_Utils_Format::bytes($memory) . '</div>';
					echo '<div>File size: ' . OU_Utils_Format::bytes(filesize(dirname(__FILE__) . "/test.docx")) . '</div>';
					?>
				</div>
				<br />
				<h2>Includes (<?=count(\Word\Config::$_incs)?>)</h2>
				<div style="padding-left: 10px">
				<?php
					 foreach (\Word\Config::$_incs as $inc)
					 {
					 	echo '<div>' . $inc . '</div>';
					 }
				?>
				</div>
			</div>				
<?php 
			// 
		}
	}
	
?>
<html>
<head>
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script>
		$(function(){
			$(".tabs .tab").on("click", function(){
				var t = $(this).parent();
				var i = $(this).index(".tab");
				var cs = t.children(".content");
				var c = $(cs.get(i));
				
				cs = cs.not(c);
				cs.not();
				
				cs.add(t.children(".tab").not(this)).removeClass("showing");
				c.add(this).addClass("showing");
				
			});
			$(".tabs .tab:first").click();
		});
	</script>
	<style>
		body{
			border: 0;
			padding: 0;
			margin: 0;
			font-family: Arial;
			width: 100%;
			height: 100%;
		}
		.page
		{
			position: absolute;
			left: 0;
			top: 0;
			right: 0;
			bottom: 0;
			margin: 20px;
		}
		h2
		{
			font-size: 18px;
			padding: 0;
			margin: 0;		
		}
		.panel
		{
			position: absolute;
			right: 0;
			width: 290px;
			top: 0;
			bottom: 0;
			overflow: auto;
			font-size: 80%;
		}
		.tabs{
			white-space: nowrap;
			position: absolute;
			left: 0;
			top: 0;
			right: 300px;
			bottom: 0;
		}
		.tabs .tab
		{
			height: 30px;
			line-height: 30px;
			float: left;
			padding: 0 10px;
			white-space: nowrap;
			cursor: default;
		}
		.tabs .tab:hover
		{
			background: #F0F0F0;
		}
		.tabs .tab.showing,
		.tabs .tab.showing:hover
		{
			background: #E0E0E0;
		}
		.tabs .content
		{
			position: absolute;
			left: 0; top: 30px; bottom: 0; right: 0;
			overflow: auto;
			clear: both;
			display: none;
			background: #E0E0E0;
		}
		.tabs .content.showing
		{
			display: block;
		}
	</style>
</head>
<body>

	<div class="page">
		<?php Test::Exec("basic.php") ?>
	</div>

</body>
</html>
