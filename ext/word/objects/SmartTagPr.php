<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_SmartTagPr.html 
	 * @property-read Attr[] $attr Smart Tag Property
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class SmartTagPr extends Object
	{
		
		/* --- Childrens --- */

		private $_attr;

		
		/* --- Childrens --- */

		protected function getAttr(){ return $this->_attr->get(); }
		public function createAttr(){ $new = new \Word\Objects\Attr("w:attr"); $this->attr->AddRef($new); return $new; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\Attr"
				)
			);
				
			parent::__construct($name);

			$this->_attr = new \Word\PropertyArray($this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 

				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{

		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>