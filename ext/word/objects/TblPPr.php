<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_TblPPr.html 
	 * @property-read TwipsMeasure $leftFromText Distance From Left of Table to Text
	 * @property-read TwipsMeasure $rightFromText (Distance From Right of Table to Text
	 * @property-read TwipsMeasure $topFromText Distance From Top of Table to Text
	 * @property-read TwipsMeasure $bottomFromText Distance From Bottom of Table to Text
	 * @property-read VAnchor $vertAnchor Table Vertical Anchor
	 * @property-read HAnchor $horzAnchor Table Horizontal Anchor
	 * @property-read XAlign $tblpXSpec Relative Horizontal Alignment From Anchor
	 * @property-read SignedTwipsMeasure $tblpX Absolute Horizontal Distance From Anchor
	 * @property-read YAlign $tblpYSpec Relative Vertical Alignment from Anchor
	 * @property-read SignedTwipsMeasure $tblpY Absolute Vertical Distance From Anchor
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class TblPPr extends Object
	{
		
		/* --- Attributes --- */

		private $_leftFromText;
		private $_rightFromText;
		private $_topFromText;
		private $_bottomFromText;
		private $_vertAnchor;
		private $_horzAnchor;
		private $_tblpXSpec;
		private $_tblpX;
		private $_tblpYSpec;
		private $_tblpY;

		
		/* --- Attributes --- */

		protected function getLeftFromText(){ return $this->_leftFromText; }
		protected function setLeftFromText($val){ $this->_leftFromText = $val; }

		protected function getRightFromText(){ return $this->_rightFromText; }
		protected function setRightFromText($val){ $this->_rightFromText = $val; }

		protected function getTopFromText(){ return $this->_topFromText; }
		protected function setTopFromText($val){ $this->_topFromText = $val; }

		protected function getBottomFromText(){ return $this->_bottomFromText; }
		protected function setBottomFromText($val){ $this->_bottomFromText = $val; }

		protected function getVertAnchor(){ return $this->_vertAnchor; }
		protected function setVertAnchor($val){ $this->_vertAnchor = $val; }

		protected function getHorzAnchor(){ return $this->_horzAnchor; }
		protected function setHorzAnchor($val){ $this->_horzAnchor = $val; }

		protected function getTblpXSpec(){ return $this->_tblpXSpec; }
		protected function setTblpXSpec($val){ $this->_tblpXSpec = $val; }

		protected function getTblpX(){ return $this->_tblpX; }
		protected function setTblpX($val){ $this->_tblpX = $val; }

		protected function getTblpYSpec(){ return $this->_tblpYSpec; }
		protected function setTblpYSpec($val){ $this->_tblpYSpec = $val; }

		protected function getTblpY(){ return $this->_tblpY; }
		protected function setTblpY($val){ $this->_tblpY = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\TwipsMeasure",
					"Consts\VAnchor",
					"Consts\HAnchor",
					"Consts\XAlign",
					"Consts\SignedTwipsMeasure",
					"Consts\YAlign"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->leftFromText === null && 
				$this->rightFromText === null && 
				$this->topFromText === null && 
				$this->bottomFromText === null && 
				$this->vertAnchor === null && 
				$this->horzAnchor === null && 
				$this->tblpXSpec === null && 
				$this->tblpX === null && 
				$this->tblpYSpec === null && 
				$this->tblpY === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->leftFromText !== null) $xml->writeAttribute("w:leftFromText", $this->leftFromText);
			if ($this->rightFromText !== null) $xml->writeAttribute("w:rightFromText", $this->rightFromText);
			if ($this->topFromText !== null) $xml->writeAttribute("w:topFromText", $this->topFromText);
			if ($this->bottomFromText !== null) $xml->writeAttribute("w:bottomFromText", $this->bottomFromText);
			if ($this->vertAnchor !== null) $xml->writeAttribute("w:vertAnchor", $this->vertAnchor);
			if ($this->horzAnchor !== null) $xml->writeAttribute("w:horzAnchor", $this->horzAnchor);
			if ($this->tblpXSpec !== null) $xml->writeAttribute("w:tblpXSpec", $this->tblpXSpec);
			if ($this->tblpX !== null) $xml->writeAttribute("w:tblpX", $this->tblpX);
			if ($this->tblpYSpec !== null) $xml->writeAttribute("w:tblpYSpec", $this->tblpYSpec);
			if ($this->tblpY !== null) $xml->writeAttribute("w:tblpY", $this->tblpY);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>