<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_Language.html 
	 * @property-read Lang $val Latin Language
	 * @property-read Lang $eastAsia East Asian Language
	 * @property-read Lang $bidi Complex Script Language
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class Language extends Object
	{
		
		/* --- Attributes --- */

		private $_val;
		private $_eastAsia;
		private $_bidi;

		
		/* --- Attributes --- */

		protected function getVal(){ return $this->_val; }
		protected function setVal($val){ $this->_val = $val; }

		protected function getEastAsia(){ return $this->_eastAsia; }
		protected function setEastAsia($val){ $this->_eastAsia = $val; }

		protected function getBidi(){ return $this->_bidi; }
		protected function setBidi($val){ $this->_bidi = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\Lang"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->val === null && 
				$this->eastAsia === null && 
				$this->bidi === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->val !== null) $xml->writeAttribute("w:val", $this->val);
			if ($this->eastAsia !== null) $xml->writeAttribute("w:eastAsia", $this->eastAsia);
			if ($this->bidi !== null) $xml->writeAttribute("w:bidi", $this->bidi);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>