<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_TblWidth.html 
	 * @property-read DecimalNumber $w Table Width Value
	 * @property-read TblWidth $type Table Width Type
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class TblWidth extends Object
	{
		
		/* --- Attributes --- */

		private $_w;
		private $_type;

		
		/* --- Attributes --- */

		protected function getW(){ return $this->_w; }
		protected function setW($val){ $this->_w = $val; }

		protected function getType(){ return $this->_type; }
		protected function setType($val){ $this->_type = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\DecimalNumber",
					"Consts\TblWidth"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->w === null && 
				$this->type === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->w !== null) $xml->writeAttribute("w:w", $this->w);
			if ($this->type !== null) $xml->writeAttribute("w:type", $this->type);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>