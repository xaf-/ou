<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_Styles.html 
	 * @property-read DocDefaults $docDefaults Document Default Paragraph and Run Properties
	 * @property-read LatentStyles $latentStyles Latent Style Information
	 * @property-read Style[] $style Style Definition
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class Styles extends Object
	{
		
		/* --- Childrens --- */

		private $_docDefaults;
		private $_latentStyles;
		private $_style;

		
		/* --- Childrens --- */

		protected function getDocDefaults(){ return $this->_docDefaults->get(); }

		protected function getLatentStyles(){ return $this->_latentStyles->get(); }

		protected function getStyle(){ return $this->_style->get(); }
		public function createStyle(){ $new = new \Word\Objects\Style("w:style"); $this->style->AddRef($new); return $new; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\DocDefaults",
					"Objects\LatentStyles",
					"Objects\Style"
				)
			);
				
			parent::__construct($name);

			$this->_docDefaults = new \Word\PropertyObject('\Word\Objects\DocDefaults', "w:docDefaults", $this);
			$this->_latentStyles = new \Word\PropertyObject('\Word\Objects\LatentStyles', "w:latentStyles", $this);
			$this->_style = new \Word\PropertyArray($this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 

				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{

		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>