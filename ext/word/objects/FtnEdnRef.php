<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_FtnEdnRef.html 
	 * @property-read OnOff $customMarkFollows Suppress Footnote/Endnote Reference Mark
	 * @property-read DecimalNumber $id Footnote/Endnote ID Reference
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class FtnEdnRef extends Object
	{
		
		/* --- Attributes --- */

		private $_customMarkFollows;
		private $_id;

		
		/* --- Attributes --- */

		protected function getCustomMarkFollows(){ return $this->_customMarkFollows; }
		protected function setCustomMarkFollows($val){ $this->_customMarkFollows = $val; }

		protected function getId(){ return $this->_id; }
		protected function setId($val){ $this->_id = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\OnOff",
					"Consts\DecimalNumber"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->customMarkFollows === null && 
				$this->id === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->customMarkFollows !== null) $xml->writeAttribute("w:customMarkFollows", $this->customMarkFollows);
			if ($this->id !== null) $xml->writeAttribute("w:id", $this->id);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return
				$this->id !== null && 


				true;
		}
		
		
	}
	
}

?>