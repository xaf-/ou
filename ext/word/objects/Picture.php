<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_Picture.html 
	 * @property-read Rel $movie Embedded Video
	 * @property-read Control $control Floating Embedded Control
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class Picture extends Object
	{
		
		/* --- Childrens --- */

		private $_movie;
		private $_control;

		
		/* --- Childrens --- */

		protected function getMovie(){ return $this->_movie->get(); }

		protected function getControl(){ return $this->_control->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\Rel",
					"Objects\Control"
				)
			);
				
			parent::__construct($name);

			$this->_movie = new \Word\PropertyObject('\Word\Objects\Rel', "w:movie", $this);
			$this->_control = new \Word\PropertyObject('\Word\Objects\Control', "w:control", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 

				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{

		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>