<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_FFTextInput.html 
	 * @property-read FFTextType $type Text Box Form Field Type
	 * @property-read String $default Default Text Box Form Field String
	 * @property-read DecimalNumber $maxLength Text Box Form Field Maximum Length
	 * @property-read String $format Text Box Form Field Formatting
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class FFTextInput extends Object
	{
		
		/* --- Childrens --- */

		private $_type;
		private $_default;
		private $_maxLength;
		private $_format;

		
		/* --- Childrens --- */

		protected function getType(){ return $this->_type->get(); }

		protected function getDefault(){ return $this->_default->get(); }

		protected function getMaxLength(){ return $this->_maxLength->get(); }

		protected function getFormat(){ return $this->_format->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\FFTextType",
					"Objects\String",
					"Objects\DecimalNumber"
				)
			);
				
			parent::__construct($name);

			$this->_type = new \Word\PropertyObject('\Word\Objects\FFTextType', "w:type", $this);
			$this->_default = new \Word\PropertyObject('\Word\Objects\String', "w:default", $this);
			$this->_maxLength = new \Word\PropertyObject('\Word\Objects\DecimalNumber', "w:maxLength", $this);
			$this->_format = new \Word\PropertyObject('\Word\Objects\String', "w:format", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 

				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{

		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>