<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_PermStart.html 
	 * @property-read String $id Annotation ID
	 * @property-read DisplacedByCustomXml $displacedByCustomXml Annotation Displaced By Custom XML Markup
	 * @property-read EdGrp $edGrp Editor Group For Range Permission
	 * @property-read String $ed Single User For Range Permission
	 * @property-read DecimalNumber $colFirst First Table Column Covered By Range Permission
	 * @property-read DecimalNumber $colLast Last Table Column Covered By Range Permission
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class PermStart extends Object
	{
		
		/* --- Attributes --- */

		private $_id;
		private $_displacedByCustomXml;
		private $_edGrp;
		private $_ed;
		private $_colFirst;
		private $_colLast;

		
		/* --- Attributes --- */

		protected function getId(){ return $this->_id; }
		protected function setId($val){ $this->_id = $val; }

		protected function getDisplacedByCustomXml(){ return $this->_displacedByCustomXml; }
		protected function setDisplacedByCustomXml($val){ $this->_displacedByCustomXml = $val; }

		protected function getEdGrp(){ return $this->_edGrp; }
		protected function setEdGrp($val){ $this->_edGrp = $val; }

		protected function getEd(){ return $this->_ed; }
		protected function setEd($val){ $this->_ed = $val; }

		protected function getColFirst(){ return $this->_colFirst; }
		protected function setColFirst($val){ $this->_colFirst = $val; }

		protected function getColLast(){ return $this->_colLast; }
		protected function setColLast($val){ $this->_colLast = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\String",
					"Consts\DisplacedByCustomXml",
					"Consts\EdGrp",
					"Consts\DecimalNumber"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->id === null && 
				$this->displacedByCustomXml === null && 
				$this->edGrp === null && 
				$this->ed === null && 
				$this->colFirst === null && 
				$this->colLast === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->id !== null) $xml->writeAttribute("w:id", $this->id);
			if ($this->displacedByCustomXml !== null) $xml->writeAttribute("w:displacedByCustomXml", $this->displacedByCustomXml);
			if ($this->edGrp !== null) $xml->writeAttribute("w:edGrp", $this->edGrp);
			if ($this->ed !== null) $xml->writeAttribute("w:ed", $this->ed);
			if ($this->colFirst !== null) $xml->writeAttribute("w:colFirst", $this->colFirst);
			if ($this->colLast !== null) $xml->writeAttribute("w:colLast", $this->colLast);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return
				$this->id !== null && 
			/* --- Attributes --- */

			/* --- Attributes --- */

			/* --- Attributes --- */

			/* --- Attributes --- */

			/* --- Attributes --- */



				true;
		}
		
		
	}
	
}

?>