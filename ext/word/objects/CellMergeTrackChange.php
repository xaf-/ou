<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_CellMergeTrackChange.html 
	 * @property-read DecimalNumber $id Annotation Identifier
	 * @property-read String $author Annotation Author
	 * @property-read DateTime $date Annotation Date
	 * @property-read AnnotationVMerge $vMerge Revised Vertical Merge Setting
	 * @property-read AnnotationVMerge $vMergeOrig Vertical Merge Setting Removed by Revision
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class CellMergeTrackChange extends Object
	{
		
		/* --- Attributes --- */

		private $_id;
		private $_author;
		private $_date;
		private $_vMerge;
		private $_vMergeOrig;

		
		/* --- Attributes --- */

		protected function getId(){ return $this->_id; }
		protected function setId($val){ $this->_id = $val; }

		protected function getAuthor(){ return $this->_author; }
		protected function setAuthor($val){ $this->_author = $val; }

		protected function getDate(){ return $this->_date; }
		protected function setDate($val){ $this->_date = $val; }

		protected function getVMerge(){ return $this->_vMerge; }
		protected function setVMerge($val){ $this->_vMerge = $val; }

		protected function getVMergeOrig(){ return $this->_vMergeOrig; }
		protected function setVMergeOrig($val){ $this->_vMergeOrig = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\DecimalNumber",
					"Consts\String",
					"Consts\DateTime",
					"Consts\AnnotationVMerge"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->id === null && 
				$this->author === null && 
				$this->date === null && 
				$this->vMerge === null && 
				$this->vMergeOrig === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->id !== null) $xml->writeAttribute("w:id", $this->id);
			if ($this->author !== null) $xml->writeAttribute("w:author", $this->author);
			if ($this->date !== null) $xml->writeAttribute("w:date", $this->date);
			if ($this->vMerge !== null) $xml->writeAttribute("w:vMerge", $this->vMerge);
			if ($this->vMergeOrig !== null) $xml->writeAttribute("w:vMergeOrig", $this->vMergeOrig);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return
				$this->id !== null && 
			/* --- Attributes --- */

				$this->author !== null && 
			/* --- Attributes --- */

			/* --- Attributes --- */

			/* --- Attributes --- */



				true;
		}
		
		
	}
	
}

?>