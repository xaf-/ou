<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_Control.html 
	 * @property-read String $name Unique Name for Embedded Control
	 * @property-read String $shapeid Associated VML Data Reference
	 * @property-read RelationshipId $id Embedded Control Properties Relationship Reference
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class Control extends Object
	{
		
		/* --- Attributes --- */

		private $_name;
		private $_shapeid;
		private $_id;

		
		/* --- Attributes --- */

		protected function getName(){ return $this->_name; }
		protected function setName($val){ $this->_name = $val; }

		protected function getShapeid(){ return $this->_shapeid; }
		protected function setShapeid($val){ $this->_shapeid = $val; }

		protected function getId(){ return $this->_id; }
		protected function setId($val){ $this->_id = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\String",
					"Consts\RelationshipId"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->name === null && 
				$this->shapeid === null && 
				$this->id === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->name !== null) $xml->writeAttribute("w:name", $this->name);
			if ($this->shapeid !== null) $xml->writeAttribute("w:shapeid", $this->shapeid);
			if ($this->id !== null) $xml->writeAttribute("r:id", $this->id);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>