<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_NumPr.html 
	 * @property-read DecimalNumber $ilvl Numbering Level Reference
	 * @property-read DecimalNumber $numId Numbering Definition Instance Reference
	 * @property-read TrackChangeNumbering $numberingChange Previous Paragraph Numbering Properties
	 * @property-read TrackChange $ins Inserted Numbering Properties
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class NumPr extends Object
	{
		
		/* --- Childrens --- */

		private $_ilvl;
		private $_numId;
		private $_numberingChange;
		private $_ins;

		
		/* --- Childrens --- */

		protected function getIlvl(){ return $this->_ilvl->get(); }

		protected function getNumId(){ return $this->_numId->get(); }

		protected function getNumberingChange(){ return $this->_numberingChange->get(); }

		protected function getIns(){ return $this->_ins->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\DecimalNumber",
					"Objects\TrackChangeNumbering",
					"Objects\TrackChange"
				)
			);
				
			parent::__construct($name);

			$this->_ilvl = new \Word\PropertyObject('\Word\Objects\DecimalNumber', "w:ilvl", $this);
			$this->_numId = new \Word\PropertyObject('\Word\Objects\DecimalNumber', "w:numId", $this);
			$this->_numberingChange = new \Word\PropertyObject('\Word\Objects\TrackChangeNumbering', "w:numberingChange", $this);
			$this->_ins = new \Word\PropertyObject('\Word\Objects\TrackChange', "w:ins", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 

				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{

		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>