<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_FramePr.html 
	 * @property-read DropCap $dropCap Drop Cap Frame
	 * @property-read DecimalNumber $lines Drop Cap Vertical Height in Lines
	 * @property-read TwipsMeasure $w Frame Width
	 * @property-read TwipsMeasure $h Frame Height
	 * @property-read TwipsMeasure $vSpace Vertical Frame Padding
	 * @property-read TwipsMeasure $hSpace Horizontal Frame Padding
	 * @property-read Wrap $wrap Text Wrapping Around Frame
	 * @property-read HAnchor $hAnchor Frame Horizontal Positioning Base
	 * @property-read VAnchor $vAnchor Frame Vertical Positioning Base
	 * @property-read SignedTwipsMeasure $x Absolute Horizontal Position
	 * @property-read XAlign $xAlign Relative Horizontal Position
	 * @property-read SignedTwipsMeasure $y Absolute Vertical Position
	 * @property-read YAlign $yAlign Relative Vertical Position
	 * @property-read HeightRule $hRule Frame Height Type
	 * @property-read OnOff $anchorLock Lock Frame Anchor to Paragraph
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class FramePr extends Object
	{
		
		/* --- Attributes --- */

		private $_dropCap;
		private $_lines;
		private $_w;
		private $_h;
		private $_vSpace;
		private $_hSpace;
		private $_wrap;
		private $_hAnchor;
		private $_vAnchor;
		private $_x;
		private $_xAlign;
		private $_y;
		private $_yAlign;
		private $_hRule;
		private $_anchorLock;

		
		/* --- Attributes --- */

		protected function getDropCap(){ return $this->_dropCap; }
		protected function setDropCap($val){ $this->_dropCap = $val; }

		protected function getLines(){ return $this->_lines; }
		protected function setLines($val){ $this->_lines = $val; }

		protected function getW(){ return $this->_w; }
		protected function setW($val){ $this->_w = $val; }

		protected function getH(){ return $this->_h; }
		protected function setH($val){ $this->_h = $val; }

		protected function getVSpace(){ return $this->_vSpace; }
		protected function setVSpace($val){ $this->_vSpace = $val; }

		protected function getHSpace(){ return $this->_hSpace; }
		protected function setHSpace($val){ $this->_hSpace = $val; }

		protected function getWrap(){ return $this->_wrap; }
		protected function setWrap($val){ $this->_wrap = $val; }

		protected function getHAnchor(){ return $this->_hAnchor; }
		protected function setHAnchor($val){ $this->_hAnchor = $val; }

		protected function getVAnchor(){ return $this->_vAnchor; }
		protected function setVAnchor($val){ $this->_vAnchor = $val; }

		protected function getX(){ return $this->_x; }
		protected function setX($val){ $this->_x = $val; }

		protected function getXAlign(){ return $this->_xAlign; }
		protected function setXAlign($val){ $this->_xAlign = $val; }

		protected function getY(){ return $this->_y; }
		protected function setY($val){ $this->_y = $val; }

		protected function getYAlign(){ return $this->_yAlign; }
		protected function setYAlign($val){ $this->_yAlign = $val; }

		protected function getHRule(){ return $this->_hRule; }
		protected function setHRule($val){ $this->_hRule = $val; }

		protected function getAnchorLock(){ return $this->_anchorLock; }
		protected function setAnchorLock($val){ $this->_anchorLock = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\DropCap",
					"Consts\DecimalNumber",
					"Consts\TwipsMeasure",
					"Consts\Wrap",
					"Consts\HAnchor",
					"Consts\VAnchor",
					"Consts\SignedTwipsMeasure",
					"Consts\XAlign",
					"Consts\YAlign",
					"Consts\HeightRule",
					"Consts\OnOff"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->dropCap === null && 
				$this->lines === null && 
				$this->w === null && 
				$this->h === null && 
				$this->vSpace === null && 
				$this->hSpace === null && 
				$this->wrap === null && 
				$this->hAnchor === null && 
				$this->vAnchor === null && 
				$this->x === null && 
				$this->xAlign === null && 
				$this->y === null && 
				$this->yAlign === null && 
				$this->hRule === null && 
				$this->anchorLock === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->dropCap !== null) $xml->writeAttribute("w:dropCap", $this->dropCap);
			if ($this->lines !== null) $xml->writeAttribute("w:lines", $this->lines);
			if ($this->w !== null) $xml->writeAttribute("w:w", $this->w);
			if ($this->h !== null) $xml->writeAttribute("w:h", $this->h);
			if ($this->vSpace !== null) $xml->writeAttribute("w:vSpace", $this->vSpace);
			if ($this->hSpace !== null) $xml->writeAttribute("w:hSpace", $this->hSpace);
			if ($this->wrap !== null) $xml->writeAttribute("w:wrap", $this->wrap);
			if ($this->hAnchor !== null) $xml->writeAttribute("w:hAnchor", $this->hAnchor);
			if ($this->vAnchor !== null) $xml->writeAttribute("w:vAnchor", $this->vAnchor);
			if ($this->x !== null) $xml->writeAttribute("w:x", $this->x);
			if ($this->xAlign !== null) $xml->writeAttribute("w:xAlign", $this->xAlign);
			if ($this->y !== null) $xml->writeAttribute("w:y", $this->y);
			if ($this->yAlign !== null) $xml->writeAttribute("w:yAlign", $this->yAlign);
			if ($this->hRule !== null) $xml->writeAttribute("w:hRule", $this->hRule);
			if ($this->anchorLock !== null) $xml->writeAttribute("w:anchorLock", $this->anchorLock);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>