<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_PaperSource.html 
	 * @property-read DecimalNumber $first First Page Printer Tray Code
	 * @property-read DecimalNumber $other Non-First Page Printer Tray Code
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class PaperSource extends Object
	{
		
		/* --- Attributes --- */

		private $_first;
		private $_other;

		
		/* --- Attributes --- */

		protected function getFirst(){ return $this->_first; }
		protected function setFirst($val){ $this->_first = $val; }

		protected function getOther(){ return $this->_other; }
		protected function setOther($val){ $this->_other = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\DecimalNumber"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->first === null && 
				$this->other === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->first !== null) $xml->writeAttribute("w:first", $this->first);
			if ($this->other !== null) $xml->writeAttribute("w:other", $this->other);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>