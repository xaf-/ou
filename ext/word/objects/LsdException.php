<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_LsdException.html 
	 * @property-read String $name Primary Style Name
	 * @property-read OnOff $locked Latent Style Locking Setting
	 * @property-read DecimalNumber $uiPriority Override default sorting order
	 * @property-read OnOff $semiHidden Semi hidden text override
	 * @property-read OnOff $unhideWhenUsed Unhide when used
	 * @property-read OnOff $qFormat Latent Style Primary Style Setting
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class LsdException extends Object
	{
		
		/* --- Attributes --- */

		private $_name;
		private $_locked;
		private $_uiPriority;
		private $_semiHidden;
		private $_unhideWhenUsed;
		private $_qFormat;

		
		/* --- Attributes --- */

		protected function getName(){ return $this->_name; }
		protected function setName($val){ $this->_name = $val; }

		protected function getLocked(){ return $this->_locked; }
		protected function setLocked($val){ $this->_locked = $val; }

		protected function getUiPriority(){ return $this->_uiPriority; }
		protected function setUiPriority($val){ $this->_uiPriority = $val; }

		protected function getSemiHidden(){ return $this->_semiHidden; }
		protected function setSemiHidden($val){ $this->_semiHidden = $val; }

		protected function getUnhideWhenUsed(){ return $this->_unhideWhenUsed; }
		protected function setUnhideWhenUsed($val){ $this->_unhideWhenUsed = $val; }

		protected function getQFormat(){ return $this->_qFormat; }
		protected function setQFormat($val){ $this->_qFormat = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\String",
					"Consts\OnOff",
					"Consts\DecimalNumber"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->name === null && 
				$this->locked === null && 
				$this->uiPriority === null && 
				$this->semiHidden === null && 
				$this->unhideWhenUsed === null && 
				$this->qFormat === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->name !== null) $xml->writeAttribute("w:name", $this->name);
			if ($this->locked !== null) $xml->writeAttribute("w:locked", $this->locked);
			if ($this->uiPriority !== null) $xml->writeAttribute("w:uiPriority", $this->uiPriority);
			if ($this->semiHidden !== null) $xml->writeAttribute("w:semiHidden", $this->semiHidden);
			if ($this->unhideWhenUsed !== null) $xml->writeAttribute("w:unhideWhenUsed", $this->unhideWhenUsed);
			if ($this->qFormat !== null) $xml->writeAttribute("w:qFormat", $this->qFormat);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return
				$this->name !== null && 
			/* --- Attributes --- */

			/* --- Attributes --- */

			/* --- Attributes --- */

			/* --- Attributes --- */

			/* --- Attributes --- */



				true;
		}
		
		
	}
	
}

?>