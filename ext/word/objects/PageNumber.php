<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_PageNumber.html 
	 * @property-read NumberFormat $fmt Page Number Format
	 * @property-read DecimalNumber $start Starting Page Number
	 * @property-read DecimalNumber $chapStyle Chapter Heading Style
	 * @property-read ChapterSep $chapSep Chapter Separator Character
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class PageNumber extends Object
	{
		
		/* --- Attributes --- */

		private $_fmt;
		private $_start;
		private $_chapStyle;
		private $_chapSep;

		
		/* --- Attributes --- */

		protected function getFmt(){ return $this->_fmt; }
		protected function setFmt($val){ $this->_fmt = $val; }

		protected function getStart(){ return $this->_start; }
		protected function setStart($val){ $this->_start = $val; }

		protected function getChapStyle(){ return $this->_chapStyle; }
		protected function setChapStyle($val){ $this->_chapStyle = $val; }

		protected function getChapSep(){ return $this->_chapSep; }
		protected function setChapSep($val){ $this->_chapSep = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\NumberFormat",
					"Consts\DecimalNumber",
					"Consts\ChapterSep"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->fmt === null && 
				$this->start === null && 
				$this->chapStyle === null && 
				$this->chapSep === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->fmt !== null) $xml->writeAttribute("w:fmt", $this->fmt);
			if ($this->start !== null) $xml->writeAttribute("w:start", $this->start);
			if ($this->chapStyle !== null) $xml->writeAttribute("w:chapStyle", $this->chapStyle);
			if ($this->chapSep !== null) $xml->writeAttribute("w:chapSep", $this->chapSep);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>