<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_SdtDropDownList.html 
	 * @property-read SdtListItem[] $listItem Drop-Down List Item
	 * @property-read String $lastValue Drop-down List Last Saved Value
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class SdtDropDownList extends Object
	{
		
		/* --- Attributes --- */

		private $_lastValue;

		/* --- Childrens --- */

		private $_listItem;

		
		/* --- Attributes --- */

		protected function getLastValue(){ return $this->_lastValue; }
		protected function setLastValue($val){ $this->_lastValue = $val; }

		/* --- Childrens --- */

		protected function getListItem(){ return $this->_listItem->get(); }
		public function createListItem(){ $new = new \Word\Objects\SdtListItem("w:listItem"); $this->listItem->AddRef($new); return $new; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\SdtListItem",
					"Consts\String"
				)
			);
				
			parent::__construct($name);

			$this->_listItem = new \Word\PropertyArray($this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->lastValue === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->lastValue !== null) $xml->writeAttribute("w:lastValue", $this->lastValue);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>