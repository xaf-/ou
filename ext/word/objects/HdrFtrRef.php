<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_HdrFtrRef.html 
	 * @property-read RelationshipId $id Relationship to Part
	 * @property-read HdrFtr $type Header or Footer Type
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class HdrFtrRef extends Object
	{
		
		/* --- Attributes --- */

		private $_id;
		private $_type;

		
		/* --- Attributes --- */

		protected function getId(){ return $this->_id; }
		protected function setId($val){ $this->_id = $val; }

		protected function getType(){ return $this->_type; }
		protected function setType($val){ $this->_type = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\RelationshipId",
					"Consts\HdrFtr"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->id === null && 
				$this->type === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->id !== null) $xml->writeAttribute("r:id", $this->id);
			if ($this->type !== null) $xml->writeAttribute("w:type", $this->type);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return
				$this->id !== null && 
			/* --- Attributes --- */

				$this->type !== null && 


				true;
		}
		
		
	}
	
}

?>