<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_SectPr.html 
	 * @property-read HdrFtrRef $headerReference Header Reference
	 * @property-read HdrFtrRef $footerReference Footer Reference
	 * @property-read FtnProps $footnotePr Section-Wide Footnote Properties
	 * @property-read EdnProps $endnotePr Section-Wide Endnote Properties
	 * @property-read SectType $type Section Type
	 * @property-read PageSz $pgSz Page Size
	 * @property-read PageMar $pgMar Page Margins
	 * @property-read PaperSource $paperSrc Paper Source Information
	 * @property-read PageBorders $pgBorders Page Borders
	 * @property-read LineNumber $lnNumType Line Numbering Settings
	 * @property-read PageNumber $pgNumType Page Numbering Settings
	 * @property-read Columns $cols Column Definitions
	 * @property-read OnOff $formProt Only Allow Editing of Form Fields
	 * @property-read VerticalJc $vAlign Vertical Text Alignment on Page
	 * @property-read OnOff $noEndnote Suppress Endnotes In Document
	 * @property-read OnOff $titlePg Different First Page Headers and Footers
	 * @property-read TextDirection $textDirection Text Flow Direction
	 * @property-read OnOff $bidi Right to Left Section Layout
	 * @property-read OnOff $rtlGutter Gutter on Right Side of Page
	 * @property-read DocGrid $docGrid Document Grid
	 * @property-read Rel $printerSettings Reference to Printer Settings Data
	 * @property-read SectPrChange $sectPrChange Revision Information for Section Properties
	 * @property-read LongHexNumber $rsidRPr Physical Section Mark Character Revision ID
	 * @property-read LongHexNumber $rsidDel Section Deletion Revision ID
	 * @property-read LongHexNumber $rsidR Section Addition Revision ID
	 * @property-read LongHexNumber $rsidSect Section Properties Revision ID
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class SectPr extends Object
	{
		
		/* --- Attributes --- */

		private $_rsidRPr;
		private $_rsidDel;
		private $_rsidR;
		private $_rsidSect;

		/* --- Childrens --- */

		private $_headerReference;
		private $_footerReference;
		private $_footnotePr;
		private $_endnotePr;
		private $_type;
		private $_pgSz;
		private $_pgMar;
		private $_paperSrc;
		private $_pgBorders;
		private $_lnNumType;
		private $_pgNumType;
		private $_cols;
		private $_formProt;
		private $_vAlign;
		private $_noEndnote;
		private $_titlePg;
		private $_textDirection;
		private $_bidi;
		private $_rtlGutter;
		private $_docGrid;
		private $_printerSettings;
		private $_sectPrChange;

		
		/* --- Attributes --- */

		protected function getRsidRPr(){ return $this->_rsidRPr; }
		protected function setRsidRPr($val){ $this->_rsidRPr = $val; }

		protected function getRsidDel(){ return $this->_rsidDel; }
		protected function setRsidDel($val){ $this->_rsidDel = $val; }

		protected function getRsidR(){ return $this->_rsidR; }
		protected function setRsidR($val){ $this->_rsidR = $val; }

		protected function getRsidSect(){ return $this->_rsidSect; }
		protected function setRsidSect($val){ $this->_rsidSect = $val; }

		/* --- Childrens --- */

		protected function getHeaderReference(){ return $this->_headerReference->get(); }

		protected function getFooterReference(){ return $this->_footerReference->get(); }

		protected function getFootnotePr(){ return $this->_footnotePr->get(); }

		protected function getEndnotePr(){ return $this->_endnotePr->get(); }

		protected function getType(){ return $this->_type->get(); }

		protected function getPgSz(){ return $this->_pgSz->get(); }

		protected function getPgMar(){ return $this->_pgMar->get(); }

		protected function getPaperSrc(){ return $this->_paperSrc->get(); }

		protected function getPgBorders(){ return $this->_pgBorders->get(); }

		protected function getLnNumType(){ return $this->_lnNumType->get(); }

		protected function getPgNumType(){ return $this->_pgNumType->get(); }

		protected function getCols(){ return $this->_cols->get(); }

		protected function getFormProt(){ return $this->_formProt->get(); }

		protected function getVAlign(){ return $this->_vAlign->get(); }

		protected function getNoEndnote(){ return $this->_noEndnote->get(); }

		protected function getTitlePg(){ return $this->_titlePg->get(); }

		protected function getTextDirection(){ return $this->_textDirection->get(); }

		protected function getBidi(){ return $this->_bidi->get(); }

		protected function getRtlGutter(){ return $this->_rtlGutter->get(); }

		protected function getDocGrid(){ return $this->_docGrid->get(); }

		protected function getPrinterSettings(){ return $this->_printerSettings->get(); }

		protected function getSectPrChange(){ return $this->_sectPrChange->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\HdrFtrRef",
					"Objects\FtnProps",
					"Objects\EdnProps",
					"Objects\SectType",
					"Objects\PageSz",
					"Objects\PageMar",
					"Objects\PaperSource",
					"Objects\PageBorders",
					"Objects\LineNumber",
					"Objects\PageNumber",
					"Objects\Columns",
					"Objects\OnOff",
					"Objects\VerticalJc",
					"Objects\TextDirection",
					"Objects\DocGrid",
					"Objects\Rel",
					"Objects\SectPrChange",
					"Consts\LongHexNumber"
				)
			);
				
			parent::__construct($name);

			$this->_headerReference = new \Word\PropertyObject('\Word\Objects\HdrFtrRef', "w:headerReference", $this);
			$this->_footerReference = new \Word\PropertyObject('\Word\Objects\HdrFtrRef', "w:footerReference", $this);
			$this->_footnotePr = new \Word\PropertyObject('\Word\Objects\FtnProps', "w:footnotePr", $this);
			$this->_endnotePr = new \Word\PropertyObject('\Word\Objects\EdnProps', "w:endnotePr", $this);
			$this->_type = new \Word\PropertyObject('\Word\Objects\SectType', "w:type", $this);
			$this->_pgSz = new \Word\PropertyObject('\Word\Objects\PageSz', "w:pgSz", $this);
			$this->_pgMar = new \Word\PropertyObject('\Word\Objects\PageMar', "w:pgMar", $this);
			$this->_paperSrc = new \Word\PropertyObject('\Word\Objects\PaperSource', "w:paperSrc", $this);
			$this->_pgBorders = new \Word\PropertyObject('\Word\Objects\PageBorders', "w:pgBorders", $this);
			$this->_lnNumType = new \Word\PropertyObject('\Word\Objects\LineNumber', "w:lnNumType", $this);
			$this->_pgNumType = new \Word\PropertyObject('\Word\Objects\PageNumber', "w:pgNumType", $this);
			$this->_cols = new \Word\PropertyObject('\Word\Objects\Columns', "w:cols", $this);
			$this->_formProt = new \Word\PropertyObject('\Word\Objects\OnOff', "w:formProt", $this);
			$this->_vAlign = new \Word\PropertyObject('\Word\Objects\VerticalJc', "w:vAlign", $this);
			$this->_noEndnote = new \Word\PropertyObject('\Word\Objects\OnOff', "w:noEndnote", $this);
			$this->_titlePg = new \Word\PropertyObject('\Word\Objects\OnOff', "w:titlePg", $this);
			$this->_textDirection = new \Word\PropertyObject('\Word\Objects\TextDirection', "w:textDirection", $this);
			$this->_bidi = new \Word\PropertyObject('\Word\Objects\OnOff', "w:bidi", $this);
			$this->_rtlGutter = new \Word\PropertyObject('\Word\Objects\OnOff', "w:rtlGutter", $this);
			$this->_docGrid = new \Word\PropertyObject('\Word\Objects\DocGrid', "w:docGrid", $this);
			$this->_printerSettings = new \Word\PropertyObject('\Word\Objects\Rel', "w:printerSettings", $this);
			$this->_sectPrChange = new \Word\PropertyObject('\Word\Objects\SectPrChange', "w:sectPrChange", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->rsidRPr === null && 
				$this->rsidDel === null && 
				$this->rsidR === null && 
				$this->rsidSect === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->rsidRPr !== null) $xml->writeAttribute("w:rsidRPr", $this->rsidRPr);
			if ($this->rsidDel !== null) $xml->writeAttribute("w:rsidDel", $this->rsidDel);
			if ($this->rsidR !== null) $xml->writeAttribute("w:rsidR", $this->rsidR);
			if ($this->rsidSect !== null) $xml->writeAttribute("w:rsidSect", $this->rsidSect);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>