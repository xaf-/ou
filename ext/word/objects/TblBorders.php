<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_TblBorders.html 
	 * @property-read Border $top Table Top Border
	 * @property-read Border $left Table Left Border
	 * @property-read Border $bottom Table Bottom Border
	 * @property-read Border $right Table Right Border
	 * @property-read Border $insideH Table Inside Horizontal Edges Border
	 * @property-read Border $insideV Table Inside Vertical Edges Border
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class TblBorders extends Object
	{
		
		/* --- Childrens --- */

		private $_top;
		private $_left;
		private $_bottom;
		private $_right;
		private $_insideH;
		private $_insideV;

		
		/* --- Childrens --- */

		protected function getTop(){ return $this->_top->get(); }

		protected function getLeft(){ return $this->_left->get(); }

		protected function getBottom(){ return $this->_bottom->get(); }

		protected function getRight(){ return $this->_right->get(); }

		protected function getInsideH(){ return $this->_insideH->get(); }

		protected function getInsideV(){ return $this->_insideV->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\Border"
				)
			);
				
			parent::__construct($name);

			$this->_top = new \Word\PropertyObject('\Word\Objects\Border', "w:top", $this);
			$this->_left = new \Word\PropertyObject('\Word\Objects\Border', "w:left", $this);
			$this->_bottom = new \Word\PropertyObject('\Word\Objects\Border', "w:bottom", $this);
			$this->_right = new \Word\PropertyObject('\Word\Objects\Border', "w:right", $this);
			$this->_insideH = new \Word\PropertyObject('\Word\Objects\Border', "w:insideH", $this);
			$this->_insideV = new \Word\PropertyObject('\Word\Objects\Border', "w:insideV", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 

				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{

		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>