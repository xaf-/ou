<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_LatentStyles.html 
	 * @property-read LsdException[] $lsdException Latent Style Exception
	 * @property-read OnOff $defLockedState Default Style Locking Setting
	 * @property-read DecimalNumber $defUIPriority Default User Interface Priority Setting
	 * @property-read OnOff $defSemiHidden Default Semi-Hidden Setting
	 * @property-read OnOff $defUnhideWhenUsed Default Hidden Until Used Setting
	 * @property-read OnOff $defQFormat Default Primary Style Setting
	 * @property-read DecimalNumber $count Latent Style Count
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class LatentStyles extends Object
	{
		
		/* --- Attributes --- */

		private $_defLockedState;
		private $_defUIPriority;
		private $_defSemiHidden;
		private $_defUnhideWhenUsed;
		private $_defQFormat;
		private $_count;

		/* --- Childrens --- */

		private $_lsdException;

		
		/* --- Attributes --- */

		protected function getDefLockedState(){ return $this->_defLockedState; }
		protected function setDefLockedState($val){ $this->_defLockedState = $val; }

		protected function getDefUIPriority(){ return $this->_defUIPriority; }
		protected function setDefUIPriority($val){ $this->_defUIPriority = $val; }

		protected function getDefSemiHidden(){ return $this->_defSemiHidden; }
		protected function setDefSemiHidden($val){ $this->_defSemiHidden = $val; }

		protected function getDefUnhideWhenUsed(){ return $this->_defUnhideWhenUsed; }
		protected function setDefUnhideWhenUsed($val){ $this->_defUnhideWhenUsed = $val; }

		protected function getDefQFormat(){ return $this->_defQFormat; }
		protected function setDefQFormat($val){ $this->_defQFormat = $val; }

		protected function getCount(){ return $this->_count; }
		protected function setCount($val){ $this->_count = $val; }

		/* --- Childrens --- */

		protected function getLsdException(){ return $this->_lsdException->get(); }
		public function createLsdException(){ $new = new \Word\Objects\LsdException("w:lsdException"); $this->lsdException->AddRef($new); return $new; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\LsdException",
					"Consts\OnOff",
					"Consts\DecimalNumber"
				)
			);
				
			parent::__construct($name);

			$this->_lsdException = new \Word\PropertyArray($this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->defLockedState === null && 
				$this->defUIPriority === null && 
				$this->defSemiHidden === null && 
				$this->defUnhideWhenUsed === null && 
				$this->defQFormat === null && 
				$this->count === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->defLockedState !== null) $xml->writeAttribute("w:defLockedState", $this->defLockedState);
			if ($this->defUIPriority !== null) $xml->writeAttribute("w:defUIPriority", $this->defUIPriority);
			if ($this->defSemiHidden !== null) $xml->writeAttribute("w:defSemiHidden", $this->defSemiHidden);
			if ($this->defUnhideWhenUsed !== null) $xml->writeAttribute("w:defUnhideWhenUsed", $this->defUnhideWhenUsed);
			if ($this->defQFormat !== null) $xml->writeAttribute("w:defQFormat", $this->defQFormat);
			if ($this->count !== null) $xml->writeAttribute("w:count", $this->count);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>