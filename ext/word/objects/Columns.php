<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_Columns.html 
	 * @property-read Column $col [1..45]    Single Column Definition
	 * @property-read OnOff $equalWidth Equal Column Widths
	 * @property-read TwipsMeasure $space Spacing Between Equal Width Columns
	 * @property-read DecimalNumber $num Number of Equal Width Columns
	 * @property-read OnOff $sep Draw Line Between Columns
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class Columns extends Object
	{
		
		/* --- Attributes --- */

		private $_equalWidth;
		private $_space;
		private $_num;
		private $_sep;

		/* --- Childrens --- */

		private $_col;

		
		/* --- Attributes --- */

		protected function getEqualWidth(){ return $this->_equalWidth; }
		protected function setEqualWidth($val){ $this->_equalWidth = $val; }

		protected function getSpace(){ return $this->_space; }
		protected function setSpace($val){ $this->_space = $val; }

		protected function getNum(){ return $this->_num; }
		protected function setNum($val){ $this->_num = $val; }

		protected function getSep(){ return $this->_sep; }
		protected function setSep($val){ $this->_sep = $val; }

		/* --- Childrens --- */

		protected function getCol(){ return $this->_col->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\Column",
					"Consts\OnOff",
					"Consts\TwipsMeasure",
					"Consts\DecimalNumber"
				)
			);
				
			parent::__construct($name);

			$this->_col = new \Word\PropertyObject('\Word\Objects\Column', "w:col", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->equalWidth === null && 
				$this->space === null && 
				$this->num === null && 
				$this->sep === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->equalWidth !== null) $xml->writeAttribute("w:equalWidth", $this->equalWidth);
			if ($this->space !== null) $xml->writeAttribute("w:space", $this->space);
			if ($this->num !== null) $xml->writeAttribute("w:num", $this->num);
			if ($this->sep !== null) $xml->writeAttribute("w:sep", $this->sep);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>