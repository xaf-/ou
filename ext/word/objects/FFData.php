<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_FFData.html 
	 * @property-read FFName[] $name Form Field Name
	 * @property-read OnOff[] $enabled Form Field Enabled
	 * @property-read OnOff[] $calcOnExit Recalculate Fields When Current Field Is Modified
	 * @property-read MacroName $entryMacro Script Function to Execute on Form Field Entry
	 * @property-read MacroName $exitMacro Script Function to Execute on Form Field Exit
	 * @property-read FFHelpText $helpText Associated Help Text
	 * @property-read FFStatusText $statusText Associated Status Text
	 * @property-read FFCheckBox[] $checkBox Checkbox Form Field Properties
	 * @property-read FFDDList[] $ddList Drop-Down List Form Field Properties
	 * @property-read FFTextInput[] $textInput Text Box Form Field Properties
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class FFData extends Object
	{
		
		/* --- Childrens --- */

		private $_name;
		private $_enabled;
		private $_calcOnExit;
		private $_entryMacro;
		private $_exitMacro;
		private $_helpText;
		private $_statusText;
		private $_checkBox;
		private $_ddList;
		private $_textInput;

		
		/* --- Childrens --- */

		protected function getName(){ return $this->_name->get(); }
		public function createName(){ $new = new \Word\Objects\FFName("w:name"); $this->name->AddRef($new); return $new; }

		protected function getEnabled(){ return $this->_enabled->get(); }
		public function createEnabled(){ $new = new \Word\Objects\OnOff("w:enabled"); $this->enabled->AddRef($new); return $new; }

		protected function getCalcOnExit(){ return $this->_calcOnExit->get(); }
		public function createCalcOnExit(){ $new = new \Word\Objects\OnOff("w:calcOnExit"); $this->calcOnExit->AddRef($new); return $new; }

		protected function getEntryMacro(){ return $this->_entryMacro->get(); }

		protected function getExitMacro(){ return $this->_exitMacro->get(); }

		protected function getHelpText(){ return $this->_helpText->get(); }

		protected function getStatusText(){ return $this->_statusText->get(); }

		protected function getCheckBox(){ return $this->_checkBox->get(); }
		public function createCheckBox(){ $new = new \Word\Objects\FFCheckBox("w:checkBox"); $this->checkBox->AddRef($new); return $new; }

		protected function getDdList(){ return $this->_ddList->get(); }
		public function createDdList(){ $new = new \Word\Objects\FFDDList("w:ddList"); $this->ddList->AddRef($new); return $new; }

		protected function getTextInput(){ return $this->_textInput->get(); }
		public function createTextInput(){ $new = new \Word\Objects\FFTextInput("w:textInput"); $this->textInput->AddRef($new); return $new; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\FFName",
					"Objects\OnOff",
					"Objects\MacroName",
					"Objects\FFHelpText",
					"Objects\FFStatusText",
					"Objects\FFCheckBox",
					"Objects\FFDDList",
					"Objects\FFTextInput"
				)
			);
				
			parent::__construct($name);

			$this->_name = new \Word\PropertyArray($this);
			$this->_enabled = new \Word\PropertyArray($this);
			$this->_calcOnExit = new \Word\PropertyArray($this);
			$this->_entryMacro = new \Word\PropertyObject('\Word\Objects\MacroName', "w:entryMacro", $this);
			$this->_exitMacro = new \Word\PropertyObject('\Word\Objects\MacroName', "w:exitMacro", $this);
			$this->_helpText = new \Word\PropertyObject('\Word\Objects\FFHelpText', "w:helpText", $this);
			$this->_statusText = new \Word\PropertyObject('\Word\Objects\FFStatusText', "w:statusText", $this);
			$this->_checkBox = new \Word\PropertyArray($this);
			$this->_ddList = new \Word\PropertyArray($this);
			$this->_textInput = new \Word\PropertyArray($this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 

				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{

		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>