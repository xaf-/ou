<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_Br.html 
	 * @property-read BrType $type Break Type
	 * @property-read BrClear $clear Restart Location For Text Wrapping Break
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class Br extends Object
	{
		
		/* --- Attributes --- */

		private $_type;
		private $_clear;

		
		/* --- Attributes --- */

		protected function getType(){ return $this->_type; }
		protected function setType($val){ $this->_type = $val; }

		protected function getClear(){ return $this->_clear; }
		protected function setClear($val){ $this->_clear = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\BrType",
					"Consts\BrClear"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->type === null && 
				$this->clear === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->type !== null) $xml->writeAttribute("w:type", $this->type);
			if ($this->clear !== null) $xml->writeAttribute("w:clear", $this->clear);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>