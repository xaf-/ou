<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_SdtContentRun.html 
	 * @property-read SmartTagRun[] $smartTag Inline-Level Smart Tag
	 * @property-read SdtRun[] $sdt Inline-Level Structured Document Tag
	 * @property-read R[] $r Text Run
	 * @property-read ProofErr $proofErr Proofing Error Anchor
	 * @property-read PermStart $permStart Range Permission Start
	 * @property-read Perm $permEnd Range Permission End
	 * @property-read Bookmark[] $bookmarkStart Bookmark Start
	 * @property-read MarkupRange[] $bookmarkEnd Bookmark End
	 * @property-read MoveBookmark[] $moveFromRangeStart Move Source Location Container - Start
	 * @property-read MarkupRange[] $moveFromRangeEnd Move Source Location Container - End
	 * @property-read MoveBookmark[] $moveToRangeStart Move Destination Location Container - Start
	 * @property-read MarkupRange[] $moveToRangeEnd Move Destination Location Container - End
	 * @property-read MarkupRange[] $commentRangeStart Comment Anchor Range Start
	 * @property-read MarkupRange[] $commentRangeEnd Comment Anchor Range End
	 * @property-read TrackChange[] $customXmlInsRangeStart Custom XML Markup Insertion Start
	 * @property-read Markup[] $customXmlInsRangeEnd Custom XML Markup Insertion End
	 * @property-read TrackChange[] $customXmlDelRangeStart Custom XML Markup Deletion Start
	 * @property-read Markup[] $customXmlDelRangeEnd Custom XML Markup Deletion End
	 * @property-read TrackChange[] $customXmlMoveFromRangeStart Custom XML Markup Move Source Start
	 * @property-read Markup[] $customXmlMoveFromRangeEnd Custom XML Markup Move Source End
	 * @property-read TrackChange[] $customXmlMoveToRangeStart Custom XML Markup Move Destination Location Start
	 * @property-read Markup[] $customXmlMoveToRangeEnd Custom XML Markup Move Destination Location End
	 * @property-read RunTrackChange $ins Inserted Run Content
	 * @property-read RunTrackChange $del Deleted Run Content
	 * @property-read RunTrackChange[] $moveFrom Move Source Run Content
	 * @property-read RunTrackChange[] $moveTo Move Destination Run Content
	 * @property-read OMath[] $oMath 
	 * @property-read SimpleField[] $fldSimple Simple Field
	 * @property-read Hyperlink[] $hyperlink Hyperlink
	 * @property-read Rel[] $subDoc Anchor for Subdocument Location
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class SdtContentRun extends Object
	{
		
		/* --- Childrens --- */

		private $_smartTag;
		private $_sdt;
		private $_r;
		private $_proofErr;
		private $_permStart;
		private $_permEnd;
		private $_bookmarkStart;
		private $_bookmarkEnd;
		private $_moveFromRangeStart;
		private $_moveFromRangeEnd;
		private $_moveToRangeStart;
		private $_moveToRangeEnd;
		private $_commentRangeStart;
		private $_commentRangeEnd;
		private $_customXmlInsRangeStart;
		private $_customXmlInsRangeEnd;
		private $_customXmlDelRangeStart;
		private $_customXmlDelRangeEnd;
		private $_customXmlMoveFromRangeStart;
		private $_customXmlMoveFromRangeEnd;
		private $_customXmlMoveToRangeStart;
		private $_customXmlMoveToRangeEnd;
		private $_ins;
		private $_del;
		private $_moveFrom;
		private $_moveTo;
		private $_oMath;
		private $_fldSimple;
		private $_hyperlink;
		private $_subDoc;

		
		/* --- Childrens --- */

		protected function getSmartTag(){ return $this->_smartTag->get(); }
		public function createSmartTag(){ $new = new \Word\Objects\SmartTagRun("w:smartTag"); $this->smartTag->AddRef($new); return $new; }

		protected function getSdt(){ return $this->_sdt->get(); }
		public function createSdt(){ $new = new \Word\Objects\SdtRun("w:sdt"); $this->sdt->AddRef($new); return $new; }

		protected function getR(){ return $this->_r->get(); }
		public function createR(){ $new = new \Word\Objects\R("w:r"); $this->r->AddRef($new); return $new; }

		protected function getProofErr(){ return $this->_proofErr->get(); }

		protected function getPermStart(){ return $this->_permStart->get(); }

		protected function getPermEnd(){ return $this->_permEnd->get(); }

		protected function getBookmarkStart(){ return $this->_bookmarkStart->get(); }
		public function createBookmarkStart(){ $new = new \Word\Objects\Bookmark("w:bookmarkStart"); $this->bookmarkStart->AddRef($new); return $new; }

		protected function getBookmarkEnd(){ return $this->_bookmarkEnd->get(); }
		public function createBookmarkEnd(){ $new = new \Word\Objects\MarkupRange("w:bookmarkEnd"); $this->bookmarkEnd->AddRef($new); return $new; }

		protected function getMoveFromRangeStart(){ return $this->_moveFromRangeStart->get(); }
		public function createMoveFromRangeStart(){ $new = new \Word\Objects\MoveBookmark("w:moveFromRangeStart"); $this->moveFromRangeStart->AddRef($new); return $new; }

		protected function getMoveFromRangeEnd(){ return $this->_moveFromRangeEnd->get(); }
		public function createMoveFromRangeEnd(){ $new = new \Word\Objects\MarkupRange("w:moveFromRangeEnd"); $this->moveFromRangeEnd->AddRef($new); return $new; }

		protected function getMoveToRangeStart(){ return $this->_moveToRangeStart->get(); }
		public function createMoveToRangeStart(){ $new = new \Word\Objects\MoveBookmark("w:moveToRangeStart"); $this->moveToRangeStart->AddRef($new); return $new; }

		protected function getMoveToRangeEnd(){ return $this->_moveToRangeEnd->get(); }
		public function createMoveToRangeEnd(){ $new = new \Word\Objects\MarkupRange("w:moveToRangeEnd"); $this->moveToRangeEnd->AddRef($new); return $new; }

		protected function getCommentRangeStart(){ return $this->_commentRangeStart->get(); }
		public function createCommentRangeStart(){ $new = new \Word\Objects\MarkupRange("w:commentRangeStart"); $this->commentRangeStart->AddRef($new); return $new; }

		protected function getCommentRangeEnd(){ return $this->_commentRangeEnd->get(); }
		public function createCommentRangeEnd(){ $new = new \Word\Objects\MarkupRange("w:commentRangeEnd"); $this->commentRangeEnd->AddRef($new); return $new; }

		protected function getCustomXmlInsRangeStart(){ return $this->_customXmlInsRangeStart->get(); }
		public function createCustomXmlInsRangeStart(){ $new = new \Word\Objects\TrackChange("w:customXmlInsRangeStart"); $this->customXmlInsRangeStart->AddRef($new); return $new; }

		protected function getCustomXmlInsRangeEnd(){ return $this->_customXmlInsRangeEnd->get(); }
		public function createCustomXmlInsRangeEnd(){ $new = new \Word\Objects\Markup("w:customXmlInsRangeEnd"); $this->customXmlInsRangeEnd->AddRef($new); return $new; }

		protected function getCustomXmlDelRangeStart(){ return $this->_customXmlDelRangeStart->get(); }
		public function createCustomXmlDelRangeStart(){ $new = new \Word\Objects\TrackChange("w:customXmlDelRangeStart"); $this->customXmlDelRangeStart->AddRef($new); return $new; }

		protected function getCustomXmlDelRangeEnd(){ return $this->_customXmlDelRangeEnd->get(); }
		public function createCustomXmlDelRangeEnd(){ $new = new \Word\Objects\Markup("w:customXmlDelRangeEnd"); $this->customXmlDelRangeEnd->AddRef($new); return $new; }

		protected function getCustomXmlMoveFromRangeStart(){ return $this->_customXmlMoveFromRangeStart->get(); }
		public function createCustomXmlMoveFromRangeStart(){ $new = new \Word\Objects\TrackChange("w:customXmlMoveFromRangeStart"); $this->customXmlMoveFromRangeStart->AddRef($new); return $new; }

		protected function getCustomXmlMoveFromRangeEnd(){ return $this->_customXmlMoveFromRangeEnd->get(); }
		public function createCustomXmlMoveFromRangeEnd(){ $new = new \Word\Objects\Markup("w:customXmlMoveFromRangeEnd"); $this->customXmlMoveFromRangeEnd->AddRef($new); return $new; }

		protected function getCustomXmlMoveToRangeStart(){ return $this->_customXmlMoveToRangeStart->get(); }
		public function createCustomXmlMoveToRangeStart(){ $new = new \Word\Objects\TrackChange("w:customXmlMoveToRangeStart"); $this->customXmlMoveToRangeStart->AddRef($new); return $new; }

		protected function getCustomXmlMoveToRangeEnd(){ return $this->_customXmlMoveToRangeEnd->get(); }
		public function createCustomXmlMoveToRangeEnd(){ $new = new \Word\Objects\Markup("w:customXmlMoveToRangeEnd"); $this->customXmlMoveToRangeEnd->AddRef($new); return $new; }

		protected function getIns(){ return $this->_ins->get(); }

		protected function getDel(){ return $this->_del->get(); }

		protected function getMoveFrom(){ return $this->_moveFrom->get(); }
		public function createMoveFrom(){ $new = new \Word\Objects\RunTrackChange("w:moveFrom"); $this->moveFrom->AddRef($new); return $new; }

		protected function getMoveTo(){ return $this->_moveTo->get(); }
		public function createMoveTo(){ $new = new \Word\Objects\RunTrackChange("w:moveTo"); $this->moveTo->AddRef($new); return $new; }

		protected function getOMath(){ return $this->_oMath->get(); }
		public function createOMath(){ $new = new \Word\Objects\OMath("m:oMath"); $this->oMath->AddRef($new); return $new; }

		protected function getFldSimple(){ return $this->_fldSimple->get(); }
		public function createFldSimple(){ $new = new \Word\Objects\SimpleField("w:fldSimple"); $this->fldSimple->AddRef($new); return $new; }

		protected function getHyperlink(){ return $this->_hyperlink->get(); }
		public function createHyperlink(){ $new = new \Word\Objects\Hyperlink("w:hyperlink"); $this->hyperlink->AddRef($new); return $new; }

		protected function getSubDoc(){ return $this->_subDoc->get(); }
		public function createSubDoc(){ $new = new \Word\Objects\Rel("w:subDoc"); $this->subDoc->AddRef($new); return $new; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\SmartTagRun",
					"Objects\SdtRun",
					"Objects\R",
					"Objects\ProofErr",
					"Objects\PermStart",
					"Objects\Perm",
					"Objects\Bookmark",
					"Objects\MarkupRange",
					"Objects\MoveBookmark",
					"Objects\TrackChange",
					"Objects\Markup",
					"Objects\RunTrackChange",
					"Objects\OMath",
					"Objects\SimpleField",
					"Objects\Hyperlink",
					"Objects\Rel"
				)
			);
				
			parent::__construct($name);

			$this->_smartTag = new \Word\PropertyArray($this);
			$this->_sdt = new \Word\PropertyArray($this);
			$this->_r = new \Word\PropertyArray($this);
			$this->_proofErr = new \Word\PropertyObject('\Word\Objects\ProofErr', "w:proofErr", $this);
			$this->_permStart = new \Word\PropertyObject('\Word\Objects\PermStart', "w:permStart", $this);
			$this->_permEnd = new \Word\PropertyObject('\Word\Objects\Perm', "w:permEnd", $this);
			$this->_bookmarkStart = new \Word\PropertyArray($this);
			$this->_bookmarkEnd = new \Word\PropertyArray($this);
			$this->_moveFromRangeStart = new \Word\PropertyArray($this);
			$this->_moveFromRangeEnd = new \Word\PropertyArray($this);
			$this->_moveToRangeStart = new \Word\PropertyArray($this);
			$this->_moveToRangeEnd = new \Word\PropertyArray($this);
			$this->_commentRangeStart = new \Word\PropertyArray($this);
			$this->_commentRangeEnd = new \Word\PropertyArray($this);
			$this->_customXmlInsRangeStart = new \Word\PropertyArray($this);
			$this->_customXmlInsRangeEnd = new \Word\PropertyArray($this);
			$this->_customXmlDelRangeStart = new \Word\PropertyArray($this);
			$this->_customXmlDelRangeEnd = new \Word\PropertyArray($this);
			$this->_customXmlMoveFromRangeStart = new \Word\PropertyArray($this);
			$this->_customXmlMoveFromRangeEnd = new \Word\PropertyArray($this);
			$this->_customXmlMoveToRangeStart = new \Word\PropertyArray($this);
			$this->_customXmlMoveToRangeEnd = new \Word\PropertyArray($this);
			$this->_ins = new \Word\PropertyObject('\Word\Objects\RunTrackChange', "w:ins", $this);
			$this->_del = new \Word\PropertyObject('\Word\Objects\RunTrackChange', "w:del", $this);
			$this->_moveFrom = new \Word\PropertyArray($this);
			$this->_moveTo = new \Word\PropertyArray($this);
			$this->_oMath = new \Word\PropertyArray($this);
			$this->_fldSimple = new \Word\PropertyArray($this);
			$this->_hyperlink = new \Word\PropertyArray($this);
			$this->_subDoc = new \Word\PropertyArray($this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 

				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{

		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>