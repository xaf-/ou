<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_SdtRow.html 
	 * @property-read SdtPr $sdtPr Structured Document Tag Properties
	 * @property-read SdtEndPr $sdtEndPr Structured Document Tag End Character Properties
	 * @property-read SdtContentRow $sdtContent Row-Level Structured Document Tag Content
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class SdtRow extends Object
	{
		
		/* --- Childrens --- */

		private $_sdtPr;
		private $_sdtEndPr;
		private $_sdtContent;

		
		/* --- Childrens --- */

		protected function getSdtPr(){ return $this->_sdtPr->get(); }

		protected function getSdtEndPr(){ return $this->_sdtEndPr->get(); }

		protected function getSdtContent(){ return $this->_sdtContent->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\SdtPr",
					"Objects\SdtEndPr",
					"Objects\SdtContentRow"
				)
			);
				
			parent::__construct($name);

			$this->_sdtPr = new \Word\PropertyObject('\Word\Objects\SdtPr', "w:sdtPr", $this);
			$this->_sdtEndPr = new \Word\PropertyObject('\Word\Objects\SdtEndPr', "w:sdtEndPr", $this);
			$this->_sdtContent = new \Word\PropertyObject('\Word\Objects\SdtContentRow', "w:sdtContent", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 

				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{

		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>