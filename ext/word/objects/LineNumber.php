<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_LineNumber.html 
	 * @property-read DecimalNumber $countBy Line Number Increments to Display
	 * @property-read DecimalNumber $start Line Numbering Starting Value
	 * @property-read TwipsMeasure $distance Distance Between Text and Line Numbering
	 * @property-read LineNumberRestart $restart Line Numbering Restart Setting
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class LineNumber extends Object
	{
		
		/* --- Attributes --- */

		private $_countBy;
		private $_start;
		private $_distance;
		private $_restart;

		
		/* --- Attributes --- */

		protected function getCountBy(){ return $this->_countBy; }
		protected function setCountBy($val){ $this->_countBy = $val; }

		protected function getStart(){ return $this->_start; }
		protected function setStart($val){ $this->_start = $val; }

		protected function getDistance(){ return $this->_distance; }
		protected function setDistance($val){ $this->_distance = $val; }

		protected function getRestart(){ return $this->_restart; }
		protected function setRestart($val){ $this->_restart = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\DecimalNumber",
					"Consts\TwipsMeasure",
					"Consts\LineNumberRestart"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->countBy === null && 
				$this->start === null && 
				$this->distance === null && 
				$this->restart === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->countBy !== null) $xml->writeAttribute("w:countBy", $this->countBy);
			if ($this->start !== null) $xml->writeAttribute("w:start", $this->start);
			if ($this->distance !== null) $xml->writeAttribute("w:distance", $this->distance);
			if ($this->restart !== null) $xml->writeAttribute("w:restart", $this->restart);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>