<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_DocGrid.html 
	 * @property-read DocGrid $type Document Grid Type
	 * @property-read DecimalNumber $linePitch Document Grid Line Pitch
	 * @property-read DecimalNumber $charSpace Document Grid Character Pitch
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class DocGrid extends Object
	{
		
		/* --- Attributes --- */

		private $_type;
		private $_linePitch;
		private $_charSpace;

		
		/* --- Attributes --- */

		protected function getType(){ return $this->_type; }
		protected function setType($val){ $this->_type = $val; }

		protected function getLinePitch(){ return $this->_linePitch; }
		protected function setLinePitch($val){ $this->_linePitch = $val; }

		protected function getCharSpace(){ return $this->_charSpace; }
		protected function setCharSpace($val){ $this->_charSpace = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\DocGrid",
					"Consts\DecimalNumber"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->type === null && 
				$this->linePitch === null && 
				$this->charSpace === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->type !== null) $xml->writeAttribute("w:type", $this->type);
			if ($this->linePitch !== null) $xml->writeAttribute("w:linePitch", $this->linePitch);
			if ($this->charSpace !== null) $xml->writeAttribute("w:charSpace", $this->charSpace);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>