<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_TblCellMar.html 
	 * @property-read TblWidth $top Table Cell Top Margin Default
	 * @property-read TblWidth $left Table Cell Left Margin Default
	 * @property-read TblWidth $bottom Table Cell Bottom Margin Default
	 * @property-read TblWidth $right Table Cell Right Margin Default
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class TblCellMar extends Object
	{
		
		/* --- Childrens --- */

		private $_top;
		private $_left;
		private $_bottom;
		private $_right;

		
		/* --- Childrens --- */

		protected function getTop(){ return $this->_top->get(); }

		protected function getLeft(){ return $this->_left->get(); }

		protected function getBottom(){ return $this->_bottom->get(); }

		protected function getRight(){ return $this->_right->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\TblWidth"
				)
			);
				
			parent::__construct($name);

			$this->_top = new \Word\PropertyObject('\Word\Objects\TblWidth', "w:top", $this);
			$this->_left = new \Word\PropertyObject('\Word\Objects\TblWidth', "w:left", $this);
			$this->_bottom = new \Word\PropertyObject('\Word\Objects\TblWidth', "w:bottom", $this);
			$this->_right = new \Word\PropertyObject('\Word\Objects\TblWidth', "w:right", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 

				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{

		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>