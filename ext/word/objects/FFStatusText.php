<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_FFStatusText.html 
	 * @property-read InfoTextType $type Status Text Type
	 * @property-read FFStatusTextVal $val Status Text Value
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class FFStatusText extends Object
	{
		
		/* --- Attributes --- */

		private $_type;
		private $_val;

		
		/* --- Attributes --- */

		protected function getType(){ return $this->_type; }
		protected function setType($val){ $this->_type = $val; }

		protected function getVal(){ return $this->_val; }
		protected function setVal($val){ $this->_val = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\InfoTextType",
					"Consts\FFStatusTextVal"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->type === null && 
				$this->val === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->type !== null) $xml->writeAttribute("w:type", $this->type);
			if ($this->val !== null) $xml->writeAttribute("w:val", $this->val);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>