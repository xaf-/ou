<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_PageMar.html 
	 * @property-read SignedTwipsMeasure $top Top Margin Spacing
	 * @property-read TwipsMeasure $right Right Margin Spacing
	 * @property-read SignedTwipsMeasure $bottom Page Bottom Spacing
	 * @property-read TwipsMeasure $left Left Margin Spacing
	 * @property-read TwipsMeasure $header Spacing to Top of Header
	 * @property-read TwipsMeasure $footer Spacing to Bottom of Footer
	 * @property-read TwipsMeasure $gutter Page Gutter Spacing
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class PageMar extends Object
	{
		
		/* --- Attributes --- */

		private $_top;
		private $_right;
		private $_bottom;
		private $_left;
		private $_header;
		private $_footer;
		private $_gutter;

		
		/* --- Attributes --- */

		protected function getTop(){ return $this->_top; }
		protected function setTop($val){ $this->_top = $val; }

		protected function getRight(){ return $this->_right; }
		protected function setRight($val){ $this->_right = $val; }

		protected function getBottom(){ return $this->_bottom; }
		protected function setBottom($val){ $this->_bottom = $val; }

		protected function getLeft(){ return $this->_left; }
		protected function setLeft($val){ $this->_left = $val; }

		protected function getHeader(){ return $this->_header; }
		protected function setHeader($val){ $this->_header = $val; }

		protected function getFooter(){ return $this->_footer; }
		protected function setFooter($val){ $this->_footer = $val; }

		protected function getGutter(){ return $this->_gutter; }
		protected function setGutter($val){ $this->_gutter = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\SignedTwipsMeasure",
					"Consts\TwipsMeasure"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->top === null && 
				$this->right === null && 
				$this->bottom === null && 
				$this->left === null && 
				$this->header === null && 
				$this->footer === null && 
				$this->gutter === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->top !== null) $xml->writeAttribute("w:top", $this->top);
			if ($this->right !== null) $xml->writeAttribute("w:right", $this->right);
			if ($this->bottom !== null) $xml->writeAttribute("w:bottom", $this->bottom);
			if ($this->left !== null) $xml->writeAttribute("w:left", $this->left);
			if ($this->header !== null) $xml->writeAttribute("w:header", $this->header);
			if ($this->footer !== null) $xml->writeAttribute("w:footer", $this->footer);
			if ($this->gutter !== null) $xml->writeAttribute("w:gutter", $this->gutter);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return
				$this->top !== null && 
			/* --- Attributes --- */

				$this->right !== null && 
			/* --- Attributes --- */

				$this->bottom !== null && 
			/* --- Attributes --- */

				$this->left !== null && 
			/* --- Attributes --- */

				$this->header !== null && 
			/* --- Attributes --- */

				$this->footer !== null && 
			/* --- Attributes --- */

				$this->gutter !== null && 


				true;
		}
		
		
	}
	
}

?>