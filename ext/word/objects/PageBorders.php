<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_PageBorders.html 
	 * @property-read Border $top Top Border
	 * @property-read Border $left Left Border
	 * @property-read Border $bottom Bottom Border
	 * @property-read Border $right Right Border
	 * @property-read PageBorderZOrder $zOrder Z-Ordering of Page Border
	 * @property-read PageBorderDisplay $display Pages to Display Page Borders
	 * @property-read PageBorderOffset $offsetFrom Page Border Positioning
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class PageBorders extends Object
	{
		
		/* --- Attributes --- */

		private $_zOrder;
		private $_display;
		private $_offsetFrom;

		/* --- Childrens --- */

		private $_top;
		private $_left;
		private $_bottom;
		private $_right;

		
		/* --- Attributes --- */

		protected function getZOrder(){ return $this->_zOrder; }
		protected function setZOrder($val){ $this->_zOrder = $val; }

		protected function getDisplay(){ return $this->_display; }
		protected function setDisplay($val){ $this->_display = $val; }

		protected function getOffsetFrom(){ return $this->_offsetFrom; }
		protected function setOffsetFrom($val){ $this->_offsetFrom = $val; }

		/* --- Childrens --- */

		protected function getTop(){ return $this->_top->get(); }

		protected function getLeft(){ return $this->_left->get(); }

		protected function getBottom(){ return $this->_bottom->get(); }

		protected function getRight(){ return $this->_right->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\Border",
					"Consts\PageBorderZOrder",
					"Consts\PageBorderDisplay",
					"Consts\PageBorderOffset"
				)
			);
				
			parent::__construct($name);

			$this->_top = new \Word\PropertyObject('\Word\Objects\Border', "w:top", $this);
			$this->_left = new \Word\PropertyObject('\Word\Objects\Border', "w:left", $this);
			$this->_bottom = new \Word\PropertyObject('\Word\Objects\Border', "w:bottom", $this);
			$this->_right = new \Word\PropertyObject('\Word\Objects\Border', "w:right", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->zOrder === null && 
				$this->display === null && 
				$this->offsetFrom === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->zOrder !== null) $xml->writeAttribute("w:zOrder", $this->zOrder);
			if ($this->display !== null) $xml->writeAttribute("w:display", $this->display);
			if ($this->offsetFrom !== null) $xml->writeAttribute("w:offsetFrom", $this->offsetFrom);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>