<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_TcPr.html 
	 * @property-read Cnf $cnfStyle Table Cell Conditional Formatting
	 * @property-read TblWidth $tcW Preferred Table Cell Width
	 * @property-read DecimalNumber $gridSpan Grid Columns Spanned by Current Table Cell
	 * @property-read HMerge $hMerge Horizontally Merged Cell
	 * @property-read VMerge $vMerge Vertically Merged Cell
	 * @property-read TcBorders $tcBorders Table Cell Borders
	 * @property-read Shd $shd Table Cell Shading
	 * @property-read OnOff $noWrap Don't Wrap Cell Content
	 * @property-read TcMar $tcMar Single Table Cell Margins
	 * @property-read TextDirection $textDirection Table Cell Text Flow Direction
	 * @property-read OnOff $tcFitText Fit Text Within Cell
	 * @property-read VerticalJc $vAlign Table Cell Vertical Alignment
	 * @property-read OnOff $hideMark Ignore End Of Cell Marker In Row Height Calculation
	 * @property-read TrackChange $cellIns Table Cell Insertion
	 * @property-read TrackChange $cellDel Table Cell Deletion
	 * @property-read CellMergeTrackChange $cellMerge Vertically Merged/Split Table Cells
	 * @property-read TcPrChange $tcPrChange Revision Information for Table Cell Properties
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class TcPr extends Object
	{
		
		/* --- Childrens --- */

		private $_cnfStyle;
		private $_tcW;
		private $_gridSpan;
		private $_hMerge;
		private $_vMerge;
		private $_tcBorders;
		private $_shd;
		private $_noWrap;
		private $_tcMar;
		private $_textDirection;
		private $_tcFitText;
		private $_vAlign;
		private $_hideMark;
		private $_cellIns;
		private $_cellDel;
		private $_cellMerge;
		private $_tcPrChange;

		
		/* --- Childrens --- */

		protected function getCnfStyle(){ return $this->_cnfStyle->get(); }

		protected function getTcW(){ return $this->_tcW->get(); }

		protected function getGridSpan(){ return $this->_gridSpan->get(); }

		protected function getHMerge(){ return $this->_hMerge->get(); }

		protected function getVMerge(){ return $this->_vMerge->get(); }

		protected function getTcBorders(){ return $this->_tcBorders->get(); }

		protected function getShd(){ return $this->_shd->get(); }

		protected function getNoWrap(){ return $this->_noWrap->get(); }

		protected function getTcMar(){ return $this->_tcMar->get(); }

		protected function getTextDirection(){ return $this->_textDirection->get(); }

		protected function getTcFitText(){ return $this->_tcFitText->get(); }

		protected function getVAlign(){ return $this->_vAlign->get(); }

		protected function getHideMark(){ return $this->_hideMark->get(); }

		protected function getCellIns(){ return $this->_cellIns->get(); }

		protected function getCellDel(){ return $this->_cellDel->get(); }

		protected function getCellMerge(){ return $this->_cellMerge->get(); }

		protected function getTcPrChange(){ return $this->_tcPrChange->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\Cnf",
					"Objects\TblWidth",
					"Objects\DecimalNumber",
					"Objects\HMerge",
					"Objects\VMerge",
					"Objects\TcBorders",
					"Objects\Shd",
					"Objects\OnOff",
					"Objects\TcMar",
					"Objects\TextDirection",
					"Objects\VerticalJc",
					"Objects\TrackChange",
					"Objects\CellMergeTrackChange",
					"Objects\TcPrChange"
				)
			);
				
			parent::__construct($name);

			$this->_cnfStyle = new \Word\PropertyObject('\Word\Objects\Cnf', "w:cnfStyle", $this);
			$this->_tcW = new \Word\PropertyObject('\Word\Objects\TblWidth', "w:tcW", $this);
			$this->_gridSpan = new \Word\PropertyObject('\Word\Objects\DecimalNumber', "w:gridSpan", $this);
			$this->_hMerge = new \Word\PropertyObject('\Word\Objects\HMerge', "w:hMerge", $this);
			$this->_vMerge = new \Word\PropertyObject('\Word\Objects\VMerge', "w:vMerge", $this);
			$this->_tcBorders = new \Word\PropertyObject('\Word\Objects\TcBorders', "w:tcBorders", $this);
			$this->_shd = new \Word\PropertyObject('\Word\Objects\Shd', "w:shd", $this);
			$this->_noWrap = new \Word\PropertyObject('\Word\Objects\OnOff', "w:noWrap", $this);
			$this->_tcMar = new \Word\PropertyObject('\Word\Objects\TcMar', "w:tcMar", $this);
			$this->_textDirection = new \Word\PropertyObject('\Word\Objects\TextDirection', "w:textDirection", $this);
			$this->_tcFitText = new \Word\PropertyObject('\Word\Objects\OnOff', "w:tcFitText", $this);
			$this->_vAlign = new \Word\PropertyObject('\Word\Objects\VerticalJc', "w:vAlign", $this);
			$this->_hideMark = new \Word\PropertyObject('\Word\Objects\OnOff', "w:hideMark", $this);
			$this->_cellIns = new \Word\PropertyObject('\Word\Objects\TrackChange', "w:cellIns", $this);
			$this->_cellDel = new \Word\PropertyObject('\Word\Objects\TrackChange', "w:cellDel", $this);
			$this->_cellMerge = new \Word\PropertyObject('\Word\Objects\CellMergeTrackChange', "w:cellMerge", $this);
			$this->_tcPrChange = new \Word\PropertyObject('\Word\Objects\TcPrChange', "w:tcPrChange", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 

				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{

		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>