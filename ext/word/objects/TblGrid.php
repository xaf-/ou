<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_TblGrid.html 
	 * @property-read TblGridCol[] $gridCol Grid Column Definition
	 * @property-read TblGridChange $tblGridChange Revision Information for Table Grid Column Definitions
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class TblGrid extends Object
	{
		
		/* --- Childrens --- */

		private $_gridCol;
		private $_tblGridChange;

		
		/* --- Childrens --- */

		protected function getGridCol(){ return $this->_gridCol->get(); }
		public function createGridCol(){ $new = new \Word\Objects\TblGridCol("w:gridCol"); $this->gridCol->AddRef($new); return $new; }

		protected function getTblGridChange(){ return $this->_tblGridChange->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\TblGridCol",
					"Objects\TblGridChange"
				)
			);
				
			parent::__construct($name);

			$this->_gridCol = new \Word\PropertyArray($this);
			$this->_tblGridChange = new \Word\PropertyObject('\Word\Objects\TblGridChange', "w:tblGridChange", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 

				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{

		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>