<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_TrPr.html 
	 * @property-read Cnf $cnfStyle Table Row Conditional Formatting
	 * @property-read DecimalNumber $divId Associated HTML div ID
	 * @property-read DecimalNumber $gridBefore Grid Columns Before First Cell
	 * @property-read DecimalNumber $gridAfter Grid Columns After Last Cell
	 * @property-read TblWidth $wBefore Preferred Width Before Table Row
	 * @property-read TblWidth $wAfter Preferred Width After Table Row
	 * @property-read OnOff $cantSplit Table Row Cannot Break Across Pages
	 * @property-read Height $trHeight Table Row Height
	 * @property-read OnOff $tblHeader Repeat Table Row on Every New Page
	 * @property-read TblWidth $tblCellSpacing Table Row Cell Spacing
	 * @property-read Jc $jc Table Row Alignment
	 * @property-read OnOff $hidden Hidden Table Row Marker
	 * @property-read TrackChange $ins Inserted Table Row
	 * @property-read TrackChange $del Deleted Table Row
	 * @property-read TrPrChange $trPrChange Revision Information for Table Row Properties
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class TrPr extends Object
	{
		
		/* --- Childrens --- */

		private $_cnfStyle;
		private $_divId;
		private $_gridBefore;
		private $_gridAfter;
		private $_wBefore;
		private $_wAfter;
		private $_cantSplit;
		private $_trHeight;
		private $_tblHeader;
		private $_tblCellSpacing;
		private $_jc;
		private $_hidden;
		private $_ins;
		private $_del;
		private $_trPrChange;

		
		/* --- Childrens --- */

		protected function getCnfStyle(){ return $this->_cnfStyle->get(); }

		protected function getDivId(){ return $this->_divId->get(); }

		protected function getGridBefore(){ return $this->_gridBefore->get(); }

		protected function getGridAfter(){ return $this->_gridAfter->get(); }

		protected function getWBefore(){ return $this->_wBefore->get(); }

		protected function getWAfter(){ return $this->_wAfter->get(); }

		protected function getCantSplit(){ return $this->_cantSplit->get(); }

		protected function getTrHeight(){ return $this->_trHeight->get(); }

		protected function getTblHeader(){ return $this->_tblHeader->get(); }

		protected function getTblCellSpacing(){ return $this->_tblCellSpacing->get(); }

		protected function getJc(){ return $this->_jc->get(); }

		protected function getHidden(){ return $this->_hidden->get(); }

		protected function getIns(){ return $this->_ins->get(); }

		protected function getDel(){ return $this->_del->get(); }

		protected function getTrPrChange(){ return $this->_trPrChange->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\Cnf",
					"Objects\DecimalNumber",
					"Objects\TblWidth",
					"Objects\OnOff",
					"Objects\Height",
					"Objects\Jc",
					"Objects\TrackChange",
					"Objects\TrPrChange"
				)
			);
				
			parent::__construct($name);

			$this->_cnfStyle = new \Word\PropertyObject('\Word\Objects\Cnf', "w:cnfStyle", $this);
			$this->_divId = new \Word\PropertyObject('\Word\Objects\DecimalNumber', "w:divId", $this);
			$this->_gridBefore = new \Word\PropertyObject('\Word\Objects\DecimalNumber', "w:gridBefore", $this);
			$this->_gridAfter = new \Word\PropertyObject('\Word\Objects\DecimalNumber', "w:gridAfter", $this);
			$this->_wBefore = new \Word\PropertyObject('\Word\Objects\TblWidth', "w:wBefore", $this);
			$this->_wAfter = new \Word\PropertyObject('\Word\Objects\TblWidth', "w:wAfter", $this);
			$this->_cantSplit = new \Word\PropertyObject('\Word\Objects\OnOff', "w:cantSplit", $this);
			$this->_trHeight = new \Word\PropertyObject('\Word\Objects\Height', "w:trHeight", $this);
			$this->_tblHeader = new \Word\PropertyObject('\Word\Objects\OnOff', "w:tblHeader", $this);
			$this->_tblCellSpacing = new \Word\PropertyObject('\Word\Objects\TblWidth', "w:tblCellSpacing", $this);
			$this->_jc = new \Word\PropertyObject('\Word\Objects\Jc', "w:jc", $this);
			$this->_hidden = new \Word\PropertyObject('\Word\Objects\OnOff', "w:hidden", $this);
			$this->_ins = new \Word\PropertyObject('\Word\Objects\TrackChange', "w:ins", $this);
			$this->_del = new \Word\PropertyObject('\Word\Objects\TrackChange', "w:del", $this);
			$this->_trPrChange = new \Word\PropertyObject('\Word\Objects\TrPrChange', "w:trPrChange", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 

				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{

		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>