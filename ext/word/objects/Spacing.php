<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_Spacing.html 
	 * @property-read TwipsMeasure $before Spacing Above Paragraph
	 * @property-read DecimalNumber $beforeLines Spacing Above Paragraph IN Line Units
	 * @property-read OnOff $beforeAutospacing Automatically Determine Spacing Above Paragraph
	 * @property-read TwipsMeasure $after Spacing Below Paragraph
	 * @property-read DecimalNumber $afterLines Spacing Below Paragraph in Line Units
	 * @property-read OnOff $afterAutospacing Automatically Determine Spacing Below Paragraph
	 * @property-read SignedTwipsMeasure $line Spacing Between Lines in Paragraph
	 * @property-read LineSpacingRule $lineRule Type of Spacing Between Lines
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class Spacing extends Object
	{
		
		/* --- Attributes --- */

		private $_before;
		private $_beforeLines;
		private $_beforeAutospacing;
		private $_after;
		private $_afterLines;
		private $_afterAutospacing;
		private $_line;
		private $_lineRule;

		
		/* --- Attributes --- */

		protected function getBefore(){ return $this->_before; }
		protected function setBefore($val){ $this->_before = $val; }

		protected function getBeforeLines(){ return $this->_beforeLines; }
		protected function setBeforeLines($val){ $this->_beforeLines = $val; }

		protected function getBeforeAutospacing(){ return $this->_beforeAutospacing; }
		protected function setBeforeAutospacing($val){ $this->_beforeAutospacing = $val; }

		protected function getAfter(){ return $this->_after; }
		protected function setAfter($val){ $this->_after = $val; }

		protected function getAfterLines(){ return $this->_afterLines; }
		protected function setAfterLines($val){ $this->_afterLines = $val; }

		protected function getAfterAutospacing(){ return $this->_afterAutospacing; }
		protected function setAfterAutospacing($val){ $this->_afterAutospacing = $val; }

		protected function getLine(){ return $this->_line; }
		protected function setLine($val){ $this->_line = $val; }

		protected function getLineRule(){ return $this->_lineRule; }
		protected function setLineRule($val){ $this->_lineRule = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\TwipsMeasure",
					"Consts\DecimalNumber",
					"Consts\OnOff",
					"Consts\SignedTwipsMeasure",
					"Consts\LineSpacingRule"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->before === null && 
				$this->beforeLines === null && 
				$this->beforeAutospacing === null && 
				$this->after === null && 
				$this->afterLines === null && 
				$this->afterAutospacing === null && 
				$this->line === null && 
				$this->lineRule === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->before !== null) $xml->writeAttribute("w:before", $this->before);
			if ($this->beforeLines !== null) $xml->writeAttribute("w:beforeLines", $this->beforeLines);
			if ($this->beforeAutospacing !== null) $xml->writeAttribute("w:beforeAutospacing", $this->beforeAutospacing);
			if ($this->after !== null) $xml->writeAttribute("w:after", $this->after);
			if ($this->afterLines !== null) $xml->writeAttribute("w:afterLines", $this->afterLines);
			if ($this->afterAutospacing !== null) $xml->writeAttribute("w:afterAutospacing", $this->afterAutospacing);
			if ($this->line !== null) $xml->writeAttribute("w:line", $this->line);
			if ($this->lineRule !== null) $xml->writeAttribute("w:lineRule", $this->lineRule);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>