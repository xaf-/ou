<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_SdtDocPart.html 
	 * @property-read String $docPartGallery Document Part Gallery Filter
	 * @property-read String $docPartCategory Document Part Category Filter
	 * @property-read OnOff $docPartUnique Built-In Document Part
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class SdtDocPart extends Object
	{
		
		/* --- Childrens --- */

		private $_docPartGallery;
		private $_docPartCategory;
		private $_docPartUnique;

		
		/* --- Childrens --- */

		protected function getDocPartGallery(){ return $this->_docPartGallery->get(); }

		protected function getDocPartCategory(){ return $this->_docPartCategory->get(); }

		protected function getDocPartUnique(){ return $this->_docPartUnique->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\String",
					"Objects\OnOff"
				)
			);
				
			parent::__construct($name);

			$this->_docPartGallery = new \Word\PropertyObject('\Word\Objects\String', "w:docPartGallery", $this);
			$this->_docPartCategory = new \Word\PropertyObject('\Word\Objects\String', "w:docPartCategory", $this);
			$this->_docPartUnique = new \Word\PropertyObject('\Word\Objects\OnOff', "w:docPartUnique", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 

				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{

		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>