<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_Border.html 
	 * @property-read Border $val Border Style
	 * @property-read HexColor $color Border Color
	 * @property-read ThemeColor $themeColor Border Theme Color
	 * @property-read UcharHexNumber $themeTint Border Theme Color Tint
	 * @property-read UcharHexNumber $themeShade Border Theme Color Shade
	 * @property-read EighthPointMeasure $sz Border Width
	 * @property-read PointMeasure $space Border Spacing Measurement
	 * @property-read OnOff $shadow Border Shadow
	 * @property-read OnOff $frame Create Frame Effect
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class Border extends Object
	{
		
		/* --- Attributes --- */

		private $_val;
		private $_color;
		private $_themeColor;
		private $_themeTint;
		private $_themeShade;
		private $_sz;
		private $_space;
		private $_shadow;
		private $_frame;

		
		/* --- Attributes --- */

		protected function getVal(){ return $this->_val; }
		protected function setVal($val){ $this->_val = $val; }

		protected function getColor(){ return $this->_color; }
		protected function setColor($val){ $this->_color = $val; }

		protected function getThemeColor(){ return $this->_themeColor; }
		protected function setThemeColor($val){ $this->_themeColor = $val; }

		protected function getThemeTint(){ return $this->_themeTint; }
		protected function setThemeTint($val){ $this->_themeTint = $val; }

		protected function getThemeShade(){ return $this->_themeShade; }
		protected function setThemeShade($val){ $this->_themeShade = $val; }

		protected function getSz(){ return $this->_sz; }
		protected function setSz($val){ $this->_sz = $val; }

		protected function getSpace(){ return $this->_space; }
		protected function setSpace($val){ $this->_space = $val; }

		protected function getShadow(){ return $this->_shadow; }
		protected function setShadow($val){ $this->_shadow = $val; }

		protected function getFrame(){ return $this->_frame; }
		protected function setFrame($val){ $this->_frame = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\Border",
					"Consts\HexColor",
					"Consts\ThemeColor",
					"Consts\UcharHexNumber",
					"Consts\EighthPointMeasure",
					"Consts\PointMeasure",
					"Consts\OnOff"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->val === null && 
				$this->color === null && 
				$this->themeColor === null && 
				$this->themeTint === null && 
				$this->themeShade === null && 
				$this->sz === null && 
				$this->space === null && 
				$this->shadow === null && 
				$this->frame === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->val !== null) $xml->writeAttribute("w:val", $this->val);
			if ($this->color !== null) $xml->writeAttribute("w:color", $this->color);
			if ($this->themeColor !== null) $xml->writeAttribute("w:themeColor", $this->themeColor);
			if ($this->themeTint !== null) $xml->writeAttribute("w:themeTint", $this->themeTint);
			if ($this->themeShade !== null) $xml->writeAttribute("w:themeShade", $this->themeShade);
			if ($this->sz !== null) $xml->writeAttribute("w:sz", $this->sz);
			if ($this->space !== null) $xml->writeAttribute("w:space", $this->space);
			if ($this->shadow !== null) $xml->writeAttribute("w:shadow", $this->shadow);
			if ($this->frame !== null) $xml->writeAttribute("w:frame", $this->frame);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return
				$this->val !== null && 
			/* --- Attributes --- */

			/* --- Attributes --- */

			/* --- Attributes --- */

			/* --- Attributes --- */

			/* --- Attributes --- */

			/* --- Attributes --- */

			/* --- Attributes --- */

			/* --- Attributes --- */



				true;
		}
		
		
	}
	
}

?>