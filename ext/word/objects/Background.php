<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_Background.html 
	 * @property-read HexColor $color Background Color
	 * @property-read ThemeColor $themeColor Background Theme Color
	 * @property-read UcharHexNumber $themeTint Border Theme Color Tint
	 * @property-read UcharHexNumber $themeShade Border Theme Color Shade
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class Background extends Object
	{
		
		/* --- Attributes --- */

		private $_color;
		private $_themeColor;
		private $_themeTint;
		private $_themeShade;

		
		/* --- Attributes --- */

		protected function getColor(){ return $this->_color; }
		protected function setColor($val){ $this->_color = $val; }

		protected function getThemeColor(){ return $this->_themeColor; }
		protected function setThemeColor($val){ $this->_themeColor = $val; }

		protected function getThemeTint(){ return $this->_themeTint; }
		protected function setThemeTint($val){ $this->_themeTint = $val; }

		protected function getThemeShade(){ return $this->_themeShade; }
		protected function setThemeShade($val){ $this->_themeShade = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\HexColor",
					"Consts\ThemeColor",
					"Consts\UcharHexNumber"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->color === null && 
				$this->themeColor === null && 
				$this->themeTint === null && 
				$this->themeShade === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->color !== null) $xml->writeAttribute("w:color", $this->color);
			if ($this->themeColor !== null) $xml->writeAttribute("w:themeColor", $this->themeColor);
			if ($this->themeTint !== null) $xml->writeAttribute("w:themeTint", $this->themeTint);
			if ($this->themeShade !== null) $xml->writeAttribute("w:themeShade", $this->themeShade);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>