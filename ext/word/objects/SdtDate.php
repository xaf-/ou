<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_SdtDate.html 
	 * @property-read String $dateFormat Date Display Mask
	 * @property-read Lang $lid Date Picker Language ID
	 * @property-read SdtDateMappingType $storeMappedDataAs Custom XML Data Date Storage Format
	 * @property-read CalendarType $calendar Date Picker Calendar Type
	 * @property-read DateTime $fullDate Last Known Date in XML Schema DateTime Format
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class SdtDate extends Object
	{
		
		/* --- Attributes --- */

		private $_fullDate;

		/* --- Childrens --- */

		private $_dateFormat;
		private $_lid;
		private $_storeMappedDataAs;
		private $_calendar;

		
		/* --- Attributes --- */

		protected function getFullDate(){ return $this->_fullDate; }
		protected function setFullDate($val){ $this->_fullDate = $val; }

		/* --- Childrens --- */

		protected function getDateFormat(){ return $this->_dateFormat->get(); }

		protected function getLid(){ return $this->_lid->get(); }

		protected function getStoreMappedDataAs(){ return $this->_storeMappedDataAs->get(); }

		protected function getCalendar(){ return $this->_calendar->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\String",
					"Objects\Lang",
					"Objects\SdtDateMappingType",
					"Objects\CalendarType",
					"Consts\DateTime"
				)
			);
				
			parent::__construct($name);

			$this->_dateFormat = new \Word\PropertyObject('\Word\Objects\String', "w:dateFormat", $this);
			$this->_lid = new \Word\PropertyObject('\Word\Objects\Lang', "w:lid", $this);
			$this->_storeMappedDataAs = new \Word\PropertyObject('\Word\Objects\SdtDateMappingType', "w:storeMappedDataAs", $this);
			$this->_calendar = new \Word\PropertyObject('\Word\Objects\CalendarType', "w:calendar", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->fullDate === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->fullDate !== null) $xml->writeAttribute("w:fullDate", $this->fullDate);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>