<?php

	/**
	 * 
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 09/08/2013
	 */

	require_once dirname(__FILE__) . "/../../../class.ou_config.php";
	
	if (!class_exists("OU_Config")) die("Require OU Framework!");
	
	OU_Config::IncClass(
		array(
			"OU_CURL"
		)
	);
	OU_Config::IncExt("phpQuery");
	
	 
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: classgen.php 300 2013-08-23 11:00:53Z xaguilarf $
	 *
	 */
	class ClassGenField implements ArrayAccess
	{
		private $_info;
		/**
		 * @var ClassGen
		 **/
		private $_clss;
		/**
		 * @var ClassGen
		 **/
		private $_resolved = null;
		public function __construct($clss, $info)
		{
			$this->_info = $info;
			$this->_clss = $clss;
		}
		
		public function resolve()
		{
			if ($this->_resolved !== null) return $this->_resolved;
			$this->_resolved = false;
			if ($this["link"])
				if ($n = ClassGen::resolve($this["link"], $this->_clss->_idx))
				{
					$this->_resolved = $n;
					$this["class"] = $n->name();
					$this["class_abbr"] = $n->name_abbr();
				}
			return $this->_resolved;
		}
		
		public function getCustomType()
		{
			
			if (!isset($this["class"]) || !$this["class"]) return false;
			
			switch (($this["class"]))
			{
				/*
				// case "ST_String":
				case "ST_Lang":
				case "ST_DateTime":
					return "string";
				case "ST_HpsMeasure":
				case "ST_PointMeasure":
				case "ST_RelationshipId":
				case "ST_DecimalNumber":
				case "ST_SignedTwipsMeasure":
					return "number";
				case "ST_TwipsMeasure":
				case "ST_LongHexNumber":
				case "ST_ShortHexNumber":
				case "ST_UcharHexNumber":
				case "ST_EighthPointMeasure":
				case "ST_HexColor":
					return "hex";
					*/
			}
			return false;
		}
		
		public function ignoreField()
		{
			switch ($this->name())
			{
				case "customXml": // Proboca bucle
				case "accPr": // Proboca bucle
				case "oMathParaPr": // Proboca bucle
				case "oMathPara": // Proboca bucle
					return true;
			}
			return false;
		}
		
		public function hasClass()
		{
			if ($this->getCustomType() === false)
				return isset($this["class"]) && $this["class"];
			else
				return false;
		}
		
		public function isConst()
		{
			if ($this->hasClass())
				return substr($this["class"], 0, 2) == "ST";
			else
				return false;
		}
		
		public function isMulti()
		{
			return  $this["multi"];
		}
		
		public function name()
		{
			$p = strpos($this["oname"], ":");
			if ($p  !== false) $p++;
			return ClassGen::trim(substr($this["oname"],$p));
		}
		
		public function offsetExists ($offset) {
			return isset($this->_info[$offset]);
		}
		
		public function offsetGet ($offset) {
			return $this->_info[$offset];
		}
		
		public function offsetSet ($offset, $value) {
			$this->_info[$offset] = $value;
		}
		
		public function offsetUnset ($offset) {
			unset($this->_info[$offset]);
		}
		
		public function generateAdd()
		{
			// _var = PropertyObject; $var = PropertyObject::get()
			return 'public function add' . ucfirst($this->name()) . '($v){ $this->' . $this->name() . '[] = $v; }';
		}
		
		public function generateAddCreate()
		{
			$hasContent = false;
			if ($this->hasClass() && $this->resolve())
			{
				foreach ($this->resolve()->childrens() as $c)
					if ($c["is_content"])
					{
						$hasContent = true;
						break;
					}  
			}
			if ($hasContent)
				return 'public function create' . ucfirst($this->name()) . '($content=null){ $new = new \Word\Objects\\'.$this["class_abbr"].'("'.addslashes($this["oname"]).'"); $new->content = $content; $this->'.($this->name()).'->AddRef($new); return $new; }';
			else
				return 'public function create' . ucfirst($this->name()) . '(){ $new = new \Word\Objects\\'.$this["class_abbr"].'("'.addslashes($this["oname"]).'"); $this->'.($this->name()).'->AddRef($new); return $new; }';
		}
		
		public function generateGetter()
		{
			if ($this->hasClass() && !$this->isConst())
				return 'protected function get' . ucfirst($this->name()) . '(){ return $this->_' . $this->name() . '->get(); }';
			else
				return 'protected function get' . ucfirst($this->name()) . '(){ return $this->_' . $this->name() . '; }';
		}
		
		public function hasSetter()
		{
			return ($this->isConst() || !$this->hasClass()) && !$this->isMulti();
		}
		
		public function generateSetter()
		{
			return 'protected function set' . ucfirst($this->name()) . '($val){ $this->_' . $this->name() . ' = $val; }';
		}
		
		public function generateWrite()
		{
			if ($this["is_content"])
				return '$xml->text($this->_'.$this->name().');';
			else if ($this->hasClass() && !$this->isConst())
				return '$this->_' . $this->name() .'->_write($xml);';
			else
				return 'if ($this->' . $this->name() .' !== null) $xml->writeAttribute("'.$this["oname"].'", $this->' . $this->name() .');'; 
		}
		
		public function generateEmpty()
		{
			if ($this->hasClass() && !$this->isConst())
				return '$this->_' . $this->name() .'->_empty()';
			else
				return '$this->' . $this->name() .' === null';
		}
		
		public function generateValidate()
		{
			if ($this->hasClass() && !$this->isConst())
				return '$this->_' . $this->name() .'->_validate()';
			else
				return '$this->' . $this->name() .' !== null';
		}
		
		public function generateProp()
		{
			$type = (isset($this["type"])&&$this["type"]?$this["type"]:"mixed");
			if (($type2 = $this->getCustomType()) !== false) $type = $type2;
			if ($this->isMulti())
			{
				if ($this->hasClass())
					return "@property-read ".$this["class_abbr"]."[] \$" . $this->name() . " " . ClassGen::trim($this["desc"]);
				else
					return "@property ".$type."[] \$" . $this->name() . " " . ClassGen::trim($this["desc"]);
			}else{
				if ($this->hasClass()) 
					return "@property-read ".$this["class_abbr"]." \$" . $this->name() . " " . ClassGen::trim($this["desc"]);
				else
					return "@property ".$type." \$" . $this->name() . " " . ClassGen::trim($this["desc"]);
			}
		}
		
		public function generateField()
		{
			if ($this->isMulti())
				return 'private $_' . $this->name() .';';
			else
				return 'private $_' . $this->name() .';';
		}
		
		public function generateConstructor()
		{
			
			if ($this->isConst())
				return false;
			
			if ($this->isMulti())
				return "\$this->_".$this->name(). " = new \\Word\\PropertyArray(\$this);";
			else if ($this->hasClass())
				return "\$this->_".$this->name(). " = new \\Word\\PropertyObject('\\Word\\Objects\\" . $this["class_abbr"] . "', \"".$this["oname"]."\", \$this);";
			else return false;
		}
		
	}

	class ClassGen
	{
		private $_doc;
		private $_url;
		public $_idx;
		
		public function name_abbr()
		{
			$n = substr($this->name(), strpos($this->name(), "_") + 1);
			$n = str_replace("-", "_", $n);
			switch (strtolower($n))
			{
				case "object": // Object ja esta ocupat
				case "empty":
				case "default":
				case "continue":
				case "switch":
				case "true":
				case "false":
				case "if":
				case "else":
				case "while":
				case "for":
				case "endif":
				case "endfor":
				case "endforeach":
				case "endwhile":
				case "null":
				case "array":
				case "echo":
					$n .= "_";
					break;
			}
			return $n;
		}
		
		public static function getURL($url)
		{
			
			$tmp = dirname(__FILE__) . "/files/tmp/" . basename($url);
			if (is_file($tmp)) return file_get_contents($tmp);
			
			global $curl;
			if (!isset($curl))
			{
				$curl = OU_CURL::Create();
				$curl->userAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36";
				$curl->referer = "http://www.schemacentral.com/sc/ooxml/e-w_styles.html";
				$curl->connectTimeout = 100;
					
				curl_setopt($curl->_handle, CURLOPT_HTTPHEADER, array(
					"Connection: keep-alive",
					"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
					"Accept-Language: es,ca;q=0.8",
					"Accept-Encoding: deflate,sdch",
					"Host: www.schemacentral.com",
					"Pragma: no-cache",
					"Cache-Control: max-age=0"
				));
			}
			$c = $curl->Get($url);
			$c = mb_convert_encoding($c, "UTF-8", "US-ASCII");
			$c = str_replace("US-ASCII", "UTF-8", $c);
			
			file_put_contents($tmp, $c);
			
			return $c;
		}
		
		private static $_tmpResolve = array();
		public static function resolve($url, $idx = 0)
		{
			// if ($idx > 2) return false;
			
			if (isset(self::$_tmpResolve[$url])) return self::$_tmpResolve[$url];
			
			// $c = file_get_contents($url);
			$c = self::getURL($url);
			
			if (preg_match('/\<b\>Type\: \<\/b\>\<a href\=\"(.*?)\"/', $c, $m))
			{
				$n = self::getclass("http://www.schemacentral.com/sc/ooxml/" . $m[1], $idx+1);
				self::$_tmpResolve[$url] = $n;
			}else{
				$n = false;
				self::$_tmpResolve[$url] = $n;
			}
			
			return $n;
		}
		
		private static $_tmpClass= array();
		private static $_tmpClassPend = array();
		public static function getclass($url, $idx = 0)
		{
			if (isset(self::$_tmpClassPend[$url])) return self::$_tmpClassPend[$url];
			if (isset(self::$_tmpClass[$url])) return self::$_tmpClass[$url];
			$n = new ClassGen($url, $idx);
			self::$_tmpClassPend[$url] = $n;
			if (substr($n->oname(), 0, 2) == "w:")
				$n->save();
			self::$_tmpClass[$url] = $n;
			unset(self::$_tmpClassPend[$url]);
			return $n;
		}
		
		public static function debug($file, $str = "", $tabs = 0)
		{
			$t = str_repeat("\t", $tabs);
			echo "<pre>";
			echo "$t/".str_repeat("*", 8)."\n";
			echo "$t * " . $file . " (" . filesize($file) . ")\n";
			if ($str) echo "$t * " . $str . "\n";
			echo "$t ".str_repeat("*", 8)."/\n";
			echo "</pre>";
			@flush(); @ob_flush();
		}
		
		public function __construct($url, $idx = 0)
		{
			$this->_url = $url;
			$this->_idx = $idx;
			
			if (false)
			{
				$c = file_get_contents($url);
			}else{
				$c = self::getURL($url);
			}
			$this->_doc = phpQuery::newDocument($c);
		}
		
		private function pq($a)
		{
			return pq($a, $this->_doc);
		}
		
		public function ns()
		{
			$a = $this->pq("b:contains('Namespace: ')")->parent();
			if (preg_match('/^Namespace\s*\:\s*(.*?)$/', $a->text(), $m))
			{
				return $m[1];
			}else{
				return false;
			}
		}
		
		public function oname()
		{
			return $this->pq("h1")->text();
		}
		
		public function name()
		{ 
			$n = $this->pq("h1")->text();
			$n = substr($n, strpos($n, ":") + 1);
			$n = str_replace("-", "_", $n);
			switch (strtolower($n))
			{
				case "empty":
				case "default":
				case "continue":
				case "switch":
				case "true":
				case "false":
				case "if":
				case "else":
				case "while":
				case "for":
				case "endif":
				case "endfor":
				case "endforeach":
				case "endwhile":
				case "null":
				case "array":
				case "echo":
					$n .= "_";
					break;
			}
			return $n;
		}
		
		public function desc()
		{
			return $this->pq("pre.d")->text();
		}
		
		public function namespac()
		{
			$tbl = $this->pq("h2:first")->next();
			return substr($tbl->find("p:first")->text(), strlen('Namespace: '));
		}
		
		public static function trim($s)
		{
			$s = trim($s, "\n\r\t ".chr(0xC2).chr(0xA0));
			//$s = str_replace(chr(0xA0), " ", $s);
			return $s;
		}
		
		public function childrens()
		{
			
			$tbl = $this->pq("h2:first")->next();
			$code_content = $tbl->find("ul:first");
			
			$content = array();
			$names = array();
			foreach ($code_content->find("li > a") as $a)
			{
				$li = pq($a)->parent();
				if (preg_match("/^xsd:(\w+)$/", $this->pq($li)->text(), $m))
				{
					$field = new ClassGenField($this, array(
						"is_content" => true,
						"oname" => "content",
						"required" => false,
						"multi" => false,
						"desc" => "Element content",
						"link" => false
					));
					if (!$field->ignoreField())
						$content[] = $field;
				}else
				if (preg_match("/^(\w\:\w+)[\s\t]*(\[\d+\.\.[\d+\*]\]|)(.*?)$/", $this->pq($li)->text(), $m))
				{
					
					// Check in group choice
					$xa = $li->parents("li");
					if ($xa->length() > 0) {
						$xa = $xa->get(0);
						$xa = $this->pq($xa);
						if (preg_match("/^Choice[\n\r\t\s]*(\[\d+\.\.[\d+\*]\])/", $xa->text(), $m2))
						{
							if (!isset($m[2]) || !$m[2])
								$m[2] = $m2[1];
							
						}
					}
					
					if (isset($names[$m[1]])) continue; $names[$m[1]] = "";
					
					$field = new ClassGenField($this, array(
						"is_content" => false,
						"oname" => self::trim($m[1]),
						"required" => $m[2] == "[1..1]" || $m[2] == "[1..*]",
						"multi" => $m[2] == "[0..*]" || $m[2] == "[1..*]",
						"desc" => trim($m[3]),
						"link" => "http://www.schemacentral.com/sc/ooxml/" . $this->pq($li)->children("a")->attr("href")
					));
					if (!$field->ignoreField())
						$content[] = $field;
					
				}else{
					echo "<pre>/** error **/ \n // " . $this->_url."\n // " . $this->name()."\n // " . $this->pq($li)->text() . "\n</pre>";
				}
			}
			
			foreach ($content as &$c)
			{
				$c->resolve();
			}
			
			return $content;
			
		}
		
		public function attributes()
		{
			
			$tbl = $this->pq("h2:first")->next();
			$code_attrs = $tbl->find("ul:first")->next()->next();
			
			$attrs = array();
			foreach ($code_attrs->find("tr")->not($code_attrs->find("tr:first")) as $tr)
			{
				$td = $this->pq($tr)->find("td");
				
				$c = trim($this->pq($td->get(1))->text());
				$field = new ClassGenField($this, array(
						"is_content" => false,
						"oname" => self::trim($this->pq($td->get(0))->text()),
						"required" => $c == "[1..1]" || $c == "[1..*]",
						"multi" => $c == "[0..*]" || $c == "[1..*]",
						"type" => substr($this->pq($td->get(2))->text(), 2),
						"desc" => $this->pq($td->get(3))->text(),
						"link" => "http://www.schemacentral.com/sc/ooxml/" . $this->pq($td)->find("a")->attr("href")
				));
				if (!$field->ignoreField())
					$attrs[] = $field;
			}
			
			foreach ($attrs as &$c)
			{
				$c->resolve();
			}
			
			return $attrs;
			
		}
		
		public function save()
		{
			$path = dirname(__FILE__) . "/files/";
			
			if ($this->isConst())
				$path .= "consts/";
			else
				$path .= "objects/";				
			
			$path .= $this->name_abbr() . ".php";
			file_put_contents($path, $this->generate());
			
			self::debug(realpath($path), $this->_url . " " . $this->_idx . "x", $this->_idx);
						
		}
		
		public function isConst()
		{
			return substr($this->name(), 0, 2) == "ST";
		}
		
		public function generate()
		{
			
			$is_class = !$this->isConst();
			
			if ($is_class)
				$code = file_get_contents(dirname(__FILE__) . "/tpl/class.php");
			else
				$code = file_get_contents(dirname(__FILE__) . "/tpl/const.php");
			
			
			$code = str_replace("%class_name%", $this->name(), $code);
			$code = str_replace("%desc%", $this->desc(), $code);
			$code = str_replace("%link%", $this->_url, $code);
			
			$abbr_name = $this->name_abbr();
			$code = str_replace("%class_name_abbr%", $abbr_name, $code);
			
			if ($is_class)
			{
				
				$code = str_replace(
						"%ns%",
						'return ' . ($this->ns() ? '"' . $this->ns() .'"' : 'false') . ';',
						$code
				);
					
				$childrens2 = $this->childrens();
				$attrs2 = $this->attributes();
				
				$attrs = array();
				$names = array();
				foreach ($attrs2 as $k)
				{
					if (isset($names[$k->name()])) continue;
					$attrs[] = $k;
					$names[$k->name()] = "";
				}
				
				$childrens = array();
				foreach ($childrens2 as $k)
				{
					if (isset($names[$k->name()])) continue;
					$childrens[] = $k;
					$names[$k->name()] = "";
				}
				

				// Custom
				$custom = "";
				if ($this->name_abbr() == "Empty_")
				{
						
					$custom .= "\t\tpublic function add() {\n";
					$custom .= "\t\t\t" . '$this->propOptions["forced"] = true;' . "\n";
					$custom .= "\t\t}\n";
						
					$custom .= "\t\tpublic function remove() {\n";
					$custom .= "\t\t\t" . '$this->propOptions["forced"] = false;' . "\n";
					$custom .= "\t\t}\n";
						
				}
				
				/*if (count($attrs) > 0)
				{
					$a = array();
					foreach ($attrs as $attr) $a[] = '$' . $attr->name() . ' = null';
					$custom .= "\t\tpublic function setAttrs(".implode(", ", $a).") {\n";
					foreach ($attrs as $attr)
						$custom .= "\t\t\t" . '$this->_'.$attr->name().' = $' . $attr->name() . ';' . "\n";
					$custom .= "\t\t}\n";
				} */
				
				$code = str_replace("%custom%", $custom, $code);
				
				
				// constructor
				
				$constr = "";
				foreach ($childrens as $c) { $str = $c->generateConstructor(); if ($str) $constr .= "\n\t\t\t" . $str ; }
				foreach ($attrs as $c) { $str = $c->generateConstructor(); if ($str) $constr .= "\n\t\t\t" . $str ; }
				
				$code = str_replace("%constr%", $constr, $code);
				
				// @properties
				
				$props = "";
				foreach ($childrens as $c) { $props .= "\n\t * ".$c->generateProp().""; }
				foreach ($attrs as $c) { $props .= "\n\t * ".$c->generateProp().""; }
				$code = str_replace("%props%", $props, $code);
				
				// Includes
				
				$incs = "";
				$incs_a = array();
				foreach ($childrens as $c) 
					if ($c->hasClass() && !isset($incs_a[$c["class_abbr"]])) {
						$t = $c->isConst() ? "Consts" : "Objects"; 
						$incs .= ",\n\t\t\t\t\t" . '"'.$t.'\\'.$c["class_abbr"].'"'; 
						$incs_a[$c["class_abbr"]] = ""; 
					}
				foreach ($attrs as $c) 
					if ($c->hasClass() && !isset($incs_a[$c["class_abbr"]])) {
						$t = $c->isConst() ? "Consts" : "Objects";
						$incs .= ",\n\t\t\t\t\t" . '"'.$t.'\\'.$c["class_abbr"].'"'; 
						$incs_a[$c["class_abbr"]] = ""; 
					}
				$code = str_replace("%incs%", $incs, $code);
				
				// Fields
				
				$fnc = function($n, $a) { if (count($a) == 0) return ""; $fields = "\t\t/* --- $n --- */\n\n"; foreach ($a as $a2) $fields .= "\t\t" . $a2->generateField() ."\n"; return $fields . "\n"; };
				$fields = "";
				$fields .= $fnc("Attributes", $attrs); 
				$fields .= $fnc("Childrens", $childrens);
				$code = str_replace("%fields%", $fields, $code);
				
				$fnc = function($n, $a) {
					$fields = ""; 
					if (count($a) == 0) return ""; $fields = "\t\t/* --- $n --- */\n\n";
					foreach ($a as $attr) {
						$fields .= "\t\t" . $attr->generateGetter() . "\n";
						if ($attr->hasSetter()) 
							$fields .= "\t\t" . $attr->generateSetter() . "\n";
						if ($attr->isMulti())
						{
							if ($attr->hasClass() && !$attr->isConst())
								$fields .= "\t\t" . $attr->generateAddCreate() . "\n";
								
						}
						$fields .= "\n";
					}
					return $fields; 
				};
				
				$fields = "";
				$fields .= $fnc("Attributes", $attrs); 
				$fields .= $fnc("Childrens", $childrens);
				$code = str_replace("%fields_funcs%", $fields, $code);
					
				// Empty
				
				$empty = "";
				$fnc = function($n, $a) { 
					if (count($a) == 0) return "";
					$empty = "\t\t\t/* --- $n --- */\n\n"; 
					foreach ($a as $a2) 
						$empty .= "\t\t\t\t" . $a2->generateEmpty() . ' && '."\n";
					return $empty . "\n";
					 
				};
				$empty .= $fnc("Attributes", $attrs);
				foreach ($childrens as $c)
				{
					if ($c["is_content"])
						$empty .= "\t\t\t\t" . $c->generateEmpty() . ' && '."\n";
				}
				// $empty .= $fnc("Childrens", $childrens); // Es guarda a _array
				$code = str_replace("%empty%", $empty, $code);
				
				// Validate
				
				$validate = "";
				$fnc = function($n, $a) { 
					if (count($a) == 0) return "";
					$first = false;
					$validate = "";
					foreach ($a as $a2)
					{
						if ($first) $validate .= "\t\t\t/* --- $n --- */\n\n"; 
						if ($a2["required"]) {
							$first = true; 
							$validate .= "\t\t\t\t" . $a2->generateValidate() . ' && '."\n";
						}
					}
					return $validate . ($first ? "\n" : "");
					 
				};
				$validate .= $fnc("Attributes", $attrs);
				foreach ($childrens as $c)
				{
					if ($c["is_content"])
						$validate .= "\t\t\t\t" . $c->generateValidate() . ' && '."\n";
				}
				// $validate .= $fnc("Childrens", $childrens); // Es guarda a _array
				$code = str_replace("%validate%", $validate, $code);
				
				// write
				$write = "";
				$fnc = function($n, $a) {
					if (count($a) == 0) return "";
					$write = "\t\t\t/* --- $n --- */\n\n";
					foreach ($a as $a2)
						$write .= "\t\t\t" . $a2->generateWrite()."\n";
					return $write . "\n";
				};
				$write .= $fnc("Attributes", $attrs);
				foreach ($childrens as $c)
				{
					if ($c["is_content"])
						$write .= "\t\t\t" . $c->generateWrite()."\n";
				}
				// $write .= $fnc("Childrens", $childrens); // Es guarda a _array
				$code = str_replace("%write%", $write, $code);
				
				
			
			}else{ // Const
				
				$str = "";
				$consts = $this->pq("body ul table.en tr");
				if ($consts->length() > 1)
				{
					for ($i = 1; $i < $consts->length(); $i++)
					{
						
						$o = pq($consts->get($i));
						$tds = $o->find("td");
						
						$value = pq($tds->get(0))->text();
						$name = $value;
						
						$name = str_replace("-", "_", $name);
						
						switch (strtolower($name))
						{
							case "empty":
							case "default":
							case "continue":
							case "switch":
							case "true":
							case "false":
							case "if":
							case "else":
							case "while":
							case "for":
							case "endif":
							case "endfor":
							case "endforeach":
							case "endwhile":
							case "null":
							case "array":
							case "echo":
								$name .= "_";
								break;
							default:
								if (is_numeric($name)) $name = "Value_" . $name;
						}
						
						
						
						$name = ucfirst($name);
						$desc = pq($tds->get(1))->text();
						
						$str .= "\t\t/**\n";
						$str .= "\t\t * $desc\n";
						$str .= "\t\t * @var string\n";
						$str .= "\t\t */\n";
						$str .= "\t\tconst $name = \"".addslashes($value)."\";\n\n";
						
					}
				}
				
				$code = str_replace("%consts%", $str, $code);
				
			}
				
			
			return $code;			
		}
		
	}

?>