<?php 

namespace Word\Consts
{

	/**
	 * Emphasis Mark Type
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_Em.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class Em
	{
		
		/**
		 * No Emphasis Mark
		 * @var string
		 */
		const None = "none";

		/**
		 * Dot Emphasis Mark Above Characters
		 * @var string
		 */
		const Dot = "dot";

		/**
		 * Comma Emphasis Mark Above Characters
		 * @var string
		 */
		const Comma = "comma";

		/**
		 * Circle Emphasis Mark Above Characters
		 * @var string
		 */
		const Circle = "circle";

		/**
		 * Dot Emphasis Mark Below Characters
		 * @var string
		 */
		const UnderDot = "underDot";


		
	}
	
}

?>