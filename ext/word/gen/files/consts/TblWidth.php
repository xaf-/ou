<?php 

namespace Word\Consts
{

	/**
	 * Table Width Units
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_TblWidth.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class TblWidth
	{
		
		/**
		 * No Width
		 * @var string
		 */
		const Nil = "nil";

		/**
		 * Width in Fiftieths of a Percent
		 * @var string
		 */
		const Pct = "pct";

		/**
		 * Width in Twentieths of a Point
		 * @var string
		 */
		const Dxa = "dxa";

		/**
		 * Automatically Determined Width
		 * @var string
		 */
		const Auto = "auto";


		
	}
	
}

?>