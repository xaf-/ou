<?php 

namespace Word\Consts
{

	/**
	 * Color Value
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_HexColor.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class HexColor
	{
		
		/**
		 * Automatically Determined Color
		 * @var string
		 */
		const Auto = "auto";


		
	}
	
}

?>