<?php 

namespace Word\Consts
{

	/**
	 * Table Overlap Setting
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_TblOverlap.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class TblOverlap
	{
		
		/**
		 * Floating Table Cannot Overlap
		 * @var string
		 */
		const Never = "never";

		/**
		 * Floating Table Can Overlap
		 * @var string
		 */
		const Overlap = "overlap";


		
	}
	
}

?>