<?php 

namespace Word\Consts
{

	/**
	 * Vertical Alignment Type
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_VerticalJc.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class VerticalJc
	{
		
		/**
		 * Align Top
		 * @var string
		 */
		const Top = "top";

		/**
		 * Align Center
		 * @var string
		 */
		const Center = "center";

		/**
		 * Vertical Justification
		 * @var string
		 */
		const Both = "both";

		/**
		 * Align Bottom
		 * @var string
		 */
		const Bottom = "bottom";


		
	}
	
}

?>