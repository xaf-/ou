<?php 

namespace Word\Consts
{

	/**
	 * Absolute Position Tab Leader Character
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_PTabLeader.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class PTabLeader
	{
		
		/**
		 * No Leader Character
		 * @var string
		 */
		const None = "none";

		/**
		 * Dot Leader Character
		 * @var string
		 */
		const Dot = "dot";

		/**
		 * Hyphen Leader Character
		 * @var string
		 */
		const Hyphen = "hyphen";

		/**
		 * Underscore Leader Character
		 * @var string
		 */
		const Underscore = "underscore";

		/**
		 * Centered Dot Leader Character
		 * @var string
		 */
		const MiddleDot = "middleDot";


		
	}
	
}

?>