<?php 

namespace Word\Consts
{

	/**
	 * Merged Cell Type
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_Merge.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class Merge
	{
		
		/**
		 * Continue Merged Region
		 * @var string
		 */
		const Continue_ = "continue";

		/**
		 * Start/Restart Merged Region
		 * @var string
		 */
		const Restart = "restart";


		
	}
	
}

?>