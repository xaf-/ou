<?php 

namespace Word\Consts
{

	/**
	 * Horizontal Alignment Location
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_XAlign.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class XAlign
	{
		
		/**
		 * Left Aligned Horizontally
		 * @var string
		 */
		const Left = "left";

		/**
		 * Centered Horizontally
		 * @var string
		 */
		const Center = "center";

		/**
		 * Right Aligned Horizontally
		 * @var string
		 */
		const Right = "right";

		/**
		 * Inside
		 * @var string
		 */
		const Inside = "inside";

		/**
		 * Outside
		 * @var string
		 */
		const Outside = "outside";


		
	}
	
}

?>