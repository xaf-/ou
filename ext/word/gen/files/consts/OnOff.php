<?php 

namespace Word\Consts
{

	/**
	 * On/Off Value
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_OnOff.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class OnOff
	{
		
		/**
		 * True
		 * @var string
		 */
		const True_ = "true";

		/**
		 * False
		 * @var string
		 */
		const False_ = "false";

		/**
		 * True
		 * @var string
		 */
		const On = "on";

		/**
		 * False
		 * @var string
		 */
		const Off = "off";

		/**
		 * False
		 * @var string
		 */
		const Value_0 = "0";

		/**
		 * True
		 * @var string
		 */
		const Value_1 = "1";


		
	}
	
}

?>