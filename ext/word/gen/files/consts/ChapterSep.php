<?php 

namespace Word\Consts
{

	/**
	 * Chapter Separator Types
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_ChapterSep.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class ChapterSep
	{
		
		/**
		 * Hyphen Chapter Separator
		 * @var string
		 */
		const Hyphen = "hyphen";

		/**
		 * Period Chapter Separator
		 * @var string
		 */
		const Period = "period";

		/**
		 * Colon Chapter Separator
		 * @var string
		 */
		const Colon = "colon";

		/**
		 * Em Dash Chapter Separator
		 * @var string
		 */
		const EmDash = "emDash";

		/**
		 * En Dash Chapter Separator
		 * @var string
		 */
		const EnDash = "enDash";


		
	}
	
}

?>