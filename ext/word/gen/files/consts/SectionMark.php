<?php 

namespace Word\Consts
{

	/**
	 * Section Type
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_SectionMark.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class SectionMark
	{
		
		/**
		 * Next Page Section Break
		 * @var string
		 */
		const NextPage = "nextPage";

		/**
		 * Column Section Break
		 * @var string
		 */
		const NextColumn = "nextColumn";

		/**
		 * Continuous Section Break
		 * @var string
		 */
		const Continuous = "continuous";

		/**
		 * Even Page Section Break
		 * @var string
		 */
		const EvenPage = "evenPage";

		/**
		 * Odd Page Section Break
		 * @var string
		 */
		const OddPage = "oddPage";


		
	}
	
}

?>