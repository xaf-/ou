<?php 

namespace Word\Consts
{

	/**
	 * Date Storage Format Types
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_SdtDateMappingType.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class SdtDateMappingType
	{
		
		/**
		 * Same As Display
		 * @var string
		 */
		const Text = "text";

		/**
		 * XML Schema Date Format
		 * @var string
		 */
		const Date = "date";

		/**
		 * XML Schema DateTime Format
		 * @var string
		 */
		const DateTime = "dateTime";


		
	}
	
}

?>