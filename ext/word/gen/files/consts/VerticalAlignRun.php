<?php 

namespace Word\Consts
{

	/**
	 * Vertical Positioning Location
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_VerticalAlignRun.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class VerticalAlignRun
	{
		
		/**
		 * Regular Vertical Positioning
		 * @var string
		 */
		const Baseline = "baseline";

		/**
		 * Superscript
		 * @var string
		 */
		const Superscript = "superscript";

		/**
		 * Subscript
		 * @var string
		 */
		const Subscript = "subscript";


		
	}
	
}

?>