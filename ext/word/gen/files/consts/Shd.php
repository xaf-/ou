<?php 

namespace Word\Consts
{

	/**
	 * Shading Patterns
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_Shd.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class Shd
	{
		
		/**
		 * No Pattern
		 * @var string
		 */
		const Nil = "nil";

		/**
		 * No Pattern
		 * @var string
		 */
		const Clear = "clear";

		/**
		 * 100% Fill Pattern
		 * @var string
		 */
		const Solid = "solid";

		/**
		 * Horizontal Stripe Pattern
		 * @var string
		 */
		const HorzStripe = "horzStripe";

		/**
		 * Vertical Stripe Pattern
		 * @var string
		 */
		const VertStripe = "vertStripe";

		/**
		 * Reverse Diagonal Stripe Pattern
		 * @var string
		 */
		const ReverseDiagStripe = "reverseDiagStripe";

		/**
		 * Diagonal Stripe Pattern
		 * @var string
		 */
		const DiagStripe = "diagStripe";

		/**
		 * Horizontal Cross Pattern
		 * @var string
		 */
		const HorzCross = "horzCross";

		/**
		 * Diagonal Cross Pattern
		 * @var string
		 */
		const DiagCross = "diagCross";

		/**
		 * Thin Horizontal Stripe Pattern
		 * @var string
		 */
		const ThinHorzStripe = "thinHorzStripe";

		/**
		 * Thin Vertical Stripe Pattern
		 * @var string
		 */
		const ThinVertStripe = "thinVertStripe";

		/**
		 * Thin Reverse Diagonal Stripe Pattern
		 * @var string
		 */
		const ThinReverseDiagStripe = "thinReverseDiagStripe";

		/**
		 * Thin Diagonal Stripe Pattern
		 * @var string
		 */
		const ThinDiagStripe = "thinDiagStripe";

		/**
		 * Thin Horizontal Cross Pattern
		 * @var string
		 */
		const ThinHorzCross = "thinHorzCross";

		/**
		 * Thin Diagonal Cross Pattern
		 * @var string
		 */
		const ThinDiagCross = "thinDiagCross";

		/**
		 * 5% Fill Pattern
		 * @var string
		 */
		const Pct5 = "pct5";

		/**
		 * 10% Fill Pattern
		 * @var string
		 */
		const Pct10 = "pct10";

		/**
		 * 12.5% Fill Pattern
		 * @var string
		 */
		const Pct12 = "pct12";

		/**
		 * 15% Fill Pattern
		 * @var string
		 */
		const Pct15 = "pct15";

		/**
		 * 20% Fill Pattern
		 * @var string
		 */
		const Pct20 = "pct20";

		/**
		 * 25% Fill Pattern
		 * @var string
		 */
		const Pct25 = "pct25";

		/**
		 * 30% Fill Pattern
		 * @var string
		 */
		const Pct30 = "pct30";

		/**
		 * 35% Fill Pattern
		 * @var string
		 */
		const Pct35 = "pct35";

		/**
		 * 37.5% Fill Pattern
		 * @var string
		 */
		const Pct37 = "pct37";

		/**
		 * 40% Fill Pattern
		 * @var string
		 */
		const Pct40 = "pct40";

		/**
		 * 45% Fill Pattern
		 * @var string
		 */
		const Pct45 = "pct45";

		/**
		 * 50% Fill Pattern
		 * @var string
		 */
		const Pct50 = "pct50";

		/**
		 * 55% Fill Pattern
		 * @var string
		 */
		const Pct55 = "pct55";

		/**
		 * 60% Fill Pattern
		 * @var string
		 */
		const Pct60 = "pct60";

		/**
		 * 62.5% Fill Pattern
		 * @var string
		 */
		const Pct62 = "pct62";

		/**
		 * 65% Fill Pattern
		 * @var string
		 */
		const Pct65 = "pct65";

		/**
		 * 70% Fill Pattern
		 * @var string
		 */
		const Pct70 = "pct70";

		/**
		 * 75% Fill Pattern
		 * @var string
		 */
		const Pct75 = "pct75";

		/**
		 * 80% Fill Pattern
		 * @var string
		 */
		const Pct80 = "pct80";

		/**
		 * 85% Fill Pattern
		 * @var string
		 */
		const Pct85 = "pct85";

		/**
		 * 87.5% Fill Pattern
		 * @var string
		 */
		const Pct87 = "pct87";

		/**
		 * 90% Fill Pattern
		 * @var string
		 */
		const Pct90 = "pct90";

		/**
		 * 95% Fill Pattern
		 * @var string
		 */
		const Pct95 = "pct95";


		
	}
	
}

?>