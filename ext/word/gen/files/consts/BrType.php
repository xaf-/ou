<?php 

namespace Word\Consts
{

	/**
	 * Break Types
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_BrType.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class BrType
	{
		
		/**
		 * Page Break
		 * @var string
		 */
		const Page = "page";

		/**
		 * Column Break
		 * @var string
		 */
		const Column = "column";

		/**
		 * Line Break
		 * @var string
		 */
		const TextWrapping = "textWrapping";


		
	}
	
}

?>