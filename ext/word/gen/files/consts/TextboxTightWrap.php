<?php 

namespace Word\Consts
{

	/**
	 * Lines To Tight Wrap Within Text Box
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_TextboxTightWrap.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class TextboxTightWrap
	{
		
		/**
		 * Do Not Tight Wrap
		 * @var string
		 */
		const None = "none";

		/**
		 * Tight Wrap All Lines
		 * @var string
		 */
		const AllLines = "allLines";

		/**
		 * Tight Wrap First and Last Lines
		 * @var string
		 */
		const FirstAndLastLine = "firstAndLastLine";

		/**
		 * Tight Wrap First Line
		 * @var string
		 */
		const FirstLineOnly = "firstLineOnly";

		/**
		 * Tight Wrap Last Line
		 * @var string
		 */
		const LastLineOnly = "lastLineOnly";


		
	}
	
}

?>