<?php 

namespace Word\Consts
{

	/**
	 * Text Highlight Colors
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_HighlightColor.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class HighlightColor
	{
		
		/**
		 * Black Highlighting Color
		 * @var string
		 */
		const Black = "black";

		/**
		 * Blue Highlighting Color
		 * @var string
		 */
		const Blue = "blue";

		/**
		 * Cyan Highlighting Color
		 * @var string
		 */
		const Cyan = "cyan";

		/**
		 * Green Highlighting Color
		 * @var string
		 */
		const Green = "green";

		/**
		 * Magenta Highlighting Color
		 * @var string
		 */
		const Magenta = "magenta";

		/**
		 * Red Highlighting Color
		 * @var string
		 */
		const Red = "red";

		/**
		 * Yellow Highlighting Color
		 * @var string
		 */
		const Yellow = "yellow";

		/**
		 * White Highlighting Color
		 * @var string
		 */
		const White = "white";

		/**
		 * Dark Blue Highlighting Color
		 * @var string
		 */
		const DarkBlue = "darkBlue";

		/**
		 * Dark Cyan Highlighting Color
		 * @var string
		 */
		const DarkCyan = "darkCyan";

		/**
		 * Dark Green Highlighting Color
		 * @var string
		 */
		const DarkGreen = "darkGreen";

		/**
		 * Dark Magenta Highlighting Color
		 * @var string
		 */
		const DarkMagenta = "darkMagenta";

		/**
		 * Dark Red Highlighting Color
		 * @var string
		 */
		const DarkRed = "darkRed";

		/**
		 * Dark Yellow Highlighting Color
		 * @var string
		 */
		const DarkYellow = "darkYellow";

		/**
		 * Dark Gray Highlighting Color
		 * @var string
		 */
		const DarkGray = "darkGray";

		/**
		 * Light Gray Highlighting Color
		 * @var string
		 */
		const LightGray = "lightGray";

		/**
		 * No Text Highlighting
		 * @var string
		 */
		const None = "none";


		
	}
	
}

?>