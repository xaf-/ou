<?php 

namespace Word\Consts
{

	/**
	 * Absolute Position Tab Positioning Base
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_PTabRelativeTo.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class PTabRelativeTo
	{
		
		/**
		 * Relative To Text Margins
		 * @var string
		 */
		const Margin = "margin";

		/**
		 * Relative To Indents
		 * @var string
		 */
		const Indent = "indent";


		
	}
	
}

?>