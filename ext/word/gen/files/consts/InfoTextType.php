<?php 

namespace Word\Consts
{

	/**
	 * Help or Status Text Type
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_InfoTextType.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class InfoTextType
	{
		
		/**
		 * Literal Text
		 * @var string
		 */
		const Text = "text";

		/**
		 * Glossary Document Entry
		 * @var string
		 */
		const AutoText = "autoText";


		
	}
	
}

?>