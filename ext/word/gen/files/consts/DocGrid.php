<?php 

namespace Word\Consts
{

	/**
	 * Document Grid Types
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_DocGrid.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class DocGrid
	{
		
		/**
		 * No Document Grid
		 * @var string
		 */
		const Default_ = "default";

		/**
		 * Line Grid Only
		 * @var string
		 */
		const Lines = "lines";

		/**
		 * Line and Character Grid
		 * @var string
		 */
		const LinesAndChars = "linesAndChars";

		/**
		 * Character Grid Only
		 * @var string
		 */
		const SnapToChars = "snapToChars";


		
	}
	
}

?>