<?php 

namespace Word\Consts
{

	/**
	 * Line Numbering Restart Position
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_LineNumberRestart.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class LineNumberRestart
	{
		
		/**
		 * Restart Line Numbering on Each Page
		 * @var string
		 */
		const NewPage = "newPage";

		/**
		 * Restart Line Numbering for Each Section
		 * @var string
		 */
		const NewSection = "newSection";

		/**
		 * Continue Line Numbering From Previous Section
		 * @var string
		 */
		const Continuous = "continuous";


		
	}
	
}

?>