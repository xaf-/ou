<?php 

namespace Word\Consts
{

	/**
	 * Conditional Table Style Formatting Types
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_TblStyleOverrideType.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class TblStyleOverrideType
	{
		
		/**
		 * Whole table formatting
		 * @var string
		 */
		const WholeTable = "wholeTable";

		/**
		 * First Row Conditional Formatting
		 * @var string
		 */
		const FirstRow = "firstRow";

		/**
		 * Last table row formatting
		 * @var string
		 */
		const LastRow = "lastRow";

		/**
		 * First Column Conditional Formatting
		 * @var string
		 */
		const FirstCol = "firstCol";

		/**
		 * Last table column formatting
		 * @var string
		 */
		const LastCol = "lastCol";

		/**
		 * Banded Column Conditional Formatting
		 * @var string
		 */
		const Band1Vert = "band1Vert";

		/**
		 * Even Column Stripe Conditional Formatting
		 * @var string
		 */
		const Band2Vert = "band2Vert";

		/**
		 * Banded Row Conditional Formatting
		 * @var string
		 */
		const Band1Horz = "band1Horz";

		/**
		 * Even Row Stripe Conditional Formatting
		 * @var string
		 */
		const Band2Horz = "band2Horz";

		/**
		 * Top right table cell formatting
		 * @var string
		 */
		const NeCell = "neCell";

		/**
		 * Top left table cell formatting
		 * @var string
		 */
		const NwCell = "nwCell";

		/**
		 * Bottom right table cell formatting
		 * @var string
		 */
		const SeCell = "seCell";

		/**
		 * Bottom left table cell formatting
		 * @var string
		 */
		const SwCell = "swCell";


		
	}
	
}

?>