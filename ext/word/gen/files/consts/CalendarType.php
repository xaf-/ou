<?php 

namespace Word\Consts
{

	/**
	 * Calendar Types
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_CalendarType.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class CalendarType
	{
		
		/**
		 * Gregorian
		 * @var string
		 */
		const Gregorian = "gregorian";

		/**
		 * Hijri
		 * @var string
		 */
		const Hijri = "hijri";

		/**
		 * Hebrew
		 * @var string
		 */
		const Hebrew = "hebrew";

		/**
		 * Taiwan
		 * @var string
		 */
		const Taiwan = "taiwan";

		/**
		 * Japanese Emperor Era
		 * @var string
		 */
		const Japan = "japan";

		/**
		 * Thai
		 * @var string
		 */
		const Thai = "thai";

		/**
		 * Korean Tangun Era
		 * @var string
		 */
		const Korea = "korea";

		/**
		 * Saka Era
		 * @var string
		 */
		const Saka = "saka";

		/**
		 * Gregorian transliterated English
		 * @var string
		 */
		const GregorianXlitEnglish = "gregorianXlitEnglish";

		/**
		 * Gregorian transliterated French
		 * @var string
		 */
		const GregorianXlitFrench = "gregorianXlitFrench";


		
	}
	
}

?>