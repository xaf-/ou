<?php 

namespace Word\Consts
{

	/**
	 * Footnote/Endnote Numbering Restart Locations
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_RestartNumber.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class RestartNumber
	{
		
		/**
		 * Continue Numbering From Previous Section
		 * @var string
		 */
		const Continuous = "continuous";

		/**
		 * Restart Numbering For Each Section
		 * @var string
		 */
		const EachSect = "eachSect";

		/**
		 * Restart Numbering On Each Page
		 * @var string
		 */
		const EachPage = "eachPage";


		
	}
	
}

?>