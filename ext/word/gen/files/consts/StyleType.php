<?php 

namespace Word\Consts
{

	/**
	 * Style Types
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_StyleType.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class StyleType
	{
		
		/**
		 * Paragraph Style
		 * @var string
		 */
		const Paragraph = "paragraph";

		/**
		 * Character Style
		 * @var string
		 */
		const Character = "character";

		/**
		 * Table Style
		 * @var string
		 */
		const Table = "table";

		/**
		 * Numbering Style
		 * @var string
		 */
		const Numbering = "numbering";


		
	}
	
}

?>