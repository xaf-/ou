<?php 

namespace Word\Consts
{

	/**
	 * Page Border Positioning Base
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_PageBorderOffset.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class PageBorderOffset
	{
		
		/**
		 * Page Border Is Positioned Relative to Page Edges
		 * @var string
		 */
		const Page = "page";

		/**
		 * Page Border Is Positioned Relative to Text Extents
		 * @var string
		 */
		const Text = "text";


		
	}
	
}

?>