<?php 

namespace Word\Consts
{

	/**
	 * Line Spacing Rule
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_LineSpacingRule.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class LineSpacingRule
	{
		
		/**
		 * Automatically Determined Line Height
		 * @var string
		 */
		const Auto = "auto";

		/**
		 * Exact Line Height
		 * @var string
		 */
		const Exact = "exact";

		/**
		 * Minimum Line Height
		 * @var string
		 */
		const AtLeast = "atLeast";


		
	}
	
}

?>