<?php 

namespace Word\Consts
{

	/**
	 * Underline Patterns
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_Underline.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class Underline
	{
		
		/**
		 * Single Underline
		 * @var string
		 */
		const Single = "single";

		/**
		 * Underline Non-Space Characters Only
		 * @var string
		 */
		const Words = "words";

		/**
		 * Double Underline
		 * @var string
		 */
		const Double = "double";

		/**
		 * Thick Underline
		 * @var string
		 */
		const Thick = "thick";

		/**
		 * Dotted Underline
		 * @var string
		 */
		const Dotted = "dotted";

		/**
		 * Thick Dotted Underline
		 * @var string
		 */
		const DottedHeavy = "dottedHeavy";

		/**
		 * Dashed Underline
		 * @var string
		 */
		const Dash = "dash";

		/**
		 * Thick Dashed Underline
		 * @var string
		 */
		const DashedHeavy = "dashedHeavy";

		/**
		 * Long Dashed Underline
		 * @var string
		 */
		const DashLong = "dashLong";

		/**
		 * Thick Long Dashed Underline
		 * @var string
		 */
		const DashLongHeavy = "dashLongHeavy";

		/**
		 * Dash-Dot Underline
		 * @var string
		 */
		const DotDash = "dotDash";

		/**
		 * Thick Dash-Dot Underline
		 * @var string
		 */
		const DashDotHeavy = "dashDotHeavy";

		/**
		 * Dash-Dot-Dot Underline
		 * @var string
		 */
		const DotDotDash = "dotDotDash";

		/**
		 * Thick Dash-Dot-Dot Underline
		 * @var string
		 */
		const DashDotDotHeavy = "dashDotDotHeavy";

		/**
		 * Wave Underline
		 * @var string
		 */
		const Wave = "wave";

		/**
		 * Heavy Wave Underline
		 * @var string
		 */
		const WavyHeavy = "wavyHeavy";

		/**
		 * Double Wave Underline
		 * @var string
		 */
		const WavyDouble = "wavyDouble";

		/**
		 * No Underline
		 * @var string
		 */
		const None = "none";


		
	}
	
}

?>