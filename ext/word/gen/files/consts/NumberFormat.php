<?php 

namespace Word\Consts
{

	/**
	 * Numbering Format
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_NumberFormat.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class NumberFormat
	{
		
		/**
		 * Decimal Numbers
		 * @var string
		 */
		const Decimal = "decimal";

		/**
		 * Uppercase Roman Numerals
		 * @var string
		 */
		const UpperRoman = "upperRoman";

		/**
		 * Lowercase Roman Numerals
		 * @var string
		 */
		const LowerRoman = "lowerRoman";

		/**
		 * Uppercase Latin Alphabet
		 * @var string
		 */
		const UpperLetter = "upperLetter";

		/**
		 * Lowercase Latin Alphabet
		 * @var string
		 */
		const LowerLetter = "lowerLetter";

		/**
		 * Ordinal
		 * @var string
		 */
		const Ordinal = "ordinal";

		/**
		 * Cardinal Text
		 * @var string
		 */
		const CardinalText = "cardinalText";

		/**
		 * Ordinal Text
		 * @var string
		 */
		const OrdinalText = "ordinalText";

		/**
		 * Hexadecimal Numbering
		 * @var string
		 */
		const Hex = "hex";

		/**
		 * Chicago Manual of Style
		 * @var string
		 */
		const Chicago = "chicago";

		/**
		 * Ideographs
		 * @var string
		 */
		const IdeographDigital = "ideographDigital";

		/**
		 * Japanese Counting System
		 * @var string
		 */
		const JapaneseCounting = "japaneseCounting";

		/**
		 * AIUEO Order Hiragana
		 * @var string
		 */
		const Aiueo = "aiueo";

		/**
		 * Iroha Ordered Katakana
		 * @var string
		 */
		const Iroha = "iroha";

		/**
		 * Double Byte Arabic Numerals
		 * @var string
		 */
		const DecimalFullWidth = "decimalFullWidth";

		/**
		 * Single Byte Arabic Numerals
		 * @var string
		 */
		const DecimalHalfWidth = "decimalHalfWidth";

		/**
		 * Japanese Legal Numbering
		 * @var string
		 */
		const JapaneseLegal = "japaneseLegal";

		/**
		 * Japanese Digital Ten Thousand Counting System
		 * @var string
		 */
		const JapaneseDigitalTenThousand = "japaneseDigitalTenThousand";

		/**
		 * Decimal Numbers Enclosed in a Circle
		 * @var string
		 */
		const DecimalEnclosedCircle = "decimalEnclosedCircle";

		/**
		 * Double Byte Arabic Numerals Alternate
		 * @var string
		 */
		const DecimalFullWidth2 = "decimalFullWidth2";

		/**
		 * Full-Width AIUEO Order Hiragana
		 * @var string
		 */
		const AiueoFullWidth = "aiueoFullWidth";

		/**
		 * Full-Width Iroha Ordered Katakana
		 * @var string
		 */
		const IrohaFullWidth = "irohaFullWidth";

		/**
		 * Initial Zero Arabic Numerals
		 * @var string
		 */
		const DecimalZero = "decimalZero";

		/**
		 * Bullet
		 * @var string
		 */
		const Bullet = "bullet";

		/**
		 * Korean Ganada Numbering
		 * @var string
		 */
		const Ganada = "ganada";

		/**
		 * Korean Chosung Numbering
		 * @var string
		 */
		const Chosung = "chosung";

		/**
		 * Decimal Numbers Followed by a Period
		 * @var string
		 */
		const DecimalEnclosedFullstop = "decimalEnclosedFullstop";

		/**
		 * Decimal Numbers Enclosed in Parenthesis
		 * @var string
		 */
		const DecimalEnclosedParen = "decimalEnclosedParen";

		/**
		 * Decimal Numbers Enclosed in a Circle
		 * @var string
		 */
		const DecimalEnclosedCircleChinese = "decimalEnclosedCircleChinese";

		/**
		 * Ideographs Enclosed in a Circle
		 * @var string
		 */
		const IdeographEnclosedCircle = "ideographEnclosedCircle";

		/**
		 * Traditional Ideograph Format
		 * @var string
		 */
		const IdeographTraditional = "ideographTraditional";

		/**
		 * Zodiac Ideograph Format
		 * @var string
		 */
		const IdeographZodiac = "ideographZodiac";

		/**
		 * Traditional Zodiac Ideograph Format
		 * @var string
		 */
		const IdeographZodiacTraditional = "ideographZodiacTraditional";

		/**
		 * Taiwanese Counting System
		 * @var string
		 */
		const TaiwaneseCounting = "taiwaneseCounting";

		/**
		 * Traditional Legal Ideograph Format
		 * @var string
		 */
		const IdeographLegalTraditional = "ideographLegalTraditional";

		/**
		 * Taiwanese Counting Thousand System
		 * @var string
		 */
		const TaiwaneseCountingThousand = "taiwaneseCountingThousand";

		/**
		 * Taiwanese Digital Counting System
		 * @var string
		 */
		const TaiwaneseDigital = "taiwaneseDigital";

		/**
		 * Chinese Counting System
		 * @var string
		 */
		const ChineseCounting = "chineseCounting";

		/**
		 * Chinese Legal Simplified Format
		 * @var string
		 */
		const ChineseLegalSimplified = "chineseLegalSimplified";

		/**
		 * Chinese Counting Thousand System
		 * @var string
		 */
		const ChineseCountingThousand = "chineseCountingThousand";

		/**
		 * Korean Digital Counting System
		 * @var string
		 */
		const KoreanDigital = "koreanDigital";

		/**
		 * Korean Counting System
		 * @var string
		 */
		const KoreanCounting = "koreanCounting";

		/**
		 * Korean Legal Numbering
		 * @var string
		 */
		const KoreanLegal = "koreanLegal";

		/**
		 * Korean Digital Counting System Alternate
		 * @var string
		 */
		const KoreanDigital2 = "koreanDigital2";

		/**
		 * Vietnamese Numerals
		 * @var string
		 */
		const VietnameseCounting = "vietnameseCounting";

		/**
		 * Lowercase Russian Alphabet
		 * @var string
		 */
		const RussianLower = "russianLower";

		/**
		 * Uppercase Russian Alphabet
		 * @var string
		 */
		const RussianUpper = "russianUpper";

		/**
		 * No Numbering
		 * @var string
		 */
		const None = "none";

		/**
		 * Number With Dashes
		 * @var string
		 */
		const NumberInDash = "numberInDash";

		/**
		 * Hebrew Numerals
		 * @var string
		 */
		const Hebrew1 = "hebrew1";

		/**
		 * Hebrew Alphabet
		 * @var string
		 */
		const Hebrew2 = "hebrew2";

		/**
		 * Arabic Alphabet
		 * @var string
		 */
		const ArabicAlpha = "arabicAlpha";

		/**
		 * Arabic Abjad Numerals
		 * @var string
		 */
		const ArabicAbjad = "arabicAbjad";

		/**
		 * Hindi Vowels
		 * @var string
		 */
		const HindiVowels = "hindiVowels";

		/**
		 * Hindi Consonants
		 * @var string
		 */
		const HindiConsonants = "hindiConsonants";

		/**
		 * Hindi Numbers
		 * @var string
		 */
		const HindiNumbers = "hindiNumbers";

		/**
		 * Hindi Counting System
		 * @var string
		 */
		const HindiCounting = "hindiCounting";

		/**
		 * Thai Letters
		 * @var string
		 */
		const ThaiLetters = "thaiLetters";

		/**
		 * Thai Numerals
		 * @var string
		 */
		const ThaiNumbers = "thaiNumbers";

		/**
		 * Thai Counting System
		 * @var string
		 */
		const ThaiCounting = "thaiCounting";


		
	}
	
}

?>