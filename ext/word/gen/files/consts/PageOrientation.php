<?php 

namespace Word\Consts
{

	/**
	 * Page Orientation
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_PageOrientation.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class PageOrientation
	{
		
		/**
		 * Portrait Mode
		 * @var string
		 */
		const Portrait = "portrait";

		/**
		 * Landscape Mode
		 * @var string
		 */
		const Landscape = "landscape";


		
	}
	
}

?>