<?php 

namespace Word\Consts
{

	/**
	 * Horizontal Anchor Location
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_HAnchor.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class HAnchor
	{
		
		/**
		 * Relative to Text Extents
		 * @var string
		 */
		const Text = "text";

		/**
		 * Relative To Margin
		 * @var string
		 */
		const Margin = "margin";

		/**
		 * Relative to Page
		 * @var string
		 */
		const Page = "page";


		
	}
	
}

?>