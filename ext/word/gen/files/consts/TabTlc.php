<?php 

namespace Word\Consts
{

	/**
	 * Custom Tab Stop Leader Character
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_TabTlc.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class TabTlc
	{
		
		/**
		 * No tab stop leader
		 * @var string
		 */
		const None = "none";

		/**
		 * Dotted leader line
		 * @var string
		 */
		const Dot = "dot";

		/**
		 * Dashed tab stop leader line
		 * @var string
		 */
		const Hyphen = "hyphen";

		/**
		 * Solid leader line
		 * @var string
		 */
		const Underscore = "underscore";

		/**
		 * Heavy solid leader line
		 * @var string
		 */
		const Heavy = "heavy";

		/**
		 * Middle dot leader line
		 * @var string
		 */
		const MiddleDot = "middleDot";


		
	}
	
}

?>