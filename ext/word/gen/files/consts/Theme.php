<?php 

namespace Word\Consts
{

	/**
	 * Theme Font
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_Theme.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class Theme
	{
		
		/**
		 * Major East Asian Theme Font
		 * @var string
		 */
		const MajorEastAsia = "majorEastAsia";

		/**
		 * Major Complex Script Theme Font
		 * @var string
		 */
		const MajorBidi = "majorBidi";

		/**
		 * Major ASCII Theme Font
		 * @var string
		 */
		const MajorAscii = "majorAscii";

		/**
		 * Major High ANSI Theme Font
		 * @var string
		 */
		const MajorHAnsi = "majorHAnsi";

		/**
		 * Minor East Asian Theme Font
		 * @var string
		 */
		const MinorEastAsia = "minorEastAsia";

		/**
		 * Minor Complex Script Theme Font
		 * @var string
		 */
		const MinorBidi = "minorBidi";

		/**
		 * Minor ASCII Theme Font
		 * @var string
		 */
		const MinorAscii = "minorAscii";

		/**
		 * Minor High ANSI Theme Font
		 * @var string
		 */
		const MinorHAnsi = "minorHAnsi";


		
	}
	
}

?>