<?php 

namespace Word\Consts
{

	/**
	 * Two Lines in One Enclosing Character Type
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_CombineBrackets.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class CombineBrackets
	{
		
		/**
		 * No Enclosing Brackets
		 * @var string
		 */
		const None = "none";

		/**
		 * Round Brackets
		 * @var string
		 */
		const Round = "round";

		/**
		 * Square Brackets
		 * @var string
		 */
		const Square = "square";

		/**
		 * Angle Brackets
		 * @var string
		 */
		const Angle = "angle";

		/**
		 * Curly Brackets
		 * @var string
		 */
		const Curly = "curly";


		
	}
	
}

?>