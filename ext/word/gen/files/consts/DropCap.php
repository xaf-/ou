<?php 

namespace Word\Consts
{

	/**
	 * Text Frame Drop Cap Location
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_ST_DropCap.html
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class DropCap
	{
		
		/**
		 * Not Drop Cap
		 * @var string
		 */
		const None = "none";

		/**
		 * Drop Cap Inside Margin
		 * @var string
		 */
		const Drop = "drop";

		/**
		 * Drop Cap Outside Margin
		 * @var string
		 */
		const Margin = "margin";


		
	}
	
}

?>