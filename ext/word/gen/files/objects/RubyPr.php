<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_RubyPr.html 
	 * @property-read RubyAlign $rubyAlign Phonetic Guide Text Alignment
	 * @property-read HpsMeasure $hps Phonetic Guide Text Font Size
	 * @property-read HpsMeasure $hpsRaise Distance Between Phonetic Guide Text and Phonetic Guide Base Text
	 * @property-read HpsMeasure $hpsBaseText Phonetic Guide Base Text Font Size
	 * @property-read Lang $lid Language ID for Phonetic Guide
	 * @property-read OnOff $dirty Invalidated Field Cache
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class RubyPr extends Object
	{
		
		/* --- Childrens --- */

		private $_rubyAlign;
		private $_hps;
		private $_hpsRaise;
		private $_hpsBaseText;
		private $_lid;
		private $_dirty;

		
		/* --- Childrens --- */

		protected function getRubyAlign(){ return $this->_rubyAlign->get(); }

		protected function getHps(){ return $this->_hps->get(); }

		protected function getHpsRaise(){ return $this->_hpsRaise->get(); }

		protected function getHpsBaseText(){ return $this->_hpsBaseText->get(); }

		protected function getLid(){ return $this->_lid->get(); }

		protected function getDirty(){ return $this->_dirty->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\RubyAlign",
					"Objects\HpsMeasure",
					"Objects\Lang",
					"Objects\OnOff"
				)
			);
				
			parent::__construct($name);

			$this->_rubyAlign = new \Word\PropertyObject('\Word\Objects\RubyAlign', "w:rubyAlign", $this);
			$this->_hps = new \Word\PropertyObject('\Word\Objects\HpsMeasure', "w:hps", $this);
			$this->_hpsRaise = new \Word\PropertyObject('\Word\Objects\HpsMeasure', "w:hpsRaise", $this);
			$this->_hpsBaseText = new \Word\PropertyObject('\Word\Objects\HpsMeasure', "w:hpsBaseText", $this);
			$this->_lid = new \Word\PropertyObject('\Word\Objects\Lang', "w:lid", $this);
			$this->_dirty = new \Word\PropertyObject('\Word\Objects\OnOff', "w:dirty", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 

				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{

		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>