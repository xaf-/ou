<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_FFCheckBox.html 
	 * @property-read HpsMeasure $size Checkbox Form Field Size
	 * @property-read OnOff $sizeAuto Automatically Size Form Field
	 * @property-read OnOff $default Default Checkbox Form Field State
	 * @property-read OnOff $checked Checkbox Form Field State
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class FFCheckBox extends Object
	{
		
		/* --- Childrens --- */

		private $_size;
		private $_sizeAuto;
		private $_default;
		private $_checked;

		
		/* --- Childrens --- */

		protected function getSize(){ return $this->_size->get(); }

		protected function getSizeAuto(){ return $this->_sizeAuto->get(); }

		protected function getDefault(){ return $this->_default->get(); }

		protected function getChecked(){ return $this->_checked->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\HpsMeasure",
					"Objects\OnOff"
				)
			);
				
			parent::__construct($name);

			$this->_size = new \Word\PropertyObject('\Word\Objects\HpsMeasure', "w:size", $this);
			$this->_sizeAuto = new \Word\PropertyObject('\Word\Objects\OnOff', "w:sizeAuto", $this);
			$this->_default = new \Word\PropertyObject('\Word\Objects\OnOff', "w:default", $this);
			$this->_checked = new \Word\PropertyObject('\Word\Objects\OnOff', "w:checked", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 

				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{

		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>