<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_R.html 
	 * @property-read RPr $rPr Run Properties
	 * @property-read Br[] $br Break
	 * @property-read Text[] $t Text
	 * @property-read Text[] $delText Deleted Text
	 * @property-read Text[] $instrText Field Code
	 * @property-read Text[] $delInstrText Deleted Field Code
	 * @property-read Empty_[] $noBreakHyphen Non Breaking Hyphen Character
	 * @property-read Empty_ $softHyphen Optional Hyphen Character
	 * @property-read Empty_ $dayShort Date Block - Short Day Format
	 * @property-read Empty_ $monthShort Date Block - Short Month Format
	 * @property-read Empty_ $yearShort Date Block - Short Year Format
	 * @property-read Empty_ $dayLong Date Block - Long Day Format
	 * @property-read Empty_ $monthLong Date Block - Long Month Format
	 * @property-read Empty_ $yearLong Date Block - Long Year Format
	 * @property-read Empty_ $annotationRef Comment Information Block
	 * @property-read Empty_ $footnoteRef Footnote Reference Mark
	 * @property-read Empty_ $endnoteRef Endnote Reference Mark
	 * @property-read Empty_ $separator Footnote/Endnote Separator Mark
	 * @property-read Empty_ $continuationSeparator Continuation Separator Mark
	 * @property-read Sym $sym Symbol Character
	 * @property-read Empty_ $pgNum Page Number Block
	 * @property-read Empty_ $cr Carriage Return
	 * @property-read Empty_ $tab Tab Character
	 * @property-read Object_[] $object Inline Embedded Object
	 * @property-read Picture[] $pict VML Object
	 * @property-read FldChar[] $fldChar Complex Field Character
	 * @property-read Ruby[] $ruby Phonetic Guide
	 * @property-read FtnEdnRef[] $footnoteReference Footnote Reference
	 * @property-read FtnEdnRef[] $endnoteReference Endnote Reference
	 * @property-read Markup[] $commentReference Comment Content Reference Mark
	 * @property-read Drawing[] $drawing DrawingML Object
	 * @property-read PTab $ptab Absolute Position Tab Character
	 * @property-read Empty_ $lastRenderedPageBreak Position of Last Calculated Page Break
	 * @property-read LongHexNumber $rsidRPr Revision Identifier for Run Properties
	 * @property-read LongHexNumber $rsidDel Revision Identifier for Run Deletion
	 * @property-read LongHexNumber $rsidR Revision Identifier for Run
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class R extends Object
	{
		
		/* --- Attributes --- */

		private $_rsidRPr;
		private $_rsidDel;
		private $_rsidR;

		/* --- Childrens --- */

		private $_rPr;
		private $_br;
		private $_t;
		private $_delText;
		private $_instrText;
		private $_delInstrText;
		private $_noBreakHyphen;
		private $_softHyphen;
		private $_dayShort;
		private $_monthShort;
		private $_yearShort;
		private $_dayLong;
		private $_monthLong;
		private $_yearLong;
		private $_annotationRef;
		private $_footnoteRef;
		private $_endnoteRef;
		private $_separator;
		private $_continuationSeparator;
		private $_sym;
		private $_pgNum;
		private $_cr;
		private $_tab;
		private $_object;
		private $_pict;
		private $_fldChar;
		private $_ruby;
		private $_footnoteReference;
		private $_endnoteReference;
		private $_commentReference;
		private $_drawing;
		private $_ptab;
		private $_lastRenderedPageBreak;

		
		/* --- Attributes --- */

		protected function getRsidRPr(){ return $this->_rsidRPr; }
		protected function setRsidRPr($val){ $this->_rsidRPr = $val; }

		protected function getRsidDel(){ return $this->_rsidDel; }
		protected function setRsidDel($val){ $this->_rsidDel = $val; }

		protected function getRsidR(){ return $this->_rsidR; }
		protected function setRsidR($val){ $this->_rsidR = $val; }

		/* --- Childrens --- */

		protected function getRPr(){ return $this->_rPr->get(); }

		protected function getBr(){ return $this->_br->get(); }
		public function createBr(){ $new = new \Word\Objects\Br("w:br"); $this->br->AddRef($new); return $new; }

		protected function getT(){ return $this->_t->get(); }
		public function createT($content=null){ $new = new \Word\Objects\Text("w:t"); $new->content = $content; $this->t->AddRef($new); return $new; }

		protected function getDelText(){ return $this->_delText->get(); }
		public function createDelText($content=null){ $new = new \Word\Objects\Text("w:delText"); $new->content = $content; $this->delText->AddRef($new); return $new; }

		protected function getInstrText(){ return $this->_instrText->get(); }
		public function createInstrText($content=null){ $new = new \Word\Objects\Text("w:instrText"); $new->content = $content; $this->instrText->AddRef($new); return $new; }

		protected function getDelInstrText(){ return $this->_delInstrText->get(); }
		public function createDelInstrText($content=null){ $new = new \Word\Objects\Text("w:delInstrText"); $new->content = $content; $this->delInstrText->AddRef($new); return $new; }

		protected function getNoBreakHyphen(){ return $this->_noBreakHyphen->get(); }
		public function createNoBreakHyphen(){ $new = new \Word\Objects\Empty_("w:noBreakHyphen"); $this->noBreakHyphen->AddRef($new); return $new; }

		protected function getSoftHyphen(){ return $this->_softHyphen->get(); }

		protected function getDayShort(){ return $this->_dayShort->get(); }

		protected function getMonthShort(){ return $this->_monthShort->get(); }

		protected function getYearShort(){ return $this->_yearShort->get(); }

		protected function getDayLong(){ return $this->_dayLong->get(); }

		protected function getMonthLong(){ return $this->_monthLong->get(); }

		protected function getYearLong(){ return $this->_yearLong->get(); }

		protected function getAnnotationRef(){ return $this->_annotationRef->get(); }

		protected function getFootnoteRef(){ return $this->_footnoteRef->get(); }

		protected function getEndnoteRef(){ return $this->_endnoteRef->get(); }

		protected function getSeparator(){ return $this->_separator->get(); }

		protected function getContinuationSeparator(){ return $this->_continuationSeparator->get(); }

		protected function getSym(){ return $this->_sym->get(); }

		protected function getPgNum(){ return $this->_pgNum->get(); }

		protected function getCr(){ return $this->_cr->get(); }

		protected function getTab(){ return $this->_tab->get(); }

		protected function getObject(){ return $this->_object->get(); }
		public function createObject(){ $new = new \Word\Objects\Object_("w:object"); $this->object->AddRef($new); return $new; }

		protected function getPict(){ return $this->_pict->get(); }
		public function createPict(){ $new = new \Word\Objects\Picture("w:pict"); $this->pict->AddRef($new); return $new; }

		protected function getFldChar(){ return $this->_fldChar->get(); }
		public function createFldChar(){ $new = new \Word\Objects\FldChar("w:fldChar"); $this->fldChar->AddRef($new); return $new; }

		protected function getRuby(){ return $this->_ruby->get(); }
		public function createRuby(){ $new = new \Word\Objects\Ruby("w:ruby"); $this->ruby->AddRef($new); return $new; }

		protected function getFootnoteReference(){ return $this->_footnoteReference->get(); }
		public function createFootnoteReference(){ $new = new \Word\Objects\FtnEdnRef("w:footnoteReference"); $this->footnoteReference->AddRef($new); return $new; }

		protected function getEndnoteReference(){ return $this->_endnoteReference->get(); }
		public function createEndnoteReference(){ $new = new \Word\Objects\FtnEdnRef("w:endnoteReference"); $this->endnoteReference->AddRef($new); return $new; }

		protected function getCommentReference(){ return $this->_commentReference->get(); }
		public function createCommentReference(){ $new = new \Word\Objects\Markup("w:commentReference"); $this->commentReference->AddRef($new); return $new; }

		protected function getDrawing(){ return $this->_drawing->get(); }
		public function createDrawing(){ $new = new \Word\Objects\Drawing("w:drawing"); $this->drawing->AddRef($new); return $new; }

		protected function getPtab(){ return $this->_ptab->get(); }

		protected function getLastRenderedPageBreak(){ return $this->_lastRenderedPageBreak->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\RPr",
					"Objects\Br",
					"Objects\Text",
					"Objects\Empty_",
					"Objects\Sym",
					"Objects\Object_",
					"Objects\Picture",
					"Objects\FldChar",
					"Objects\Ruby",
					"Objects\FtnEdnRef",
					"Objects\Markup",
					"Objects\Drawing",
					"Objects\PTab",
					"Consts\LongHexNumber"
				)
			);
				
			parent::__construct($name);

			$this->_rPr = new \Word\PropertyObject('\Word\Objects\RPr', "w:rPr", $this);
			$this->_br = new \Word\PropertyArray($this);
			$this->_t = new \Word\PropertyArray($this);
			$this->_delText = new \Word\PropertyArray($this);
			$this->_instrText = new \Word\PropertyArray($this);
			$this->_delInstrText = new \Word\PropertyArray($this);
			$this->_noBreakHyphen = new \Word\PropertyArray($this);
			$this->_softHyphen = new \Word\PropertyObject('\Word\Objects\Empty_', "w:softHyphen", $this);
			$this->_dayShort = new \Word\PropertyObject('\Word\Objects\Empty_', "w:dayShort", $this);
			$this->_monthShort = new \Word\PropertyObject('\Word\Objects\Empty_', "w:monthShort", $this);
			$this->_yearShort = new \Word\PropertyObject('\Word\Objects\Empty_', "w:yearShort", $this);
			$this->_dayLong = new \Word\PropertyObject('\Word\Objects\Empty_', "w:dayLong", $this);
			$this->_monthLong = new \Word\PropertyObject('\Word\Objects\Empty_', "w:monthLong", $this);
			$this->_yearLong = new \Word\PropertyObject('\Word\Objects\Empty_', "w:yearLong", $this);
			$this->_annotationRef = new \Word\PropertyObject('\Word\Objects\Empty_', "w:annotationRef", $this);
			$this->_footnoteRef = new \Word\PropertyObject('\Word\Objects\Empty_', "w:footnoteRef", $this);
			$this->_endnoteRef = new \Word\PropertyObject('\Word\Objects\Empty_', "w:endnoteRef", $this);
			$this->_separator = new \Word\PropertyObject('\Word\Objects\Empty_', "w:separator", $this);
			$this->_continuationSeparator = new \Word\PropertyObject('\Word\Objects\Empty_', "w:continuationSeparator", $this);
			$this->_sym = new \Word\PropertyObject('\Word\Objects\Sym', "w:sym", $this);
			$this->_pgNum = new \Word\PropertyObject('\Word\Objects\Empty_', "w:pgNum", $this);
			$this->_cr = new \Word\PropertyObject('\Word\Objects\Empty_', "w:cr", $this);
			$this->_tab = new \Word\PropertyObject('\Word\Objects\Empty_', "w:tab", $this);
			$this->_object = new \Word\PropertyArray($this);
			$this->_pict = new \Word\PropertyArray($this);
			$this->_fldChar = new \Word\PropertyArray($this);
			$this->_ruby = new \Word\PropertyArray($this);
			$this->_footnoteReference = new \Word\PropertyArray($this);
			$this->_endnoteReference = new \Word\PropertyArray($this);
			$this->_commentReference = new \Word\PropertyArray($this);
			$this->_drawing = new \Word\PropertyArray($this);
			$this->_ptab = new \Word\PropertyObject('\Word\Objects\PTab', "w:ptab", $this);
			$this->_lastRenderedPageBreak = new \Word\PropertyObject('\Word\Objects\Empty_', "w:lastRenderedPageBreak", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->rsidRPr === null && 
				$this->rsidDel === null && 
				$this->rsidR === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->rsidRPr !== null) $xml->writeAttribute("w:rsidRPr", $this->rsidRPr);
			if ($this->rsidDel !== null) $xml->writeAttribute("w:rsidDel", $this->rsidDel);
			if ($this->rsidR !== null) $xml->writeAttribute("w:rsidR", $this->rsidR);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>