<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_TabStop.html 
	 * @property-read TabJc $val Tab Stop Type
	 * @property-read TabTlc $leader Tab Leader Character
	 * @property-read SignedTwipsMeasure $pos Tab Stop Position
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class TabStop extends Object
	{
		
		/* --- Attributes --- */

		private $_val;
		private $_leader;
		private $_pos;

		
		/* --- Attributes --- */

		protected function getVal(){ return $this->_val; }
		protected function setVal($val){ $this->_val = $val; }

		protected function getLeader(){ return $this->_leader; }
		protected function setLeader($val){ $this->_leader = $val; }

		protected function getPos(){ return $this->_pos; }
		protected function setPos($val){ $this->_pos = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\TabJc",
					"Consts\TabTlc",
					"Consts\SignedTwipsMeasure"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->val === null && 
				$this->leader === null && 
				$this->pos === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->val !== null) $xml->writeAttribute("w:val", $this->val);
			if ($this->leader !== null) $xml->writeAttribute("w:leader", $this->leader);
			if ($this->pos !== null) $xml->writeAttribute("w:pos", $this->pos);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return
				$this->val !== null && 
			/* --- Attributes --- */

			/* --- Attributes --- */

				$this->pos !== null && 


				true;
		}
		
		
	}
	
}

?>