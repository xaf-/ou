<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_Fonts.html 
	 * @property-read Hint $hint Font Content Type
	 * @property-read String $ascii ASCII Font
	 * @property-read String $hAnsi High ANSI Font
	 * @property-read String $eastAsia East Asian Font
	 * @property-read String $cs Complex Script Font
	 * @property-read Theme $asciiTheme ASCII Theme Font
	 * @property-read Theme $hAnsiTheme High ANSI Theme Font
	 * @property-read Theme $eastAsiaTheme East Asian Theme Font
	 * @property-read Theme $cstheme Complex Script Theme Font
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class Fonts extends Object
	{
		
		/* --- Attributes --- */

		private $_hint;
		private $_ascii;
		private $_hAnsi;
		private $_eastAsia;
		private $_cs;
		private $_asciiTheme;
		private $_hAnsiTheme;
		private $_eastAsiaTheme;
		private $_cstheme;

		
		/* --- Attributes --- */

		protected function getHint(){ return $this->_hint; }
		protected function setHint($val){ $this->_hint = $val; }

		protected function getAscii(){ return $this->_ascii; }
		protected function setAscii($val){ $this->_ascii = $val; }

		protected function getHAnsi(){ return $this->_hAnsi; }
		protected function setHAnsi($val){ $this->_hAnsi = $val; }

		protected function getEastAsia(){ return $this->_eastAsia; }
		protected function setEastAsia($val){ $this->_eastAsia = $val; }

		protected function getCs(){ return $this->_cs; }
		protected function setCs($val){ $this->_cs = $val; }

		protected function getAsciiTheme(){ return $this->_asciiTheme; }
		protected function setAsciiTheme($val){ $this->_asciiTheme = $val; }

		protected function getHAnsiTheme(){ return $this->_hAnsiTheme; }
		protected function setHAnsiTheme($val){ $this->_hAnsiTheme = $val; }

		protected function getEastAsiaTheme(){ return $this->_eastAsiaTheme; }
		protected function setEastAsiaTheme($val){ $this->_eastAsiaTheme = $val; }

		protected function getCstheme(){ return $this->_cstheme; }
		protected function setCstheme($val){ $this->_cstheme = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\Hint",
					"Consts\String",
					"Consts\Theme"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->hint === null && 
				$this->ascii === null && 
				$this->hAnsi === null && 
				$this->eastAsia === null && 
				$this->cs === null && 
				$this->asciiTheme === null && 
				$this->hAnsiTheme === null && 
				$this->eastAsiaTheme === null && 
				$this->cstheme === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->hint !== null) $xml->writeAttribute("w:hint", $this->hint);
			if ($this->ascii !== null) $xml->writeAttribute("w:ascii", $this->ascii);
			if ($this->hAnsi !== null) $xml->writeAttribute("w:hAnsi", $this->hAnsi);
			if ($this->eastAsia !== null) $xml->writeAttribute("w:eastAsia", $this->eastAsia);
			if ($this->cs !== null) $xml->writeAttribute("w:cs", $this->cs);
			if ($this->asciiTheme !== null) $xml->writeAttribute("w:asciiTheme", $this->asciiTheme);
			if ($this->hAnsiTheme !== null) $xml->writeAttribute("w:hAnsiTheme", $this->hAnsiTheme);
			if ($this->eastAsiaTheme !== null) $xml->writeAttribute("w:eastAsiaTheme", $this->eastAsiaTheme);
			if ($this->cstheme !== null) $xml->writeAttribute("w:cstheme", $this->cstheme);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>