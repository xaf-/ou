<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_Placeholder.html 
	 * @property-read String $docPart Document Part Reference
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class Placeholder extends Object
	{
		
		/* --- Childrens --- */

		private $_docPart;

		
		/* --- Childrens --- */

		protected function getDocPart(){ return $this->_docPart->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\String"
				)
			);
				
			parent::__construct($name);

			$this->_docPart = new \Word\PropertyObject('\Word\Objects\String', "w:docPart", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 

				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{

		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>