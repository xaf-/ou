<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_TcBorders.html 
	 * @property-read Border $top Table Cell Top Border
	 * @property-read Border $left Table Cell Left Border
	 * @property-read Border $bottom Table Cell Bottom Border
	 * @property-read Border $right Table Cell Right Border
	 * @property-read Border $insideH Table Cell Inside Horizontal Edges Border
	 * @property-read Border $insideV Table Cell Inside Vertical Edges Border
	 * @property-read Border $tl2br Table Cell Top Left to Bottom Right Diagonal Border
	 * @property-read Border $tr2bl Table Cell Top Right to Bottom Left Diagonal Border
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class TcBorders extends Object
	{
		
		/* --- Childrens --- */

		private $_top;
		private $_left;
		private $_bottom;
		private $_right;
		private $_insideH;
		private $_insideV;
		private $_tl2br;
		private $_tr2bl;

		
		/* --- Childrens --- */

		protected function getTop(){ return $this->_top->get(); }

		protected function getLeft(){ return $this->_left->get(); }

		protected function getBottom(){ return $this->_bottom->get(); }

		protected function getRight(){ return $this->_right->get(); }

		protected function getInsideH(){ return $this->_insideH->get(); }

		protected function getInsideV(){ return $this->_insideV->get(); }

		protected function getTl2br(){ return $this->_tl2br->get(); }

		protected function getTr2bl(){ return $this->_tr2bl->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\Border"
				)
			);
				
			parent::__construct($name);

			$this->_top = new \Word\PropertyObject('\Word\Objects\Border', "w:top", $this);
			$this->_left = new \Word\PropertyObject('\Word\Objects\Border', "w:left", $this);
			$this->_bottom = new \Word\PropertyObject('\Word\Objects\Border', "w:bottom", $this);
			$this->_right = new \Word\PropertyObject('\Word\Objects\Border', "w:right", $this);
			$this->_insideH = new \Word\PropertyObject('\Word\Objects\Border', "w:insideH", $this);
			$this->_insideV = new \Word\PropertyObject('\Word\Objects\Border', "w:insideV", $this);
			$this->_tl2br = new \Word\PropertyObject('\Word\Objects\Border', "w:tl2br", $this);
			$this->_tr2bl = new \Word\PropertyObject('\Word\Objects\Border', "w:tr2bl", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 

				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{

		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>