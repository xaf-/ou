<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_Shd.html 
	 * @property-read Shd $val Shading Pattern
	 * @property-read HexColor $color Shading Pattern Color
	 * @property-read ThemeColor $themeColor Shading Pattern Theme Color
	 * @property-read UcharHexNumber $themeTint Shading Pattern Theme Color Tint
	 * @property-read UcharHexNumber $themeShade Shading Pattern Theme Color Shade
	 * @property-read HexColor $fill Shading Background Color
	 * @property-read ThemeColor $themeFill Shading Background Theme Color
	 * @property-read UcharHexNumber $themeFillTint Shading Background Theme Color Tint
	 * @property-read UcharHexNumber $themeFillShade Shading Background Theme Color Shade
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class Shd extends Object
	{
		
		/* --- Attributes --- */

		private $_val;
		private $_color;
		private $_themeColor;
		private $_themeTint;
		private $_themeShade;
		private $_fill;
		private $_themeFill;
		private $_themeFillTint;
		private $_themeFillShade;

		
		/* --- Attributes --- */

		protected function getVal(){ return $this->_val; }
		protected function setVal($val){ $this->_val = $val; }

		protected function getColor(){ return $this->_color; }
		protected function setColor($val){ $this->_color = $val; }

		protected function getThemeColor(){ return $this->_themeColor; }
		protected function setThemeColor($val){ $this->_themeColor = $val; }

		protected function getThemeTint(){ return $this->_themeTint; }
		protected function setThemeTint($val){ $this->_themeTint = $val; }

		protected function getThemeShade(){ return $this->_themeShade; }
		protected function setThemeShade($val){ $this->_themeShade = $val; }

		protected function getFill(){ return $this->_fill; }
		protected function setFill($val){ $this->_fill = $val; }

		protected function getThemeFill(){ return $this->_themeFill; }
		protected function setThemeFill($val){ $this->_themeFill = $val; }

		protected function getThemeFillTint(){ return $this->_themeFillTint; }
		protected function setThemeFillTint($val){ $this->_themeFillTint = $val; }

		protected function getThemeFillShade(){ return $this->_themeFillShade; }
		protected function setThemeFillShade($val){ $this->_themeFillShade = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\Shd",
					"Consts\HexColor",
					"Consts\ThemeColor",
					"Consts\UcharHexNumber"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->val === null && 
				$this->color === null && 
				$this->themeColor === null && 
				$this->themeTint === null && 
				$this->themeShade === null && 
				$this->fill === null && 
				$this->themeFill === null && 
				$this->themeFillTint === null && 
				$this->themeFillShade === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->val !== null) $xml->writeAttribute("w:val", $this->val);
			if ($this->color !== null) $xml->writeAttribute("w:color", $this->color);
			if ($this->themeColor !== null) $xml->writeAttribute("w:themeColor", $this->themeColor);
			if ($this->themeTint !== null) $xml->writeAttribute("w:themeTint", $this->themeTint);
			if ($this->themeShade !== null) $xml->writeAttribute("w:themeShade", $this->themeShade);
			if ($this->fill !== null) $xml->writeAttribute("w:fill", $this->fill);
			if ($this->themeFill !== null) $xml->writeAttribute("w:themeFill", $this->themeFill);
			if ($this->themeFillTint !== null) $xml->writeAttribute("w:themeFillTint", $this->themeFillTint);
			if ($this->themeFillShade !== null) $xml->writeAttribute("w:themeFillShade", $this->themeFillShade);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return
				$this->val !== null && 
			/* --- Attributes --- */

			/* --- Attributes --- */

			/* --- Attributes --- */

			/* --- Attributes --- */

			/* --- Attributes --- */

			/* --- Attributes --- */

			/* --- Attributes --- */

			/* --- Attributes --- */



				true;
		}
		
		
	}
	
}

?>