<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_TblGridChange.html 
	 * @property-read TblGridBase $tblGrid Previous Table Grid
	 * @property-read DecimalNumber $id Annotation Identifier
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class TblGridChange extends Object
	{
		
		/* --- Attributes --- */

		private $_id;

		/* --- Childrens --- */

		private $_tblGrid;

		
		/* --- Attributes --- */

		protected function getId(){ return $this->_id; }
		protected function setId($val){ $this->_id = $val; }

		/* --- Childrens --- */

		protected function getTblGrid(){ return $this->_tblGrid->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\TblGridBase",
					"Consts\DecimalNumber"
				)
			);
				
			parent::__construct($name);

			$this->_tblGrid = new \Word\PropertyObject('\Word\Objects\TblGridBase', "w:tblGrid", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->id === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->id !== null) $xml->writeAttribute("w:id", $this->id);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return
				$this->id !== null && 


				true;
		}
		
		
	}
	
}

?>