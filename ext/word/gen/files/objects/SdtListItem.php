<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_SdtListItem.html 
	 * @property-read String $displayText List Entry Display Text
	 * @property-read String $value List Entry Value
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class SdtListItem extends Object
	{
		
		/* --- Attributes --- */

		private $_displayText;
		private $_value;

		
		/* --- Attributes --- */

		protected function getDisplayText(){ return $this->_displayText; }
		protected function setDisplayText($val){ $this->_displayText = $val; }

		protected function getValue(){ return $this->_value; }
		protected function setValue($val){ $this->_value = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\String"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->displayText === null && 
				$this->value === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->displayText !== null) $xml->writeAttribute("w:displayText", $this->displayText);
			if ($this->value !== null) $xml->writeAttribute("w:value", $this->value);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>