<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_Text.html 
	 * @property mixed $content Element content
	 * @property specified $space Content Contains Significant Whitespace
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class Text extends Object
	{
		
		/* --- Attributes --- */

		private $_space;

		/* --- Childrens --- */

		private $_content;

		
		/* --- Attributes --- */

		protected function getSpace(){ return $this->_space; }
		protected function setSpace($val){ $this->_space = $val; }

		/* --- Childrens --- */

		protected function getContent(){ return $this->_content; }
		protected function setContent($val){ $this->_content = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->space === null && 

				$this->content === null && 

				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->space !== null) $xml->writeAttribute("xml:space", $this->space);

			$xml->text($this->_content);

		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return
				$this->content !== null && 

				true;
		}
		
		
	}
	
}

?>