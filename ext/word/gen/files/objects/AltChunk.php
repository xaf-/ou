<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_AltChunk.html 
	 * @property-read AltChunkPr $altChunkPr External Content Import Properties
	 * @property-read RelationshipId $id Relationship to Part
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class AltChunk extends Object
	{
		
		/* --- Attributes --- */

		private $_id;

		/* --- Childrens --- */

		private $_altChunkPr;

		
		/* --- Attributes --- */

		protected function getId(){ return $this->_id; }
		protected function setId($val){ $this->_id = $val; }

		/* --- Childrens --- */

		protected function getAltChunkPr(){ return $this->_altChunkPr->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\AltChunkPr",
					"Consts\RelationshipId"
				)
			);
				
			parent::__construct($name);

			$this->_altChunkPr = new \Word\PropertyObject('\Word\Objects\AltChunkPr', "w:altChunkPr", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->id === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->id !== null) $xml->writeAttribute("r:id", $this->id);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>