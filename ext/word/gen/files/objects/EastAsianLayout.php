<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_EastAsianLayout.html 
	 * @property-read DecimalNumber $id East Asian Typography Run ID
	 * @property-read OnOff $combine Two Lines in One
	 * @property-read CombineBrackets $combineBrackets Display Brackets Around Two Lines in One
	 * @property-read OnOff $vert Horizontal in Vertical (Rotate Text)
	 * @property-read OnOff $vertCompress Compress Rotated Text to Line Height
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class EastAsianLayout extends Object
	{
		
		/* --- Attributes --- */

		private $_id;
		private $_combine;
		private $_combineBrackets;
		private $_vert;
		private $_vertCompress;

		
		/* --- Attributes --- */

		protected function getId(){ return $this->_id; }
		protected function setId($val){ $this->_id = $val; }

		protected function getCombine(){ return $this->_combine; }
		protected function setCombine($val){ $this->_combine = $val; }

		protected function getCombineBrackets(){ return $this->_combineBrackets; }
		protected function setCombineBrackets($val){ $this->_combineBrackets = $val; }

		protected function getVert(){ return $this->_vert; }
		protected function setVert($val){ $this->_vert = $val; }

		protected function getVertCompress(){ return $this->_vertCompress; }
		protected function setVertCompress($val){ $this->_vertCompress = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\DecimalNumber",
					"Consts\OnOff",
					"Consts\CombineBrackets"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->id === null && 
				$this->combine === null && 
				$this->combineBrackets === null && 
				$this->vert === null && 
				$this->vertCompress === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->id !== null) $xml->writeAttribute("w:id", $this->id);
			if ($this->combine !== null) $xml->writeAttribute("w:combine", $this->combine);
			if ($this->combineBrackets !== null) $xml->writeAttribute("w:combineBrackets", $this->combineBrackets);
			if ($this->vert !== null) $xml->writeAttribute("w:vert", $this->vert);
			if ($this->vertCompress !== null) $xml->writeAttribute("w:vertCompress", $this->vertCompress);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>