<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_SdtPr.html 
	 * @property-read RPr $rPr Run Properties For Structured Document Tag Contents
	 * @property-read String $alias Friendly Name
	 * @property-read Lock $lock Locking Setting
	 * @property-read Placeholder $placeholder Structured Document Tag Placeholder Text
	 * @property-read OnOff $showingPlcHdr Current Contents Are Placeholder Text
	 * @property-read DataBinding $dataBinding XML Mapping
	 * @property-read OnOff $temporary Remove Structured Document Tag When Contents Are Edited
	 * @property-read DecimalNumber $id Unique ID
	 * @property-read String $tag Programmatic Tag
	 * @property-read Empty_ $equation Equation Structured Document Tag
	 * @property-read SdtComboBox $comboBox Combo Box Structured Document Tag
	 * @property-read SdtDate $date Date Structured Document Tag
	 * @property-read SdtDocPart $docPartObj Built-In Document Part Structured Document Tag
	 * @property-read SdtDocPart $docPartList Document Part Gallery Structured Document Tag
	 * @property-read SdtDropDownList $dropDownList Drop-Down List Structured Document Tag
	 * @property-read Empty_ $picture Picture Structured Document Tag
	 * @property-read Empty_ $richText Rich Text Structured Document Tag
	 * @property-read SdtText $text Plain Text Structured Document Tag
	 * @property-read Empty_ $citation Citation Structured Document Tag
	 * @property-read Empty_ $group Group Structured Document Tag
	 * @property-read Empty_ $bibliography Bibliography Structured Document Tag
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class SdtPr extends Object
	{
		
		/* --- Childrens --- */

		private $_rPr;
		private $_alias;
		private $_lock;
		private $_placeholder;
		private $_showingPlcHdr;
		private $_dataBinding;
		private $_temporary;
		private $_id;
		private $_tag;
		private $_equation;
		private $_comboBox;
		private $_date;
		private $_docPartObj;
		private $_docPartList;
		private $_dropDownList;
		private $_picture;
		private $_richText;
		private $_text;
		private $_citation;
		private $_group;
		private $_bibliography;

		
		/* --- Childrens --- */

		protected function getRPr(){ return $this->_rPr->get(); }

		protected function getAlias(){ return $this->_alias->get(); }

		protected function getLock(){ return $this->_lock->get(); }

		protected function getPlaceholder(){ return $this->_placeholder->get(); }

		protected function getShowingPlcHdr(){ return $this->_showingPlcHdr->get(); }

		protected function getDataBinding(){ return $this->_dataBinding->get(); }

		protected function getTemporary(){ return $this->_temporary->get(); }

		protected function getId(){ return $this->_id->get(); }

		protected function getTag(){ return $this->_tag->get(); }

		protected function getEquation(){ return $this->_equation->get(); }

		protected function getComboBox(){ return $this->_comboBox->get(); }

		protected function getDate(){ return $this->_date->get(); }

		protected function getDocPartObj(){ return $this->_docPartObj->get(); }

		protected function getDocPartList(){ return $this->_docPartList->get(); }

		protected function getDropDownList(){ return $this->_dropDownList->get(); }

		protected function getPicture(){ return $this->_picture->get(); }

		protected function getRichText(){ return $this->_richText->get(); }

		protected function getText(){ return $this->_text->get(); }

		protected function getCitation(){ return $this->_citation->get(); }

		protected function getGroup(){ return $this->_group->get(); }

		protected function getBibliography(){ return $this->_bibliography->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\RPr",
					"Objects\String",
					"Objects\Lock",
					"Objects\Placeholder",
					"Objects\OnOff",
					"Objects\DataBinding",
					"Objects\DecimalNumber",
					"Objects\Empty_",
					"Objects\SdtComboBox",
					"Objects\SdtDate",
					"Objects\SdtDocPart",
					"Objects\SdtDropDownList",
					"Objects\SdtText"
				)
			);
				
			parent::__construct($name);

			$this->_rPr = new \Word\PropertyObject('\Word\Objects\RPr', "w:rPr", $this);
			$this->_alias = new \Word\PropertyObject('\Word\Objects\String', "w:alias", $this);
			$this->_lock = new \Word\PropertyObject('\Word\Objects\Lock', "w:lock", $this);
			$this->_placeholder = new \Word\PropertyObject('\Word\Objects\Placeholder', "w:placeholder", $this);
			$this->_showingPlcHdr = new \Word\PropertyObject('\Word\Objects\OnOff', "w:showingPlcHdr", $this);
			$this->_dataBinding = new \Word\PropertyObject('\Word\Objects\DataBinding', "w:dataBinding", $this);
			$this->_temporary = new \Word\PropertyObject('\Word\Objects\OnOff', "w:temporary", $this);
			$this->_id = new \Word\PropertyObject('\Word\Objects\DecimalNumber', "w:id", $this);
			$this->_tag = new \Word\PropertyObject('\Word\Objects\String', "w:tag", $this);
			$this->_equation = new \Word\PropertyObject('\Word\Objects\Empty_', "w:equation", $this);
			$this->_comboBox = new \Word\PropertyObject('\Word\Objects\SdtComboBox', "w:comboBox", $this);
			$this->_date = new \Word\PropertyObject('\Word\Objects\SdtDate', "w:date", $this);
			$this->_docPartObj = new \Word\PropertyObject('\Word\Objects\SdtDocPart', "w:docPartObj", $this);
			$this->_docPartList = new \Word\PropertyObject('\Word\Objects\SdtDocPart', "w:docPartList", $this);
			$this->_dropDownList = new \Word\PropertyObject('\Word\Objects\SdtDropDownList', "w:dropDownList", $this);
			$this->_picture = new \Word\PropertyObject('\Word\Objects\Empty_', "w:picture", $this);
			$this->_richText = new \Word\PropertyObject('\Word\Objects\Empty_', "w:richText", $this);
			$this->_text = new \Word\PropertyObject('\Word\Objects\SdtText', "w:text", $this);
			$this->_citation = new \Word\PropertyObject('\Word\Objects\Empty_', "w:citation", $this);
			$this->_group = new \Word\PropertyObject('\Word\Objects\Empty_', "w:group", $this);
			$this->_bibliography = new \Word\PropertyObject('\Word\Objects\Empty_', "w:bibliography", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 

				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{

		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>