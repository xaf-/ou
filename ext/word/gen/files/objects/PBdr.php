<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_PBdr.html 
	 * @property-read Border $top Paragraph Border Above Identical Paragraphs
	 * @property-read Border $left Left Paragraph Border
	 * @property-read Border $bottom Paragraph Border Between Identical Paragraphs
	 * @property-read Border $right Right Paragraph Border
	 * @property-read Border $between Paragraph Border Between Identical Paragraphs
	 * @property-read Border $bar Paragraph Border Between Facing Pages
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class PBdr extends Object
	{
		
		/* --- Childrens --- */

		private $_top;
		private $_left;
		private $_bottom;
		private $_right;
		private $_between;
		private $_bar;

		
		/* --- Childrens --- */

		protected function getTop(){ return $this->_top->get(); }

		protected function getLeft(){ return $this->_left->get(); }

		protected function getBottom(){ return $this->_bottom->get(); }

		protected function getRight(){ return $this->_right->get(); }

		protected function getBetween(){ return $this->_between->get(); }

		protected function getBar(){ return $this->_bar->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\Border"
				)
			);
				
			parent::__construct($name);

			$this->_top = new \Word\PropertyObject('\Word\Objects\Border', "w:top", $this);
			$this->_left = new \Word\PropertyObject('\Word\Objects\Border', "w:left", $this);
			$this->_bottom = new \Word\PropertyObject('\Word\Objects\Border', "w:bottom", $this);
			$this->_right = new \Word\PropertyObject('\Word\Objects\Border', "w:right", $this);
			$this->_between = new \Word\PropertyObject('\Word\Objects\Border', "w:between", $this);
			$this->_bar = new \Word\PropertyObject('\Word\Objects\Border', "w:bar", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 

				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{

		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>