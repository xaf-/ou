<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_FldChar.html 
	 * @property-read Text $fldData Custom Field Data
	 * @property-read FFData $ffData Form Field Properties
	 * @property-read TrackChangeNumbering $numberingChange Previous Numbering Field Properties
	 * @property-read FldCharType $fldCharType Field Character Type
	 * @property-read OnOff $fldLock Field Should Not Be Recalculated
	 * @property-read OnOff $dirty Field Result Invalidated
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class FldChar extends Object
	{
		
		/* --- Attributes --- */

		private $_fldCharType;
		private $_fldLock;
		private $_dirty;

		/* --- Childrens --- */

		private $_fldData;
		private $_ffData;
		private $_numberingChange;

		
		/* --- Attributes --- */

		protected function getFldCharType(){ return $this->_fldCharType; }
		protected function setFldCharType($val){ $this->_fldCharType = $val; }

		protected function getFldLock(){ return $this->_fldLock; }
		protected function setFldLock($val){ $this->_fldLock = $val; }

		protected function getDirty(){ return $this->_dirty; }
		protected function setDirty($val){ $this->_dirty = $val; }

		/* --- Childrens --- */

		protected function getFldData(){ return $this->_fldData->get(); }

		protected function getFfData(){ return $this->_ffData->get(); }

		protected function getNumberingChange(){ return $this->_numberingChange->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\Text",
					"Objects\FFData",
					"Objects\TrackChangeNumbering",
					"Consts\FldCharType",
					"Consts\OnOff"
				)
			);
				
			parent::__construct($name);

			$this->_fldData = new \Word\PropertyObject('\Word\Objects\Text', "w:fldData", $this);
			$this->_ffData = new \Word\PropertyObject('\Word\Objects\FFData', "w:ffData", $this);
			$this->_numberingChange = new \Word\PropertyObject('\Word\Objects\TrackChangeNumbering', "w:numberingChange", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->fldCharType === null && 
				$this->fldLock === null && 
				$this->dirty === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->fldCharType !== null) $xml->writeAttribute("w:fldCharType", $this->fldCharType);
			if ($this->fldLock !== null) $xml->writeAttribute("w:fldLock", $this->fldLock);
			if ($this->dirty !== null) $xml->writeAttribute("w:dirty", $this->dirty);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return
				$this->fldCharType !== null && 
			/* --- Attributes --- */

			/* --- Attributes --- */



				true;
		}
		
		
	}
	
}

?>