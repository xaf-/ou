<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_TblPr.html 
	 * @property-read String $tblStyle Referenced Table Style
	 * @property-read TblPPr $tblpPr Floating Table Positioning
	 * @property-read TblOverlap $tblOverlap Floating Table Allows Other Tables to Overlap
	 * @property-read OnOff $bidiVisual Visually Right to Left Table
	 * @property-read DecimalNumber $tblStyleRowBandSize Number of Rows in Row Band
	 * @property-read DecimalNumber $tblStyleColBandSize Number of Columns in Column Band
	 * @property-read TblWidth $tblW Preferred Table Width
	 * @property-read Jc $jc Table Alignment
	 * @property-read TblWidth $tblCellSpacing Table Cell Spacing Default
	 * @property-read TblWidth $tblInd Table Indent from Leading Margin
	 * @property-read TblBorders $tblBorders Table Borders
	 * @property-read Shd $shd Table Shading
	 * @property-read TblLayoutType $tblLayout Table Layout
	 * @property-read TblCellMar $tblCellMar Table Cell Margin Defaults
	 * @property-read ShortHexNumber $tblLook Table Style Conditional Formatting Settings
	 * @property-read TblPrChange $tblPrChange Revision Information for Table Properties
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class TblPr extends Object
	{
		
		/* --- Childrens --- */

		private $_tblStyle;
		private $_tblpPr;
		private $_tblOverlap;
		private $_bidiVisual;
		private $_tblStyleRowBandSize;
		private $_tblStyleColBandSize;
		private $_tblW;
		private $_jc;
		private $_tblCellSpacing;
		private $_tblInd;
		private $_tblBorders;
		private $_shd;
		private $_tblLayout;
		private $_tblCellMar;
		private $_tblLook;
		private $_tblPrChange;

		
		/* --- Childrens --- */

		protected function getTblStyle(){ return $this->_tblStyle->get(); }

		protected function getTblpPr(){ return $this->_tblpPr->get(); }

		protected function getTblOverlap(){ return $this->_tblOverlap->get(); }

		protected function getBidiVisual(){ return $this->_bidiVisual->get(); }

		protected function getTblStyleRowBandSize(){ return $this->_tblStyleRowBandSize->get(); }

		protected function getTblStyleColBandSize(){ return $this->_tblStyleColBandSize->get(); }

		protected function getTblW(){ return $this->_tblW->get(); }

		protected function getJc(){ return $this->_jc->get(); }

		protected function getTblCellSpacing(){ return $this->_tblCellSpacing->get(); }

		protected function getTblInd(){ return $this->_tblInd->get(); }

		protected function getTblBorders(){ return $this->_tblBorders->get(); }

		protected function getShd(){ return $this->_shd->get(); }

		protected function getTblLayout(){ return $this->_tblLayout->get(); }

		protected function getTblCellMar(){ return $this->_tblCellMar->get(); }

		protected function getTblLook(){ return $this->_tblLook->get(); }

		protected function getTblPrChange(){ return $this->_tblPrChange->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\String",
					"Objects\TblPPr",
					"Objects\TblOverlap",
					"Objects\OnOff",
					"Objects\DecimalNumber",
					"Objects\TblWidth",
					"Objects\Jc",
					"Objects\TblBorders",
					"Objects\Shd",
					"Objects\TblLayoutType",
					"Objects\TblCellMar",
					"Objects\ShortHexNumber",
					"Objects\TblPrChange"
				)
			);
				
			parent::__construct($name);

			$this->_tblStyle = new \Word\PropertyObject('\Word\Objects\String', "w:tblStyle", $this);
			$this->_tblpPr = new \Word\PropertyObject('\Word\Objects\TblPPr', "w:tblpPr", $this);
			$this->_tblOverlap = new \Word\PropertyObject('\Word\Objects\TblOverlap', "w:tblOverlap", $this);
			$this->_bidiVisual = new \Word\PropertyObject('\Word\Objects\OnOff', "w:bidiVisual", $this);
			$this->_tblStyleRowBandSize = new \Word\PropertyObject('\Word\Objects\DecimalNumber', "w:tblStyleRowBandSize", $this);
			$this->_tblStyleColBandSize = new \Word\PropertyObject('\Word\Objects\DecimalNumber', "w:tblStyleColBandSize", $this);
			$this->_tblW = new \Word\PropertyObject('\Word\Objects\TblWidth', "w:tblW", $this);
			$this->_jc = new \Word\PropertyObject('\Word\Objects\Jc', "w:jc", $this);
			$this->_tblCellSpacing = new \Word\PropertyObject('\Word\Objects\TblWidth', "w:tblCellSpacing", $this);
			$this->_tblInd = new \Word\PropertyObject('\Word\Objects\TblWidth', "w:tblInd", $this);
			$this->_tblBorders = new \Word\PropertyObject('\Word\Objects\TblBorders', "w:tblBorders", $this);
			$this->_shd = new \Word\PropertyObject('\Word\Objects\Shd', "w:shd", $this);
			$this->_tblLayout = new \Word\PropertyObject('\Word\Objects\TblLayoutType', "w:tblLayout", $this);
			$this->_tblCellMar = new \Word\PropertyObject('\Word\Objects\TblCellMar', "w:tblCellMar", $this);
			$this->_tblLook = new \Word\PropertyObject('\Word\Objects\ShortHexNumber', "w:tblLook", $this);
			$this->_tblPrChange = new \Word\PropertyObject('\Word\Objects\TblPrChange', "w:tblPrChange", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 

				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{

		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>