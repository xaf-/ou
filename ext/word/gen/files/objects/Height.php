<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_Height.html 
	 * @property-read TwipsMeasure $val Table Row Height
	 * @property-read HeightRule $hRule Table Row Height Type
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class Height extends Object
	{
		
		/* --- Attributes --- */

		private $_val;
		private $_hRule;

		
		/* --- Attributes --- */

		protected function getVal(){ return $this->_val; }
		protected function setVal($val){ $this->_val = $val; }

		protected function getHRule(){ return $this->_hRule; }
		protected function setHRule($val){ $this->_hRule = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\TwipsMeasure",
					"Consts\HeightRule"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->val === null && 
				$this->hRule === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->val !== null) $xml->writeAttribute("w:val", $this->val);
			if ($this->hRule !== null) $xml->writeAttribute("w:hRule", $this->hRule);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>