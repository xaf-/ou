<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_PageSz.html 
	 * @property-read TwipsMeasure $w Page Width
	 * @property-read TwipsMeasure $h Page Height
	 * @property-read PageOrientation $orient Page Orientation
	 * @property-read DecimalNumber $code Printer Paper Code
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class PageSz extends Object
	{
		
		/* --- Attributes --- */

		private $_w;
		private $_h;
		private $_orient;
		private $_code;

		
		/* --- Attributes --- */

		protected function getW(){ return $this->_w; }
		protected function setW($val){ $this->_w = $val; }

		protected function getH(){ return $this->_h; }
		protected function setH($val){ $this->_h = $val; }

		protected function getOrient(){ return $this->_orient; }
		protected function setOrient($val){ $this->_orient = $val; }

		protected function getCode(){ return $this->_code; }
		protected function setCode($val){ $this->_code = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\TwipsMeasure",
					"Consts\PageOrientation",
					"Consts\DecimalNumber"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->w === null && 
				$this->h === null && 
				$this->orient === null && 
				$this->code === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->w !== null) $xml->writeAttribute("w:w", $this->w);
			if ($this->h !== null) $xml->writeAttribute("w:h", $this->h);
			if ($this->orient !== null) $xml->writeAttribute("w:orient", $this->orient);
			if ($this->code !== null) $xml->writeAttribute("w:code", $this->code);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>