<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_FFDDList.html 
	 * @property-read DecimalNumber $result Drop-Down List Selection
	 * @property-read DecimalNumber $default Default Drop-Down List Item Index
	 * @property-read String[] $listEntry Drop-Down List Entry
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class FFDDList extends Object
	{
		
		/* --- Childrens --- */

		private $_result;
		private $_default;
		private $_listEntry;

		
		/* --- Childrens --- */

		protected function getResult(){ return $this->_result->get(); }

		protected function getDefault(){ return $this->_default->get(); }

		protected function getListEntry(){ return $this->_listEntry->get(); }
		public function createListEntry(){ $new = new \Word\Objects\String("w:listEntry"); $this->listEntry->AddRef($new); return $new; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\DecimalNumber",
					"Objects\String"
				)
			);
				
			parent::__construct($name);

			$this->_result = new \Word\PropertyObject('\Word\Objects\DecimalNumber', "w:result", $this);
			$this->_default = new \Word\PropertyObject('\Word\Objects\DecimalNumber', "w:default", $this);
			$this->_listEntry = new \Word\PropertyArray($this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 

				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{

		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>