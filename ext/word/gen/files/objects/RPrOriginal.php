<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_RPrOriginal.html 
	 * @property-read String $rStyle Referenced Character Style
	 * @property-read Fonts $rFonts Run Fonts
	 * @property-read OnOff $b Bold
	 * @property-read OnOff $bCs Complex Script Bold
	 * @property-read OnOff $i Italics
	 * @property-read OnOff $iCs Complex Script Italics
	 * @property-read OnOff $caps Display All Characters As Capital Letters
	 * @property-read OnOff $smallCaps Small Caps
	 * @property-read OnOff $strike Single Strikethrough
	 * @property-read OnOff $dstrike Double Strikethrough
	 * @property-read OnOff $outline Display Character Outline
	 * @property-read OnOff $shadow Shadow
	 * @property-read OnOff $emboss Embossing
	 * @property-read OnOff $imprint Imprinting
	 * @property-read OnOff $noProof Do Not Check Spelling or Grammar
	 * @property-read OnOff $snapToGrid Use Document Grid Settings For Inter-Character Spacing
	 * @property-read OnOff $vanish Hidden Text
	 * @property-read OnOff $webHidden Web Hidden Text
	 * @property-read Color $color Run Content Color
	 * @property-read SignedTwipsMeasure $spacing Character Spacing Adjustment
	 * @property-read TextScale $w Expanded/Compressed Text
	 * @property-read HpsMeasure $kern Font Kerning
	 * @property-read SignedHpsMeasure $position Vertically Raised or Lowered Text
	 * @property-read HpsMeasure $sz Font Size
	 * @property-read HpsMeasure $szCs Complex Script Font Size
	 * @property-read Highlight $highlight Text Highlighting
	 * @property-read Underline $u Underline
	 * @property-read TextEffect $effect Animated Text Effect
	 * @property-read Border $bdr Text Border
	 * @property-read Shd $shd Run Shading
	 * @property-read FitText $fitText Manual Run Width
	 * @property-read VerticalAlignRun $vertAlign Subscript/Superscript Text
	 * @property-read OnOff $rtl Right To Left Text
	 * @property-read OnOff $cs Use Complex Script Formatting on Run
	 * @property-read Em $em Emphasis Mark
	 * @property-read Language $lang Languages for Run Content
	 * @property-read EastAsianLayout $eastAsianLayout East Asian Typography Settings
	 * @property-read OnOff $specVanish Paragraph Mark Is Always Hidden
	 * @property-read OnOff $oMath Office Open XML Math
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class RPrOriginal extends Object
	{
		
		/* --- Childrens --- */

		private $_rStyle;
		private $_rFonts;
		private $_b;
		private $_bCs;
		private $_i;
		private $_iCs;
		private $_caps;
		private $_smallCaps;
		private $_strike;
		private $_dstrike;
		private $_outline;
		private $_shadow;
		private $_emboss;
		private $_imprint;
		private $_noProof;
		private $_snapToGrid;
		private $_vanish;
		private $_webHidden;
		private $_color;
		private $_spacing;
		private $_w;
		private $_kern;
		private $_position;
		private $_sz;
		private $_szCs;
		private $_highlight;
		private $_u;
		private $_effect;
		private $_bdr;
		private $_shd;
		private $_fitText;
		private $_vertAlign;
		private $_rtl;
		private $_cs;
		private $_em;
		private $_lang;
		private $_eastAsianLayout;
		private $_specVanish;
		private $_oMath;

		
		/* --- Childrens --- */

		protected function getRStyle(){ return $this->_rStyle->get(); }

		protected function getRFonts(){ return $this->_rFonts->get(); }

		protected function getB(){ return $this->_b->get(); }

		protected function getBCs(){ return $this->_bCs->get(); }

		protected function getI(){ return $this->_i->get(); }

		protected function getICs(){ return $this->_iCs->get(); }

		protected function getCaps(){ return $this->_caps->get(); }

		protected function getSmallCaps(){ return $this->_smallCaps->get(); }

		protected function getStrike(){ return $this->_strike->get(); }

		protected function getDstrike(){ return $this->_dstrike->get(); }

		protected function getOutline(){ return $this->_outline->get(); }

		protected function getShadow(){ return $this->_shadow->get(); }

		protected function getEmboss(){ return $this->_emboss->get(); }

		protected function getImprint(){ return $this->_imprint->get(); }

		protected function getNoProof(){ return $this->_noProof->get(); }

		protected function getSnapToGrid(){ return $this->_snapToGrid->get(); }

		protected function getVanish(){ return $this->_vanish->get(); }

		protected function getWebHidden(){ return $this->_webHidden->get(); }

		protected function getColor(){ return $this->_color->get(); }

		protected function getSpacing(){ return $this->_spacing->get(); }

		protected function getW(){ return $this->_w->get(); }

		protected function getKern(){ return $this->_kern->get(); }

		protected function getPosition(){ return $this->_position->get(); }

		protected function getSz(){ return $this->_sz->get(); }

		protected function getSzCs(){ return $this->_szCs->get(); }

		protected function getHighlight(){ return $this->_highlight->get(); }

		protected function getU(){ return $this->_u->get(); }

		protected function getEffect(){ return $this->_effect->get(); }

		protected function getBdr(){ return $this->_bdr->get(); }

		protected function getShd(){ return $this->_shd->get(); }

		protected function getFitText(){ return $this->_fitText->get(); }

		protected function getVertAlign(){ return $this->_vertAlign->get(); }

		protected function getRtl(){ return $this->_rtl->get(); }

		protected function getCs(){ return $this->_cs->get(); }

		protected function getEm(){ return $this->_em->get(); }

		protected function getLang(){ return $this->_lang->get(); }

		protected function getEastAsianLayout(){ return $this->_eastAsianLayout->get(); }

		protected function getSpecVanish(){ return $this->_specVanish->get(); }

		protected function getOMath(){ return $this->_oMath->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\String",
					"Objects\Fonts",
					"Objects\OnOff",
					"Objects\Color",
					"Objects\SignedTwipsMeasure",
					"Objects\TextScale",
					"Objects\HpsMeasure",
					"Objects\SignedHpsMeasure",
					"Objects\Highlight",
					"Objects\Underline",
					"Objects\TextEffect",
					"Objects\Border",
					"Objects\Shd",
					"Objects\FitText",
					"Objects\VerticalAlignRun",
					"Objects\Em",
					"Objects\Language",
					"Objects\EastAsianLayout"
				)
			);
				
			parent::__construct($name);

			$this->_rStyle = new \Word\PropertyObject('\Word\Objects\String', "w:rStyle", $this);
			$this->_rFonts = new \Word\PropertyObject('\Word\Objects\Fonts', "w:rFonts", $this);
			$this->_b = new \Word\PropertyObject('\Word\Objects\OnOff', "w:b", $this);
			$this->_bCs = new \Word\PropertyObject('\Word\Objects\OnOff', "w:bCs", $this);
			$this->_i = new \Word\PropertyObject('\Word\Objects\OnOff', "w:i", $this);
			$this->_iCs = new \Word\PropertyObject('\Word\Objects\OnOff', "w:iCs", $this);
			$this->_caps = new \Word\PropertyObject('\Word\Objects\OnOff', "w:caps", $this);
			$this->_smallCaps = new \Word\PropertyObject('\Word\Objects\OnOff', "w:smallCaps", $this);
			$this->_strike = new \Word\PropertyObject('\Word\Objects\OnOff', "w:strike", $this);
			$this->_dstrike = new \Word\PropertyObject('\Word\Objects\OnOff', "w:dstrike", $this);
			$this->_outline = new \Word\PropertyObject('\Word\Objects\OnOff', "w:outline", $this);
			$this->_shadow = new \Word\PropertyObject('\Word\Objects\OnOff', "w:shadow", $this);
			$this->_emboss = new \Word\PropertyObject('\Word\Objects\OnOff', "w:emboss", $this);
			$this->_imprint = new \Word\PropertyObject('\Word\Objects\OnOff', "w:imprint", $this);
			$this->_noProof = new \Word\PropertyObject('\Word\Objects\OnOff', "w:noProof", $this);
			$this->_snapToGrid = new \Word\PropertyObject('\Word\Objects\OnOff', "w:snapToGrid", $this);
			$this->_vanish = new \Word\PropertyObject('\Word\Objects\OnOff', "w:vanish", $this);
			$this->_webHidden = new \Word\PropertyObject('\Word\Objects\OnOff', "w:webHidden", $this);
			$this->_color = new \Word\PropertyObject('\Word\Objects\Color', "w:color", $this);
			$this->_spacing = new \Word\PropertyObject('\Word\Objects\SignedTwipsMeasure', "w:spacing", $this);
			$this->_w = new \Word\PropertyObject('\Word\Objects\TextScale', "w:w", $this);
			$this->_kern = new \Word\PropertyObject('\Word\Objects\HpsMeasure', "w:kern", $this);
			$this->_position = new \Word\PropertyObject('\Word\Objects\SignedHpsMeasure', "w:position", $this);
			$this->_sz = new \Word\PropertyObject('\Word\Objects\HpsMeasure', "w:sz", $this);
			$this->_szCs = new \Word\PropertyObject('\Word\Objects\HpsMeasure', "w:szCs", $this);
			$this->_highlight = new \Word\PropertyObject('\Word\Objects\Highlight', "w:highlight", $this);
			$this->_u = new \Word\PropertyObject('\Word\Objects\Underline', "w:u", $this);
			$this->_effect = new \Word\PropertyObject('\Word\Objects\TextEffect', "w:effect", $this);
			$this->_bdr = new \Word\PropertyObject('\Word\Objects\Border', "w:bdr", $this);
			$this->_shd = new \Word\PropertyObject('\Word\Objects\Shd', "w:shd", $this);
			$this->_fitText = new \Word\PropertyObject('\Word\Objects\FitText', "w:fitText", $this);
			$this->_vertAlign = new \Word\PropertyObject('\Word\Objects\VerticalAlignRun', "w:vertAlign", $this);
			$this->_rtl = new \Word\PropertyObject('\Word\Objects\OnOff', "w:rtl", $this);
			$this->_cs = new \Word\PropertyObject('\Word\Objects\OnOff', "w:cs", $this);
			$this->_em = new \Word\PropertyObject('\Word\Objects\Em', "w:em", $this);
			$this->_lang = new \Word\PropertyObject('\Word\Objects\Language', "w:lang", $this);
			$this->_eastAsianLayout = new \Word\PropertyObject('\Word\Objects\EastAsianLayout', "w:eastAsianLayout", $this);
			$this->_specVanish = new \Word\PropertyObject('\Word\Objects\OnOff', "w:specVanish", $this);
			$this->_oMath = new \Word\PropertyObject('\Word\Objects\OnOff', "w:oMath", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 

				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{

		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>