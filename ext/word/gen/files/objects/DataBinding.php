<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_DataBinding.html 
	 * @property-read String $prefixMappings XML Namespace Prefix Mappings
	 * @property-read String $xpath XPath
	 * @property-read String $storeItemID Custom XML Data Storage ID
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class DataBinding extends Object
	{
		
		/* --- Attributes --- */

		private $_prefixMappings;
		private $_xpath;
		private $_storeItemID;

		
		/* --- Attributes --- */

		protected function getPrefixMappings(){ return $this->_prefixMappings; }
		protected function setPrefixMappings($val){ $this->_prefixMappings = $val; }

		protected function getXpath(){ return $this->_xpath; }
		protected function setXpath($val){ $this->_xpath = $val; }

		protected function getStoreItemID(){ return $this->_storeItemID; }
		protected function setStoreItemID($val){ $this->_storeItemID = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\String"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->prefixMappings === null && 
				$this->xpath === null && 
				$this->storeItemID === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->prefixMappings !== null) $xml->writeAttribute("w:prefixMappings", $this->prefixMappings);
			if ($this->xpath !== null) $xml->writeAttribute("w:xpath", $this->xpath);
			if ($this->storeItemID !== null) $xml->writeAttribute("w:storeItemID", $this->storeItemID);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return
				$this->xpath !== null && 
			/* --- Attributes --- */

				$this->storeItemID !== null && 


				true;
		}
		
		
	}
	
}

?>