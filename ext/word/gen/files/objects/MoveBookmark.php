<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_MoveBookmark.html 
	 * @property-read DecimalNumber $id Annotation Identifier
	 * @property-read DisplacedByCustomXml $displacedByCustomXml Annotation Marker Relocated For Custom XML Markup
	 * @property-read DecimalNumber $colFirst First Table Column Covered By Bookmark
	 * @property-read DecimalNumber $colLast Last Table Column Covered By Bookmark
	 * @property-read String $name Bookmark Name
	 * @property-read String $author Annotation Author
	 * @property-read DateTime $date Annotation Date
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class MoveBookmark extends Object
	{
		
		/* --- Attributes --- */

		private $_id;
		private $_displacedByCustomXml;
		private $_colFirst;
		private $_colLast;
		private $_name;
		private $_author;
		private $_date;

		
		/* --- Attributes --- */

		protected function getId(){ return $this->_id; }
		protected function setId($val){ $this->_id = $val; }

		protected function getDisplacedByCustomXml(){ return $this->_displacedByCustomXml; }
		protected function setDisplacedByCustomXml($val){ $this->_displacedByCustomXml = $val; }

		protected function getColFirst(){ return $this->_colFirst; }
		protected function setColFirst($val){ $this->_colFirst = $val; }

		protected function getColLast(){ return $this->_colLast; }
		protected function setColLast($val){ $this->_colLast = $val; }

		protected function getName(){ return $this->_name; }
		protected function setName($val){ $this->_name = $val; }

		protected function getAuthor(){ return $this->_author; }
		protected function setAuthor($val){ $this->_author = $val; }

		protected function getDate(){ return $this->_date; }
		protected function setDate($val){ $this->_date = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\DecimalNumber",
					"Consts\DisplacedByCustomXml",
					"Consts\String",
					"Consts\DateTime"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->id === null && 
				$this->displacedByCustomXml === null && 
				$this->colFirst === null && 
				$this->colLast === null && 
				$this->name === null && 
				$this->author === null && 
				$this->date === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->id !== null) $xml->writeAttribute("w:id", $this->id);
			if ($this->displacedByCustomXml !== null) $xml->writeAttribute("w:displacedByCustomXml", $this->displacedByCustomXml);
			if ($this->colFirst !== null) $xml->writeAttribute("w:colFirst", $this->colFirst);
			if ($this->colLast !== null) $xml->writeAttribute("w:colLast", $this->colLast);
			if ($this->name !== null) $xml->writeAttribute("w:name", $this->name);
			if ($this->author !== null) $xml->writeAttribute("w:author", $this->author);
			if ($this->date !== null) $xml->writeAttribute("w:date", $this->date);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return
				$this->id !== null && 
			/* --- Attributes --- */

			/* --- Attributes --- */

			/* --- Attributes --- */

			/* --- Attributes --- */

				$this->name !== null && 
			/* --- Attributes --- */

				$this->author !== null && 
			/* --- Attributes --- */

				$this->date !== null && 


				true;
		}
		
		
	}
	
}

?>