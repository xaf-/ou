<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_DocDefaults.html 
	 * @property-read RPrDefault $rPrDefault Default Run Properties
	 * @property-read PPrDefault $pPrDefault Default Paragraph Properties
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class DocDefaults extends Object
	{
		
		/* --- Childrens --- */

		private $_rPrDefault;
		private $_pPrDefault;

		
		/* --- Childrens --- */

		protected function getRPrDefault(){ return $this->_rPrDefault->get(); }

		protected function getPPrDefault(){ return $this->_pPrDefault->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\RPrDefault",
					"Objects\PPrDefault"
				)
			);
				
			parent::__construct($name);

			$this->_rPrDefault = new \Word\PropertyObject('\Word\Objects\RPrDefault', "w:rPrDefault", $this);
			$this->_pPrDefault = new \Word\PropertyObject('\Word\Objects\PPrDefault', "w:pPrDefault", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 

				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{

		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>