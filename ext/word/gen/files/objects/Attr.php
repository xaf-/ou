<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_Attr.html 
	 * @property-read String $uri Namespace
	 * @property-read String $name Name
	 * @property-read String $val Value
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class Attr extends Object
	{
		
		/* --- Attributes --- */

		private $_uri;
		private $_name;
		private $_val;

		
		/* --- Attributes --- */

		protected function getUri(){ return $this->_uri; }
		protected function setUri($val){ $this->_uri = $val; }

		protected function getName(){ return $this->_name; }
		protected function setName($val){ $this->_name = $val; }

		protected function getVal(){ return $this->_val; }
		protected function setVal($val){ $this->_val = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\String"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->uri === null && 
				$this->name === null && 
				$this->val === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->uri !== null) $xml->writeAttribute("w:uri", $this->uri);
			if ($this->name !== null) $xml->writeAttribute("w:name", $this->name);
			if ($this->val !== null) $xml->writeAttribute("w:val", $this->val);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return
				$this->name !== null && 
			/* --- Attributes --- */

				$this->val !== null && 


				true;
		}
		
		
	}
	
}

?>