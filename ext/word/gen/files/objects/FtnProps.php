<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_FtnProps.html 
	 * @property-read FtnPos $pos Footnote Placement
	 * @property-read NumFmt $numFmt Footnote Numbering Format
	 * @property-read DecimalNumber $numStart Footnote and Endnote Numbering Starting Value
	 * @property-read NumRestart $numRestart Footnote and Endnote Numbering Restart Location
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class FtnProps extends Object
	{
		
		/* --- Childrens --- */

		private $_pos;
		private $_numFmt;
		private $_numStart;
		private $_numRestart;

		
		/* --- Childrens --- */

		protected function getPos(){ return $this->_pos->get(); }

		protected function getNumFmt(){ return $this->_numFmt->get(); }

		protected function getNumStart(){ return $this->_numStart->get(); }

		protected function getNumRestart(){ return $this->_numRestart->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\FtnPos",
					"Objects\NumFmt",
					"Objects\DecimalNumber",
					"Objects\NumRestart"
				)
			);
				
			parent::__construct($name);

			$this->_pos = new \Word\PropertyObject('\Word\Objects\FtnPos', "w:pos", $this);
			$this->_numFmt = new \Word\PropertyObject('\Word\Objects\NumFmt', "w:numFmt", $this);
			$this->_numStart = new \Word\PropertyObject('\Word\Objects\DecimalNumber', "w:numStart", $this);
			$this->_numRestart = new \Word\PropertyObject('\Word\Objects\NumRestart', "w:numRestart", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 

				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{

		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>