<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_Document.html 
	 * @property-read Background $background Document Background
	 * @property-read Body $body Document Body
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class Document extends Object
	{
		
		/* --- Childrens --- */

		private $_background;
		private $_body;

		
		/* --- Childrens --- */

		protected function getBackground(){ return $this->_background->get(); }

		protected function getBody(){ return $this->_body->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\Background",
					"Objects\Body"
				)
			);
				
			parent::__construct($name);

			$this->_background = new \Word\PropertyObject('\Word\Objects\Background', "w:background", $this);
			$this->_body = new \Word\PropertyObject('\Word\Objects\Body', "w:body", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 

				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{

		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>