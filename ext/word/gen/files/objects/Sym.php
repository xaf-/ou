<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_Sym.html 
	 * @property-read String $font Symbol Character Font
	 * @property-read ShortHexNumber $char Symbol Character Code
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class Sym extends Object
	{
		
		/* --- Attributes --- */

		private $_font;
		private $_char;

		
		/* --- Attributes --- */

		protected function getFont(){ return $this->_font; }
		protected function setFont($val){ $this->_font = $val; }

		protected function getChar(){ return $this->_char; }
		protected function setChar($val){ $this->_char = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\String",
					"Consts\ShortHexNumber"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->font === null && 
				$this->char === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->font !== null) $xml->writeAttribute("w:font", $this->font);
			if ($this->char !== null) $xml->writeAttribute("w:char", $this->char);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>