<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_Style.html 
	 * @property-read String $name Primary Style Name
	 * @property-read String $aliases Alternate Style Names
	 * @property-read String $basedOn Parent Style ID
	 * @property-read String $next Style For Next Paragraph
	 * @property-read String $link Linked Style Reference
	 * @property-read OnOff $autoRedefine Automatically Merge User Formatting Into Style Definition
	 * @property-read OnOff $hidden Hide Style From User Interface
	 * @property-read DecimalNumber $uiPriority Optional User Interface Sorting Order
	 * @property-read OnOff $semiHidden Hide Style From Main User Interface
	 * @property-read OnOff $unhideWhenUsed Remove Semi-Hidden Property When Style Is Used
	 * @property-read OnOff $qFormat Primary Style
	 * @property-read OnOff $locked Style Cannot Be Applied
	 * @property-read OnOff $personal E-Mail Message Text Style
	 * @property-read OnOff $personalCompose E-Mail Message Composition Style
	 * @property-read OnOff $personalReply E-Mail Message Reply Style
	 * @property-read LongHexNumber $rsid Revision Identifier for Style Definition
	 * @property-read PPr $pPr Style Paragraph Properties
	 * @property-read RPr $rPr Run Properties
	 * @property-read TblPrBase $tblPr Style Table Properties
	 * @property-read TrPr $trPr Style Table Row Properties
	 * @property-read TcPr $tcPr Style Table Cell Properties
	 * @property-read TblStylePr[] $tblStylePr Style Conditional Table Formatting Properties
	 * @property-read StyleType $type Style Type
	 * @property-read String $styleId Style ID
	 * @property-read OnOff $default Default Style
	 * @property-read OnOff $customStyle User-Defined Style
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class Style extends Object
	{
		
		/* --- Attributes --- */

		private $_type;
		private $_styleId;
		private $_default;
		private $_customStyle;

		/* --- Childrens --- */

		private $_name;
		private $_aliases;
		private $_basedOn;
		private $_next;
		private $_link;
		private $_autoRedefine;
		private $_hidden;
		private $_uiPriority;
		private $_semiHidden;
		private $_unhideWhenUsed;
		private $_qFormat;
		private $_locked;
		private $_personal;
		private $_personalCompose;
		private $_personalReply;
		private $_rsid;
		private $_pPr;
		private $_rPr;
		private $_tblPr;
		private $_trPr;
		private $_tcPr;
		private $_tblStylePr;

		
		/* --- Attributes --- */

		protected function getType(){ return $this->_type; }
		protected function setType($val){ $this->_type = $val; }

		protected function getStyleId(){ return $this->_styleId; }
		protected function setStyleId($val){ $this->_styleId = $val; }

		protected function getDefault(){ return $this->_default; }
		protected function setDefault($val){ $this->_default = $val; }

		protected function getCustomStyle(){ return $this->_customStyle; }
		protected function setCustomStyle($val){ $this->_customStyle = $val; }

		/* --- Childrens --- */

		protected function getName(){ return $this->_name->get(); }

		protected function getAliases(){ return $this->_aliases->get(); }

		protected function getBasedOn(){ return $this->_basedOn->get(); }

		protected function getNext(){ return $this->_next->get(); }

		protected function getLink(){ return $this->_link->get(); }

		protected function getAutoRedefine(){ return $this->_autoRedefine->get(); }

		protected function getHidden(){ return $this->_hidden->get(); }

		protected function getUiPriority(){ return $this->_uiPriority->get(); }

		protected function getSemiHidden(){ return $this->_semiHidden->get(); }

		protected function getUnhideWhenUsed(){ return $this->_unhideWhenUsed->get(); }

		protected function getQFormat(){ return $this->_qFormat->get(); }

		protected function getLocked(){ return $this->_locked->get(); }

		protected function getPersonal(){ return $this->_personal->get(); }

		protected function getPersonalCompose(){ return $this->_personalCompose->get(); }

		protected function getPersonalReply(){ return $this->_personalReply->get(); }

		protected function getRsid(){ return $this->_rsid->get(); }

		protected function getPPr(){ return $this->_pPr->get(); }

		protected function getRPr(){ return $this->_rPr->get(); }

		protected function getTblPr(){ return $this->_tblPr->get(); }

		protected function getTrPr(){ return $this->_trPr->get(); }

		protected function getTcPr(){ return $this->_tcPr->get(); }

		protected function getTblStylePr(){ return $this->_tblStylePr->get(); }
		public function createTblStylePr(){ $new = new \Word\Objects\TblStylePr("w:tblStylePr"); $this->tblStylePr->AddRef($new); return $new; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\String",
					"Objects\OnOff",
					"Objects\DecimalNumber",
					"Objects\LongHexNumber",
					"Objects\PPr",
					"Objects\RPr",
					"Objects\TblPrBase",
					"Objects\TrPr",
					"Objects\TcPr",
					"Objects\TblStylePr",
					"Consts\StyleType"
				)
			);
				
			parent::__construct($name);

			$this->_name = new \Word\PropertyObject('\Word\Objects\String', "w:name", $this);
			$this->_aliases = new \Word\PropertyObject('\Word\Objects\String', "w:aliases", $this);
			$this->_basedOn = new \Word\PropertyObject('\Word\Objects\String', "w:basedOn", $this);
			$this->_next = new \Word\PropertyObject('\Word\Objects\String', "w:next", $this);
			$this->_link = new \Word\PropertyObject('\Word\Objects\String', "w:link", $this);
			$this->_autoRedefine = new \Word\PropertyObject('\Word\Objects\OnOff', "w:autoRedefine", $this);
			$this->_hidden = new \Word\PropertyObject('\Word\Objects\OnOff', "w:hidden", $this);
			$this->_uiPriority = new \Word\PropertyObject('\Word\Objects\DecimalNumber', "w:uiPriority", $this);
			$this->_semiHidden = new \Word\PropertyObject('\Word\Objects\OnOff', "w:semiHidden", $this);
			$this->_unhideWhenUsed = new \Word\PropertyObject('\Word\Objects\OnOff', "w:unhideWhenUsed", $this);
			$this->_qFormat = new \Word\PropertyObject('\Word\Objects\OnOff', "w:qFormat", $this);
			$this->_locked = new \Word\PropertyObject('\Word\Objects\OnOff', "w:locked", $this);
			$this->_personal = new \Word\PropertyObject('\Word\Objects\OnOff', "w:personal", $this);
			$this->_personalCompose = new \Word\PropertyObject('\Word\Objects\OnOff', "w:personalCompose", $this);
			$this->_personalReply = new \Word\PropertyObject('\Word\Objects\OnOff', "w:personalReply", $this);
			$this->_rsid = new \Word\PropertyObject('\Word\Objects\LongHexNumber', "w:rsid", $this);
			$this->_pPr = new \Word\PropertyObject('\Word\Objects\PPr', "w:pPr", $this);
			$this->_rPr = new \Word\PropertyObject('\Word\Objects\RPr', "w:rPr", $this);
			$this->_tblPr = new \Word\PropertyObject('\Word\Objects\TblPrBase', "w:tblPr", $this);
			$this->_trPr = new \Word\PropertyObject('\Word\Objects\TrPr', "w:trPr", $this);
			$this->_tcPr = new \Word\PropertyObject('\Word\Objects\TcPr', "w:tcPr", $this);
			$this->_tblStylePr = new \Word\PropertyArray($this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->type === null && 
				$this->styleId === null && 
				$this->default === null && 
				$this->customStyle === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->type !== null) $xml->writeAttribute("w:type", $this->type);
			if ($this->styleId !== null) $xml->writeAttribute("w:styleId", $this->styleId);
			if ($this->default !== null) $xml->writeAttribute("w:default", $this->default);
			if ($this->customStyle !== null) $xml->writeAttribute("w:customStyle", $this->customStyle);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>