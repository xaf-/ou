<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_RunTrackChange.html 
	 * @property-read SmartTagRun[] $smartTag Inline-Level Smart Tag
	 * @property-read SdtRun[] $sdt Inline-Level Structured Document Tag
	 * @property-read R[] $r Text Run
	 * @property-read ProofErr $proofErr Proofing Error Anchor
	 * @property-read PermStart $permStart Range Permission Start
	 * @property-read Perm $permEnd Range Permission End
	 * @property-read Bookmark[] $bookmarkStart Bookmark Start
	 * @property-read MarkupRange[] $bookmarkEnd Bookmark End
	 * @property-read MoveBookmark[] $moveFromRangeStart Move Source Location Container - Start
	 * @property-read MarkupRange[] $moveFromRangeEnd Move Source Location Container - End
	 * @property-read MoveBookmark[] $moveToRangeStart Move Destination Location Container - Start
	 * @property-read MarkupRange[] $moveToRangeEnd Move Destination Location Container - End
	 * @property-read MarkupRange[] $commentRangeStart Comment Anchor Range Start
	 * @property-read MarkupRange[] $commentRangeEnd Comment Anchor Range End
	 * @property-read TrackChange[] $customXmlInsRangeStart Custom XML Markup Insertion Start
	 * @property-read Markup[] $customXmlInsRangeEnd Custom XML Markup Insertion End
	 * @property-read TrackChange[] $customXmlDelRangeStart Custom XML Markup Deletion Start
	 * @property-read Markup[] $customXmlDelRangeEnd Custom XML Markup Deletion End
	 * @property-read TrackChange[] $customXmlMoveFromRangeStart Custom XML Markup Move Source Start
	 * @property-read Markup[] $customXmlMoveFromRangeEnd Custom XML Markup Move Source End
	 * @property-read TrackChange[] $customXmlMoveToRangeStart Custom XML Markup Move Destination Location Start
	 * @property-read Markup[] $customXmlMoveToRangeEnd Custom XML Markup Move Destination Location End
	 * @property-read RunTrackChange $ins Inserted Run Content
	 * @property-read RunTrackChange $del Deleted Run Content
	 * @property-read RunTrackChange[] $moveFrom Move Source Run Content
	 * @property-read RunTrackChange[] $moveTo Move Destination Run Content
	 * @property-read OMath[] $oMath 
	 * @property-read Acc[] $acc Accent
	 * @property-read Bar[] $bar Bar
	 * @property-read Box[] $box Box Function
	 * @property-read BorderBox[] $borderBox Border-Box Function
	 * @property-read D[] $d Delimiter Function
	 * @property-read EqArr[] $eqArr Equation-Array Function
	 * @property-read F[] $f Fraction Function
	 * @property-read Func[] $func Function Apply Function
	 * @property-read GroupChr[] $groupChr Group-Character Function
	 * @property-read LimLow[] $limLow Lower-Limit Function
	 * @property-read LimUpp[] $limUpp Upper-Limit Function
	 * @property-read M[] $m Matrix Function
	 * @property-read Nary[] $nary n-ary Operator Function
	 * @property-read Phant[] $phant Phantom Function
	 * @property-read Rad[] $rad Radical Function
	 * @property-read SPre[] $sPre Pre-Sub-Superscript Function
	 * @property-read SSub[] $sSub Subscript Function
	 * @property-read SSubSup[] $sSubSup Sub-Superscript Function
	 * @property-read SSup[] $sSup Superscript Function
	 * @property-read DecimalNumber $id Annotation Identifier
	 * @property-read String $author Annotation Author
	 * @property-read DateTime $date Annotation Date
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class RunTrackChange extends Object
	{
		
		/* --- Attributes --- */

		private $_id;
		private $_author;
		private $_date;

		/* --- Childrens --- */

		private $_smartTag;
		private $_sdt;
		private $_r;
		private $_proofErr;
		private $_permStart;
		private $_permEnd;
		private $_bookmarkStart;
		private $_bookmarkEnd;
		private $_moveFromRangeStart;
		private $_moveFromRangeEnd;
		private $_moveToRangeStart;
		private $_moveToRangeEnd;
		private $_commentRangeStart;
		private $_commentRangeEnd;
		private $_customXmlInsRangeStart;
		private $_customXmlInsRangeEnd;
		private $_customXmlDelRangeStart;
		private $_customXmlDelRangeEnd;
		private $_customXmlMoveFromRangeStart;
		private $_customXmlMoveFromRangeEnd;
		private $_customXmlMoveToRangeStart;
		private $_customXmlMoveToRangeEnd;
		private $_ins;
		private $_del;
		private $_moveFrom;
		private $_moveTo;
		private $_oMath;
		private $_acc;
		private $_bar;
		private $_box;
		private $_borderBox;
		private $_d;
		private $_eqArr;
		private $_f;
		private $_func;
		private $_groupChr;
		private $_limLow;
		private $_limUpp;
		private $_m;
		private $_nary;
		private $_phant;
		private $_rad;
		private $_sPre;
		private $_sSub;
		private $_sSubSup;
		private $_sSup;

		
		/* --- Attributes --- */

		protected function getId(){ return $this->_id; }
		protected function setId($val){ $this->_id = $val; }

		protected function getAuthor(){ return $this->_author; }
		protected function setAuthor($val){ $this->_author = $val; }

		protected function getDate(){ return $this->_date; }
		protected function setDate($val){ $this->_date = $val; }

		/* --- Childrens --- */

		protected function getSmartTag(){ return $this->_smartTag->get(); }
		public function createSmartTag(){ $new = new \Word\Objects\SmartTagRun("w:smartTag"); $this->smartTag->AddRef($new); return $new; }

		protected function getSdt(){ return $this->_sdt->get(); }
		public function createSdt(){ $new = new \Word\Objects\SdtRun("w:sdt"); $this->sdt->AddRef($new); return $new; }

		protected function getR(){ return $this->_r->get(); }
		public function createR(){ $new = new \Word\Objects\R("w:r"); $this->r->AddRef($new); return $new; }

		protected function getProofErr(){ return $this->_proofErr->get(); }

		protected function getPermStart(){ return $this->_permStart->get(); }

		protected function getPermEnd(){ return $this->_permEnd->get(); }

		protected function getBookmarkStart(){ return $this->_bookmarkStart->get(); }
		public function createBookmarkStart(){ $new = new \Word\Objects\Bookmark("w:bookmarkStart"); $this->bookmarkStart->AddRef($new); return $new; }

		protected function getBookmarkEnd(){ return $this->_bookmarkEnd->get(); }
		public function createBookmarkEnd(){ $new = new \Word\Objects\MarkupRange("w:bookmarkEnd"); $this->bookmarkEnd->AddRef($new); return $new; }

		protected function getMoveFromRangeStart(){ return $this->_moveFromRangeStart->get(); }
		public function createMoveFromRangeStart(){ $new = new \Word\Objects\MoveBookmark("w:moveFromRangeStart"); $this->moveFromRangeStart->AddRef($new); return $new; }

		protected function getMoveFromRangeEnd(){ return $this->_moveFromRangeEnd->get(); }
		public function createMoveFromRangeEnd(){ $new = new \Word\Objects\MarkupRange("w:moveFromRangeEnd"); $this->moveFromRangeEnd->AddRef($new); return $new; }

		protected function getMoveToRangeStart(){ return $this->_moveToRangeStart->get(); }
		public function createMoveToRangeStart(){ $new = new \Word\Objects\MoveBookmark("w:moveToRangeStart"); $this->moveToRangeStart->AddRef($new); return $new; }

		protected function getMoveToRangeEnd(){ return $this->_moveToRangeEnd->get(); }
		public function createMoveToRangeEnd(){ $new = new \Word\Objects\MarkupRange("w:moveToRangeEnd"); $this->moveToRangeEnd->AddRef($new); return $new; }

		protected function getCommentRangeStart(){ return $this->_commentRangeStart->get(); }
		public function createCommentRangeStart(){ $new = new \Word\Objects\MarkupRange("w:commentRangeStart"); $this->commentRangeStart->AddRef($new); return $new; }

		protected function getCommentRangeEnd(){ return $this->_commentRangeEnd->get(); }
		public function createCommentRangeEnd(){ $new = new \Word\Objects\MarkupRange("w:commentRangeEnd"); $this->commentRangeEnd->AddRef($new); return $new; }

		protected function getCustomXmlInsRangeStart(){ return $this->_customXmlInsRangeStart->get(); }
		public function createCustomXmlInsRangeStart(){ $new = new \Word\Objects\TrackChange("w:customXmlInsRangeStart"); $this->customXmlInsRangeStart->AddRef($new); return $new; }

		protected function getCustomXmlInsRangeEnd(){ return $this->_customXmlInsRangeEnd->get(); }
		public function createCustomXmlInsRangeEnd(){ $new = new \Word\Objects\Markup("w:customXmlInsRangeEnd"); $this->customXmlInsRangeEnd->AddRef($new); return $new; }

		protected function getCustomXmlDelRangeStart(){ return $this->_customXmlDelRangeStart->get(); }
		public function createCustomXmlDelRangeStart(){ $new = new \Word\Objects\TrackChange("w:customXmlDelRangeStart"); $this->customXmlDelRangeStart->AddRef($new); return $new; }

		protected function getCustomXmlDelRangeEnd(){ return $this->_customXmlDelRangeEnd->get(); }
		public function createCustomXmlDelRangeEnd(){ $new = new \Word\Objects\Markup("w:customXmlDelRangeEnd"); $this->customXmlDelRangeEnd->AddRef($new); return $new; }

		protected function getCustomXmlMoveFromRangeStart(){ return $this->_customXmlMoveFromRangeStart->get(); }
		public function createCustomXmlMoveFromRangeStart(){ $new = new \Word\Objects\TrackChange("w:customXmlMoveFromRangeStart"); $this->customXmlMoveFromRangeStart->AddRef($new); return $new; }

		protected function getCustomXmlMoveFromRangeEnd(){ return $this->_customXmlMoveFromRangeEnd->get(); }
		public function createCustomXmlMoveFromRangeEnd(){ $new = new \Word\Objects\Markup("w:customXmlMoveFromRangeEnd"); $this->customXmlMoveFromRangeEnd->AddRef($new); return $new; }

		protected function getCustomXmlMoveToRangeStart(){ return $this->_customXmlMoveToRangeStart->get(); }
		public function createCustomXmlMoveToRangeStart(){ $new = new \Word\Objects\TrackChange("w:customXmlMoveToRangeStart"); $this->customXmlMoveToRangeStart->AddRef($new); return $new; }

		protected function getCustomXmlMoveToRangeEnd(){ return $this->_customXmlMoveToRangeEnd->get(); }
		public function createCustomXmlMoveToRangeEnd(){ $new = new \Word\Objects\Markup("w:customXmlMoveToRangeEnd"); $this->customXmlMoveToRangeEnd->AddRef($new); return $new; }

		protected function getIns(){ return $this->_ins->get(); }

		protected function getDel(){ return $this->_del->get(); }

		protected function getMoveFrom(){ return $this->_moveFrom->get(); }
		public function createMoveFrom(){ $new = new \Word\Objects\RunTrackChange("w:moveFrom"); $this->moveFrom->AddRef($new); return $new; }

		protected function getMoveTo(){ return $this->_moveTo->get(); }
		public function createMoveTo(){ $new = new \Word\Objects\RunTrackChange("w:moveTo"); $this->moveTo->AddRef($new); return $new; }

		protected function getOMath(){ return $this->_oMath->get(); }
		public function createOMath(){ $new = new \Word\Objects\OMath("m:oMath"); $this->oMath->AddRef($new); return $new; }

		protected function getAcc(){ return $this->_acc->get(); }
		public function createAcc(){ $new = new \Word\Objects\Acc("m:acc"); $this->acc->AddRef($new); return $new; }

		protected function getBar(){ return $this->_bar->get(); }
		public function createBar(){ $new = new \Word\Objects\Bar("m:bar"); $this->bar->AddRef($new); return $new; }

		protected function getBox(){ return $this->_box->get(); }
		public function createBox(){ $new = new \Word\Objects\Box("m:box"); $this->box->AddRef($new); return $new; }

		protected function getBorderBox(){ return $this->_borderBox->get(); }
		public function createBorderBox(){ $new = new \Word\Objects\BorderBox("m:borderBox"); $this->borderBox->AddRef($new); return $new; }

		protected function getD(){ return $this->_d->get(); }
		public function createD(){ $new = new \Word\Objects\D("m:d"); $this->d->AddRef($new); return $new; }

		protected function getEqArr(){ return $this->_eqArr->get(); }
		public function createEqArr(){ $new = new \Word\Objects\EqArr("m:eqArr"); $this->eqArr->AddRef($new); return $new; }

		protected function getF(){ return $this->_f->get(); }
		public function createF(){ $new = new \Word\Objects\F("m:f"); $this->f->AddRef($new); return $new; }

		protected function getFunc(){ return $this->_func->get(); }
		public function createFunc(){ $new = new \Word\Objects\Func("m:func"); $this->func->AddRef($new); return $new; }

		protected function getGroupChr(){ return $this->_groupChr->get(); }
		public function createGroupChr(){ $new = new \Word\Objects\GroupChr("m:groupChr"); $this->groupChr->AddRef($new); return $new; }

		protected function getLimLow(){ return $this->_limLow->get(); }
		public function createLimLow(){ $new = new \Word\Objects\LimLow("m:limLow"); $this->limLow->AddRef($new); return $new; }

		protected function getLimUpp(){ return $this->_limUpp->get(); }
		public function createLimUpp(){ $new = new \Word\Objects\LimUpp("m:limUpp"); $this->limUpp->AddRef($new); return $new; }

		protected function getM(){ return $this->_m->get(); }
		public function createM(){ $new = new \Word\Objects\M("m:m"); $this->m->AddRef($new); return $new; }

		protected function getNary(){ return $this->_nary->get(); }
		public function createNary(){ $new = new \Word\Objects\Nary("m:nary"); $this->nary->AddRef($new); return $new; }

		protected function getPhant(){ return $this->_phant->get(); }
		public function createPhant(){ $new = new \Word\Objects\Phant("m:phant"); $this->phant->AddRef($new); return $new; }

		protected function getRad(){ return $this->_rad->get(); }
		public function createRad(){ $new = new \Word\Objects\Rad("m:rad"); $this->rad->AddRef($new); return $new; }

		protected function getSPre(){ return $this->_sPre->get(); }
		public function createSPre(){ $new = new \Word\Objects\SPre("m:sPre"); $this->sPre->AddRef($new); return $new; }

		protected function getSSub(){ return $this->_sSub->get(); }
		public function createSSub(){ $new = new \Word\Objects\SSub("m:sSub"); $this->sSub->AddRef($new); return $new; }

		protected function getSSubSup(){ return $this->_sSubSup->get(); }
		public function createSSubSup(){ $new = new \Word\Objects\SSubSup("m:sSubSup"); $this->sSubSup->AddRef($new); return $new; }

		protected function getSSup(){ return $this->_sSup->get(); }
		public function createSSup(){ $new = new \Word\Objects\SSup("m:sSup"); $this->sSup->AddRef($new); return $new; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\SmartTagRun",
					"Objects\SdtRun",
					"Objects\R",
					"Objects\ProofErr",
					"Objects\PermStart",
					"Objects\Perm",
					"Objects\Bookmark",
					"Objects\MarkupRange",
					"Objects\MoveBookmark",
					"Objects\TrackChange",
					"Objects\Markup",
					"Objects\RunTrackChange",
					"Objects\OMath",
					"Objects\Acc",
					"Objects\Bar",
					"Objects\Box",
					"Objects\BorderBox",
					"Objects\D",
					"Objects\EqArr",
					"Objects\F",
					"Objects\Func",
					"Objects\GroupChr",
					"Objects\LimLow",
					"Objects\LimUpp",
					"Objects\M",
					"Objects\Nary",
					"Objects\Phant",
					"Objects\Rad",
					"Objects\SPre",
					"Objects\SSub",
					"Objects\SSubSup",
					"Objects\SSup",
					"Consts\DecimalNumber",
					"Consts\String",
					"Consts\DateTime"
				)
			);
				
			parent::__construct($name);

			$this->_smartTag = new \Word\PropertyArray($this);
			$this->_sdt = new \Word\PropertyArray($this);
			$this->_r = new \Word\PropertyArray($this);
			$this->_proofErr = new \Word\PropertyObject('\Word\Objects\ProofErr', "w:proofErr", $this);
			$this->_permStart = new \Word\PropertyObject('\Word\Objects\PermStart', "w:permStart", $this);
			$this->_permEnd = new \Word\PropertyObject('\Word\Objects\Perm', "w:permEnd", $this);
			$this->_bookmarkStart = new \Word\PropertyArray($this);
			$this->_bookmarkEnd = new \Word\PropertyArray($this);
			$this->_moveFromRangeStart = new \Word\PropertyArray($this);
			$this->_moveFromRangeEnd = new \Word\PropertyArray($this);
			$this->_moveToRangeStart = new \Word\PropertyArray($this);
			$this->_moveToRangeEnd = new \Word\PropertyArray($this);
			$this->_commentRangeStart = new \Word\PropertyArray($this);
			$this->_commentRangeEnd = new \Word\PropertyArray($this);
			$this->_customXmlInsRangeStart = new \Word\PropertyArray($this);
			$this->_customXmlInsRangeEnd = new \Word\PropertyArray($this);
			$this->_customXmlDelRangeStart = new \Word\PropertyArray($this);
			$this->_customXmlDelRangeEnd = new \Word\PropertyArray($this);
			$this->_customXmlMoveFromRangeStart = new \Word\PropertyArray($this);
			$this->_customXmlMoveFromRangeEnd = new \Word\PropertyArray($this);
			$this->_customXmlMoveToRangeStart = new \Word\PropertyArray($this);
			$this->_customXmlMoveToRangeEnd = new \Word\PropertyArray($this);
			$this->_ins = new \Word\PropertyObject('\Word\Objects\RunTrackChange', "w:ins", $this);
			$this->_del = new \Word\PropertyObject('\Word\Objects\RunTrackChange', "w:del", $this);
			$this->_moveFrom = new \Word\PropertyArray($this);
			$this->_moveTo = new \Word\PropertyArray($this);
			$this->_oMath = new \Word\PropertyArray($this);
			$this->_acc = new \Word\PropertyArray($this);
			$this->_bar = new \Word\PropertyArray($this);
			$this->_box = new \Word\PropertyArray($this);
			$this->_borderBox = new \Word\PropertyArray($this);
			$this->_d = new \Word\PropertyArray($this);
			$this->_eqArr = new \Word\PropertyArray($this);
			$this->_f = new \Word\PropertyArray($this);
			$this->_func = new \Word\PropertyArray($this);
			$this->_groupChr = new \Word\PropertyArray($this);
			$this->_limLow = new \Word\PropertyArray($this);
			$this->_limUpp = new \Word\PropertyArray($this);
			$this->_m = new \Word\PropertyArray($this);
			$this->_nary = new \Word\PropertyArray($this);
			$this->_phant = new \Word\PropertyArray($this);
			$this->_rad = new \Word\PropertyArray($this);
			$this->_sPre = new \Word\PropertyArray($this);
			$this->_sSub = new \Word\PropertyArray($this);
			$this->_sSubSup = new \Word\PropertyArray($this);
			$this->_sSup = new \Word\PropertyArray($this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->id === null && 
				$this->author === null && 
				$this->date === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->id !== null) $xml->writeAttribute("w:id", $this->id);
			if ($this->author !== null) $xml->writeAttribute("w:author", $this->author);
			if ($this->date !== null) $xml->writeAttribute("w:date", $this->date);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return
				$this->id !== null && 
			/* --- Attributes --- */

				$this->author !== null && 
			/* --- Attributes --- */



				true;
		}
		
		
	}
	
}

?>