<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_PPrBase.html 
	 * @property-read String $pStyle Referenced Paragraph Style
	 * @property-read OnOff $keepNext Keep Paragraph With Next Paragraph
	 * @property-read OnOff $keepLines Keep All Lines On One Page
	 * @property-read OnOff $pageBreakBefore Start Paragraph on Next Page
	 * @property-read FramePr $framePr Text Frame Properties
	 * @property-read OnOff $widowControl Allow First/Last Line to Display on a Separate Page
	 * @property-read NumPr $numPr Numbering Definition Instance Reference
	 * @property-read OnOff $suppressLineNumbers Suppress Line Numbers for Paragraph
	 * @property-read PBdr $pBdr Paragraph Borders
	 * @property-read Shd $shd Paragraph Shading
	 * @property-read Tabs $tabs Set of Custom Tab Stops
	 * @property-read OnOff $suppressAutoHyphens Suppress Hyphenation for Paragraph
	 * @property-read OnOff $kinsoku Use East Asian Typography Rules for First and Last Character per Line
	 * @property-read OnOff $wordWrap Allow Line Breaking At Character Level
	 * @property-read OnOff $overflowPunct Allow Punctuation to Extent Past Text Extents
	 * @property-read OnOff $topLinePunct Compress Punctuation at Start of a Line
	 * @property-read OnOff $autoSpaceDE Automatically Adjust Spacing of Latin and East Asian Text
	 * @property-read OnOff $autoSpaceDN Automatically Adjust Spacing of East Asian Text and Numbers
	 * @property-read OnOff $bidi Right to Left Paragraph Layout
	 * @property-read OnOff $adjustRightInd Automatically Adjust Right Indent When Using Document Grid
	 * @property-read OnOff $snapToGrid Use Document Grid Settings for Inter-Line Paragraph Spacing
	 * @property-read Spacing $spacing Spacing Between Lines and Above/Below Paragraph
	 * @property-read Ind $ind Paragraph Indentation
	 * @property-read OnOff $contextualSpacing Ignore Spacing Above and Below When Using Identical Styles
	 * @property-read OnOff $mirrorIndents Use Left/Right Indents as Inside/Outside Indents
	 * @property-read OnOff $suppressOverlap Prevent Text Frames From Overlapping
	 * @property-read Jc $jc Paragraph Alignment
	 * @property-read TextDirection $textDirection Paragraph Text Flow Direction
	 * @property-read TextAlignment $textAlignment Vertical Character Alignment on Line
	 * @property-read TextboxTightWrap $textboxTightWrap Allow Surrounding Paragraphs to Tight Wrap to Text Box Contents
	 * @property-read DecimalNumber $outlineLvl Associated Outline Level
	 * @property-read DecimalNumber $divId Associated HTML div ID
	 * @property-read Cnf $cnfStyle Paragraph Conditional Formatting
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class PPrBase extends Object
	{
		
		/* --- Childrens --- */

		private $_pStyle;
		private $_keepNext;
		private $_keepLines;
		private $_pageBreakBefore;
		private $_framePr;
		private $_widowControl;
		private $_numPr;
		private $_suppressLineNumbers;
		private $_pBdr;
		private $_shd;
		private $_tabs;
		private $_suppressAutoHyphens;
		private $_kinsoku;
		private $_wordWrap;
		private $_overflowPunct;
		private $_topLinePunct;
		private $_autoSpaceDE;
		private $_autoSpaceDN;
		private $_bidi;
		private $_adjustRightInd;
		private $_snapToGrid;
		private $_spacing;
		private $_ind;
		private $_contextualSpacing;
		private $_mirrorIndents;
		private $_suppressOverlap;
		private $_jc;
		private $_textDirection;
		private $_textAlignment;
		private $_textboxTightWrap;
		private $_outlineLvl;
		private $_divId;
		private $_cnfStyle;

		
		/* --- Childrens --- */

		protected function getPStyle(){ return $this->_pStyle->get(); }

		protected function getKeepNext(){ return $this->_keepNext->get(); }

		protected function getKeepLines(){ return $this->_keepLines->get(); }

		protected function getPageBreakBefore(){ return $this->_pageBreakBefore->get(); }

		protected function getFramePr(){ return $this->_framePr->get(); }

		protected function getWidowControl(){ return $this->_widowControl->get(); }

		protected function getNumPr(){ return $this->_numPr->get(); }

		protected function getSuppressLineNumbers(){ return $this->_suppressLineNumbers->get(); }

		protected function getPBdr(){ return $this->_pBdr->get(); }

		protected function getShd(){ return $this->_shd->get(); }

		protected function getTabs(){ return $this->_tabs->get(); }

		protected function getSuppressAutoHyphens(){ return $this->_suppressAutoHyphens->get(); }

		protected function getKinsoku(){ return $this->_kinsoku->get(); }

		protected function getWordWrap(){ return $this->_wordWrap->get(); }

		protected function getOverflowPunct(){ return $this->_overflowPunct->get(); }

		protected function getTopLinePunct(){ return $this->_topLinePunct->get(); }

		protected function getAutoSpaceDE(){ return $this->_autoSpaceDE->get(); }

		protected function getAutoSpaceDN(){ return $this->_autoSpaceDN->get(); }

		protected function getBidi(){ return $this->_bidi->get(); }

		protected function getAdjustRightInd(){ return $this->_adjustRightInd->get(); }

		protected function getSnapToGrid(){ return $this->_snapToGrid->get(); }

		protected function getSpacing(){ return $this->_spacing->get(); }

		protected function getInd(){ return $this->_ind->get(); }

		protected function getContextualSpacing(){ return $this->_contextualSpacing->get(); }

		protected function getMirrorIndents(){ return $this->_mirrorIndents->get(); }

		protected function getSuppressOverlap(){ return $this->_suppressOverlap->get(); }

		protected function getJc(){ return $this->_jc->get(); }

		protected function getTextDirection(){ return $this->_textDirection->get(); }

		protected function getTextAlignment(){ return $this->_textAlignment->get(); }

		protected function getTextboxTightWrap(){ return $this->_textboxTightWrap->get(); }

		protected function getOutlineLvl(){ return $this->_outlineLvl->get(); }

		protected function getDivId(){ return $this->_divId->get(); }

		protected function getCnfStyle(){ return $this->_cnfStyle->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\String",
					"Objects\OnOff",
					"Objects\FramePr",
					"Objects\NumPr",
					"Objects\PBdr",
					"Objects\Shd",
					"Objects\Tabs",
					"Objects\Spacing",
					"Objects\Ind",
					"Objects\Jc",
					"Objects\TextDirection",
					"Objects\TextAlignment",
					"Objects\TextboxTightWrap",
					"Objects\DecimalNumber",
					"Objects\Cnf"
				)
			);
				
			parent::__construct($name);

			$this->_pStyle = new \Word\PropertyObject('\Word\Objects\String', "w:pStyle", $this);
			$this->_keepNext = new \Word\PropertyObject('\Word\Objects\OnOff', "w:keepNext", $this);
			$this->_keepLines = new \Word\PropertyObject('\Word\Objects\OnOff', "w:keepLines", $this);
			$this->_pageBreakBefore = new \Word\PropertyObject('\Word\Objects\OnOff', "w:pageBreakBefore", $this);
			$this->_framePr = new \Word\PropertyObject('\Word\Objects\FramePr', "w:framePr", $this);
			$this->_widowControl = new \Word\PropertyObject('\Word\Objects\OnOff', "w:widowControl", $this);
			$this->_numPr = new \Word\PropertyObject('\Word\Objects\NumPr', "w:numPr", $this);
			$this->_suppressLineNumbers = new \Word\PropertyObject('\Word\Objects\OnOff', "w:suppressLineNumbers", $this);
			$this->_pBdr = new \Word\PropertyObject('\Word\Objects\PBdr', "w:pBdr", $this);
			$this->_shd = new \Word\PropertyObject('\Word\Objects\Shd', "w:shd", $this);
			$this->_tabs = new \Word\PropertyObject('\Word\Objects\Tabs', "w:tabs", $this);
			$this->_suppressAutoHyphens = new \Word\PropertyObject('\Word\Objects\OnOff', "w:suppressAutoHyphens", $this);
			$this->_kinsoku = new \Word\PropertyObject('\Word\Objects\OnOff', "w:kinsoku", $this);
			$this->_wordWrap = new \Word\PropertyObject('\Word\Objects\OnOff', "w:wordWrap", $this);
			$this->_overflowPunct = new \Word\PropertyObject('\Word\Objects\OnOff', "w:overflowPunct", $this);
			$this->_topLinePunct = new \Word\PropertyObject('\Word\Objects\OnOff', "w:topLinePunct", $this);
			$this->_autoSpaceDE = new \Word\PropertyObject('\Word\Objects\OnOff', "w:autoSpaceDE", $this);
			$this->_autoSpaceDN = new \Word\PropertyObject('\Word\Objects\OnOff', "w:autoSpaceDN", $this);
			$this->_bidi = new \Word\PropertyObject('\Word\Objects\OnOff', "w:bidi", $this);
			$this->_adjustRightInd = new \Word\PropertyObject('\Word\Objects\OnOff', "w:adjustRightInd", $this);
			$this->_snapToGrid = new \Word\PropertyObject('\Word\Objects\OnOff', "w:snapToGrid", $this);
			$this->_spacing = new \Word\PropertyObject('\Word\Objects\Spacing', "w:spacing", $this);
			$this->_ind = new \Word\PropertyObject('\Word\Objects\Ind', "w:ind", $this);
			$this->_contextualSpacing = new \Word\PropertyObject('\Word\Objects\OnOff', "w:contextualSpacing", $this);
			$this->_mirrorIndents = new \Word\PropertyObject('\Word\Objects\OnOff', "w:mirrorIndents", $this);
			$this->_suppressOverlap = new \Word\PropertyObject('\Word\Objects\OnOff', "w:suppressOverlap", $this);
			$this->_jc = new \Word\PropertyObject('\Word\Objects\Jc', "w:jc", $this);
			$this->_textDirection = new \Word\PropertyObject('\Word\Objects\TextDirection', "w:textDirection", $this);
			$this->_textAlignment = new \Word\PropertyObject('\Word\Objects\TextAlignment', "w:textAlignment", $this);
			$this->_textboxTightWrap = new \Word\PropertyObject('\Word\Objects\TextboxTightWrap', "w:textboxTightWrap", $this);
			$this->_outlineLvl = new \Word\PropertyObject('\Word\Objects\DecimalNumber', "w:outlineLvl", $this);
			$this->_divId = new \Word\PropertyObject('\Word\Objects\DecimalNumber', "w:divId", $this);
			$this->_cnfStyle = new \Word\PropertyObject('\Word\Objects\Cnf', "w:cnfStyle", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 

				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{

		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>