<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_TrackChange.html 
	 * @property-read DecimalNumber $id Annotation Identifier
	 * @property-read String $author Annotation Author
	 * @property-read DateTime $date Annotation Date
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class TrackChange extends Object
	{
		
		/* --- Attributes --- */

		private $_id;
		private $_author;
		private $_date;

		
		/* --- Attributes --- */

		protected function getId(){ return $this->_id; }
		protected function setId($val){ $this->_id = $val; }

		protected function getAuthor(){ return $this->_author; }
		protected function setAuthor($val){ $this->_author = $val; }

		protected function getDate(){ return $this->_date; }
		protected function setDate($val){ $this->_date = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\DecimalNumber",
					"Consts\String",
					"Consts\DateTime"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->id === null && 
				$this->author === null && 
				$this->date === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->id !== null) $xml->writeAttribute("w:id", $this->id);
			if ($this->author !== null) $xml->writeAttribute("w:author", $this->author);
			if ($this->date !== null) $xml->writeAttribute("w:date", $this->date);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return
				$this->id !== null && 
			/* --- Attributes --- */

				$this->author !== null && 
			/* --- Attributes --- */



				true;
		}
		
		
	}
	
}

?>