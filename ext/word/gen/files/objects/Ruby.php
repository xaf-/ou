<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_Ruby.html 
	 * @property-read RubyPr $rubyPr Phonetic Guide Properties
	 * @property-read RubyContent $rt Phonetic Guide Text
	 * @property-read RubyContent $rubyBase Phonetic Guide Base Text
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class Ruby extends Object
	{
		
		/* --- Childrens --- */

		private $_rubyPr;
		private $_rt;
		private $_rubyBase;

		
		/* --- Childrens --- */

		protected function getRubyPr(){ return $this->_rubyPr->get(); }

		protected function getRt(){ return $this->_rt->get(); }

		protected function getRubyBase(){ return $this->_rubyBase->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\RubyPr",
					"Objects\RubyContent"
				)
			);
				
			parent::__construct($name);

			$this->_rubyPr = new \Word\PropertyObject('\Word\Objects\RubyPr', "w:rubyPr", $this);
			$this->_rt = new \Word\PropertyObject('\Word\Objects\RubyContent', "w:rt", $this);
			$this->_rubyBase = new \Word\PropertyObject('\Word\Objects\RubyContent', "w:rubyBase", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 

				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{

		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>