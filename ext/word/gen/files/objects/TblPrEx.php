<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_TblPrEx.html 
	 * @property-read TblWidth $tblW Preferred Table Width Exception
	 * @property-read Jc $jc Table Alignment Exception
	 * @property-read TblWidth $tblCellSpacing Table Cell Spacing Exception
	 * @property-read TblWidth $tblInd Table Indent from Leading Margin Exception
	 * @property-read TblBorders $tblBorders Table Borders Exceptions
	 * @property-read Shd $shd Table Shading Exception
	 * @property-read TblLayoutType $tblLayout Table Layout Exception
	 * @property-read TblCellMar $tblCellMar Table Cell Margin Exceptions
	 * @property-read ShortHexNumber $tblLook Table Style Conditional Formatting Settings Exception
	 * @property-read TblPrExChange $tblPrExChange Revision Information for Table-Level Property Exceptions
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class TblPrEx extends Object
	{
		
		/* --- Childrens --- */

		private $_tblW;
		private $_jc;
		private $_tblCellSpacing;
		private $_tblInd;
		private $_tblBorders;
		private $_shd;
		private $_tblLayout;
		private $_tblCellMar;
		private $_tblLook;
		private $_tblPrExChange;

		
		/* --- Childrens --- */

		protected function getTblW(){ return $this->_tblW->get(); }

		protected function getJc(){ return $this->_jc->get(); }

		protected function getTblCellSpacing(){ return $this->_tblCellSpacing->get(); }

		protected function getTblInd(){ return $this->_tblInd->get(); }

		protected function getTblBorders(){ return $this->_tblBorders->get(); }

		protected function getShd(){ return $this->_shd->get(); }

		protected function getTblLayout(){ return $this->_tblLayout->get(); }

		protected function getTblCellMar(){ return $this->_tblCellMar->get(); }

		protected function getTblLook(){ return $this->_tblLook->get(); }

		protected function getTblPrExChange(){ return $this->_tblPrExChange->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\TblWidth",
					"Objects\Jc",
					"Objects\TblBorders",
					"Objects\Shd",
					"Objects\TblLayoutType",
					"Objects\TblCellMar",
					"Objects\ShortHexNumber",
					"Objects\TblPrExChange"
				)
			);
				
			parent::__construct($name);

			$this->_tblW = new \Word\PropertyObject('\Word\Objects\TblWidth', "w:tblW", $this);
			$this->_jc = new \Word\PropertyObject('\Word\Objects\Jc', "w:jc", $this);
			$this->_tblCellSpacing = new \Word\PropertyObject('\Word\Objects\TblWidth', "w:tblCellSpacing", $this);
			$this->_tblInd = new \Word\PropertyObject('\Word\Objects\TblWidth', "w:tblInd", $this);
			$this->_tblBorders = new \Word\PropertyObject('\Word\Objects\TblBorders', "w:tblBorders", $this);
			$this->_shd = new \Word\PropertyObject('\Word\Objects\Shd', "w:shd", $this);
			$this->_tblLayout = new \Word\PropertyObject('\Word\Objects\TblLayoutType', "w:tblLayout", $this);
			$this->_tblCellMar = new \Word\PropertyObject('\Word\Objects\TblCellMar', "w:tblCellMar", $this);
			$this->_tblLook = new \Word\PropertyObject('\Word\Objects\ShortHexNumber', "w:tblLook", $this);
			$this->_tblPrExChange = new \Word\PropertyObject('\Word\Objects\TblPrExChange', "w:tblPrExChange", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 

				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{

		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>