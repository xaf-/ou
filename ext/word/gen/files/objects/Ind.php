<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_Ind.html 
	 * @property-read SignedTwipsMeasure $left Left Indentation
	 * @property-read DecimalNumber $leftChars Left Indentation in Character Units
	 * @property-read SignedTwipsMeasure $right Right Indentation
	 * @property-read DecimalNumber $rightChars Right Indentation in Character Units
	 * @property-read TwipsMeasure $hanging Indentation Removed from First Line
	 * @property-read DecimalNumber $hangingChars Indentation Removed From First Line in Character Units
	 * @property-read TwipsMeasure $firstLine Additional First Line Indentation
	 * @property-read DecimalNumber $firstLineChars Additional First Line Indentation in Character Units
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class Ind extends Object
	{
		
		/* --- Attributes --- */

		private $_left;
		private $_leftChars;
		private $_right;
		private $_rightChars;
		private $_hanging;
		private $_hangingChars;
		private $_firstLine;
		private $_firstLineChars;

		
		/* --- Attributes --- */

		protected function getLeft(){ return $this->_left; }
		protected function setLeft($val){ $this->_left = $val; }

		protected function getLeftChars(){ return $this->_leftChars; }
		protected function setLeftChars($val){ $this->_leftChars = $val; }

		protected function getRight(){ return $this->_right; }
		protected function setRight($val){ $this->_right = $val; }

		protected function getRightChars(){ return $this->_rightChars; }
		protected function setRightChars($val){ $this->_rightChars = $val; }

		protected function getHanging(){ return $this->_hanging; }
		protected function setHanging($val){ $this->_hanging = $val; }

		protected function getHangingChars(){ return $this->_hangingChars; }
		protected function setHangingChars($val){ $this->_hangingChars = $val; }

		protected function getFirstLine(){ return $this->_firstLine; }
		protected function setFirstLine($val){ $this->_firstLine = $val; }

		protected function getFirstLineChars(){ return $this->_firstLineChars; }
		protected function setFirstLineChars($val){ $this->_firstLineChars = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\SignedTwipsMeasure",
					"Consts\DecimalNumber",
					"Consts\TwipsMeasure"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->left === null && 
				$this->leftChars === null && 
				$this->right === null && 
				$this->rightChars === null && 
				$this->hanging === null && 
				$this->hangingChars === null && 
				$this->firstLine === null && 
				$this->firstLineChars === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->left !== null) $xml->writeAttribute("w:left", $this->left);
			if ($this->leftChars !== null) $xml->writeAttribute("w:leftChars", $this->leftChars);
			if ($this->right !== null) $xml->writeAttribute("w:right", $this->right);
			if ($this->rightChars !== null) $xml->writeAttribute("w:rightChars", $this->rightChars);
			if ($this->hanging !== null) $xml->writeAttribute("w:hanging", $this->hanging);
			if ($this->hangingChars !== null) $xml->writeAttribute("w:hangingChars", $this->hangingChars);
			if ($this->firstLine !== null) $xml->writeAttribute("w:firstLine", $this->firstLine);
			if ($this->firstLineChars !== null) $xml->writeAttribute("w:firstLineChars", $this->firstLineChars);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>