<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_Object.html 
	 * @property-read Control $control Inline Embedded Control
	 * @property-read TwipsMeasure $dxaOrig Original Image Width
	 * @property-read TwipsMeasure $dyaOrig Original Image Height
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class Object_ extends Object
	{
		
		/* --- Attributes --- */

		private $_dxaOrig;
		private $_dyaOrig;

		/* --- Childrens --- */

		private $_control;

		
		/* --- Attributes --- */

		protected function getDxaOrig(){ return $this->_dxaOrig; }
		protected function setDxaOrig($val){ $this->_dxaOrig = $val; }

		protected function getDyaOrig(){ return $this->_dyaOrig; }
		protected function setDyaOrig($val){ $this->_dyaOrig = $val; }

		/* --- Childrens --- */

		protected function getControl(){ return $this->_control->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\Control",
					"Consts\TwipsMeasure"
				)
			);
				
			parent::__construct($name);

			$this->_control = new \Word\PropertyObject('\Word\Objects\Control', "w:control", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->dxaOrig === null && 
				$this->dyaOrig === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->dxaOrig !== null) $xml->writeAttribute("w:dxaOrig", $this->dxaOrig);
			if ($this->dyaOrig !== null) $xml->writeAttribute("w:dyaOrig", $this->dyaOrig);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>