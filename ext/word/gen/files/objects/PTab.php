<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_PTab.html 
	 * @property-read PTabAlignment $alignment Positional Tab Stop Alignment
	 * @property-read PTabRelativeTo $relativeTo Positional Tab Base
	 * @property-read PTabLeader $leader Tab Leader Character
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class PTab extends Object
	{
		
		/* --- Attributes --- */

		private $_alignment;
		private $_relativeTo;
		private $_leader;

		
		/* --- Attributes --- */

		protected function getAlignment(){ return $this->_alignment; }
		protected function setAlignment($val){ $this->_alignment = $val; }

		protected function getRelativeTo(){ return $this->_relativeTo; }
		protected function setRelativeTo($val){ $this->_relativeTo = $val; }

		protected function getLeader(){ return $this->_leader; }
		protected function setLeader($val){ $this->_leader = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\PTabAlignment",
					"Consts\PTabRelativeTo",
					"Consts\PTabLeader"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->alignment === null && 
				$this->relativeTo === null && 
				$this->leader === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->alignment !== null) $xml->writeAttribute("w:alignment", $this->alignment);
			if ($this->relativeTo !== null) $xml->writeAttribute("w:relativeTo", $this->relativeTo);
			if ($this->leader !== null) $xml->writeAttribute("w:leader", $this->leader);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return
				$this->alignment !== null && 
			/* --- Attributes --- */

				$this->relativeTo !== null && 
			/* --- Attributes --- */

				$this->leader !== null && 


				true;
		}
		
		
	}
	
}

?>