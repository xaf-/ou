<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_Column.html 
	 * @property-read TwipsMeasure $w Column Width
	 * @property-read TwipsMeasure $space Space Before Following Column
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class Column extends Object
	{
		
		/* --- Attributes --- */

		private $_w;
		private $_space;

		
		/* --- Attributes --- */

		protected function getW(){ return $this->_w; }
		protected function setW($val){ $this->_w = $val; }

		protected function getSpace(){ return $this->_space; }
		protected function setSpace($val){ $this->_space = $val; }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Consts\TwipsMeasure"
				)
			);
				
			parent::__construct($name);

		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->w === null && 
				$this->space === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->w !== null) $xml->writeAttribute("w:w", $this->w);
			if ($this->space !== null) $xml->writeAttribute("w:space", $this->space);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return

				true;
		}
		
		
	}
	
}

?>