<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * 
	 * @link http://www.schemacentral.com/sc/ooxml/t-w_CT_TblStylePr.html 
	 * @property-read PPr $pPr Table Style Conditional Formatting Paragraph Properties
	 * @property-read RPr $rPr Table Style Conditional Formatting Run Properties
	 * @property-read TblPrBase $tblPr Table Style Conditional Formatting Table Properties
	 * @property-read TrPr $trPr Table Style Conditional Formatting Table Row Properties
	 * @property-read TcPr $tcPr Table Style Conditional Formatting Table Cell Properties
	 * @property-read TblStyleOverrideType $type Table Style Conditional Formatting Type
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class TblStylePr extends Object
	{
		
		/* --- Attributes --- */

		private $_type;

		/* --- Childrens --- */

		private $_pPr;
		private $_rPr;
		private $_tblPr;
		private $_trPr;
		private $_tcPr;

		
		/* --- Attributes --- */

		protected function getType(){ return $this->_type; }
		protected function setType($val){ $this->_type = $val; }

		/* --- Childrens --- */

		protected function getPPr(){ return $this->_pPr->get(); }

		protected function getRPr(){ return $this->_rPr->get(); }

		protected function getTblPr(){ return $this->_tblPr->get(); }

		protected function getTrPr(){ return $this->_trPr->get(); }

		protected function getTcPr(){ return $this->_tcPr->get(); }



		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object",
					"Objects\PPr",
					"Objects\RPr",
					"Objects\TblPrBase",
					"Objects\TrPr",
					"Objects\TcPr",
					"Consts\TblStyleOverrideType"
				)
			);
				
			parent::__construct($name);

			$this->_pPr = new \Word\PropertyObject('\Word\Objects\PPr', "w:pPr", $this);
			$this->_rPr = new \Word\PropertyObject('\Word\Objects\RPr', "w:rPr", $this);
			$this->_tblPr = new \Word\PropertyObject('\Word\Objects\TblPrBase', "w:tblPr", $this);
			$this->_trPr = new \Word\PropertyObject('\Word\Objects\TrPr', "w:trPr", $this);
			$this->_tcPr = new \Word\PropertyObject('\Word\Objects\TcPr', "w:tcPr", $this);
		}
		

		
		public function _namespace()
		{
			return "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
			/* --- Attributes --- */

				$this->type === null && 


				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
			/* --- Attributes --- */

			if ($this->type !== null) $xml->writeAttribute("w:type", $this->type);


		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return
				$this->type !== null && 


				true;
		}
		
		
	}
	
}

?>