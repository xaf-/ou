<?php

	require_once(dirname(__FILE__) . "/classgen.php");

	error_reporting(3);
	ini_set("xdebug.max_nesting_level", 1000);
	set_time_limit(0);
	
	$urls = array(
		"http://www.schemacentral.com/sc/ooxml/t-w_CT_Styles.html",
		"http://www.schemacentral.com/sc/ooxml/t-w_CT_Style.html",
		"http://www.schemacentral.com/sc/ooxml/t-w_CT_Body.html",
		"http://www.schemacentral.com/sc/ooxml/t-w_CT_Background.html",
		"http://www.schemacentral.com/sc/ooxml/t-w_CT_Object.html",
		"http://www.schemacentral.com/sc/ooxml/t-w_CT_Document.html"
	);
	
	foreach ($urls as $url)
	{
		echo "<h2>$url</h2>";
		$c = new ClassGen($url);
		$c->save();
	}

?>