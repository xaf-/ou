<?php 

namespace Word\Consts
{

	/**
	 * %desc%
	 * @link %link%
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class %class_name_abbr%
	{
		
%consts%
		
	}
	
}

?>