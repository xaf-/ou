<?php 

namespace Word\Objects
{
	
	use Word\Config;
	use Word\Object;
	
	/**
	 * %desc%
	 * @link %link% %props%
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class %class_name_abbr% extends Object
	{
		
%fields%		
%fields_funcs%

		public function __construct($name)
		{
			
			Config::IncClass(
				array(
					"Object"%incs%
				)
			);
				
			parent::__construct($name);
%constr%
		}
		
%custom%
		
		public function _namespace()
		{
			%ns%
		}

		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_empty()
		 */
		public function _emptyObject()
		{
			return 
%empty%
				true;
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_writeObject()
		 */
		public function _writeObject($xml)
		{
%write%
		}
		
		/**
		 * (non-PHPdoc)
		 * @see \Word\Object::_validateObject()
		 */
		public function _validateObject()
		{
			return
%validate%
				true;
		}
		
		
	}
	
}

?>