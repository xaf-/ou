<?php

namespace CSSParse;

/**
 * A CSSBlockList is a CSSList whose DeclarationBlocks are guaranteed to contain valid declaration blocks or at-rules.
 * Most CSSLists conform to this category but some at-rules (such as @keyframes) do not.
 */
abstract class CSSBlockList extends CSSList {
	protected function allDeclarationBlocks(&$aResult) {
		foreach ($this->aContents as $mContent) {
			if ($mContent instanceof DeclarationBlock) {
				$aResult[] = $mContent;
			} else if ($mContent instanceof CSSBlockList) {
				$mContent->allDeclarationBlocks($aResult);
			}
		}
	}

	protected function allRuleSets(&$aResult) {
		foreach ($this->aContents as $mContent) {
			if ($mContent instanceof RuleSet) {
				$aResult[] = $mContent;
			} else if ($mContent instanceof CSSBlockList) {
				$mContent->allRuleSets($aResult);
			}
		}
	}

	protected function allValues($oElement, &$aResult, $sSearchString = null, $bSearchInFunctionArguments = false) {
		if ($oElement instanceof CSSBlockList) {
			foreach ($oElement->getContents() as $oContent) {
				$this->allValues($oContent, $aResult, $sSearchString, $bSearchInFunctionArguments);
			}
		} else if ($oElement instanceof RuleSet) {
			foreach ($oElement->getRules($sSearchString) as $oRule) {
				$this->allValues($oRule, $aResult, $sSearchString, $bSearchInFunctionArguments);
			}
		} else if ($oElement instanceof Rule) {
			$this->allValues($oElement->getValue(), $aResult, $sSearchString, $bSearchInFunctionArguments);
		} else if ($oElement instanceof ValueList) {
			if ($bSearchInFunctionArguments || !($oElement instanceof CSSFunction)) {
				foreach ($oElement->getListComponents() as $mComponent) {
					$this->allValues($mComponent, $aResult, $sSearchString, $bSearchInFunctionArguments);
				}
			}
		} else {
			//Non-List Value or String (CSS identifier)
			$aResult[] = $oElement;
		}
	}

	protected function allSelectors(&$aResult, $sSpecificitySearch = null) {
		$aDeclarationBlocks = array();
		$this->allDeclarationBlocks($aDeclarationBlocks);
		foreach ($aDeclarationBlocks as $oBlock) {
			foreach ($oBlock->getSelectors() as $oSelector) {
				if ($sSpecificitySearch === null) {
					$aResult[] = $oSelector;
				} else {
					$sComparison = "\$bRes = {$oSelector->getSpecificity()} $sSpecificitySearch;";
					eval($sComparison);
					if ($bRes) {
						$aResult[] = $oSelector;
					}
				}
			}
		}
	}

}

/**
 * A RuleSet constructed by an unknown @-rule. @font-face rules are rendered into AtRule objects.
 */
class AtRuleBlockList extends CSSBlockList implements AtRule {

	private $sType;
	private $sArgs;

	public function __construct($sType, $sArgs = '') {
		parent::__construct();
		$this->sType = $sType;
		$this->sArgs = $sArgs;
	}

	public function atRuleName() {
		return $this->sType;
	}

	public function atRuleArgs() {
		return $this->sArgs;
	}

	public function __toString() {
		$sResult = "@{$this->sType} {$this->sArgs}{";
		$sResult .= parent::__toString();
		$sResult .= '}';
		return $sResult;
	}

}



/**
 * A CSSList is the most generic container available. Its contents include RuleSet as well as other CSSList objects.
 * Also, it may contain Import and Charset objects stemming from @-rules.
 */
abstract class CSSList {

	protected $aContents;

	public function __construct() {
		$this->aContents = array();
	}

	public function append($oItem) {
		$this->aContents[] = $oItem;
	}

	/**
	 * Removes an item from the CSS list.
	 * @param RuleSet|Import|Charset|CSSList $oItemToRemove May be a RuleSet (most likely a DeclarationBlock), a Import, a Charset or another CSSList (most likely a MediaQuery)
	 */
	public function remove($oItemToRemove) {
		$iKey = array_search($oItemToRemove, $this->aContents, true);
		if ($iKey !== false) {
			unset($this->aContents[$iKey]);
		}
	}

	public function removeDeclarationBlockBySelector($mSelector, $bRemoveAll = false) {
		if ($mSelector instanceof DeclarationBlock) {
			$mSelector = $mSelector->getSelectors();
		}
		if (!is_array($mSelector)) {
			$mSelector = explode(',', $mSelector);
		}
		foreach ($mSelector as $iKey => &$mSel) {
			if (!($mSel instanceof Selector)) {
				$mSel = new Selector($mSel);
			}
		}
		foreach ($this->aContents as $iKey => $mItem) {
			if (!($mItem instanceof DeclarationBlock)) {
				continue;
			}
			if ($mItem->getSelectors() == $mSelector) {
				unset($this->aContents[$iKey]);
				if (!$bRemoveAll) {
					return;
				}
			}
		}
	}

	public function __toString() {
		$sResult = '';
		foreach ($this->aContents as $oContent) {
			$sResult .= $oContent->__toString();
		}
		return $sResult;
	}

	public function getContents() {
		return $this->aContents;
	}
}


/**
 * The root CSSList of a parsed file. Contains all top-level css contents, mostly declaration blocks, but also any @-rules encountered.
 */
class Document extends CSSBlockList {

    /**
     * Gets all DeclarationBlock objects recursively.
     */
    public function getAllDeclarationBlocks() {
        $aResult = array();
        $this->allDeclarationBlocks($aResult);
        return $aResult;
    }

    /**
     * @deprecated use getAllDeclarationBlocks()
     */
    public function getAllSelectors() {
        return $this->getAllDeclarationBlocks();
    }

    /**
     * Returns all RuleSet objects found recursively in the tree.
     */
    public function getAllRuleSets() {
        $aResult = array();
        $this->allRuleSets($aResult);
        return $aResult;
    }

    /**
     * Returns all Value objects found recursively in the tree.
     * @param (object|string) $mElement the CSSList or RuleSet to start the search from (defaults to the whole document). If a string is given, it is used as rule name filter (@see{RuleSet->getRules()}).
     * @param (bool) $bSearchInFunctionArguments whether to also return Value objects used as Function arguments.
     */
    public function getAllValues($mElement = null, $bSearchInFunctionArguments = false) {
        $sSearchString = null;
        if ($mElement === null) {
            $mElement = $this;
        } else if (is_string($mElement)) {
            $sSearchString = $mElement;
            $mElement = $this;
        }
        $aResult = array();
        $this->allValues($mElement, $aResult, $sSearchString, $bSearchInFunctionArguments);
        return $aResult;
    }

    /**
     * Returns all Selector objects found recursively in the tree.
     * Note that this does not yield the full DeclarationBlock that the selector belongs to (and, currently, there is no way to get to that).
     * @param $sSpecificitySearch An optional filter by specificity. May contain a comparison operator and a number or just a number (defaults to "==").
     * @example getSelectorsBySpecificity('>= 100')
     */
    public function getSelectorsBySpecificity($sSpecificitySearch = null) {
        if (is_numeric($sSpecificitySearch) || is_numeric($sSpecificitySearch[0])) {
            $sSpecificitySearch = "== $sSpecificitySearch";
        }
        $aResult = array();
        $this->allSelectors($aResult, $sSpecificitySearch);
        return $aResult;
    }

    /**
     * Expands all shorthand properties to their long value
     */
    public function expandShorthands() {
        foreach ($this->getAllDeclarationBlocks() as $oDeclaration) {
            $oDeclaration->expandShorthands();
        }
    }

    /*
     * Create shorthands properties whenever possible
     */

    public function createShorthands() {
        foreach ($this->getAllDeclarationBlocks() as $oDeclaration) {
            $oDeclaration->createShorthands();
        }
    }

}

class KeyFrame extends CSSList implements AtRule {

	private $vendorKeyFrame;
	private $animationName;

	public function __construct() {
		parent::__construct();
		$this->vendorKeyFrame = null;
		$this->animationName  = null;
	}

	public function setVendorKeyFrame($vendorKeyFrame) {
		$this->vendorKeyFrame = $vendorKeyFrame;
	}

	public function getVendorKeyFrame() {
		return $this->vendorKeyFrame;
	}

	public function setAnimationName($animationName) {
		$this->animationName = $animationName;
	}

	public function getAnimationName() {
		return $this->animationName;
	}

	public function __toString() {
		$sResult = "@{$this->vendorKeyFrame} {$this->animationName} {";
		$sResult .= parent::__toString();
		$sResult .= '}';
		return $sResult;
	}

	public function atRuleName() {
		return $this->vendorKeyFrame;
	}

	public function atRuleArgs() {
		return $this->animationName;
	}
}

/**
 * Parser class parses CSS from text into a data structure.
 */
class Parser {

	private $sText;
	private $iCurrentPosition;
	private $oParserSettings;
	private $sCharset;
	private $iLength;

	public function __construct($sText, Settings $oParserSettings = null) {
		$this->sText = $sText;
		$this->iCurrentPosition = 0;
		if ($oParserSettings === null) {
			$oParserSettings = Settings::create();
		}
		$this->oParserSettings = $oParserSettings;
	}

	public function setCharset($sCharset) {
		$this->sCharset = $sCharset;
		$this->iLength = $this->strlen($this->sText);
	}

	public function getCharset() {
		return $this->sCharset;
	}

	public function parse() {
		$this->setCharset($this->oParserSettings->sDefaultCharset);
		$oResult = new Document();
		$this->parseDocument($oResult);
		return $oResult;
	}

	private function parseDocument(Document $oDocument) {
		$this->consumeWhiteSpace();
		$this->parseList($oDocument, true);
	}

	private function parseList(CSSList $oList, $bIsRoot = false) {
		while (!$this->isEnd()) {
			if ($this->comes('@')) {
				$oList->append($this->parseAtRule());
			} else if ($this->comes('}')) {
				$this->consume('}');
				if ($bIsRoot) {
					throw new \Exception("Unopened {");
				} else {
					return;
				}
			} else {
				$oList->append($this->parseSelector());
			}
			$this->consumeWhiteSpace();
		}
		if (!$bIsRoot) {
			throw new \Exception("Unexpected end of document");
		}
	}

	private function parseAtRule() {
		$this->consume('@');
		$sIdentifier = $this->parseIdentifier();
		$this->consumeWhiteSpace();
		if ($sIdentifier === 'import') {
			$oLocation = $this->parseURLValue();
			$this->consumeWhiteSpace();
			$sMediaQuery = null;
			if (!$this->comes(';')) {
				$sMediaQuery = $this->consumeUntil(';');
			}
			$this->consume(';');
			return new Import($oLocation, $sMediaQuery);
		} else if ($sIdentifier === 'charset') {
			$sCharset = $this->parseStringValue();
			$this->consumeWhiteSpace();
			$this->consume(';');
			$this->setCharset($sCharset->getString());
			return new Charset($sCharset);
		} else if (self::identifierIs($sIdentifier, 'keyframes')) {
			$oResult = new KeyFrame();
			$oResult->setVendorKeyFrame($sIdentifier);
			$oResult->setAnimationName(trim($this->consumeUntil('{')));
			$this->consume('{');
			$this->consumeWhiteSpace();
			$this->parseList($oResult);
			return $oResult;
		} else if ($sIdentifier === 'namespace') {
			$sPrefix = null;
			$mUrl = $this->parsePrimitiveValue();
			if (!$this->comes(';')) {
				$sPrefix = $mUrl;
				$mUrl = $this->parsePrimitiveValue();
			}
			$this->consume(';');
			if ($sPrefix !== null && !is_string($sPrefix)) {
				throw new \Exception('Wrong namespace prefix '.$sPrefix);
			}
			if (!($mUrl instanceof String || $mUrl instanceof URL)) {
				throw new \Exception('Wrong namespace url of invalid type '.$mUrl);
			}
			return new CSSNamespace($mUrl, $sPrefix);
		} else {
			//Unknown other at rule (font-face or such)
			$sArgs = $this->consumeUntil('{');
			$this->consume('{');
			$this->consumeWhiteSpace();
			$bUseRuleSet = true;
			foreach(explode('/', AtRule::BLOCK_RULES) as $sBlockRuleName) {
				if(self::identifierIs($sIdentifier, $sBlockRuleName)) {
					$bUseRuleSet = false;
					break;
				}
			}
			if($bUseRuleSet) {
				$oAtRule = new AtRuleSet($sIdentifier, $sArgs);
				$this->parseRuleSet($oAtRule);
			} else {
				$oAtRule = new AtRuleBlockList($sIdentifier, $sArgs);
				$this->parseList($oAtRule);
			}
			return $oAtRule;
		}
	}

	private function parseIdentifier($bAllowFunctions = true, $bIgnoreCase = true) {
		$sResult = $this->parseCharacter(true);
		if ($sResult === null) {
			throw new UnexpectedTokenException($sResult, $this->peek(5), 'identifier');
		}
		$sCharacter = null;
		while (($sCharacter = $this->parseCharacter(true)) !== null) {
			$sResult .= $sCharacter;
		}
		if ($bIgnoreCase) {
			$sResult = $this->strtolower($sResult);
		}
		if ($bAllowFunctions && $this->comes('(')) {
			$this->consume('(');
			$aArguments = $this->parseValue(array('=', ' ', ','));
			$sResult = new CSSFunction($sResult, $aArguments);
			$this->consume(')');
		}
		return $sResult;
	}

	private function parseStringValue() {
		$sBegin = $this->peek();
		$sQuote = null;
		if ($sBegin === "'") {
			$sQuote = "'";
		} else if ($sBegin === '"') {
			$sQuote = '"';
		}
		if ($sQuote !== null) {
			$this->consume($sQuote);
		}
		$sResult = "";
		$sContent = null;
		if ($sQuote === null) {
			//Unquoted strings end in whitespace or with braces, brackets, parentheses
			while (!preg_match('/[\\s{}()<>\\[\\]]/isu', $this->peek())) {
				$sResult .= $this->parseCharacter(false);
			}
		} else {
			while (!$this->comes($sQuote)) {
				$sContent = $this->parseCharacter(false);
				if ($sContent === null) {
					throw new \Exception("Non-well-formed quoted string {$this->peek(3)}");
				}
				$sResult .= $sContent;
			}
			$this->consume($sQuote);
		}
		return new String($sResult);
	}

	private function parseCharacter($bIsForIdentifier) {
		if ($this->peek() === '\\') {
			$this->consume('\\');
			if ($this->comes('\n') || $this->comes('\r')) {
				return '';
			}
			$aMatches;
			if (preg_match('/[0-9a-fA-F]/Su', $this->peek()) === 0) {
				return $this->consume(1);
			}
			$sUnicode = $this->consumeExpression('/^[0-9a-fA-F]{1,6}/u');
			if ($this->strlen($sUnicode) < 6) {
				//Consume whitespace after incomplete unicode escape
				if (preg_match('/\\s/isSu', $this->peek())) {
					if ($this->comes('\r\n')) {
						$this->consume(2);
					} else {
						$this->consume(1);
					}
				}
			}
			$iUnicode = intval($sUnicode, 16);
			$sUtf32 = "";
			for ($i = 0; $i < 4; $i++) {
				$sUtf32 .= chr($iUnicode & 0xff);
				$iUnicode = $iUnicode >> 8;
			}
			return iconv('utf-32le', $this->sCharset, $sUtf32);
		}
		if ($bIsForIdentifier) {
			if (preg_match('/[a-zA-Z0-9]|-|_/u', $this->peek()) === 1) {
				return $this->consume(1);
			} else if (ord($this->peek()) > 0xa1) {
				return $this->consume(1);
			} else {
				return null;
			}
		} else {
			return $this->consume(1);
		}
		// Does not reach here
		return null;
	}

	private function parseSelector() {
		$oResult = new DeclarationBlock();
		$oResult->setSelector($this->consumeUntil('{'));
		$this->consume('{');
		$this->consumeWhiteSpace();
		$this->parseRuleSet($oResult);
		return $oResult;
	}

	private function parseRuleSet($oRuleSet) {
		while ($this->comes(';')) {
			$this->consume(';');
			$this->consumeWhiteSpace();
		}
		while (!$this->comes('}')) {
			$oRule = null;
			if($this->oParserSettings->bLenientParsing) {
				try {
					$oRule = $this->parseRule();
				} catch (UnexpectedTokenException $e) {
					try {
						$sConsume = $this->consumeUntil(array("\n", ";", '}'), true);
						// We need to “unfind” the matches to the end of the ruleSet as this will be matched later
						if($this->streql($this->substr($sConsume, $this->strlen($sConsume)-1, 1), '}')) {
							$this->iCurrentPosition--;
						} else {
							$this->consumeWhiteSpace();
							while ($this->comes(';')) {
								$this->consume(';');
							}
						}
					} catch (UnexpectedTokenException $e) {
						// We’ve reached the end of the document. Just close the RuleSet.
						return;
					}
				}
			} else {
				$oRule = $this->parseRule();
			}
			if($oRule) {
				$oRuleSet->addRule($oRule);
			}
			$this->consumeWhiteSpace();
		}
		$this->consume('}');
	}

	private function parseRule() {
		$oRule = new Rule($this->parseIdentifier());
		$this->consumeWhiteSpace();
		$this->consume(':');
		$oValue = $this->parseValue(self::listDelimiterForRule($oRule->getRule()));
		$oRule->setValue($oValue);
		if ($this->comes('!')) {
			$this->consume('!');
			$this->consumeWhiteSpace();
			$this->consume('important');
			$oRule->setIsImportant(true);
		}
		while ($this->comes(';')) {
			$this->consume(';');
			$this->consumeWhiteSpace();
		}
		return $oRule;
	}

	private function parseValue($aListDelimiters) {
		$aStack = array();
		$this->consumeWhiteSpace();
		//Build a list of delimiters and parsed values
		while (!($this->comes('}') || $this->comes(';') || $this->comes('!') || $this->comes(')'))) {
			if (count($aStack) > 0) {
				$bFoundDelimiter = false;
				foreach ($aListDelimiters as $sDelimiter) {
					if ($this->comes($sDelimiter)) {
						array_push($aStack, $this->consume($sDelimiter));
						$this->consumeWhiteSpace();
						$bFoundDelimiter = true;
						break;
					}
				}
				if (!$bFoundDelimiter) {
					//Whitespace was the list delimiter
					array_push($aStack, ' ');
				}
			}
			array_push($aStack, $this->parsePrimitiveValue());
			$this->consumeWhiteSpace();
		}
		//Convert the list to list objects
		foreach ($aListDelimiters as $sDelimiter) {
			if (count($aStack) === 1) {
				return $aStack[0];
			}
			$iStartPosition = null;
			while (($iStartPosition = array_search($sDelimiter, $aStack, true)) !== false) {
				$iLength = 2; //Number of elements to be joined
				for ($i = $iStartPosition + 2; $i < count($aStack); $i+=2) {
					if ($sDelimiter !== $aStack[$i]) {
						break;
					}
					$iLength++;
				}
				$oList = new RuleValueList($sDelimiter);
				for ($i = $iStartPosition - 1; $i - $iStartPosition + 1 < $iLength * 2; $i+=2) {
					$oList->addListComponent($aStack[$i]);
				}
				array_splice($aStack, $iStartPosition - 1, $iLength * 2 - 1, array($oList));
			}
		}
		return $aStack[0];
	}

	private static function listDelimiterForRule($sRule) {
		if (preg_match('/^font($|-)/', $sRule)) {
			return array(',', '/', ' ');
		}
		return array(',', ' ', '/');
	}

	private function parsePrimitiveValue() {
		$oValue = null;
		$this->consumeWhiteSpace();
		if (is_numeric($this->peek()) || ($this->comes('-.') && is_numeric($this->peek(1, 2))) || (($this->comes('-') || $this->comes('.')) && is_numeric($this->peek(1, 1)))) {
			$oValue = $this->parseNumericValue();
		} else if ($this->comes('#') || $this->comes('rgb') || $this->comes('hsl')) {
			$oValue = $this->parseColorValue();
		} else if ($this->comes('url')) {
			$oValue = $this->parseURLValue();
		} else if ($this->comes("'") || $this->comes('"')) {
			$oValue = $this->parseStringValue();
		} else {
			$oValue = $this->parseIdentifier(true, false);
		}
		$this->consumeWhiteSpace();
		return $oValue;
	}

	private function parseNumericValue($bForColor = false) {
		$sSize = '';
		if ($this->comes('-')) {
			$sSize .= $this->consume('-');
		}
		while (is_numeric($this->peek()) || $this->comes('.')) {
			if ($this->comes('.')) {
				$sSize .= $this->consume('.');
			} else {
				$sSize .= $this->consume(1);
			}
		}
		$fSize = floatval($sSize);
		$sUnit = null;
		foreach(explode('/', Size::ABSOLUTE_SIZE_UNITS.'/'.Size::RELATIVE_SIZE_UNITS.'/'.Size::NON_SIZE_UNITS) as $sDefinedUnit) {
			if ($this->comes($sDefinedUnit, 0, true)) {
				$sUnit = $sDefinedUnit;
				$this->consume($sDefinedUnit);
				break;
			}
		}
		return new Size($fSize, $sUnit, $bForColor);
	}

	private function parseColorValue() {
		$aColor = array();
		if ($this->comes('#')) {
			$this->consume('#');
			$sValue = $this->parseIdentifier(false);
			if ($this->strlen($sValue) === 3) {
				$sValue = $sValue[0] . $sValue[0] . $sValue[1] . $sValue[1] . $sValue[2] . $sValue[2];
			}
			$aColor = array('r' => new Size(intval($sValue[0] . $sValue[1], 16), null, true), 'g' => new Size(intval($sValue[2] . $sValue[3], 16), null, true), 'b' => new Size(intval($sValue[4] . $sValue[5], 16), null, true));
		} else {
			$sColorMode = $this->parseIdentifier(false);
			$this->consumeWhiteSpace();
			$this->consume('(');
			$iLength = $this->strlen($sColorMode);
			for ($i = 0; $i < $iLength; $i++) {
				$this->consumeWhiteSpace();
				$aColor[$sColorMode[$i]] = $this->parseNumericValue(true);
				$this->consumeWhiteSpace();
				if ($i < ($iLength - 1)) {
					$this->consume(',');
				}
			}
			$this->consume(')');
		}
		return new Color($aColor);
	}

	private function parseURLValue() {
		$bUseUrl = $this->comes('url');
		if ($bUseUrl) {
			$this->consume('url');
			$this->consumeWhiteSpace();
			$this->consume('(');
		}
		$this->consumeWhiteSpace();
		$oResult = new URL($this->parseStringValue());
		if ($bUseUrl) {
			$this->consumeWhiteSpace();
			$this->consume(')');
		}
		return $oResult;
	}
	
	/**
	* Tests an identifier for a given value. Since identifiers are all keywords, they can be vendor-prefixed. We need to check for these versions too.
	*/
	private static function identifierIs($sIdentifier, $sMatch, $bCaseInsensitive = true) {
	 	return preg_match("/^(-\\w+-)?$sMatch$/".($bCaseInsensitive ? 'i' : ''), $sIdentifier) === 1;
	}

	private function comes($sString, $iOffset = 0, $bCaseInsensitive = true) {
		if ($this->isEnd()) {
			return false;
		}
		$sPeek = $this->peek($sString, $iOffset);
		return $this->streql($sPeek, $sString, $bCaseInsensitive);
	}

	private function peek($iLength = 1, $iOffset = 0) {
		if ($this->isEnd()) {
			return '';
		}
		if (is_string($iLength)) {
			$iLength = $this->strlen($iLength);
		}
		if (is_string($iOffset)) {
			$iOffset = $this->strlen($iOffset);
		}
		$iOffset = $this->iCurrentPosition + $iOffset;
		if ($iOffset >= $this->iLength) {
			return '';
		}
		$iLength = min($iLength, $this->iLength-$iOffset);
		return $this->substr($this->sText, $iOffset, $iLength);
	}

	private function consume($mValue = 1) {
		if (is_string($mValue)) {
			$iLength = $this->strlen($mValue);
			if (!$this->streql($this->substr($this->sText, $this->iCurrentPosition, $iLength), $mValue)) {
				throw new UnexpectedTokenException($mValue, $this->peek(max($iLength, 5)));
			}
			$this->iCurrentPosition += $this->strlen($mValue);
			return $mValue;
		} else {
			if ($this->iCurrentPosition + $mValue > $this->iLength) {
				throw new UnexpectedTokenException($mValue, $this->peek(5), 'count');
			}
			$sResult = $this->substr($this->sText, $this->iCurrentPosition, $mValue);
			$this->iCurrentPosition += $mValue;
			return $sResult;
		}
	}

	private function consumeExpression($mExpression) {
		$aMatches;
		if (preg_match($mExpression, $this->inputLeft(), $aMatches, PREG_OFFSET_CAPTURE) === 1) {
			return $this->consume($aMatches[0][0]);
		}
		throw new UnexpectedTokenException($mExpression, $this->peek(5), 'expression');
	}

	private function consumeWhiteSpace() {
		do {
			while (preg_match('/\\s/isSu', $this->peek()) === 1) {
				$this->consume(1);
			}
		} while ($this->consumeComment());
	}

	private function consumeComment() {
		if ($this->comes('/*')) {
			$this->consumeUntil('*/');
			$this->consume('*/');
			return true;
		}
		return false;
	}

	private function isEnd() {
		return $this->iCurrentPosition >= $this->iLength;
	}

	private function consumeUntil($aEnd, $bIncludeEnd = false) {
		$aEnd = is_array($aEnd) ? $aEnd : array($aEnd);
		$iEndPos = null;
		foreach ($aEnd as $sEnd) {
			$iCurrentEndPos = $this->strpos($this->sText, $sEnd, $this->iCurrentPosition);
			if($iCurrentEndPos === false) {
				continue;
			}
			if($iEndPos === null || $iCurrentEndPos < $iEndPos) {
				$iEndPos = $iCurrentEndPos + ($bIncludeEnd ? $this->strlen($sEnd) : 0);
			}
		}
		if ($iEndPos === null) {
			throw new UnexpectedTokenException('One of ("'.implode('","', $aEnd).'")', $this->peek(5), 'search');
		}
		return $this->consume($iEndPos - $this->iCurrentPosition);
	}

	private function inputLeft() {
		return $this->substr($this->sText, $this->iCurrentPosition, -1);
	}

	private function substr($sString, $iStart, $iLength) {
		if ($this->oParserSettings->bMultibyteSupport) {
			return mb_substr($sString, $iStart, $iLength, $this->sCharset);
		} else {
			return substr($sString, $iStart, $iLength);
		}
	}

	private function strlen($sString) {
		if ($this->oParserSettings->bMultibyteSupport) {
			return mb_strlen($sString, $this->sCharset);
		} else {
			return strlen($sString);
		}
	}

	private function streql($sString1, $sString2, $bCaseInsensitive = true) {
		if($bCaseInsensitive) {
			return $this->strtolower($sString1) === $this->strtolower($sString2);
		} else {
			return $sString1 === $sString2;
		}
	}

	private function strtolower($sString) {
		if ($this->oParserSettings->bMultibyteSupport) {
			return mb_strtolower($sString, $this->sCharset);
		} else {
			return strtolower($sString);
		}
	}

	private function strpos($sString, $sNeedle, $iOffset) {
		if ($this->oParserSettings->bMultibyteSupport) {
			return mb_strpos($sString, $sNeedle, $iOffset, $this->sCharset);
		} else {
			return strpos($sString, $sNeedle, $iOffset);
		}
	}

}


/**
* Thrown if the CSS parsers encounters a token it did not expect
*/
class UnexpectedTokenException extends \Exception {
	private $sExpected;
	private $sFound;
	// Possible values: literal, identifier, count, expression, search
	private $sMatchType;

	public function __construct($sExpected, $sFound, $sMatchType = 'literal') {
		$this->sExpected = $sExpected;
		$this->sFound = $sFound;
		$this->sMatchType = $sMatchType;
		$sMessage = "Token “{$sExpected}” ({$sMatchType}) not found. Got “{$sFound}”.";
		if($this->sMatchType === 'search') {
			$sMessage = "Search for “{$sExpected}” returned no results. Context: “{$sFound}”.";
		} else if($this->sMatchType === 'count') {
			$sMessage = "Next token was expected to have {$sExpected} chars. Context: “{$sFound}”.";
		} else if($this->sMatchType === 'identifier') {
			$sMessage = "Identifier expected. Got “{$sFound}”";
		}
		parent::__construct($sMessage);
	}
}

interface AtRule {
	const BLOCK_RULES = 'media/document/supports/region-style/font-feature-values';
	// Since there are more set rules than block rules, we’re whitelisting the block rules and have anything else be treated as a set rule.
	const SET_RULES = 'font-face/counter-style/page/swash/styleset/annotation'; //…and more font-specific ones (to be used inside font-feature-values)
	
	public function atRuleName();
	public function atRuleArgs();
	public function __toString();
}

/**
* CSSNamespace represents an @namespace rule.
*/
class CSSNamespace implements AtRule {
	private $mUrl;
	private $sPrefix;
	
	public function __construct($mUrl, $sPrefix = null) {
		$this->mUrl = $mUrl;
		$this->sPrefix = $sPrefix;
	}
	
	public function __toString() {
		return '@namespace '.($this->sPrefix === null ? '' : $this->sPrefix.' ').$this->mUrl->__toString().';';
	}
	
	public function getUrl() {
		return $this->mUrl;
	}

	public function getPrefix() {
		return $this->sPrefix;
	}

	public function setUrl($mUrl) {
		$this->mUrl = $mUrl;
	}

	public function setPrefix($sPrefix) {
		$this->sPrefix = $sPrefix;
	}

	public function atRuleName() {
		return 'namespace';
	}

	public function atRuleArgs() {
		$aResult = array($this->mUrl);
		if($this->sPrefix) {
			array_unshift($aResult, $this->sPrefix);
		}
		return $aResult;
	}
}

/**
 * Class representing an @charset rule.
 * The following restrictions apply:
 * • May not be found in any CSSList other than the Document.
 * • May only appear at the very top of a Document’s contents.
 * • Must not appear more than once.
 */
class Charset implements AtRule {

	private $sCharset;

	public function __construct($sCharset) {
		$this->sCharset = $sCharset;
	}

	public function setCharset($sCharset) {
		$this->sCharset = $sCharset;
	}

	public function getCharset() {
		return $this->sCharset;
	}

	public function __toString() {
		return "@charset {$this->sCharset->__toString()};";
	}

	public function atRuleName() {
		return 'charset';
	}

	public function atRuleArgs() {
		return $this->sCharset;
	}
}

/**
* Class representing an @import rule.
*/
class Import implements AtRule {
	private $oLocation;
	private $sMediaQuery;
	
	public function __construct(URL $oLocation, $sMediaQuery) {
		$this->oLocation = $oLocation;
		$this->sMediaQuery = $sMediaQuery;
	}
	
	public function setLocation($oLocation) {
			$this->oLocation = $oLocation;
	}

	public function getLocation() {
			return $this->oLocation;
	}
	
	public function __toString() {
		return "@import ".$this->oLocation->__toString().($this->sMediaQuery === null ? '' : ' '.$this->sMediaQuery).';';
	}

	public function atRuleName() {
		return 'import';
	}

	public function atRuleArgs() {
		$aResult = array($this->oLocation);
		if($this->sMediaQuery) {
			array_push($aResult, $this->sMediaQuery);
		}
		return $aResult;
	}
}

/**
 * Class representing a single CSS selector. Selectors have to be split by the comma prior to being passed into this class.
 */
class Selector {

	const
			NON_ID_ATTRIBUTES_AND_PSEUDO_CLASSES_RX = '/
			(\.[\w]+)										 # classes
			|
			\[(\w+)											 # attributes
			|
			(\:(												 # pseudo classes
				link|visited|active
				|hover|focus
				|lang
				|target
				|enabled|disabled|checked|indeterminate
				|root
				|nth-child|nth-last-child|nth-of-type|nth-last-of-type
				|first-child|last-child|first-of-type|last-of-type
				|only-child|only-of-type
				|empty|contains
			))
		/ix',
			ELEMENTS_AND_PSEUDO_ELEMENTS_RX = '/
			((^|[\s\+\>\~]+)[\w]+ # elements
			|										
			\:{1,2}(								 # pseudo-elements
				after|before
				|first-letter|first-line
				|selection
			)
		)/ix';

	private $sSelector;
	private $iSpecificity;

	public function __construct($sSelector, $bCalculateSpecificity = false) {
		$this->setSelector($sSelector);
		if ($bCalculateSpecificity) {
			$this->getSpecificity();
		}
	}

	public function getSelector() {
		return $this->sSelector;
	}

	public function setSelector($sSelector) {
		$this->sSelector = trim($sSelector);
		$this->iSpecificity = null;
	}

	public function __toString() {
		return $this->getSelector();
	}

	public function getSpecificity() {
		if ($this->iSpecificity === null) {
			$a = 0;
			/// @todo should exclude \# as well as "#"
			$aMatches;
			$b = substr_count($this->sSelector, '#');
			$c = preg_match_all(self::NON_ID_ATTRIBUTES_AND_PSEUDO_CLASSES_RX, $this->sSelector, $aMatches);
			$d = preg_match_all(self::ELEMENTS_AND_PSEUDO_ELEMENTS_RX, $this->sSelector, $aMatches);
			$this->iSpecificity = ($a * 1000) + ($b * 100) + ($c * 10) + $d;
		}
		return $this->iSpecificity;
	}

}

/**
 * RuleSets contains Rule objects which always have a key and a value.
 * In CSS, Rules are expressed as follows: “key: value[0][0] value[0][1], value[1][0] value[1][1];”
 */
class Rule {

	private $sRule;
	private $mValue;
	private $bIsImportant;

	public function __construct($sRule) {
		$this->sRule = $sRule;
		$this->mValue = null;
		$this->bIsImportant = false;
	}

	public function setRule($sRule) {
		$this->sRule = $sRule;
	}

	public function getRule() {
		return $this->sRule;
	}

	public function getValue() {
		return $this->mValue;
	}

	public function setValue($mValue) {
		$this->mValue = $mValue;
	}

	/**
	 *	@deprecated Old-Style 2-dimensional array given. Retained for (some) backwards-compatibility. Use setValue() instead and wrapp the value inside a RuleValueList if necessary.
	 */
	public function setValues($aSpaceSeparatedValues) {
		$oSpaceSeparatedList = null;
		if (count($aSpaceSeparatedValues) > 1) {
			$oSpaceSeparatedList = new RuleValueList(' ');
		}
		foreach ($aSpaceSeparatedValues as $aCommaSeparatedValues) {
			$oCommaSeparatedList = null;
			if (count($aCommaSeparatedValues) > 1) {
				$oCommaSeparatedList = new RuleValueList(',');
			}
			foreach ($aCommaSeparatedValues as $mValue) {
				if (!$oSpaceSeparatedList && !$oCommaSeparatedList) {
					$this->mValue = $mValue;
					return $mValue;
				}
				if ($oCommaSeparatedList) {
					$oCommaSeparatedList->addListComponent($mValue);
				} else {
					$oSpaceSeparatedList->addListComponent($mValue);
				}
			}
			if (!$oSpaceSeparatedList) {
				$this->mValue = $oCommaSeparatedList;
				return $oCommaSeparatedList;
			} else {
				$oSpaceSeparatedList->addListComponent($oCommaSeparatedList);
			}
		}
		$this->mValue = $oSpaceSeparatedList;
		return $oSpaceSeparatedList;
	}

	/**
	 *	@deprecated Old-Style 2-dimensional array returned. Retained for (some) backwards-compatibility. Use getValue() instead and check for the existance of a (nested set of) ValueList object(s).
	 */
	public function getValues() {
		if (!$this->mValue instanceof RuleValueList) {
			return array(array($this->mValue));
		}
		if ($this->mValue->getListSeparator() === ',') {
			return array($this->mValue->getListComponents());
		}
		$aResult = array();
		foreach ($this->mValue->getListComponents() as $mValue) {
			if (!$mValue instanceof RuleValueList || $mValue->getListSeparator() !== ',') {
				$aResult[] = array($mValue);
				continue;
			}
			if ($this->mValue->getListSeparator() === ' ' || count($aResult) === 0) {
				$aResult[] = array();
			}
			foreach ($mValue->getListComponents() as $mValue) {
				$aResult[count($aResult) - 1][] = $mValue;
			}
		}
		return $aResult;
	}

	/**
	 * Adds a value to the existing value. Value will be appended if a RuleValueList exists of the given type. Otherwise, the existing value will be wrapped by one.
	 */
	public function addValue($mValue, $sType = ' ') {
		if (!is_array($mValue)) {
			$mValue = array($mValue);
		}
		if (!$this->mValue instanceof RuleValueList || $this->mValue->getListSeparator() !== $sType) {
			$mCurrentValue = $this->mValue;
			$this->mValue = new RuleValueList($sType);
			if ($mCurrentValue) {
				$this->mValue->addListComponent($mCurrentValue);
			}
		}
		foreach ($mValue as $mValueItem) {
			$this->mValue->addListComponent($mValueItem);
		}
	}

	public function setIsImportant($bIsImportant) {
		$this->bIsImportant = $bIsImportant;
	}

	public function getIsImportant() {
		return $this->bIsImportant;
	}

	public function __toString() {
		$sResult = "{$this->sRule}: ";
		if ($this->mValue instanceof Value) { //Can also be a ValueList
			$sResult .= $this->mValue->__toString();
		} else {
			$sResult .= $this->mValue;
		}
		if ($this->bIsImportant) {
			$sResult .= ' !important';
		}
		$sResult .= ';';
		return $sResult;
	}

}


/**
 * A RuleSet constructed by an unknown @-rule. @font-face rules are rendered into AtRule objects.
 */
class AtRuleSet extends RuleSet implements AtRule {

	private $sType;
	private $sArgs;

	public function __construct($sType, $sArgs = '') {
		parent::__construct();
		$this->sType = $sType;
		$this->sArgs = $sArgs;
	}

	public function atRuleName() {
		return $this->sType;
	}

	public function atRuleArgs() {
		return $this->sArgs;
	}

	public function __toString() {
		$sResult = "@{$this->sType} {$this->sArgs}{";
		$sResult .= parent::__toString();
		$sResult .= '}';
		return $sResult;
	}

}

/**
 * Declaration blocks are the parts of a css file which denote the rules belonging to a selector.
 * Declaration blocks usually appear directly inside a Document or another CSSList (mostly a MediaQuery).
 */
class DeclarationBlock extends RuleSet {

	private $aSelectors;

	public function __construct() {
		parent::__construct();
		$this->aSelectors = array();
	}

	public function setSelectors($mSelector) {
		if (is_array($mSelector)) {
			$this->aSelectors = $mSelector;
		} else {
			$this->aSelectors = explode(',', $mSelector);
		}
		foreach ($this->aSelectors as $iKey => $mSelector) {
			if (!($mSelector instanceof Selector)) {
				$this->aSelectors[$iKey] = new Selector($mSelector);
			}
		}
	}

	/**
	 * @deprecated use getSelectors()
	 */
	public function getSelector() {
		return $this->getSelectors();
	}

	/**
	 * @deprecated use setSelectors()
	 */
	public function setSelector($mSelector) {
		$this->setSelectors($mSelector);
	}

	public function getSelectors() {
		return $this->aSelectors;
	}

	/**
	 * Split shorthand declarations (e.g. +margin+ or +font+) into their constituent parts.
	 * */
	public function expandShorthands() {
		// border must be expanded before dimensions
		$this->expandBorderShorthand();
		$this->expandDimensionsShorthand();
		$this->expandFontShorthand();
		$this->expandBackgroundShorthand();
		$this->expandListStyleShorthand();
	}

	/**
	 * Create shorthand declarations (e.g. +margin+ or +font+) whenever possible.
	 * */
	public function createShorthands() {
		$this->createBackgroundShorthand();
		$this->createDimensionsShorthand();
		// border must be shortened after dimensions 
		$this->createBorderShorthand();
		$this->createFontShorthand();
		$this->createListStyleShorthand();
	}

	/**
	 * Split shorthand border declarations (e.g. <tt>border: 1px red;</tt>)
	 * Additional splitting happens in expandDimensionsShorthand
	 * Multiple borders are not yet supported as of 3
	 * */
	public function expandBorderShorthand() {
		$aBorderRules = array(
			'border', 'border-left', 'border-right', 'border-top', 'border-bottom'
		);
		$aBorderSizes = array(
			'thin', 'medium', 'thick'
		);
		$aRules = $this->getRulesAssoc();
		foreach ($aBorderRules as $sBorderRule) {
			if (!isset($aRules[$sBorderRule]))
				continue;
			$oRule = $aRules[$sBorderRule];
			$mRuleValue = $oRule->getValue();
			$aValues = array();
			if (!$mRuleValue instanceof RuleValueList) {
				$aValues[] = $mRuleValue;
			} else {
				$aValues = $mRuleValue->getListComponents();
			}
			foreach ($aValues as $mValue) {
				if ($mValue instanceof Value) {
					$mNewValue = clone $mValue;
				} else {
					$mNewValue = $mValue;
				}
				if ($mValue instanceof Size) {
					$sNewRuleName = $sBorderRule . "-width";
				} else if ($mValue instanceof Color) {
					$sNewRuleName = $sBorderRule . "-color";
				} else {
					if (in_array($mValue, $aBorderSizes)) {
						$sNewRuleName = $sBorderRule . "-width";
					} else/* if(in_array($mValue, $aBorderStyles)) */ {
						$sNewRuleName = $sBorderRule . "-style";
					}
				}
				$oNewRule = new Rule($sNewRuleName);
				$oNewRule->setIsImportant($oRule->getIsImportant());
				$oNewRule->addValue(array($mNewValue));
				$this->addRule($oNewRule);
			}
			$this->removeRule($sBorderRule);
		}
	}

	/**
	 * Split shorthand dimensional declarations (e.g. <tt>margin: 0px auto;</tt>)
	 * into their constituent parts.
	 * Handles margin, padding, border-color, border-style and border-width.
	 * */
	public function expandDimensionsShorthand() {
		$aExpansions = array(
			'margin' => 'margin-%s',
			'padding' => 'padding-%s',
			'border-color' => 'border-%s-color',
			'border-style' => 'border-%s-style',
			'border-width' => 'border-%s-width'
		);
		$aRules = $this->getRulesAssoc();
		foreach ($aExpansions as $sProperty => $sExpanded) {
			if (!isset($aRules[$sProperty]))
				continue;
			$oRule = $aRules[$sProperty];
			$mRuleValue = $oRule->getValue();
			$aValues = array();
			if (!$mRuleValue instanceof RuleValueList) {
				$aValues[] = $mRuleValue;
			} else {
				$aValues = $mRuleValue->getListComponents();
			}
			$top = $right = $bottom = $left = null;
			switch (count($aValues)) {
				case 1:
					$top = $right = $bottom = $left = $aValues[0];
					break;
				case 2:
					$top = $bottom = $aValues[0];
					$left = $right = $aValues[1];
					break;
				case 3:
					$top = $aValues[0];
					$left = $right = $aValues[1];
					$bottom = $aValues[2];
					break;
				case 4:
					$top = $aValues[0];
					$right = $aValues[1];
					$bottom = $aValues[2];
					$left = $aValues[3];
					break;
			}
			foreach (array('top', 'right', 'bottom', 'left') as $sPosition) {
				$oNewRule = new Rule(sprintf($sExpanded, $sPosition));
				$oNewRule->setIsImportant($oRule->getIsImportant());
				$oNewRule->addValue(${$sPosition});
				$this->addRule($oNewRule);
			}
			$this->removeRule($sProperty);
		}
	}

	/**
	 * Convert shorthand font declarations
	 * (e.g. <tt>font: 300 italic 11px/14px verdana, helvetica, sans-serif;</tt>)
	 * into their constituent parts.
	 * */
	public function expandFontShorthand() {
		$aRules = $this->getRulesAssoc();
		if (!isset($aRules['font']))
			return;
		$oRule = $aRules['font'];
		// reset properties to 'normal' per http://www.w3.org/TR/21/fonts.html#font-shorthand
		$aFontProperties = array(
			'font-style' => 'normal',
			'font-variant' => 'normal',
			'font-weight' => 'normal',
			'font-size' => 'normal',
			'line-height' => 'normal'
		);
		$mRuleValue = $oRule->getValue();
		$aValues = array();
		if (!$mRuleValue instanceof RuleValueList) {
			$aValues[] = $mRuleValue;
		} else {
			$aValues = $mRuleValue->getListComponents();
		}
		foreach ($aValues as $mValue) {
			if (!$mValue instanceof Value) {
				$mValue = mb_strtolower($mValue);
			}
			if (in_array($mValue, array('normal', 'inherit'))) {
				foreach (array('font-style', 'font-weight', 'font-variant') as $sProperty) {
					if (!isset($aFontProperties[$sProperty])) {
						$aFontProperties[$sProperty] = $mValue;
					}
				}
			} else if (in_array($mValue, array('italic', 'oblique'))) {
				$aFontProperties['font-style'] = $mValue;
			} else if ($mValue == 'small-caps') {
				$aFontProperties['font-variant'] = $mValue;
			} else if (
					in_array($mValue, array('bold', 'bolder', 'lighter'))
					|| ($mValue instanceof Size
					&& in_array($mValue->getSize(), range(100, 900, 100)))
			) {
				$aFontProperties['font-weight'] = $mValue;
			} else if ($mValue instanceof RuleValueList && $mValue->getListSeparator() == '/') {
				list($oSize, $oHeight) = $mValue->getListComponents();
				$aFontProperties['font-size'] = $oSize;
				$aFontProperties['line-height'] = $oHeight;
			} else if ($mValue instanceof Size && $mValue->getUnit() !== null) {
				$aFontProperties['font-size'] = $mValue;
			} else {
				$aFontProperties['font-family'] = $mValue;
			}
		}
		foreach ($aFontProperties as $sProperty => $mValue) {
			$oNewRule = new Rule($sProperty);
			$oNewRule->addValue($mValue);
			$oNewRule->setIsImportant($oRule->getIsImportant());
			$this->addRule($oNewRule);
		}
		$this->removeRule('font');
	}

	/*
	 * Convert shorthand background declarations
	 * (e.g. <tt>background: url("chess.png") gray 50% repeat fixed;</tt>)
	 * into their constituent parts.
	 * @see http://www.w3.org/TR/21/colors.html#propdef-background
	 * */

	public function expandBackgroundShorthand() {
		$aRules = $this->getRulesAssoc();
		if (!isset($aRules['background']))
			return;
		$oRule = $aRules['background'];
		$aBgProperties = array(
			'background-color' => array('transparent'), 'background-image' => array('none'),
			'background-repeat' => array('repeat'), 'background-attachment' => array('scroll'),
			'background-position' => array(new Size(0, '%'), new Size(0, '%'))
		);
		$mRuleValue = $oRule->getValue();
		$aValues = array();
		if (!$mRuleValue instanceof RuleValueList) {
			$aValues[] = $mRuleValue;
		} else {
			$aValues = $mRuleValue->getListComponents();
		}
		if (count($aValues) == 1 && $aValues[0] == 'inherit') {
			foreach ($aBgProperties as $sProperty => $mValue) {
				$oNewRule = new Rule($sProperty);
				$oNewRule->addValue('inherit');
				$oNewRule->setIsImportant($oRule->getIsImportant());
				$this->addRule($oNewRule);
			}
			$this->removeRule('background');
			return;
		}
		$iNumBgPos = 0;
		foreach ($aValues as $mValue) {
			if (!$mValue instanceof Value) {
				$mValue = mb_strtolower($mValue);
			}
			if ($mValue instanceof URL) {
				$aBgProperties['background-image'] = $mValue;
			} else if ($mValue instanceof Color) {
				$aBgProperties['background-color'] = $mValue;
			} else if (in_array($mValue, array('scroll', 'fixed'))) {
				$aBgProperties['background-attachment'] = $mValue;
			} else if (in_array($mValue, array('repeat', 'no-repeat', 'repeat-x', 'repeat-y'))) {
				$aBgProperties['background-repeat'] = $mValue;
			} else if (in_array($mValue, array('left', 'center', 'right', 'top', 'bottom'))
					|| $mValue instanceof Size
			) {
				if ($iNumBgPos == 0) {
					$aBgProperties['background-position'][0] = $mValue;
					$aBgProperties['background-position'][1] = 'center';
				} else {
					$aBgProperties['background-position'][$iNumBgPos] = $mValue;
				}
				$iNumBgPos++;
			}
		}
		foreach ($aBgProperties as $sProperty => $mValue) {
			$oNewRule = new Rule($sProperty);
			$oNewRule->setIsImportant($oRule->getIsImportant());
			$oNewRule->addValue($mValue);
			$this->addRule($oNewRule);
		}
		$this->removeRule('background');
	}

	public function expandListStyleShorthand() {
		$aListProperties = array(
			'list-style-type' => 'disc',
			'list-style-position' => 'outside',
			'list-style-image' => 'none'
		);
		$aListStyleTypes = array(
			'none', 'disc', 'circle', 'square', 'decimal-leading-zero', 'decimal',
			'lower-roman', 'upper-roman', 'lower-greek', 'lower-alpha', 'lower-latin',
			'upper-alpha', 'upper-latin', 'hebrew', 'armenian', 'georgian', 'cjk-ideographic',
			'hiragana', 'hira-gana-iroha', 'katakana-iroha', 'katakana'
		);
		$aListStylePositions = array(
			'inside', 'outside'
		);
		$aRules = $this->getRulesAssoc();
		if (!isset($aRules['list-style']))
			return;
		$oRule = $aRules['list-style'];
		$mRuleValue = $oRule->getValue();
		$aValues = array();
		if (!$mRuleValue instanceof RuleValueList) {
			$aValues[] = $mRuleValue;
		} else {
			$aValues = $mRuleValue->getListComponents();
		}
		if (count($aValues) == 1 && $aValues[0] == 'inherit') {
			foreach ($aListProperties as $sProperty => $mValue) {
				$oNewRule = new Rule($sProperty);
				$oNewRule->addValue('inherit');
				$oNewRule->setIsImportant($oRule->getIsImportant());
				$this->addRule($oNewRule);
			}
			$this->removeRule('list-style');
			return;
		}
		foreach ($aValues as $mValue) {
			if (!$mValue instanceof Value) {
				$mValue = mb_strtolower($mValue);
			}
			if ($mValue instanceof Url) {
				$aListProperties['list-style-image'] = $mValue;
			} else if (in_array($mValue, $aListStyleTypes)) {
				$aListProperties['list-style-types'] = $mValue;
			} else if (in_array($mValue, $aListStylePositions)) {
				$aListProperties['list-style-position'] = $mValue;
			}
		}
		foreach ($aListProperties as $sProperty => $mValue) {
			$oNewRule = new Rule($sProperty);
			$oNewRule->setIsImportant($oRule->getIsImportant());
			$oNewRule->addValue($mValue);
			$this->addRule($oNewRule);
		}
		$this->removeRule('list-style');
	}

	public function createShorthandProperties(array $aProperties, $sShorthand) {
		$aRules = $this->getRulesAssoc();
		$aNewValues = array();
		foreach ($aProperties as $sProperty) {
			if (!isset($aRules[$sProperty]))
				continue;
			$oRule = $aRules[$sProperty];
			if (!$oRule->getIsImportant()) {
				$mRuleValue = $oRule->getValue();
				$aValues = array();
				if (!$mRuleValue instanceof RuleValueList) {
					$aValues[] = $mRuleValue;
				} else {
					$aValues = $mRuleValue->getListComponents();
				}
				foreach ($aValues as $mValue) {
					$aNewValues[] = $mValue;
				}
				$this->removeRule($sProperty);
			}
		}
		if (count($aNewValues)) {
			$oNewRule = new Rule($sShorthand);
			foreach ($aNewValues as $mValue) {
				$oNewRule->addValue($mValue);
			}
			$this->addRule($oNewRule);
		}
	}

	public function createBackgroundShorthand() {
		$aProperties = array(
			'background-color', 'background-image', 'background-repeat',
			'background-position', 'background-attachment'
		);
		$this->createShorthandProperties($aProperties, 'background');
	}

	public function createListStyleShorthand() {
		$aProperties = array(
			'list-style-type', 'list-style-position', 'list-style-image'
		);
		$this->createShorthandProperties($aProperties, 'list-style');
	}

	/**
	 * Combine border-color, border-style and border-width into border
	 * Should be run after create_dimensions_shorthand!
	 * */
	public function createBorderShorthand() {
		$aProperties = array(
			'border-width', 'border-style', 'border-color'
		);
		$this->createShorthandProperties($aProperties, 'border');
	}

	/*
	 * Looks for long format CSS dimensional properties
	 * (margin, padding, border-color, border-style and border-width) 
	 * and converts them into shorthand CSS properties.
	 * */

	public function createDimensionsShorthand() {
		$aPositions = array('top', 'right', 'bottom', 'left');
		$aExpansions = array(
			'margin' => 'margin-%s',
			'padding' => 'padding-%s',
			'border-color' => 'border-%s-color',
			'border-style' => 'border-%s-style',
			'border-width' => 'border-%s-width'
		);
		$aRules = $this->getRulesAssoc();
		foreach ($aExpansions as $sProperty => $sExpanded) {
			$aFoldable = array();
			foreach ($aRules as $sRuleName => $oRule) {
				foreach ($aPositions as $sPosition) {
					if ($sRuleName == sprintf($sExpanded, $sPosition)) {
						$aFoldable[$sRuleName] = $oRule;
					}
				}
			}
			// All four dimensions must be present
			if (count($aFoldable) == 4) {
				$aValues = array();
				foreach ($aPositions as $sPosition) {
					$oRule = $aRules[sprintf($sExpanded, $sPosition)];
					$mRuleValue = $oRule->getValue();
					$aRuleValues = array();
					if (!$mRuleValue instanceof RuleValueList) {
						$aRuleValues[] = $mRuleValue;
					} else {
						$aRuleValues = $mRuleValue->getListComponents();
					}
					$aValues[$sPosition] = $aRuleValues;
				}
				$oNewRule = new Rule($sProperty);
				if ((string) $aValues['left'][0] == (string) $aValues['right'][0]) {
					if ((string) $aValues['top'][0] == (string) $aValues['bottom'][0]) {
						if ((string) $aValues['top'][0] == (string) $aValues['left'][0]) {
							// All 4 sides are equal
							$oNewRule->addValue($aValues['top']);
						} else {
							// Top and bottom are equal, left and right are equal
							$oNewRule->addValue($aValues['top']);
							$oNewRule->addValue($aValues['left']);
						}
					} else {
						// Only left and right are equal
						$oNewRule->addValue($aValues['top']);
						$oNewRule->addValue($aValues['left']);
						$oNewRule->addValue($aValues['bottom']);
					}
				} else {
					// No sides are equal 
					$oNewRule->addValue($aValues['top']);
					$oNewRule->addValue($aValues['left']);
					$oNewRule->addValue($aValues['bottom']);
					$oNewRule->addValue($aValues['right']);
				}
				$this->addRule($oNewRule);
				foreach ($aPositions as $sPosition) {
					$this->removeRule(sprintf($sExpanded, $sPosition));
				}
			}
		}
	}

	/**
	 * Looks for long format CSS font properties (e.g. <tt>font-weight</tt>) and 
	 * tries to convert them into a shorthand CSS <tt>font</tt> property. 
	 * At least font-size AND font-family must be present in order to create a shorthand declaration.
	 * */
	public function createFontShorthand() {
		$aFontProperties = array(
			'font-style', 'font-variant', 'font-weight', 'font-size', 'line-height', 'font-family'
		);
		$aRules = $this->getRulesAssoc();
		if (!isset($aRules['font-size']) || !isset($aRules['font-family'])) {
			return;
		}
		$oNewRule = new Rule('font');
		foreach (array('font-style', 'font-variant', 'font-weight') as $sProperty) {
			if (isset($aRules[$sProperty])) {
				$oRule = $aRules[$sProperty];
				$mRuleValue = $oRule->getValue();
				$aValues = array();
				if (!$mRuleValue instanceof RuleValueList) {
					$aValues[] = $mRuleValue;
				} else {
					$aValues = $mRuleValue->getListComponents();
				}
				if ($aValues[0] !== 'normal') {
					$oNewRule->addValue($aValues[0]);
				}
			}
		}
		// Get the font-size value
		$oRule = $aRules['font-size'];
		$mRuleValue = $oRule->getValue();
		$aFSValues = array();
		if (!$mRuleValue instanceof RuleValueList) {
			$aFSValues[] = $mRuleValue;
		} else {
			$aFSValues = $mRuleValue->getListComponents();
		}
		// But wait to know if we have line-height to add it
		if (isset($aRules['line-height'])) {
			$oRule = $aRules['line-height'];
			$mRuleValue = $oRule->getValue();
			$aLHValues = array();
			if (!$mRuleValue instanceof RuleValueList) {
				$aLHValues[] = $mRuleValue;
			} else {
				$aLHValues = $mRuleValue->getListComponents();
			}
			if ($aLHValues[0] !== 'normal') {
				$val = new RuleValueList('/');
				$val->addListComponent($aFSValues[0]);
				$val->addListComponent($aLHValues[0]);
				$oNewRule->addValue($val);
			}
		} else {
			$oNewRule->addValue($aFSValues[0]);
		}
		$oRule = $aRules['font-family'];
		$mRuleValue = $oRule->getValue();
		$aFFValues = array();
		if (!$mRuleValue instanceof RuleValueList) {
			$aFFValues[] = $mRuleValue;
		} else {
			$aFFValues = $mRuleValue->getListComponents();
		}
		$oFFValue = new RuleValueList(',');
		$oFFValue->setListComponents($aFFValues);
		$oNewRule->addValue($oFFValue);

		$this->addRule($oNewRule);
		foreach ($aFontProperties as $sProperty) {
			$this->removeRule($sProperty);
		}
	}

	public function __toString() {
		$sResult = implode(', ', $this->aSelectors) . ' {';
		$sResult .= parent::__toString();
		$sResult .= '}' . "\n";
		return $sResult;
	}

}


/**
 * RuleSet is a generic superclass denoting rules. The typical example for rule sets are declaration block.
 * However, unknown At-Rules (like @font-face) are also rule sets.
 */
abstract class RuleSet {

	private $aRules;

	public function __construct() {
		$this->aRules = array();
	}

	public function addRule(Rule $oRule) {
		$sRule = $oRule->getRule();
		if(!isset($this->aRules[$sRule])) {
			$this->aRules[$sRule] = array();
		}
		$this->aRules[$sRule][] = $oRule;
	}

	/**
	 * Returns all rules matching the given rule name
	 * @param (null|string|Rule) $mRule pattern to search for. If null, returns all rules. if the pattern ends with a dash, all rules starting with the pattern are returned as well as one matching the pattern with the dash excluded. passing a Rule behaves like calling getRules($mRule->getRule()).
	 * @example $oRuleSet->getRules('font-') //returns an array of all rules either beginning with font- or matching font.
	 * @example $oRuleSet->getRules('font') //returns array(0 => $oRule, …) or array().
	 */
	public function getRules($mRule = null) {
		if ($mRule instanceof Rule) {
			$mRule = $mRule->getRule();
		}
		$aResult = array();
		foreach($this->aRules as $sName => $aRules) {
			// Either no search rule is given or the search rule matches the found rule exactly or the search rule ends in “-” and the found rule starts with the search rule.
			if(!$mRule || $sName === $mRule || (strrpos($mRule, '-') === strlen($mRule) - strlen('-') && (strpos($sName, $mRule) === 0 || $sName === substr($mRule, 0, -1)))) {
				$aResult = array_merge($aResult, $aRules);
			}
		}
		return $aResult;
	}
	
	/**
	 * Returns all rules matching the given pattern and returns them in an associative array with the rule’s name as keys. This method exists mainly for backwards-compatibility and is really only partially useful.
	 * @param (string) $mRule pattern to search for. If null, returns all rules. if the pattern ends with a dash, all rules starting with the pattern are returned as well as one matching the pattern with the dash excluded. passing a Rule behaves like calling getRules($mRule->getRule()).
	 * Note: This method loses some information: Calling this (with an argument of 'background-') on a declaration block like { background-color: green; background-color; rgba(0, 127, 0, 0.7); } will only yield an associative array containing the rgba-valued rule while @link{getRules()} would yield an indexed array containing both.
	 */
	public function getRulesAssoc($mRule = null) {
		$aResult = array();
		foreach($this->getRules($mRule) as $oRule) {
			$aResult[$oRule->getRule()] = $oRule;
		}
		return $aResult;
	}

	/**
	* Remove a rule from this RuleSet. This accepts all the possible values that @link{getRules()} accepts. If given a Rule, it will only remove this particular rule (by identity). If given a name, it will remove all rules by that name. Note: this is different from pre-v.2.0 behaviour of PHP-CSS-Parser, where passing a Rule instance would remove all rules with the same name. To get the old behvaiour, use removeRule($oRule->getRule()).
 * @param (null|string|Rule) $mRule pattern to remove. If $mRule is null, all rules are removed. If the pattern ends in a dash, all rules starting with the pattern are removed as well as one matching the pattern with the dash excluded. Passing a Rule behaves matches by identity.
	*/
	public function removeRule($mRule) {
		if($mRule instanceof Rule) {
			$sRule = $mRule->getRule();
			if(!isset($this->aRules[$sRule])) {
				return;
			}
			foreach($this->aRules[$sRule] as $iKey => $oRule) {
				if($oRule === $mRule) {
					unset($this->aRules[$sRule][$iKey]);
				}
			}
		} else {
			foreach($this->aRules as $sName => $aRules) {
				// Either no search rule is given or the search rule matches the found rule exactly or the search rule ends in “-” and the found rule starts with the search rule or equals it (without the trailing dash).
				if(!$mRule || $sName === $mRule || (strrpos($mRule, '-') === strlen($mRule) - strlen('-') && (strpos($sName, $mRule) === 0 || $sName === substr($mRule, 0, -1)))) {
					unset($this->aRules[$sName]);
				}
			}
		}
	}

	public function __toString() {
		$sResult = '';
		foreach ($this->aRules as $aRules) {
			foreach($aRules as $oRule) {
				$sResult .= $oRule->__toString();
			}
		}
		return $sResult;
	}

}


/**
 * Parser settings class.
 *
 * Configure parser behaviour here.
 */
class Settings {
	/**
	* Multi-byte string support. If true (default), will use (slower) mb_strlen, mb_convert_case, mb_substr and mb_strpos functions. Otherwise, the normal (ASCII-Only) functions will be used.
	*/
	public $bMultibyteSupport = true;

	/**
	* The default charset for the CSS if no `@charset` rule is found. Defaults to utf-8.
	*/
	public $sDefaultCharset = 'utf-8';

	/**
	* Lenient parsing. When used (which is true by default), the parser will not choke on unexpected tokens but simply ignore them.
	*/
	public $bLenientParsing = true;

	private function __construct() {}

	public static function create() {
		return new Settings();
	}
	
	public function withMultibyteSupport($bMultibyteSupport = true) {
		$this->bMultibyteSupport = $bMultibyteSupport;
		return $this;
	}
	
	public function withDefaultCharset($sDefaultCharset) {
		$this->sDefaultCharset = $sDefaultCharset;
		return $this;
	}
	
	public function withLenientParsing($bLenientParsing = true) {
		$this->bLenientParsing = $bLenientParsing;
		return $this;
	}
	
	public function beStrict() {
		return $this->withLenientParsing(false);
	}
}

class CSSFunction extends ValueList {

	private $sName;

	public function __construct($sName, $aArguments, $sSeparator = ',') {
		if($aArguments instanceof RuleValueList) {
			$sSeparator = $aArguments->getListSeparator();
			$aArguments = $aArguments->getListComponents();
		}
		$this->sName = $sName;
		parent::__construct($aArguments, $sSeparator);
	}

	public function getName() {
		return $this->sName;
	}

	public function setName($sName) {
		$this->sName = $sName;
	}

	public function getArguments() {
		return $this->aComponents;
	}

	public function __toString() {
		$aArguments = parent::__toString();
		return "{$this->sName}({$aArguments})";
	}

}

class Color extends CSSFunction {

	public function __construct($aColor) {
		parent::__construct(implode('', array_keys($aColor)), $aColor);
	}

	public function getColor() {
		return $this->aComponents;
	}

	public function setColor($aColor) {
		$this->setName(implode('', array_keys($aColor)));
		$this->aComponents = $aColor;
	}

	public function getColorDescription() {
		return $this->getName();
	}

	public function __toString() {
		// Shorthand RGB color values
		// TODO: Include in output settings (once they’re done)
		if(implode('', array_keys($this->aComponents)) === 'rgb') {
			$sResult = sprintf(
				'%02x%02x%02x',
				$this->aComponents['r']->__toString(),
				$this->aComponents['g']->__toString(),
				$this->aComponents['b']->__toString()
			);
			return '#'.(($sResult[0] == $sResult[1]) && ($sResult[2] == $sResult[3]) && ($sResult[4] == $sResult[5]) ? "$sResult[0]$sResult[2]$sResult[4]" : $sResult);
		}
		return parent::__toString();
	}
}


abstract class PrimitiveValue extends Value {
	
}

class RuleValueList extends ValueList {

	public function __construct($sSeparator = ',') {
		parent::__construct(array(), $sSeparator);
	}

}

class Size extends PrimitiveValue {

	const ABSOLUTE_SIZE_UNITS = 'px/cm/mm/mozmm/in/pt/pc/vh/vw/vm/vmin/vmax/rem'; //vh/vw/vm(ax)/vmin/rem are absolute insofar as they don’t scale to the immediate parent (only the viewport)
	const RELATIVE_SIZE_UNITS = '%/em/ex/ch';
	const NON_SIZE_UNITS = 'deg/grad/rad/s/ms/turns/Hz/kHz';

	private $fSize;
	private $sUnit;
	private $bIsColorComponent;

	public function __construct($fSize, $sUnit = null, $bIsColorComponent = false) {
		$this->fSize = floatval($fSize);
		$this->sUnit = $sUnit;
		$this->bIsColorComponent = $bIsColorComponent;
	}

	public function setUnit($sUnit) {
		$this->sUnit = $sUnit;
	}

	public function getUnit() {
		return $this->sUnit;
	}

	public function setSize($fSize) {
		$this->fSize = floatval($fSize);
	}

	public function getSize() {
		return $this->fSize;
	}

	public function isColorComponent() {
		return $this->bIsColorComponent;
	}

	/**
	 * Returns whether the number stored in this Size really represents a size (as in a length of something on screen).
	 * @return false if the unit an angle, a duration, a frequency or the number is a component in a Color object.
	 */
	public function isSize() {
		if (in_array($this->sUnit, explode('/', self::NON_SIZE_UNITS))) {
			return false;
		}
		return !$this->isColorComponent();
	}

	public function isRelative() {
		if (in_array($this->sUnit, explode('/', self::RELATIVE_SIZE_UNITS))) {
			return true;
		}
		if ($this->sUnit === null && $this->fSize != 0) {
			return true;
		}
		return false;
	}

	public function __toString() {
		$l = localeconv();
		return preg_replace(array('/' . preg_quote($l['decimal_point'], '/') . '/', '/^(-?)0\./'), '$1.', $this->fSize) . ($this->sUnit === null ? '' : $this->sUnit);
	}

}


class String extends PrimitiveValue {

	private $sString;

	public function __construct($sString) {
		$this->sString = $sString;
	}

	public function setString($sString) {
		$this->sString = $sString;
	}

	public function getString() {
		return $this->sString;
	}

	public function __toString() {
		$sString = addslashes($this->sString);
		$sString = str_replace("\n", '\A', $sString);
		return '"' . $sString . '"';
	}

}

class URL extends PrimitiveValue {

	private $oURL;

	public function __construct(String $oURL) {
		$this->oURL = $oURL;
	}

	public function setURL(String $oURL) {
		$this->oURL = $oURL;
	}

	public function getURL() {
		return $this->oURL;
	}

	public function __toString() {
		return "url({$this->oURL->__toString()})";
	}

}

abstract class Value {

	public abstract function __toString();
}


abstract class ValueList extends Value {

	protected $aComponents;
	protected $sSeparator;

	public function __construct($aComponents = array(), $sSeparator = ',') {
		if (!is_array($aComponents)) {
			$aComponents = array($aComponents);
		}
		$this->aComponents = $aComponents;
		$this->sSeparator = $sSeparator;
	}

	public function addListComponent($mComponent) {
		$this->aComponents[] = $mComponent;
	}

	public function getListComponents() {
		return $this->aComponents;
	}

	public function setListComponents($aComponents) {
		$this->aComponents = $aComponents;
	}

	public function getListSeparator() {
		return $this->sSeparator;
	}

	public function setListSeparator($sSeparator) {
		$this->sSeparator = $sSeparator;
	}

	function __toString() {
		return implode($this->sSeparator, $this->aComponents);
	}

}
