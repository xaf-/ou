<?php

	namespace Shortcut;

	\OU_Config::IncClass("OU_Steam");

	class Steam extends \OU_Steam
	{

		private function bchexdec($hex) {
			if(strlen($hex) == 1) {
				return hexdec($hex);
			} else {
				$remain = substr($hex, 0, -1);
				$last = substr($hex, -1);
				return bcadd(bcmul(16, $this->bchexdec($remain)), hexdec($last));
			}
		}

		private function bcdechex($dec) {
			$last = bcmod($dec, 16);
			$remain = bcdiv(bcsub($dec, $last), 16);

			if($remain == 0) {
				return dechex($last);
			} else {
				return $this->bcdechex($remain).dechex($last);
			}
		}

		/**
		 * @param $timestamp
		 */
		public function WriteFiletime($timestamp){

			// TIMESTAMP to FILETIME

			$res = $timestamp;

			$res = bcadd($res, 11644473600, 7);
			$res = bcmul($res, 10000000, 7);

			$hex = $this->bcdechex($res);

			$str = str_repeat("0", 16 - strlen($hex)) . $hex;

			// Lower + Highter

			$strHig = substr($str, 0, 8);
			$strLow = substr($str, 8);

			$lower = hexdec($strLow);
			$higher = hexdec($strHig);

			$this->WriteUInt32($lower);
			$this->WriteUInt32($higher);

		}

		/**
		 * @return number|string
		 */
		public function ReadFiletime(){

			// Lower + Highter

			$lower = $this->ReadUInt32();

			$strLow = dechex($lower);
			$strLow = str_repeat("0", 8 - strlen($strLow)) . $strLow;

			$higher = $this->ReadUInt32();

			$strHig = dechex($higher);
			$strHig = str_repeat("0", 8 - strlen($strHig)) . $strHig;

			// FILETIME to TIMESTAMP

			$res = $this->bchexdec($strHig . $strLow);

			$res = bcdiv($res, '10000000', 7);
			$res = bcsub($res, 11644473600, 7);

			return $res;
		}

		/**
		 * @param int $bytes
		 * @return string
		 */
		public function ReadStr($bytes = 1)
		{
			$res = "";
			while (true)
			{
				$char = $this->Read($bytes);
				if (bin2hex($char) == str_repeat("0", $bytes * 2)) break;
				$res .= bin2hex($char);
			}
			return hex2bin($res);
		}

		/**
		 * @param $str
		 */
		public function WriteStr($str)
		{
			$this->Write($str . "\0");
		}

	}
