<?php

	namespace Shortcut;

	class VolumeID extends Base
	{

		public $DriveType;
		public $DriveSerialNumber;
		public $VolumeLabelOffset;
		public $VolumeLabelOffsetUnicode;
		public $Data;

		public function read($file){

			$pos = $file->Position;

			// TODO: S'hauria de pasar a var local.
			$VolumeIDSize = $file->ReadUInt32();
			$this->DriveType = $file->ReadUInt32();
			$this->DriveSerialNumber = $file->ReadUInt32();
			$this->VolumeLabelOffset = $file->ReadUInt32();
			if ($this->VolumeLabelOffset == 0x00000014)
				$this->VolumeLabelOffsetUnicode = $file->ReadUInt32();
			$this->Data = $file->Read($VolumeIDSize - ($file->Position - $pos));

		}

		public function write($file){

			$pos = $file->Position;

			$file->WriteUInt32(0x00000000);
			$file->WriteUInt32($this->DriveType);
			$file->WriteUInt32($this->DriveSerialNumber);
			$file->WriteUInt32($this->VolumeLabelOffset);
			if ($this->VolumeLabelOffset == 0x00000014)
				$file->WriteUInt32($this->VolumeLabelOffsetUnicode);
			$file->Write($this->Data);

			$pos2 = $file->Position;
			$file->Position = $pos;
			$file->WriteUInt32($pos2 - $pos + strlen($this->Data));
			$file->Position = $pos2;

		}
	}