<?php

	namespace Shortcut;

	require_once(dirname(__FILE__) . "/Base.php");

	class CommonNetworkRelativeLink extends Base
	{

		const FLAG_ValidDevice = 0x1;
		const FLAG_ValidNetType = 0x2;

		public $CommonNetworkRelativeLinkSize;
		public $CommonNetworkRelativeLinkFlags;
		public $NetworkProviderType;
		public $NetName;
		public $DeviceName;
		public $NetNameUnicode;
		public $DeviceNameUnicode;

		public function hasCommonNetworkRelativeLinkFlags($flag)
		{
			return ($this->CommonNetworkRelativeLinkFlags & $flag) == $flag;
		}

		public function read($file){

			$start = $file->Position;

			$this->CommonNetworkRelativeLinkSize = $file->ReadUInt32();
			$this->CommonNetworkRelativeLinkFlags = $file->ReadInt32();
			$NetNameOffset = $file->ReadUInt32();
			$DeviceNameOffset = $file->ReadUInt32();
			$this->NetworkProviderType = $file->ReadUInt32();
			if ($NetNameOffset > 0x14)
			{
				$NetNameOffsetUnicode = $file->ReadUInt32();
				$DeviceNameOffsetUnicode = $file->ReadUInt32();
			}
			$this->NetName = $file->ReadStr();
			if ($DeviceNameOffset > 0 && $DeviceNameOffset != $start + $file->Position)
				$file->Position = $start + $file->Position;
			$this->DeviceName = $file->ReadStr();
			if ($NetNameOffset > 0x14)
			{
				$this->NetNameUnicode = $file->ReadStr();
				$this->DeviceNameUnicode = $file->ReadStr();
			}

		}

		public function write($file){

			$hasUnicode = $this->NetNameUnicode || $this->DeviceNameUnicode;

			$startOffset = $file->Position;
			$file->WriteUInt32($this->CommonNetworkRelativeLinkSize);
			$file->WriteInt32($this->CommonNetworkRelativeLinkFlags);

			$posOffset = $file->Position;
			$file->WriteUInt32(0x0);
			$file->WriteUInt32(0x0);

			$file->WriteUInt32($this->NetworkProviderType);

			if ($hasUnicode)
			{
				$posOffsetUnicode = $file->Position;
				$file->WriteUInt32(0x0);
				$file->WriteUInt32(0x0);
			}

			$posName = $file->Position;
			$file->WriteStr($this->NetName);
			$posDevice = $file->Position;
			$file->WriteStr($this->DeviceName);

			$pos = $file->Position;
			$file->Position = $posOffset;
			$file->WriteUInt32($posName - $startOffset);
			$file->WriteUInt32($posDevice - $startOffset);
			$file->Position = $pos;

			if ($hasUnicode){

				$posName = $file->Position;
				$file->WriteStr($this->NetNameUnicode);
				$posDevice = $file->Position;
				$file->WriteStr($this->DeviceNameUnicode);

				$pos = $file->Position;
				$file->Position = $posOffsetUnicode;
				$file->WriteUInt32($posName - $startOffset);
				$file->WriteUInt32($posDevice - $startOffset);
				$file->Position = $pos;

			}

		}

	}