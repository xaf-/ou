<?php

	namespace Shortcut;

	require_once(dirname(__FILE__) . "/Base.php");

	class ExtraData extends Base
	{

		/**
		 * @var \Shortcut\Blocks\Base[]
		 */
		public $Blocks = array();
		public $TerminalBlock;

		public function write($file)
		{
			foreach ($this->Blocks as $block)
			{
				$file->WriteUInt32($block->BlockSize);
				$file->WriteUInt32($block::$SIGN);
				$block->write($file);
			}
			$file->WriteUInt32($this->TerminalBlock);
		}

		public function read($file)
		{

			$_blocks = array(
				"ShimDataBlock",
				"ConsoleDataBlock",
				"ConsoleFEDataBlock",
				"DarwinDataBlock",
				"EnvironmentVariableDataBlock",
				"IconEnvironmentDataBlock",
				"KnownFolderDataBlock",
				"PropertyStoreDataBlock",
				"SpecialFolderDataBlock",
				"TrackerDataBlock",
				"VistaAndAboveIDListDataBlock"
			);

			foreach ($_blocks as $block)
				require_once(dirname(__FILE__) . "/blocks/" . $block . ".php");

			while ($file->Position < $file->Size - 4){

				$blockSize = $file->ReadUInt32();
				$blockSign = sprintf("%u", $file->ReadUInt32());

				$ok = false;
				foreach ($_blocks as $block)
				{
					$class = 'Shortcut\Blocks\\' . $block;

					/* @var $class \Shortcut\Blocks\Base' */
					if ($class::$SIGN == $blockSign)
					{
						$ok = true;
						$item = new $class($blockSize);
						$item->read($file);
						$this->Blocks[] = $item;
						break;
					}
				}

				if (!$ok)
				{
					break;
				}

			}

			$this->TerminalBlock = $file->ReadUInt32();

		}
	}