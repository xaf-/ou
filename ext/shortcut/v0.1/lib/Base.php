<?php

	namespace Shortcut;

	abstract class Base{

		public function __construct(){ }

		/**
		 * @param Steam $file
		 * @return mixed
		 */
		public abstract function read($file);
		/**
		 * @param Steam $file
		 * @return mixed
		 */
		public abstract function write($file);

	}