<?php

	namespace Shortcut;

	require_once(dirname(__FILE__) . "/Base.php");
	require_once(dirname(__FILE__) . "/ItemID.php");

	class IDList extends Base
	{

		public $iDListSize;
		/**
		 * @var ItemID[]
		 */
		public $IdList = array();
		public $TerminalID;

		public function read($file){

			// TODO: S'hauria de pasar a var local.
			$this->iDListSize = $file->ReadUInt16();
			$to = $file->Position + $this->iDListSize - 2;
			while ($file->Position < $to)
			{
				$item = new ItemID();
				$item->read($file);
				$this->IdList[] = $item;
			}
			$this->TerminalID = $file->ReadUInt16();
		}

		public function write($file){

			$pos = $file->Position;

			$file->WriteUInt16(0x0);
			foreach ($this->IdList as $item)
			{
				$item->write($file);
			}
			$file->WriteUInt16($this->TerminalID);

			$pos2 = $file->Position;
			$file->Position = $pos;


			$file->WriteUInt16($pos2 - $pos - 2);


			$file->Position = $pos2;

		}

	}