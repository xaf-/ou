<?php

	namespace Shortcut;

	require_once(dirname(__FILE__) . "/Base.php");
	require_once(dirname(__FILE__) . "/VolumeID.php");
	require_once(dirname(__FILE__) . "/CommonNetworkRelativeLink.php");

	class LinkInfo extends Base
	{

		const FLAG_VolumeIDAndLocalBasePath = 1;
		const FLAG_CommonNetworkRelativeLinkAndPathSuffix = 2;

		public $LinkInfoSize;
		public $LinkInfoHeaderSize;
		public $LinkInfoFlags;
		/**
		 * @var VolumeID
		 */
		public $VolumeID;
		public $LocalBasePath;
		/**
		 * @var CommonNetworkRelativeLink
		 */
		public $CommonNetworkRelativeLink;
		public $CommonPathSuffix;
		public $LocalBasePathUnicode ;
		public $CommonPathSuffixUnicode;

		public function hasLinkInfoFlags($flag)
		{
			return ($this->LinkInfoFlags & $flag) == $flag;
		}

		public function __construct()
		{
			$this->VolumeID = new VolumeID();
			$this->CommonNetworkRelativeLink = new CommonNetworkRelativeLink();
		}

		public function read($file){

			$start = $file->Position;

			$this->LinkInfoSize = $file->ReadUInt32();
			$this->LinkInfoHeaderSize = $file->ReadUInt32();

			$hasOptional = $this->LinkInfoHeaderSize >= 0x24;

			$this->LinkInfoFlags = $file->ReadInt32();

			$VolumeIDOffset = $file->ReadUInt32();
			$LocalBasePathOffset = $file->ReadUInt32();
			$CommonNetworkRelativeLinkOffset = $file->ReadUInt32();
			$CommonPathSuffixOffset = $file->ReadUInt32();

			if ($hasOptional){
				$LocalBasePathOffsetUnicode = $file->ReadInt32();
				$CommonPathSuffixOffsetUnicode = $file->ReadInt32();
			}
			if ($this->hasLinkInfoFlags(self::FLAG_VolumeIDAndLocalBasePath))
			{
				if ($VolumeIDOffset > 0 && $VolumeIDOffset != $file->Position + $start)
					$file->Position = $start + $VolumeIDOffset;
				$this->VolumeID->read($file);
				if ($LocalBasePathOffset > 0 && $LocalBasePathOffset != $file->Position + $start)
					$file->Position = $start + $LocalBasePathOffset;
				$this->LocalBasePath = $file->ReadStr();
			}
			if ($this->hasLinkInfoFlags(self::FLAG_CommonNetworkRelativeLinkAndPathSuffix))
			{
				if ($CommonNetworkRelativeLinkOffset > 0 && $CommonNetworkRelativeLinkOffset != $file->Position + $start)
					$file->Position = $start + $CommonNetworkRelativeLinkOffset;
				$this->CommonNetworkRelativeLink->read($file);
				if ($CommonPathSuffixOffset > 0 && $CommonPathSuffixOffset != $file->Position + $start)
					$file->Position = $start + $CommonPathSuffixOffset;
				$this->CommonPathSuffix = $file->ReadStr();

			}
			if ($hasOptional){
				if ($this->hasLinkInfoFlags(self::FLAG_VolumeIDAndLocalBasePath))
				{
					if ($LocalBasePathOffsetUnicode > 0 && $LocalBasePathOffsetUnicode != $file->Position + $start)
						$file->Position = $start + $LocalBasePathOffsetUnicode;
					$this->LocalBasePathUnicode = $file->ReadStr(4);
				}
				if ($this->hasLinkInfoFlags(self::FLAG_CommonNetworkRelativeLinkAndPathSuffix))
				{
					if ($CommonPathSuffixOffsetUnicode > 0 && $CommonPathSuffixOffsetUnicode != $file->Position + $start)
						$file->Position = $start + $CommonPathSuffixOffsetUnicode;
					$this->CommonPathSuffixUnicode = $file->ReadStr(4);
				}
			}

		}

		public function write($file){

			$startOffset = $file->Position;

			$file->WriteUInt32($this->LinkInfoSize);
			$file->WriteUInt32($this->LinkInfoHeaderSize);

			$hasOpcional = false;

			$file->WriteInt32($this->LinkInfoFlags);

			$posOffsets = $file->Position;
			$file->WriteUInt32(0x0);
			$file->WriteUInt32(0x0);
			$file->WriteUInt32(0x0);
			$file->WriteUInt32(0x0);

			if ($hasOpcional)
			{
				$posOffsetsUnicode = $file->Position;
				$file->WriteInt32(0x0);
				$file->WriteInt32(0x0);
			}

			if ($this->hasLinkInfoFlags(self::FLAG_VolumeIDAndLocalBasePath))
			{
				$posVolumeID = $file->Position;
				$this->VolumeID->write($file);
				$posLocalBasePath = $file->Position;
				$file->WriteStr($this->LocalBasePath);
			}else{
				$posVolumeID = 0;
				$posLocalBasePath = 0;
			}

			if ($this->hasLinkInfoFlags(self::FLAG_CommonNetworkRelativeLinkAndPathSuffix))
			{
				$posCommonNetworkRelativeLink = $file->Position;
				$this->CommonNetworkRelativeLink->write($file);
				$posCommonPathSuffix = $file->Position;
				$file->WriteStr($this->CommonPathSuffix);
			}else{
				$posCommonNetworkRelativeLink = 0;
				$posCommonPathSuffix = 0;
			}


			$pos = $file->Position;
			$file->Position = $posOffsets;
			$file->WriteUInt32($posVolumeID > 0 ? $posVolumeID - $startOffset : 0);
			$file->WriteUInt32($posLocalBasePath > 0 ? $posLocalBasePath - $startOffset : 0);
			$file->WriteUInt32($posCommonNetworkRelativeLink > 0 ? $posCommonNetworkRelativeLink - $startOffset : 0);
			$file->WriteUInt32($posCommonPathSuffix > 0 ? $posCommonPathSuffix - $startOffset : 0);
			$file->Position = $pos;


			if ($hasOpcional)
			{
				if ($this->hasLinkInfoFlags(self::FLAG_VolumeIDAndLocalBasePath))
				{
					$posLocalBasePathUnicode = $file->Position;
					$file->WriteStr($this->LocalBasePathUnicode, 4);
				}else
					$posLocalBasePathUnicode = 0;

				if ($this->hasLinkInfoFlags(self::FLAG_CommonNetworkRelativeLinkAndPathSuffix))
				{
					$posCommonPathSuffixUnicode = $file->Position;
					$file->WriteStr($this->CommonPathSuffixUnicode, 4);
				}else
					$posCommonPathSuffixUnicode = 0;

				$pos = $file->Position;
				$file->Position = $posOffsets;
				$file->WriteUInt32($posLocalBasePathUnicode > 0 ? $posLocalBasePathUnicode - $startOffset : 0);
				$file->WriteUInt32($posCommonPathSuffixUnicode > 0 ? $posCommonPathSuffixUnicode - $startOffset : 0);
				$file->Position = $pos;

			}

		}
	}