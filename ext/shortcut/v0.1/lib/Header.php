<?php

	namespace Shortcut;

	require_once(dirname(__FILE__) . "/Base.php");

	class Header extends Base{

		const FLAG_HasLinkTargetIDList = 0x1;
		const FLAG_HasLinkInfo = 0x2;
		const FLAG_HasName = 0x4;
		const FLAG_HasRelativePath = 0x8;
		const FLAG_HasWorkingDir = 0x10;
		const FLAG_HasArguments = 0x20;
		const FLAG_HasIconLocation = 0x40;
		const FLAG_IsUnicode = 0x80;
		const FLAG_ForceNoLinkInfo = 0x100;
		const FLAG_HasExpString = 0x200;
		const FLAG_RunInSeparateProcess = 0x400;
		const FLAG_Unused1 = 0x800;
		const FLAG_HasDarwinID = 0x1000;
		const FLAG_RunAsUser = 0x2000;
		const FLAG_HasExpIcon = 0x4000;
		const FLAG_NoPidlAlias = 0x8000;
		const FLAG_Unused2 = 0x10000;
		const FLAG_RunWithShimLayer = 0x20000;
		const FLAG_ForceNoLinkTrack = 0x40000;
		const FLAG_EnableTargetMetadata = 0x80000;
		const FLAG_DisableLinkPathTracking = 0x100000;
		const FLAG_DisableKnownFolderTracking = 0x200000;
		const FLAG_DisableKnownFolderAlias = 0x400000;
		const FLAG_AllowLinkToLink = 0x800000;
		const FLAG_UnaliasOnSave = 0x1000000;
		const FLAG_PreferEnvironmentPath = 0x2000000;
		const FLAG_KeepLocalIDListForUNCTarget = 0x4000000;

		public $HeaderSize;
		public $LinkCLSID;
		public $LinkFlags;
		public $FileAttributes;
		public $CreationTime;
		public $AccessTime;
		public $WriteTime;
		public $FileSize;
		public $IconIndex;
		public $ShowCommand;
		public $HotKey;
		public $Reserved1;
		public $Reserved2;
		public $Reserved3;

		public function hasLinkFlag($flag)
		{
			return ($this->LinkFlags & $flag) == $flag;
		}

		public function read($file){


			$this->HeaderSize = $file->ReadInt32();
			$this->LinkCLSID = $file->Read(16);
			$this->LinkFlags = $file->ReadInt32();
			$this->FileAttributes = $file->ReadInt32();
			$this->CreationTime = $file->ReadFiletime();
			$this->AccessTime = $file->ReadFiletime();
			$this->WriteTime = $file->ReadFiletime();
			$this->FileSize = $file->ReadUInt32();
			$this->IconIndex = $file->ReadUInt32();
			$this->ShowCommand = $file->ReadUInt32();
			$this->HotKey = $file->ReadInt16();

			$this->Reserved1 = $file->Read(2);
			$this->Reserved2 = $file->Read(4);
			$this->Reserved3 = $file->Read(4);

		}

		public function write($file){

			$file->WriteInt32($this->HeaderSize);
			$file->Write($this->LinkCLSID, 16);
			$file->WriteInt32($this->LinkFlags);
			$file->WriteInt32($this->FileAttributes);
			$file->WriteFiletime($this->CreationTime);
			$file->writeFiletime($this->AccessTime);
			$file->writeFiletime($this->WriteTime);
			$file->WriteUInt32($this->FileSize);
			$file->WriteUInt32($this->IconIndex);
			$file->WriteUInt32($this->ShowCommand);
			$file->WriteInt16($this->HotKey, 2);

			$file->Write($this->Reserved1, 2);
			$file->Write($this->Reserved2, 4);
			$file->Write($this->Reserved3, 4);

		}

	}