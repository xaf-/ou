<?php

	namespace Shortcut\Blocks;

	require_once(dirname(__FILE__) . "/Base.php");

	class KnownFolderDataBlock extends Base
	{

		public static $SIGN = 0xA000000B;

		public $KnownFolderID;
		public $Offset;

		public function write($file)
		{
			$file->Write($this->KnownFolderID, 16);
			$file->WriteUInt32($this->Offset);
		}
		public function read($file)
		{
			$this->KnownFolderID = $file->Read(16);
			$this->Offset = $file->ReadUInt32();
		}
	}