<?php

	namespace Shortcut\Blocks;

	require_once(dirname(__FILE__) . "/Base.php");

	class ConsoleFEDataBlock extends Base
	{

		public static $SIGN = 0xA0000004;

		public $CodePage;

		public function write($file)
		{
			$file->WriteUInt32($this->CodePage);
		}
		public function read($file)
		{

			$this->CodePage = $file->ReadUInt32();

		}
	}