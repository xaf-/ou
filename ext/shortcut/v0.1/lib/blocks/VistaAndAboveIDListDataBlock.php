<?php

	namespace Shortcut\Blocks;

	require_once(dirname(__FILE__) . "/Base.php");

	class VistaAndAboveIDListDataBlock extends Base
	{

		public static $SIGN = 0xA000000C;

		public $IDList;

		public function __construct($blockSize)
		{
			parent::__construct($blockSize);
			$this->IDList = new \Shortcut\IDList();
		}

		public function write($file)
		{
			$this->IDList->write($file);
		}
		public function read($file)
		{
			$this->IDList->read($file);
		}
	}