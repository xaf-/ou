<?php

	namespace Shortcut\Blocks;

	require_once(dirname(__FILE__) . "/Base.php");

	class SpecialFolderDataBlock extends Base
	{

		public static $SIGN = 0xA0000005;

		public $SpecialFolderID;
		public $Offset;

		public function write($file)
		{
			$file->WriteUInt32($this->SpecialFolderID);
			$file->WriteUInt32($this->Offset);
		}

		public function read($file)
		{
			$this->SpecialFolderID = $file->ReadUInt32();
			$this->Offset = $file->ReadUInt32();
		}
	}