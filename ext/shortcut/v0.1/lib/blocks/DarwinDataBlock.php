<?php

	namespace Shortcut\Blocks;

	require_once(dirname(__FILE__) . "/Base.php");

	class DarwinDataBlock extends Base
	{

		public static $SIGN = 0xA0000006;

		public $DarwinDataAnsi;
		public $DarwinDataUnicode;


		public function write($file)
		{

			$file->Write($this->DarwinDataAnsi, 260);
			$file->Write($this->DarwinDataUnicode, 520);

		}
		public function read($file)
		{

			$this->DarwinDataAnsi = $file->Read(260);
			$this->DarwinDataUnicode = $file->Read(520);

		}
	}