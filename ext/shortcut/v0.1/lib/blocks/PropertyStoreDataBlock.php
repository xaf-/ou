<?php

	namespace Shortcut\Blocks;

	use Shortcut\PropertyStorage\PropertyStorage;

	require_once(dirname(__FILE__) . "/Base.php");
	require_once(dirname(__FILE__) . "/../propertyStorage/PropertyStorage.php");

	class PropertyStoreDataBlock extends Base
	{

		public static $SIGN = 0xA0000009;

		public $PropertyStore;

		public function __construct($BlockSize)
		{
			parent::__construct($BlockSize);
			$this->PropertyStore = new PropertyStorage();
		}

		public function read($file){
			$this->PropertyStore->read($file);
		}

		public function write($file){
			$this->PropertyStore->write($file);
		}

	}