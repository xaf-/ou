<?php

	namespace Shortcut\Blocks;

	require_once(dirname(__FILE__) . "/Base.php");

	class ConsoleDataBlock extends Base
	{

		const FLAG_FOREGROUND_BLUE = 0x0001;
		const FLAG_FOREGROUND_GREEN = 0x0002;
		const FLAG_FOREGROUND_RED = 0x0004;
		const FLAG_FOREGROUND_INTENSITY = 0x0008;
		const FLAG_BACKGROUND_BLUE = 0x0010;
		const FLAG_BACKGROUND_GREEN = 0x0020;
		const FLAG_BACKGROUND_RED = 0x0040;
		const FLAG_BACKGROUND_INTENSITY = 0x0080;

		public static $SIGN = 0xA0000002;

		public $FillAttributes;
		public $PopupFillAttributes;
		public $ScreenBufferSizeX;
		public $ScreenBufferSizeY;
		public $WindowSizeX;
		public $WindowSizeY;
		public $WindowOriginX;
		public $WindowOriginY;
		public $Unused1;
		public $Unused2;
		public $FontSize;
		public $FontFamily;
		public $FontWeight;
		public $FaceName;
		public $CursorSize;
		public $FullScreen;
		public $QuickEdit;
		public $InsertMode;
		public $AutoPosition;
		public $HistoryBufferSize;
		public $NumberOfHistoryBuffers;
		public $HistoryNoDup;
		public $ColorTable;

		public function write($file)
		{

			$file->WriteUInt16($this->FillAttributes);
			$file->WriteUInt16($this->PopupFillAttributes);
			$file->WriteInt16($this->ScreenBufferSizeX);
			$file->WriteInt16($this->ScreenBufferSizeY);
			$file->WriteInt16($this->WindowSizeX);
			$file->WriteInt16($this->WindowSizeY);
			$file->WriteInt16($this->WindowOriginX);
			$file->WriteInt16($this->WindowOriginY);
			$file->Write($this->Unused1, 4);
			$file->Write($this->Unused2, 4);
			$file->WriteUInt32($this->FontSize);
			$file->WriteUInt32($this->FontFamily);
			$file->WriteUInt32($this->FontWeight);
			$file->Write($this->FaceName, 64);
			$file->WriteUInt32($this->CursorSize);
			$file->WriteUInt32($this->FullScreen);
			$file->WriteUInt32($this->QuickEdit);
			$file->WriteUInt32($this->InsertMode);
			$file->WriteUInt32($this->AutoPosition);
			$file->WriteUInt32($this->HistoryBufferSize);
			$file->WriteUInt32($this->NumberOfHistoryBuffers);
			$file->WriteUInt32($this->HistoryNoDup);
			$file->WriteUInt32($this->ColorTable);

		}

		public function read($file)
		{

			$this->FillAttributes = $file->ReadUInt16();
			$this->PopupFillAttributes = $file->ReadUInt16();
			$this->ScreenBufferSizeX = $file->ReadInt16();
			$this->ScreenBufferSizeY = $file->ReadInt16();
			$this->WindowSizeX = $file->ReadInt16();
			$this->WindowSizeY = $file->ReadInt16();
			$this->WindowOriginX = $file->ReadInt16();
			$this->WindowOriginY = $file->ReadInt16();
			$this->Unused1 = $file->Read(4);
			$this->Unused2 = $file->Read(4);
			$this->FontSize = $file->ReadUInt32();
			$this->FontFamily = $file->ReadUInt32();
			$this->FontWeight = $file->ReadUInt32();
			$this->FaceName = $file->Read(64);
			$this->CursorSize = $file->ReadUInt32();
			$this->FullScreen = $file->ReadUInt32();
			$this->QuickEdit = $file->ReadUInt32();
			$this->InsertMode = $file->ReadUInt32();
			$this->AutoPosition = $file->ReadUInt32();
			$this->HistoryBufferSize = $file->ReadUInt32();
			$this->NumberOfHistoryBuffers = $file->ReadUInt32();
			$this->HistoryNoDup = $file->ReadUInt32();
			$this->ColorTable = $file->ReadUInt32();

		}
	}