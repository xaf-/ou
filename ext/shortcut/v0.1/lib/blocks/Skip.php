<?php

	namespace Shortcut\Blocks;

	require_once(dirname(__FILE__) . "/Base.php");

	abstract class Skip extends Base
	{

		private $Data;

		public function read($file){
			$this->Data = $file->Read($this->BlockSize - 4 - 4);
		}

		public function write($file){
			$file->Write($this->Data, $this->BlockSize - 4 - 4);
		}

	}