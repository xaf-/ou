<?php

	namespace Shortcut\Blocks;

	require_once(dirname(__FILE__) . "/Base.php");

	class TrackerDataBlock extends Base
	{

		public static $SIGN = 0xA0000003;

		public $Length;
		public $Version;
		public $MachineID;
		public $Droid;
		public $DroidBirth;

		public function write($file)
		{

			$file->WriteUInt32($this->Length);
			$file->WriteUInt32($this->Version);
			$file->Write($this->MachineID, 16);
			$file->Write($this->Droid, 32);
			$file->Write($this->DroidBirth, 32);

		}

		public function read($file)
		{

			$this->Length = $file->ReadUInt32();
			$this->Version = $file->ReadUInt32();
			$this->MachineID = $file->Read(16);
			$this->Droid = $file->Read(32);
			$this->DroidBirth = $file->Read(32);

		}
	}