<?php

	namespace Shortcut\Blocks;

	require_once(dirname(__FILE__) . "/../Base.php");

	abstract class Base extends \Shortcut\Base
	{

		public $BlockSize;

		public static $SIGN = null;

		public function __construct($BlockSize)
		{
			parent::__construct();
			$this->BlockSize = $BlockSize;
		}

	}