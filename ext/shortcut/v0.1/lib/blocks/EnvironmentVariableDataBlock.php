<?php

	namespace Shortcut\Blocks;

	require_once(dirname(__FILE__) . "/Base.php");

	class EnvironmentVariableDataBlock extends Base
	{

		public static $SIGN = 0xA0000001;

		public $TargetAnsi;
		public $TargetUnicode;

		public function write($file)
		{
			$file->Write($this->TargetAnsi, 260);
			$file->Write($this->TargetUnicode, 520);
		}
		public function read($file)
		{
			$this->TargetAnsi = $file->Read(260);
			$this->TargetUnicode = $file->Read(520);
		}
	}