<?php

	namespace Shortcut;

	require_once(dirname(__FILE__) . "/base.php");

	class ItemID extends Base
	{

		public $ItemIdSize;
		public $ItemIdData;

		public function read($file){

			// TODO: S'hauria de pasar a var local.
			$this->ItemIdSize = $file->ReadUInt16();
			$this->ItemIdData = $file->Read($this->ItemIdSize - 2);

		}

		public function write($file){
			$file->WriteUInt16(strlen($this->ItemIdData) + 2);
			$file->Write($this->ItemIdData);
		}
	}