<?php

	namespace Shortcut\PropertyStorage;

	require_once(dirname(__FILE__) . "/Base.php");

	class PropertyType
	{
		const VT_EMPTY = 0x0000;
		const VT_NULL = 0x0001;
		const VT_I2 = 0x0002;
		const VT_I4 = 0x0003;
		const VT_R4 = 0x0004;
		const VT_R8 = 0x0005;
		const VT_CY = 0x0006;
		const VT_DATE = 0x0007;
		const VT_BSTR = 0x0008;
		const VT_ERROR = 0x000A;
		const VT_BOOL = 0x000B;
		const VT_DECIMAL = 0x000E;
		const VT_I1 = 0x0010;
		const VT_UI1 = 0x0011;
		const VT_UI2 = 0x0012;
		const VT_UI4 = 0x0013;
		const VT_I8 = 0x0014;
		const VT_UI8 = 0x0015;
		const VT_INT = 0x0016;
		const VT_UINT = 0x0017;
		const VT_LPSTR = 0x001E;
		const VT_LPWSTR = 0x001F;
		const VT_FILETIME = 0x0040;
		const VT_BLOB = 0x0041;
		const VT_STREAM = 0x0042;
		const VT_STORAGE = 0x0043;
		const VT_STREAMED_Object = 0x0044;
		const VT_STORED_Object = 0x0045;
		const VT_BLOB_Object = 0x0046;
		const VT_CF = 0x0047;
		const VT_CLSID = 0x0048;
		const VT_VERSIONED_STREAM = 0x0049;
		const VT_VECTOR_I2 = 0x1002;
		const VT_VECTOR_I4 = 0x1003;
		const VT_VECTOR_R4 = 0x1004;
		const VT_VECTOR_R8 = 0x1005;
		const VT_VECTOR_CY = 0x1006;
		const VT_VECTOR_DATE = 0x1007;
		const VT_VECTOR_BSTR = 0x1008;
		const VT_VECTOR_ERROR = 0x100A;
		const VT_VECTOR_BOOL = 0x100B;
		const VT_VECTOR_VARIANT = 0x100C;
		const VT_VECTOR_I1 = 0x1010;
		const VT_VECTOR_UI1 = 0x1011;
		const VT_VECTOR_UI2 = 0x1012;
		const VT_VECTOR_UI4 = 0x1013;
		const VT_VECTOR_I8 = 0x1014;
		const VT_VECTOR_UI8 = 0x1015;
		const VT_VECTOR_LPSTR = 0x101E;
		const VT_VECTOR_LPWSTR = 0x101F;
		const VT_VECTOR_FILETIME = 0x1040;
		const VT_VECTOR_CF = 0x1047;
		const VT_VECTOR_CLSID = 0x1048;
		const VT_ARRAY_I2 = 0x2002;
		const VT_ARRAY_I4 = 0x2003;
		const VT_ARRAY_R4 = 0x2004;
		const VT_ARRAY_R8 = 0x2005;
		const VT_ARRAY_CY = 0x2006;
		const VT_ARRAY_DATE = 0x2007;
		const VT_ARRAY_BSTR = 0x2008;
		const VT_ARRAY_ERROR = 0x200A;
		const VT_ARRAY_BOOL = 0x200B;
		const VT_ARRAY_VARIANT = 0x200C;
		const VT_ARRAY_DECIMAL = 0x200E;
		const VT_ARRAY_I1 = 0x2010;
		const VT_ARRAY_UI1 = 0x2011;
		const VT_ARRAY_UI2 = 0x2012;
		const VT_ARRAY_UI4 = 0x2013;
		const VT_ARRAY_INT = 0x2016;
		const VT_ARRAY_UINT = 0x2017;

		/**
		 * @param \Shortcut\Steam $file
		 * @param $type
		 */
		public function readValue($file, $type){
			switch ($type)
			{
				case VT_EMPTY:
					$file->ReadUInt16();
					return 0x0000;
				case VT_NULL:
					$file->ReadUInt16();
					return 0x0001;
				case VT_I2:
					return $file->ReadInt16();
				case VT_I4:
					return $file->ReadUInt32();
				case VT_R4:
					return $file->Read(4);
				case VT_R8:
					return $file->Read(8);
				case VT_CY:
					return $file->Read(8);
				case VT_DATE:
					return $file->Read(8);
				case VT_BSTR:
					$Size = $file->ReadUInt32();
					return $file->Read($Size);
				case VT_ERROR:
					return $file->ReadUInt32();
				case VT_BOOL:
					return $file->ReadInt32();
				case VT_DECIMAL:
					$file->Read(2 + 1 + 1 + 4 + 8);
					return 0x0;
				case VT_I1:
					return $file->ReadInt32();
				case VT_UI1:
					return $file->ReadUInt32();
				case VT_UI2:
					return $file->ReadUInt32();
				case VT_UI4:
					return $file->ReadUInt32();
				case VT_I8:
					return $file->Read(8);
				case VT_UI8:
					return $file->Read(8);
				case VT_INT:
					return $file->ReadInt32();
				case VT_UINT:
					return $file->ReadUInt32();
				case VT_LPSTR:
					$Size = $file->ReadUInt32();
					return $file->Read($Size);
				case VT_LPWSTR:
					$Length = $file->ReadUInt32();
					return $file->Read($Length);
				case VT_FILETIME:
					$file->Read(4 + 4);
					return 0x0;
				case VT_BLOB:
				case VT_BLOB_Object:
					$Size = $file->ReadUInt32();
					return $file->Read($Size);
				case VT_STREAM:
				case VT_STORAGE:
				case VT_STREAMED_Object:
				case VT_STORED_Object:
					throw new \Exception("Not impl.");
					return 0x0;
				case VT_CF:
					//TODO: Continuar!
					break;
				case VT_CLSID:
					break;
				case VT_VERSIONED_STREAM:
					break;
				case VT_VECTOR_I2:
					break;
				case VT_VECTOR_I4:
					break;
				case VT_VECTOR_R4:
					break;
				case VT_VECTOR_R8:
					break;
				case VT_VECTOR_CY:
					break;
				case VT_VECTOR_DATE:
					break;
				case VT_VECTOR_BSTR:
					break;
				case VT_VECTOR_ERROR:
					break;
				case VT_VECTOR_BOOL:
					break;
				case VT_VECTOR_VARIANT:
					break;
				case VT_VECTOR_I1:
					break;
				case VT_VECTOR_UI1:
					break;
				case VT_VECTOR_UI2:
					break;
				case VT_VECTOR_UI4:
					break;
				case VT_VECTOR_I8:
					break;
				case VT_VECTOR_UI8:
					break;
				case VT_VECTOR_LPSTR:
					break;
				case VT_VECTOR_LPWSTR:
					break;
				case VT_VECTOR_FILETIME:
					break;
				case VT_VECTOR_CF:
					break;
				case VT_VECTOR_CLSID:
					break;
				case VT_ARRAY_I2:
					break;
				case VT_ARRAY_I4:
					break;
				case VT_ARRAY_R4:
					break;
				case VT_ARRAY_R8:
					break;
				case VT_ARRAY_CY:
					break;
				case VT_ARRAY_DATE:
					break;
				case VT_ARRAY_BSTR:
					break;
				case VT_ARRAY_ERROR:
					break;
				case VT_ARRAY_BOOL:
					break;
				case VT_ARRAY_VARIANT:
					break;
				case VT_ARRAY_DECIMAL:
					break;
				case VT_ARRAY_I1:
					break;
				case VT_ARRAY_UI1:
					break;
				case VT_ARRAY_UI2:
					break;
				case VT_ARRAY_UI4:
					break;
				case VT_ARRAY_INT:
					break;
				case VT_ARRAY_UINT:
					break;
			}
		}

	}

?>