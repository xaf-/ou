<?php

	namespace Shortcut;

	require_once(dirname(__FILE__) . "/lib/Base.php");
	require_once(dirname(__FILE__) . "/lib/IDList.php");
	require_once(dirname(__FILE__) . "/lib/Header.php");
	require_once(dirname(__FILE__) . "/lib/LinkInfo.php");
	require_once(dirname(__FILE__) . "/lib/Steam.php");
	require_once(dirname(__FILE__) . "/lib/ExtraData.php");


	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id$
	 */
	class File
	{

		/**
		 * @var Header
		 */
		public $Header;
		/**
		 * @var IDList
		 */
		public $IDList;
		/**
		 * @var LinkInfo
		 */
		public $LinkInfo;

		public $NameString;
		public $RelativePath;
		public $WorkingDir;
		public $CommandLineArgs;
		public $IconLocation;

		/**
		 * @var ExtraData
		 */
		public $ExtraData;

		public function __construct()
		{
			$this->Header = new Header();
			$this->IDList = new IDList();
			$this->LinkInfo = new LinkInfo();
			$this->ExtraData = new ExtraData();
		}


		/**
		 * @param Steam $file
		 * @return string
		 */
		private function readStringData($file)
		{
			$numChars = $file->ReadUInt16();
			return $file->Read($numChars * 2);
		}

		/**
		 * @param Steam $file
		 * @param string $str
		 */
		private function writeStringData($file, $str)
		{
			$file->WriteUInt16(strlen($str) / 2);
			$file->Write($str);
		}

		public function save($filename){

			$file = new Steam($filename, 'w+');

			$this->Header->write($file);

			if ($this->Header->hasLinkFlag(Header::FLAG_HasLinkTargetIDList))
				$this->IDList->write($file);

			if ($this->Header->hasLinkFlag(Header::FLAG_HasLinkInfo))
				$this->LinkInfo->write($file);

			if ($this->Header->hasLinkFlag(Header::FLAG_HasName))
				$this->writeStringData($file, $this->NameString);

			if ($this->Header->hasLinkFlag(Header::FLAG_HasRelativePath))
				$this->writeStringData($file, $this->RelativePath);

			if ($this->Header->hasLinkFlag(Header::FLAG_HasWorkingDir))
				$this->writeStringData($file, $this->WorkingDir);

			if ($this->Header->hasLinkFlag(Header::FLAG_HasArguments))
				$this->writeStringData($file, $this->CommandLineArgs);

			if ($this->Header->hasLinkFlag(Header::FLAG_HasIconLocation))
				$this->writeStringData($file, $this->IconLocation);

			$this->ExtraData->write($file);

		}

		/**
		 *
		 */
		public function open($fileName)
		{

			$file = new Steam($fileName, 'r');

			$this->Header->read($file);

			if ($this->Header->hasLinkFlag(Header::FLAG_HasLinkTargetIDList))
				$this->IDList->read($file);

			if ($this->Header->hasLinkFlag(Header::FLAG_HasLinkInfo))
				$this->LinkInfo->read($file);

			if ($this->Header->hasLinkFlag(Header::FLAG_HasName))
				$this->NameString = $this->readStringData($file);

			if ($this->Header->hasLinkFlag(Header::FLAG_HasRelativePath))
				$this->RelativePath = $this->readStringData($file);

			if ($this->Header->hasLinkFlag(Header::FLAG_HasWorkingDir))
				$this->WorkingDir = $this->readStringData($file);

			if ($this->Header->hasLinkFlag(Header::FLAG_HasArguments))
				$this->CommandLineArgs = $this->readStringData($file);

			if ($this->Header->hasLinkFlag(Header::FLAG_HasIconLocation))
				$this->IconLocation = $this->readStringData($file);

			$this->ExtraData->read($file);

		}


	}