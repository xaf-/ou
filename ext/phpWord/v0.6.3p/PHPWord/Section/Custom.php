<?php
/**
 * PHPWord_Section_Content
 *
 * @category   PHPWord
 * @package    PHPWord_Section
 * @copyright  Copyright (c) 2011 PHPWord
 */
class PHPWord_Section_Custom {
	
	/**
	 * Text content
	 * 
	 * @var string
	 */
	private $_content;
	
	
	/**
	 * Create a new Custom Element
	 * 
	 * @var string $text
	 * @var mixed $style
	 */
	public function __construct($content = null) {
		$this->_content = $content;
		return $this;
	}
	
	
	/**
	 * Get Text content
	 * 
	 * @return string
	 */
	public function getContent() {
		return $this->_content;
	}
}
?>