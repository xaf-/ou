<?php

	
	/**
	 * Crea la conexión con una base de datos
	 * @author ouanalytics
	 */

	OU_Config::IncClass(
		array(
			"OU_Path",
			"OU_App",
			'OU_Base',
			'OU_Options',
			'OU_Data_Values',
			'OU_Query'
			)
		);
		
	/**
	 * @property-read unknown $resource
	 * @property string|OU_Query $query
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_database.php 340 2014-03-16 23:37:36Z xaguilarf $
	 */
	class OU_DB_Results extends OU_Array implements Iterator 
	{
		
		private $_position = 0;
		protected $_query;
		protected $_db;
		protected $_fields;
		
		/**
		 * @var unknown Recurso de la base de datos.
		 */
		public $resource;
		public $options = array();
		
		/* Iterator */
		
			/* Iterator */
			/* (non-PHPdoc)
			 * @see Iterator::current()
			 * @return OU_DB_Result
			 */
			public function current (  )
			{
				$this->checkRow($this->_position);
				return $this->_array[$this->_position];
			}
			
			/* (non-PHPdoc)
			 * @see Iterator::key()
			* @return number
			*/
			public function key (  )
			{
				return $this->_position;
			}
			
			/**
			 * (non-PHPdoc)
			 * @see OU_Array::next()
			 */
			public function next (  )
			{
				++$this->_position;
			}
			
			/**
			 * (non-PHPdoc)
			 * @see OU_Array::rewind()
			 */
			public function rewind (  )
			{
				$this->_position = 0;
			}
			
			/* (non-PHPdoc)
			 * @see Iterator::valid()
			* @return bool
			*/
			public function valid (  )
			{
				return array_key_exists($this->_position, $this->_array);
			}

			
		/* Countable */
			
			public function count ( )
			{
				return count($this->_array);
			}
			
		/* ArrayAccess */
			
			/*
			 * (non-PHPdoc) @see ArrayAccess::offsetGet()
			* Antenció ! Nomes funciona en PHP >= 5.3.4.
			*/
			public function &offsetGet($offset) {
				
				$this->checkRow($offset);
				return $this->_array[$offset];
			}
			
		/* ----- */
			
		private function checkRow($key)
		{
			if ($this->_array[$key] === NULL)
			{
				$this->seek($key);
				$this->_array[$key] = $this->db()->Fetch($this);
			}
		}
			
		/* ----- */
		
		public function free()
		{
			foreach ($this->_array as $v) if ($v) $v->free();
			// unset($this->_array); Si es borra, es borra la informacio que es una propietat que ja existeix.
			$this->_array = array();
			$this->db()->Free($this);
		}
			
		public function changeKeys($column)
		{
			
			$a = array();
			foreach ($this->_array as $k=>$v)
			{
				$a[$v->{$column}] = $v;
			}
			$this->_array = $a;
			
			return $this;
			
		}
		
		public function __construct($query, $resource, $db=false)
		{
			$this->resource = $resource; 
			$this->_db = $db ? $db : OU_Database::db();
			$this->query = $query;
		}
		
		public function seek($offset)
		{
			if ($offset !== $this->_position)
			{
				$this->db()->Seek($this, $offset);
			}
		}
		
		/**
		 * Recibe la conexión de la base de datos
		 * @return OU_Database
		 */
		public function db()
		{
			return $this->_db;
		}
		
		/**
		 * Asigna los campos de la consulta.
		 * @param array|OU_Array $fields
		 */
		public function setFields($fields)
		{
			if (is_array($fields)) 
				$fields = OU_Array::FromArray($fields);
			$this->_fields = $fields;
		}
		
		/**
		 * Devuelve el primer registro del resultado.
		 * @return static
		 */
		public function first()
		{
			foreach ($this as $field)
				return $field;
			return false;
		}
		
		/**
		 * Elimina una columna
		 * @param string $field Nombre de la columna
		 */
		public function removeField($field)
		{
			$k = false;
			foreach ($this->getFields() as $k=>$v)
			{
				if ($v->name == $field)
				{
					break;
				}
			}
			if ($k)
			{
				foreach ($this->_array as $v2)
				{
					$v2->removeField($field);
				}
				unset($this->_fields->{$k});
			}
		}
		
		/**
		 * Renombra una columna
		 * @param string $old
		 * @param string $new
		 */
		public function renameField($old, $new)
		{
			foreach ($this->getFields() as $k=>$v)
			{
				if ($v->name == $old)
				{
					$v->name = $new;
				}
			}
			
			foreach ($this->_array as $k=>$v)
			{
				$v->renameField($old, $new);
			}
			
		}
		
		/**
		 * Recibe el campos de la consulta o FALSE si no se ha podido encontrar.
		 * @return OU_DB_Result
		 */
		public function getField($fieldName)
		{
			foreach ($this->getFields() as $field)
			{
				if ($field->orgname)
				{
					if ($field->orgname == $fieldName)
					{
						return $field;
					}
				}
				if ($field->name == $fieldName)
				{
					return $field;
				}
			}
			return false;
		}
		
		/**
		 * Recibe los campos de la consulta.
		 * @return OU_Array
		 */
		public function getFields()
		{
			return $this->_fields;
		}
		
		public function __get ( $name )
		{
			if ($name == "query" && isset($this->_query))
				return $this->_query->sql();
			else
				return parent::__get($name);
		}
		
		public function __set ( $name , $value )
		{
			if ($name == "query")
				return ($this->_query = OU_Query::q($value));
			else {
				return parent::__set($name, $value);
			}
		}
		
		/**
		 * Genera una table simple con los resultados de la consulta.
		 * @param OU_Options|array $options
		 * @return string
		 */
		public function htmlTable($options = array())
		{
			
			$options = OU_Options::FromArray(
				$options,
				array(
					"class" => false
				)
			);
			
			$html = "";
			$html .= "<table border=1".($options->class ? " class=\"$options->class\"" : "").">";
				$html .= "<thead>";
					$html .= '<tr>';
					foreach ($this->getFields() as $field)
					{
						$html .= '<td>'.addslashes($field->name).'</td>';
					}
					$html .= '</tr>';
				$html .= "</thead>";
				$html .= "<tbody>";
					foreach ($this as $a)
					{
						$html .= '<tr>';
						foreach ($this->getFields() as $field)
						{
							$html .= '<td>'.addslashes($a->getFieldStr($field->name)."").'</td>';
						}
						$html .= '</tr>';
					}
				$html .= "</tbody>";
			$html .= "</table>";
			return $html;
		}
		
		/**
		 * Recibe el valor máximo de un campo
		 * @param string $fieldName
		 */
		public function max($fieldName)
		{
			
			$v = false;
			foreach ($this as $f)
			{
				$v2 = $f->{$fieldName};
				if ($v === false)
					$v = $v2;
				else
					$v = max($v, $v2);
			}
			
			return $v;
			
		}

		/**
		 * Recibe el valor mínimo de un campo
		 * @param string $fieldName
		 * @return bool|mixed
		 */
		public function min($fieldName)
		{
				
			$v = false;
			foreach ($this as $f)
			{
				$v2 = $f->{$fieldName};
				if ($v === false)
					$v = $v2;
				else
					$v = min($v, $v2);
			}
			
			return $v;
				
		}
		
		/**
		 * @see OU_DB_Result::convertCharset()
		 * @param string $from
		 * @param string $to
		 */
		public function convertCharset($to, $from=null)
		{
			foreach ($this->_array as $a)
				$a->convertCharset($to, $from);
		}
		
	}
		
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 */
	class OU_DB_Result extends OU_Data_Values
	{
		
		public function isEqual($row)
		{
			foreach ($this->_parent->getFields() as $field)
			{
				if ($this->getField($field->name) !== $row->getField($field->name))
				{
					return false;
				} 
			}
			return true;
		}
		
		public $options = array();
		protected $_parent;
		
		/**
		 * @param OU_DB_Results $parent
		 */
		public function __construct($parent)
		{
			$this->_parent = $parent;
		}
		
		public function free()
		{
			unset($this->options);
			unset($this->values);
		}
		
		/**
		 * Elimina una columna
		 * @param string $field
		 */
		public function removeField($field)
		{
			if (isset($this->{$field}))
			{
				unset($this->{$field});
			}
		}
		
		/**
		 * Renombra una columna
		 * @param string $old
		 * @param string $new
		 */
		public function renameField($old, $new)
		{
			if (isset($this->{$old}))
			{
				$v = $this->{$old};
				unset($this->{$old});
				$this->{$new} = $v;
			}
		}
		
		/**
		 * Recibe la conexión con la base de datos
		 * @see OU_DB_Results::db()
		 * @return OU_Database
		 */
		public function db()
		{
			return $this->_parent->db();
		}
		
		/**
		 * Comprueba si existe un campo virtual. Es virtual cuando no existe valor del campo y existe una funcion con el nombre "_getNombredelcampo".
		 * @param string $fieldName
		 * @return boolean
		 */
		public function isVirtualField($fieldName)
		{
			return !isset($this->{$fieldName}) && method_exists($this, "_get" . ucfirst($fieldName));
		}

		/**
		 * Recibe el valor de un campo.
		 * @param mixed $fieldName
		 * @return mixed
		 */
		public function getField($fieldName)
		{
			if ($this->isVirtualField($fieldName))
				return call_user_func(array($this, "_get" . ucfirst($fieldName)));
			else
				return $this->{$fieldName};
				
		}

		/**
		 * Recibe el valor de un campo en string.
		 * @param mixed $fieldName
		 * @return mixed|string
		 */
		public function getFieldStr($fieldName)
		{
			$value = $this->getField($fieldName);
			if (is_object($value))
			{
				if ($value instanceof DateTime)
					return $value->format('Y-m-d H:i:s');
				else
					return $value . "";
			}else
				return $value;
		}
		
		/**
		 * @see OU_DB_Result::getField() 
		 */
		public function fromName($fieldName)
		{
			return $this->getField($fieldName);
		}
		
		/**
		 * A diferencia de fromName, fromIndex recible el valor a partir de el índice de la columna (Zero-based).
		 * @see OU_DB_Result::fromName()
		 * @param integer $fieldIndex
		 * @return mixed|boolean
		 */
		public function fromIndex($fieldIndex)
		{
			$cnt = 0;
			foreach ($this->_parent->getFields() as $k)
			{
				if ($cnt === $fieldIndex)
				{
					$fieldName = $k->name;
					return $this->fromName($fieldName);
				}
				$cnt++;
			}
			return false;
		}
		
		/**
		 * Recibe el valor buscando la columna por su nombre (si $fieldIndexOrName es un texto) o su posición (si $fieldIndexOrName es un número)
		 * @see OU_DB_Result::fromName()
		 * @see fromIndex()
		 * @param string|integer $fieldIndexOrName
		 * @return mixed
		 */
		public function fromAny($fieldIndexOrName)
		{
			if (is_numeric($fieldIndexOrName))
			{
				$fieldIndexOrName = intval($fieldIndexOrName);
				return $this->fromIndex($fieldIndexOrName);
			}else{
				return $this->fromName($fieldIndexOrName);
			}
		}

		public function getFields(){
			return $this->_parent ? $this->_parent->getFields() : null;
		}
		
		/**
		 * Rebie la información de un campo (tamaño de columna en table, alineación, valor formateado, etc.).
		 * @param string $fieldName
		 * @return array string 
		 */
		public function getDisplayInfo($fieldName)
		{
			
			$val = $this->fromName($fieldName);
			$field = $this->_parent ? $this->_parent->getField($fieldName) : null;
			
			$res = array();
				
			
			if ($field && isset($field->displayName) && $field->displayName && $field->displayName !== $fieldName)
			{
				return $this->getDisplayInfo($field->displayName);
			}
					
			$type = $field ? $field->type : MYSQLI_TYPE_VAR_STRING;
			
			switch ($type)
			{
			
				case MYSQLI_TYPE_VAR_STRING:
						
					$val = trim($val);
					if ($field && isset($field->capitalize) && $field->capitalize)
						$val = ucwords(mb_strtolower($val));
						
					break;
				case MYSQLI_TYPE_DATE:
					if ($val)
					{
						$res["width"] = 100;
						$res["align"] = "right";
						$val = strftime("%d/%m/%Y", strtotime($val));
					}
					break;
				case MYSQLI_TYPE_TIMESTAMP:
				case MYSQLI_TYPE_DATETIME:
					$res["width"] = 190;
					$res["align"] = "right";
					$val = strftime("%d/%m/%Y %H:%M:%S", strtotime($val));
					break;
			}
			
			$res["val"] = $val;
			
			return $res;
		}
		
		/**
		 * Recibe la el valor formateado de un campo. Para recibir más información consulta getDisplayInfo().
		 * @see OU_DB_Result::getDisplayInfo()
		 * @param string $fieldName
		 * @return string
		 */
		public function getDisplayValue($fieldName)
		{
			
			$res = $this->getDisplayInfo($fieldName);
			return $res["val"];
					
		}

		/**
		 * Genera una consulta SQL Update
		 * @param string $tableName Nombre de la tabla
		 * @param mixed $where Vea la documentación de OU_Query::where() para los valores admitidos.
		 * @return string
		 */
		public function generateUpdate($tableName, $where, $options = array())
		{
			
			$options = OU_Options::FromArray(
					$options,
					array(
							"exclude" => array(
							)
					)
			);
			
			$options->exclude->Add("TS"); // All timestamps
			
			$v = array();
			foreach ($this->_parent->getFields() as $field) {
				if ($options->exclude->IndexOf($field->name) !== false) continue;
				$val = $this->{$field->name};
				$v["`$field->name`"] = $val;
			}
			
			$query = 
				OU_Query::q("update $tableName")->
					sets($v)->
					// sets($this)->
					where($where);
			
			return $query->sql();
			
		}
		
		/**
		 * Genera una consulta SQL Delete
		 * @param string $tableName Nombre de la tabla
		 * @param mixed $where Vea la documentación de OU_Query::where() para los valores admitidos.
		 */
		public function generateDelete($tableName, $where)
		{
			$query = 
				OU_Query::q("delete")->
					from($tableName)->
					where($where);
			return $query->sql();
		}
		
		/**
		 * Genera una consulta SQL Insert 
		 * @return string Sentencia SQL
		 */
		public function generateInsert($tableName, $options = array())
		{
			
			$options = OU_Options::FromArray(
				$options,
				array(
					"exclude" => array(
					)
				)
			);
			
			$options->exclude->Add("TS"); // All timestamps
			
			if (!$this->_parent) return false;
			$f = array();
			$v = array();
			foreach ($this->_parent->getFields() as $field) {
				if ($options->exclude->IndexOf($field->name) !== false) continue; 
				$f[] = "`$field->name`";
				$val = $this->{$field->name};
				if ($val === null)
					$val = "NULL";
				else
					$val = "'".OU_Database::db()->slashes($val)."'";
				$v[] = $val; 
			}
			return "insert into $tableName (".implode(",", $f).") VALUES (".implode(",", $v).");";
		}
		
		/**
		 * Genera una consulta SQL Replace 
		 * @return string Sentencia SQL
		 */
		public function generateReplace($tableName, $options = array())
		{
			
			$options = OU_Options::FromArray(
				$options,
				array(
					"exclude" => array(
					)
				)
			);
			
			$options->exclude->Add("TS"); // All timestamps
			
			if (!$this->_parent) return false;
			$f = array();
			$v = array();
			foreach ($this->_parent->getFields() as $field) {
				if ($options->exclude->IndexOf($field->name) !== false) continue; 
				$f[] = "`$field->name`";
				$val = $this->{$field->name};
				if ($val === null)
					$val = "NULL";
				else
					$val = "'".OU_Database::db()->slashes($val)."'";
				$v[] = $val; 
			}
			return "replace into $tableName (".implode(",", $f).") VALUES (".implode(",", $v).");";
		}
		
		/**
		 * Convierte todos los campos de este registro de una codificación a otra.
		 * @param string $to Codificación de salida. También se permite utf8encode y utfdecode para utilizar directamente las funciones nativas.
		 * @param string $from Codificación de entrada. Vease la documentación de mb_convert_encoding() para los valores admitidos. 
		 * @return OU_DB_Result Se devuelve a si mismo para facilitar el acceso a otras funciones en una linea. Ej.: $obj->convertCharset("utf8")->generateInsert("my_table");
		 */
		public function convertCharset($to, $from=null)
		{
			
			foreach ($this->_parent->getFields() as $field)
			{
				$fieldName = $field->name;
				
				switch ($this->db()->fieldType($field->type))
				{
					case OU_Database::FIELDTYPE_BLOB:
						if (!@is_string($this->{$fieldName}))
							break;
					case OU_Database::FIELDTYPE_STRING:
						if ($to == "utf8decode")
							$this->{$fieldName} = utf8_decode($this->{$fieldName});
						else if ($to == "utf8encode")
							$this->{$fieldName} = utf8_encode($this->{$fieldName});
						else
							$this->{$fieldName} = mb_convert_encoding($this->{$fieldName}, $to, $from);
						break;
				}
			}
			
			return $this;
			
		}
		
	}

	/**
	 * @see			OU_DB_MySQL
	 * @see			OU_DB_Sqlite
	 * @author		ouanalytics
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @example		ou_database/perform.php
	 * 
	 * @property mixed $link_db
	 * 
	 * @event		beforeQuery 
	 * @event		afterQuery 
	 * 
	 * @property string $name
	 */
	abstract class OU_Database extends OU_Base {
		
		const FIELDTYPE_BLOB = MYSQLI_TYPE_BLOB;
		const FIELDTYPE_STRING = MYSQLI_TYPE_VAR_STRING;
		const FIELDTYPE_OTHER = 0;
		
		protected function getLink_db(){ return $this->_db; }
		
		// Events
		public static $beforeQueryEvent = array();
		
		protected function getName(){
			if (isset($this->_options->name))
				return $this->_options->name;
			else
				return null;
		}
		
		private $_lastSqls = array();
		
		protected static $_dbs = null;
		protected $_db;
		protected $_options;
		
		protected static function triggerEvents($array, $args = array())
		{
			foreach ($array as $a)
			{
				call_user_func_array($a, $args);
			}
		}

		/**
		 * @param string $sql
		 * @param $resource
		 * @param array|OU_Options $options
		 * @return OU_DB_Results
		 */
		protected function createResults($sql, $resource, $options = array())
		{
			$options = OU_Options::FromArray($options,
				array(
					"classResults" => "OU_DB_Results"
				)
			);
			$r = new $options->classResults($sql, $resource, $this);
			$r->options = $options;
			return $r;
		}

		/**
		 * @param OU_DB_Results $results
		 * @param array|OU_Options $options
		 * @return OU_DB_Result
		 */
		protected function createResult($results, $options = array())
		{
			$options = OU_Options::FromArray($options,
				array(
					"classResult" => "OU_DB_Result"
				)
			);
			$r = new $options->classResult($results);
			$r->options = $options;
			return $r;
		}
		
		protected function Init() { }
		
		/**
		 * Recibe las opciones por defecto
		 */
		abstract protected function getDefaultOptions();
		abstract protected function InternalQuery($sql, $options = array());
		/**
		 * @param OU_DB_Results $results
		 * @param array|OU_Array|OU_Options $options
		 */
		protected function InternalFetchFields($results, $options = array())
		{
			$options = OU_Options::FromArray(
				$options,
				array(
					
				)
			);
			if ($options->fetch_all)
			{
				while (($field = $this->Fetch($results)) !== NULL) {
					$results->Add($field);
				}
				$this->Free($results);
			}else{
				for ($i = 0; $i < $results->count(); $i++) {
					$results->Add(NULL);
				}
			}
		}
		
		protected function getOptions() {
			return array();
		}
		
		private $_try_check_connect = false;
		private $_is_checking = false;
		public function CheckConnect()
		{
			$this->_is_checking = true;
			if ($this->connected()) return;
			if ($this->_try_check_connect) return;
			$this->_try_check_connect = true;
				
			if (
				$this->_options->hasOptionKey("host") &&
				$this->_options->hasOptionKey("username") &&
				$this->_options->hasOptionKey("dbname")
			)
			{
					
				$this->Connect(
					$this->_options->host,
					$this->_options->username,
					$this->_options->hasOptionKey("password") ? $this->_options->password : "",
					$this->_options->dbname,
					$this->_options->hasOptionKey("port") ? $this->_options->port : false
				);
			
			}
			
			$this->_is_checking = false;
				
		}
		
		protected function ResetConnect()
		{
			$this->_db = null;
			$this->_try_check_connect = false;
		}
		
		public function __destruct() {}
		
		public function __construct($options = array()) {
			
			$options = OU_Options::FromArray(
				$options,
				OU_Options::FromArray(
					$this->getOptions(),
					$this->getDefaultOptions()
				)
			);
			
			$this->_options = $options;
			
			if ($options->hasOptionKey("connectOnStart") && $options->connectOnStart)
				$this->CheckConnect();
			
			
			if (self::$_dbs == null)
			{
				self::$_dbs = new OU_Array();
			}
			self::$_dbs->Add($this);
			
			$this->Init();
			
		}
		
		public function connected()
		{
			if (!$this->_is_checking)
				$this->CheckConnect();
			return $this->_db && $this->_db !== false;
		}
			
		public function runSQL($fileName, $params=array(), $options=array())
		{
			OU_Utils_Smarty::assign($params);
			$path = OU_App::app()->path() . OU_App::$_dir_sql . "/";
			$content = OU_App::app()->tpl($fileName, $path);
			return $this->Query($content, $options);
		}
		
		/**
		 * Accede a la primera conexión establecida. 
		 * @return OU_Database|boolean
		 */
		public static function db($name=null)
		{
			if (self::$_dbs)
			{
				if ($name !== null)
				{
					foreach (self::$_dbs as $db)
					{
						if ($db->name === $name)
						{
							return $db;
						}
					}
				}else
					return self::$_dbs->First();
			}
			else
				return false;
		}
		
		/**
		 * Dar formato a las comillas 
		 */
		public function slashes($str)
		{
			return addslashes($str);
		}
		
		/**
		 * Devuelve el tipo de variable según el tipo de base de datos
		 * @param number $type
		 * @return string
		 */
		public function fieldType($type)
		{
				
			switch ($field->type)
			{
				case MYSQLI_TYPE_BLOB: 
					return self::FIELDTYPE_BLOB;
				case MYSQLI_TYPE_VAR_STRING:
				case MYSQLI_TYPE_STRING:
					return self::FIELDTYPE_STRING;
				default:
					return self::FIELDTYPE_OTHER;
			}
				
		}
		
		/**
		 * Ejecuta una consulta.
		 * @param string $sql
		 * @param array|OU_Array|OU_Options $options
		 * 		Opciones:
		 * 		<table>
		 * 			<tr><td>classResults</td><td>string</td><td>Nombre de la clase donde se almacenarán los resultados. (Defecto: OU_DB_Results)</td></tr>
		 * 			<tr><td>classResult</td><td>string</td><td>Nombre de la clase donde se almacenarán las filas. (Defecto: OU_DB_Result)</td></tr>
		 * 			<tr><td>store</td><td>bool</td><td>Indica si se debe cargarse toda la información o acceder a las filas a traves de un puntero. (Defecto: True)</td></tr>
		 * 		</table>
		 * @return OU_DB_Results
		 */
		public function Query($sql, $options = array())
		{
			
			$this->raise("beforeQuery", array("sql" => $sql));
			
			$this->CheckConnect();
			
			$options = OU_Options::FromArray(
				$options,
				array(
					// "classResult" => "OU_DB_Result",
					// "classResults" => "OU_DB_Results",
					"fetch_all" => true
				)
			);
			
			self::triggerEvents(self::$beforeQueryEvent, array($this, &$sql));
			$this->_lastSqls[] = $sql;
			$q = $this->InternalQuery($sql, $options);
			if (!$q)
			{
				$last = $this->LastError();
				$last = count($last) > 0 ? $last[count($last) - 1] : ""; 
				trigger_error("Error en la consulta '$sql'. \n" . $last, E_USER_NOTICE );
			}
			
			$this->raise("afterQuery", array("sql" => $sql));
			
			return $q;
		}
		
		/**
		 * Recibe la ID del último registro insertado
		 * @return number 
		 */
		abstract public function getInsertId();
		
		/**
		 * Establece una conexión con el servidor de la base de datos.
		 * @param string $host Nombre del host
		 * @param string $user Nombre de usuario
		 * @param string $pass Contraseña de la base de datos
		 * @param string $dbname Nombre de la base de datos a utilizar
		 * @param string $port Puerto de la conexión
		 */
		abstract public function Connect($host, $user, $pass, $dbname, $port = false);
		/***
		 * Destruye la conexión con la base de datos
		 */
		abstract public function Disconnect();
		/**
		 * 
		 */
		abstract public function LastError();
		/**
		 * @param OU_DB_Results $results
		 */
		abstract public function Fetch($results);
		/**
		 * @param OU_DB_Results $results
		 */
		abstract public function Count($results);
		/**
		 * @param OU_DB_Results $results
		 */
		abstract public function Seek($results, $offset);
		/**
		 * @param OU_DB_Results $results
		 */
		abstract public function Free($results);
		
		public function LastSql(){ return OU_Array::FromArray($this->_lastSqls)->Last(); }
		public function LastSqls(){ return $this->_lastSqls; }
			
	}

?>