<?php

	/**
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 12/11/2012
	 */

	OU_Config::IncClass(
		array(
			"OU_Base"
		)			
	);
	 
	/**
	 * Permite la lectura/modificación de archivos de acceso ou. (*.ouaccess)
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_access.php 135 2012-11-19 11:51:45Z xaguilarf $
	 */
	class OU_Access extends OU_Base
	{
		private $_path = false;
		private $_values = array();
		public function __construct($path)
		{
			$this->_path = $path;
			$this->_load();
		}
		
		private function _loadFile($fn)
		{

			if (($h = fopen($fn, "r")) !== FALSE) {
			    while (($datos = fgetcsv($h, 0, " ")) !== FALSE) {
			    	
			    	if (count($datos) > 0)
			    	{
				    	$v = array();
				    	if (count($datos) > 1)
				    		for ($i = 1; $i < count($datos); $i++) $v[] = $datos[$i];
				    	
				    	$this->_values[trim($datos[0])] = $v;
				    	
			    	}
			    	
			    }
			}
			
		}
		
		public function get($name, $implodeGlue = false)
		{
			
			if (isset($this->_values[$name]))
				return $implodeGlue ? implode($implodeGlue, $this->_values[$name]) : $this->_values[$name];
			else if ($this->_parent)
				return $this->_parent->get($name, $implodeGlue);
			else
				return false;
		}
		
		private $_parent = false;
		private function _loadParent()
		{
			$old = realpath($this->_path);
			$new = realpath($this->_path . "/../");
			if ($old != $new)
				$this->_parent = new OU_Access($new);
		}
		
		private function _load()
		{
			if (is_dir($this->_path))
			{
				
				$fn = $this->_path . "/.ouaccess";
				if (file_exists($fn))
				{
					$this->_loadFile($fn);
				}
				$this->_loadParent();
				
			}else
				return false;
		}
		
	}

?>