<?php

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 25/06/2012
	 */
	 
	OU_Config::IncClass(
		array(
			"OU_Base"
		)
	);

	/**
	 * Permite la manipulación de una Array. Ofrece una serie de opciones que facilitan la gestión de las arrays.
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_array.php 335 2013-11-13 12:07:42Z xaguilarf $
	 * 
	 */
	class OU_Array extends OU_Base implements ArrayAccess, Countable, Iterator
	{

		protected $_fileName = false;
		protected $_modified = false;		
		protected $_array = array();
		protected $_autosave = false;
		
		/* Countable */
		public function count ( )
		{
			return count($this->_array);
		}
		
		/**
		 * @param array|OU_Options $options
		 */
		public function __construct($options = array())
		{
			if (is_object($options) && $options instanceof OU_Array) $options = $options->toArray();
			$this->_autosave = isset($options["autosave"]) && $options["autosave"];
			$this->_array = array();			
		}
		
		// Iterator
		//private $_iteratorPos = 0;
		public function current () {
			return current($this->_array);
			// return $this[$this->_iteratorPos];
		}
		
		public function next () {
			// ++$this->_iteratorPos;
			next($this->_array);
		}
		
		public function key () {
			//return $this->_iteratorPos;
			return key($this->_array);
		}
		
		public function valid () {
			//return isset($this[$this->_iteratorPos]);
			return key($this->_array) !== NULL;
		}
		
		public function rewind () {
			reset($this->_array);
			// $this->_iteratorPos = 0;
		}
		
		/**
		 * Indica si la array se ha modificado.
		 * @return boolean
		 */
		public function isModified()
		{
			return $this->_modified;
		}
		
		/*
		 * (non-PHPdoc) @see ArrayAccess::offsetExists()
		 */
		public function offsetExists($offset) {
			return isset($this->_array[$offset]);
		}
		/*
		 * (non-PHPdoc) @see ArrayAccess::offsetGet()
		 * Antenció ! Nomes funciona en PHP >= 5.3.4.
		 */
		public function &offsetGet($offset) {
			return $this->_array[$offset];
		}
		
		/*
		 * (non-PHPdoc) @see ArrayAccess::offsetSet()
		 */
		public function offsetSet($offset, $value) {
			if (!isset($this->_array[$offset]) || $this->_array[$offset] !== $value)
			{
				//$this->_array[$offset] = $value;
				$this->Add($offset, $value);
				
			}
		}
		/*
		 * (non-PHPdoc) @see ArrayAccess::offsetUnset()
		 */
		public function offsetUnset($offset) {
			unset($this->_array[$offset]);
			$this->DoChange();
		}
		
		/**
		 * Crea una instancia de la clase OU_Array partiendo de un array.
		 * @var Array|OU_Array $_array con las propiedades y valores de las opciones.
		 * @var Array|OU_Array $base Permite definir unas opciones "por defecto"
		 * @return OU_Array
		 */
		public static function FromArray($_array, $base=array())
		{
			
			$a1 = $_array;
			$a2 = $base;
			
			if ($a1 instanceof OU_Array) $a1 = $a1->_array;
			if ($a2 instanceof OU_Array) $a2 = $a2->_array;
			
			$a3 = array();
			foreach ($a2 as $k=>$v)
			{
				if (is_array($v))
					$a3[$k] = self::FromArray($v);
				else
					$a3[$k] = $v;
			}
			
			
			$array = $a3;
			foreach ($a1 as $k=>$v)
				if (is_array($v))
				{
					$array[$k] = self::FromArray($v, isset($a2[$k]) && (is_array($a2[$k]) || $a2[$k] instanceof OU_Array) ? $a2[$k] : array());
				} 
				else
					$array[$k] = $v;
			
			$o = new static();
			$o->_array = $array;
			return $o;
			
		}
		
		/**
		 * Crea una instancia de la clase OU_Array partiendo de los datos almacenados en un archivo.
		 * @param string $filename Dirección local del archivo a abrir.
		 * @see OU_Array::LoadFromFile()
		 * @return OU_Array
		 */
		public static function FromFile($filename, $options = array())
		{
			$options = OU_Options::FromArray($options, array("autosave" => false))->toArray();
			$array = new OU_Array($options);
			if (file_exists($filename))
			{
				$array->LoadFromFile($filename);
			}else{
				$array->_fileName = $filename;
			}
			return $array;
		}
		
		/**
		 * Permite asignar valores en $_array como si fueran propiedades de la clase OU_Array
		 * Esta función es interna de PHP y no debe accederse directamente.
		 * <code>
		 * $_array = OU_Array::FromArray(array(test => true));
		 * $_array->test = FALSE;
		 * if ($_array->test === FALSE) echo "TEST is FALSE";
		 * </code>
		 */
		public function __set($name, $value)
	    {
        	$this->Add($name, $value);
	    }
		
		/**
		 * Permite obtener los valores de $_array como si fueran propiedades de la clase OU_Array
		 * Esta función es interna de PHP y no debe accederse directamente.
		 * <code>
		 * $_array = OU_Array::FromArray(array(test => true));
		 * $_array->test = FALSE;
		 * if ($_array->test === FALSE) echo "TEST is FALSE";
		 * </code>
		 */
		public function __get ( $name )
		{
			if ($this->__isset($name))
				return $this->_array[$name];
			else
				return false;
		}
		
		/**
		 * Permite comprobar si existe un valor en la array de $_array.
		 * Esta función es interna de PHP y no debe accederse directamente. Debe utilizarse la función isset(). 
		 * <code>
		 * $_array = OU_Array::FromArray(array());
		 * $_array->test = FALSE;
		 * if (isset($_array->test)) echo "TEST is set";
		 * </code>
		 */
		public function __isset( $name)
		{
			return array_key_exists($name, $this->_array);
		}
		
		/**
		 * Permite eliminar un valor de la array de $_array.
		 * Esta función es interna de PHP y no debe accederse directamente. Debe utilizarse la función unset(). 
		 * <code>
		 * $_array = OU_Array::FromArray(array());
		 * $_array->test = FALSE;
		 * unset($_array->test);
		 * </code>
		 */
		public function __unset($name)
		{
			unset($this->_array[$name]);
			$this->DoChange();
		}
		
		/**
		 * Devuelve una array con los valores de esta clase
		 * @return Array
		 */
		public function toArray($recusrive = false)
		{
			if ($recusrive)
			{
				$a = array();
				foreach ($this->_array as $k => $v)
				{
					if (is_object($v) && $v instanceof OU_Array)
						$a[$k] = $v->toArray($recusrive);
					else
						$a[$k] = $v;
				}
				return $a;
			}else
				return $this->_array;
		}
		
		/**
		 * Notifica a la array que no se han realizado cambios.
		 */
		public function DoChange()
		{
			$this->_modified = true;
			if ($this->_autosave) $this->Save();
			$this->raise("change");
		}

		/**
		 * Devuelve la cantidad total de elementos de la array.
		 * @return number
		 */
		/*
		 * Dona problemes amb $this->count() de Countable
		public function Count()
		{
			return count($this->_array);
		}
		*/
		
		/**
		 * Vacía la array
		 */
		public function Clear()
		{
			$this->_array = array();
			$this->raise("clear");
			$this->DoChange();
			return $this;
		}
		
		/**
		 * Comprueba si existe la clave
		 * @param mixed $key
		 */
		public function Has($key)
		{
			return isset($this->_array[$key]);
		}
		
		/**
		 * Combina una array con la presente.
		 * @param array|OU_Array $array
		 * @param array|OU_Options Lista de opciones: 
		 * 	- hasKeys : Especifica si se deben utilizar las claves al combinar con otra array. (Defecto: true)
		 *  - recursive : Si el valor se encuentra en true se añadirán todas las arrays con OU_Array que encuentre recursivamente. (Defecto: false)
		 */
		public function AddArray($array, $options = array())
		{
			
			$options = OU_Options::FromArray(
					$options,
					array(
						"recursive" => false,
						"hasKeys" 	=> true
					)
			);
			
			if ($array instanceof OU_Array)
				$array = $array->toArray();
			
			foreach ($array as $k=>$v)
			{
				if (is_array($v))
				{
					$new = new OU_Array();
					$new->AddArray($v, $options);
					$v = $new;
				}
				
				if ($options->hasKeys)
					$this->Add($k, $v);
				else
					$this->Add($v);
			}
			
			return $this;
			
		}

		public function Unshift($value){
			return array_unshift($this->_array, $value);
		}

		/**
		 * Añade un elemento al array.
		 * @param string $key_or_value $key_or_value se utilizará de clave cuando $value no este definido, en caso contrario se utilizará como valor. Si el valor es NULL se utilizará el segundo parámentro como el valor añadiendo el elemento en la array con una clave numerica autoincrementada.
		 * @param string $value Valor del elemento a introducir en la variable. Si no se encuentra definido se utilizará el primer parámetro como el valor.
		 * @return $this
		 */
		public function Add($key_or_value, $value = "{�}")
		{
			
			$issetKey = 
				($value !== "{�}" && $key_or_value !== NULL);
			
			$k = $key_or_value;
			$v = $value ===  "{�}" ? $key_or_value : $value;
			
			if ($issetKey)
			{
				if (!isset($this->_array[$k]) || $this->_array[$k] !== $v)
				{
					$this->_array[$k] = $v;
					$this->DoChange();
				}
			}
			else
			{
				$this->_array[] = $v;
				$this->DoChange();
			}
			
			return $this;
			
		}
		
		/**
		 * Devuelve la clave del elemento que coincida con el valor
		 * @param mixed Valor a encontrar
		 * @param OU_Options|OU_Array|array $options Opciones disponibles:
		 *  - searchInKeys : Busca $value en las claves de los elementos. (Defecto: false)
		 *  - searchInValues : Busca $value en los valores de los elementos. (Defecto: true)
		 * @return array|bool|mixed Devuelve una array si se ha solicitado mediante $options la búsqueda de dos valores. En caso de devolver un solo valor y de encontrar $value en la array el valor devuelto es la clave del elemento encontrado o FALSE si no se ha encontrado.  
		 */
		public function IndexOf($value, $options = array())
		{
			$options = OU_Options::FromArray(
				$options,
				array(
					"searchInKeys" 		=> false,
					"searchInValues" 	=> true
				)
			);
			
			$res = array();
			
			if ($options->searchInValues)
			{
				$res[] = array_search($value, $this->_array);
			}
			
			if ($options->searchInKeys && array_key_exists($value, $this->_array))
			{
				$res[] = $value;
			}
			
			if (count($res) == 1)
				return $res[0];
			else
				return $res;
			
		}
		
		public function Shift()
		{
			if ($this->count() > 0)
			{
				array_shift($this->_array);
				$this->DoChange();
			}
		}
		
		public function Pop()
		{
			if ($this->count() > 0)
			{
				array_pop($this->_array);
				$this->DoChange();
			}
		}		
		
		/**
		 * Elimina un elemento del array desde su clave.
		 * @param mixed $key
		 */
		public function Remove($key)
		{
			if ($this->Has($key))
			{
				unset($this->_array[$key]);
				$this->DoChange();
			}
		}
		
		/**
		 * Ordena una array sin afectar al orden de las claves.
		 * @param string $fnc Defecto: strnatcmp
		 * @param array $options
		 * @return boolean
		 */
		public function Sort($fnc = null, $options = array())
		{
			
			$options = OU_Options::FromArray(
				$options,
				array(
						
				)
			);
			
			if ($fnc !== null)
			{
				return usort($this->_array, $fnc);
			}else{
				return usort($this->_array, 'strnatcmp');
			}
			
		}
		
		
		/**
		 * Ordena una array manteniendo la relacion con las claves.
		 * @param string $fnc Defecto: strnatcmp
		 * @param array $options
		 * @return boolean
		 */
		public function SortA($fnc = null, $options = array())
		{
			$options = OU_Options::FromArray(
				$options,
				array(
				)
			);
				
			if ($fnc !== null)
			{
				return uasort($this->_array, $fnc);
			}else{
				return uasort($this->_array, 'strnatcmp');
			}
		}
		
		
		/**
		 * Ordena una array mediante sus claves.
		 * @param string $fnc Defecto: strnatcmp
		 * @param array $options
		 * @return boolean
		 */
		public function SortK($fnc = null, $options = array())
		{
			$options = OU_Options::FromArray(
				$options,
				array(
				)
			);
				
			if ($fnc !== null)
			{
				return uksort($this->_array, $fnc);
			}else{
				return uksort($this->_array, 'strnatcmp');
			}
		}
		
		/**
		 * Elimina una rango de elementos de la matriz del array.
		 * @param number $offset
		 * @param number $length
		 */
		public function Splice($offset, $length = null)
		{
			$r = array_splice($this->_array, $offset, $length);
			if ($r && count($r) > 0){
				$this->DoChange();
			}
		}
		
		/**
		 * Devuelve la secuencia de elementos de la matriz array según lo especificado por los parámetros de length y offset.
		 * @param number $offset
		 * @param number|null $length
		 * @return this
		 */
		public function Slice($offset, $length = null)
		{
			return OU_Array::FromArray(array_slice($this->_array, $offset, $length));
		}
		
		/**
		 * Une elementos de un array en una cadena
		 * @param string|char $char Especifica una cadena de texto o un caracterer para separarar los elementos del array.
		 * @return string
		 */
		public function Join($char, $options = array())
		{
		    $field = isset($options["field"]) ? $options["field"] : false;
		    if ($field)
		    {
		        $str = "";
		        $cnt = 0;
		        foreach ($this->_array as $v)
		        {
		            $str .= ($cnt > 0 ? $char : "") . $v->{$field};
		            $cnt++;
		        }
		        return $str;
		    }else{
                return implode($char, $this->_array);
		    }
		}
		
		/**
		 * Separa una cadena de texto en elementos de 
		 * @param string|char $char Especifica una cadena de texto o un caracterer para separar el texto.
		 * @param string $text Texto a dividir.
		 * @return OU_Array
		 */
		public static function Split($char, $text)
		{
			return OU_Array::FromArray(explode($char, $text));
		}
		
		/**
		 * Recibe el ultimo elemento de la array. Si se especifica un valor en $value se modificará el valor del último elemento del array.
		 * @return mixed|boolean
		 */
		public function Last($value = "{�}")
		{
			if (count($this->_array) > 0)
			{
				if ($value !== "{�}")
				{
					end($this->_array);
					$this->Add($this->key(), $value);
				}else
					return end($this->_array);
			}else 
				return false;
		}
		
		/**
		 * Recibe el primer elemento del array.
		 * @return mixed|boolean
		 */
		public function First()
		{
			foreach ($this->_array as $k=>$v)
				return $v;
			return false;
		}
		
		/**
		 * Guarda los datos en el mismo archivo que se ha abierto.
		 * @return boolean
		 */
		public function Save()
		{
			if ($this->_fileName === false) return false;
			$this->SaveToFile($this->_fileName);
			return true;
		}
		
		/**
		 * Guarda los datos a un archivo.
		 * @param string $filename Dirección local del archivo a guardar.
		 */
		public function SaveToFile($filename)
		{
			// $fp = fopen($filename, 'w+');
			// fwrite($fp, serialize($this->_array));
			// fclose($fp);
			$opts = JSON_HEX_AMP;
			if (defined('JSON_PRETTY_PRINT')) $opts = $opts | JSON_PRETTY_PRINT;
			file_put_contents($filename, json_encode($this->_array, $opts));
			$this->_modified = false;
		}
		
		/**
		 * Abree y lee los datos del array desde un archivo.
		 * @param string $filename Dirección local del archivo a abrir.
		 */
		public function LoadFromFile($filename)
		{
			$this->_array = json_decode(file_get_contents($filename), true);
				
			$this->_fileName = $filename;
			//$this->_array = unserialize(file_get_contents($filename));
			$this->_modified = false;
		}
				
	}
	
?>