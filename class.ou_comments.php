<?php 

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 22/10/2012
	 */
	 
	/**
	 * Lee los comentarios de un archivo. 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_comments.php 265 2013-07-12 01:08:34Z xaguilarf $
	 */
	class OU_Comments
	{

		/**
		 * @var OU_Comment[]
		 */
		private $_comments = array();
		private $_fileName;
		private $_description;
		/**
		 * @var OU_Options
		 */
		private $_options;
		public function __construct($content, $options = array())
		{
			$this->_options = OU_Options::FromArray(
				$options,
				array(
					"numAsterisk" => 2
				)
			);
			$this->_load($content);
		}
		
		/**
		 * Obtiene la información de los comentarios de un archivo.
		 * @param string $fileName
		 * @param array|OU_Array|OU_Options $options
		 * @throws Exception
		 * @return OU_Comments
		 */
		public static function fromFile($fileName, $options = array())
		{
			if (!file_exists($fileName) || !is_file($fileName))
				throw new Exception("No se ha encontrado el archivo especificado. " . $fileName);
			$obj = new OU_Comments(file_get_contents($fileName), $options);
			$obj->_fileName = $fileName;
			return $obj;
		}
		
		/**
		 * Recible el listado de comentarios encontrados.
		 * @return OU_Comment[]
		 */
		public function comments()
		{
			return $this->_comments;
		}
		
		/**
		 * Devuelve la descripción del comentario.
		 * @return string
		 */
		public function description()
		{
			return $this->_description;
		}
		
		/**
		 * Recibe el comentario con nombre $name. En caso contrario False.
		 * @param unknown_type $name
		 * @return OU_Comment|boolean
		 */
		public function comment($name)
		{
			if (isset($this->_comments[$name]))
			{
				return $this->_comments[$name];
			}

			return false;
		}
		
		private function _load($content)
		{
			$matches = array();
			//$content = $this->_content;
			$pattern = '\/'.(str_repeat('\*', $this->_options->numAsterisk)).'.*?\*\/';
			
			
			
			if ($content && preg_match_all('/'.$pattern.'/mixs', $content, $matches, PREG_SET_ORDER))
			{
				
				foreach ($matches as $match)
				{
					$matches = array();
					$content2 = $match[0];
					$matches2 = array();
					
					$patternVar = '.*\@(?P<var_name>[\w\-]+)(?P<var_value>.*)';
					// $patternVar = '.*\@(?P<var_name>\w+)\s+(?P<var_value>.*)'; // S'ha tret \s+ entre var_name i var_value perque es liaba amb <start> !
					$patternStart = '\s*\/\*+\s*';
					$patternEnd = '.*\*+\/\s*';
					$patternElse = '(?P<else_value>.*)';
					
					$pattern = sprintf('((?P<var>%s)|(?P<start>%s)|(?P<end>%s)|(?P<else>%s))', $patternVar, $patternStart, $patternEnd, $patternElse);
					
					if ($content && preg_match_all('/^'.$pattern.'$/mix', $content2, $matches2, PREG_SET_ORDER | PREG_OFFSET_CAPTURE))
					{
						
						$else = "";
						
						$inDesc = true;
						$inVar = false;
						$lastVar = false;
						
						foreach ($matches2 as $match2)
						{

							$type = "nothing";
							if (isset($match2["else"])) $type = "else";
							else if (isset($match2["end"])) $type = "end";
							else if (isset($match2["start"])) $type = "start";
							else if (isset($match2["var"])) $type = "var";

							switch ($type)
							{
								
								case "else":
									$else2 = trim($match2["else_value"][0], "\t");
									$else2 = trim($else2, " *");
									$else2 = str_replace(array("\n", "\r"), "", $else2);
									if ($else2) 
									{
										if ($else) $else .= "\n";
										
										if ($inDesc)
											$else .= $else2;	
										else if ($inVar)
										{
											/* @var OU_Comment $last */
											$last = $lastVar;
											$last->addValue($else2);
											//$vals = &$last->values();
											//$vals->Add($else2);
											//
											//$vals[count($vals) - 1] .=  " " . $else2;
										}									
									} 
									break;
									
								case "var":
									$inDesc = false;
									$inVar = true;
									$original = $match2[0][0];
									$offset = $match2[0][1];
									$name = $match2["var_name"][0];
									$value = $match2["var_value"][0];
									
									if ($c = $this->comment($name))
									{
										$c->addValue($value);
										$lastVar = $c;
									}
									else
									{
										$lastVar = new OU_Comment($name, $value, $original, $offset);
										$this->_comments[$name] = $lastVar;
									}
									
									break;
							}
							
						}
						
						$this->_description = trim($else);
						
					}
				}
			}
			
		}
		
		private function _mb_wordwrap_array($string, $width)
		{
			if (($len = mb_strlen($string, 'UTF-8')) <= $width)
			{
				return array($string);
			}
		
			$return = array();
			$last_space = FALSE;
			$i = 0;
		
			do
			{
				if (mb_substr($string, $i, 1, 'UTF-8') == ' ')
				{
					$last_space = $i;
				}
		
				if ($i > $width)
				{
					$last_space = ($last_space == 0) ? $width : $last_space;
		
					$return[] = trim(mb_substr($string, 0, $last_space, 'UTF-8'));
					$string = mb_substr($string, $last_space, $len, 'UTF-8');
					$len = mb_strlen($string, 'UTF-8');
					$i = 0;
				}
		
				$i++;
			}
			while ($i < $len);
		
			$return[] = trim($string);
		
			return $return;
		}
		
		/**
		 * Genera el comentario a string.
		 * @param array|OU_Options|OU_Array $options
		 * @return string
		 */
		public function toString($options = array())
		{
			
			$options = OU_Options::FromArray($options, array(
				"width" => 90
			));
			
			$str = "";
			$str .= "/".str_repeat("*", $this->_options->numAsterisk)."\n";
			
			$desc = $this->description();
			$ds = explode("\n", $desc);
			$desc = "";
			foreach ($ds as $d)
			{
				$a = $this->_mb_wordwrap_array(trim($d), $options->width);
				foreach ($a as $b)
				{
					$desc .= " * " . $b . "\n";
				}
			}
			
			$str .= $desc;
			
			foreach ($this->_comments as $comment)
			{
				/* @var $comment OU_Comment */
				$str .= sprintf(" * @%s %s", $comment->name(), $comment->value()) . "\n";
			}
			$str .= " */\n";
			return $str;
		}
		
	}
	 
	/**
	 * Información sobre un elemento de los comentarios.
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * 
	 */
	class OU_Comment
	{
		
		private $_name;
		private $_values = array();
		private $_original;
		private $_offset;
		
		/**
		 * Recibe el nombre del comentario 
		 */
		public function name() { return $this->_name; }
		
		public static function _paramizeValue($value)
		{
			$r =  str_getcsv(trim(str_replace("\t", " ", $value), "; "), " ");
			return $r;
		}
		
		/**
		 * Devuelve un valor separado por parametros en una array. Ejemplo "param1" param2 "param3 more3" = array(3).
		 * @return array
		 */
		public function paramized() {
			return self::_paramizeValue($this->value());
		}
		
		/**
		 * Como self::values() pero paramarizado. Vea self::paramized() para mas información.
		 * @return multitype:multitype: 
		 */
		public function paramizedValues()
		{
			$a = array();
			foreach ($this->values() as $value)
				$a[] = self::_paramizeValue($value);
			return $a;
		}
		/**
		 * Recibe el valor del comentario 
		 */
		public function value($value = null) { if ($value !== null) { $this->_values[0] = $value; } return OU_Array::FromArray($this->_values)->First(); }
		/**
		 * Recibe el listao de valores de este comentario
		 * @return array
		 */
		public function &values() { return $this->_values; }
		
		/**
		 * Compara si coincide este comentario con $name. (Insencible a mayúsculas).
		 * @param string $name
		 * @return boolean
		 */
		public function is($name)
		{
			return mb_strtolower($this->name()) === mb_strtolower(trim($name));
		}
		
		/**
		 * Añade un valor a este comentario
		 * @param mixed $val
		 */
		public function addValue($val)
		{
			$this->_values[] = trim($val);
		}
		
		public function __construct($name, $value, $original, $offset)
		{
			$this->_name = trim($name);
			$this->addValue($value);
			$this->_original = $original;
			$this->_offset = $offset;
		}
		
	}

?>