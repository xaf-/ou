<?php

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 25/06/2012
	 */
	 
	OU_Config::IncClass(
		array(
			"OU_Array",
			"OU_Base",
			"OU_Options"
		)
	);
	
	/**
	 * Genera consultas SQL y las ejecuta a la base de datos. 
	 * 
	 * @author		fontcolor
	 * @package		OU Framework
	 * @example		ou_query.php
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_query.php 305 2013-09-05 10:54:14Z xaguilarf $
	 * 
	 */
	class OU_Query extends OU_Base {
		
		private static $REGEXP_QUERYTYPE = '/^\s*(?P<queryType>\w+)(.*)$/xim';
		private static $REGEXP_QUERYSELECT = 
			'/^
			\s*
			SELECT(?P<fields>.*?)
			(|FROM(?P<tables>.*?)
				(|WHERE(?P<where>.*?))
				(|GROUP BY(?P<groupBy>.*?))
				(|HAVING(?P<having>.*?))
				(|ORDER BY(?P<orderBy>.*?))
				(|LIMIT(?P<limit>.*?))
				(|PROCEDURE(?P<proc>.*?))
				(|INTO \w+(?P<into>.*?))
			)
			\s*
			$/muisx';
		private static $REGEXP_QUERYTRUNCATE = 
			'/^
			\s*
			TRUNCATE(?P<tables>.*?)
			\s*
			$/muisx';
		private static $REGEXP_QUERYUPDATE= 
			'/^
			\s*
			UPDATE(?P<tables>.*?)
			(|
				(|SET(?P<sets>.*?))
				(|WHERE(?P<where>.*?))
				(|ORDER BY(?P<orderBy>.*?))
				(|LIMIT(?P<limit>.*?))
			)
			\s*
			$/muisx';
		private static $REGEXP_QUERYINSERT= 
			'/^
			\s*
			INSERT(.*?)INTO(?P<tables>.*?)
			(|
				(|SET(?P<sets>.*?))
			)
			\s*
			$/muisx';
		private static $REGEXP_QUERYDROPTABLE= 
			'/^
			\s*
			DROP(.*?)TABLE(?P<tables>.*?)
			\s*
			$/muisx';
		
		private $_queryType = "select";
		private $_where = false;
		private $_having;
		private $_tables;
		private $_fields;
		private $_sets;
		private $_orderBy;
		private $_groupBy;
		
		private $_limitFrom = false;
		private $_limitCount = false;
		
		public  function __construct() {
			
			$this->_tables = new OU_Array();
			$this->_fields = new OU_Array();
			$this->_sets = new OU_Array();
			$this->_orderBy = new OU_Array();
			$this->_groupBy = new OU_Array();
			
		}
	
		function __destruct() {
		}
		
		/**
		 * @see addslashes()
		 * @param string $value
		 * @return string
		 */
		public static function slashes($value)
		{
			return addslashes($value);
		}
		
		/**
		 * Modifica el tipo de consulta (select, update, insert...)
		 * @param string $type
		 * @return OU_Query
		 */
		public function setQueryType($type)
		{
			$this->_queryType = trim(strtolower($type));
			return $this;
		}
		
		/**
		 * @see	OU_Query::setQueryType()
		 * @return OU_Query
		 */
		public static function select()
		{
			return self::q("select");
		}
		
		/**
		 * Inicializa una consulta SQL. $query puede contener todo o parte de la consulta. Ejemplo : 
		 * <code>
		 * 	$result = OU_Query::q("select")->from("table_1")->where("field_1='hola'");
		 * 	$result = OU_Query::q("select *")->from("table_1")->where("field_1='hola'");
		 * 	$result = OU_Query::q("select * from table_1 where field_1='hola'");
		 * </code>
		 * @param string $query Consulta SQL.
		 * @return OU_Query|boolean FALSE si no se ha podido leer el código.
		 */
		public static function q($query)
		{
			
			$arr = array();
			if (preg_match(OU_Query::$REGEXP_QUERYTYPE, $query, $arr))
			{
				$obj = new OU_Query ();
				$obj->setQueryType(strtolower($arr["queryType"]));
				
				switch (strtolower($obj->_queryType))
				{
					case "drop":
						$arr = array();
						if (preg_match(OU_Query::$REGEXP_QUERYDROPTABLE, $query, $arr))
						{
						
							if (isset($arr["tables"]) && !empty($arr["tables"]))
							{
								$obj->from($arr["tables"]);
							}
						
						}
						break;
					case "insert":
						$arr = array();
						if (preg_match(OU_Query::$REGEXP_QUERYINSERT, $query, $arr))
						{
							if (isset($arr["sets"]))
							{
								$obj->fields($arr["sets"]);
							}
								
							if (isset($arr["tables"]) && !empty($arr["tables"]))
							{
								$obj->from($arr["tables"]);
							}
								
						}
							break;
							
					case "truncate":
						$arr = array();
						if (preg_match(OU_Query::$REGEXP_QUERYTRUNCATE, $query, $arr))
						{
							if (isset($arr["tables"]) && !empty($arr["tables"]))
							{
								$obj->from($arr["tables"]);
							}
								
						}
							break;
						
					case "update":
						$arr = array();
						if (preg_match(OU_Query::$REGEXP_QUERYUPDATE, $query, $arr))
						{
							if (isset($arr["sets"]))
							{
								$obj->fields($arr["sets"]);
							}
								
							if (isset($arr["tables"]) && !empty($arr["tables"]))
							{
								$obj->from($arr["tables"]);
							}
								
							if (isset($arr["where"]) && !empty($arr["where"]))
							{
								$obj->where($arr["where"]);
							}
							
							if (isset($arr["limit"]) && !empty($arr["limit"]))
							{
								list($from, $count) = explode(",", $arr["limit"]);
								$obj->limit(intval($from), is_numeric($count) && $count > 0 ? intval($count) : false);
							}
							
							if (isset($arr["orderBy"]) && !empty($arr["orderBy"]))
							{
								$obj->orderBy($arr["orderBy"]);
							}
						
						}
						break;
					case "select":


						
						$arr = array();
						if (preg_match(OU_Query::$REGEXP_QUERYSELECT, $query, $arr))
						{
							if (isset($arr["fields"]))
							{
								$obj->fields($arr["fields"]);
							}
								
							if (isset($arr["tables"]) && !empty($arr["tables"]))
							{
								$obj->from($arr["tables"]);
							}
								
							if (isset($arr["where"]) && !empty($arr["where"]))
							{
								$obj->where($arr["where"]);
							}
							
							if (isset($arr["having"]) && !empty($arr["having"]))
							{
								$obj->having($arr["having"]);
							}
							
							if (isset($arr["limit"]) && !empty($arr["limit"]))
							{
								@list($from, $count) = explode(",", $arr["limit"]);
								$obj->limit(intval($from), is_numeric($count) && $count > 0 ? intval($count) : false);
							}
							
							if (isset($arr["orderBy"]) && !empty($arr["orderBy"]))
							{
								$obj->orderBy($arr["orderBy"]);
							}
							
							if (isset($arr["groupBy"]) && !empty($arr["groupBy"]))
							{
								$obj->groupBy($arr["groupBy"]);
							}
								
						}
						
						break;
				}
				
				return $obj;
				
			}else
				return false;
		}
		
		/**
		 * Devuelve la última conexión establecida. Consulta OU_Database::db() para más información.
		 * @see OU_Database::db()
		 * @return bool|OU_Database último OU_Database registrado, o FALSE si no existe.
		 */
		public static function db()
		{
			OU_Config::IncClass("OU_Database");
			$db = OU_Database::db();
			if ($db)
				return $db;
			else
				self::error("No se encuentra ninguna conexión establecida de la base de datos.");
		}
		
		/**
		 * Ejecuta la consulta actual en la última conexión establecida.
		 * @return OU_DB_Results Resultados de la consulta
		 */
		public function query($options=array())
		{
			$options = OU_Options::FromArray(
				$options,
				array(
					"db" => self::db()
				)
			);
			$sql = $this->sql();
			return $options->db->Query($sql, $options);
		}
		
		/**
		 * Igual a OU_Query->query() pero sin retorno de los resultados de la consulta.
		 * @param $options
		 * @return OU_Query Devuelve la instancia de sigo mismo.
		 * @see OU_Query::query() Vea para más información
		 */
		public function exec($options = array())
		{
			$this->query($options);
			return $this;
		}
		
		/**
		 * Comprueba si existen resultados para una consulta.
		 * @param OU_DB_Results $result
		 * @return boolean TRUE si existen datos de retorno, FALSE en caso contrario.
		 */
		public function exists(&$result = null)
		{
			$result = $this->query();
			return $result && count($result) > 0 ? true : false;
		}
		
		/**
		 * Genera una excepción.
		 * @param string $msg
		 * @throws Exception
		 */
		public static function error($msg = "Error al crear la consulta SQL")
		{
			throw new Exception($msg);
		}

		/**
		 * Obtén o asigna los sets de un update.
		 * @param null $sets
		 * @return OU_Query|string Devuelve el valor de sets si $sets es null, sino devuelve $this.
		 * @throws Exception
		 * @internal param array|OU_Array|OU_Options|string $fields
		 */
		public function sets($sets = null)
		{
			
			// Opciones o operaciones
			if ($sets instanceof OU_Options)
			{
				if ($sets->hasOption("clear"))
				{
					$this->_sets->Clear();
				}
				return $this;
			}
			
			// Lectura
			if ($sets === null)
			{
				$sql = $this->_sets->Join(",");
				return $sql;
			}
			
			// Assignar valor
			
			if (is_string($sets))
			{
				$sets = explode(",", $sets);
			}
			
			if ($sets instanceof OU_Array)
			{
				$sets = $sets->toArray();
			}
			
			if (is_array($sets))
			{
				foreach ($sets as $k=>$set)
				{
					if (is_numeric($k))
						$this->_sets->Add($set);
					else
					{
						if ($set === null)
							$val = "NULL";
						else
							$val = "'".$this->db()->slashes(trim($set))."'";
							
						$this->_sets->Add(trim($k)."=" . $val);
					}
				}
				
			}else 
				OU_Query::error();
				
			return $this;
				
		} 
		
		/**
		 * Obtén o asigna los campos de un select.
		 * @param string|OU_Array|array|OU_Options $fields
		 * @return string|OU_Query Devuelve el valor de fields si $fields es null, sino devuelve $this.
		 */
		public function fields($fields = null, $repalce = false)
		{
			
			// Opciones o operaciones
			if ($fields instanceof OU_Options)
			{
				if ($fields->hasOption("clear"))
				{
					$this->_fields->Clear();
				}
				return $this;
			}
			
			// Lectura
			if ($fields === null)
			{
				$sql = $this->_fields->Join(",");
				return $sql;
			}
			
			// Assignar valor
			
			if (is_string($fields))
			{
				if (trim($fields) == "") return $this;
				$fields = explode(",", $fields);
			}
			
			if ($fields instanceof OU_Array)
			{
				$fields = $fields->toArray();
			}
			
			if (is_array($fields))
			{
				if ($repalce) $this->_fields->Clear();
				foreach ($fields as $field)
				{
					if (is_string($field))
					{
						
						$field = trim($field);
						$this->_fields->Add($field);
						
					}else 
						OU_Query::error();
				}
				
			}else 
				OU_Query::error();
				
			return $this;
				
		} 
		
		/**
		 * @see OU_Query::from()
		 * @param string|OU_Array|array|OU_Options $tableName
		 * @return string|OU_Query 
		 */
		public function into($tableName = null)
		{
			return $this->from($tableName);
		}
		
		/**
		 * Obtén o asigna las tablas.
		 * @param string|OU_Array|array|OU_Options $tableName
		 * @return string|OU_Query Devuelve el valor de from si $tableName es null, sino devuelve $this.
		 */
		public function from($tableName = null)
		{
			// Opciones o operaciones
			if ($tableName instanceof OU_Options)
			{
				if ($tableName->hasOption("clear"))
				{
					$this->_tables->Clear();
				}
				return $this;
			}
				
			// Lectura
			if ($tableName === null)
			{
				$sql = $this->_tables->Join(",");
				return $sql;
			}
				
			// Assignar valor
					
			if (is_string($tableName))
			{
				$tableName = explode(",", $tableName);
			}
			
			if ($tableName instanceof OU_Array)
			{
				$tableName = $tableName->toArray();
			}
			
			if (is_array($tableName))
			{
				foreach ($tableName as $table)
				{
					if (is_string($table))
					{
						
						$table = trim($table);
						$this->_tables->Add($table);
						
					}else 
						OU_Query::error();
				}
				
			}else 
				OU_Query::error();
				
			return $this;
			
		}
		
		/**
		 * Obtén o asigna los having.
		 * @param string|OU_Array|array|OU_Options $having
		 * @return string|OU_Query Devuelve el valor de having si $having es null, sino devuelve $this.
		 */
		public function having($having = null, $concat_or = false)
		{
			// Opciones o operaciones
			if ($having instanceof OU_Options)
			{
				if ($having->hasOption("clear"))
				{
					$this->_having = "";
				}
				return $this;
			}
			
			// Lectura
			if ($having === null)
			{
				$sql = $this->_having;
				return $sql;
			}
			
			// Assignar valor
			
			if ($having instanceof OU_Array)
			{
				$having = $having->toArray();
			}
			
			if (is_array($having))
			{
				$a = array();
				foreach ($having as $k=>$v)
					$a[] = (strpos($k, ".") === false ? "`$k`" : $k) . "='".OU_Query::slashes($v)."'";
				$having = implode(",", $a);
			}
			
			if (is_string($having))
			{
				$having = trim($having);
				// Comprobar si existe un having
				if (!$this->_having)
				{
					$this->_having = $having;
				}else if (preg_match('/(AND|OR)\s*$/mix', $this->_having) > 0)
				{
					$this->_having .= " " . $having;
				}else{
					$this->_having .= " " . ($concat_or ? "OR" : "AND") . " " . $having;
				}
				
			}
			
			return $this;
		}
		
		/**
		 * Obtén o asigna los where.
		 * @param string|OU_Array|array|OU_Options $where
		 * @return string|OU_Query Devuelve el valor de where si $where es null, sino devuelve $this.
		 */
		public function where($where = null, $concat_or = false)
		{
			
			// Opciones o operaciones
			if ($where instanceof OU_Options)
			{
				if ($where->hasOption("clear"))
				{
					$this->_where = "";
				}
				return $this;
			}
				
			// Lectura
			if ($where === null)
			{
				$sql = $this->_where;
				return $sql;
			}
				
			// Assignar valor
					
			if ($where instanceof OU_Array)
			{
				$where = $where->toArray();
			}
				
			if (is_array($where))
			{
				$a = array();
				foreach ($where as $k=>$v)
					$a[] = (strpos($k, ".") === false && strpos($k, "(") === false ? "`$k`" : $k) . "='".OU_Query::slashes($v)."'";
				$where = implode(" AND ", $a);
			}
				
			if (is_string($where))
			{
				
				$where = trim($where);
		
				// Comprobar si existe un where
				if (!$this->_where)
				{
					$this->_where .= $where;
				}else if ($this->_where && preg_match('/(AND|OR)\s*$/mix', $this->_where) > 0)
				{
					$this->_where .= " " . $where;
				}else{
					$this->_where .= " " . ($concat_or ? "OR" : "AND") . " " . $where;
				}
		
			}
				
			return $this;
		}
		
		/**
		 * Obtén o asigna los "order by" de un select.
		 * @param number|null|OU_Options $orderBy
		 * @return string|OU_Query Devuelve el valor de orderBy si $orderBy es null, sino devuelve $this.
		 */
		public function orderBy($orderBy = null)
		{

			// Opciones o operaciones
			if ($orderBy instanceof OU_Options)
			{
				if ($orderBy->hasOption("clear"))
				{
					$this->_orderBy->Clear();
				}
				return $this;
			}
				
			// Lectura
			if ($orderBy === null)
			{
				return $this->_orderBy->Join(",");
			}
			
			// Assignar valor
					
			if (is_string($orderBy))
			{
				$this->_orderBy->Clear();
				$this->_orderBy->Add(trim($orderBy));				
			}
			
			if ($orderBy instanceof OU_Array)
			{
				$orderBy = $orderBy->toArray();
			}
			
			if (is_array($orderBy))
			{
				foreach ($orderBy as $o)
					$this->_orderBy->Add(trim($o));
			}
			
			return $this;
			
		}
		
		/**
		 * Obtén o asigna los "group by" de un select.
		 * @param number|null|OU_Options $groupBy
		 * @return string|OU_Query Devuelve el valor de groupBy si $groupBy es null, sino devuelve $this.
		 */
		public function groupBy($groupBy = null)
		{
			
			// Opciones o operaciones
			if ($groupBy instanceof OU_Options)
			{
				if ($groupBy->hasOption("clear"))
				{
					$this->_groupBy->Clear();
				}
				return $this;
			}
				
			// Lectura
			if ($groupBy === null)
			{
				return $this->_groupBy->Join(",");
			}
			
			// Assignar valor
					
			if (is_string($groupBy))
			{
				$this->_groupBy->Add($groupBy);
			}
				
			if ($groupBy instanceof OU_Array)
			{
				$groupBy = $groupBy->toArray();
			}
			
			if (is_array($groupBy))
			{
				foreach ($groupBy as $o)
					$this->groupBy->Add($o);
			}
				
			return $this;
				
		}
		
		/**
		 * Obtén o asigna los limites de un select.
		 * @param number|null|OU_Options $from
		 * @return string|OU_Query Devuelve el valor de limit si $from es null, sino devuelve $this.
		 */
		public function limit($from = null, $count = false)
		{
			// Opciones o operaciones
			if ($from instanceof OU_Options)
			{
				if ($from->hasOption("clear"))
				{
					$this->_limitCount = false;
					$this->_limitFrom = false;
				}
				return $this;
			}
			
			// Lectura
			if ($from === null)
			{
				if ($this->_limitFrom === false && $this->_limitCount === false)
					return false;
				else
					return $this->_limitFrom . ($this->_limitCount ? "," . $this->_limitCount : "");
			}
						
			// Assignar valor
			
			$this->_limitCount = $count;
			$this->_limitFrom = $from;
			
			return $this;
			
		}
		
		/**
		 * Genera el código SQL.
		 * @return string
		 */
		public function sql()
		{
			
			switch ($this->_queryType)
			{
				
				case "drop":
					
					$sql = $this->_queryType . " TABLE " . $this->from();
					
					break;
				
				case "select":
			
					$sql = $this->_queryType . " " . $this->fields() . " FROM " . $this->from();
					$r = $this->where(); if ($r) $sql .= " WHERE $r";
					$r = $this->groupBy(); if ($r) $sql .= " GROUP BY $r";
					$r = $this->having(); if ($r) $sql .= " HAVING $r";
					$r = $this->orderBy(); if ($r) $sql .= " ORDER BY $r";
					$r = $this->limit(); if ($r !== false) $sql .= " LIMIT $r";
					
					break;
				
				case "update":
					
					$sql = $this->_queryType . " " . $this->from();
					$r = $this->sets(); if ($r) $sql .= " SET $r";
					$r = $this->where(); if ($r) $sql .= " WHERE $r";
					$r = $this->orderBy(); if ($r) $sql .= " ORDER BY $r";
					$r = $this->limit(); if ($r !== false) $sql .= " LIMIT $r";
					
					break;
			
				case "delete":
					
					$sql = $this->_queryType . " from " . $this->from();
					$r = $this->where(); if ($r) $sql .= " WHERE $r";
					$r = $this->limit(); if ($r !== false) $sql .= " LIMIT $r";
					
					break;
			
				case "insert":
					
					$sql = $this->_queryType . " INTO " . $this->from();
					$r = $this->sets(); if ($r) $sql .= " SET $r";
					
					break;
					
				case "truncate":
					
					$sql = $this->_queryType . " " . $this->from();
					
					break;
			
			}
			
			return $sql;
			
		}
		
		public function __toString()
		{
			return $this->sql();
		}
		
	}

?>