<?php

	$code = file_get_contents(dirname(__FILE__) . "/files/syntax.php");
	
	OU_Debug_Colorize::loadTheme(dirname(__FILE__) . "/files/theme-2.xml");
	echo OU_Debug_Colorize::syntax($code);

	OU_Debug_Colorize::loadTheme();
	echo OU_Debug_Colorize::syntax($code);

?>