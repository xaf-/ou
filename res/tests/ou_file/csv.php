<table>
<?php 
	$file = new OU_File_CSV(dirname(__FILE__) . "/files/test.csv");
	$file->open();
	
	echo "<tr>";
	for ($x = 0; $x < $file->columnCount; $x++)
		echo "<th>" . $file->indexToString($x) . "</th>";
	echo "</tr>";
	
	for ($y = 1; $y <= $file->rowCount; $y++)
	{
		echo "<tr>";
		for ($x = 0; $x < $file->columnCount; $x++)
			echo "<td>" . $file->cell($x, $y)->value . "</td>";
		echo "</tr>";
	}
?>
</table>