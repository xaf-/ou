<ou:register name="test_control_text" strip="true">
	<input ou:param="type" ou:param="test_control |class|" ou:param="value" /> <ou:content />
</ou:register>

<ou:register name="test_control_password" inherit="test_control_text" strip="true" params='{"class":"test_password", "type":"password"}'>
	(<ou:content />)
</ou:register>

<ou:test_control_text value="test abc">content abc</ou:test_control_text>
<ou:test_control_password value="test def">content def</ou:test_control_password>