<style>
	.test_page_header { background: red; }
	.test_page_body { background: green; }
	.test_page_footer { background: blue; }
</style>
<ou:register name="test_page" strip="true">
	<div class="test_page_header"><ou:part_content name="header" /></div>
	<div class="test_page_body"><ou:part_content name="body" /></div>
	<div class="test_page_footer"><ou:part_content name="footer" /></div>
</ou:register>

<ou:test_page>
	<ou:part name="header">Header content</ou:part>
	<ou:part name="body">Body content</ou:part>
	<ou:part name="footer">Footer content</ou:part>
</ou:test_page>