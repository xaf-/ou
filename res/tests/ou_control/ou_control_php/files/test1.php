	
	<div test="x">
	</div>

	<ou:register name="page">
	
		<html>
		<head>
			<ou:js file="ext/jquery/1.8.3.js" param2="asd" />
			<ou:css file="controls.css" param2="asd" />
			<ou:part_content name="head" />
		</head>
		<body ou:param="class| adeu">
			<div class="header">
				<ou:part_content name="header" />
			</div>
			<div>
				<ou:content />
			</div>
			<div class="footer">
				<ou:part_content name="footer" />
			</div>
		</body>
		</html>
		
	</ou:register>
	
	<ou:register name="subpage" inherited="page">
		<ou:content />
	</ou:register>
	
	<ou:register name="test1_test">
		<div class="test">
			<ou:content />
			<br />-----------------<br />
		</div>
	</ou:register>
	
	<ou:register name="button" strip="true">
		<button ou:param="style">
			<ou:content />
		</button>
	</ou:register>
	
	<ou:register name="textbox" strip="true">
		<input 
			ou:param="type" 
			ou:param="style" 
			ou:param="text |class|" 
			<?php if (isset($this->content)) { ?>value="<?php echo addslashes($this->content); ?>" <?php } ?> 
		/>
	</ou:register>
	
	<ou:register name="textpass" inherited="textbox" class="pass" type="password"><ou:content /></ou:register>
	
	<?php /* -------- TEST ---------- */ ?>
	
	<ou:subpage class="hola">
	
		<ou:part name="header">
			<h1>Titol 1</h1>
		</ou:part>
	
		<ou:part name="footer">
			<h3>footer</h3>
		</ou:part>
			
		<ou:test1_test>
		<?php
			echo get_class($this) . '<br/>';
			echo $this->controller->uri();
		?>
		</ou:test1_test>
		
		<?php
			$var = "test"; 
		?>
		
		<ou:test1_test>
			
			<?php for ($i = 0; $i < 10; $i++) { ?>
				<ou:button><?=$var?> <?=$i?></ou:button>
			<?php } ?>
			
			<ou:button xxx="asd" style="background: red">asd</ou:button>	
			
		</ou:test1_test>
		
		<ou:test1_test>
			<ou:textbox>value_test</ou:textbox>
			<ou:textpass>value_test</ou:textpass>
		</ou:test1_test>
				
		<?php
			echo "hola"; 
		?>
		
	</ou:subpage>