<?php

	$instance = OU_Path::Create(__FILE__);

	function demo_instance_test($instance)
	{
		echo $instance . "<br>";
		echo "parent : " . $instance->parentDirectory . "<br>";
		echo "name : " . $instance->name . "<br>";
		echo "isDir : " . ($instance->isDir ? "true" : "false") . "<br>"; 
		echo "isFile : " . ($instance->isFile ? "true" : "false") . "<br>";
		echo "isUrl : " . ($instance->isUrl ? "true" : "false") . "<br>";
		echo "ext : " . $instance->ext . "<br>";
		echo "url : " . $instance->url . "<br>";
		echo "breadcrumb : " . $instance->breadcrumb . "<br>";
		echo "query: " . $instance->query . "<br>";
		echo "hash: " . $instance->hash . "<br>";
	}

	demo_instance_test($instance);
	
	$instance->ext = ".jpg";
	echo str_repeat("-", 100) . "<br>";
	
	demo_instance_test($instance);

	$instance->parentDirectory = dirname(__FILE__) . "/..";
	echo str_repeat("-", 100) . "<br>";
	
	demo_instance_test($instance);
	
	echo str_repeat("-", 100) . "<br>";
	$instance = OU_Path::Create("/abc/def/index.php?h=123#xxx");
	$instance->queryVars->Add("var1", "value1");
	$instance->hash = "123123";
	$instance->name = "hello";
	$instance->ext = ".txt.doc";
	
	demo_instance_test($instance);

?>