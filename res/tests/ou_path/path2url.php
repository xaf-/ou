<?php 

	// Convierte una dirección local a URL.
	$url = OU_Path::Path2Url(dirname(__FILE__));
	echo "url = $url\n";

	// Convierte una dirección local a URL con https.
	$url = OU_Path::Path2Url(dirname(__FILE__), array("useSSH" => true));
	echo "url = $url\n";

	// Convierte una dirección local a URL sin el host.
	$url = OU_Path::Path2Url(dirname(__FILE__), array("useHost" => false));
	echo "url = $url\n";

?>