# Compiladores

Existen una serie de compiladores que extienden la funcionalidad del servidor. Algunos ejemplos: 

## Compilador LESS

[LessCSS The dynamic stylesheet language.](http://lesscss.org/)

	@the-border: 1px;
	@base-color: #111;
	@red:        #842210;

	#header {
  		color: @base-color * 3;
  		border-left: @the-border;
  		border-right: @the-border * 2;
	}
	#footer { 
  		color: @base-color + #003300;
  		border-color: desaturate(@red, 10%);
	}

* Falta afegir plugins... (-webkit-*)

## Compilador JS

[JS Closure](https://developers.google.com/closure/compiler/)

Añade la libraria Closure Library UI y minimiza el código.

## Compilador SCSS

[SCSS/SASS Language](http://sass-lang.com/)

	$blue: #3bbfce;
	$margin: 16px;

	.content-navigation {
		border-color: $blue;
		color: darken($blue, 9%);
	}
	.border {
		padding: $margin / 2;
		margin: $margin / 2;
		border-color: $blue;
	}
	
## Compilador TPL

[TPL Smarty Template Engine](http://www.smarty.net/)

Proporciona la interficie suficiente para ejecutar los archivos tpl de Smarty.

## Compilador MD

[MD Markdown](http://daringfireball.net/projects/markdown/syntax)

