# Controladores

## Introducción

Los controladores controlan la salida del contenido que se va a mostrar al navegador. El controlador a ejecutar será el que coincida con las expresiones regulares de cada uno con la dirección URI actual (URI relativa a la raíz de la aplicación).

Se tratan de clases deribadas de [OU_Controller](../../../classes/OU_Controller/) con una información añadida al inicio de fichero para indicar el funcionamiento de cada uno. La información se declara con el mismo formato que PHPDoc pero con tres asteríscos iniciales.

A continuación aparecen las opciones disponibles para los controladores. Cualquier opción que no aparezca a la lista se puede utilizar libremente para guardar un valor personalizable.

<table>
	<tr>
		<td>@route</td>
		<td>
			Deve de contener un patrón de expresión regular. 
			El valor de esta opción es la que determina si este controlador será ejecutado dependiendo de la URI relativa actual.
		</td>
	</tr>
	<tr>
		<td>@extend</td>
		<td>Permite extender la búsqueda de controladores a un subdirectorio. </td>
	</tr>
	<tr>
		<td>@uri</td>
		<td>Sirbe para indicar como generar la URI en caso de ejecutar <a href="../../../classes/OU_Controller/#m:uri">OU_Controller::uri()</a>.
	</tr>
	<tr>
		<td>@use</td>
		<td>Facilita el requerimiento de dependencias buscando los archivos en la librería o de otros controladores.</td>
	</tr>
</table>   

Existe la posiblidad de utilizar funciones para los valores de las opciones. Se debe indicar de la siguietne forma. fnc:NombreDeLaFunción, donde NombreDeLaFunción debe ser una función existente del controlador.

## Funcionamiento

Los controladores son creados siempre, esto es necesario para poder acceder a la información de todos los controladores en todo momento. Tenga en cuenta esto para evitar sobrecargar los constructores. Para inicializar variables o procesar información debe realizarse en <a href="../../../classes/OU_Controller/#m:_initialize">OU\_Controller::_initialize().</a>

Los controladores nunca deben de crearse, esto se ocupa la aplicación. Vea la sección de compiladores para más información.

Las funciones más importantes son las siguientes:

### _initialize() [<small>(Ver)</small>](../../../classes/OU_Controller/#m:_initialize)

> Sirbe para inicializar toda aquella información a ejecutar al principio de todo. Un ejemplo: session_start().

### post() [<small>(Ver)</small>](../../../classes/OU_Controller/#m:post)

> En esta función se controla todas las operaciones que se deben ejecutar antes de generar el contenido. Por ejemplo: establecer variables de sesión, controlar las variables de formularios recibidas, crear redirecciones, etc.

### pre_content() [<small>(Ver)</small>](../../../classes/OU_Controller/#m:pre_content)

> Se ejecuta entre el proceso post() y content(). Ideal para terminar las conexiones de base de datos o cerrar la sesión para permitir múltiples accesos a la aplicación de un mismo usuario.

### content() [<small>(Ver)</small>](../../../classes/OU_Controller/#m:content)

> En este apartado se genra el contenido de la aplicación que se mostrará al navegador. Utiliza el retorno de la función para devolver el contenido. Puedes utilizar las funciones del controllador, [tpl](../../../OU_Controller/#m:tpl) y [php](../../../OU_Controller/#m:php) para usar los compiladores correspondientes. 