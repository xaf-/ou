# Aplicaciones

## Introducción

Se pueden crear aplicaciones de manera simple y rápida mediante controladores.  

Las aplicaciones tambien se ocupan de gestionar y compilar los recursos de la web y sus caches.


## Estructura de archivos

Es necesario que los archivos se encuentren distribuidos de una forma concreta:

<table>
	<tr>
		<th>Variable</th>
		<th>Valor por defecto</th>
		<th></th>
	</tr>
	<tbody>
		<tr>
			<td>OU_App::$_dir_tpl</td>
			<td>tpl</td>
			<td>Archivos de smarty</td>
		</tr>
		<tr>
			<td>OU_App::$_dir_config</td>
			<td>config</td>
			<td>Archivos de configuración (.cnf, .ini, .php, ...)</td>
		</tr>
		<tr>
			<td>OU_App::$_dir_controllers</td>
			<td>controllers</td>
			<td>Archivos de controladores</td>
		</tr>
		<tr>
			<td>OU_App::$_dir_libs</td>
			<td>classes</td>
			<td>Archivos de classes de php</td>
		</tr>
		<tr>
			<td>OU_App::$_dir_sql</td>
			<td>sql</td>
			<td>Archivo sql. Puede ejecutarse con código de smarty con OU_Database::runSQL($fileName)</td>
		</tr>
		<tr>
			<td>OU_App::$_dir_db</td>
			<td>db</td>
			<td>Archivos de bases de datos</td>
		</tr>
		<tr>
			<td>OU_App::$_dir_lang</td>
			<td>lang</td>
			<td>Archivos de traducciones en diferentes lenguajes. Ex: (CAT.lang, ES.lang, ...)</td>
		</tr>
	</tbody>
</table>  

## Controladores

Los controladores controlan la salida del contenido que se va a mostrar al navegador. El controlador a ejecutar será el que coincida con las expresiones regulares de cada uno con la dirección URI (URI relativa a la raíz de la aplicación) actual.

[Más información](../App/Controllers.md/)

Cada controlador debe proporcionar la información necesaria. Esta información se indica en los comentarios de inicio de archivo de la siguiente forma:

	/*** 
	 * @route list\/(?<entity>\w+)\/orderBy\/(?<column>\w+)\/
	 * @customVar Bla blabla bla 
	 * @use OU_Path, CustomClass 
	 */
	 
	 class MyController extends OU_Controller{
	 	public funtion content() { return "hello"; }
	 }

<p></p>
Ejemplo : http://.../base/list/persons/orderBy/name/
<p></p>
	 
	 @route		Expresión regular a comprobar con el URI. (puede haber múltiples). Se puede acceder al resultado mediante OU_Controller::$_request.
	 @use		Listado de clases a incluir separado por comas. Las clases se buscan con la función OU_App::Inc*.
	 @customVar Variables personalizadas. Se puede acceder mediante OU_Controller::$_vars.

<p></p>

*¡ Los comentarios deben empezar por "/" seguido de 3 asteriscos "***" !*

*¡ No puede haber más de un controlador en el mismo archivo !*

