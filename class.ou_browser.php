<?php

	/*
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 27/06/2012
	 */

	OU_Config::IncClass("OU_Base");
	
	OU_Config::IncExt("browser");
	 
	/**
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_browser.php 187 2013-03-07 12:00:48Z xaguilarf $
	 * @example		ou_browser.php
	 */
	class OU_Browser extends OU_Base
	{
		
		private static function _userAgent()
		{
			return isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';;
		}
		
		private static $_bwsr = null;
		/**
		 * @return Browser
		 */
		private static function _getBwsr()
		{
			if (self::$_bwsr === null)
			{
				self::$_bwsr = new Browser(self::_userAgent());
			}
			return self::$_bwsr;
		}
		
		public static function info()
		{
			$browser = self::_getBwsr();
			
			$info = array();
			$info["browser"] = $browser->getBrowser();
			$info["platform"] = $browser->getPlatform();
			$info["version"] = $browser->getVersion();
			
			return $info;
			
		}
		
		public static function version()
		{
			return self::_getBwsr()->getVersion();
		}
		
		public static function isGreaterVersion($version)
		{
			return version_compare(self::version(), $version, '>=');
		}

		public static function isLowerVersion($version)
		{
			return version_compare(self::version(), $version, '<');
		}

		public static function isIPhone()
		{
			return (strpos(self::_userAgent(), 'iPhone') !== FALSE);
		}
		
		public static function isIPad()
		{
			return (strpos(self::_userAgent(), 'iPad') !== FALSE);
		}
		
		public static function isAndroid()
		{
			return (strpos(mb_strtolower(self::_userAgent()), 'android') !== FALSE);
		}
		
		public static function isBlackBerry()
		{
			return (strpos(mb_strtolower(self::_userAgent()), 'blackberry') !== FALSE);
		}
		
		public static function isIE()
		{
			return (strpos(mb_strtolower(self::_userAgent()), 'msie') !== FALSE);
		}
		
		public static function isGecko()
		{
			return (strpos(self::_userAgent(), 'Gecko') !== FALSE);
		}
		
		public static function isFirefox()
		{
			return (strpos(self::_userAgent(), 'Firefox') !== FALSE);
		}
		
		public static function isChrome()
		{
			return (strpos(self::_userAgent(), 'Chrome') !== FALSE);
		}
		
		public static function isSafari()
		{
			return (strpos(self::_userAgent(), 'Safari') !== FALSE) && !self::isChrome();
		}
		
		public static function isOpera()
		{
			return (strpos(self::_userAgent(), 'Opera') !== FALSE);
		}
		
		public static function isWebkit()
		{
			return (strpos(mb_strtolower(self::_userAgent()), 'webkit') !== FALSE);
		}
		
		public static function isWindows()
		{
			return (strpos(mb_strtolower(self::_userAgent()), 'windows') !== FALSE);
		}
		
		public static function isLinux()
		{
			return (strpos(mb_strtolower(self::_userAgent()), 'linux') !== FALSE);
		}
		
		public static function isMac()
		{
			return (strpos(mb_strtolower(self::_userAgent()), 'macintosh') !== FALSE) ||
				   (strpos(mb_strtolower(self::_userAgent()), 'mac_powerpc') !== FALSE);
		}
		
		public static function isBot()
		{
			return self::OS() == "Search Bot";
		}
		
		public static function isMobile()
		{
			return 
				self::isAndroid() ||
				self::isBlackBerry() ||
				self::isIPad() ||
				self::isIPad();
		}
		
		public static function lang() {
			if (isset($_SERVER["HTTP_ACCEPT_LANGUAGE"]))
				return self::_parseDefaultLanguage($_SERVER["HTTP_ACCEPT_LANGUAGE"]);
			else
				return self::_parseDefaultLanguage(NULL);
		}
		
		private static function _parseDefaultLanguage($http_accept, $deflang = "en") {
			if(isset($http_accept) && strlen($http_accept) > 1)  {
				# Split possible languages into array
				$x = explode(",",$http_accept);
				foreach ($x as $val) {
					#check for q-value and create associative array. No q-value means 1 by rule
					if(preg_match("/(.*);q=([0-1]{0,1}\.\d{0,4})/i",$val,$matches))
						$lang[$matches[1]] = (float)$matches[2];
					else
						$lang[$val] = 1.0;
				}

				#return default language (highest q-value)
				$qval = 0.0;
				foreach ($lang as $key => $value) {
					if ($value > $qval) {
						$qval = (float)$value;
						$deflang = $key;
					}
				}
			}
			return strtolower($deflang);
		}
		
		public static function OS() {
			
			$userAgent = self::_userAgent();
			
			$oses = array (
					'iPhone' => '(iPhone)',
					'Windows 3.11' => 'Win16',
					'Windows 95' => '(Windows 95)|(Win95)|(Windows_95)', // Use regular expressions as value to identify operating system
					'Windows 98' => '(Windows 98)|(Win98)',
					'Windows 2000' => '(Windows NT 5.0)|(Windows 2000)',
					'Windows XP' => '(Windows NT 5.1)|(Windows XP)',
					'Windows 2003' => '(Windows NT 5.2)',
					'Windows Vista' => '(Windows NT 6.0)|(Windows Vista)',
					'Windows 7' => '(Windows NT 6.1)|(Windows 7)',
					'Windows NT 4.0' => '(Windows NT 4.0)|(WinNT4.0)|(WinNT)|(Windows NT)',
					'Windows ME' => 'Windows ME',
					'Open BSD'=>'OpenBSD',
					'Sun OS'=>'SunOS',
					'Linux'=>'(Linux)|(X11)',
					'Safari' => '(Safari)',
					'Macintosh'=>'(Mac_PowerPC)|(Macintosh)',
					'QNX'=>'QNX',
					'BeOS'=>'BeOS',
					'OS/2'=>'OS/2',
					'Search Bot'=>'(nuhk)|(Googlebot)|(Yammybot)|(Openbot)|(Slurp/cat)|(msnbot)|(ia_archiver)'
			);
		
			foreach($oses as $os=>$pattern){ 
				if(preg_match("/$pattern/", $userAgent)) { 
					return $os; 
				}
			}
			return 'Unknown'; 
			
		}
		
	}

?>