<?php

	/**
	 * The source code is given as is. The author is not responsible           
	 * for any possible damage done due to the use of this code.                 
	 * The component can be freely used in any application. The complete         
	 * source code remains property of the author and may not be distributed,    
	 * published, given or sold in any form as such. No parts of the source      
	 * code can be included in any other component or application without        
	 * written authorization of fontcolor. 13/11/2012
	 */

	OU_Config::IncClass(
		array(
			"OU_Base",
			"OU_Path",
			"OU_Debug",
			"OU_Options"
		)
	);
	
	OU_Config::IncClassHelper(
		array(
			"OU_WSDL_Descriptor"
		)
	);
	
	OU_Config::IncExt("nusoap");
	 
	/**
	 * Crea un servidor WSDL. El servidor registra todos los metodos de todas las clases encontradas de los archivos php que se 
	 * encuentren en la carpeta especificada. 
	 * <ul>
	 * 		<li>Utiliza OU_Debug para detectar toda la información de la clase. (métodos, parametros,...)</li>
	 * 		<li>Utiliza PHPDocs para resolver los tipos</li>
	 * 		<li>Utiliza OU_Comments para extender la funcionalidad en cada tipo: *todo</li>
	 * 		<li>Solo se registran las clases no abstractas y con el tag en phpDoc "wsdl"</li>
	 * 		<li>Solo se registran los metodos publicos</li>
	 * </ul>  
	 * @author		fontcolor
	 * @package		OU Framework
	 * @copyright	Copyright (c) 2012 fontcolor (http://x-s.es)
	 * @version		$Id: class.ou_wsdl.php 171 2013-01-30 12:04:44Z xaguilarf $
	 *
	 */
	class OU_WSDL extends OU_Base
	{
		
		/**
		 * @var OU_Array
		 */
		protected  $_ports;
        public $assoc_request = array();
		
		private static $_instance = null;
		/**
		 * @return OU_WSDL
		 */
		public static function instance()
		{
			return self::$_instance;
		}
		
		private $_path;
		/**
		 * @var soap_server
		 */
		public $_server;
		public $_options;
		
		public function setError($str, $code = "Server")
		{
			$this->_server->fault($code, $str);
		}
		
		public function __construct($path, $options = array())
		{
			$this->_ports = new OU_Array();
			self::$_instance = $this;
			$options = OU_Options::FromArray(
				$options, 
				array(
					"autoload" => false,
					"uri" => OU_Path::Path2Url($_SERVER["SCRIPT_FILENAME"]) . "/",
					"url" => OU_Path::Path2Url($_SERVER["SCRIPT_FILENAME"]) . "/",
					"name" => "OU Web Service"
				)
			);
			$this->_options = $options;
			$this->_path = realpath($path);
			$this->_server = new soap_server();
            $this->_server->decode_utf8 = false;

			$this->_server->custommethod = "__executeWSDL";

			$this->_server->configureWSDL(
				$options->name,
				false,
				$options->url,
				'rpc', 'http://schemas.xmlsoap.org/soap/http' //, 'http://soapinterop.org/xsd'
			);

            $this->_server->soap_defencoding = "UTF-8";
            $this->_server->decode_utf8 = false;


			if ($options->autoload) {
				$this->_load();
			}

		}
		
		private $_types = array();
		private function _getType($type, &$info = array())
		{

            $isArray = false;
            $info["type"] = $type;

			if (isset($this->_types[$type]))
			{
				$type = "tns:" . $this->_types[$type];
			}
			else
			{

                $isArray = false;
                if (substr($type, -2) == "[]")
                {
                    $isArray = true;
                    $type = substr($type, 0, -2);
                    $info["isArray"] = true;
                }

                $forceNative = false;

                switch (trim(strtolower($type))){
                    case "bool":
                        $type = "boolean";
                        break;
                    case "datetime":
                        $type = "dateTime";
                        $forceNative = true;
                        break;
                    case "number":
                    case "int":
                    case "int32":
                        $type = "integer";
                        break;
                    case "float":
                    case "double":
                    case "extended":
                        $type = "decimal";
                        break;
                }

                if (class_exists($type) && !$forceNative)
                {
                    $this->addComplexTypeFromClass($type, $isArray);
                    $type = "tns:" . $type;
                }else{
                    $type = "xsd:" . $type;
                }

			}

            $info["isArray"] = $isArray;

			return $type;
		}
		
		private function _registerClass($className)
		{


			$class = OU_Debug::reference($className);

			if (!$class->comments()->comment("wsdl")) return;
			if ($class->isAbstract()) return;
				
			foreach ($class->methods() as $method)
			{
				/* @var $method OU_Debug_Method */

				// Nomes registra methodes amb "final"
				if (!$method->isFinal()) continue;

				$out = array();
				
				$type = $method->result("string");
				$type = $this->_getType($type, $info);
					
				$out["result"] = $type;
                if ($info["isArray"]){
                    $out["result"] = $this->_createArray($type);
                }
				
				$name = $class->name() . "." . $method->name();
				
				//
				/*
				$in = array();
				$params = array();
				foreach ($method->params() as $param)
				{
					/* @var $param OU_Debug_Parameter * /
					$in[$param->name()] = $this->_getType($param->type("string"));					
				}
				*/
				
				$namer = $name . ".Request";

                $this->assoc_request[$namer] = array(
                    "className" => $class->name(),
                    "methodName" => $method->name()
                );

				$e = array();
				foreach ($method->params() as $param)
				{
					/* @var $param OU_Debug_Parameter */

					 $a = array(
						 "name" => $param->name(),
						 "type" => $this->_getType($param->type("string"), $info)
					);

                    if ($info["isArray"]){
                        $a["type"] = "tns:".$this->_createArray($a["type"]);
                    }else {

                        if ($param->isOptional()) {
                            $a["minOccurs"] = 0;
                        } else {
                            $a["minOccurs"] = 1;
                        }

                        if ($info["isArray"]) {
                            $a["maxOccurs"] = "unbounded";
                        } else {
                            $a["maxOccurs"] = "1";
                        }

                    }

                    $e[$param->name()] = $a;
					 
				}
				
				$this->addComplexType($namer, $namer, $e);

				 $in = array("request" => $this->_getType($namer));

				// Register message/part
				$this->_server->register(
					$name,
					$in,
					$out,
					false,
					$this->_options->uri . urlencode($name) . "/",
					'rpc',
					'encoded',
					$method->description()
				);
				
			}
		}
		
		private function _loadFile($fileName)
		{
			$content = file_get_contents($fileName);
			$m = array();
			if (preg_match_all("/class (?P<className>\w+)(\s|\n\r)+(extends|implements|\{)/m", $content, $m))
			{
				require_once($fileName);
				foreach ($m["className"] as $className)
				{
					$this->_registerClass($className);
				}
			}
		}
		
		private function _extendPath($path)
		{
			$path = $path . "/*.php";
			foreach (glob($path) as $file)
			{
				if (is_file($file))
				{
					$this->_loadFile($file);
				}
			}
		}
		
		private function _load()
		{
			$path = $this->_path . "/";
			$this->_configure();
			$this->_extendPath($path);
		}
		
		public function load()
		{
			$this->_load();
		}
		
		public function addSimpleType($name, $base, $enum = array())
		{
			$this->_types["$name"] = "$name";
			$type = $this->_getType($base);
			$this->_server->wsdl->addSimpleType(
				$name, 
				$type, 
				$name, 
				'scalar', 
				$enum);
		}
		
		public function addComplexTypeFromClass($className)
		{
			$class = OU_Debug::reference($className);
			$elements = array();
			foreach ($class->props() as $prop)
			{
				/* @var $prop OU_Debug_Property */
				
				if (!$prop->isPublic()) continue;

				$elements[$prop->name()] =
					array(
						"name" => $prop->name(),
						"type" => $this->_getType($prop->type("string"), $info)
					);

                if ($info["isArray"]){
                    $elements[$prop->name()]["maxOccurs"] = "unbounded";
                }
				
			}
			$this->addComplexType($className, $className, $elements, "struct");
		}
		
		public function addComplexType($name, $type, $elements = array(), $phpType = "array", $attributes = array())
		{

			$this->_types[$type] = $name;
			$this->_server->wsdl->addComplexType(
					$name,
					"complexType",
					$phpType,
					'sequence',
					'',
					$elements,
					$attributes
			);

		}
		
		protected function _configure()
		{
			// $this->addComplexType("arrayString", "array");
		}

        public function isWSDL(){

            $h = getallheaders();
            return isset($_REQUEST["wsdl"]) || isset($h["SOAPAction"]);
        }
		
		public function service()
		{
				
			global $HTTP_RAW_POST_DATA;
			$data = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : file_get_contents( 'php://input' );

			if ($this->isWSDL())
			{

				error_reporting(3);

				$this->_server->service($data, function(){
					$_REQUEST = OU_WSDL::instance()->_server->methodparams;
				});

			}else{
		
				$desc = new OU_WSDL_Descriptor($this);
				$desc->generate();

			}
				
		}

        private $_arrays = array();

        /**
         * @return string
         */
        protected function _createArray($type)
        {

            $t = \OU_WSDL_Descriptor::Correct($type);
            $name = $t . "Array";

            if (!isset($this->_arrays[$name]))
            {

                $this->_server->wsdl->addComplexType(
                    $name,
                    "complexType",
                    "array",
                    '',
                    'SOAP-ENC:Array',
                    array(),
                    array(
                        array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:' . $t . '[]')
                    ),
                    'tns:' . $t
                );

                $this->_arrays[$name] = "";

            }

            return $name;
        }

    }
	
?>