<?php

	/**
	 * @property number $Position
	 * @property-read number $Size
	 */
	class OU_Steam extends OU_Base
	{

		private $_pointer;

		public function __construct($filename, $mode){
			$this->_pointer = fopen($filename, $mode . "b");
		}

		public function __destruct()
		{
			fclose($this->_pointer);
		}

		protected function getPosition(){ return ftell($this->_pointer); }
		protected function setPosition($pos){ fseek($this->_pointer, $pos, SEEK_SET); }

		protected function getSize(){
			$s = fstat($this->_pointer);
			return $s["size"];
		}

		public function Read($count)
		{
			return fread($this->_pointer, $count);
		}

		public function Write($buffer, $count = null){

			if ($count === null) $count = strlen($buffer);
			$s = ""; for ($i = 0; $i < $count - strlen($buffer); $i++) $s .= "\0";
			return fwrite($this->_pointer, $s . $buffer, $count);
		}

		public function WriteInt32($int) { $this->Write(pack("l", $int)); }
		public function ReadInt32() { return unpack("l", $this->Read(4))[1]; }

		public function WriteUInt32($int) { $this->Write(pack("L", $int)); }
		public function ReadUInt32() { return unpack("L", $this->Read(4))[1]; }

		public function WriteInt16($int) { $this->Write(pack("s", $int)); }
		public function ReadInt16() { return unpack("s", $this->Read(2))[1]; }

		public function WriteUInt16($int) { $this->Write(pack("S", $int)); }
		public function ReadUInt16() { return unpack("S", $this->Read(2))[1]; }

	}

?>